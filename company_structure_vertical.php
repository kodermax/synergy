<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Структура компании (вертикаль)");
?><?$APPLICATION->IncludeComponent(
	"mcart:mcart.structure.visual", 
	".default", 
	array(
		"MAX_DEPTH" => "5",
		"DETAIL_URL" => "/company/structure.php?set_filter_structure=Y&structure_UF_DEPARTMENT=#ID#",
		"PROFILE_URL" => "/company/personal/user/#ID#/",
		"PM_URL" => "/company/personal/messages/chat/#ID#/",
		"NAME_TEMPLATE" => "",
		"SHOW_LOGIN" => "Y",
		"USE_USER_LINK" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "2592000",
		"PATH_TO_VIDEO_CALL" => "/company/personal/video/#USER_ID#/"
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>