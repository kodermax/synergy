<?php


ini_set("memory_limit","1024M");
set_time_limit(86400);
define("NEED_AUTH", false);
define("NOT_CHECK_PERMISSIONS", true);
error_reporting(E_ALL ^E_NOTICE);
$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
require($_SERVER["DOCUMENT_ROOT"]."/crm/deal/def.ini.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
while(@ob_end_clean());

CModule::IncludeModule('iblock');
CModule::IncludeModule('crm');

$USER->Authorize(1);

class sync_akada {
    
    
    //чтений csv файла с данными физ. лиц.
    public function FL_GetCSVData($file){
        if(file_exists($file)){
            $str = file_get_contents($file);
            $data = explode("\r\n",$str);
            //unset($date[0]);
            foreach($data as $line){
                $res[] = explode(';',$line);
            }
            if(count($res)){
                return $res;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
    
    //запись массива с данными ф.л. в файл
    public function FL_PutCSVData($data, $file){
        $text = implode(";",$data)."\r\n";
        file_put_contents($file, $text, FILE_APPEND);
    }
    
    
    //поиск контакта по фио+д.р.
    public function cont_FindByFIO_DR($F,$I,$O,$DR){
        $arSelectContact = array(
            'ID',
            'NAME',
            'SECOND_NAME',
            'LAST_NAME',
            'BIRTHDATE',
            AKADA_CONTACT_GUID
        );

        if($F && $I && $O && $DR){
            $arFilter = array(
                "LAST_NAME" => $F,
                "NAME" =>  $I,
                "SECOND_NAME" => $O,
                "BIRTHDATE" => $DR
                    );

            $objContactList = CCrmContact::GetList(array('DATE_CREATE' => 'ASC'), $arFilter, $arSelectContact);
            if($cnt = $objContactList->Fetch()){
              
                return $cnt;
            }
            else{
                return false;
            }
        }     
        return false;
    }
    
    
    //поиск контакта по фио+д.р.
    public function cont_FindByPass($pass_ser,$pass_num){
        $arSelectContact = array(
            'ID',
            'NAME',
            'SECOND_NAME',
            'LAST_NAME',
            'BIRTHDATE',
            AKADA_CONTACT_GUID
        );
        if($pass_num){
            $arFilter = array(
                PROP_CONTACT_PASSPORT_NUMBER => $pass_num,
                PROP_CONTACT_PASSPORT_SERIA =>  $pass_ser);


            $objContactList = CCrmContact::GetList(array('DATE_CREATE' => 'ASC'), $arFilter, $arSelectContact);
            if($cnt = $objContactList->Fetch()){
                return $cnt;
            }
            else{
                return false;
            }
        }
        return false;
    }    
    
    
    //поиск контакта по AKADA GUID
    public function cont_FindByAGUID($guid){
        $arSelectContact = array(
            'ID',
            'NAME',
            'SECOND_NAME',
            'LAST_NAME',
            'BIRTHDATE'
        );
        if($guid){
            $arFilter = array(AKADA_CONTACT_GUID => $guid);

            $objContactList = CCrmContact::GetList(array('DATE_CREATE' => 'ASC'), $arFilter, $arSelectContact);
            if($cnt = $objContactList->Fetch()){
                return $cnt;
            }
            else{
                return false;
            }
        }
        return false;
    }    
    
    
    //запись в контакт (апдейт) данных контакта
    public function SaveContactInfo($id,$data = array()){
        if(intval($id) < 1 || !count($data)) return false;
        $contact = new CCrmContact;
        $contact->Update($id , $data, false, false);        
    }


    //обработка массива данных из акады по физ лицам
    public function SyncFLFromCSV(&$data){
        if(count($data) > 1){
            //unset($data[0]);
            $i = 0;
            unlink($_SERVER["DOCUMENT_ROOT"]."/akada/fl_put.csv");
            foreach($data as &$rec){
                $s = false;
                $i++;
                ///if($i > 15) break;
                if($i > 1){
                    $arSelectContact = array(
                        'ID',
                        'NAME',

                        'SECOND_NAME',
                        'LAST_NAME',
                        'BIRTHDATE'
                    );

                    //Ищем по GUID
                    if($rec[0] && !$s && false){
                        echo $i.'. Ищем по GUID '.$rec[0];
                        $cntc = sync_akada::cont_FindByAGUID($rec[0]);
                        if($cntc){
                            echo '. Нашли.'.$cntc['ID']."\r\n";
                            $rec[1] = $cntc['ID'];
                            $s = true;
                        }
                        else{
                            echo '. Не нашли.'."\r\n";
                        }
                    }
                   
                    //Ищем по паспорту
                    if($rec[10] && !$s){
                        echo $i.'. Ищем по паспорту '.$rec[9].' '.$rec[10];
                        $cntc = sync_akada::cont_FindByPass($rec[9],$rec[10]);
                        if($cntc){

                            echo '. Нашли.'.$cntc['ID']."\r\n";
                            
                            if($cntc[AKADA_CONTACT_GUID] == '' || $cntc[AKADA_CONTACT_GUID]  == $rec[0]){
                                $rec[1] = $cntc['ID'];
                                if($cntc[AKADA_CONTACT_GUID] != $rec[0]){
                                    sync_akada::SaveContactInfo($cntc['ID'],array(AKADA_CONTACT_GUID => $rec[0]));
                                }
                                $s = true;
                            }
                        }
                        else{
                            echo '. Не нашли.'."\r\n";
                        }
                    }
                    //Ищем по ФИО + д.р.
                    if($rec[2] && $rec[3] && $rec[4] && $rec[5] && !$s){
                        echo $i.'. Ищем по ФИО и др '.$rec[2].' '.$rec[3].' '.$rec[4].'('.$rec[5].')';
                        $cntc = sync_akada::cont_FindByFIO_DR($rec[2],$rec[3],$rec[4],$rec[5]);
                        if($cntc){
                            echo '. Нашли.'.$cntc['ID']."\r\n";
                            
                            if($cntc[AKADA_CONTACT_GUID] == '' || $cntc[AKADA_CONTACT_GUID]  == $rec[0]){
                                $rec[1] = $cntc['ID'];
                                if($cntc[AKADA_CONTACT_GUID] != $rec[0]){
                                    sync_akada::SaveContactInfo($cntc['ID'],array(AKADA_CONTACT_GUID => $rec[0]));
                                }
                                $s = true;
                            }
                        }
                        else{
                            echo '. Не нашли.'."\r\n";
                        }
                    }
                }
                sync_akada::FL_PutCSVData($rec, $_SERVER["DOCUMENT_ROOT"]."/akada/fl_put.csv");
                
            }
        }
        else{
            return false;
        }
    }
    
}

$data = sync_akada::FL_GetCSVData($_SERVER["DOCUMENT_ROOT"]."/akada/fl.csv");

if(count($data) > 1){
    sync_akada::SyncFLFromCSV($data);
    
    
}
/*
 
    [0] => Array
        (
            [0] => GUID 1S
            [1] => GUID Bitrix
            [2] => Фамилия
            [3] => Имя
            [4] => Отчество
            [5] => Дата рождения
            [6] => GUID Гражданства
            [7] => Code пола
            [8] => GUID Вида документа
            [9] => Серия
            [10] => Номер
            [11] => Дата выдачи
            [12] => Кем выдан
            [13] => Код подразделения
        )

    [1] => Array
        (
            [0] => 28b53870-c270-11e5-ad50-9c8e99fb2ee2
            [1] => 
            [2] => Дьячкова
            [3] => Юлия
            [4] => Семеновна
            [5] => 13.11.1987
            [6] => f97ad326-0eba-11db-a554-00025b00afb6
            [7] => 1
            [8] => a69b8827-04ed-11db-83da-0011b107a2de
            [9] => 4511
            [10] => 193007
            [11] => 14.06.2011
            [12] => Отделом УФМС России по гор.Москве по району Теплый Стан
            [13] => 770-122
        )
 
 */

function trace($var){
    echo '<pre>';
    echo var_export($var,true);
    echo '</pre>';
}



