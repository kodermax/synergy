<?php

global $start_time;
$start_time = time(); 

ini_set("memory_limit","1024M");
set_time_limit(86400);
define("NEED_AUTH", false);
define("NOT_CHECK_PERMISSIONS", true);
error_reporting(E_ALL ^E_NOTICE);
$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
require($_SERVER["DOCUMENT_ROOT"]."/crm/deal/def.ini.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
while(@ob_end_clean());    

slog('Запуск');

$url = "http://sok-devapp/ws/bitrixcrm.1cws?wsdl";
    
    $login = "iis1";
    $pas = "iis";
    $writeGo = false;
    $writeGo = true;
    $arParamsSoap = Array(
	"login" => $login,
	"password" => $pas
    ); 
    
    $client = new SoapClient($url, $arParamsSoap);
     
    
    CModule::IncludeModule("iblock");
    CModule::IncludeModule("catalog");
    $el = new CIBlockElement;
    
    
    $arIblock = Array(
        		/*IBLOCK_ID_CODEFORMOFLEARNING,*/ 70, //Форма обучения code=FormOfLearning
                        /*IBLOCK_ID_CODELEVEL,*/ 71, //Уровень образования code=Level
                        /*IBLOCK_ID_CLASSES,*/ 82, //классы сode=Classes
                        /*IBLOCK_ID_CODESUBLEVEL,*/ 74, //Подуровень образования code=SubLevel
                        73=>/*IBLOCK_ID_SEMESTR*/ 73, //Семестр code=Semester
                    );

    $arFilter = Array("IBLOCK_ID" => $arIblock);
    $arSelect = Array("ID", "NAME", "PROPERTY_ID_1C", "IBLOCK_ID");
    $objMatrixPropsCode = CIBlockElement::GetList(false, $arFilter, false, $arNav, $arSelect);
    while($arMatrixPropsCode = $objMatrixPropsCode->Fetch()){
	$arResult["MATRIX_PROPS_CODE"][$arMatrixPropsCode["IBLOCK_ID"]][$arMatrixPropsCode["ID"]] = $arMatrixPropsCode;   
    }
    slog('Считали в массив первую группу иблоков матрицы');
    
    unset($arIblock[73]);
    $sql = 'UPDATE b_iblock_element SET ACTIVE="N" WHERE IBLOCK_ID IN ('.implode(',',$arIblock).')';
    $DB->Query($sql);    
    slog('Деактивировали элементы первой группы иблоков матрицы');
 

    
    
    
    $arIblock = Array(
                        /*IBLOCK_ID_SPECIALITY,*/ 62, //Специальности code=Specialitys
                        /*IBLOCK_ID_SPECIALIZATIONS,*/ 63, //Специализации code=Specializations
                        /*IBLOCK_ID_COURSE,*/ 69, //курсы code=Courses
                        /*IBLOCK_ID_DURATION,*/ 66, //Срок обучения code=Durations
                        /*IBLOCK_ID_FILIAL,*/ 60, //Филиалы code=Filials
                        /*IBLOCK_ID_FACULTET,*/ 65, //Факультеты code=Faculties
                        /*IBLOCK_ID_TRAINING_PROGRAMS,*/ 61, //Программы обучения code=TrainingPrograms
                        /*IBLOCK_ID_TYPE_OF_PROGRAMS,*/ 67, //Виды программ code=TypesOfPrograms
                        /*IBLOCK_ID_UNIT,*/ 72, //Подразделения code=Units
                        /*IBLOCK_ID_ADDITIONAL_CATEGORY,*/ 64, //Дополнительная Категория code=AdditionalCategories
                        /*IBLOCK_ID_ACADEMIC_YEARS*/ 68 //Учебный год code=AcademicYears
                    );            
    $arFilter = Array("IBLOCK_ID" => $arIblock);
    $arSelect = Array("ID", "NAME", "PROPERTY_GUID", "IBLOCK_ID");
    $objMatrixPropsGuid = CIBlockElement::GetList(false, $arFilter, false, $arNav, $arSelect);
    while($arrMatrixPropsGuid = $objMatrixPropsGuid->Fetch()){
	$arResult["MATRIX_PROPS_GUID"][$arrMatrixPropsGuid["IBLOCK_ID"]][$arrMatrixPropsGuid["ID"]] = $arrMatrixPropsGuid;
    }
    slog('Считали в массив вторую группу иблоков матрицы');
    $sql = 'UPDATE b_iblock_element SET ACTIVE="N" WHERE IBLOCK_ID IN ('.implode(',',$arIblock).')';
    $DB->Query($sql);
    slog('Деактивировали элементы второй группы иблоков матрицы');

    
    slog('                             ');
    
    if($writeGo == true){
	$arMatrixPropsCode[/*IBLOCK_ID_CODEFORMOFLEARNING*/70] = $client->GetFormsOfLearning();
	$arMatrixPropsCode[/*IBLOCK_ID_CODEFORMOFLEARNING*/70] = (array)$arMatrixPropsCode[/*IBLOCK_ID_CODEFORMOFLEARNING*/70];
	$arMatrixPropsCode[/*IBLOCK_ID_CODEFORMOFLEARNING*/70] = (array)$arMatrixPropsCode[/*IBLOCK_ID_CODEFORMOFLEARNING*/70]["return"];

	$arMatrixPropsCode[/*IBLOCK_ID_CODELEVEL*/71] = $client->GetLevels();
	$arMatrixPropsCode[/*IBLOCK_ID_CODELEVEL*/71] = (array)$arMatrixPropsCode[/*IBLOCK_ID_CODELEVEL*/71];
	$arMatrixPropsCode[/*IBLOCK_ID_CODELEVEL*/71] = (array)$arMatrixPropsCode[IBLOCK_ID_CODELEVEL]["return"];

	$arMatrixPropsCode[/*IBLOCK_ID_CODESUBLEVEL*/74] = $client->GetSubLevels();
	$arMatrixPropsCode[/*IBLOCK_ID_CODESUBLEVEL*/74] = (array)$arMatrixPropsCode[/*IBLOCK_ID_CODESUBLEVEL*/74];
	$arMatrixPropsCode[/*IBLOCK_ID_CODESUBLEVEL*/74] = (array)$arMatrixPropsCode[/*IBLOCK_ID_CODESUBLEVEL*/74]["return"];

	$arMatrixPropsCode[/*IBLOCK_ID_CLASSES*/82] = $client->GetClasses();
	$arMatrixPropsCode[/*IBLOCK_ID_CLASSES*/82] = (array)$arMatrixPropsCode[/*IBLOCK_ID_CLASSES*/82];
	$arMatrixPropsCode[/*IBLOCK_ID_CLASSES*/82] = (array)$arMatrixPropsCode[/*IBLOCK_ID_CLASSES*/82]["return"];
        slog('Запросили данные по первой группе из Акады');

        foreach($arMatrixPropsCode as $keyIblock => $arProperties){
            foreach($arProperties["Data"] as $arProps){
    		$hasThisForm = false;
		$arProps = (array)$arProps;
                foreach($arResult["MATRIX_PROPS_CODE"][$keyIblock] as $id =>$val){
                    if($arProps["ID"] == $val["PROPERTY_ID_1C_VALUE"]){
                        $hasThisForm = $id;
                    }
                }

                $arElement = Array(
                        "ACTIVE" => 'Y',
                        "IBLOCK_ID" => $keyIblock,
                        "NAME" => $arProps["Name"],
                        "PROPERTY_VALUES" => Array(
                        "ID_1C" => $arProps["ID"]
                        )
                );

    		if($hasThisForm == false){
                    $el->Add($arElement, false, false);
                }
                else{
                    $el->Update($hasThisForm,$arElement);
                }                
            }
	}
        slog('Обновили/добавили данные первой группы');
        
        
	$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALITY*/62] = $client->GetSpecialitys();
	$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALITY*/62] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALITY*/62];
	$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALITY*/62] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALITY*/62]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALIZATIONS*/63] = $client->GetSpecializations();
	$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALIZATIONS*/63] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALIZATIONS*/63];
	$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALIZATIONS*/63] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_SPECIALIZATIONS*/63]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_COURSE*/69] = $client->GetCourses();
	$arMatrixPropsGuid[/*IBLOCK_ID_COURSE*/69] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_COURSE*/69];
	$arMatrixPropsGuid[/*IBLOCK_ID_COURSE*/69] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_COURSE*/69]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_DURATION*/66] = $client->GetDurations();
	$arMatrixPropsGuid[/*IBLOCK_ID_DURATION*/66] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_DURATION*/66];
	$arMatrixPropsGuid[/*IBLOCK_ID_DURATION*/66] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_DURATION*/66]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_FILIAL*/60] = $client->GetFilials();
	$arMatrixPropsGuid[/*IBLOCK_ID_FILIAL*/60] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_FILIAL*/60];
	$arMatrixPropsGuid[/*IBLOCK_ID_FILIAL*/60] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_FILIAL*/60]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_FACULTET*/65] = $client->GetFaculties();
	$arMatrixPropsGuid[/*IBLOCK_ID_FACULTET*/65] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_FACULTET*/65];
	$arMatrixPropsGuid[/*IBLOCK_ID_FACULTET*/65] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_FACULTET*/65]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_TRAINING_PROGRAMS*/61] = $client->GetTrainingPrograms();
	$arMatrixPropsGuid[/*IBLOCK_ID_TRAINING_PROGRAMS*/61] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_TRAINING_PROGRAMS*/61];
	$arMatrixPropsGuid[/*IBLOCK_ID_TRAINING_PROGRAMS*/61] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_TRAINING_PROGRAMS*/61]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_TYPE_OF_PROGRAMS*/67] = $client->GetTypesOfPrograms();
	$arMatrixPropsGuid[/*IBLOCK_ID_TYPE_OF_PROGRAMS*/67] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_TYPE_OF_PROGRAMS*/67];
	$arMatrixPropsGuid[/*IBLOCK_ID_TYPE_OF_PROGRAMS*/67] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_TYPE_OF_PROGRAMS*/67]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_UNIT*/72] = $client->GetUnits();
	$arMatrixPropsGuid[/*IBLOCK_ID_UNIT*/72] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_UNIT*/72];
	$arMatrixPropsGuid[/*IBLOCK_ID_UNIT*/72] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_UNIT*/72]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_ADDITIONAL_CATEGORY*/64] = $client->GetAdditionalCategories();
	$arMatrixPropsGuid[/*IBLOCK_ID_ADDITIONAL_CATEGORY*/64] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_ADDITIONAL_CATEGORY*/64];
	$arMatrixPropsGuid[/*IBLOCK_ID_ADDITIONAL_CATEGORY*/64] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_ADDITIONAL_CATEGORY*/64]["return"];

	$arMatrixPropsGuid[/*IBLOCK_ID_ACADEMIC_YEARS*/68] = $client->GetAcademicYears();
	$arMatrixPropsGuid[/*IBLOCK_ID_ACADEMIC_YEARS*/68] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_ACADEMIC_YEARS*/68];
	$arMatrixPropsGuid[/*IBLOCK_ID_ACADEMIC_YEARS*/68] = (array)$arMatrixPropsGuid[/*IBLOCK_ID_ACADEMIC_YEARS*/68]["return"];
        slog('Запросили данные по второй группе из Акады');
        
        
        foreach($arMatrixPropsGuid as $keyIblock => $arProperties){
            foreach($arProperties["Data"] as $arProps){
    		$hasThisForm = false;
		$arProps = (array)$arProps;
                foreach($arResult["MATRIX_PROPS_GUID"][$keyIblock] as $id => $val){
                    if($arProps["GUID"] == $val["PROPERTY_GUID_VALUE"]){
			$hasThisForm = $id;
                    }
		}
                
                $arElement = Array(
                    "IBLOCK_ID" => $keyIblock,
                    "ACTIVE" => 'Y',
                    "NAME" => $arProps["Name"],
                    "PROPERTY_VALUES" => Array(
                        "GUID" => $arProps["GUID"],
                        "Name1c" => $arProps["Name"]
                    )
                );
                
                if($keyIblock == 66){
                    $arElement["PROPERTY_VALUES"]['CountSemesters'] = $arProps["CountSemesters"];
                    //$arElement["NAME"] = $arProps["CountSemesters"];
                }
                
    		if($hasThisForm == false){
                    $el->Add($arElement, false, false);
                }
                else{
                    $el->Update($hasThisForm,$arElement);
                }
            }
        }
        slog('Обновили/добавили данные второй группы');
        slog('                             ');
        
        
        
        $arSelect = Array("ID", "XML_ID");
        $arFilter = Array("IBLOCK_ID"=>MAIN_CATALOG_ID, "IBLOCK_SECTION_ID" => IBLOCK_SECTION_ID_UNIVER);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->Fetch()){
            $arCNP[$ob['XML_ID']] = $ob['ID'];
        }
        slog('Считали каталог');
        $sql = 'UPDATE b_iblock_element SET ACTIVE="N" WHERE IBLOCK_ID='.MAIN_CATALOG_ID.' AND IBLOCK_SECTION_ID='.IBLOCK_SECTION_ID_UNIVER;
        $DB->Query($sql);
        slog('Деактивировали записи каталога');
        
      
	$cnp = $client->GetCNP();
	$cnp = (array)$cnp;
	$cnp = (array)$cnp["return"];
	foreach($cnp["Data"] as $k => $arProduct){
            
    		$arProduct = (array)$arProduct;
		$arParams[$k]["PROPERTY_VALUES"] = Array();
                $arProduct_h = $arProduct;
                unset($arProduct_h["Price"]);
                $hash = md5(implode('.',$arProduct_h));
                
                
		if($arProduct["GUIDSpeciality"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_SPECIALITY*/62] as $arPropGuid){
	            	if($arProduct["GUIDSpeciality"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["SPECIALITY"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDAcademicYear"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_ACADEMIC_YEARS*/68] as $arPropGuid){
                        if($arProduct["GUIDAcademicYear"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["ACADEMIC_YEAR"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDAdditionalCategory"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_ADDITIONAL_CATEGORY*/64] as $arPropGuid){
	            	if($arProduct["GUIDAdditionalCategory"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["ADDITIONAL_CATEGORY"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDCourses"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_COURSE*/69] as $arPropGuid){
                        if($arProduct["GUIDCourses"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["COURSES"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDDuration"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_DURATION*/66] as $arPropGuid){
                        if($arProduct["GUIDDuration"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["DURATION"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDFaculty"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_FACULTET*/65] as $arPropGuid){
	            	if($arProduct["GUIDFaculty"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["FACULTY"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDFilial"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_FILIAL*/60] as $arPropGuid){
	            	if($arProduct["GUIDFilial"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["FILIAL"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDSpeciality"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_SPECIALITY*/62] as $arPropGuid){
	            	if($arProduct["GUIDSpeciality"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["SPECIALITY"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDSpecialization"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_SPECIALIZATIONS*/63] as $arPropGuid){
	            	if($arProduct["GUIDSpecialization"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["SPECIALIZATION"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDTrainingProgram"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_TRAINING_PROGRAMS*/61] as $arPropGuid){
                        if($arProduct["GUIDTrainingProgram"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["TRAININGPROGRAM"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDTypesOfPrograms"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_TYPE_OF_PROGRAMS*/67] as $arPropGuid){
                        if($arProduct["GUIDTypesOfPrograms"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["TYPESOFPROGRAMS"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["GUIDUnit"]){
                    foreach($arResult["MATRIX_PROPS_GUID"][/*IBLOCK_ID_UNIT*/72] as $arPropGuid){
	            	if($arProduct["GUIDUnit"] == $arPropGuid["PROPERTY_GUID_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["UNIT"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["CodeClass"]){
                    foreach($arResult["MATRIX_PROPS_CODE"][/*IBLOCK_ID_CLASSES*/82] as $arPropGuid){
	            	if($arProduct["CodeClass"] == $arPropGuid["PROPERTY_ID_1C_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["CLASS"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["CodeFormOfLearning"]){
                    foreach($arResult["MATRIX_PROPS_CODE"][/*IBLOCK_ID_CODEFORMOFLEARNING*/70] as $arPropGuid){
	            	if($arProduct["CodeFormOfLearning"] == $arPropGuid["PROPERTY_ID_1C_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["FORMOFLEARNING"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["CodeLevel"]){
                    foreach($arResult["MATRIX_PROPS_CODE"][/*IBLOCK_ID_CODELEVEL*/71] as $arPropGuid){
                        if($arProduct["CodeLevel"] == $arPropGuid["PROPERTY_ID_1C_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["LEVEL"] = $arPropGuid["ID"];
                        }
                    }
		}
		if($arProduct["CodeSubLevel"]){
                    foreach($arResult["MATRIX_PROPS_CODE"][/*IBLOCK_ID_CODESUBLEVEL*/74] as $arPropGuid){
	            	if($arProduct["CodeSubLevel"] == $arPropGuid["PROPERTY_ID_1C_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["SUBLEVEL"] = $arPropGuid["ID"];
	            	}
                    }
		}
		if($arProduct["Semester"]){
                    foreach($arResult["MATRIX_PROPS_CODE"][/*IBLOCK_ID_SEMESTR*/73] as $arPropGuid){
	            	if($arProduct["Semester"] == $arPropGuid["PROPERTY_ID_1C_VALUE"]){
                            $arParams[$k]["PROPERTY_VALUES"]["SEMESTR"] = $arPropGuid["ID"];
	            	}
                    }
		}
		$arParams[$k]["PROPERTY_VALUES"]["Plan"] = $arProduct["Plan"];
		$arParams[$k]["PROPERTY_VALUES"]["Price"] = $arProduct["Price"];
		$arParams[$k]["PROPERTY_VALUES"]["Budget"] = $arProduct["Budget"];
		$arParams[$k]["IBLOCK_ID"] = MAIN_CATALOG_ID;
		$arParams[$k]["IBLOCK_SECTION_ID"] = IBLOCK_SECTION_ID_UNIVER;
		$arParams[$k]["ACTIVE"] = "Y";
		$arParams[$k]["NAME"] = "Программа ".($k+100001);
		$arParams[$k]["XML_ID"] = $hash;
                
                if($arCNP[$arParams[$k]["XML_ID"]] > 0){
                    $productId = $arCNP[$arParams[$k]["XML_ID"]];
                    $el->update($arCNP[$arParams[$k]["XML_ID"]],$arParams[$k]);
                    slog('Обновили запись каталога "'.$arParams[$k]["NAME"].'" ('.$hash.')');
                }
                else{
                    $productId = $el->Add($arParams[$k], false, false);
                    slog('Добавили запись каталога "'.$arParams[$k]["NAME"].'" ('.$hash.')');
                }
		CPrice::SetBasePrice($productId, $arProduct["Price"], "RUB");   

        }
        slog('Обновили/добавили записи каталога');
    }   
 /*
  *                     [0] => stdClass Object
                        (
                            [GUIDSpeciality] => 4b3b7667-330a-11e2-812e-9c8e99fb2ee6
                            [GUIDTrainingProgram] => 241152c9-04e8-11db-86d0-00e018f5a60d
                            [GUIDFilial] => 48904a01-e1e9-11e1-b76b-9c8e99fb2ee6
                            [CodeClass] => 1
                            [GUIDSpecialization] => 00000000-0000-0000-0000-000000000000
                            [CodeLevel] => 1
                            [CodeSubLevel] => 1
                            [GUIDDuration] => 894dd4f9-04f6-11db-83da-0011b107a2de
                            [GUIDAdditionalCategory] => 00000000-0000-0000-0000-000000000000
                            [GUIDAcademicYear] => 27187b45-6798-11dd-ade7-00304863c48b
                            [Semester] => 1
                            [GUIDFaculty] => 00000000-0000-0000-0000-000000000000
                            [GUIDTypesOfPrograms] => 00000000-0000-0000-0000-000000000000
                            [Budget] => 2
                            [GUIDUnit] => 449f4ef8-e1e8-11e1-b76b-9c8e99fb2ee6
                            [GUIDCourses] => 80522625-acdd-44b2-814a-d0dc07ae5631
                            [CodeFormOfLearning] => 3
                            [Price] => 39500
                            [Plan] => 15
                        )
  */
function slog($msg){
    global $start_time;
    @mkdir($_SERVER["DOCUMENT_ROOT"].'/akada/logs');
    @mkdir($_SERVER["DOCUMENT_ROOT"].'/akada/logs/sync_catalog');
    echo date('Y.m.d H:i:s#  ')."".$msg."\r\n";
    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/akada/logs/sync_catalog/'.date('Y_m_d_H_i_s',$start_time).'.log', date('Y.m.d H:i:s#  ')."".$msg."\r\n", FILE_APPEND);
}