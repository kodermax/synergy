<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

if ($USER->isAdmin()) {
    $arFiles = ShowDMSexchangeLogs();
    echo "<b>Логи импорта ВСЕХ отсутствий из DMS (окончательный импорт):</b><br>";
    foreach ($arFiles['IMPORTFULL'] as $filename) {
        echo "<a href='/local/modules/intranet/tools/logs/" . basename($filename) . "'>" . basename($filename) . "</a><br>";
    }

    echo "<br><b>Логи импорта ВСЕХ отсутствий из DMS (промежуточный импорт):</b><br>";
    foreach ($arFiles['IMPORTINTERM'] as $filename) {
        echo "<a href='/local/modules/intranet/tools/logs/" . basename($filename) . "'>" . basename($filename) . "</a><br>";
    }

    echo "<br><b>Логи импорта ОБНОВЛЕННЫХ записей из DMS:</b><br>";
    foreach ($arFiles['IMPORT_UPD'] as $filename) {
        echo "<a href='/local/modules/intranet/tools/logs/" . basename($filename) . "'>" . basename($filename) . "</a><br>";
    }

    echo "<br><b>Логи импорта фактически отработанного сотрудниками времени за определенную дату:</b><br>";
    foreach ($arFiles['WORKTIME'] as $filename) {
        echo "<a href='/local/modules/intranet/tools/logs/" . basename($filename) . "'>" . basename($filename) . "</a><br>";
    }

    echo "<br><b>Логи экспорта SaltoID по ВСЕМ пользователям в AD (вообще это механизм разовый):</b><br>";
    foreach ($arFiles['IDTOADALL'] as $filename) {
        echo "<a href='/local/modules/intranet/tools/logs/" . basename($filename) . "'>" . basename($filename) . "</a><br>";
    }

    echo "<br><b>Логи ЭКСПОРТА отдельных записей из битрикс в DMS (когда запись обновляется в BX и сразу уходит в DMS):</b><br>";
    foreach ($arFiles['UPDDMS'] as $filename) {
        echo "<a href='/local/modules/intranet/tools/logs/" . basename($filename) . "'>" . basename($filename) . "</a><br>";
    }

    echo "<br><b>Логи экспорта SaltoID в AD при редактировании отдельной записи (настройки пользователя):</b><br>";
    foreach ($arFiles['IDTOAD'] as $filename) {
        echo "<a href='/local/modules/intranet/tools/logs/" . basename($filename) . "'>" . basename($filename) . "</a><br>";
    }

    echo "<br><b>Логи экспорта нестандартного рабочего графика отдельного сотрудника в DMS:</b><br>";
    foreach ($arFiles['SCHEDULE'] as $filename) {
        echo "<a href='/local/modules/intranet/tools/logs/" . basename($filename) . "'>" . basename($filename) . "</a><br>";
    }
} else {
    echo "<b>У вас нет прав на просмотр лог-файлов...</b><br>";
}

//$NotifyRes = SendAbsenceIMMessage("Необходимо <a href='/company/absence.php'>добавить комментарий и подтвердить факт нарушения</a>.",
//    "Зафиксировано нарушение трудовой дисциплины", 328347);
//print_r($NotifyRes);
//$NotifyRes = AddAbsenceToLog("Необходимо добавить комментарий и подтвердить факт нарушения. Для этого пройдите по ссылке: <a href='/company/absence.php'>контроль трудовой дисциплины</a>",
//    "Добавлена запись о нарушении трудовой дисциплины", 328347);
//print_r($NotifyRes);

//$tmp1 = GetUserAssistant(811);
//echo "Заместитель: ";
//print_r($tmp1['ID']);
//
//$tmp2 = GetAbsenceNotifyRecipients(1768);
//echo "Получатели сообщения: ";
//print_r($tmp2);
