<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/duration.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/mssql_connect.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/defines.php");

global $APPLICATION, $USER;

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

CJSCore::Init('jquery');
$APPLICATION->AddHeadScript("/local/assets/jquery.maskedinput.min.js", true);

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y 00:00:00");
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y 23:59:59");
$arResult["DFROM"]=(duration2sec($_REQUEST["dfrom"]))?$_REQUEST["dfrom"]:"00:00:00:00";
$arResult["DTO"]=(duration2sec($_REQUEST["dto"]))?$_REQUEST["dto"]:"01:00:00:00";

$arResult["DFROMS"]=duration2sec($arResult["DFROM"]);
$arResult["DTOS"]=duration2sec($arResult["DTO"]);

$arResult["DFROMD"]=($arResult["DFROMS"]==0)?0:$arResult["DFROMS"]/86400;
$arResult["DTOD"]=($arResult["DTOS"]==0)?0:$arResult["DTOS"]/86400;

$arResult["ABON_TYPE_LIST"]=[
    0=>"Неизвестно",
    1=>"Внутренний",
    2=>"Внешний"
];


$arResult["CONNECT_DIR_LIST"]=[
      0 => "Неизвестно",
      1 => "Входящий",
      2 => "Исходящий",
      3 => "Внутренний",
      4 => "Внешний"
];

$arResult["GROUP_LIST"]=[];

$sql="SELECT dbo.tGroups.ID as GId,
	dbo.tGroups.Name as GName,
	dbo.tUsers.Login,
	dbo.tUsers.ID,
	dbo.tUsers.Name_F,
	dbo.tUsers.Name_I,
	dbo.tUsers.Deleted,
	dbo.tUsers.IsSystem
FROM dbo.tUsers LEFT JOIN dbo.tGroupsLinks ON dbo.tUsers.ID = dbo.tGroupsLinks.IDUser
	 LEFT JOIN dbo.tGroups ON dbo.tGroupsLinks.IDGroup = dbo.tGroups.ID
WHERE dbo.tUsers.IsSystem = 0 AND dbo.tUsers.Deleted = 0
ORDER BY dbo.tGroups.Name ASC, dbo.tUsers.Name_F ASC, dbo.tUsers.Name_I ASC";

$res = mssql_query($sql);
while ($row = mssql_fetch_assoc($res)) {
    if($row["GId"]!="")
    {
        $arResult["GROUP_LIST"][$row["GId"]]["NAME"]=$row["GName"];
        $arResult["GROUP_LIST"][$row["GId"]]["USERS"][$row["ID"]]=$row["Name_F"]." ".$row["Name_I"];
    }

    if(trim($row["Name_F"])!="" && trim($row["Name_I"])!="")
    {
        $arResult["GROUP_USER_LIST"][$row["ID"]]=[
            "NAME"=>$row["Name_F"]." ".$row["Name_I"],
            "ID"=>$row["ID"]
        ];
    }


    $arResult["USER_LIST"][$row["ID"]]=$row["Name_F"]." ".$row["Name_I"];
}

usort ($arResult["GROUP_USER_LIST"], function($a, $b) {
    return strcmp($a["NAME"], $b["NAME"]);
});

$arResult["OPERATOR"]=(is_array($_REQUEST["operator"]))?$_REQUEST["operator"]:[];

$arResult["ABON"]=(intval($_REQUEST["abon"])>0)?$_REQUEST["abon"]:"";

$arResult["LIMIT"]=100;
$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PAGE"]=($_REQUEST["page"])?$_REQUEST["page"]:1;

if($arResult["MODE"] && !$arResult["ERROR"])
{
    $start=time();
    $from=preg_replace("/^(\d\d)\.(\d\d)\./","$2/$1/",$arResult["FROM"]);
    $to=preg_replace("/^(\d\d)\.(\d\d)\./","$2/$1/",$arResult["TO"]);

    $listID=join(" ",$arResult["OPERATOR"]);

    $listID=(trim($listID)!="")?"'".$listID."'":"NULL";
    $ABON=(trim($arResult["ABON"])!="")?"'".$arResult["ABON"]."'":"NULL";


    $sql="SELECT [WHAT] FROM
          report_all_connections('{$USER->GetLogin()}','{$from}','{$to}',{$arResult["DFROMD"]},{$arResult["DTOD"]},{$ABON},{$listID})";

    $sql_count=str_replace("[WHAT]","count(*) as cnt",$sql);

    //AddMessage2Log($sql_count);

    $res=mssql_query($sql_count);
    if($row=mssql_fetch_assoc($res))
    {
        $arResult["ROWCOUNT"]=$row["cnt"];
    }

    $sql=str_replace("[WHAT]","*",$sql);
    $sql.=" ORDER BY TimeStart DESC";
    if($arResult["PAGE"]>0)
    {
        $sql.=" OFFSET ".(($arResult["PAGE"]-1)*$arResult["LIMIT"])." ROWS FETCH NEXT ".$arResult["LIMIT"]." ROWS ONLY";
    }

    //AddMessage2Log($sql);

    $res=mssql_query($sql);
    if (!$res) {
        $arResult["ERROR"][]=mssql_get_last_message();
    }
    else
    {
        while(($row=mssql_fetch_assoc($res)))
        {
            $row["TimeStart"]=preg_replace("/^(\d{4})-(\d{2})-(\d{2})/","$3.$2.$1",preg_replace("/\..*/","",$row["TimeStart"]));
            $row["DurationFull"]=day2duration($row["DurationFull"]);
            $row["DurationWait"]=day2duration($row["DurationWait"]);
            $row["DurationTalk"]=day2duration($row["DurationTalk"]);
            $row["ConnectionDirection"]=$arResult["CONNECT_DIR_LIST"][$row["ConnectionDirection"]];
            if($arResult["USER_LIST"][$row["AIDUser"]])
                $row["AIDUser"]=$arResult["USER_LIST"][$row["AIDUser"]];
            elseif($arResult["GROUP_LIST"][$row["AIDUser"]])
                $row["AIDUser"]=$arResult["GROUP_LIST"]["NAME"];
            else
                $row["AIDUser"]=(!empty($row["AIDUser"]))?"Неизвестно ({$row["AIDUser"]})":"";

            if($arResult["USER_LIST"][$row["BIDUser"]])
                $row["BIDUser"]=$arResult["USER_LIST"][$row["BIDUser"]];
            elseif($arResult["GROUP_LIST"][$row["BIDUser"]])
                $row["BIDUser"]=$arResult["GROUP_LIST"]["NAME"];
            else
                $row["BIDUser"]=(!empty($row["BIDUser"]))?"Неизвестно ({$row["BIDUser"]})":"";

            $row["AAbonentType"]=$arResult["ABON_TYPE_LIST"][$row["AAbonentType"]];
            $row["BAbonentType"]=$arResult["ABON_TYPE_LIST"][$row["BAbonentType"]];

            $arResult["RESULT"][]=$row;
        }
    }

    //AddMessage2Log(print_r($arResult, TRUE));
    $arResult["TIME"]=time()-$start;
}

$this->IncludeComponentTemplate();
?>