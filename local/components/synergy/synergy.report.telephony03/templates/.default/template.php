<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<script>
    var componentPath = "<?=$componentPath?>";
    $(document).ready(

        function()
        {

            $.mask.definitions['m']="[0-5]";
            $.mask.definitions['h']="[0-2]";

            $('#dfrom').mask('99:99:99:99',{placeholder:"00:00:00:00"});
            $('#dto').mask('99:99:99:99',{placeholder:"00:00:00:00"});
            $('#number').mask('?99999999999999',{placeholder:" "});
            $('#abon').mask('?99999999999999',{placeholder:" "});

            $("a[data-name='pager']").click(
                function()
                {
                    $('#page').val($(this).data("key"));
                    $('#form1').submit();
                    return false;
                }
            );

            //$('#h_scroll').height($(window).height()-50);

		            var wdthAll = $(".table-result").width();
		            var wdth = $('#h_scroll_content').width();
		            if(wdthAll>wdth) {
			            var b_scroll = $('<div style="overflow-x:scroll;width:'+wdth+'px"/>');
			            var b_cont = $('<div style="height:1px"></div>').width(wdthAll);
			            b_scroll.append(b_cont);
			            b_scroll.scroll(function(){
				            $('#h_scroll').scrollLeft(b_scroll.scrollLeft());
			            });
			            $('#h_scroll').before(b_scroll);
			            $('#h_scroll').scroll(function(){
				            b_scroll.scrollLeft($('#h_scroll').scrollLeft());
			            });
		            }

		     $(window).resize(function()
		     {
               $('#h_scroll').height($(window).height()-50);

             });

            $(".a_audio").bind("play",
                function()
                {
                    $("#b"+$(this).data("key")).trigger("play");
                }
            );

            $(".a_audio").bind("pause",
                function()
                {
                    $("#b"+$(this).data("key")).trigger("pause");
                }
            );

            $(".a_audio").bind("volumechange",
                function()
                {
                    $("#b"+$(this).data("key")).prop("volume",$(this).prop("volume"));
                }
            );
            $(".a_audio").bind("seeked",
                function()
                {
                    $("#b"+$(this).data("key")).prop("currentTime",$(this).prop("currentTime"));
                }
            );
            $(".a_audio").bind("timeupdate",
                function()
                {
                    if($("#b"+$(this).data("key")).prop("muted")!=$(this).prop("muted"))
                        $("#b"+$(this).data("key")).prop("muted",$(this).prop("muted"));
                }
            );


        }

    );
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1" id="form1">
        <input type="hidden" name="mode" value="tbl">
        <input type="hidden" name="page" value="1" id="page">
        <div class="form-element">
            <label for="from">Период:</label>
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "Y",
     "HIDE_TIMEBAR" => "N"
    )
);?>

        </div>
        <div class="form-element">
            <label for="dfrom">Продолжительность:</label>
            <input id="dfrom" name="dfrom" type="text" value="<?=$arResult["DFROM"]?>" size="11" autocomplete="off"> -
            <input id="dto" name="dto" type="text" value="<?=$arResult["DTO"]?>" size="11" autocomplete="off">
        </div>
        <div class="form-element">
            <label for="bcount">Абонент:</label>
            <input id="abon" name="abon" type="text" value="<?=$arResult["ABON"]?>">
        </div>
        <div class="form-element">
            <label for='operator[]'>Оператор:</label>
            <select id='operator[]' name='operator[]' multiple size="20">
              <? foreach ($arResult["GROUP_LIST"] as $arGId=>$arGroup):?>
                  <? $sel=in_array($arGId,$arResult["OPERATOR"])?" selected":""; ?>
                  <option value="<?=$arGId?>" style="font-weight: bold; font-size: 12px;" <?=$sel?>><?=$arGroup["NAME"]?></option>
              <? endforeach;?>
              <? foreach ($arResult["GROUP_USER_LIST"] as $arUser):?>
                    <? $sel=in_array($arUser["ID"],$arResult["OPERATOR"])?" selected":""; ?>
                    <option value="<?=$arUser["ID"]?>"<?=$sel?>>&nbsp;&nbsp;&nbsp;<?=$arUser["NAME"]?></option>
                <? endforeach; ?>
            </select>
        </div>
        <br>
        <br>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Полная таблица разговоров</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="dfrom" type="hidden" value="<?=$arResult["DFROM"]?>">
        <input name="dto" type="hidden" value="<?=$arResult["DTO"]?>">
        <? foreach($arResult["OPERATOR"] as  $oper):?>
        <input name="operator[]" type="hidden" value="<?=$oper?>">
        <? endforeach;?>
        <input name="abon" type="hidden" value="<?=$arResult["ABON"]?>">
        <input name="page" type="hidden" value="-1">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <div>Всего найдено: <?=$arResult["ROWCOUNT"]?></div>
    <div>Страницы:
        <? for($pN=1;$pN<=ceil($arResult["ROWCOUNT"]/$arResult["LIMIT"]);$pN++):?>
        <? if($pN==$arResult["PAGE"]):?>
            <?=$pN?>
        <? else: ?>
            <a href="#" data-name="pager" data-key="<?=$pN?>"><?=$pN?></a>
        <? endif;?>
        <? endfor;?>
    </div>
    <div id="h_scroll"  style="overflow:scroll;">
	<div id="h_scroll_content">
    <? endif;?>
    <table class="table-result">
        <tr>
			<th rowspan="2">Дата и время</th>
			<th colspan="3">Продолжительность</th>
			<th>Состояние</th>
			<th colspan="3">Абонент А</th>
			<th colspan="3">Абонент Б</th>
            <? if($arResult["MODE"]!="exl"):?>
            <th rowspan="2">Запись разговора</th>
            <? endif;?>
		</tr>
		<tr>
		    <!-- Продолжительность -->
		    <th>Итого</th>
		    <th>Ожидание</th>
		    <th>Разговор</th>
		    <!-- Состояние -->
		    <th>Направление</th>
		    <!-- Абонент А -->
            <th>Номер</th>
            <th>Тип</th>
            <th>Оператор</th>
            <!-- Абонент Б -->
            <th>Номер</th>
            <th>Тип</th>
            <th>Оператор</th>
		</tr>
        <? $id=0;
        foreach($arResult["RESULT"] as $arItem):
            $id++;
        ?>
        <tr data-key="<?=$arItem["ID"]?>">
            <!-- Дата и время -->
            <td><?=$arItem["TimeStart"]?></td>
		    <!-- Продолжительность -->
		    <td><?=$arItem["DurationFull"]?></td>
		    <td><?=$arItem["DurationWait"]?></td>
		    <td><?=$arItem["DurationTalk"]?></td>
		    <!-- Состояние -->
		    <td><?=$arItem["ConnectionDirection"]?></td>
		    <!-- абонент А-->
            <td><?=$arItem["ANumber"]?></td>
            <td><?=$arItem["AAbonentType"]?></td>
            <td><?=$arItem["AFIO"]?></td>
		    <!-- абонент Б-->
            <td><?=$arItem["BNumber"]?></td>
            <td><?=$arItem["BAbonentType"]?></td>
            <td><?=$arItem["BFIO"]?></td>
            <? if($arResult["MODE"]!="exl"):?>
                <td>
                    <? if(!strpos($_SERVER['HTTP_USER_AGENT'],"MSIE")):?>
                        <? if(trim($arItem["AWavFile"])!="" && file_exists($_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_WAV.(str_replace("\\","/",$arItem["AWavFile"])))):?>
                            <audio id="a<?=$id?>" controls="" class="a_audio" data-key="<?=$id?>" src="<?=TELEPHONY_PATH_TO_WAV?><?= str_replace("\\","/",$arItem["AWavFile"]) ?>" >
                                HTML5 Audio is not supported
                            </audio>
                        <? endif;?>
                        <? if(trim($arItem["BWavFile"])!="" && file_exists($_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_WAV.(str_replace("\\","/",$arItem["BWavFile"])))):?>
                            <audio id="b<?=$id?>" class="b_audio" <? if(trim($arItem["AWavFile"])==""):?>controls<? endif;?> data-key="<?=$id?>" src="<?=TELEPHONY_PATH_TO_WAV?><?= str_replace("\\","/",$arItem["BWavFile"]) ?>">
                                HTML5 Audio is not supported
                            </audio>
                        <? endif;?>
                    <? else: ?>
                        Воспроизведение невозможно в Internet Explorer
                    <? endif;?>
                    <? if(trim($arItem["AWavFile"])!="" && file_exists($_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_WAV.(str_replace("\\","/",$arItem["AWavFile"])))
                        && trim($arItem["BWavFile"])!="" && file_exists($_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_WAV.(str_replace("\\","/",$arItem["BWavFile"])))
                    ):?>
                        <div style="font-size: 12px;width: auto; text-align: right;"><a href="/local/lib/telephony/dl.php?id=<?=$arItem["ID"]?>">Скачать</a></div>
                    <? endif;?>
                </td>
            <?endif;?>
        </tr>
        <?  //AddMessage2Log(print_r( $arItem , TRUE)); ?>
        <? endforeach;?>
	</table>
	<div>Время формирования отчета: <?=$arResult["TIME"]?>с</div>
	<? if($arResult["MODE"]!="exl"):?>
	</div>
	</div>
	<div>Страницы:
        <? for($pN=1;$pN<=ceil($arResult["ROWCOUNT"]/$arResult["LIMIT"]);$pN++):?>
        <? if($pN==$arResult["PAGE"]):?>
            <?=$pN?>
        <? else: ?>
            <a href="#" data-name="pager" data-key="<?=$pN?>"><?=$pN?></a>
        <? endif;?>
        <? endfor;?>
    </div>
    <? endif;?>
</div>
<? elseif($arResult["MODE"] && !$arResult["ERROR"]):?>
<div class="result">
<b>По Вашему запросу ничего не найдено</b>
</div>
<? else:?>
<div class="error">
<? foreach ($arResult["ERROR"] as $error):?>
<b><?=$error;?></b>
<? endforeach; ?>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>