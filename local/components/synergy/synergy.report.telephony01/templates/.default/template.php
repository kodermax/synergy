<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<script src="/bitrix/templates/bitrix24/js/audiojs/audio.min.js"></script>
<script>
    var componentPath = "<?=$componentPath?>";
    $(document).ready(

        function()
        {

            $.mask.definitions['m']="[0-5]";
            $.mask.definitions['h']="[0-2]";

            $('#dfrom').mask('99:99:99:99',{placeholder:"00:00:00:00"});
            $('#dto').mask('99:99:99:99',{placeholder:"00:00:00:00"});
            $('#number').mask('?99999999999999',{placeholder:" "});
            $('#abon').mask('?99999999999999',{placeholder:" "});

            $("a[data-name='pager']").click(
                function()
                {
                    $('#page').val($(this).data("key"));
                    $('#form1').submit();
                    return false;
                }
            );

            $('#h_scroll').height($(window).height()-50);

		            var wdthAll = $(".table-result").width();
		            var wdth = $('#h_scroll_content').width();
		            if(wdthAll>wdth) {
			            var b_scroll = $('<div style="overflow-x:scroll;width:'+wdth+'px"/>');
			            var b_cont = $('<div style="height:1px"></div>').width(wdthAll);
			            b_scroll.append(b_cont);
			            b_scroll.scroll(function(){
				            $('#h_scroll').scrollLeft(b_scroll.scrollLeft());
			            });
			            $('#h_scroll').before(b_scroll);
			            $('#h_scroll').scroll(function(){
				            b_scroll.scrollLeft($('#h_scroll').scrollLeft());
			            });
		            }

		     $(window).resize(function()
		     {
               $('#h_scroll').height($(window).height()-50);

             });

             $(".table-result tr").has("td").click(
                function()
                {
                    if($(this).data("key"))
                    {
                        $(".table-result tr").has("td").css("background-color","#FFFFFF");
                        $(this).css("background-color","#C5C5C5");
                        $.get( componentPath+"/ajaxAdv.php", { id: $(this).data("key") } )
                            .done(function( data ) {
                                $(".calls-report").html(data);
                                $(".calls-report").show();
                            })
                            .fail(
                                function()
                                {
                                    $(this).css("background-color","#FFFFFF");
                                }
                            );

                    }


                }
             );
             /*$(".calls-report").click(
                function()
                {
                    $(".calls-report").hide();
                    $(".table-result tr").has("td").css("background-color","#FFFFFF");
                }
             );*/

        }

    );
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1" id="form1">
        <input type="hidden" name="mode" value="tbl">
        <input type="hidden" name="page" value="1" id="page">
        <div class="form-element">
            <label for="from">Период:</label>
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "Y",
     "HIDE_TIMEBAR" => "N"
    )
);?>

        </div>
        <div class="form-element">
            <label for="dfrom">Продолжительность:</label>
            <input id="dfrom" name="dfrom" type="text" value="<?=$arResult["DFROM"]?>" size="11" autocomplete="off"> -
            <input id="dto" name="dto" type="text" value="<?=$arResult["DTO"]?>" size="11" autocomplete="off">
        </div>
         <div class="form-element">
            <label for='seanceresult'>Результат:</label>
            <select id='seanceresult' name='seanceresult'>
            <option value="999"> - </option>
              <? foreach ($arResult["SEANCE_RESULT_LIST"] as $arID=>$arName):?>
                  <? $sel=($arID==$arResult["SEANCE_RESULT"])?" selected":""; ?>
                  <option value="<?=$arID?>"<?=$sel?>><?=$arName?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-element">
            <label for="number">Номер:</label>
            <input id="number" name="number" type="text" value="<?=$arResult["NUMBER"]?>">
        </div>
        <div class="form-element">
            <label for='operator[]'>Оператор:</label>
            <select id='operator[]' name='operator[]' multiple size="20">
              <? foreach ($arResult["GROUP_LIST"] as $arGId=>$arGroup):?>
                  <? $sel=in_array($arGId,$arResult["OPERATOR"])?" selected":""; ?>
                  <option value="<?=$arGId?>" style="font-weight: bold; font-size: 12px;" <?=$sel?>><?=$arGroup["NAME"]?></option>
              <? endforeach;?>
              <? foreach ($arResult["GROUP_USER_LIST"] as $arUser):?>
                    <? $sel=in_array($arUser["ID"],$arResult["OPERATOR"])?" selected":""; ?>
                    <option value="<?=$arUser["ID"]?>"<?=$sel?>>&nbsp;&nbsp;&nbsp;<?=$arUser["NAME"]?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-element">
            <label for="bcount">Количество абонентов Б:</label>
            <input id="bcount" name="bcount" type="text" value="<?=$arResult["BCOUNT"]?>">
        </div>
        <div class="form-element">
            <label for="bcount">Абонент:</label>
            <input id="abon" name="abon" type="text" value="<?=$arResult["ABON"]?>">
        </div>
        <div class="form-element">
            <label for='seancetype'>Направление:</label>
            <select id='seancetype' name='seancetype'>
            <option value="999"> - </option>
              <? foreach ($arResult["SEANCE_TYPE_LIST"] as $arID=>$arName):?>
                  <? $sel=($arID==$arResult["SEANCE_TYPE"])?" selected":""; ?>
                  <option value="<?=$arID?>"<?=$sel?>><?=$arName?></option>
                <? endforeach; ?>
            </select>
        </div>
        <br>
        <br>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Полная таблица звонков</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="dfrom" type="hidden" value="<?=$arResult["DFROM"]?>">
        <input name="dto" type="hidden" value="<?=$arResult["DTO"]?>">
        <input name="seanceresult" type="hidden" value="<?=$arResult["SEANCE_RESULT"]?>">
        <input name="number" type="hidden" value="<?=$arResult["NUMBER"]?>">
        <? foreach($arResult["OPERATOR"] as  $oper):?>
        <input name="operator[]" type="hidden" value="<?=$oper?>">
        <? endforeach;?>
        <input name="bcount" type="hidden" value="<?=$arResult["BCOUNT"]?>">
        <input name="abon" type="hidden" value="<?=$arResult["ABON"]?>">
        <input name="seancetype" type="hidden" value="<?=$arResult["SEANCE_TYPE"]?>">
        <input name="page" type="hidden" value="-1">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <div>Всего найдено: <?=$arResult["ROWCOUNT"]?></div>
    <div>Страницы:
        <? for($pN=1;$pN<=ceil($arResult["ROWCOUNT"]/$arResult["LIMIT"]);$pN++):?>
        <? if($pN==$arResult["PAGE"]):?>
            <?=$pN?>
        <? else: ?>
            <a href="#" data-name="pager" data-key="<?=$pN?>"><?=$pN?></a>
        <? endif;?>
        <? endfor;?>
    </div>
    <div class="calls-report">&nbsp;</div>
    <div id="h_scroll"  style="overflow:scroll;">
	<div id="h_scroll_content">
    <? endif;?>
    <table class="table-result">
		<tr>
			<th rowspan="2">Дата и время</th>
			<th colspan="5">Продолжительность</th>
			<th colspan="3">Состояние и результат</th>
			<th>DTMF</th>
			<th colspan="3">Звонящий абонент</th>
			<th colspan="7">Первый абонент Б</th>
			<th colspan="7">Второй абонент Б</th>
			<th colspan="7">Третий абонент Б</th>
			<th colspan="7">Четвертый абонент Б</th>
			<th colspan="7">Последний абонент Б</th>
			<th colspan="1">Идентификаторы</th>
		</tr>
		<tr>
		    <!-- Продолжительность -->
		    <th>Итого</th>
		    <th>Ожидание</th>
		    <th>Разговор</th>
		    <th>Удержание</th>
		    <th>IVR</th>
		    <!-- Состояние и результат -->
		    <th>Направление</th>
		    <th>Результат</th>
		    <th>Количество абонентов Б</th>
		    <!-- DTMF -->
		    <th>DTMF (в IVR)</th>
		    <!-- Звонящий абонент -->
            <th>Номер</th>
            <th>Набранный номер</th>
            <th>Имя</th>
            <!-- Первый абонент Б-->
            <th>Номер</th>
            <th>Имя</th>
            <th>Тип</th>
            <th>Продолжительность</th>
            <th>Ожидание</th>
            <th>Разговор</th>
            <th>Удержание</th>
            <!-- Второй абонент Б-->
            <th>Номер</th>
            <th>Имя</th>
            <th>Тип</th>
            <th>Продолжительность</th>
            <th>Ожидание</th>
            <th>Разговор</th>
            <th>Удержание</th>
            <!-- Третий абонент Б-->
            <th>Номер</th>
            <th>Имя</th>
            <th>Тип</th>
            <th>Продолжительность</th>
            <th>Ожидание</th>
            <th>Разговор</th>
            <th>Удержание</th>
            <!-- Четвертый абонент Б -->
            <th>Номер</th>
            <th>Имя</th>
            <th>Тип</th>
            <th>Продолжительность</th>
            <th>Ожидание</th>
            <th>Разговор</th>
            <th>Удержание</th>
            <!--Последний абонент Б -->
            <th>Номер</th>
            <th>Имя</th>
            <th>Тип</th>
            <th>Продолжительность</th>
            <th>Ожидание</th>
            <th>Разговор</th>
            <th>Удержание</th>
            <!--Идентификаторы -->
            <th>ID</th>
        </tr>
        <? foreach($arResult["RESULT"] as $arItem):?>
        <tr data-key="<?=$arItem["ID"]?>">
            <!-- Дата и время -->
            <td><?=$arItem["TimeStart"]?></td>
		    <!-- Продолжительность -->
		    <td><?=$arItem["DurationFull"]?></td>
		    <td><?=$arItem["DurationWait"]?></td>
		    <td><?=$arItem["DurationTalk"]?></td>
		    <td><?=$arItem["DurationHold"]?></td>
		    <td><?=$arItem["DurationIVR"]?></td>
		    <!-- Состояние и результат -->
		    <td><?=$arItem["SeanceType"]?></td>
		    <td><?=$arItem["SeanceResult"]?></td>
		    <td><?=$arItem["BCount"]?></td>
		    <!-- DTMF -->
		    <td><?=$arItem["IVRDTMF"]?></td>
		    <!-- Звонящий абонент -->
            <td><?=$arItem["ANumber"]?></td>
            <td><?=$arItem["ANumberDealed"]?></td>
            <td><?=$arItem["AFIO"]?></td>
            <? foreach(["1","2","3","4","L"] as $char):?>
            <!-- <?=$char?> абонент Б-->
            <td><?=$arItem["B".$char."Number"]?></td>
            <td><?=$arItem["B".$char."FIO"]?></td>
            <td><?=$arItem["B".$char."AbonentType"]?></td>
            <td><?=$arItem["B".$char."DurationFull"]?></td>
            <td><?=$arItem["B".$char."DurationWait"]?></td>
            <td><?=$arItem["B".$char."DurationTalk"]?></td>
            <td><?=$arItem["B".$char."DurationHold"]?></td>
            <? endforeach;?>
            <!--Идентификаторы -->
            <td><?=$arItem["ID"]?></td>
        </tr>
        <?  //AddMessage2Log(print_r( $arItem , TRUE)); ?>
        <? endforeach;?>
	</table>
	<div>Время формирования отчета: <?=$arResult["TIME"]?>с</div>
	<? if($arResult["MODE"]!="exl"):?>
	</div>
	</div>
	<div>Страницы:
        <? for($pN=1;$pN<=ceil($arResult["ROWCOUNT"]/$arResult["LIMIT"]);$pN++):?>
        <? if($pN==$arResult["PAGE"]):?>
            <?=$pN?>
        <? else: ?>
            <a href="#" data-name="pager" data-key="<?=$pN?>"><?=$pN?></a>
        <? endif;?>
        <? endfor;?>
    </div>
    <? endif;?>
</div>
<? elseif($arResult["MODE"] && !$arResult["ERROR"]):?>
<div class="result">
<b>По Вашему запросу ничего не найдено</b>
</div>
<? else:?>
<div class="error">
<? foreach ($arResult["ERROR"] as $error):?>
<b><?=$error;?></b>
<? endforeach; ?>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>