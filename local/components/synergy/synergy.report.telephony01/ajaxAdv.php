<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 22.10.15
 * Time: 10:36
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/duration.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/mssql_connect.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/defines.php");
?>
    <style>
        table {
            border-collapse: collapse;
        }

        .table-result td {
            border: 1px solid #AAAAAA;
            padding: 1px 5px;
            font-size: 10px;
            text-align: left;
        }

        .table-result th {
            border: 1px solid #000000;
            padding: 1px 5px;
            font-weight: bold;
            font-size: 10px;
            background-color: #C0C0C0;
            text-align: center
        }
    </style>
<?

if(intval($_REQUEST["id"])!=0)
{
    $sql = "SELECT
      [ID]
      ,[TimeStart]
      ,[DurationFull]
      ,[DurationWait]
      ,[DurationTalk]
      ,[DurationHold]
      ,[ConnectionDirection]
      ,[ANumber]
      ,dbo.fnGetFIOFromPhone(ANumber) AS AUser
      ,[AAbonentType]
      ,[BNumber]
      ,dbo.fnGetFIOFromPhone(BNumber) AS BUser
      ,[BAbonentType]
      ,[DisconnectCause]
      ,[DisconnectSide]
      ,[ConnectionResult]
      ,[ConnectionState]
      ,[AWavFile]
      ,[BWavFile]
  FROM [Infinity].[dbo].[tConnections]
  WHERE IDSeance=" . $_REQUEST["id"] . " ORDER BY TimeStart ASC";


    $res = mssql_query($sql);
    if (!$res)
    {
        echo mssql_get_last_message();
    }
    else
    {
        while (($row = mssql_fetch_assoc($res)))
        {
            $row["TimeStart"] = preg_replace("/^(\d{4})-(\d{2})-(\d{2})/", "$3.$2.$1", preg_replace("/\..*/", "", $row["TimeStart"]));
            $row["DurationFull"] = day2duration($row["DurationFull"]);
            $row["DurationWait"] = day2duration($row["DurationWait"]);
            $row["DurationTalk"] = day2duration($row["DurationTalk"]);
            $row["DurationHold"] = day2duration($row["DurationHold"]);
            $row["DurationIVR"] = day2duration($row["DurationIVR"]);
            $arResult["RESULT"][] = $row;
        }
    }
    if (count($arResult["RESULT"])>0)
    {
        ?>
        <table class="table-result" id="call-result">
            <tr>
                <th>Дата и время</th>
                <th>Причина</th>
                <th>Разговор</th>
                <th>Номер А</th>
                <th>Имя А</th>
                <th>Номер Б</th>
                <th>Имя Б</th>
                <th>Запись разговора</th>
            </tr>
            <?
            $id=0;
            foreach ($arResult["RESULT"] as $call):
                $id++;
                ?>
                <tr>
                    <td><?= $call["TimeStart"] ?></td>
                    <td><?= $call["DisconnectCause"] ?></td>
                    <td><?= $call["DurationTalk"] ?></td>
                    <td><?= $call["ANumber"] ?></td>
                    <td><?= $call["AUser"] ?></td>
                    <td><?= $call["BNumber"] ?></td>
                    <td><?= $call["BUser"] ?></td>
                    <td>
                        <? if(!strpos($_SERVER['HTTP_USER_AGENT'],"MSIE")):?>
                            <? if(trim($call["AWavFile"])!="" && file_exists($_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_WAV.(str_replace("\\","/",$call["AWavFile"])))):?>
                            <audio id="a<?=$id?>" controls="" class="a_audio" data-key="<?=$id?>" src="<?=TELEPHONY_PATH_TO_WAV?><?= str_replace("\\","/",$call["AWavFile"]) ?>" >
                                HTML5 Audio is not supported
                            </audio>
                           <? endif;?>
                            <? if(trim($call["BWavFile"])!="" && file_exists($_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_WAV.(str_replace("\\","/",$call["BWavFile"])))):?>
                            <audio id="b<?=$id?>" class="b_audio" <? if(trim($call["AWavFile"])==""):?>controls<? endif;?> data-key="<?=$id?>" src="<?=TELEPHONY_PATH_TO_WAV?><?= str_replace("\\","/",$call["BWavFile"]) ?>">
                                HTML5 Audio is not supported
                            </audio>
                        <? endif;?>
                        <? else: ?>
                            Воспроизведение невозможно в Internet Explorer
                        <? endif;?>
                        <? if(trim($call["AWavFile"])!="" && file_exists($_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_WAV.(str_replace("\\","/",$call["AWavFile"])))
                             && trim($call["BWavFile"])!="" && file_exists($_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_WAV.(str_replace("\\","/",$call["BWavFile"])))
                             ):?>
                        <div style="font-size: 12px;width: auto; text-align: right;"><a href="/local/lib/telephony/dl.php?id=<?=$call["ID"]?>">Скачать</a></div>
                        <? endif;?>
                    </td>
                </tr>
            <? endforeach; ?>
        </table>
        <?
    }
    else
    {
        echo "Нет информации";
    }
}
?>
<div style="text-align: right; cursor: pointer;" id="closeit">[Закрыть]</div>
<script>
    var playing=false;
    var current_play=0;

    $(document).ready(
        function()
        {
            $("#closeit").click(
                function()
                {
                    $(".calls-report").hide();
                    $(".table-result tr").has("td").css("background-color","#FFFFFF");
                }
            );
            $(".a_audio").bind("play",
                function()
                {
                    $("#b"+$(this).data("key")).trigger("play");
                }
            );

            $(".a_audio").bind("pause",
                function()
                {
                    $("#b"+$(this).data("key")).trigger("pause");
                }
            );

            $(".a_audio").bind("volumechange",
                function()
                {
                    $("#b"+$(this).data("key")).prop("volume",$(this).prop("volume"));
                }
            );
            $(".a_audio").bind("seeked",
                function()
                {
                    $("#b"+$(this).data("key")).prop("currentTime",$(this).prop("currentTime"));
                }
            );
            $(".a_audio").bind("timeupdate",
                function()
                {
                    if($("#b"+$(this).data("key")).prop("muted")!=$(this).prop("muted"))
                        $("#b"+$(this).data("key")).prop("muted",$(this).prop("muted"));
                }
            );
        }
    );
</script>
