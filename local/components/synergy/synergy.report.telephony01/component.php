<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/duration.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/mssql_connect.php");

global $APPLICATION, $USER;

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

CJSCore::Init('jquery');

$APPLICATION->AddHeadScript("/local/assets/jquery.maskedinput.min.js", true);

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y 00:00:00");
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y 23:59:59");
$arResult["DFROM"]=(duration2sec($_REQUEST["dfrom"]))?$_REQUEST["dfrom"]:"00:00:00:00";
$arResult["DTO"]=(duration2sec($_REQUEST["dto"]))?$_REQUEST["dto"]:"01:00:00:00";

$arResult["DFROMS"]=duration2sec($arResult["DFROM"]);
$arResult["DTOS"]=duration2sec($arResult["DTO"]);

$arResult["DFROMD"]=($arResult["DFROMS"]==0)?0:$arResult["DFROMS"]/86400;
$arResult["DTOD"]=($arResult["DTOS"]==0)?0:$arResult["DTOS"]/86400;

$arResult["SEANCE_RESULT_LIST"]=[
    0 => "Неизвестно",
    100=>"Успешно",
    101=>"Обработан первым оператором",
    102=>"Обработан операторами",
    103=>"Обработан внешним агентом",
    104=>"Обработан в конференции",
    111=>"Занято",
    112=>"Не подняли трубку",
    113=>"Номер не существует",
    114=>"Обработан в IVR",
    131=>"Звонящий не дождался ответа и отбился",
    141=>"Сработал факс",
    151=>"Слишком короткий разговор",
    191=>"Потерян во время ожидания ответа",
    192=>"Потерян во время удержания",
    200=>"Ошибка"
];

$arResult["SEANCE_RESULT"]=(intval($_REQUEST["seanceresult"])>0)?$_REQUEST["seanceresult"]:"999";

$arResult["SEANCE_STATE_LIST"]=[
    0=>"Неизвестно",
    12=>"Наборномера",
    13=>"Ожиданиеответа",
    21=>"Разговор двух абонентов",
    31=>"Конференция",
    41=>"Удержание",
    100=>"Завершен"
];

$arResult["NUMBER"]=(intval($_REQUEST["number"])>0)?$_REQUEST["number"]:"";

$arResult["GROUP_LIST"]=[];

$sql="SELECT dbo.tGroups.ID as GId,
	dbo.tGroups.Name as GName,
	dbo.tUsers.Login,
	dbo.tUsers.ID,
	dbo.tUsers.Name_F,
	dbo.tUsers.Name_I,
	dbo.tUsers.Deleted,
	dbo.tUsers.IsSystem
FROM dbo.tUsers LEFT JOIN dbo.tGroupsLinks ON dbo.tUsers.ID = dbo.tGroupsLinks.IDUser
	 LEFT JOIN dbo.tGroups ON dbo.tGroupsLinks.IDGroup = dbo.tGroups.ID
WHERE dbo.tUsers.IsSystem = 0 AND dbo.tUsers.Deleted = 0
ORDER BY dbo.tGroups.Name ASC, dbo.tUsers.Name_F ASC, dbo.tUsers.Name_I ASC";

$res = mssql_query($sql);
while ($row = mssql_fetch_assoc($res)) {
    if($row["GId"]!="")
    {
        $arResult["GROUP_LIST"][$row["GId"]]["NAME"]=$row["GName"];
        $arResult["GROUP_LIST"][$row["GId"]]["USERS"][$row["ID"]]=$row["Name_F"]." ".$row["Name_I"];
    }

    if(trim($row["Name_F"])!="" && trim($row["Name_I"])!="")
    {
        $arResult["GROUP_USER_LIST"][$row["ID"]]=[
            "NAME"=>$row["Name_F"]." ".$row["Name_I"],
            "ID"=>$row["ID"]
        ];
    }


    $arResult["USER_LIST"][$row["ID"]]=$row["Name_F"]." ".$row["Name_I"];
}

usort ($arResult["GROUP_USER_LIST"], function($a, $b) {
    return strcmp($a["NAME"], $b["NAME"]);
});

$arResult["OPERATOR"]=(is_array($_REQUEST["operator"]))?$_REQUEST["operator"]:[];

$arResult["BCOUNT"]=(intval($_REQUEST["bcount"])>0)?intval($_REQUEST["bcount"]):0;
$arResult["ABON"]=(intval($_REQUEST["abon"])>0)?$_REQUEST["abon"]:"";

$arResult["SEANCE_TYPE_LIST"]=[
    0 => "Неизвестно",
    1 => "Входящий",
    2 => "Исходящий",
    3 => "Внутренный"
];

$arResult["SEANCE_TYPE"]=(intval($_REQUEST["seancetype"])>0)?$_REQUEST["seancetype"]:"999";

$arResult["ABON_TYPE_LIST"]=[
    0=>"Неизвестно",
    1=>"Внешний",
    2=>"Внутренний",
    3=>"IVR",
    4=>"Очередь"
];

$arResult["LIMIT"]=100;
$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PAGE"]=($_REQUEST["page"])?$_REQUEST["page"]:1;

if($arResult["MODE"] && !$arResult["ERROR"])
{
    $start=time();
    $from=preg_replace("/^(\d\d)\.(\d\d)\./","$2/$1/",$arResult["FROM"]);
    $to=preg_replace("/^(\d\d)\.(\d\d)\./","$2/$1/",$arResult["TO"]);

    $listID=join(" ",$arResult["OPERATOR"]);

    $SEANCE_RESULT=($arResult["SEANCE_RESULT"] && $arResult["SEANCE_RESULT"]!=999)?$arResult["SEANCE_RESULT"]:"NULL";

    $SEANCE_TYPE=($arResult["SEANCE_TYPE"] && $arResult["SEANCE_TYPE"]!=999)?$arResult["SEANCE_TYPE"]:"NULL";

    $NUMBER=(trim($arResult["NUMBER"])!="")?"'".$arResult["NUMBER"]."'":"NULL";

    $listID=(trim($listID)!="")?"'".$listID."'":"NULL";

    $ABON=(trim($arResult["ABON"])!="")?"'".$arResult["ABON"]."'":"NULL";

    $BCOUNT=($arResult["BCOUNT"]>0)?$arResult["BCOUNT"]:"NULL";

    $sql="SELECT [WHAT] FROM
          report_all_seances('{$USER->GetLogin()}','{$from}','{$to}',{$arResult["DFROMD"]},{$arResult["DTOD"]},{$SEANCE_RESULT},{$SEANCE_TYPE},{$BCOUNT},{$ABON},{$NUMBER},{$listID})";

    $sql_count=str_replace("[WHAT]","count(*) as cnt",$sql);

    //o($sql_count);

    //AddMessage2Log($sql_count);
    $res=mssql_query($sql_count);
    if($row=mssql_fetch_assoc($res))
    {
        $arResult["ROWCOUNT"]=$row["cnt"];
    }


    $sql=str_replace("[WHAT]","*",$sql);

    $sql.=" ORDER BY TimeStart DESC";
    if($arResult["PAGE"]>0)
    {
        $sql.=" OFFSET ".(($arResult["PAGE"]-1)*$arResult["LIMIT"])." ROWS FETCH NEXT ".$arResult["LIMIT"]." ROWS ONLY";
    }

    //AddMessage2Log($sql);
    $res=mssql_query($sql);
    if (!$res) {
        $arResult["ERROR"][]=mssql_get_last_message();
    }
    else
    {
        while(($row=mssql_fetch_assoc($res)))
        {
            $row["TimeStart"]=preg_replace("/^(\d{4})-(\d{2})-(\d{2})/","$3.$2.$1",preg_replace("/\..*/","",$row["TimeStart"]));
            $row["DurationFull"]=day2duration($row["DurationFull"]);
            $row["DurationWait"]=day2duration($row["DurationWait"]);
            $row["DurationTalk"]=day2duration($row["DurationTalk"]);
            $row["DurationHold"]=day2duration($row["DurationHold"]);
            $row["DurationIVR"]=day2duration($row["DurationIVR"]);
            $row["SeanceType"]=$arResult["SEANCE_TYPE_LIST"][$row["SeanceType"]];
            $row["SeanceResult"]=$arResult["SEANCE_RESULT_LIST"][$row["SeanceResult"]];
            $row["SeanceState"]=$arResult["SEANCE_STATE_LIST"][$row["SeanceState"]];
            if($arResult["USER_LIST"][$row["AIDUser"]])
                $row["AIDUser"]=$arResult["USER_LIST"][$row["AIDUser"]];
            elseif($arResult["GROUP_LIST"][$row["AIDUser"]])
                $row["AIDUser"]=$arResult["GROUP_LIST"]["NAME"];
            else
                $row["AIDUser"]=(!empty($row["AIDUser"]))?"Неизвестно ({$row["AIDUser"]})":"";

            foreach(["1","2","3","4","L"] as $char)
            {
                if($arResult["USER_LIST"][$row["B".$char."IDUser"]])
                    $row["B".$char."IDUser"]=$arResult["USER_LIST"][$row["B".$char."IDUser"]];
                elseif($arResult["GROUP_LIST"][$row["B".$char."IDUser"]])
                    $row["B".$char."IDUser"]=$arResult["GROUP_LIST"]["NAME"];
                else
                    $row["B".$char."IDUser"]=(!empty($row["B".$char."IDUser"]))?"Неизвестно ({$row["B".$char."IDUser"]})":"";

                $row["B".$char."DurationFull"]=day2duration($row["B".$char."DurationFull"]);
                $row["B".$char."DurationWait"]=day2duration($row["B".$char."DurationWait"]);
                $row["B".$char."DurationTalk"]=day2duration($row["B".$char."DurationTalk"]);
                $row["B".$char."DurationHold"]=day2duration($row["B".$char."DurationHold"]);
                $row["B".$char."AbonentType"]=$arResult["ABON_TYPE_LIST"][$row["B".$char."AbonentType"]];
            }

            $arResult["RESULT"][]=$row;
        }
    }

    //AddMessage2Log(print_r($arResult, TRUE));
    $arResult["TIME"]=time()-$start;
}

$this->IncludeComponentTemplate();
?>