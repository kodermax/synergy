<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

CJSCore::Init('jquery');

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");

$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PERIOD"]=$_REQUEST["period"];
$arResult["DEVIDE"]=$_REQUEST["devide"];

$arResult["PERIODS"] = Array(
    "cday"=>"Текущий день",
    "pday"=>"Предыдущий день",
    "cweek"=>"Текущая неделя",
    "pweek"=>"Предыдущая неделя",
    "cmonth"=>"Текущий месяц",
    "pmonth"=>"Предыдущий месяц",
    "cyear"=>"Текущий год",
    "pyear"=>"Предыдущий год",
    "interval"=>"Интервал"
);

$arResult["DEVIDES"]=array(
    "0"=>"За весь период",
    "1"=>"По дням",
    "2"=>"По неделям",
    "3"=>"По месяцам"
);

$arResult["DEPTS"] = [
    3445 => "Диспетчерская Служба",
    3452 => "Отдел продаж №1",
    3455 => "Отдел продаж №2",
    3457 => "Отдел продаж №3",
    3458 => "Отдел продаж №4",
    3459 => "Отдел продаж №5",
    3464 => "Магистратура",
    3461 => "Отдел по взаимодействию с Министерством Обороны и МЧС России",
    3463 =>"Центр языковой подготовки",
    3454=>"Отдел продаж №10"
];

$arResult["DS"]=3445;

$arResult["SOURCE_DESCRIPTION"]=['LINGVA','Университет','Мегакампус','Megacampus'];

$arResult["A_DEPTS"] = [
    4260 => "Амазонки",
    3794 => "Спарта",
    3449 => "ШБ",
    4320 => "Русь",
    651 => "Регион", //поока неизвестно что с этим делать
    3718 => "Дубай",
    4110 => "МОИ",
    3423 => "МО",
    3933 => "Диджитал"
];

$arResult["A_DIVISION"]=[4260,3794,4320];

$arResult["A_DEPTS_ALL"] = [];

foreach($arResult["A_DEPTS"] as $deptID=>$deptName)
{
    $arResult["A_DEPTS_ALL"][$deptID][]=$deptID;

    $rsParentSection = CIBlockSection::GetByID($deptID);
    if ($arParentSection = $rsParentSection->GetNext())
    {
        $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
        $rsDept = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
        while ($arDept = $rsDept->GetNext())
        {
            if(!$arResult["A_DEPTS"][$arDept["ID"]])
                $arResult["A_DEPTS_ALL"][$deptID][]=$arDept["ID"];
        }
    }
}

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;



}


if($arResult["MODE"])
{

    foreach($arResult["DEPTS"] as $deptID=>$deptName)
    {
        $rsUser= CUser::GetList($by='ID', $order='ASC', array('UF_DEPARTMENT' => $deptID), array('SELECT' => array('UF_DEPARTMENT')));

        while($arUser=$rsUser->GetNext())
        {
            if(!$arResult["USERS"][$arUser["ID"]])
            {
                $arResult["USERS"][$arUser["ID"]]=$deptID;
            }
            $arResult["DEPT_USERS"][$deptID][$arUser["ID"]]=true;
        }
    }

    foreach($arResult["A_DEPTS"] as $deptID=>$deptName)
    {
        $rsUser= CUser::GetList($by='ID', $order='ASC', array('UF_DEPARTMENT' =>$arResult["A_DEPTS_ALL"][$deptID]), array('SELECT' => array('UF_DEPARTMENT')));

        while($arUser=$rsUser->GetNext())
        {
            if(!$arResult["A_USERS"][$arUser["ID"]])
            {
                $arResult["A_USERS"][$arUser["ID"]]=$deptID;
            }
            $arResult["A_DEPT_USERS"][$deptID][$arUser["ID"]]=true;
        }
    }

    $list_user=join(",",array_keys($arResult["USERS"]));
    if($list_user=="")
        $list_user="-1";

    $list_user_ds = join(",",array_keys($arResult["DEPT_USERS"][$arResult["DS"]]));
    if($list_user_ds=="")
        $list_user_ds="-1";

    $from=$DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to=$DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));
    $groupby="";
    $select="";

    switch($arResult["DEVIDE"])
    {
        case 1:
            $groupby='`YEAR`,`MONTH`,`DAY`,';
            $select="DAYOFMONTH(`crm_lead`.DATE_CREATE) as `DAY`,
        MONTH(`crm_lead`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
        case 2:
            $groupby='`YEAR`,`WEEK`,';
            $select="YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,
        WEEK(`crm_lead`.DATE_CREATE) as `WEEK`,";
            break;
        case 3:
            $groupby='`YEAR`,`MONTH`,';
            $select="MONTH(`crm_lead`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
        case 4:
            $groupby='`YEAR`,';
            $select="YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
    }

    //Считаем общее количество заявок

    $sql="
      select
        $select
        if(`uts`.".PROP_LEAD_LAND."='proftest' OR `uts`.".PROP_LEAD_LAND."='egemetr','_NONOBJECT','_OBJECT') AS TOTAL,
        `uts`.".PROP_LEAD_LAND." AS `LEND`,
        `crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
        COUNT(`crm_lead`.ID) as CNT
      from
        `b_crm_lead` `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
        AND (`crm_lead`.`ASSIGNED_BY_ID` IN ($list_user) OR (`crm_lead`.`ASSIGNED_BY_ID` NOT IN ($list_user) AND `uts`.".PROP_LEAD_DISPATCHER." IN ($list_user_ds)))
        AND `uts`.".PROP_LEAD_LAND."!='' AND `uts`.".PROP_LEAD_LAND." is not NULL
      GROUP BY $groupby `LEND`,`ASSIGNED_BY_ID`
      ORDER BY `crm_lead`.DATE_CREATE, `uts`.".PROP_LEAD_LAND.", `crm_lead`.`ASSIGNED_BY_ID`
    ";

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }
        if(!$arResult["RESULT"][$date][$row["TOTAL"]])
            $arResult["RESULT"][$date][$row["TOTAL"]]=0;
        $arResult["RESULT"][$date][$row["TOTAL"]]+=$row["CNT"];
    }

    // Всего ДС и всего невалидных

    $sql="
      select
        $select
        `crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
        if(`crm_lead`.STATUS_ID NOT IN('5','9','11'),'_VALID','_NOTVALID') AS `STATUS_ID`,
        COUNT(`crm_lead`.ID) as CNT
      from
        `b_crm_lead` `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
        AND `uts`.".PROP_LEAD_DISPATCHER." IN ($list_user_ds)
        AND `uts`.".PROP_LEAD_LAND."!='' AND `uts`.".PROP_LEAD_LAND." is not NULL
        AND `uts`.".PROP_LEAD_LAND."!='proftest'
      GROUP BY $groupby `ASSIGNED_BY_ID`,`STATUS_ID`
      ORDER BY `crm_lead`.`ASSIGNED_BY_ID`,`crm_lead`.`STATUS_ID`,`crm_lead`.DATE_CREATE
    ";

    $res=$DB->Query($sql);

    while($row=$res->Fetch())
    {
        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }
        if(!$arResult["RESULT"][$date][$row["STATUS_ID"]])
            $arResult["RESULT"][$date][$row["STATUS_ID"]]=0;
        $arResult["RESULT"][$date][$row["STATUS_ID"]]+=$row["CNT"];
    }

    // Дубль, ошибка номера, тест

    $sql="
      select
        $select
        `crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
        `crm_lead`.STATUS_ID AS `STATUS_ID`,
        COUNT(`crm_lead`.ID) as CNT
      from
        `b_crm_lead` `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
        AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user_ds)
        AND `uts`.".PROP_LEAD_LAND."!='' AND `uts`.".PROP_LEAD_LAND." is not NULL
        AND `uts`.".PROP_LEAD_LAND."!='proftest'
        AND `crm_lead`.STATUS_ID IN('5','9','11')
      GROUP BY $groupby `ASSIGNED_BY_ID`,`STATUS_ID`
      ORDER BY `crm_lead`.`ASSIGNED_BY_ID`,`crm_lead`.`STATUS_ID`,`crm_lead`.DATE_CREATE
    ";

    $res=$DB->Query($sql);

    while($row=$res->Fetch())
    {
        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }
        if(!$arResult["RESULT"][$date][$row["STATUS_ID"]])
            $arResult["RESULT"][$date][$row["STATUS_ID"]]=0;
        $arResult["RESULT"][$date][$row["STATUS_ID"]]+=$row["CNT"];
    }

    // Передано в др подразделения и Переданы ДС и Ушли владельцам контактов
    // И по отделам

    $sql="
      select
        $select
        if(`uts`.".PROP_LEAD_DISPATCHER." IN ($list_user_ds), '_BYDS','_NOTBYDS') as DS,
        `crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
        `crm_lead`.`SOURCE_DESCRIPTION` as `SOURCE_DESCRIPTION`,
        COUNT(`crm_lead`.ID) as CNT
      from
        `b_crm_lead` `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
        AND `crm_lead`.`ASSIGNED_BY_ID` NOT IN ($list_user)
        AND `uts`.".PROP_LEAD_LAND."!='' AND `uts`.".PROP_LEAD_LAND." is not NULL
        AND `uts`.".PROP_LEAD_LAND."!='proftest'
      GROUP BY $groupby `ASSIGNED_BY_ID`,DS, `SOURCE_DESCRIPTION`
      ORDER BY `crm_lead`.DATE_CREATE, `crm_lead`.`ASSIGNED_BY_ID`
    ";

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $dept=$arResult["A_USERS"][$row["ASSIGNED_BY_ID"]];
        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }
        if(!$arResult["RESULT"][$date][$row["DS"]])
            $arResult["RESULT"][$date][$row["DS"]]=0;
        $arResult["RESULT"][$date][$row["DS"]]+=$row["CNT"];
        if($dept)
        {

            if(!$arResult["RESULT"][$date]["_ADEPT"][$dept])
                $arResult["RESULT"][$date]["_ADEPT"][$dept]=0;

            if(!$arResult["RESULT"][$date]["_ALLDS"])
                $arResult["RESULT"][$date]["_ALLDS"]=0;

            if(
                $row["DS"]=="_BYDS" ||
                ($row["DS"]=="_NOTBYDS" && in_array($dept,$arResult["A_DIVISION"]) && in_array($row["SOURCE_DESCRIPTION"],$arResult["SOURCE_DESCRIPTION"]))
              )
            {
                $arResult["RESULT"][$date]["_ADEPT"][$dept]+=$row["CNT"];
                $arResult["RESULT"][$date]["_ALLDS"]+=$row["CNT"];
            }
        }
    }

    // Повторы


    $sql="
      select
        $select
        `crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
        `uts`.".PROP_LEAD_DISPATCHER." as `DISP`,
        COUNT(`crm_lead`.ID) as CNT
      from
        `b_crm_lead` `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
        AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user)
        AND `uts`.".PROP_LEAD_LAND."!='' AND `uts`.".PROP_LEAD_LAND." is not NULL
        AND `uts`.".PROP_LEAD_LAND."!='proftest'
        AND `crm_lead`.STATUS_ID='DETAILS'
      GROUP BY $groupby `ASSIGNED_BY_ID`,`DISP`
      ORDER BY `crm_lead`.`ASSIGNED_BY_ID`,`crm_lead`.DATE_CREATE
    ";

    $res=$DB->Query($sql);

    while($row=$res->Fetch())
    {
        $dept_assign=$arResult["USERS"][$row["ASSIGNED_BY_ID"]];
        $dept_disp=$arResult["USERS"][$row["DISP"]];
        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }

        if(!$arResult["RESULT"][$date]["_13"])
            $arResult["RESULT"][$date]["_13"]=0;
        $arResult["RESULT"][$date]["_13"]+=$row["CNT"];

        if($dept_disp==$arResult["DS"] && $dept_assign!=$arResult["DS"] && in_array($dept_assign,array_keys($arResult["DEPTS"])))
        {
            if(!$arResult["RESULT"][$date]["_14"])
                $arResult["RESULT"][$date]["_14"]=0;
            $arResult["RESULT"][$date]["_14"]+=$row["CNT"];
        }

        if($dept_disp==$arResult["DS"] && $dept_assign==$arResult["DS"])
        {
            if(!$arResult["RESULT"][$date]["_15"])
                $arResult["RESULT"][$date]["_15"]=0;
            $arResult["RESULT"][$date]["_15"]+=$row["CNT"];
        }

        if($dept_disp!=$arResult["DS"] && in_array($dept_disp,array_keys($arResult["DEPTS"])) && $dept_assign!=$arResult["DS"] && in_array($dept_assign,array_keys($arResult["DEPTS"])))
        {
            if(!$arResult["RESULT"][$date]["_16"])
                $arResult["RESULT"][$date]["_16"]=0;
            $arResult["RESULT"][$date]["_16"]+=$row["CNT"];
        }

    }


    // TO DO sql для подсчета


}
$this->IncludeComponentTemplate();
?>