<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<script>
var devides=<? echo json_encode($arResult["DEVIDES"]);?>;
$(document).ready(
    function()
    {
        $("#period").change(
            function()
            {
                if($("#period").val()=="interval")
                    $("#interval").css('display', 'inline-block');
                else
                    $("#interval").hide();

                var max_devide=1;
                 switch($("#period").val())
                {
                    case "cmonth":
                    case "pmonth":
                        max_devide=2;
                        break;
                    case "cyear":
                    case "pyear":
                    case "interval":
                        max_devide=3;
                        break;
                }

                $("#devide").empty();
                for(var i=0; i<=max_devide; i++)
                {
                    $("#devide").append("<option value=\""+i+"\">"+devides[i]+"</option>");
                }
            }
        );
    }

);
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1">
        <input type="hidden" name="mode" value="tbl">
        <div class="form-element">
            <label for="period">Период:</label>
            <select id="period" name="period">
                <? foreach($arResult["PERIODS"] as $k=>$v):?>
                    <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                <? endforeach; ?>
            </select>
        <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "N",
     "HIDE_TIMEBAR" => "Y"
    )
);?>
        </div>
        </div>
         <div class="form-element">
            <label for="devide">Разбивка:</label>
            <select id="devide" name="devide">

            <?
             $max_devide=1;
            switch($arResult["PERIOD"])
            {
                case "cmonth":
                case "pmonth":
                    $max_devide=2;
                    break;
                case "cyear":
                case "pyear":
                case "interval":
                    $max_devide=3;
                    break;
            }
            ?>
                <? for($arDiv=0;$arDiv<=$max_devide; $arDiv++):?>
                    <option value="<?=$arDiv?>"<?=($arResult["DEVIDE"]==$arDiv)?" selected":""?>><?=$arResult["DEVIDES"][$arDiv]?></option>
                <? endfor; ?>
            </select>
        </div>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Воронка эффективности лидов</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="period" type="hidden" value="<?=$arResult["PERIOD"]?>">
        <input name="devide" type="hidden" value="<?=$arResult["DEVIDE"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? endif;?>
    <? $arData=$arResult["RESULT"];?>
    <? $vsego=[]; ?>
    <table class="table-result">
		<tr>
		    <th>№</th>
			<th>&nbsp;</th>
    		<? foreach($arData as $arDate=>$arCat):?>
			<th><?=$arDate?></th>
			<? endforeach;?>
			<th>Итого</th>
		</tr>
		<tr style=" background-color: #EBF1DE;">
		    <td>1</td>
		    <td style="text-align: center;"><b>Всего лидов КД</b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $arCat["_ALL"]=$arData[$arDate]["_ALL"]=intval($arCat["_OBJECT"]+$arCat["_NONOBJECT"]);?>
            <? $itogo+=$arCat["_ALL"];?>
			<td><?=intval($arCat["_ALL"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #DAEEF3;">
            <td>2</td>
		    <td style="text-align: left; ">Целевые</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_OBJECT"]);?>
			<td><?=intval($arCat["_OBJECT"])?></td>
			<? endforeach;?>
			<? $itogoObj=$itogo;?>
			<td><?=$itogo?></td>
        </tr>
        <tr>
            <td>3</td>
		    <td style="text-align: left;">Нецелевые</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_NONOBJECT"]);?>
			<td><?=intval($arCat["_NONOBJECT"])?></td>
			<? endforeach;?>
			<? $itogoNonObj=$itogo;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #DAEEF3;">
            <td>4</td>
		    <td style="text-align: left; ">Всего ДС</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_VALID"]+$arCat["_NOTVALID"]);?>
			<td><?=intval($arCat["_VALID"]+$arCat["_NOTVALID"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
         <tr style="background-color: #F1DCDB;">
            <td>5</td>
		    <td style="text-align: left; ">Всего валидных</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_VALID"]);?>
			<td><?=intval($arCat["_VALID"])?></td>
			<? $vsego[$arDate]=intval($arCat["_VALID"]);?>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr>
            <td>6</td>
		    <td style="text-align: left; ">Дубль</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["5"]);?>
			<td><?=intval($arCat["5"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr>
            <td>7</td>
		    <td style="text-align: left; ">Ошибка номера</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["9"]);?>
			<td><?=intval($arCat["9"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr>
            <td>8</td>
		    <td style="text-align: left; ">Тест</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["11"]);?>
			<td><?=intval($arCat["11"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #FDE9D9;">
            <td>9</td>
		    <td style="text-align: left; ">Передано в др подразделения</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_ALLDS"]);?>
			<td><?=intval($arCat["_ALLDS"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #FDE9D9;">
            <td>10</td>
		    <td style="text-align: left; ">Переданы ДС</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_BYDS"]);?>
			<td><?=intval($arCat["_BYDS"])?></td>
			<? $vsego[$arDate]-=intval($arCat["_BYDS"]);?>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #FDE9D9;">
            <td>11</td>
		    <td style="text-align: left; ">Ушли владельцам контактов</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_ALLDS"])-intval($arCat["_BYDS"]);?>
			<td><?=intval($arCat["_ALLDS"])-intval($arCat["_BYDS"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <? $num=true;?>
        <? foreach($arResult["A_DEPTS"] as $deptID=>$deptName):?>
        <tr>
            <? if($num):?>
            <td rowspan="<?=count($arResult["A_DEPTS"])?>">12</td>
            <? $num=false; ?>
            <? endif;?>
		    <td style="text-align: left; "><?=$deptName?></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_ADEPT"][$deptID]);?>
			<td><?=intval($arCat["_ADEPT"][$deptID])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <? $iN++;?>
        <? endforeach;?>
        <tr style="background-color: #F2F2F2;">
            <td>13</td>
		    <td style="text-align: left; "><b>Повторные заявки</b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_13"]);?>
			<td><?=intval($arCat["_13"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #F2F2F2;">
            <td>14</td>
		    <td style="text-align: left; "><b>Переданы ДС</b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_14"]);?>
			<td><?=intval($arCat["_14"])?></td>
			<? $vsego[$arDate]-=intval($arCat["_14"]);?>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #F2F2F2;">
            <td>15</td>
		    <td style="text-align: left; "><b>Повторы ДС</b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_15"]);?>
			<td><?=intval($arCat["_15"])?></td>
			<? $vsego[$arDate]-=intval($arCat["_15"]);?>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #F2F2F2;">
            <td>16</td>
		    <td style="text-align: left; "><b>Ушли владельцам контактов</b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_16"]);?>
			<td><?=intval($arCat["_16"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #EBF1DE;">
            <td>17</td>
		    <td style="text-align: left; "><b>Всего эффект</b></td>
		    <? $itogoVsego=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogoVsego+=intval($vsego[$arDate]);?>
			<td><?=intval($vsego[$arDate])?></td>
			<? endforeach;?>
			<td><?=$itogoVsego?></td>
        </tr>
        <tr style="background-color: #DA9694;">
            <td>18</td>
		    <td style="text-align: left; "><b>Всего эффект</b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_OBJECT"]);?>
			<td><?=intval($vsego[$arDate]*100/$arCat["_OBJECT"])?>%</td>
			<? endforeach;?>
			<td><?=intval($itogoVsego*100/$itogo)?>%</td>
        </tr>
	</table>
</div>
<? elseif($arResult["MODE"]):?>
<div class="result">
<b>По Вашему запросу ничего не найдено</b>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>