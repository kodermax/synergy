<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CJSCore::Init('jquery');
$APPLICATION->AddHeadScript("/local/assets/chosen/chosen.jquery.min.js", true);
$APPLICATION->SetAdditionalCSS("/local/assets/chosen/chosen.min.css", true);

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");

$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PERIOD"]=$_REQUEST["period"];

$arResult["PERIODS"] = Array(
    "cday"=>"Текущий день",
    "pday"=>"Предыдущий день",
    "cweek"=>"Текущая неделя",
    "pweek"=>"Предыдущая неделя",
    "cmonth"=>"Текущий месяц",
    "pmonth"=>"Предыдущий месяц",
    "cyear"=>"Текущий год",
    "pyear"=>"Предыдущий год",
    "interval"=>"Интервал"
);

$arResult["MAIN_DEPT"]=3249;

if(intval($_REQUEST["dept"])==0)
{
    $arResult["DEPT"]=$arResult["MAIN_DEPT"];
}
else
    $arResult["DEPT"]=$_REQUEST["dept"];

$arResult["REC"]=$_REQUEST["rec"];

$rsDept=CIBlockSection::GetTreeList(array("IBLOCK_ID"=>1,"ACTIVE"=>"Y"));
while($arDept=$rsDept->GetNext())
{
    if($arDept["IBLOCK_SECTION_ID"]==$arResult["MAIN_DEPT"] || $arDept["ID"]==$arResult["MAIN_DEPT"])
        $arResult["DEPT_LIST"][]=$arDept;
}

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;



}


if($arResult["MODE"])
{

    $rsDept=CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1,"ACTIVE"=>"Y","ID"=>$arResult["DEPT"]),false,array("ID","NAME"));
    $arResult["DEPT_DATA"][$arResult["DEPT"]]=$rsDept->Fetch();

    $rsUser=CIntranetUtils::GetDepartmentEmployees(array_keys($arResult["DEPT_DATA"]),($arResult["REC"]==1));


    while($arUser=$rsUser->GetNext())
    {
        $arResult["USERS"][$arUser["ID"]]=$arUser;
    }

    $list_user=join(",",array_keys($arResult["USERS"]));
    if($list_user=="")
        $list_user="-1";

    $from=$DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to=$DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));
    $groupby="";
    $select="";

    $sql="
        SELECT
                crm_lead.ID,
                crm_lead.TITLE,
                uts.".PROP_LEAD_CHANNEL." CHANNEL,
                uts.".PROP_LEAD_SOURCE." SOURCE,
                uts.".PROP_LEAD_LAND." LAND,
                crm_lead.SOURCE_DESCRIPTION ADD_SOURCE,
                contact.`NAME` `NAME`,
                contact.`LAST_NAME` `LAST_NAME`,
                contact.`SECOND_NAME` `SECOND_NAME`,
                `crm_lead`.DATE_CREATE `DATE_CREATE`,
                uts.".PROP_LEAD_PAGEFROM." PAGEFROM,
                status.`NAME` STATUS_NAME,
                multi.`VALUE` SITE
                FROM
                        `b_crm_lead` `crm_lead`
                        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
                        LEFT JOIN `b_crm_status` `status` ON (`crm_lead`.STATUS_ID=`status`.STATUS_ID AND `status`.ENTITY_ID='STATUS')
                        LEFT JOIN `b_crm_contact` `contact` ON (`crm_lead`.CONTACT_ID=`contact`.ID)
                        LEFT JOIN `b_crm_field_multi` `multi` ON (`crm_lead`.CONTACT_ID=`multi`.ELEMENT_ID AND `multi`.TYPE_ID='WEB' AND `multi`.ENTITY_ID='CONTACT')
                WHERE
                        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'  AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user)
                ORDER BY crm_lead.DATE_CREATE, crm_lead.ID

        ";

    //echo $sql;

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $row["DATE"]=$DB->FormatDate($row["DATE_CREATE"],"YYYY-MM-DD HH:MI:SS","DD.MM.YYYY");
        $arResult["RESULT"][]=$row;
    }
}
$this->IncludeComponentTemplate();
?>