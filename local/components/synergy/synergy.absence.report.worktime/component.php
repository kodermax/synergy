<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 13.11.2015
        * Time: 13:10
        */
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

define('WORKTIME_IBLOCK_CODE', 'worktime');

if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

ini_set('display_errors','On');
error_reporting('E_ALL');

session_start();
$arResult['ACTION'] = $_REQUEST['action'];
$date_start = $_REQUEST['datestart'];
$date_end = $_REQUEST['dateend'];
$department = $_REQUEST['department'];

// Устанавливаем значения фильтра ----------------------
$arResult['NO_PARAMS'] = false;
$arResult['NEED_TO_GENERATE'] = false;

if ($arResult['ACTION'] == "ajaxreport"){
    $_SESSION['worktime_report_department'] = $department;
    $arResult['DEPARTMENT'] = $department;
    if (empty($date_start)){
        $arResult['NO_PARAMS'] = true;
    } else {
        $_SESSION['worktime_report_date_start'] = $date_start;
        $arResult['DATE_START'] = $date_start;
    }
    if (empty($date_end)){
        $arResult['NO_PARAMS'] = true;
    } else {
        $_SESSION['worktime_report_date_end'] = $date_end;
        $arResult['DATE_END'] = $date_end;
    }
//
} elseif (($arResult['ACTION'] == "empty") || (empty($arResult['ACTION']))){
    if (isset($_SESSION['worktime_report_department'])){
        $arResult['DEPARTMENT'] = $_SESSION['worktime_report_department'];
    } else{
        $ar_depts = CIntranetUtils::GetUserDepartments($USER->GetID());
        if (!empty($ar_depts)) {
            $arResult['DEPARTMENT'] = $ar_depts[0];
        }
    }
    if (isset($_SESSION['worktime_report_date_start'])){
        $arResult['DATE_START'] = $_SESSION['worktime_report_date_start'];
    } else {
// Первоначальная инициализация
// Если текущее число более 5, то устанавливаем первое число текущего месяца
// Иначе первое число предыдущего
        if (date("d") >= 5) {
            $arResult['DATE_START'] = "01." . date("m.Y");
        } else
        {
            $arResult['DATE_START'] = "01." . date("m.Y", strtotime("-6 day"));
        }
    }
    if (isset($_SESSION['worktime_report_date_end'])){
        $arResult['DATE_END'] = $_SESSION['worktime_report_date_end'];
    } else{
        $arResult['DATE_END'] = ''; // Пустое значение
    }
    if ((empty($arResult['DATE_START'])) || (empty($arResult['DATE_END']))){
        $arResult['NO_PARAMS'] = true;
    }
    $arResult['NEED_TO_GENERATE'] = true;
}
// --------------------- закончили с инициализацией фильтра


if ($arResult['ACTION'] == "ajaxreport"){
// Если не установлены параметры, то ничего не считаем
    if (!$arResult['NO_PARAMS']) {
// Ищем ID инфоблока по коду
        $ibDB = CIBLock::GetList(array(), array('CODE' => WORKTIME_IBLOCK_CODE));
        $tmpIB = $ibDB->fetch();
        $IB_ID = $tmpIB['ID'];

// Определяем фильтр по подразделению
        if (!empty($department)) {
            $dbDepUsers = CIntranetUtils::GetDepartmentEmployees($department, true, false, false);
            while ($arDepUsers_tmp = $dbDepUsers->fetch()) {
                $arDepUsers[] = $arDepUsers_tmp['ID'];
            }
        } else {
            $arDepUsers = array();
        }
// Формируем ограничения на основании подразделения к которому принадлежит пользователь
// Если пользователь относится к группе "Администраторы контроля посещаемости", то фильтровать не будем
// Иначе добавляем в фильтр самого пользователя и всех его подчиненных
        $isABSadmin = isAbsenceAdmin($USER->GetID());
        if (!$isABSadmin) {
            $arUserFilter = GetSubordinateEmployees_ex($USER->GetID(), CHECKTYPE);
        } else{
            $arUserFilter = false; // НЕ фильтруем пользователей
        }
// **********************************
// Выбираем всех активных пользователей из системы
// Создаем массив, в котором индексом будет ID пользователя
// В массив помещаем только пользователей, соответствующих фильтру
        $db_list = CUser::GetList(($by=""), ($order="asc"), array('ACTIVE'=>'Y'));
        while($ar_list=$db_list->fetch()) {
            $locuserID = $ar_list['ID'];
            if (((empty($arDepUsers)) || (in_array($locuserID, $arDepUsers))) && (($isABSadmin) || in_array($locuserID, $arUserFilter))) {
                $arResult['USERS'][$locuserID]['NAME'] = $ar_list['LAST_NAME'] . " " . $ar_list['NAME'];
                $arResult['USERS'][$locuserID]['WORK_POSITION'] = $ar_list['WORK_POSITION']; // Должность
// Получаем подразделение
                $ar_depts = CIntranetUtils::GetUserDepartments($locuserID);
                if (!empty($ar_depts)) {
// Получим информацию о первом подразделении (если у пользователя больше 1-го подразделения, то они не будут обработаны
                    $depDate = CIntranetUtils::GetDepartmentsData(array($ar_depts[0]));
                    $arResult['USERS'][$locuserID]['DEPARTMENT'] = $depDate[$ar_depts[0]];
// Получаем руководителя подразделения, в котором работает сотрудник
                    $ar_manager = GetUserManager($locuserID);
                    if ($ar_manager !== false) {
                        $arResult['USERS'][$locuserID]['MANAGER_ID'] = $ar_manager['ID'];
                        $arResult['USERS'][$locuserID]['MANAGER'] = $ar_manager['LAST_NAME'] . " " . $ar_manager['NAME'];
                    }
                }
            }
        }
// Теперь выбираем все элементы инфоблока worktime и подсчитываем рабочее время каждого сотружника
// Получаем список элементов инфоблока
        $arFilter['IBLOCK_ID'] = $IB_ID;
        $arFilter['>=DATE_ACTIVE_FROM'] = $date_start . " 00:00:00";
        $arFilter['<=DATE_ACTIVE_TO'] = $date_end . " 23:59:59";
        $db_list = CIBlockElement::GetList(array(), $arFilter, false, false, array('IBLOCK_ID', 'ID', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', 'PROPERTY_USER'));
        while($ar_el=$db_list->fetch()){
            $locuserID = $ar_el['PROPERTY_USER_VALUE'];
            if (((empty($arDepUsers)) || (in_array($locuserID, $arDepUsers))) && (($isABSadmin) || in_array($locuserID, $arUserFilter))) {
                $ts = MakeTimeStamp($ar_el['DATE_ACTIVE_FROM']);
                $te = MakeTimeStamp($ar_el['DATE_ACTIVE_TO']);
                $time = ($te - $ts) / 3600;
                if (empty($arResult['USERS'][$locuserID]['WORKTIME'])) {
                    $arResult['USERS'][$locuserID]['WORKTIME'] = $time;
                } else {
                    $arResult['USERS'][$locuserID]['WORKTIME'] += $time;
                }
            }
        }
        uasort($arResult['USERS'], "uasort_cmp");
    }
}
$this->IncludeComponentTemplate();

function uasort_cmp($a, $b){
    if ($a['NAME'] < $b['NAME']) return -1;
    if ($a['NAME'] > $b['NAME']) return 1;
    if ($a['NAME'] == $b['NAME']) return 0;
}