<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 10.11.2015
 * Time: 12:31
 */
?>
<?
//$APPLICATION->AddHeadScript('/local/components/synergy/synergy.absence.report.violations/templates/.default/jquery.tablesorter.js');
//$APPLICATION->AddHeadScript('/local/components/synergy/synergy.absence.report.violations/templates/.default/jquery.tablesorter.pager.js');
$APPLICATION->AddHeadScript('/local/components/synergy/synergy.absence.report.violations/templates/.default/jquery.dataTables.js');
$APPLICATION->AddHeadString('<link href="/local/components/synergy/synergy.absence.report.violations/templates/.default/jquery.tablesorter.pager.css">');


//echo "DATE_START: ".$arResult['DATE_START']."<br>";
//echo "DATE_END: ".$arResult['DATE_END']."<br>";
//echo "DEPARTMENT: ".$arResult['DEPARTMENT']."<br>";
if ($arResult['NEED_TO_GENERATE']){?>
<div class="report-params clearfix">
    <div style="float: left; margin-top: 4px; width: 390px;">
        <p style="display: block; float: left; margin: 3px 5px 0 0">Диапазон отчета:</p>
        <?
        $APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
                "SHOW_INPUT" => "Y",
                "FORM_NAME" => "",
                "INPUT_NAME" => "date_start",
                "INPUT_NAME_FINISH" => "date_end",
                "INPUT_VALUE" => $arResult['DATE_START'],
                "INPUT_VALUE_FINISH" => $arResult['DATE_END'],
                "SHOW_TIME" => "N",
                "HIDE_TIMEBAR" => "Y"
            )
        );?>
    </div>
    <div id="department-filter-div" style="float: left; margin-top: 4px; width: 320px;">
        <p style="display: block; float: left; margin: 3px 5px 0 0">Подразделение:</p>
        <?
        $arUserFields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', 0, LANGUAGE_ID);
        $arDeptField = $arUserFields['UF_DEPARTMENT'];
        $arDeptField['FIELD_NAME'] = 'department';
        $arDeptField['MULTIPLE'] = 'N';
        $arDeptField['SETTINGS']['LIST_HEIGHT'] = 1;
        $arDeptField["ENTITY_VALUE_ID"] = 20;
        $arDeptField["VALUE"][] = $arResult['DEPARTMENT'];
        CIntranetUtils::ShowDepartmentFilter($arDeptField, false);
        ?>
    </div>
    <div style="float: left;">
        <span>
            <a class="generate-href" href="javascript:void(0)" onclick="GenerateReport()">Сформировать</a>
        </span>

    </div>
</div>
<div class="report-table-div clearfix">
    <?
    if ($arResult['NO_PARAMS']) {
        $WarningText = "Не выбраны параметры отчета...";
        $color = "#fb5555";
    } else {
        $WarningText = "Для генерации отчета нажмите кнопку 'Сформировать'";
        $color = "#688b09";
    }?>
        <p style="color:<?=$color?>" class="no-data"><?=$WarningText?></p>
</div>
<?}
if ($arResult['ACTION'] == "ajaxreport"){
    if ($arResult['NO_PARAMS']) {
        echo "<p style='color: #fb5555' class='no-data'>Не выбраны параметры отчета...</p>";
        return;
    }
    ?>

    <table class="report-table">
        <thead>
        <tr>
            <th class="report-table-th col-1">Сотрудник</th>
            <th class="report-table-th col-2">Подразделение</th>
            <th class="report-table-th col-3">Должность</th>
            <th class="report-table-th col-7">Отработано (ч.)</th>
            <th class="report-table-th col-8">Руководитель</th>
        </tr>
        </thead>
        <tbody>
        <?

        foreach($arResult['USERS'] as $key=>$val){
            ?>
            <tr>
                <td class="report-table-td col-1"><?
//                    $val['NAME']
                    $APPLICATION->IncludeComponent("bitrix:main.user.link",
                        '',
                        array(
                            "ID" => $key,
                            "USE_THUMBNAIL_LIST" => "N",
                            "NAME_TEMPLATE" => "#LAST_NAME# #NAME#",
                        ),
                        false,
                        array("HIDE_ICONS" => "Y")
                    );?></td>
                <td class="report-table-td col-2"><?=$val['DEPARTMENT']?></td>
                <td class="report-table-td col-3"><?=$val['WORK_POSITION']?></td>
                <td class="report-table-td col-4"><?=($val['WORKTIME'] != 0) ? number_format($val['WORKTIME'], 2) : ''?></td>
                <td class="report-table-td col-5"><?
//                    $val['MANAGER']
                    if (!empty($val['MANAGER_ID'])) {
                        $APPLICATION->IncludeComponent("bitrix:main.user.link",
                            '',
                            array(
                                "ID" => $val['MANAGER_ID'],
                                "USE_THUMBNAIL_LIST" => "N",
                                "NAME_TEMPLATE" => "#LAST_NAME# #NAME#",
                            ),
                            false,
                            array("HIDE_ICONS" => "Y")
                        );
                    }
                    ?></td>
            </tr>
        <?}
        ?>
        </tbody>
    </table>
<!--        <div id="pager" class="pager" style="position: static" onclick="alert('!!!');">-->
<!--            <img src="/local/components/synergy/synergy.absence.report.violations/templates/.default/images/first.png" class="first"-->
<!--                onclick="ChangeApproveStyle()"/>-->
<!--            <img src="/local/components/synergy/synergy.absence.report.violations/templates/.default/images/prev.png" class="prev"/>-->
<!--            <span class="pagedisplay"></span>-->
<!--            <img src="/local/components/synergy/synergy.absence.report.violations/templates/.default/images/next.png" class="next"/>-->
<!--            <img src="/local/components/synergy/synergy.absence.report.violations/templates/.default/images/last.png" class="last"/>-->
<!--            <span class="lbl">Выводить по:</span>-->
<!--            <select class="pagesize">-->
<!--                <option value="10">10</option>-->
<!--                <option value="20">20</option>-->
<!--                <option selected="selected" value="30">30</option>-->
<!--                <option value="40">40</option>-->
<!--                <option value="50">40</option>-->
<!--            </select>-->
<!--            <span class="lbl" style="margin-left: 0">строк</span>-->
<!--        </div>-->
<?}?>