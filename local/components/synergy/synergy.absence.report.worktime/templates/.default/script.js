function GenerateReport(){
    BX.showWait();
    var ms = new Date();
    //$('.report-table-div').load("/local/components/synergy/synergy.absence.report.violations/ajax.php?id=" + ms.getTime() +
    //"&action=ajaxreport" + "&datestart=" + $('#date_start').val() + "&dateend=" + $('#date_end').val() +
    //"&department=" + $("select[name='department']").val());

    var ajaxRes;
        ajaxRes = $.ajax({
            type: "GET",
            url: "/local/components/synergy/synergy.absence.report.worktime/ajax.php?id=" + ms.getTime() +
            "&action=ajaxreport" + "&datestart=" + $('#date_start').val() + "&dateend=" + $('#date_end').val() +
            "&department=" + $("select[name='department']").val(),
            success: function (prmData) {
                $('.report-table-div').html(ajaxRes.responseText);
// Остальные действия выполняем только если вернули данные (иначе pager ошибку выдает, с которой не хочется разбираться)
                if (($('.report-table tbody tr').size()) > 1){
                    $('.bx-user-info-name').attr('target', 'blank');
                    $('.report-table').DataTable({
                        "language": {
                            "paginate": {
                                "previous": "Назад",
                                "next": "Вперед"
                            },
                            "search": "Поиск:",
                            "zeroRecords": "Нет сотрудников, соответствующих фильтру"
                        },
                        "pageLength": 30,
                        "info": false,
                        "lengthChange": false,
                        "autoWidth": false,
                        "orderMulti": true,
                        "ordering": true,
                        "order": [[ 4, 'asc' ], [ 0, 'asc' ]],
                    });
                    $('.dataTables_filter label input').focus();
                }
                BX.closeWait();
            }
        });
    return false;
}

