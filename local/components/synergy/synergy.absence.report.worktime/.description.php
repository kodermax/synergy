<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 11.12.2015
 * Time: 15:57
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Synergy: Отчет по отработке",
    "DESCRIPTION" => 'Отчет по отработанному времени',
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "intranet",
        "NAME" => "Корпоративный портал",
        "CHILD" => array(
            "ID" => "timeman",
            "NAME" => "Учет рабочего времени",
        ),
    ),
);