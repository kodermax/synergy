<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<script>
var devides=<? echo json_encode($arResult["DEVIDES"]);?>;
$(document).ready(
    function()
    {
        $("#period").change(
            function()
            {
                if($("#period").val()=="interval")
                    $("#interval").css('display', 'inline-block');
                else
                    $("#interval").hide();

                var max_devide=1;
                 switch($("#period").val())
                {
                    case "cmonth":
                    case "pmonth":
                        max_devide=2;
                        break;
                    case "cyear":
                    case "pyear":
                    case "interval":
                        max_devide=3;
                        break;
                }

                $("#devide").empty();
                for(var i=0; i<=max_devide; i++)
                {
                    $("#devide").append("<option value=\""+i+"\">"+devides[i]+"</option>");
                }


            }
        );

    }

);
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1" id="form1">
        <input type="hidden" name="mode" value="tbl">
        <div class="form-element">
            <label for="period">Период:</label>
            <select id="period" name="period">
                <? foreach($arResult["PERIODS"] as $k=>$v):?>
                    <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                <? endforeach; ?>
            </select>
        <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "N",
     "HIDE_TIMEBAR" => "Y"
    )
);?>
        </div>
        </div>
         <div class="form-element">
            <label for="devide">Разбивка:</label>
            <select id="devide" name="devide">

            <?
             $max_devide=1;
            switch($arResult["PERIOD"])
            {
                case "cmonth":
                case "pmonth":
                    $max_devide=2;
                    break;
                case "cyear":
                case "pyear":
                case "interval":
                    $max_devide=3;
                    break;
            }
            ?>
                <? for($arDiv=0;$arDiv<=$max_devide; $arDiv++):?>
                    <option value="<?=$arDiv?>"<?=($arResult["DEVIDE"]==$arDiv)?" selected":""?>><?=$arResult["DEVIDES"][$arDiv]?></option>
                <? endfor; ?>
            </select>
        </div>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Отчет по источникам (лендам)</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="period" type="hidden" value="<?=$arResult["PERIOD"]?>">
        <input name="devide" type="hidden" value="<?=$arResult["DEVIDE"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? endif;?>
    <? foreach($arResult["RESULT"] as $arDate=>$arData):?>
    <h3><?=$arDate?></h3>
    <table class="table-result">
		<tr>
		    <th>№</th>
			<th>Ленд</th>
			<? foreach($arResult["DEPTS"] as $dept):?>
			<th><?=$dept?></th>
			<? endforeach; ?>
			<? foreach($arResult["REGIONS"] as $regionID=>$region):?>
			<th><?=$region?></th>
			<? if($regionID==0):?>
			<th><?=$region?>%</th>
			<? endif; ?>
			<? endforeach; ?>
			<th>Итого</th>
		</tr>
		<? ksort($arData); ?>
		<? $arSum["DEPT"]=[];?>
		<? $arSum["REGION"]=[];?>
		<? $iN=1;?>
		<? foreach($arData as $arLend=>$arItem):?>
            <tr>
                <td align="right"><?=$iN?></td>
                <td style="text-align: left;"><?=($arLend=="")?"-- нет --":$arLend?></td>
                <? foreach($arResult["DEPTS"] as $deptID=>$deptName):?>
                    <? $cnt=intval($arItem["DEPT"][$deptID]); ?>
                    <? $arSum["DEPT"][$deptID]+=$cnt ?>
                    <td><?=$cnt?></td>
                <? endforeach; ?>
                <? $sumAll = 0;?>
                <? foreach($arItem["REGION"] as $cnt) $sumAll+=$cnt;?>
                <? foreach($arResult["REGIONS"] as $regionID=>$regionName):?>
                    <? $cnt=intval($arItem["REGION"][$regionName]); ?>
                    <? $arSum["REGION"][$regionName]+=$cnt ?>
                    <td><?=$cnt?></td>
                    <? if($regionID==0):?>
                    <td><?=($sumAll!=0)?number_format(round(($cnt/$sumAll)*100,2,PHP_ROUND_HALF_EVEN), 2, ',', ' '):0?></td>
                    <? endif; ?>
                <? endforeach; ?>
                 <td><?=$sumAll?></td>
            </tr>
            <? $iN++; ?>
        <? endforeach;?>
            <tr>
                <td align="right"><?=$iN?></td>
                <td style="text-align: left;"><b>Итого</b></td>
                <? foreach($arResult["DEPTS"] as $deptID=>$deptName):?>
                    <td><b><?=intval($arSum["DEPT"][$deptID])?></b></td>
                <? endforeach; ?>
                <? $sumAll = 0;?>
                <? foreach($arSum["REGION"] as $cnt) $sumAll+=$cnt;?>
                <? foreach($arResult["REGIONS"] as $regionID=>$regionName):?>
                    <td><b><?=intval($arSum["REGION"][$regionName])?></b></td>
                    <? if($regionID==0):?>
                    <td><b><?=($sumAll!=0)?number_format(round((intval($arSum["REGION"][$regionName])/$sumAll)*100,2,PHP_ROUND_HALF_EVEN), 2, ',', ' '):0?></b></td>
                    <? endif; ?>
                <? endforeach; ?>
                 <td><b><?=$sumAll?></b></td>
            </tr>
	</table>
	<? endforeach; ?>
</div>
<? elseif($arResult["MODE"]):?>
<div class="result">
<b>По Вашему запросу ничего не найдено</b>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>
