<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

function getDatesByWeek($_week_number, $_year = null)
{
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

CJSCore::Init('jquery');

$arResult["FROM"] = ((bool)strtotime($_REQUEST["from"])) ? $_REQUEST["from"] : date("d.m.Y", strtotime("yesterday"));
$arResult["TO"] = ((bool)strtotime($_REQUEST["to"])) ? $_REQUEST["to"] : date("d.m.Y");

$arResult["MODE"] = ($_REQUEST["mode"] == "tbl" || $_REQUEST["mode"] == "exl") ? $_REQUEST["mode"] : null;
$arResult["PERIOD"] = $_REQUEST["period"];
$arResult["DEVIDE"] = $_REQUEST["devide"];

$arResult["PERIODS"] = [
    "cday" => "Текущий день",
    "pday" => "Предыдущий день",
    "cweek" => "Текущая неделя",
    "pweek" => "Предыдущая неделя",
    "cmonth" => "Текущий месяц",
    "pmonth" => "Предыдущий месяц",
    "cyear" => "Текущий год",
    "pyear" => "Предыдущий год",
    "interval" => "Интервал"
];

$arResult["DEVIDES"] = [
    "0" => "За весь период",
    "1" => "По дням",
    "2" => "По неделям",
    "3" => "По месяцам"
];


$arResult["DEPTS"] = [
    3445 => "ДС",
    3452 => "ОП1",
    3455 => "ОП2",
    3457 => "ОП3",
    3458 => "ОП4",
    3459 => "ОП5",
    3464 => "МАГ",
    3461 => "ВЧ",
    3463 =>"ЦЯП",
    4260 =>"ДП1",
    753=>"ДП2",
    4320 =>"ДП3",
    3454 =>"ОП10"
];


$arResult["DEPTS_ALL"] = [];

foreach($arResult["DEPTS"] as $deptID=>$deptName)
{
    $arResult["DEPTS_ALL"][$deptID][]=$deptID;

    $rsParentSection = CIBlockSection::GetByID($deptID);
    if ($arParentSection = $rsParentSection->GetNext())
    {
        $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
        $rsDept = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
        while ($arDept = $rsDept->GetNext())
        {
            if(!$arResult["DEPTS"][$arDept["ID"]])
                $arResult["DEPTS_ALL"][$deptID][]=$arDept["ID"];
        }
    }
}

$arResult["TOWNS"] = [
    "Апрелевка",
    "Балашиха",
    "Бронницы",
    "Верея",
    "Видное",
    "Волоколамск",
    "Воскресенск",
    "Высоковск",
    "Дедовск",
    "Дзержинский",
    "Дмитров",
    "Долгопрудный",
    "Дрезна",
    "Домодедово",
    "Дубна",
    "Егорьевск",
    "Железнодорожный",
    "Жуковский",
    "Зарайск",
    "Зеленоград",
    "Ивантеевка",
    "Истра",
    "Кашира",
    "Климовск",
    "Клин",
    "Коломна",
    "Королев",
    "Котельники",
    "Красноармейск",
    "Красногорск",
    "Краснозаводск",
    "Краснознаменск",
    "Куровское",
    "Ликино-Дулево",
    "Лобня",
    "Лосино-Петровский",
    "Луховицы",
    "Лыткарино",
    "Люберцы",
    "Можайск",
    "Москва",
    "Мытищи",
    "Наро-Фоминск",
    "Ногинск",
    "Одинцово",
    "Ожерелье",
    "Озеры",
    "Орехово-Зуево",
    "Павловский Посад",
    "Подольск",
    "Протвино",
    "Пушкино",
    "Пущино",
    "Раменское",
    "Реутов",
    "Руза",
    "Рошаль",
    "Сергиев Посад",
    "Серпухов",
    "Солнечногорск",
    "Ступино",
    "Сходня",
    "Талдом",
    "Троицк",
    "Фрязино",
    "Химки",
    "Хотьково",
    "Черноголовка",
    "Чехов",
    "Шатура",
    "Щербинка",
    "Щёлково",
    "Электрогорск",
    "Электросталь",
    "Электроугли",
    "Юбилейный",
    "Яхрома"
];

$arResult["REGIONS"]=[
    "МСК",
    "Регион"
];

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;



}

// UF_CRM_1417763397 - ленд из b_uts_crm_lead
// UF_CRM_1417767839 - регион

if ($arResult["MODE"])
{

    foreach($arResult["DEPTS"] as $deptID=>$deptName)
    {
        $rsUser= CUser::GetList($by='ID', $order='ASC', array('UF_DEPARTMENT' => $arResult["DEPTS_ALL"][$deptID]), array('SELECT' => array('UF_DEPARTMENT')));

        while($arUser=$rsUser->GetNext())
        {
            if(!$arResult["USERS"][$arUser["ID"]])
            {
                $arResult["USERS"][$arUser["ID"]]=$deptID;
            }
            $arResult["DEPT_USERS"][$deptID][$arUser["ID"]]=true;
        }
    }

    $from=$DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to=$DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));
    $list_user=join(",",array_keys($arResult["USERS"]));
    if($list_user=="")
        $list_user="-1";
    $groupby="";
    $select="";

    switch($arResult["DEVIDE"])
    {
        case 1:
            $groupby='`YEAR`,`MONTH`,`DAY`,';
            $select="DAYOFMONTH(`crm_lead`.DATE_CREATE) as `DAY`,
        MONTH(`crm_lead`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
        case 2:
            $groupby='`YEAR`,`WEEK`,';
            $select="YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,
        WEEK(`crm_lead`.DATE_CREATE) as `WEEK`,";
            break;
        case 3:
            $groupby='`YEAR`,`MONTH`,';
            $select="MONTH(`crm_lead`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
        case 4:
            $groupby='`YEAR`,';
            $select="YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
    }

    $sql="
      select
        $select
        `uts`.UF_CRM_1417763397 AS `LEND`,
        `crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
        COUNT(`crm_lead`.ID) as CNT
      from
        `b_crm_lead` `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to' AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user) AND `uts`.UF_CRM_1417763397!='' AND `uts`.UF_CRM_1417763397 is not NULL
      GROUP BY $groupby `LEND`,`ASSIGNED_BY_ID`
      ORDER BY `crm_lead`.DATE_CREATE, `uts`.UF_CRM_1417763397, `crm_lead`.`ASSIGNED_BY_ID`
    ";

    //echo $sql."<br>";

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $dept=$arResult["USERS"][$row["ASSIGNED_BY_ID"]];
        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }
        if(!$arResult["RESULT"][$date][$row["LEND"]]["DEPT"][$dept])
            $arResult["RESULT"][$date][$row["LEND"]]["DEPT"][$dept]=0;
        $arResult["RESULT"][$date][$row["LEND"]]["DEPT"][$dept]+=$row["CNT"];

    }

    $sql="
      select
        $select
        `uts`.UF_CRM_1417763397 as `LEND`,
        `uts`.`UF_CRM_1417767839` AS `REGION`,
        COUNT(`crm_lead`.ID) as CNT
      from
        `b_crm_lead` `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'  AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user) AND `uts`.UF_CRM_1417763397!='' AND `uts`.UF_CRM_1417763397 is not NULL
      GROUP BY $groupby `LEND`,`REGION`
      ORDER BY `crm_lead`.DATE_CREATE,`uts`.UF_CRM_1417763397,`uts`.`UF_CRM_1417767839`
    ";

    //echo $sql."<br>";

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        if(in_array($row["REGION"],$arResult["TOWNS"]))
        {
            $region=$arResult["REGIONS"][0];
        }
        else
        {
            $region=$arResult["REGIONS"][1];
        }

        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }
        if(!$arResult["RESULT"][$date][$row["LEND"]]["REGION"][$region])
            $arResult["RESULT"][$date][$row["LEND"]]["REGION"][$region]=0;
        $arResult["RESULT"][$date][$row["LEND"]]["REGION"][$region]+=$row["CNT"];

    }

}
$this->IncludeComponentTemplate();
?>