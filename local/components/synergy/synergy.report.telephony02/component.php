<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/duration.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/telephony/mssql_connect.php");

global $APPLICATION, $USER;

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

CJSCore::Init('jquery');

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y 00:00:00");
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y 23:59:59");

$arResult["GROUP_LIST"]=[];

$sql="SELECT dbo.tGroups.ID as GId,
	dbo.tGroups.Name as GName,
	dbo.tUsers.Login,
	dbo.tUsers.ID,
	dbo.tUsers.Name_F,
	dbo.tUsers.Name_I,
	dbo.tUsers.Deleted,
	dbo.tUsers.IsSystem
FROM dbo.tUsers LEFT JOIN dbo.tGroupsLinks ON dbo.tUsers.ID = dbo.tGroupsLinks.IDUser
	 LEFT JOIN dbo.tGroups ON dbo.tGroupsLinks.IDGroup = dbo.tGroups.ID
WHERE dbo.tUsers.IsSystem = 0 AND dbo.tUsers.Deleted = 0
ORDER BY dbo.tGroups.Name ASC, dbo.tUsers.Name_F ASC, dbo.tUsers.Name_I ASC";

$res = mssql_query($sql);
while ($row = mssql_fetch_assoc($res)) {
    if($row["GId"]!="")
    {
        $arResult["GROUP_LIST"][$row["GId"]]["NAME"]=$row["GName"];
        $arResult["GROUP_LIST"][$row["GId"]]["USERS"][$row["ID"]]=$row["Name_F"]." ".$row["Name_I"];
    }

    if(trim($row["Name_F"])!="" && trim($row["Name_I"])!="")
    {
        $arResult["GROUP_USER_LIST"][$row["ID"]]=[
            "NAME"=>$row["Name_F"]." ".$row["Name_I"],
            "ID"=>$row["ID"]
        ];
    }


    $arResult["USER_LIST"][$row["ID"]]=$row["Name_F"]." ".$row["Name_I"];
}

usort ($arResult["GROUP_USER_LIST"], function($a, $b) {
    return strcmp($a["NAME"], $b["NAME"]);
});

$arResult["OPERATOR"]=(is_array($_REQUEST["operator"]))?$_REQUEST["operator"]:[];
$arResult["LIMIT"]=100;
$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PAGE"]=($_REQUEST["page"])?$_REQUEST["page"]:1;

if($arResult["MODE"] && !$arResult["ERROR"])
{
    $start=time();
    $from=preg_replace("/^(\d\d)\.(\d\d)\./","$2/$1/",$arResult["FROM"]);
    $to=preg_replace("/^(\d\d)\.(\d\d)\./","$2/$1/",$arResult["TO"]);

    $listID=join(" ",$arResult["OPERATOR"]);


    $sql="SELECT * FROM [dbo].[report_kd_common_v3] ('{$from}', '{$to}', '{$listID}', '{$USER->GetLogin()}')";


    $sql_count=str_replace("*","count(*) as cnt",$sql);

    //AddMessage2Log($sql_count);
    /*$res=mssql_query($sql_count);
    if($row=mssql_fetch_assoc($res))
    {
        $arResult["ROWCOUNT"]=$row["cnt"];
    }

    $sql=str_replace("[WHAT]",$what_all,$sql);

    $sql.=" ORDER BY TimeStart DESC";
    if($arResult["PAGE"]>0)
    {
        $sql.=" OFFSET ".(($arResult["PAGE"]-1)*$arResult["LIMIT"])." ROWS FETCH NEXT ".$arResult["LIMIT"]." ROWS ONLY";
    }
    */

    //AddMessage2Log($sql);
    $res=mssql_query($sql);
    if (!$res) {
        $arResult["ERROR"][]=mssql_get_last_message();
    }
    else
    {
        while(($row=mssql_fetch_assoc($res)))
        {
            //$row["userName"]
            //$row["userID"]
            //$row["userPhone"]
            $row["durationCall"]=day2duration($row["durationCall"]);
            //$row["countCall"]
            //$row["countOutgoingCall"]
            $row["durationOutgoingCall"]=day2duration($row["durationOutgoingCall"]);
            //$row["countIncomingCall"]
            $row["durationIncomingCall"]=day2duration($row["durationIncomingCall"]);
            //$row["countDispatchCall"]
            $row["durationDispatchCall"]=day2duration($row["durationDispatchCall"]);
            $row["averageDuration"]=day2duration($row["averageDuration"]);
            $row["averageIncomingDuration"]=day2duration($row["averageIncomingDuration"]);
            $row["averageOutgoingDuration"]=day2duration($row["averageOutgoingDuration"]);
            //$row["countFailCalls"]
            //$row["countAllCalls"]

            $arResult["RESULT"][]=$row;
        }
    }

    //AddMessage2Log(print_r($arResult, TRUE));
    $arResult["TIME"]=time()-$start;
}


$this->IncludeComponentTemplate();
?>