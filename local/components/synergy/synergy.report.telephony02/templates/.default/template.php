<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<script>
    var componentPath = "<?=$componentPath?>";
    $(document).ready(

        function()
        {
            $("a[data-name='pager']").click(
                function()
                {
                    $('#page').val($(this).data("key"));
                    $('#form1').submit();
                    return false;
                }
            );

            $('#h_scroll').height($(window).height()-50);

		            var wdthAll = $(".table-result").width();
		            var wdth = $('#h_scroll_content').width();
		            if(wdthAll>wdth) {
			            var b_scroll = $('<div style="overflow-x:scroll;width:'+wdth+'px"/>');
			            var b_cont = $('<div style="height:1px"></div>').width(wdthAll);
			            b_scroll.append(b_cont);
			            b_scroll.scroll(function(){
				            $('#h_scroll').scrollLeft(b_scroll.scrollLeft());
			            });
			            $('#h_scroll').before(b_scroll);
			            $('#h_scroll').scroll(function(){
				            b_scroll.scrollLeft($('#h_scroll').scrollLeft());
			            });
		            }

		     $(window).resize(function()
		     {
               $('#h_scroll').height($(window).height()-50);

             });

        }

    );
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1" id="form1">
        <input type="hidden" name="mode" value="tbl">
        <input type="hidden" name="page" value="1" id="page">
        <div class="form-element">
            <label for="from">Период:</label>
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "Y",
     "HIDE_TIMEBAR" => "N"
    )
);?>

        </div>
        <div class="form-element">
            <label for='operator[]'>Оператор:</label>
            <select id='operator[]' name='operator[]' multiple size="20">
              <? foreach ($arResult["GROUP_LIST"] as $arGId=>$arGroup):?>
                  <? $sel=in_array($arGId,$arResult["OPERATOR"])?" selected":""; ?>
                  <option value="<?=$arGId?>" style="font-weight: bold; font-size: 12px;" <?=$sel?>><?=$arGroup["NAME"]?></option>
              <? endforeach;?>
              <? foreach ($arResult["GROUP_USER_LIST"] as $arUser):?>
                    <? $sel=in_array($arUser["ID"],$arResult["OPERATOR"])?" selected":""; ?>
                    <option value="<?=$arUser["ID"]?>"<?=$sel?>>&nbsp;&nbsp;&nbsp;<?=$arUser["NAME"]?></option>
                <? endforeach; ?>
            </select>
        </div>
        <br>
        <br>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Сводный отчет</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <? foreach($arResult["OPERATOR"] as  $oper):?>
        <input name="operator[]" type="hidden" value="<?=$oper?>">
        <? endforeach;?>
        <input name="page" type="hidden" value="-1">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <div id="h_scroll"  style="overflow:scroll;">
	<div id="h_scroll_content">
    <? endif;?>
    <table class="table-result">
		<tr>
		    <th>ФИО</th>
		    <th>Телефон</th>
		    <th>Кол-во всех</th>
		    <th>Кол-во недозвонов</th>
		    <th>Кол-во успешных</th>
		    <th>Длительность всех</th>
		    <th>Количество исходящих</th>
		    <th>Длительность исходящих</th>
		    <th>Количество входящих</th>
		    <th>Длительность входящих</th>
		    <th>Количество переключений</th>
		    <th>Длительность переключений</th>
		    <th>Сред. время всех</th>
		    <th>Сред. время входящих</th>
		    <th>Сред. время исходящих</th>
        </tr>
        <? foreach($arResult["RESULT"] as $arItem):?>
        <tr data-key="<?=$arItem["userID"]?>">
            <td><?=$arItem["userName"]?></td>
            <td><?=$arItem["userPhone"]?></td>
            <td><?=$arItem["countAllCalls"]?></td>
            <td><?=$arItem["countFailCalls"]?></td>
            <td><?=$arItem["countCall"]?></td>
            <td><?=$arItem["durationCall"]?></td>
            <td><?=$arItem["countOutgoingCall"]?></td>
            <td><?=$arItem["durationOutgoingCall"]?></td>
            <td><?=$arItem["countIncomingCall"]?></td>
            <td><?=$arItem["durationIncomingCall"]?></td>
            <td><?=$arItem["countDispatchCall"]?></td>
            <td><?=$arItem["durationDispatchCall"]?></td>
            <td><?=$arItem["averageDuration"]?></td>
            <td><?=$arItem["averageIncomingDuration"]?></td>
            <td><?=$arItem["averageOutgoingDuration"]?></td>
        </tr>
        <?  //AddMessage2Log(print_r( $arItem , TRUE)); ?>
        <? endforeach;?>
	</table>
	<div>Время формирования отчета: <?=$arResult["TIME"]?>с</div>
	<? if($arResult["MODE"]!="exl"):?>
	</div>
	</div>
    <? endif;?>
</div>
<? elseif($arResult["MODE"] && !$arResult["ERROR"]):?>
<div class="result">
<b>По Вашему запросу ничего не найдено</b>
</div>
<? else:?>
<div class="error">
<? foreach ($arResult["ERROR"] as $error):?>
<b><?=$error;?></b>
<? endforeach; ?>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>