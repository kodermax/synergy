<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<pre>
<? //var_dump($arResult["RESULT"])?>
</pre>
<script>
var devides=<? echo json_encode($arResult["DEVIDES"]);?>;
$(document).ready(
    function()
    {
        $("#period").change(
            function()
            {
                if($("#period").val()=="interval")
                    $("#interval").css('display', 'inline-block');
                else
                    $("#interval").hide();

                var max_devide=1;
                 switch($("#period").val())
                {
                    case "cmonth":
                    case "pmonth":
                        max_devide=2;
                        break;
                    case "cyear":
                    case "pyear":
                    case "interval":
                        max_devide=3;
                        break;
                }

                $("#devide").empty();
                for(var i=0; i<=max_devide; i++)
                {
                    $("#devide").append("<option value=\""+i+"\">"+devides[i]+"</option>");
                }
            }
        );
    }

);
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1">
        <input type="hidden" name="mode" value="tbl">
        <div class="form-element">
            <label for="period">Период:</label>
            <select id="period" name="period">
                <? foreach($arResult["PERIODS"] as $k=>$v):?>
                    <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                <? endforeach; ?>
            </select>
        <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "N",
     "HIDE_TIMEBAR" => "Y"
    )
);?>
        </div>
        </div>
         <div class="form-element">
            <label for="devide">Разбивка:</label>
            <select id="devide" name="devide">

            <?
             $max_devide=1;
            switch($arResult["PERIOD"])
            {
                case "cmonth":
                case "pmonth":
                    $max_devide=2;
                    break;
                case "cyear":
                case "pyear":
                case "interval":
                    $max_devide=3;
                    break;
            }
            ?>
                <? for($arDiv=0;$arDiv<=$max_devide; $arDiv++):?>
                    <option value="<?=$arDiv?>"<?=($arResult["DEVIDE"]==$arDiv)?" selected":""?>><?=$arResult["DEVIDES"][$arDiv]?></option>
                <? endfor; ?>
            </select>
        </div>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Ежедневный отчет по заявкам</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="period" type="hidden" value="<?=$arResult["PERIOD"]?>">
        <input name="devide" type="hidden" value="<?=$arResult["DEVIDE"]?>">
        <input name="dept" type="hidden" value="<?=$arResult["DEPT"]?>">
        <input name="rec" type="hidden" value="<?=$arResult["REC"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? endif;?>
    <? $arData=$arResult["RESULT"];?>
    <table class="table-result">
		<tr>
		    <th>№</th>
			<th>Категория</th>
    		<? foreach($arData as $arDate=>$arCat):?>
			<th><?=$arDate?></th>
			<? endforeach;?>
			<th>Итого</th>
		</tr>
		<tr style="background-color: #E6B8B7;">
		    <td style="text-align: center"><b>1</b></td>
		    <td style="text-align: left;"><b><i>Количество заявок</i></b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $arCat["_ALL"]=$arData[$arDate]["_ALL"]=intval($arCat["_OBJECT"]+$arCat["_NONOBJECT"]);?>
            <? $itogo+=$arCat["_ALL"];?>
			<td><?=intval($arCat["_ALL"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr>
		    <td style="text-align: center">1.1</td>
		    <td style="text-align: left;">Количество целевых заявок</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_OBJECT"]);?>
			<td><?=intval($arCat["_OBJECT"])?></td>
			<? endforeach;?>
			<? $itogoObj=$itogo;?>
			<td><?=$itogo?></td>
        </tr>
        <tr>
		    <td style="text-align: center">1.2</td>
		    <td style="text-align: left;">Количество нецелевых заявок</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_NONOBJECT"]);?>
			<td><?=intval($arCat["_NONOBJECT"])?></td>
			<? endforeach;?>
			<? $itogoNonObj=$itogo;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #E6B8B7;">
		    <td style="text-align: center"><b>2</b></td>
		    <td style="text-align: left;"><b><i>Количество заявок бракованных</i></b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $allS=0; ?>
            <? foreach($arResult["JUNK"] as $arJunkID=>$arJunkName) $allS+=intval($arCat[$arJunkID]);?>
            <? $arData[$arDate]["_JUNK"]=intval($allS); ?>
            <? $itogo+=intval($allS);?>
			<td><?=$allS?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <? $arI=1; ?>
        <? foreach($arResult["JUNK"] as $arJunkID=>$arJunkName):?>
        <tr>
		    <td style="text-align: center">2.<?=$arI?></td>
		    <td style="text-align: left;"><?=$arJunkName?></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat[$arJunkID]);?>
			<td><?=intval($arCat[$arJunkID])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <? $arI++; ?>
        <? endforeach;?>
        <tr style="background-color: #FABF8E;">
		    <td style="text-align: center">2.6</b></td>
		    <td style="text-align: left;"><i>Отношение к количеству целевых заявок в %</i></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $allS=0; ?>
            <? foreach($arResult["JUNK"] as $arJunkID=>$arJunkName) $allS+=intval($arCat[$arJunkID]);?>
            <? $itogo+=intval($allS);?>
			<td><i><?=(intval($arCat["_OBJECT"])==0)?0:ceil(intval($allS)*100/intval($arCat["_OBJECT"]))?></i></td>
			<? endforeach;?>
			<td><i><?=(intval($itogoObj)==0)?0:ceil(intval($itogo)*100/intval($itogoObj))?></i></td>
        </tr>
        <tr style="background-color: #E6B8B7;">
		    <td style="text-align: center"><b>3</b></td>
		    <td style="text-align: left;"><b><i>Количество заявок тестовых</i></b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["11"]);?>
			<td><?=intval($arCat["11"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #E6B8B7;">
		    <td style="text-align: center"><b>4</b></td>
		    <td style="text-align: left;"><b><i>Количество заявок, переданных в другие подразделения</i></b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["GONE"]);?>
			<td><?=intval($arCat["GONE"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #FABF8E;">
		    <td style="text-align: center">4.1</b></td>
		    <td style="text-align: left;"><i>Отношение к общему количеству заявок в %</i></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["GONE"]);?>
			<td><i><?=(intval($arCat["_OBJECT"])==0 && intval($arCat["_NONOBJECT"])==0)?0:ceil(intval($arCat["GONE"])*100/intval($arCat["_OBJECT"]+$arCat["_NONOBJECT"]))?></i></td>
			<? endforeach;?>
			<td><i><?=(intval($itogoObj)==0 && intval($itogoNonObj)==0 )?0:ceil(intval($itogo)*100/intval($itogoObj+$itogoNonObj))?></i></td>
        </tr>
        <tr style="background-color: #E6B8B7;">
		    <td style="text-align: center"><b>5</b></td>
		    <td style="text-align: left;"><b><i>Количество заявок, принятых в работу</i></b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["_OBJECT"])-intval($arCat["_JUNK"])-intval($arCat["GONE"]);?>
			<td><?=(intval($arCat["_OBJECT"])-intval($arCat["_JUNK"])-intval($arCat["GONE"]))?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <tr style="background-color: #DCE6F1;">
		    <td style="text-align: center"><b>*</b></td>
		    <td style="text-align: left;"><b><i>Не обработаны менеджером</i></b></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat["NEW"]);?>
			<td><?=intval($arCat["NEW"])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <? $arI=1; ?>
        <? foreach($arResult["WORK"] as $arWorkID=>$arWorkName):?>
        <tr>
		    <td style="text-align: center">5.<?=$arI?></td>
		    <td style="text-align: left;"><?=$arWorkName?></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat[$arWorkID]);?>
			<td><?=intval($arCat[$arWorkID])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <? $arI++; ?>
        <? endforeach;?>
        <tr>
		    <td style="text-align: center">5.4</td>
		    <td style="text-align: left;">Думающие</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
             <? $itogo+=intval($arCat["IN_PROCESS"])+intval($arCat["17"]);?>
			<td><?=(intval($arCat["IN_PROCESS"])+intval($arCat["17"]))?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
         <tr>
		    <td style="text-align: center">5.5</td>
		    <td style="text-align: left;">Отказ от поступления после консультации</td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $allS=0; ?>
            <? foreach($arResult["REJECT"] as $arRejectID=>$arRejectName) $allS+=intval($arCat[$arRejectID]);?>
            <? $itogo+=intval($allS);?>
			<td><?=$allS?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <? $arI=1; ?>
        <? foreach($arResult["REJECT"] as $arRejectID=>$arRejectName):?>
        <? if($arRejectID==2) continue; ?>
        <tr style="background-color: #EBF1DE;">
		    <td style="text-align: center">5.5.<?=$arI?></td>
		    <td style="text-align: left;"><?=$arRejectName?></td>
		    <? $itogo=0;?>
            <? foreach($arData as $arDate=>$arCat):?>
            <? $itogo+=intval($arCat[$arRejectID]);?>
			<td><?=intval($arCat[$arRejectID])?></td>
			<? endforeach;?>
			<td><?=$itogo?></td>
        </tr>
        <? $arI++; ?>
        <? endforeach;?>
	</table>
</div>
<? elseif($arResult["MODE"]):?>
<div class="result">
<b>По Вашему запросу ничего не найдено</b>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>