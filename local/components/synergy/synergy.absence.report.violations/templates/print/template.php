<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 10.11.2015
 * Time: 12:31
 */
?>

<div id="report-head">
    <h2>Отчетность по нарушениям</h2>
    <p>Начальная дата: <?=(!empty($arResult['DATE_START']) ? $arResult['DATE_START'] : "Не задана")?></p>
    <p>Конечная дата: <?=(!empty($arResult['DATE_END']) ? $arResult['DATE_END'] : "Не задана")?></p>
    <p>Подразделение: <?=$arResult['DEPARTMENT_NAME']?></p>
</div>
<?

if ($arResult['NO_PARAMS']) {
    echo "<p style='color: #fb5555' class='no-data'>Не выбраны параметры отчета...</p>";
    return;
}
?>

<table class="report-table" id="report-table-header">
    <thead>
    <tr>
        <th class="report-table-td-1">Сотрудник</th>
        <th class="report-table-td-2">Организация</th>
        <th class="report-table-td-3">Подразделение</th>
        <th class="report-table-td-4">Должность</th>
        <th class="report-table-td-5"><span class="vertcol">Опоздания</span></th>
        <th class="report-table-td-6"><span class="vertcol">Ранние уходы</span></th>
        <th class="report-table-td-7"><span class="vertcol">Отсутствия</span></th>
        <th class="report-table-td-8"><span class="vertcol">Итого</span></th>
        <th class="report-table-td-9">Руководитель</th>
    </tr>
    </thead>
</table>
    <?

    foreach ($arResult['USERS'] as $key => $val) {
        if ($val['TOTAL'] > 0) {
            ?>
            <table class="report-table">
                <tbody>
                <tr>
                    <td class="report-table-td-1"><?= $val['NAME'] ?></td>
                    <td class="report-table-td-2"><?=$val['WORK_COMPANY']?></td>
                    <td class="report-table-td-3"><?= $val['DEPARTMENT'] ?></td>
                    <td class="report-table-td-4"><?= $val['WORK_POSITION'] ?></td>
                    <td class="report-table-td-5 centercol"><?= $val['SYN_LATE'] ?></td>
                    <td class="report-table-td-6 centercol"><?= $val['SYN_LEAVING'] ?></td>
                    <td class="report-table-td-7 centercol"><?= $val['SYN_ABSENT'] ?></td>
                    <td class="report-table-td-8 centercol"><?= $val['TOTAL'] ?></td>
                    <td class="report-table-td-9"><?= $val['MANAGER'] ?></td>
                </tr>
                </tbody>
            </table>
        <? } ?>
    <?
    }
    ?>