<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 10.11.2015
 * Time: 12:31
 */
//print_r($arResult['MEMO_PARAMS']);
?>
<div class="report-wrapper">
<div id="report-head">
    <table class="table-header-wrapper">
        <tr>
            <td>
                <table class="table-header-left">
                    <tr>
                        <td class="col-1">
                            <b>Кому:</b>
                        </td>
                        <td>
                            <?=$arResult['MEMO_PARAMS']['MEMO_RECIPIENT_FIO_2']?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Дата:</b>
                        </td>
                        <td>
                            <?=$arResult['GENERATE_DATE']?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Тема:</b>
                        </td>
                        <td>
                            Нарушения трудовой дисциплины сотрудниками университета.
                        </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: top; padding-left: 70px;">
                <table>
                    <tr>
                        <td class="col-utv"><b>Утверждаю:<br><?=$arResult['MEMO_PARAMS']['MEMO_RECIPIENT_FIO']?></b></td>
                    </tr>
                    <tr>
                        <td class="col-utv">________________________</td>
                    </tr>
                    <tr>
                        <td class="col-utv">"___" ____________  <?=date('Y')." г."?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <h2>Служебная записка</h2>
    <p>По итогам мониторинга трудовой дисциплины в период с <?=$arResult['DATE_START']?> по <?=$arResult['DATE_END']?> у представленных сотрудников были выявлены нарушения трудовой дисциплины. Прошу Вас наложить соответствующие дисциплинарные взыскания:</p>
</div>

<?

if ($arResult['NO_PARAMS']) {
    echo "<p style='color: #fb5555' class='no-data'>Не выбраны параметры отчета...</p>";
    return;
}
?>

<table class="report-table" id="report-table-header">
    <thead>
    <tr>
        <th class="report-table-td-1">№ п/п</th>
        <th class="report-table-td-2">Сотрудник</th>
        <th class="report-table-td-3">Кол-во нарушений</th>
        <th class="report-table-td-4">% снижения доплаты за месяц</th>
    </tr>
    </thead>
</table>
    <?
    $i = 1;
    foreach ($arResult['USERS'] as $key => $val) {
        if ($val['TOTAL'] > 0) {
            ?>
            <table class="report-table">
                <tbody>
                <tr>
                    <td class="report-table-td-1"><?=$i?></td>
                    <td class="report-table-td-2"><?= $val['NAME'] ?></td>
                    <td class="report-table-td-3"><?=$val['TOTAL']?></td>
                    <td class="report-table-td-4">100%</td>
                </tr>
                </tbody>
            </table>
        <?} ?>
    <?
        $i++;
    }
    ?>
    <div class="report-footer">
        <table class="table-footer">
            <tr>
                <td class="left">
                    <b>С уважением,</b>
                </td>
                <tf></tf>
            </tr>
            <tr>
                <td class="left"><?=$arResult['MEMO_PARAMS']['MEMO_AUTHOR_POSITION']?></td>
                <td class="right">_________________<?=$arResult['MEMO_PARAMS']['MEMO_AUTHOR_FIO']?></td>
            </tr>
        </table>
        <table class="table-footer" style="margin-top: 20px;">
            <tr>
                <td class="left">
                    <b>СОГЛАСОВАНО:</b>
                </td>
                <tf></tf>
            </tr>
            <tr>
                <td class="left"><?=$arResult['MEMO_PARAMS']['MEMO_APPROVE_POSITION']?></td>
                <td class="right">_________________<?=$arResult['MEMO_PARAMS']['MEMO_APPROVE_FIO']?></td>
            </tr>
        </table>

    </div>

</div>