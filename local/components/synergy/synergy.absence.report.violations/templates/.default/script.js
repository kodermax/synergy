function GenerateReport(){
    BX.showWait();
    var ms = new Date();
    //$('.report-table-div').load("/local/components/synergy/synergy.absence.report.violations/ajax.php?id=" + ms.getTime() +
    //"&action=ajaxreport" + "&datestart=" + $('#date_start').val() + "&dateend=" + $('#date_end').val() +
    //"&department=" + $("select[name='department']").val());

    var ajaxRes;
        ajaxRes = $.ajax({
            type: "GET",
            url: "/local/components/synergy/synergy.absence.report.violations/ajax.php?id=" + ms.getTime() +
            "&action=ajaxreport" + "&datestart=" + $('#date_start').val() + "&dateend=" + $('#date_end').val() +
            "&department=" + $("select[name='department']").val(),
            success: function (prmData) {
                $('.report-table-div').html(ajaxRes.responseText);
// Остальные действия выполняем только если вернули данные (иначе pager ошибку выдает, с которой не хочется разбираться)
                if (($('.report-table tbody tr').size()) > 1){
                    ChangeApproveStyle();
                    $('.bx-user-info-name').attr('target', 'blank');
                    $('.report-table').DataTable({
                        "language": {
                            "paginate": {
                                "previous": "Назад",
                                "next": "Вперед"
                            },
                            "search": "Поиск:",
                            "zeroRecords": "Нет сотрудников, соответствующих фильтру"
                        },
                        "pageLength": 30,
                        "info": false,
                        "lengthChange": false,
                        "autoWidth": false,
                        "orderMulti": true,
                        "ordering": true,
                        "order": [[ 7, 'asc' ], [ 0, 'asc' ]],
                    });
                    $('.dataTables_filter label input').focus();
                    ChangeApproveStyle();
                    $('.report-table').on('draw.dt', function(){
                        ChangeApproveStyle();
                    });
                    //$('.report-table').on('search.dt', function(){
                    //    ChangeApproveStyle();
                    //});
                }
                BX.closeWait();
            }
        });
    return false;
}

function ChangeApproveStyle(){
    if ($('#shownotapprove').prop("checked")){
        $('.notapproved').css('background-color', '#fb5555');
        $('.report-table tbody tr.odd td.notapproved').css('background-color', '#fb5555');
        $('.report-table tbody tr.even td.notapproved').css('background-color', '#fda2a2');

    }  else{
        $('.notapproved').css('background-color', 'transparent');
        $('.report-table tbody tr.odd td.notapproved').css('background-color', '#e2eef3');
        $('.report-table tbody tr.even td.notapproved').css('background-color', 'transparent');
    }

}

$(function(){
   $('.report-table-div').on("click", ".total-violation-href", function(){
// Ищем необходимый div
       var fDiv = $(this).siblings('.user_violations-div');
// Скрываем все открытые div-ы данного класса
       $('.user_violations-div:visible').each(function(){
           $(this).hide();
       });
       fDiv.css({
           left: ($(document).width() - fDiv.outerWidth())/2,
           top: ($(window).height() - fDiv.outerHeight())/2 - $(window).height()*0.05
       });
       fDiv.show();
   });
    $('.report-table-div').on("click", ".user_violations-div-close, .user_violations-div-close-button", function(){
        $('.user_violations-div:visible').fadeOut(200);
        return false;
    });
});

function PrintReport(prmPage){
    var href = prmPage + '?action=ajaxreport';
    var ds = $('#date_start').val();
    var de = $('#date_end').val();
    var podr = $('.filter-dropdown').val();
    if (ds !==""){
        href += "&datestart=" + ds;
    }
    if (de !==""){
        href += "&dateend=" + de;
    }
    if (podr !==""){
        href += "&department=" + podr;
    }
    window.open(href, "_blank");
}



