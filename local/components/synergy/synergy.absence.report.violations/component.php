<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 13.11.2015
        * Time: 13:10
        */
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

ini_set('display_errors','On');
error_reporting('E_ALL');

session_start();
$iblock_code = 'absence'; // Код инфоблока absence
$arResult['ACTION'] = $_REQUEST['action'];
$date_start = $_REQUEST['datestart'];
$date_end = $_REQUEST['dateend'];
$department = $_REQUEST['department'];

// Устанавливаем значения фильтра ----------------------
$arResult['NO_PARAMS'] = false;
$arResult['NEED_TO_GENERATE'] = false;

if ($arResult['ACTION'] == "ajaxreport"){
    $_SESSION['violation_report_department'] = $department;
    $arResult['DEPARTMENT'] = $department;
    if (!empty($department)) {
        $DepInfo = CIntranetUtils::GetDepartmentsData(array($department));
        $arResult['DEPARTMENT_NAME'] = $DepInfo[$department];
    } else {
        $arResult['DEPARTMENT_NAME'] = "Все подразделения";
    }
    if (empty($date_start)){
        $arResult['NO_PARAMS'] = true;
    } else {
        $_SESSION['violation_report_date_start'] = $date_start;
        $arResult['DATE_START'] = $date_start;
    }
    if (empty($date_end)){
        $arResult['NO_PARAMS'] = true;
    } else {
        $_SESSION['violation_report_date_end'] = $date_end;
        $arResult['DATE_END'] = $date_end;
    }
//
} elseif (($arResult['ACTION'] == "empty") || (empty($arResult['ACTION']))){
    if (isset($_SESSION['violation_report_department'])){
        $arResult['DEPARTMENT'] = $_SESSION['violation_report_department'];
    } else{
        $ar_depts = CIntranetUtils::GetUserDepartments($USER->GetID());
        if (!empty($ar_depts)) {
            $arResult['DEPARTMENT'] = $ar_depts[0];
        }
    }
    if (isset($_SESSION['violation_report_date_start'])){
        $arResult['DATE_START'] = $_SESSION['violation_report_date_start'];
    } else {
// Первоначальная инициализация
// Если текущее число более 5, то устанавливаем первое число текущего месяца
// Иначе первое число предыдущего
        if (date("d") >= 5) {
            $arResult['DATE_START'] = "01." . date("m.Y");
        } else
        {
            $arResult['DATE_START'] = "01." . date("m.Y", strtotime("-6 day"));
        }
    }
    if (isset($_SESSION['violation_report_date_end'])){
        $arResult['DATE_END'] = $_SESSION['violation_report_date_end'];
    } else{
        $arResult['DATE_END'] = ''; // Пустое значение
    }
    if ((empty($arResult['DATE_START'])) || (empty($arResult['DATE_END']))){
        $arResult['NO_PARAMS'] = true;
    }
    $arResult['NEED_TO_GENERATE'] = true;
}
// --------------------- закончили с инициализацией фильтра

if ($arResult['ACTION'] == "ajaxreport"){
// Если не установлены параметры, то ничего не считаем
    if (!$arResult['NO_PARAMS']) {
// Передаем в шаблон данные, необходимые для шаблона служебной записки
        $arResult['GENERATE_DATE'] = date('d.m.Y');
        $printParams = GetMemoSettings();
        if ($printParams !== false) {
            $memoParams = GetMemoSettings();
            $arResult['MEMO_PARAMS'] = GetMemoSettings();
//            $arResult['MEMO_RECIPIENT_2'] = $memoParams['MEMO_RECIPIENT_FIO_2'];
//            $arResult['MEMO_AUTHOR_FIO'] = $memoParams['MEMO_AUTHOR_FIO'];
//            $arResult[''] = $memoParams[''];
//            $arResult[''] = $memoParams[''];
//            $arResult[''] = $memoParams[''];
        }
// Ищем ID инфоблока по коду
        $ibDB = CIBLock::GetList(array(), array('CODE' => $iblock_code));
        $tmpIB = $ibDB->fetch();
        $IB_ID = $tmpIB['ID'];

// Найдем варианты "Типов отсутствий" для опоздания, раннего ухода, отсутствия
        $db_list = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $IB_ID));
        while ($ar_list = $db_list->fetch()) {
            if ($ar_list['XML_ID'] == 'SYN_LATE') {
                $arAbsenceType['SYN_LATE'] = $ar_list['ID'];
            }
            if ($ar_list['XML_ID'] == 'SYN_LEAVING') {
                $arAbsenceType['SYN_LEAVING'] = $ar_list['ID'];
            }
            if ($ar_list['XML_ID'] == 'SYN_ABSENT') {
                $arAbsenceType['SYN_ABSENT'] = $ar_list['ID'];
            }
        }
// Получаем список элементов инфоблока
        $arFilter['IBLOCK_ID'] = $IB_ID;
        $arFilter['>=DATE_ACTIVE_FROM'] = $date_start . " 00:00:00";
        $arFilter['<=DATE_ACTIVE_TO'] = $date_end . " 23:59:59";
        $arFilter['PROPERTY_SALTO_APPROVED'] = 0;

// Как оказалось, в отчет надо помещать только неутвержденные записи
// Соответственно, отбираем только неутвержденные
        if (!empty($department)) {
            $dbDepUsers = CIntranetUtils::GetDepartmentEmployees($department, true, false, false);
            while ($arDepUsers_tmp = $dbDepUsers->fetch()) {
                $arDepUsers[] = $arDepUsers_tmp['ID'];
            }
//        print_r($arDepUsers);
        } else {
            $arDepUsers = array();
        }
// Формируем ограничения на основании подразделения к которому принадлежит пользователь
// Если пользователь относится к группе "Администраторы контроля посещаемости", то фильтровать не будем
// Иначе добавляем в фильтр самого пользователя и всех его подчиненных
        $isABSadmin = isAbsenceAdmin($USER->GetID());
        if (!$isABSadmin) {
// Изначально было так:
//            $arUserFilter[] = $USER->GetID();
//            $dbUserFilter = CIntranetUtils::GetSubordinateEmployees($USER->GetID(), CHECKTYPE);
//            while ($arUser = $dbUserFilter->fetch()) {
//                $arUserFilter[] = $arUser['ID'];
//            }
// После добавления функционала секретарей стало так
            $arUserFilter = GetSubordinateEmployees_ex($USER->GetID(), CHECKTYPE);
        } else{
            $arUserFilter = false; // НЕ фильтруем пользователей
        }
// **********************************

        $db_list = CIBlockElement::GetList(array("NAME"), $arFilter, array('DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO',
            'PROPERTY_USER', 'PROPERTY_ABSENCE_TYPE', 'PROPERTY_SALTO_APPROVED', 'DETAIL_TEXT', 'NAME'));
        $total = 0; // Общее количество нарушений
        while ($ar_list = $db_list->fetch()) {
            $locuserID = $ar_list['PROPERTY_USER_VALUE'];
// Проверяем, был ли уже добавлен пользователь в массив (чтобы еще раз не запрашивать по нему данные
            if (!array_key_exists($arResult['USERS'], $locuserID)) {
// Проверяем условие фильтра по подразделениям:
// Добавляем пользователя, если фильтр по подразделениям пустой
// или он входит в подразделение, а также либо если текущий пользователь "Администратор контроля посещаемости"
// либо он руководитель подразделения
                if (((empty($arDepUsers)) || (in_array($locuserID, $arDepUsers))) && (($isABSadmin) || in_array($locuserID, $arUserFilter))) {
                    $db_User = CUser::GetByID($locuserID);
                    $ar_User = $db_User->fetch();
//            print_r($ar_User); echo "<br>";
                    $arResult['USERS'][$locuserID]['NAME'] = $ar_User['LAST_NAME'] . " " . $ar_User['NAME'];
                    $arResult['USERS'][$locuserID]['WORK_POSITION'] = $ar_User['WORK_POSITION']; // Должность
                    $arResult['USERS'][$locuserID]['WORK_COMPANY'] = $ar_User['WORK_COMPANY']; // Организация
                    $arResult['USERS'][$locuserID]['APPROVED'] = 1; // Изначально считаем, что запись утверждена, потом еще раз проверим
// Получаем подразделение
                    $ar_depts = CIntranetUtils::GetUserDepartments($locuserID);
                    if (!empty($ar_depts)) {
// Получим информацию о первом подразделении (если у пользователя больше 1-го подразделения, то они не будут обработаны
                        $depDate = CIntranetUtils::GetDepartmentsData(array($ar_depts[0]));
//                print_r($depDate);
//                echo "<br>";
                        $arResult['USERS'][$locuserID]['DEPARTMENT'] = $depDate[$ar_depts[0]];
// Получаем руководителя подразделения, в котором работает сотрудник
                        $ar_manager = GetUserManager($locuserID);
                        if ($ar_manager !== false) {
                            $arResult['USERS'][$locuserID]['MANAGER_ID'] = $ar_manager['ID'];
                            $arResult['USERS'][$locuserID]['MANAGER'] = $ar_manager['LAST_NAME'] . " " . $ar_manager['NAME'];
                        }
//                    print_r($ar_manager);
                    }
                } else {
                    continue;
                }
            }
// Добавляем дополнительную информацию для всплывающих подсказок
            $viol_list = array();
            $viol_list['DATE'] = $DB->FormatDate($ar_list['DATE_ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY");
            $viol_list['TYPE'] = ''; // заполним позже
            $viol_list['DETAIL'] = preg_replace("#(.*)\";#", "", $ar_list['DETAIL_TEXT']);
            $viol_list['NAME'] = $ar_list['NAME'];
// Пока что так поставим ссылку (потом надо бы облагородить)
            $viol_list['HREF'] = "/company/absence.php?&user=".$locuserID."#AP:week|".MakeTimeStamp($DB->FormatDate($ar_list['DATE_ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY"), "DD.MM.YYYY")."000";

// Подсчитываем нарушения каждого вида
// Опоздания...
            if ($ar_list['PROPERTY_ABSENCE_TYPE_ENUM_ID'] == $arAbsenceType['SYN_LATE']) {
                $viol_list['TYPE'] = 'Опоздание';
                if (empty($arResult['USERS'][$locuserID]['SYN_LATE'])) {
                    $arResult['USERS'][$locuserID]['SYN_LATE'] = 1;
                } else {
                    $arResult['USERS'][$locuserID]['SYN_LATE']++;
                }
                $arResult['USERS'][$locuserID]['TOTAL']++;
            }
// Ранний уход...
            if ($ar_list['PROPERTY_ABSENCE_TYPE_ENUM_ID'] == $arAbsenceType['SYN_LEAVING']) {
                $viol_list['TYPE'] = 'Ранний уход';
                if (empty($arResult['USERS'][$locuserID]['SYN_LEAVING'])) {
                    $arResult['USERS'][$locuserID]['SYN_LEAVING'] = 1;
                } else {
                    $arResult['USERS'][$locuserID]['SYN_LEAVING']++;
                }
                $arResult['USERS'][$locuserID]['TOTAL']++;
            }
// Отсутствие...
            if ($ar_list['PROPERTY_ABSENCE_TYPE_ENUM_ID'] == $arAbsenceType['SYN_ABSENT']) {
                $viol_list['TYPE'] = 'Отсутствие';
                if (empty($arResult['USERS'][$locuserID]['SYN_ABSENT'])) {
                    $arResult['USERS'][$locuserID]['SYN_ABSENT'] = 1;
                } else {
                    $arResult['USERS'][$locuserID]['SYN_ABSENT']++;
                }
                $arResult['USERS'][$locuserID]['TOTAL']++;
            }
// Проверим, утверждена ли запись
            if ($ar_list['PROPERTY_SALTO_APPROVED_VALUE'] != 1) {
                $arResult['USERS'][$locuserID]['APPROVED'] = -1;
            }
            $arResult['USERS'][$locuserID]['VIOL_LIST'][] = $viol_list;
        }
        uasort($arResult['USERS'], "uasort_cmp");
    }
}
$this->IncludeComponentTemplate();

function uasort_cmp($a, $b){
    if ($a['NAME'] < $b['NAME']) return -1;
    if ($a['NAME'] > $b['NAME']) return 1;
    if ($a['NAME'] == $b['NAME']) return 0;
}