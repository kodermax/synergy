<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<script>
    var componentPath = "<?=$componentPath?>";
    $(document).ready(

        function()
        {
            $("#period").change(
                function()
                {
                    if($("#period").val()=="interval")
                        $("#interval").css('display', 'inline-block');
                    else
                        $("#interval").hide();
                }
            );
            $("#dept").chosen({no_results_text: "Ничего не найдено!"});
            $("#workarea-content").css("min-height","1000px");
        }
    );
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1" id="form1">
        <input type="hidden" name="mode" value="tbl">
        <div class="form-element">
            <label for="funnel">Вид воронки:</label>
            <select id="funnel" name="funnel">
                <option value="total"<?=($arResult["FUNNEL"]=="total")?" selected":""?>>Классическая</option>
                <option value="progressive"<?=($arResult["FUNNEL"]=="progressive")?" selected":""?>>Конверсионная</option>
            </select>
        </div>
        <div class="form-element">
        <label for="period">Период:</label>
            <select id="period" name="period">
                <? foreach($arResult["PERIODS"] as $k=>$v):?>
                    <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                <? endforeach; ?>
            </select>
        <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "N",
     "HIDE_TIMEBAR" => "Y"
    )
);?>
        </div>
        </div>
        <div class="form-element"  style="display: inline-block !important;">
            <label for='dept'>Отдел / Сотрудник:</label>
            <select id='dept' name='dept'>
              <? foreach ($arResult["DEPT_LIST"] as $arItem):?>
                  <? $sel=($arItem["ID"]==$arResult["DEPT"])?" selected":""; ?>
                  <option value="<?=$arItem["ID"]?>"<?=$sel?>><b><?=(str_repeat("&nbsp;",($arItem["DEPTH_LEVEL"]-1)*4)." ".$arItem["NAME"])?></b></option>
                <? endforeach; ?>
                <? foreach ($arResult["USER_LIST"] as $arItem):?>
                    <? $sel=($arItem["ID"]==$arResult["USER"])?" selected":""; ?>
                    <option value="usr_<?=$arItem["ID"]?>"<?=$sel?>><?=$arItem["LAST_NAME"]." ".$arItem["NAME"]?></b></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-element" style="display: inline-block !important;">
            <input id="rec" name="rec" type="checkbox" value="1" <?=($arResult["REC"]==1)?" checked":""?>>
            <label for="rec"> Учитывать подотделы</label>
        </div>
        <div class="form-element">
            <label for="number">Продукт:</label>
            <input id="product" name="product" type="text" value="<?=$arResult["PRODUCT"]?>">
        </div>
        <div class="form-element">
            <label for="number">Кампания:</label>
            <input id="company" name="company" type="text" value="<?=$arResult["COMPANY"]?>">
        </div>
        <div class="form-element">
            <label for="number">Ленд:</label>
            <input id="land" name="land" type="text" value="<?=$arResult["LAND"]?>">
        </div>
        <br>
        <br>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Воронка продаж</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="funnel" type="hidden" value="<?=$arResult["FUNNEL"]?>">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="dept" type="hidden" value="<?=($arResult["DEPT"]!=-1)?$arResult["DEPT"]:"usr_".$arResult["USER"]?>">
        <input name="rec" type="hidden" value="<?=$arResult["REC"]?>">
        <input name="product" type="hidden" value="<?=$arResult["PRODUCT"]?>">
        <input name="company" type="hidden" value="<?=$arResult["COMPANY"]?>">
        <input name="land" type="hidden" value="<?=$arResult["LAND"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? endif;?>
    <h2><?=$arResult["FROM"]?>-<?=$arResult["TO"]?></h2>
    <h2><?=($arResult["DEPT"]!=-1)?$arResult["DEPT_DATA"]["NAME"]:$arResult["USER_DATA"]?></h2>
    <table class="table-result">
        <tr style="background-color: #BFBFBF;">
            <th>Статус</th>
            <th>Количество</th>
            <th>% от общего</th>
        </tr>
        <tr style="background-color: #BFBFBF;">
            <td style="text-align: left;">Общий итог</td>
            <td><?=$arResult["RESULT"]["_TOTAL"]?></td>
            <td>100%</td>
        </tr>
        <? foreach($arResult["COLORS"] as $color=>$statuses):?>
            <? foreach($statuses as $status_id):?>
            <tr style="background-color: <?=$color?>;">
                <td style="text-align: left;"><?=$arResult["STATUS"][$status_id]?></td>
                <td><?=intval($arResult["RESULT"][$status_id]["CNT"])?></td>
                <td><?=($arResult["RESULT"]["_TOTAL"]!=0)?round($arResult["RESULT"][$status_id]["CNT"]/$arResult["RESULT"]["_TOTAL"]*100,2):0?>%</td>
            </tr>
            <? endforeach; ?>
        <? endforeach; ?>
    </table>
    <br>
    <table class="table-result"
        <tr style="background-color: #BFBFBF;">
            <th>Стадия сделки</th>
            <th>Количество</th>
            <th>Доля</th>
            <th>Возможная сумма</th>
            <th>Доход от выигранных сделок</th>
        </tr>
    <tr style="background-color: #BFBFBF;">
        <td style="text-align: left;">Общий итог</td>
        <td><?=$arResult["RESULT_DEAL"]["_TOTAL"]["CNT"]?></td>
        <td>100%</td>
        <td><?=number_format(intval($arResult["RESULT_DEAL"]["_TOTAL"]["SUM"]),0,",",".")?></td>
        <td><?=number_format(intval($arResult["RESULT_DEAL"]["_TOTAL"]["SUMWON"]),0,",",".")?></td>
    </tr>
        <? foreach($arResult["COLORS_DEAL"] as $color=>$deal_stages):?>
            <? foreach($deal_stages as $deal_stage_id):?>
                <tr style="background-color: <?=$color?>;">
                    <td style="text-align: left;"><?=$arResult["DEAL_STAGE"][$deal_stage_id]?></td>
                    <td><?=intval($arResult["RESULT_DEAL"][$deal_stage_id]["CNT"])?></td>
                    <td><?=($arResult["RESULT_DEAL"]["_TOTAL"]["CNT"]!=0)?round($arResult["RESULT_DEAL"][$deal_stage_id]["CNT"]/$arResult["RESULT_DEAL"]["_TOTAL"]["CNT"]*100,2):0?>%</td>
                    <td><?=number_format(intval($arResult["RESULT_DEAL"][$deal_stage_id]["SUM"]),0,",",".")?></td>
                    <td><?=($deal_stage_id=="WON")?number_format(intval($arResult["RESULT_DEAL"]["_TOTAL"]["SUMWON"]),0,",","."):""?></td>
                </tr>
            <? endforeach; ?>
        <? endforeach; ?>
    </table>

    <br>
    <table class="table-result"
    <tr>
        <td style="text-align: left;"><b>Общий итог</b></td>
        <td><?=intval($arResult["RESULT"]["_TOTAL"])?></td>
        <td>100%</td>
    </tr>
    <tr>
        <td style="text-align: left;">В работе</td>
        <td><?=intval($arResult["RESULT_TOTAL"]["WORK"])?></td>
        <td><?=(intval($arResult["RESULT"]["_TOTAL"])!=0)?round($arResult["RESULT_TOTAL"]["WORK"]/$arResult["RESULT"]["_TOTAL"]*100,2):0?>%</td>
    </tr>
    <tr>
        <td style="text-align: left;">Сконвертирован</td>
        <td><?=intval($arResult["RESULT_TOTAL"]["CONVERTED"])?></td>
        <td><?=(intval($arResult["RESULT"]["_TOTAL"])!=0)?round($arResult["RESULT_TOTAL"]["CONVERTED"]/$arResult["RESULT"]["_TOTAL"]*100,2):0?>%</td>
    </tr>
    <tr style="background-color: #D8D8D8;">
        <td style="text-align: left;">Сделок в работе</td>
        <td><?=intval($arResult["RESULT_TOTAL"]["WORK_DEAL"])?></td>
        <td><?=(intval($arResult["RESULT"]["_TOTAL"])!=0)?round($arResult["RESULT_TOTAL"]["WORK_DEAL"]/$arResult["RESULT"]["_TOTAL"]*100,2):0?>%</td>
    </tr>
    <tr style="background-color: #D8D8D8;">
        <td style="text-align: left;">Договор</td>
        <td><?=intval($arResult["RESULT_TOTAL"]["DOGOVOR_DEAL"])?></td>
        <td><?=(intval($arResult["RESULT"]["_TOTAL"])!=0)?round($arResult["RESULT_TOTAL"]["DOGOVOR_DEAL"]/$arResult["RESULT"]["_TOTAL"]*100,2):0?>%</td>
    </tr>
    <tr style="background-color: #D8D8D8;">
        <td style="text-align: left;">Успешная</td>
        <td><?=intval($arResult["RESULT_TOTAL"]["WON_DEAL"])?></td>
        <td><?=(intval($arResult["RESULT"]["_TOTAL"])!=0)?round($arResult["RESULT_TOTAL"]["WON_DEAL"]/$arResult["RESULT"]["_TOTAL"]*100,2):0?>%</td>
    </tr>
    <tr style="background-color: #D8D8D8;">
        <td style="text-align: left;">Сумма успешных сделок</td>
        <td><?=number_format(intval($arResult["RESULT_TOTAL"]["SUM_DEAL"]),0,",",".")?></td>
        <td>&nbsp;</td>
    </tr>
    </table>
</div>
<? elseif($arResult["MODE"] && !$arResult["ERROR"]):?>
<div class="result">
<b>По Вашему запросу ничего не найдено</b>
</div>
<? else:?>
<div class="error">
<? foreach ($arResult["ERROR"] as $error):?>
<b><?=$error;?></b>
<? endforeach; ?>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>