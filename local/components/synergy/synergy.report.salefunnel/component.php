<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

function getDatesByWeek($_week_number, $_year = null)
{
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CJSCore::Init('jquery');
$APPLICATION->AddHeadScript("/local/assets/chosen/chosen.jquery.min.js", true);
$APPLICATION->SetAdditionalCSS("/local/assets/chosen/chosen.min.css", true);

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

$arResult["FROM"] = ((bool)strtotime($_REQUEST["from"])) ? $_REQUEST["from"] : date("d.m.Y", strtotime("yesterday"));
$arResult["TO"] = ((bool)strtotime($_REQUEST["to"])) ? $_REQUEST["to"] : date("d.m.Y");

$arResult["MODE"] = ($_REQUEST["mode"] == "tbl" || $_REQUEST["mode"] == "exl") ? $_REQUEST["mode"] : null;
$arResult["PERIOD"] = $_REQUEST["period"];
$arResult["PRODUCT"] = $_REQUEST["product"];
$arResult["COMPANY"] = $_REQUEST["company"];
$arResult["LAND"] = $_REQUEST["land"];
$arResult["FUNNEL"] = ($_REQUEST["funnel"] == "total" || $_REQUEST["funnel"] == "progressive") ? $_REQUEST["funnel"] : "total";


$arResult["PERIODS"] = Array(
    "cday" => "Текущий день",
    "pday" => "Предыдущий день",
    "cweek" => "Текущая неделя",
    "pweek" => "Предыдущая неделя",
    "cmonth" => "Текущий месяц",
    "pmonth" => "Предыдущий месяц",
    "cyear" => "Текущий год",
    "pyear" => "Предыдущий год",
    "interval" => "Интервал"
);

$arResult["THIRD"] = [
    "WORK" => ["ON_HOLD", "17", "CANNOT_CONTACT", "ASSIGNED", "DETAILS", "NEW"],
];

$arResult["THIRD_DEAL"] = [
    "WORK" => ["NEW", "DETAILS", "9", "PROPOSAL", "NEGOTIATION", "1", "2", "3", "10", "8", "4", "11","12","13","14"],
];

$arResult["COLORS"] = [
    "#C2D69B" => ["CONVERTED"],
    "#DBE5F1" => ["NEW","ON_HOLD", "17", "CANNOT_CONTACT", "ASSIGNED", "DETAILS", "IN_PROCESS"],
    "#F7A291" => ["12", "14", "15", "8", "13", "10", "2", "5", "9", "JUNK", "16","11"]
];

$arResult["COLORS_DEAL"] = [
    "#C2D69B" => ["WON"],
    "#DBE5F1" => ["NEW", "DETAILS", "9", "PROPOSAL", "NEGOTIATION", "1", "2", "3", "10", "8", "4", "11","12","13","14"],
    "#F7A291" => ["LOSE", "5", "6", "7"]
];

$arResult["RESULT_TOTAL"]=
    [
        "WORK"=>0,
        "CONVERTED"=>0,
        "WORK_DEAL"=>0,
        "DOGOVOR_DEAL"=>0,
        "WON_DEAL"=>0,
        "SUM_DEAL"=>0
    ];

if (!$_REQUEST["dept"])
{
    $rsUser = CUser::GetList(($by = "ID"), ($order = "desc"), array("ID" => $USER->GetID()), array("SELECT" => array("UF_DEPARTMENT")));
    if ($arUser = $rsUser->Fetch())
    {
        $arResult["DEPT"] = $arUser["UF_DEPARTMENT"][0];
        $arResult["USER"] = -1;
    }
    else
    {
        $arResult["USER"] = -1;
        $arResult["DEPT"] = 1;
    }

}
else
{
    if (preg_match("/usr_(\d+)/", $_REQUEST["dept"], $matches))
    {
        $arResult["USER"] = $matches[1];
        $arResult["DEPT"] = -1;
    }
    else
    {
        $arResult["USER"] = -1;
        $arResult["DEPT"] = $_REQUEST["dept"];
    }

}

$arResult["REC"] = $_REQUEST["rec"];

$rsDept = CIBlockSection::GetTreeList(array("IBLOCK_ID" => 1, "ACTIVE" => "Y"));
while ($arDept = $rsDept->GetNext())
{
    $arResult["DEPT_LIST"][] = $arDept;
}

$rsUser = CUser::GetList(($by = "LAST_NAME"), ($order = "ASC"), ["ACTIVE" => "Y"], ["SELECT" => ["UF_DEPARTMENT"]]);
while ($arUser = $rsUser->GetNext())
{
    if (trim($arUser["LAST_NAME"]) == "") continue;
    if (intval($arUser["UF_DEPARTMENT"][0]) == 0) continue;
    $arResult["USER_LIST"][$arUser["ID"]] = ["ID" => $arUser["ID"], "NAME" => $arUser["NAME"], "LAST_NAME" => $arUser["LAST_NAME"]];
}

switch ($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"] = $arResult["TO"] = date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"] = $arResult["TO"] = date("d.m.Y", strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"] = date("d.m.Y", strtotime("last Monday"));
        $arResult["TO"] = date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"] = date("d.m.Y", strtotime("last Monday -1 week"));
        $arResult["TO"] = date("d.m.Y", strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"] = "01." . date("m.Y");
        $arResult["TO"] = date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"] = "01." . date("m.Y", strtotime("previous month"));
        $arResult["TO"] = date("d.m.Y", strtotime("01." . date("m.Y") . " -1 day"));
        break;
    case "cyear":
        $arResult["FROM"] = "01.01." . date("Y");
        $arResult["TO"] = date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"] = "01.01." . date("Y", strtotime("previous year"));
        $arResult["TO"] = "31.12." . date("Y", strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"] = ((bool)strtotime($_REQUEST["from"])) ? $_REQUEST["from"] : date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"] = ((bool)strtotime($_REQUEST["to"])) ? $_REQUEST["to"] : date("d.m.Y");
        break;
}

$sql = "SELECT STATUS_ID,NAME,ENTITY_ID FROM b_crm_status AS `status` WHERE `status`.ENTITY_ID='STATUS' OR `status`.ENTITY_ID='DEAL_STAGE' ORDER BY STATUS_ID";

$res = $DB->Query($sql);
while ($row = $res->Fetch())
{
    $arResult[$row["ENTITY_ID"]][$row["STATUS_ID"]] = $row["NAME"];
}

if ($arResult["MODE"])
{

    if ($arResult["DEPT"] != -1)
    {
        $rsDept = CIBlockSection::GetList(array(), array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "ID" => $arResult["DEPT"]), false, array("ID", "NAME"));
        $arResult["DEPT_DATA"] = $rsDept->Fetch();

        if ($arResult["REC"] == 1)
        {
            $recursive = true;
        }
        else
        {
            $recursive = false;
        }

        $rsUser = CIntranetUtils::GetDepartmentEmployees($arResult["DEPT"], $recursive,false,'N');

        while ($arUser = $rsUser->GetNext())
        {
            $arResult["USERS"][$arUser["ID"]] = $arUser;
        }

        $list_user = join(",", array_keys($arResult["USERS"]));
    }
    else
    {
        $arResult["USER_DATA"] = $arResult["USER_LIST"][$arResult["USER"]]["LAST_NAME"] . " " . $arResult["USER_LIST"][$arResult["USER"]]["NAME"];
        $list_user = $arResult["USER"];
    }

    if ($list_user == "")
    {
        $list_user = "-1";
    }

    $from = $DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to = $DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));

    $sqladd_lead = "";

    if ($arResult["LAND"] != "")
    {
        $sqladd_lead .= " AND `uts`." . PROP_LEAD_LAND . "='" . $arResult["LAND"] . "'";
    }
    if ($arResult["PRODUCT"] != "")
    {
        $sqladd_lead .= " AND `uts`." . PROP_LEAD_PRODUCT . "='" . $arResult["PRODUCT"] . "'";
    }
    if ($arResult["COMPANY"] != "")
    {
        $sqladd_lead .= " AND `uts`." . PROP_LEAD_CAMPANY . "='" . $arResult["COMPANY"] . "'";
    }

    $sql = "
        SELECT
                        `crm_lead`.`STATUS_ID` AS `STATUS_ID`,
                        COUNT(`crm_lead`.ID) AS CNT
                FROM

                      `b_crm_lead` `crm_lead`
                      left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
                WHERE
                    DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
                    AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user)
                    $sqladd_lead
                GROUP BY `STATUS_ID`
                ORDER BY `crm_lead`.STATUS_ID
        ";

    $arResult["SQL"]=$sql;

    $res = $DB->Query($sql);
    while ($row = $res->Fetch())
    {
        if (in_array($row["STATUS_ID"], $arResult["THIRD"]["WORK"]))
        {
            $arResult["RESULT_TOTAL"]["WORK"] += $row["CNT"];
        }
        if ($row["STATUS_ID"] == "CONVERTED")
        {
            $arResult["RESULT_TOTAL"]["CONVERTED"] += $row["CNT"];
        }

        $arResult["RESULT"][$row["STATUS_ID"]] = [
            "NAME" => $row["NAME"],
            "CNT" => $row["CNT"]
        ];
        $arResult["RESULT"]["_TOTAL"] += $row["CNT"];
    }

    $sqladd_deal = "";

    if ($arResult["LAND"] != "")
    {
        $sqladd_deal .= " AND `uts`." . PROP_DEAL_LAND . "='" . $arResult["LAND"] . "'";
    }
    if ($arResult["PRODUCT"] != "")
    {
        $sqladd_deal .= " AND `uts`." . PROP_DEAL_PRODUCT . "='" . $arResult["PRODUCT"] . "'";
    }
    if ($arResult["COMPANY"] != "")
    {
        $sqladd_deal .= " AND `uts`." . PROP_DEAL_CAMPANY . "='" . $arResult["COMPANY"] . "'";
    }

    if($arResult["FUNNEL"]=="total")
    {
        $sql_cond="DATE(`crm_deal`.DATE_CREATE) BETWEEN '$from' AND '$to' AND `crm_deal`.`ASSIGNED_BY_ID` IN ($list_user)".$sqladd_deal;
    }
    elseif($arResult["FUNNEL"]=="progressive")
    {
        $sql_cond="`crm_deal`.LEAD_ID IN (SELECT `crm_lead`.ID FROM `b_crm_lead` `crm_lead` left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID) ".
            "WHERE DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to' AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user)".$sqladd_lead.")";
    }

    $sql = "
        SELECT
                        `crm_deal`.`STAGE_ID` AS `STAGE_ID`,
                        COUNT(`crm_deal`.ID) AS CNT,
                        SUM(`crm_deal`.OPPORTUNITY) AS `SUM`
                FROM

                      `b_crm_deal` `crm_deal`
                      left join b_uts_crm_deal as `uts` on(`uts`.VALUE_ID=`crm_deal`.ID)
                WHERE
                    $sql_cond
                GROUP BY `STAGE_ID`
                ORDER BY `crm_deal`.STAGE_ID
        ";

    //echo $sql;

    $res = $DB->Query($sql);
    while ($row = $res->Fetch())
    {

        if (in_array($row["STAGE_ID"], $arResult["THIRD_DEAL"]["WORK"]))
        {
            $arResult["RESULT_TOTAL"]["WORK_DEAL"] += $row["CNT"];
        }
        if ($row["STAGE_ID"] == "8")
        {
            $arResult["RESULT_TOTAL"]["DOGOVOR_DEAL"] += $row["CNT"];
        }
        if ($row["STAGE_ID"] == "WON")
        {
            $arResult["RESULT_TOTAL"]["WON_DEAL"] += $row["CNT"];
            $arResult["RESULT_TOTAL"]["SUM_DEAL"] += $row["SUM"];
        }

        $arResult["RESULT_DEAL"][$row["STAGE_ID"]] = [
            "NAME" => $row["NAME"],
            "CNT" => $row["CNT"],
            "SUM" => $row["SUM"]
        ];
        $arResult["RESULT_DEAL"]["_TOTAL"]["CNT"] += $row["CNT"];
        $arResult["RESULT_DEAL"]["_TOTAL"]["SUM"] += $row["SUM"];
        if ($row["STAGE_ID"] == "WON")
        {
            $arResult["RESULT_DEAL"]["_TOTAL"]["SUMWON"] += $row["SUM"];
        }
    }

}
$this->IncludeComponentTemplate();
?>


