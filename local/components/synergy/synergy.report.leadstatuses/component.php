<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CJSCore::Init('jquery');
$APPLICATION->AddHeadScript("/local/assets/chosen/chosen.jquery.min.js", true);
$APPLICATION->SetAdditionalCSS("/local/assets/chosen/chosen.min.css", true);

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");



$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");

$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PERIOD"]=$_REQUEST["period"];
$arResult["DEVIDE"]=$_REQUEST["devide"];
$arResult["DATETYPE"]=$_REQUEST["datetype"];
if($arResult["DATETYPE"]=="assigned")
    $arResult["DATEFIELD"]="`uts`.".PROP_LEAD_ASSIGNED_DATE;
else
    $arResult["DATEFIELD"]="`crm_lead`.DATE_CREATE";

$arResult["PERIODS"] = Array(
    "cday"=>"Текущий день",
    "pday"=>"Предыдущий день",
    "cweek"=>"Текущая неделя",
    "pweek"=>"Предыдущая неделя",
    "cmonth"=>"Текущий месяц",
    "pmonth"=>"Предыдущий месяц",
    "cyear"=>"Текущий год",
    "pyear"=>"Предыдущий год",
    "interval"=>"Интервал"
);

$arResult["DEVIDES"]=array(
    "0"=>"За весь период",
    "1"=>"По дням",
    "2"=>"По неделям",
    "3"=>"По месяцам"
);

$arResult["COLORS"] = [
    "#DBE5F1" => ["NEW","IN_PROCESS","DETAILS", "CANNOT_CONTACT", "ASSIGNED", "ON_HOLD","17"],
    "#C2D69B" => ["CONVERTED"],
    "#F7A291" => ["9", "5", "JUNK", "8", "10", "12", "13", "14", "15", "16","2"]
];

if(intval($_REQUEST["dept"])==0)
{
    $rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_DEPARTMENT")));
    if($arUser=$rsUser->Fetch())
    {
        $arResult["DEPT"]=$arUser["UF_DEPARTMENT"][0];
    }
    else
        $arResult["DEPT"]=1;
}
else
    $arResult["DEPT"]=$_REQUEST["dept"];

$arResult["REC"]=$_REQUEST["rec"];

$rsDept=CIBlockSection::GetTreeList(array("IBLOCK_ID"=>1,"ACTIVE"=>"Y"));
while($arDept=$rsDept->GetNext())
{
    $arResult["DEPT_LIST"][]=$arDept;
}

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;
}

$sql = "SELECT STATUS_ID,NAME,ENTITY_ID FROM b_crm_status AS `status` WHERE `status`.ENTITY_ID='STATUS' ORDER BY STATUS_ID";

$res = $DB->Query($sql);
while ($row = $res->Fetch())
{
    $arResult[$row["ENTITY_ID"]][$row["STATUS_ID"]] = $row["NAME"];
}

if($arResult["MODE"])
{

    $rsDept=CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1,"ACTIVE"=>"Y","ID"=>$arResult["DEPT"]),false,array("ID","NAME"));
    $arResult["DEPT_DATA"][$arResult["DEPT"]]=$rsDept->Fetch();

    if($arResult["REC"]==1)
    {
        $rsParentSection = CIBlockSection::GetByID($arResult["DEPT"]);
        if ($arParentSection = $rsParentSection->GetNext())
        {
            $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],"ACTIVE"=>"Y");
            $rsDept = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
            while ($arDept = $rsDept->GetNext())
            {
                $arResult["DEPT_DATA"][$arDept["ID"]]=$arDept;
            }
        }
    }

    foreach($arResult["DEPT_DATA"] as $deptID=>$deptName)
    {
        $rsUser= CUser::GetList($by='ID', $order='ASC', array('UF_DEPARTMENT' => $deptID), array('SELECT' => array('UF_DEPARTMENT')));

        while($arUser=$rsUser->GetNext())
        {
            if(!$arResult["USERS"][$arUser["ID"]])
            {
                $arResult["USERS"][$arUser["ID"]]["DEPT"]=$deptID;
                $arResult["USERS"][$arUser["ID"]]["NAME"]=$arUser["LAST_NAME"]." ".$arUser["NAME"];
                $arResult["USERS_BY_NAME"][$arUser["LAST_NAME"]." ".$arUser["NAME"]]=$arUser["ID"];
            }
        }
    }

    ksort($arResult["USERS_BY_NAME"]);

    $list_user=join(",",array_keys($arResult["USERS"]));
    if($list_user=="")
        $list_user="-1";

    $from=$DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to=$DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));
    $groupby="";
    $select="";

    switch($arResult["DEVIDE"])
    {
        case 1:
            $groupby='`YEAR`,`MONTH`,`DAY`,';
            $select="DAYOFMONTH(".$arResult["DATEFIELD"].") as `DAY`,
        MONTH(".$arResult["DATEFIELD"].") as `MONTH`,
        YEAR(".$arResult["DATEFIELD"].") as `YEAR`,";
            break;
        case 2:
            $groupby='`YEAR`,`WEEK`,';
            $select="YEAR(".$arResult["DATEFIELD"].") as `YEAR`,
        WEEK(".$arResult["DATEFIELD"].") as `WEEK`,";
            break;
        case 3:
            $groupby='`YEAR`,`MONTH`,';
            $select="MONTH(".$arResult["DATEFIELD"].") as `MONTH`,
        YEAR(".$arResult["DATEFIELD"].") as `YEAR`,";
            break;
        case 4:
            $groupby='`YEAR`,';
            $select="YEAR(".$arResult["DATEFIELD"].") as `YEAR`,";
            break;
    }

    $sql="
      select
        $select
        `crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
        `crm_lead`.STATUS_ID AS `STATUS_ID`,
        COUNT(`crm_lead`.ID) as CNT
      from
        `b_crm_lead` `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(".$arResult["DATEFIELD"].") BETWEEN '$from' AND '$to'
        AND `crm_lead`.ASSIGNED_BY_ID IN ($list_user)
      GROUP BY $groupby `ASSIGNED_BY_ID`,`STATUS_ID`
      ORDER BY `crm_lead`.`ASSIGNED_BY_ID`,`crm_lead`.`STATUS_ID`,".$arResult["DATEFIELD"];

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }
        if(!$arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]][$row["STATUS_ID"]])
            $arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]][$row["STATUS_ID"]]=0;

        if(!$arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]]["_TOTAL"])
            $arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]]["_TOTAL"]=0;

        $arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]][$row["STATUS_ID"]]+=$row["CNT"];
        $arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]]["_TOTAL"]+=$row["CNT"];

        if(!$arResult["TOTAL"]["_TOTAL"])
            $arResult["TOTAL"]["_TOTAL"]=0;

        $arResult["TOTAL"]["_TOTAL"]+=$row["CNT"];

        if(!$arResult["TOTAL"][$row["STATUS_ID"]])
            $arResult["TOTAL"][$row["STATUS_ID"]]=0;
        $arResult["TOTAL"][$row["STATUS_ID"]]+=$row["CNT"];


    }


}
$this->IncludeComponentTemplate();
?>