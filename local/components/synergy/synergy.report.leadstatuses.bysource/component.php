<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 *
 * Необходимо для ускорения работы создать 2 индекса
 * create index dh_VALUE_ID on b_uts_order (VALUE_ID);
 * create index dh_UF_DEAL_ID on b_uts_order (UF_DEAL_ID);
 *
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CJSCore::Init('jquery');
$APPLICATION->AddHeadScript("/local/assets/chosen/chosen.jquery.min.js", true);
$APPLICATION->SetAdditionalCSS("/local/assets/chosen/chosen.min.css", true);

$APPLICATION->SetAdditionalCSS("https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css",true);
$APPLICATION->AddHeadScript("https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js",true);


CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");

$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PERIOD"]=$_REQUEST["period"];
$arResult["DEVIDE"]=$_REQUEST["devide"];
$arResult["LAND"]=$_REQUEST["land"];

$arResult["PERIODS"] = Array(
    "cday"=>"Текущий день",
    "pday"=>"Предыдущий день",
    "cweek"=>"Текущая неделя",
    "pweek"=>"Предыдущая неделя",
    "cmonth"=>"Текущий месяц",
    "pmonth"=>"Предыдущий месяц",
    "cyear"=>"Текущий год",
    "pyear"=>"Предыдущий год",
    "interval"=>"Интервал"
);

$arResult["DEVIDES"]=array(
    "0"=>"За весь период",
    "1"=>"По дням",
    "2"=>"По неделям",
    "3"=>"По месяцам"
);

if(intval($_REQUEST["dept"])==0)
{
    if($USER->GetID()==171) // Для Нищерета отдел по умолчанию - отдел продаж 9
    {
        $arResult["DEPT"]=3463;
    }
    else
    {
        $rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_DEPARTMENT")));
        if($arUser=$rsUser->Fetch())
        {
            $arResult["DEPT"]=$arUser["UF_DEPARTMENT"][0];
        }
        else
            $arResult["DEPT"]=1;
    }

}
else
    $arResult["DEPT"]=$_REQUEST["dept"];

$arResult["REC"]=$_REQUEST["rec"];

$rsDept=CIBlockSection::GetTreeList(array("IBLOCK_ID"=>1,"ACTIVE"=>"Y"));
while($arDept=$rsDept->GetNext())
{
    $arResult["DEPT_LIST"][]=$arDept;
}

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;
}

$sql = "SELECT STATUS_ID,NAME,ENTITY_ID FROM b_crm_status AS `status` WHERE `status`.ENTITY_ID='STATUS' or `status`.ENTITY_ID='DEAL_STAGE' or `status`.ENTITY_ID='SOURCE' ORDER BY SORT";

$res = $DB->Query($sql);
while ($row = $res->Fetch())
{
    $arResult[$row["ENTITY_ID"]][$row["STATUS_ID"]] = $row["NAME"];
}

if($arResult["MODE"])
{

    $rsDept = CIBlockSection::GetList(array(), array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "ID" => $arResult["DEPT"]), false, array("ID", "NAME"));
    $arResult["DEPT_DATA"] = $rsDept->Fetch();
    //$deptsSql[]="b_uts_crm_lead." . PROP_LEAD_DEPARTMENT_STRUCTURE . " LIKE '%:" . $arResult["DEPT"] . ";%'";

    if ($arResult["REC"] == 1)
    {
       /* $rsParentSection = CIBlockSection::GetByID($arResult["DEPT"]);
        if ($arParentSection = $rsParentSection->GetNext())
        {
            $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
            $rsDept = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
            while ($arDept = $rsDept->GetNext())
            {
                $deptsSql[]="b_uts_crm_lead." . PROP_LEAD_DEPARTMENT_STRUCTURE . " LIKE '%:" . $arDept["ID"] . ";%'";
            }
        }*/

        $recursive = true;
    }
    else
    {
        $recursive = false;
    }

    //$sqlDept="AND (".join(" OR ",$deptsSql)." ) ";

    $rsUser = CIntranetUtils::GetDepartmentEmployees($arResult["DEPT"], $recursive, false, 'N');

    while ($arUser = $rsUser->GetNext())
    {
        $arResult["USERS"][$arUser["ID"]] = $arUser;
    }

    $list_user = join(",", array_keys($arResult["USERS"]));

    if ($list_user == "")
    {
        $list_user = "-1";
    }

    $sqlDept="AND b_crm_lead.ASSIGNED_BY_ID IN ($list_user)";

    $from = $DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to = $DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));

    $sqladd_lead = "";

    if ($arResult["LAND"] != "")
    {
        $sqladd_lead .= " AND b_uts_crm_lead." . PROP_LEAD_LAND . "='" . $arResult["LAND"] . "'";
    }

    $sql = "SELECT
        b_uts_crm_lead.".PROP_LEAD_SOURCE." AS `NAME`,
	    COUNT(b_crm_lead.ID) as CNT_LEAD,
	    b_crm_lead.STATUS_ID
      FROM b_crm_lead
	        LEFT JOIN b_uts_crm_lead on(b_uts_crm_lead.VALUE_ID=b_crm_lead.ID)
	  WHERE
	    DATE(b_crm_lead.DATE_CREATE) BETWEEN '$from' AND '$to'
        $sqlDept
        $sqladd_lead
      GROUP BY b_uts_crm_lead.".PROP_LEAD_SOURCE.", b_crm_lead.STATUS_ID
      ORDER BY b_uts_crm_lead.".PROP_LEAD_SOURCE.",b_crm_lead.STATUS_ID
      ";

    $res = $DB->Query($sql);

    $arResult["SQL"]=$sql;

    while ($row = $res->Fetch())
    {
        if(trim($row["NAME"])=="")
            $row["NAME"]="-- не указан --";
        $arResult["RESULT"][$row["NAME"]]["LEAD"][$row["STATUS_ID"]]+=$row["CNT_LEAD"];
        $arResult["RESULT"][$row["NAME"]]["LEAD"]["_TOTAL"]+=$row["CNT_LEAD"];

        $arResult["RESULT"]["_TOTAL"]["LEAD"][$row["STATUS_ID"]]+=$row["CNT_LEAD"];
        $arResult["RESULT"]["_TOTAL"]["LEAD"]["_TOTAL"]+=$row["CNT_LEAD"];
    }


    $sql = "SELECT
        b_uts_crm_lead.".PROP_LEAD_SOURCE." AS `NAME`,
	    COUNT(b_crm_deal.ID) as CNT_DEAL,
	    b_crm_deal.STAGE_ID,
	    SUM(b_crm_deal.OPPORTUNITY) AS `SUM`,
	    b_sale_order.PAYED,
	    COUNT(b_sale_order.ID) AS CNT_ORDER,
	    SUM(b_sale_order.PRICE) AS PRICE
      FROM b_crm_deal
	        INNER JOIN b_crm_lead ON b_crm_deal.LEAD_ID = b_crm_lead.ID
	        LEFT JOIN b_uts_crm_lead on(b_uts_crm_lead.VALUE_ID=b_crm_lead.ID)
	        LEFT JOIN b_uts_order on(b_uts_order.UF_DEAL_ID=b_crm_deal.ID)
	        LEFT JOIN b_sale_order on(b_sale_order.ID = b_uts_order.VALUE_ID)
	  WHERE
	    DATE(b_crm_lead.DATE_CREATE) BETWEEN '$from' AND '$to'
        $sqlDept
        $sqladd_lead
      GROUP BY b_uts_crm_lead.".PROP_LEAD_SOURCE.",b_crm_deal.STAGE_ID,b_sale_order.PAYED
      ORDER BY b_uts_crm_lead.".PROP_LEAD_SOURCE.",b_crm_deal.STAGE_ID,b_sale_order.PAYED
      ";


    $res = $DB->Query($sql);

    $arResult["SQL"]=$sql;

    while ($row = $res->Fetch())
    {
        if(trim($row["NAME"])=="")
            $row["NAME"]="-- не указан --";
        $arResult["RESULT"][$row["NAME"]]["DEAL"][$row["STAGE_ID"]]+=$row["CNT_DEAL"];
        $arResult["RESULT"][$row["NAME"]]["DEAL"]["_TOTAL"]+=$row["CNT_DEAL"];

        $arResult["RESULT"]["_TOTAL"]["DEAL"][$row["STAGE_ID"]]+=$row["CNT_DEAL"];
        $arResult["RESULT"]["_TOTAL"]["DEAL"]["_TOTAL"]+=$row["CNT_DEAL"];

        if($row["STAGE_ID"]=="WON")
        {
            $arResult["RESULT"][$row["NAME"]]["DEAL"]["_SUM"]+=$row["SUM"];
            $arResult["RESULT"]["_TOTAL"]["DEAL"]["_SUM"]+=$row["SUM"];
        }
        $arResult["RESULT"][$row["NAME"]]["ORDER"]["_ALL"]+=$row["CNT_ORDER"];
        $arResult["RESULT"]["_TOTAL"]["ORDER"]["_ALL"]+=$row["CNT_ORDER"];
        if($row["PAYED"]=="Y")
        {
            $arResult["RESULT"][$row["NAME"]]["ORDER"]["_SUM"]+=$row["PRICE"];
            $arResult["RESULT"][$row["NAME"]]["ORDER"]["_PAYED"]+=$row["CNT_ORDER"];
            $arResult["RESULT"]["_TOTAL"]["ORDER"]["_SUM"]+=$row["PRICE"];
            $arResult["RESULT"]["_TOTAL"]["ORDER"]["_PAYED"]+=$row["CNT_ORDER"];
        }

    }

}
$this->IncludeComponentTemplate();
?>