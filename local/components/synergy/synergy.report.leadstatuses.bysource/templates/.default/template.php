<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<script>
var devides=<? echo json_encode($arResult["DEVIDES"]);?>;
$(document).ready(
    function()
    {
        $("#period").change(
            function()
            {
                if($("#period").val()=="interval")
                    $("#interval").css('display', 'inline-block');
                else
                    $("#interval").hide();

                var max_devide=1;
                 switch($("#period").val())
                {
                    case "cmonth":
                    case "pmonth":
                        max_devide=2;
                        break;
                    case "cyear":
                    case "pyear":
                    case "interval":
                        max_devide=3;
                        break;
                }

                $("#devide").empty();
                for(var i=0; i<=max_devide; i++)
                {
                    $("#devide").append("<option value=\""+i+"\">"+devides[i]+"</option>");
                }
            }
        );
        $("#dept").chosen({no_results_text: "Ничего не найдено!"});

        $('#mytable').DataTable({"paging": false, "searching": false,"info": false});

        if($(window).height()<$("#mytable").height())
            $('#h_scroll').height($(window).height()-50);
        else
            $('#h_scroll').height($("#mytable").height()+50);

        var wdthAll = $(".table-result").width();
        var wdth = $('#h_scroll_content').width();
        if(wdthAll>wdth) {
            var b_scroll = $('<div style="overflow-x:scroll; display: none; width:'+wdth+'px"/>');
            var b_cont = $('<div style="height:1px"></div>').width(wdthAll);
            b_scroll.append(b_cont);
            b_scroll.scroll(function(){
                $('#h_scroll').scrollLeft(b_scroll.scrollLeft());
            });
            $('#h_scroll').before(b_scroll);
            $('#h_scroll').scroll(function(){
                b_scroll.scrollLeft($('#h_scroll').scrollLeft());
            });
        }

        $(window).resize(function()
        {
            if($(window).height()<$("#mytable").height())
                $('#h_scroll').height($(window).height()-50);
            else
                $('#h_scroll').height($("#mytable").height()+20);
        });

        $("#mytable td").mouseover(function() {
            var tds = $( this ).parent().find("td"),
            index = $.inArray( this, tds );

            console.log(index);

            $("#mytable td:nth-child("+( index + 1 )+")").addClass("td_hover");
            $("#mytable th:nth-child("+( index + 1 )+")").addClass("td_hover");
        }).mouseout(function() {
            var tds = $( this ).parent().find("td"),

            index = $.inArray( this, tds );

            $("#mytable td:nth-child("+( index + 1 )+")").removeClass("td_hover");
            $("#mytable th:nth-child("+( index + 1 )+")").removeClass("td_hover");
        });



    }

);
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1">
        <input type="hidden" name="mode" value="tbl">
        <div class="form-element">
            <label for="period">Период:</label>
            <select id="period" name="period">
                <? foreach($arResult["PERIODS"] as $k=>$v):?>
                    <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                <? endforeach; ?>
            </select>
        <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "N",
     "HIDE_TIMEBAR" => "Y"
    )
);?>
        </div>
        </div>
        <div class="form-element">
            <label for="rec">Лэнд: </label>
            <input id="land" name="land" type="text" value="<?=$arResult["LAND"]?>">
        </div>
        <div class="form-element">
            <label for='dept'>Отдел:</label>
            <select id='dept' name='dept'>
              <? foreach ($arResult["DEPT_LIST"] as $arItem):?>
                  <? $sel=($arItem["ID"]==$arResult["DEPT"])?" selected":""; ?>
                  <option value="<?=$arItem["ID"]?>"<?=$sel?>><?=(str_repeat("&nbsp;",($arItem["DEPTH_LEVEL"]-1)*4)." ".$arItem["NAME"])?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-element">
            <input id="rec" name="rec" type="checkbox" value="1" <?=($arResult["REC"]==1)?" checked":""?>>
            <label for="rec"> Учитывать подотделы</label>
        </div>

        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Отчет по статусам лидов по источникам</title>
</head>
<body>
<? endif;?>
<div class="result">
<? if($arResult["RESULT"]):?>
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="land" type="hidden" value="<?=$arResult["LAND"]?>">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="period" type="hidden" value="<?=$arResult["PERIOD"]?>">
        <input name="devide" type="hidden" value="<?=$arResult["DEVIDE"]?>">
        <input name="dept" type="hidden" value="<?=$arResult["DEPT"]?>">
        <input name="rec" type="hidden" value="<?=$arResult["REC"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? if($USER->IsAdmin() && false): ?>
            <pre><?=$arResult["SQL"]?></pre>
    <? endif;?>
    <? endif;?>
    <h2><?=$arResult["FROM"]?>-<?=$arResult["TO"]?></h2>
    <? if($arResult["MODE"]!="exl"):?>
        <div id="h_scroll"  style="overflow:scroll;">
        <div id="h_scroll_content">
    <? endif;?>
    <table class="table-result" id="mytable">
        <thead>
    <tr>
        <th>№</th>
        <th>Источник</th>
        <th style="background-color:#ADD8E6;">Общее количество лидов в строке</th>
        <? foreach($arResult["STATUS"] as $arStatusName):?>
        <th><?=$arStatusName?></th>
        <? endforeach;?>
        <th style="background-color:#ADD8E6;">Общее количество сделок в строке</th>
        <? foreach($arResult["DEAL_STAGE"] as $arStageName):?>
            <th><?=$arStageName?></th>
        <? endforeach;?>
        <th>Сумма успешных сделок</th>
        <th>Количество выставленных счетов</th>
        <th>Количество оплаченных счетов</th>
        <th>Сумма оплаченных счетов</th>
        <th>% конверсии из лидов в оплаты</th>
        <th>% конверсии из сделов в оплаты</th>
    </tr>
        </thead>
    <? $iN=1;?>
        <tbody>
    <? foreach($arResult["RESULT"] as $arSource=>$arData):?>
        <? if($arSource=="_TOTAL") continue; ?>
        <tr>
            <td style="text-align: right;"><?=$iN?></td>
            <td style="text-align: left;"><?=$arSource?></td>
            <td style="background-color:#ADD8E6;"><?=$arData["LEAD"]["_TOTAL"]?></td>
            <? foreach($arResult["STATUS"] as $arStatusID=>$arStatusName):?>
                <td><?=intval($arData["LEAD"][$arStatusID])?></td>
            <? endforeach;?>
            <td style="background-color:#ADD8E6;"><?=$arData["DEAL"]["_TOTAL"]?></td>
            <? foreach($arResult["DEAL_STAGE"] as $arStageID=>$arStageName):?>
                <td><?=intval($arData["DEAL"][$arStageID])?></td>
            <? endforeach;?>
            <td><?=number_format($arData["DEAL"]["_SUM"],0,".","")?></td>
            <td><?=number_format($arData["ORDER"]["_ALL"],0,".","")?></td>
            <td><?=number_format($arData["ORDER"]["_PAYED"],0,".","")?></td>
            <td><?=number_format($arData["ORDER"]["_SUM"],0,".","")?></td>
            <td><?=number_format(($arData["ORDER"]["_PAYED"]/$arData["LEAD"]["_TOTAL"])*100,2,",","")?></td>
            <td><?=number_format(($arData["ORDER"]["_PAYED"]/$arData["DEAL"]["_TOTAL"])*100,2,",","")?></td>

        </tr>
        <? $iN++;?>
    <? endforeach;?>
        </tbody>
        <tfoot>
        <tr>
            <td style="text-align: right;">&nbsp;</td>
            <td style="text-align: left;"><b>Итого</b></td>
            <td style="background-color:#ADD8E6;"><b><?=$arResult["RESULT"]["_TOTAL"]["LEAD"]["_TOTAL"]?></b></td>
            <? foreach($arResult["STATUS"] as $arStatusID=>$arStatusName):?>
                <td><b><?=intval($arResult["RESULT"]["_TOTAL"]["LEAD"][$arStatusID])?></b></td>
            <? endforeach;?>
            <td style="background-color:#ADD8E6;"><b><?=$arResult["RESULT"]["_TOTAL"]["DEAL"]["_TOTAL"]?></b></td>
            <? foreach($arResult["DEAL_STAGE"] as $arStageID=>$arStageName):?>
                <td><b><?=intval($arResult["RESULT"]["_TOTAL"]["DEAL"][$arStageID])?></b></td>
            <? endforeach;?>
            <td><b><?=number_format($arResult["RESULT"]["_TOTAL"]["DEAL"]["_SUM"],0,".","")?></b></td>
            <td><b><?=number_format($arResult["RESULT"]["_TOTAL"]["ORDER"]["_ALL"],0,".","")?></b></td>
            <td><b><?=number_format($arResult["RESULT"]["_TOTAL"]["ORDER"]["_PAYED"],0,".","")?></b></td>
            <td><b><?=number_format($arResult["RESULT"]["_TOTAL"]["ORDER"]["_SUM"],0,".","")?></b></td>
            <td><b><?=number_format(($arResult["RESULT"]["_TOTAL"]["ORDER"]["_PAYED"]/$arResult["RESULT"]["_TOTAL"]["LEAD"]["_TOTAL"])*100,2,",","")?></b></td>
            <td><b><?=number_format(($arResult["RESULT"]["_TOTAL"]["ORDER"]["_PAYED"]/$arResult["RESULT"]["_TOTAL"]["DEAL"]["_TOTAL"])*100,2,",","")?></b></td>
        </tr>
        </tfoot>
    </table>
    <? if($arResult["MODE"]!="exl"):?>
    </div>
    </div>
    <? endif;?>
<? elseif($arResult["MODE"]):?>
<b>По Вашему запросу ничего не найдено</b>
<? endif;?>
</div>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>