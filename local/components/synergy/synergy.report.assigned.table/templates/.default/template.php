<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
    <script>
        var devides=<? echo json_encode($arResult["DEVIDES"]);?>;
        $(document).ready(
            function()
            {
                $("#period").change(
                    function()
                    {
                        if($("#period").val()=="interval")
                            $("#interval").css('display', 'inline-block');
                        else
                            $("#interval").hide();

                    }
                );
            }

        );
    </script>
    <div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
    <div class="filter">
        <form method="POST" name="form1">
            <input type="hidden" name="mode" value="tbl">
            <div class="form-element">
                <label for="period">Период:</label>
                <select id="period" name="period">
                    <? foreach($arResult["PERIODS"] as $k=>$v):?>
                        <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                    <? endforeach; ?>
                </select>
                <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
                    <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
                            "SHOW_INPUT" => "Y",
                            "FORM_NAME" => "form1",
                            "INPUT_NAME" => "from",
                            "INPUT_NAME_FINISH" => "to",
                            "INPUT_VALUE" => $arResult["FROM"],
                            "INPUT_VALUE_FINISH" => $arResult["TO"],
                            "SHOW_TIME" => "N",
                            "HIDE_TIMEBAR" => "Y"
                        )
                    );?>
                </div>
            </div>
            <div class="form-element">
                <input type="submit" value="Найти">
            </div>
        </form>
    </div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Отчет по соединенным</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="period" type="hidden" value="<?=$arResult["PERIOD"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? endif;?>
    <table class="table-result">
        <tr>
            <th>Дата соединения</th>
            <th>Дата создания</th>
            <th>Имя</th>
            <th>Продукт</th>
            <th>Телефон</th>
            <th>Ответственный</th>
            <th>Ленд</th>
            <th>Дистпетчер</th>
            <th>Отдел ответственного</th>
        </tr>
        <? foreach($arResult["RESULT"] as $arLead):?>
            <tr>
                <td><?=$arLead["ASSIGNED_DATE"]?></td>
                <td><?=$arLead["DATE_CREATE"]?></td>
                <td><?=$arLead["NAME"]?></td>
                <td><?=$arLead["PRODUCT"]?></td>
                <td><?=$arLead["PHONE"]?></td>
                <td><?=$arLead["ASSIGNED_BY_ID"]?></td>
                <td><?=$arLead["LAND"]?></td>
                <td><?=$arLead["DISP"]?></td>
                <td><?=$arLead["DEPT"]?></td>
            </tr>
        <? endforeach;?>
    </table>
</div>
<? elseif($arResult["MODE"]):?>
<div class="result">
<? if($arResult["ERROR"]):?>
<? foreach($arResult["ERROR"] as $error):?>
<div style="color: red;"><?=$error?></div>
<? endforeach;?>
<?else:?>
<b>По Вашему запросу ничего не найдено</b>
<? endif;?>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>
