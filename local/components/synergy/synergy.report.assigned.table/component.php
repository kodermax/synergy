<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

CJSCore::Init('jquery');

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");

$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PERIOD"]=$_REQUEST["period"];
$arResult["DEVIDE"]=$_REQUEST["devide"];

$arResult["PERIODS"] = Array(
    "cday"=>"Текущий день",
    "pday"=>"Предыдущий день",
    "cweek"=>"Текущая неделя",
    "pweek"=>"Предыдущая неделя",
    "cmonth"=>"Текущий месяц",
    "pmonth"=>"Предыдущий месяц",
    "cyear"=>"Текущий год",
    "pyear"=>"Предыдущий год",
    "interval"=>"Интервал"
);

$arResult["DEPTS"] = [
    3445 => "Диспетчерская Служба",
    3452 => "Отдел продаж №1",
    3455 => "Отдел продаж №2",
    3457 => "Отдел продаж №3",
    3458 => "Отдел продаж №4",
    3459 => "Отдел продаж №5",
    3464 => "Магистратура",
    3461 => "Отдел по взаимодействию с Министерством Обороны и МЧС России",
    3463 =>"Центр языковой подготовки",
    3454=>"Отдел продаж №10"
];

$arResult["DS"]=3445;

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;
}

if(strtotime($arResult["FROM"])<strtotime("04.11.2015 00:00:00"))
    $arResult["ERROR"][]="Дата начала периода должна быть больше либо равна 04.11.2015";

if(strtotime($arResult["FROM"])>strtotime($arResult["TO"]))
    $arResult["ERROR"][]="Дата конца периода должна быть больше либо равна дате начала периода";


if($arResult["MODE"] && !is_array($arResult["ERROR"]))
{

    $rsUser= CUser::GetList($by='ID', $order='ASC', array('UF_DEPARTMENT' => array_keys($arResult["DEPTS"])), array('SELECT' => array('UF_DEPARTMENT')));


    while($arUser=$rsUser->GetNext())
    {
        $arResult["USERS"][$arUser["ID"]]=$arUser;
        $arResult["DEPT_USERS"][$arUser["UF_DEPARTMENT"][0]][$arUser["ID"]]=true;
    }

    $list_user=join(",",array_keys($arResult["USERS"]));
    if($list_user=="")
        $list_user="-1";

    $list_user_ds = join(",",array_keys($arResult["DEPT_USERS"][$arResult["DS"]]));
    if($list_user_ds=="")
        $list_user_ds="-1";

    $from=$DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to=$DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));


    $sql = "
 		SELECT
 		    `crm_lead`.DATE_CREATE AS DATE_CREATE,
 		    `uts`.".PROP_LEAD_ASSIGNED_DATE." as ASSIGNED_DATE,
 		    `uts`.".PROP_LEAD_DISPATCHER." AS DISP,
			`crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
			`crm_contact`.NAME as NAME,
			`crm_multi`.VALUE as PHONE,
			`uts`.".PROP_LEAD_LAND." as `LAND`,
			`uts`.".PROP_LEAD_PRODUCT." as `PRODUCT`
		FROM
			`b_crm_lead` `crm_lead`
			LEFT JOIN `b_uts_crm_lead` `uts` ON (`crm_lead`.ID=`uts`.VALUE_ID)
			LEFT JOIN `b_crm_contact` `crm_contact` ON (`crm_lead`.CONTACT_ID=`crm_contact`.ID)
			LEFT JOIN `b_crm_field_multi` `crm_multi` ON (`crm_lead`.ID=`crm_multi`.ELEMENT_ID AND `crm_multi`.ENTITY_ID='LEAD' AND `crm_multi`.TYPE_ID='PHONE')
		WHERE
		     DATE(`uts`.".PROP_LEAD_ASSIGNED_DATE.") BETWEEN '$from' AND '$to'
		     AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user) AND `uts`.".PROP_LEAD_DISPATCHER." IN ($list_user_ds)
		     AND `uts`.".PROP_LEAD_LAND."!='' AND `uts`.".PROP_LEAD_LAND." is not NULL
		      AND `uts`.".PROP_LEAD_LAND."!='proftest'
		ORDER BY    `uts`.".PROP_LEAD_ASSIGNED_DATE.", `crm_lead`.DATE_CREATE
	";

    $res=$DB->Query($sql);
	while($row=$res->Fetch())
    {
        $row["DATE_CREATE"]=($row["DATE_CREATE"]!="")?$DB->FormatDate($row["DATE_CREATE"],"YYYY-MM-DD HH:MI:SS","DD.MM.YYYY"):"";
        $row["ASSIGNED_DATE"]=($row["ASSIGNED_DATE"]!="")?$DB->FormatDate($row["ASSIGNED_DATE"],"YYYY-MM-DD HH:MI:SS","DD-MM-YYYY"):"";

        $row["DISP"]=(intval($row["DISP"])>0)?$arResult["USERS"][$row["DISP"]]["LAST_NAME"]." ".$arResult["USERS"][$row["DISP"]]["NAME"]:"";
        $row["DEPT"]=(intval($row["ASSIGNED_BY_ID"])>0)?$arResult["DEPTS"][$arResult["USERS"][$row["ASSIGNED_BY_ID"]]["UF_DEPARTMENT"][0]]:"";
        $row["ASSIGNED_BY_ID"]=(intval($row["ASSIGNED_BY_ID"])>0)?$arResult["USERS"][$row["ASSIGNED_BY_ID"]]["LAST_NAME"]." ".
            $arResult["USERS"][$row["ASSIGNED_BY_ID"]]["NAME"]:"";


        $arResult["RESULT"][]=$row;
    }

}
$this->IncludeComponentTemplate();
?>