<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

function getDatesByWeek($_week_number, $_year = null)
{
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CJSCore::Init('jquery');
$APPLICATION->AddHeadScript("/local/assets/chosen/chosen.jquery.min.js", true);
$APPLICATION->SetAdditionalCSS("/local/assets/chosen/chosen.min.css", true);

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

$arResult["FROM"] = ((bool)strtotime($_REQUEST["from"])) ? $_REQUEST["from"] : date("d.m.Y", strtotime("yesterday"));
$arResult["TO"] = ((bool)strtotime($_REQUEST["to"])) ? $_REQUEST["to"] : date("d.m.Y");

$arResult["MODE"] = ($_REQUEST["mode"] == "tbl" || $_REQUEST["mode"] == "exl") ? $_REQUEST["mode"] : null;
$arResult["PERIOD"] = $_REQUEST["period"];
$arResult["PRODUCT"] = trim($_REQUEST["product"]);
$arResult["COMPANY"] = trim($_REQUEST["company"]);
$arResult["LAND"] = trim($_REQUEST["land"]);
$arResult["SOURCE"] = trim($_REQUEST["source"]);
$arResult["STAGE"] = $_REQUEST["stage"];


$arResult["PERIODS"] = Array(
    "cday" => "Текущий день",
    "pday" => "Предыдущий день",
    "cweek" => "Текущая неделя",
    "pweek" => "Предыдущая неделя",
    "cmonth" => "Текущий месяц",
    "pmonth" => "Предыдущий месяц",
    "cyear" => "Текущий год",
    "pyear" => "Предыдущий год",
    "interval" => "Интервал"
);

if (!$_REQUEST["dept"])
{
    $rsUser = CUser::GetList(($by = "ID"), ($order = "desc"), array("ID" => $USER->GetID()), array("SELECT" => array("UF_DEPARTMENT")));
    if ($arUser = $rsUser->Fetch())
    {
        $arResult["DEPT"] = $arUser["UF_DEPARTMENT"][0];
        $arResult["USER"] = -1;
    }
    else
    {
        $arResult["USER"] = -1;
        $arResult["DEPT"] = 1;
    }

}
else
{
    if (preg_match("/usr_(\d+)/", $_REQUEST["dept"], $matches))
    {
        $arResult["USER"] = $matches[1];
        $arResult["DEPT"] = -1;
    }
    else
    {
        $arResult["USER"] = -1;
        $arResult["DEPT"] = $_REQUEST["dept"];
    }

}

$arResult["REC"] = $_REQUEST["rec"];

$rsDept = CIBlockSection::GetTreeList(array("IBLOCK_ID" => 1, "ACTIVE" => "Y"));
while ($arDept = $rsDept->GetNext())
{
    $arResult["DEPT_LIST"][$arDept["ID"]] = $arDept;
}

$rsUser = CUser::GetList(($by = "LAST_NAME"), ($order = "ASC"), ["ACTIVE" => "Y"], ["SELECT" => ["UF_DEPARTMENT"]]);
while ($arUser = $rsUser->GetNext())
{
    if (trim($arUser["LAST_NAME"]) == "") continue;
    if (intval($arUser["UF_DEPARTMENT"][0]) == 0) continue;
    $arResult["USER_LIST"][$arUser["ID"]] = ["ID" => $arUser["ID"], "NAME" => $arUser["NAME"], "LAST_NAME" => $arUser["LAST_NAME"], "SECOND_NAME" => $arUser["SECOND_NAME"], "DEPT"=>$arResult["DEPT_LIST"][$arUser["UF_DEPARTMENT"][0]]["NAME"]];
}

switch ($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"] = $arResult["TO"] = date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"] = $arResult["TO"] = date("d.m.Y", strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"] = date("d.m.Y", strtotime("last Monday"));
        $arResult["TO"] = date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"] = date("d.m.Y", strtotime("last Monday -1 week"));
        $arResult["TO"] = date("d.m.Y", strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"] = "01." . date("m.Y");
        $arResult["TO"] = date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"] = "01." . date("m.Y", strtotime("previous month"));
        $arResult["TO"] = date("d.m.Y", strtotime("01." . date("m.Y") . " -1 day"));
        break;
    case "cyear":
        $arResult["FROM"] = "01.01." . date("Y");
        $arResult["TO"] = date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"] = "01.01." . date("Y", strtotime("previous year"));
        $arResult["TO"] = "31.12." . date("Y", strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"] = ((bool)strtotime($_REQUEST["from"])) ? $_REQUEST["from"] : date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"] = ((bool)strtotime($_REQUEST["to"])) ? $_REQUEST["to"] : date("d.m.Y");
        break;
}

$sql = "SELECT STATUS_ID,NAME,ENTITY_ID FROM b_crm_status AS `status` WHERE `status`.ENTITY_ID='DEAL_STAGE' ORDER BY NAME";

$res = $DB->Query($sql);
while ($row = $res->Fetch())
{
    $arResult["STAGE_LIST"][$row["STATUS_ID"]]=["ID"=>$row["STATUS_ID"],"NAME"=> $row["NAME"]];
}

if ($arResult["MODE"])
{

    if ($arResult["DEPT"] != -1)
    {
        $rsDept = CIBlockSection::GetList(array(), array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "ID" => $arResult["DEPT"]), false, array("ID", "NAME"));
        $arResult["DEPT_DATA"] = $rsDept->Fetch();

        if ($arResult["REC"] == 1)
        {
            $recursive = true;
        }
        else
        {
            $recursive = false;
        }

        $rsUser = CIntranetUtils::GetDepartmentEmployees($arResult["DEPT"], $recursive);

        while ($arUser = $rsUser->GetNext())
        {
            $arResult["USERS"][$arUser["ID"]] = $arUser;
        }

        $list_user = join(",", array_keys($arResult["USERS"]));
    }
    else
    {
        $arResult["USER_DATA"] = $arResult["USER_LIST"][$arResult["USER"]]["LAST_NAME"] . " " . $arResult["USER_LIST"][$arResult["USER"]]["NAME"]. " " . $arResult["USER_LIST"][$arResult["USER"]]["SECOND_NAME"];
        $list_user = $arResult["USER"];
    }

    if ($list_user == "")
    {
        $list_user = "-1";
    }

    $from = $DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to = $DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));

    $sqladd = "";

    if ($arResult["LAND"] != "")
    {
        $sqladd .= " AND `uts`." . PROP_DEAL_LAND . "='" . $arResult["LAND"] . "'";
    }
    if ($arResult["STAGE"] != "")
    {
        $sqladd .= " AND deal.STAGE_ID='" . $arResult["STAGE"] . "'";
    }
    if ($arResult["COMPANY"] != "")
    {
        $sqladd .= " AND `uts`." . PROP_DEAL_CAMPANY . "='" . $arResult["COMPANY"] . "'";
    }
    if ($arResult["SOURCE"] != "")
    {
        $sqladd .= " AND `uts`." . PROP_DEAL_SOURCE . "='" . $arResult["SOURCE"] . "'";
    }

    $sql = "SELECT
        deal.ID,
        deal.ASSIGNED_BY_ID,
        deal.TITLE,
        deal.STAGE_ID,
        uts.".PROP_DEAL_LAND." as LAND,
        uts.".PROP_DEAL_CAMPANY." as COMPANY,
        uts.".PROP_DEAL_COUNTRY." as COUNTRY,
        uts.".PROP_DEAL_CITY." as CITY,
        contact.`VALUE`,
        contact.`TYPE_ID`,
        names.`LAST_NAME`,
        names.`NAME`,
        names.`SECOND_NAME`
FROM b_crm_field_multi contact
          INNER JOIN b_crm_deal deal ON contact.ELEMENT_ID = deal.CONTACT_ID
          INNER JOIN b_crm_contact names ON names.ID = deal.CONTACT_ID
          LEFT JOIN b_uts_crm_deal uts on uts.VALUE_ID=deal.ID
WHERE contact.ENTITY_ID='CONTACT' AND deal.ASSIGNED_BY_ID IN ($list_user)
          AND DATE(deal.DATE_CREATE) BETWEEN '$from' AND '$to'
          $sqladd
ORDER by deal.ID
";

    //echo $sql;

    $res = $DB->Query($sql);
    while ($row = $res->Fetch())
    {
        if($arResult["RESULT"][$row["ID"]])
        {
            $arResult["RESULT"][$row["ID"]][$row["TYPE_ID"]]=($arResult["RESULT"][$row["ID"]][$row["TYPE_ID"]])?
                $arResult["RESULT"][$row["ID"]][$row["TYPE_ID"]].", ".$row["VALUE"]:$row["VALUE"];
        }
        else
        {
            $arResult["RESULT"][$row["ID"]]=[
                "TITLE"=>$row["TITLE"],
                "STAGE"=>$arResult["STAGE_LIST"][$row["STAGE_ID"]]["NAME"],
                "RESPONSIBLE"=>$arResult["USER_LIST"][$row["ASSIGNED_BY_ID"]]["LAST_NAME"] . " " . $arResult["USER_LIST"][$row["ASSIGNED_BY_ID"]]["NAME"] . " " . $arResult["USER_LIST"][$row["ASSIGNED_BY_ID"]]["SECOND_NAME"],
                "RESPONSIBLE_DEPT"=>$arResult["USER_LIST"][$row["ASSIGNED_BY_ID"]]["DEPT"],
                "LAND"=>$row["LAND"],
                "COMPANY"=>$row["COMPANY"],
                "COUNTRY"=>$row["COUNTRY"],
                "CITY"=>$row["CITY"],
                "FULL_NAME"=>trim($row["LAST_NAME"]." ".$row["NAME"]." ".$row["SECOND_NAME"]),
                $row["TYPE_ID"]=>$row["VALUE"]
            ];
        }


    }




}
$this->IncludeComponentTemplate();
?>


