<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if(in_array(18,$USER->GetUserGroupArray()) || in_array(20,$USER->GetUserGroupArray()) || $USER->IsAdmin()):?>
<? if($arResult["MODE"]!="exl"):?>
<script>
    var componentPath = "<?=$componentPath?>";
    $(document).ready(

        function()
        {
            $("#period").change(
                function()
                {
                    if($("#period").val()=="interval")
                        $("#interval").css('display', 'inline-block');
                    else
                        $("#interval").hide();
                }
            );
            $("#dept").chosen({no_results_text: "Ничего не найдено!"});
            $("#workarea-content").css("min-height","1000px");
        }
    );
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1" id="form1">
        <input type="hidden" name="mode" value="tbl">
        <div class="form-element">
        <label for="period">Период:</label>
            <select id="period" name="period">
                <? foreach($arResult["PERIODS"] as $k=>$v):?>
                    <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                <? endforeach; ?>
            </select>
        <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "N",
     "HIDE_TIMEBAR" => "Y"
    )
);?>
        </div>
        </div>
        <div class="form-element">
            <label for='stage'>Стадия:</label>
            <select id='stage' name='stage'>
                <option value="">Все</option>
                <? foreach ($arResult["STAGE_LIST"] as $arItem):?>
                    <? $sel=($arItem["ID"]==$arResult["STAGE"])?" selected":""; ?>
                    <option value="<?=$arItem["ID"]?>"<?=$sel?>><?=$arItem["NAME"]?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-element"  style="display: inline-block !important;">
            <label for='dept'>Отдел / Сотрудник:</label>
            <select id='dept' name='dept'>
              <? foreach ($arResult["DEPT_LIST"] as $arItem):?>
                  <? $sel=($arItem["ID"]==$arResult["DEPT"])?" selected":""; ?>
                  <option value="<?=$arItem["ID"]?>"<?=$sel?>><b><?=(str_repeat("&nbsp;",($arItem["DEPTH_LEVEL"]-1)*4)." ".$arItem["NAME"])?></b></option>
                <? endforeach; ?>
                <? foreach ($arResult["USER_LIST"] as $arItem):?>
                    <? $sel=($arItem["ID"]==$arResult["USER"])?" selected":""; ?>
                    <option value="usr_<?=$arItem["ID"]?>"<?=$sel?>><?=$arItem["LAST_NAME"]." ".$arItem["NAME"]?></b></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-element" style="display: inline-block !important;">
            <input id="rec" name="rec" type="checkbox" value="1" <?=($arResult["REC"]==1)?" checked":""?>>
            <label for="rec"> Учитывать подотделы</label>
        </div>
        <div class="form-element">
            <label for="company">Кампания:</label>
            <input id="company" name="company" type="text" value="<?=$arResult["COMPANY"]?>">
        </div>
        <div class="form-element">
            <label for="land">Ленд:</label>
            <input id="land" name="land" type="text" value="<?=$arResult["LAND"]?>">
        </div>
        <div class="form-element">
            <label for="source">Источник:</label>
            <input id="source" name="source" type="text" value="<?=$arResult["SOURCE"]?>">
        </div>
        <br>
        <br>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Отчет с выгрузкой контактов по сделкам</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="stage" type="hidden" value="<?=$arResult["STAGE"]?>">
        <input name="dept" type="hidden" value="<?=($arResult["DEPT"]!=-1)?$arResult["DEPT"]:"usr_".$arResult["USER"]?>">
        <input name="rec" type="hidden" value="<?=$arResult["REC"]?>">
        <input name="company" type="hidden" value="<?=$arResult["COMPANY"]?>">
        <input name="land" type="hidden" value="<?=$arResult["LAND"]?>">
        <input name="source" type="hidden" value="<?=$arResult["SOURCE"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? endif;?>
    <h2><?=$arResult["FROM"]?>-<?=$arResult["TO"]?></h2>
    <h2><?=($arResult["DEPT"]!=-1)?$arResult["DEPT_DATA"]["NAME"]:$arResult["USER_DATA"]?></h2>
    <table class="table-result">
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Стадия</th>
            <th>Ответственный</th>
            <th>Структурное подразделение</th>
            <th>Полное имя</th>
            <th>Телефоны</th>
            <th>Email</th>
            <th>Лэнд</th>
            <th>Кампания</th>
            <th>Город</th>
            <th>Страна</th>
        </tr>
        <? foreach ($arResult["RESULT"] as $arID=>$arItem):?>
        <tr>
            <td><?=$arID?></td>
            <td><?=$arItem["TITLE"]?></td>
            <td><?=$arItem["STAGE"]?></td>
            <td><?=$arItem["RESPONSIBLE"]?></td>
            <td><?=$arItem["RESPONSIBLE_DEPT"]?></td>
            <td><?=$arItem["FULL_NAME"]?></td>
            <td><?=$arItem["PHONE"]?></td>
            <td><?=$arItem["EMAIL"]?></td>
            <td><?=$arItem["LAND"]?></td>
            <td><?=$arItem["COMPANY"]?></td>
            <td><?=$arItem["CITY"]?></td>
            <td><?=$arItem["COUNTRY"]?></td>
        </tr>
        <? endforeach;?>
    </table>
</div>
<? elseif($arResult["MODE"] && !$arResult["ERROR"]):?>
<div class="result">
<b>По Вашему запросу ничего не найдено</b>
</div>
<? else:?>
<div class="error">
<? foreach ($arResult["ERROR"] as $error):?>
<b><?=$error;?></b>
<? endforeach; ?>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>
<? else: ?>
<div class="error">
    Доступ закрыт
</div>
<? endif;?>