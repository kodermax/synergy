<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 01.12.2015
 * Time: 14:40
 */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

$action = $_REQUEST['action'];

if ($action == 'saveabssettings'){
    $userassistant = $_REQUEST['userassistant'];
    $dis_notify1 = $_REQUEST['dis_notify1'];
    $dis_notify2 = $_REQUEST['dis_notify2'];
    $dis_notify3 = $_REQUEST['dis_notify3'];

//    CUser::GetList(($by="personal_country"), ($order="desc"), array("ID"=>$USER->GetId()), array("SELECT" => array("UF_USER_ASSISTANT")));
    $user = new CUser;
    if (!empty($userassistant)){
        $res = $user->Update($USER->GetId(), array("UF_USER_ASSISTANT"=>$userassistant));
    } else {
        $res = $user->Update($USER->GetId(), array("UF_USER_ASSISTANT"=>""));
    }
    if ($res === true){
//Ищем элемент инфоблока
        $dbEl = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>SCHEDULE_IBLOCK_CODE, 'PROPERTY_USER'=>$USER->GetId()), false, false,
            array('ID', 'IBLOCK_ID', 'PROPERTY_DIS_NOTIFY_1', 'PROPERTY_DIS_NOTIFY_2', 'PROPERTY_DIS_NOTIFY_3'));
        $arEl = $dbEl->fetch();
        if (!empty($arEl)){
            CIBlockElement::SetPropertyValuesEx($arEl['ID'], false, array('DIS_NOTIFY_1'=>$dis_notify1, 'DIS_NOTIFY_2'=>$dis_notify2, 'DIS_NOTIFY_3'=>$dis_notify3));
            echo json_encode(array("res" => 1, 'ID'=>$arEl['ID']));
        } else {
// Если элемент инфобоока не найден, то надо его создавать
// Ищем ID инфоблока по коду
            $ibDB = CIBLock::GetList(array(), array('CODE' => SCHEDULE_IBLOCK_CODE));
            $tmpIB = $ibDB->fetch();

            $arSettings = array();
            $arSettings['IBLOCK_ID'] = $tmpIB['ID'];
            $arSettings['NAME'] = $USER->GetLastName()." ".$USER->GetFirstName();
            $arSettings['ACTIVE'] = "Y";
            $el = new CIBlockElement;
            $res = $el->Add($arSettings);
            if ($res !== false){
                $arProps['USER'] = $USER->GetID();
                $arProps['STD_SCHEDULE'] = 1;
                $arProps['DIS_NOTIFY_1'] = $dis_notify1;
                $arProps['DIS_NOTIFY_2'] = $dis_notify2;
                $arProps['DIS_NOTIFY_3'] = $dis_notify3;
                CIBlockElement::SetPropertyValuesEx($res, $tmpIB['ID'], $arProps);
                echo json_encode(array("res" => 2, 'ID'=>$res, 'USER'=>$arProps['USER']));
            } else {
                echo json_encode(array("res" => -1, "errortxt"=>"Ошибка добавления инфоблока: ".$el->LAST_ERROR));
            }
        }
    }else{
        echo json_encode(array("res" => -1, "errortxt"=>"Ошибка: ".$user->LAST_ERROR));
    }
}