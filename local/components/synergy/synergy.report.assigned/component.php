<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

CJSCore::Init('jquery');

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");

$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PERIOD"]=$_REQUEST["period"];
$arResult["DEVIDE"]=$_REQUEST["devide"];

$arResult["PERIODS"] = Array(
    "cday"=>"Текущий день",
    "pday"=>"Предыдущий день",
    "cweek"=>"Текущая неделя",
    "pweek"=>"Предыдущая неделя",
    "cmonth"=>"Текущий месяц",
    "pmonth"=>"Предыдущий месяц",
    "cyear"=>"Текущий год",
    "pyear"=>"Предыдущий год",
    "interval"=>"Интервал"
);

$arResult["DEVIDES"]=array(
    "0"=>"За весь период",
    "1"=>"По дням",
    "2"=>"По неделям",
    "3"=>"По месяцам"
);

$arResult["DEPTS"] = [
    3445 => "Диспетчерская Служба",
    3452 => "Отдел продаж №1",
    3455 => "Отдел продаж №2",
    3457 => "Отдел продаж №3",
    3458 => "Отдел продаж №4",
    3459 => "Отдел продаж №5",
    3464 => "Магистратура",
    3461 => "Отдел по взаимодействию с Министерством Обороны и МЧС России",
//    3463 =>"Центр языковой подготовки",
    3454=>"Отдел продаж №10"
];

$arResult["DS"]=3445;

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;
}

if(strtotime($arResult["FROM"])<strtotime("04.11.2015 00:00:00"))
    $arResult["ERROR"][]="Дата начала периода должна быть больше либо равна 04.11.2015";

if(strtotime($arResult["FROM"])>strtotime($arResult["TO"]))
    $arResult["ERROR"][]="Дата конца периода должна быть больше либо равна дате начала периода";


if($arResult["MODE"] && !is_array($arResult["ERROR"]))
{

    $rsUser= CUser::GetList($by='ID', $order='ASC', array('UF_DEPARTMENT' => array_keys($arResult["DEPTS"])), array('SELECT' => array('UF_DEPARTMENT')));


    while($arUser=$rsUser->GetNext())
    {
        $arResult["USERS"][$arUser["ID"]]=$arUser;
        $arResult["DEPT_USERS"][$arUser["UF_DEPARTMENT"][0]][$arUser["ID"]]=true;
    }

    $list_user=join(",",array_keys($arResult["USERS"]));
    if($list_user=="")
        $list_user="-1";

    $list_user_ds = join(",",array_keys($arResult["DEPT_USERS"][$arResult["DS"]]));
    if($list_user_ds=="")
        $list_user_ds="-1";

    $from=$DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to=$DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));
    
    // Считаем только статусы без даты назначения
    
    
    $groupby="";
    $select="";

    
    switch($arResult["DEVIDE"])
    {
        case 1:
            $groupby='`YEAR`,`MONTH`,`DAY`,';
            $select="DAYOFMONTH(`crm_lead`.DATE_CREATE) as `DAY`,
        MONTH(`crm_lead`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
        case 2:
            $groupby='`YEAR`,`WEEK`,';
            $select="YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,
        WEEK(`crm_lead`.DATE_CREATE) as `WEEK`,";
            break;
        case 3:
            $groupby='`YEAR`,`MONTH`,';
            $select="MONTH(`crm_lead`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
        case 4:
            $groupby='`YEAR`,';
            $select="YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
    }

    $sql = "
 		SELECT
 		    $select
 		    `uts`.".PROP_LEAD_DISPATCHER." AS `DISPATCHER`,
			`crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
			`crm_lead`.`STATUS_ID` AS `STATUS_ID`,
			COUNT(`crm_lead`.`ASSIGNED_BY_ID`) AS `CNT`
		FROM
			`b_crm_lead` `crm_lead`
			LEFT JOIN `b_uts_crm_lead` `uts` ON (`crm_lead`.ID=`uts`.VALUE_ID)
		WHERE
		     `uts`.".PROP_LEAD_ASSIGNED_DATE." IS NULL AND DATE(`crm_lead`.`DATE_CREATE`) BETWEEN '$from' AND '$to'
		     AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user) AND `uts`.".PROP_LEAD_DISPATCHER." IN ($list_user_ds)
		     AND `uts`.".PROP_LEAD_LAND."!='' AND `uts`.".PROP_LEAD_LAND." is not NULL
		     AND `uts`.".PROP_LEAD_LAND."!='proftest'
		GROUP BY
		    $groupby
		    `DISPATCHER`,
			`ASSIGNED_BY_ID`,
			`STATUS_ID`
	";
    

    $res=$DB->Query($sql);
	while($row=$res->Fetch())
    {
        $user_disp=$arResult["USERS"][$row["DISPATCHER"]]["LAST_NAME"]." ".$arResult["USERS"][$row["DISPATCHER"]]["NAME"];
        $dept_disp=$arResult["USERS"][$row["DISPATCHER"]]["UF_DEPARTMENT"][0];
        $user=$arResult["USERS"][$row["ASSIGNED_BY_ID"]]["LAST_NAME"]." ".$arResult["USERS"][$row["ASSIGNED_BY_ID"]]["NAME"];
        $dept=$arResult["USERS"][$row["ASSIGNED_BY_ID"]]["UF_DEPARTMENT"][0];

        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }

        $arResult["RESULT"][$date][$user_disp]["TOTAL"]+=$row["CNT"];
        $arResult["RESULT_TOTAL"]["TOTAL"]+=$row["CNT"];
        if($row["STATUS_ID"]=="NEW" && $dept==$arResult["DS"])
        {
            $arResult["RESULT"][$date][$user_disp]["NEW"]+=$row["CNT"];
            $arResult["RESULT_TOTAL"]["NEW"]+=$row["CNT"];
        }

        if(in_array($row["STATUS_ID"],["9","5","11","DETAILS"]) && $dept==$arResult["DS"])
        {
            $arResult["RESULT"][$date][$user_disp]["JUNK"]+=$row["CNT"];
            $arResult["RESULT_TOTAL"]["JUNK"]+=$row["CNT"];
        }

        if($row["STATUS_ID"]=="CANNOT_CONTACT" && $dept==$arResult["DS"])
        {
            $arResult["RESULT"][$date][$user_disp]["CANNOT_CONTACT"]+=$row["CNT"];
            $arResult["RESULT_TOTAL"]["CANNOT_CONTACT"]+=$row["CNT"];
        }

    }

    // Считаем только статусы по дате назначения


    $groupby="";
    $select="";


    switch($arResult["DEVIDE"])
    {
        case 1:
            $groupby='`YEAR`,`MONTH`,`DAY`,';
            $select="DAYOFMONTH(`uts`.".PROP_LEAD_ASSIGNED_DATE.") as `DAY`,
        MONTH(`uts`.".PROP_LEAD_ASSIGNED_DATE.") as `MONTH`,
        YEAR(`uts`.".PROP_LEAD_ASSIGNED_DATE.") as `YEAR`,";
            break;
        case 2:
            $groupby='`YEAR`,`WEEK`,';
            $select="YEAR(`uts`.".PROP_LEAD_ASSIGNED_DATE.") as `YEAR`,
        WEEK(`uts`.".PROP_LEAD_ASSIGNED_DATE.") as `WEEK`,";
            break;
        case 3:
            $groupby='`YEAR`,`MONTH`,';
            $select="MONTH(`uts`.".PROP_LEAD_ASSIGNED_DATE.") as `MONTH`,
        YEAR(`uts`.".PROP_LEAD_ASSIGNED_DATE.") as `YEAR`,";
            break;
        case 4:
            $groupby='`YEAR`,';
            $select="YEAR(`uts`.".PROP_LEAD_ASSIGNED_DATE.") as `YEAR`,";
            break;
    }

    $sql = "
 		SELECT
 		    $select
 		    `uts`.".PROP_LEAD_DISPATCHER." AS `DISPATCHER`,
			`crm_lead`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
			`crm_lead`.`STATUS_ID` AS `STATUS_ID`,
			DATE(`uts`.".PROP_LEAD_ASSIGNED_DATE.") AS `ASSIGNED_DATE`,
			DATE(`crm_lead`.`DATE_CREATE`) AS `DATE_CREATE`,
			COUNT(`crm_lead`.`ASSIGNED_BY_ID`) AS `CNT`
		FROM
			`b_crm_lead` `crm_lead`
			LEFT JOIN `b_uts_crm_lead` `uts` ON (`crm_lead`.ID=`uts`.VALUE_ID)
		WHERE
		     DATE(`uts`.".PROP_LEAD_ASSIGNED_DATE.") BETWEEN '$from' AND '$to'
		     AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user) AND `uts`.".PROP_LEAD_DISPATCHER." IN ($list_user_ds)
		     AND `uts`.".PROP_LEAD_LAND."!='' AND `uts`.".PROP_LEAD_LAND." is not NULL
		     AND `uts`.".PROP_LEAD_LAND."!='proftest'
		GROUP BY
		    $groupby
		    `DISPATCHER`,
			`ASSIGNED_BY_ID`,
			`STATUS_ID`,
			`ASSIGNED_DATE`,
			`DATE_CREATE`
	";


    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $user_disp=$arResult["USERS"][$row["DISPATCHER"]]["LAST_NAME"]." ".$arResult["USERS"][$row["DISPATCHER"]]["NAME"];
        $dept_disp=$arResult["USERS"][$row["DISPATCHER"]]["UF_DEPARTMENT"][0];
        $user=$arResult["USERS"][$row["ASSIGNED_BY_ID"]]["LAST_NAME"]." ".$arResult["USERS"][$row["ASSIGNED_BY_ID"]]["NAME"];
        $dept=$arResult["USERS"][$row["ASSIGNED_BY_ID"]]["UF_DEPARTMENT"][0];

        $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $date = date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                break;
        }

        $arResult["RESULT"][$date][$user_disp]["TOTAL"]+=$row["CNT"];
        $arResult["RESULT_TOTAL"]["TOTAL"]+=$row["CNT"];

        if($dept!=$arResult["DS"])
        {
            $arResult["RESULT"][$date][$user_disp]["ASSIGNED"]+=$row["CNT"];
            $arResult["RESULT_TOTAL"]["ASSIGNED"]+=$row["CNT"];
        }

        if($dept!=$arResult["DS"] && $row["ASSIGNED_DATE"]==$row["DATE_CREATE"])
        {
            $arResult["RESULT"][$date][$user_disp]["ASSIGNED_DD"]+=$row["CNT"];
            $arResult["RESULT_TOTAL"]["ASSIGNED_DD"]+=$row["CNT"];
        }

        if(in_array($row["STATUS_ID"],["9","5","11","DETAILS"]) && $dept==$arResult["DS"])
        {
            $arResult["RESULT"][$date][$user_disp]["JUNK"]+=$row["CNT"];
            $arResult["RESULT_TOTAL"]["JUNK"]+=$row["CNT"];
        }

        if($row["STATUS_ID"]=="CANNOT_CONTACT" && $dept==$arResult["DS"])
        {
            $arResult["RESULT"][$date][$user_disp]["CANNOT_CONTACT"]+=$row["CNT"];
            $arResult["RESULT_TOTAL"]["CANNOT_CONTACT"]+=$row["CNT"];
        }
    }
    
}
$this->IncludeComponentTemplate();
?>