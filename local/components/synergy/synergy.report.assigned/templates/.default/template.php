<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
    <script>
        var devides=<? echo json_encode($arResult["DEVIDES"]);?>;
        $(document).ready(
            function()
            {
                $("#period").change(
                    function()
                    {
                        if($("#period").val()=="interval")
                            $("#interval").css('display', 'inline-block');
                        else
                            $("#interval").hide();

                        var max_devide=1;
                        switch($("#period").val())
                        {
                            case "cmonth":
                            case "pmonth":
                                max_devide=2;
                                break;
                            case "cyear":
                            case "pyear":
                            case "interval":
                                max_devide=3;
                                break;
                        }

                        $("#devide").empty();
                        for(var i=0; i<=max_devide; i++)
                        {
                            $("#devide").append("<option value=\""+i+"\">"+devides[i]+"</option>");
                        }
                    }
                );
            }

        );
    </script>
    <div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
    <div class="filter">
        <form method="POST" name="form1">
            <input type="hidden" name="mode" value="tbl">
            <div class="form-element">
                <label for="period">Период:</label>
                <select id="period" name="period">
                    <? foreach($arResult["PERIODS"] as $k=>$v):?>
                        <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                    <? endforeach; ?>
                </select>
                <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
                    <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
                            "SHOW_INPUT" => "Y",
                            "FORM_NAME" => "form1",
                            "INPUT_NAME" => "from",
                            "INPUT_NAME_FINISH" => "to",
                            "INPUT_VALUE" => $arResult["FROM"],
                            "INPUT_VALUE_FINISH" => $arResult["TO"],
                            "SHOW_TIME" => "N",
                            "HIDE_TIMEBAR" => "Y"
                        )
                    );?>
                </div>
            </div>
            <div class="form-element">
                <label for="devide">Разбивка:</label>
                <select id="devide" name="devide">

                    <?
                    $max_devide=1;
                    switch($arResult["PERIOD"])
                    {
                        case "cmonth":
                        case "pmonth":
                            $max_devide=2;
                            break;
                        case "cyear":
                        case "pyear":
                        case "interval":
                            $max_devide=3;
                            break;
                    }
                    ?>
                    <? for($arDiv=0;$arDiv<=$max_devide; $arDiv++):?>
                        <option value="<?=$arDiv?>"<?=($arResult["DEVIDE"]==$arDiv)?" selected":""?>><?=$arResult["DEVIDES"][$arDiv]?></option>
                    <? endfor; ?>
                </select>
            </div>
            <div class="form-element">
                <input type="submit" value="Найти">
            </div>
        </form>
    </div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Отчет по соединенным</title>
</head>
<body>
<? endif;?>

<? if($arResult["RESULT"]):?>
<div class="result">
    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="period" type="hidden" value="<?=$arResult["PERIOD"]?>">
        <input name="devide" type="hidden" value="<?=$arResult["DEVIDE"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? endif;?>
    <table class="table-result">
        <tr style="background-color: #D9D9D9;">
            <th>Дата / Период</th>
            <th>Фио диспетчера</th>
            <th>Всего заявок у диспетчера</th>
            <th>Обработано</th>
            <th>Обработано %</th>
            <th>Эффект</th>
            <th>Соединено</th>
            <th>Соединено %</th>
            <th>Соединено д/д</th>
            <th>Соединено по недозвонам</th>
            <th>Брак</th>
            <th>Брак %</th>
            <th>Недозвоны</th>
            <th>Недозвоны %</th>
        </tr>
        <? foreach($arResult["RESULT"] as $arDate=>$arUser):?>
            <? foreach($arUser as $arUserName=>$arUserData):?>
                <tr>
                    <td style="background-color: #D9D9D9;"><?=$arDate?></td>
                    <td><?=$arUserName?></td>
                    <td><?=intval($arUserData["TOTAL"])?></td>
                    <? $obr=intval($arUserData["TOTAL"])-intval($arUserData["NEW"])?>
                    <td><?=$obr?></td>
                    <td><?=ceil($obr/intval($arUserData["TOTAL"])*100)?></td>
                    <? $effect=intval($arUserData["TOTAL"])-intval($arUserData["JUNK"])?>
                    <td><?=$effect?></td>
                    <td><?=intval($arUserData["ASSIGNED"])?></td>
                    <td><?=ceil(intval($arUserData["ASSIGNED"])/$effect*100)?></td>
                    <td><?=intval($arUserData["ASSIGNED_DD"])?></td>
                    <td><?=intval($arUserData["ASSIGNED"])-intval($arUserData["ASSIGNED_DD"])?></td>
                    <td><?=intval($arUserData["JUNK"])?></td>
                    <td><?=ceil(intval($arUserData["JUNK"])/intval($arUserData["TOTAL"])*100)?></td>
                    <td><?=intval($arUserData["CANNOT_CONTACT"])?></td>
                    <td><?=ceil(intval($arUserData["CANNOT_CONTACT"])/$effect*100)?></td>
                </tr>
            <? endforeach;?>
            <tr  style="background-color: #D9D9D9;">
                <td colspan="2">Итого</td>
                <td><?=intval($arResult["RESULT_TOTAL"]["TOTAL"])?></td>
                <? $obr=intval($arResult["RESULT_TOTAL"]["TOTAL"])-intval($arResult["RESULT_TOTAL"]["NEW"])?>
                <td><?=$obr?></td>
                <td><?=ceil($obr/intval($arResult["RESULT_TOTAL"]["TOTAL"])*100)?></td>
                <? $effect=intval($arResult["RESULT_TOTAL"]["TOTAL"])-intval($arResult["RESULT_TOTAL"]["JUNK"])?>
                <td><?=$effect?></td>
                <td><?=intval($arResult["RESULT_TOTAL"]["ASSIGNED"])?></td>
                <td><?=ceil(intval($arResult["RESULT_TOTAL"]["ASSIGNED"])/$effect*100)?></td>
                <td><?=intval($arResult["RESULT_TOTAL"]["ASSIGNED_DD"])?></td>
                <td><?=intval($arResult["RESULT_TOTAL"]["ASSIGNED"])-intval($arResult["RESULT_TOTAL"]["ASSIGNED_DD"])?></td>
                <td><?=intval($arResult["RESULT_TOTAL"]["JUNK"])?></td>
                <td><?=ceil(intval($arResult["RESULT_TOTAL"]["JUNK"])/intval($arResult["RESULT_TOTAL"]["TOTAL"])*100)?></td>
                <td><?=intval($arResult["RESULT_TOTAL"]["CANNOT_CONTACT"])?></td>
                <td><?=ceil(intval($arResult["RESULT_TOTAL"]["CANNOT_CONTACT"])/$effect*100)?></td>
            </tr>
        <? endforeach;?>
    </table>
</div>
<? elseif($arResult["MODE"]):?>
<div class="result">
<? if($arResult["ERROR"]):?>
<? foreach($arResult["ERROR"] as $error):?>
<div style="color: red;"><?=$error?></div>
<? endforeach;?>
<?else:?>
<b>По Вашему запросу ничего не найдено</b>
<? endif;?>
</div>
<? endif;?>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>
