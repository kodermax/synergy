<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

CJSCore::Init('jquery');

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");

$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PERIOD"]=$_REQUEST["period"];
$arResult["DEVIDE"]=$_REQUEST["devide"];

$arResult["PERIODS"] = Array(
    "cday"=>"Текущий день",
    "pday"=>"Предыдущий день",
    "cweek"=>"Текущая неделя",
    "pweek"=>"Предыдущая неделя",
    "cmonth"=>"Текущий месяц",
    "pmonth"=>"Предыдущий месяц",
    "cyear"=>"Текущий год",
    "pyear"=>"Предыдущий год",
    "interval"=>"Интервал"
);

$arResult["DEVIDES"]=array(
    "0"=>"За весь период",
    "1"=>"По дням",
    "2"=>"По неделям",
    "3"=>"По месяцам"
);

$arResult["DEPTS"] = [
    3445 => "Диспетчерская Служба",
    3452 => "Отдел продаж №1",
    3455 => "Отдел продаж №2",
    3457 => "Отдел продаж №3",
    3458 => "Отдел продаж №4",
    3459 => "Отдел продаж №5",
    3464 => "Магистратура",
    3461 => "Отдел по взаимодействию с Министерством Обороны и МЧС России",
//    3463 =>"Центр языковой подготовки",
    3454=>"Отдел продаж №10"
];

$arResult["DS"]=3445;

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;



}

$arResult["JUNK"]=[
    "9"=>"Ошибка номера",
    "5"=>"Дубль",
    "11"=>"Тест",
    "DETAILS"=>"Повторные заявки"
];

$arResult["CANNOT_CONTACT"]=[
    "CANNOT_CONTACT"=>"Недозвон",
];

/*$junk_statuses=array("JUNK","2","12","13","14","15","16","8","9","5","10","11");
$missed_status="CANNOT_CONTACT";
$assigned_status="ASSIGNED";*/




if($arResult["MODE"])
{

    foreach($arResult["DEPTS"] as $deptID=>$deptName)
    {
        $rsUser= CUser::GetList($by='ID', $order='ASC', array('UF_DEPARTMENT' => $deptID), array('SELECT' => array('UF_DEPARTMENT')));

        while($arUser=$rsUser->GetNext())
        {
            if(!$arResult["USERS"][$arUser["ID"]])
            {
                $arResult["USERS"][$arUser["ID"]]=$deptID;
            }
            $arResult["DEPT_USERS"][$deptID][$arUser["ID"]]=true;
        }
    }


    $list_user=join(",",array_keys($arResult["USERS"]));
    if($list_user=="")
        $list_user="-1";

    $list_user_ds = join(",",array_keys($arResult["DEPT_USERS"][$arResult["DS"]]));
    if($list_user_ds=="")
        $list_user_ds="-1";

    $from=$DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to=$DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));
    $groupby="";
    $select="";

    switch($arResult["DEVIDE"])
    {
        case 1:
            $groupby='`YEAR`,`MONTH`,`DAY`,';
            $select="DAYOFMONTH(`crm_lead`.DATE_CREATE) as `DAY`,
        MONTH(`crm_lead`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
        case 2:
            $groupby='`YEAR`,`WEEK`,';
            $select="YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,
        WEEK(`crm_lead`.DATE_CREATE) as `WEEK`,";
            break;
        case 3:
            $groupby='`YEAR`,`MONTH`,';
            $select="MONTH(`crm_lead`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
        case 4:
            $groupby='`YEAR`,';
            $select="YEAR(`crm_lead`.DATE_CREATE) as `YEAR`,";
            break;
    }

    // Считаем всего

    $sql="
      select
        $select
        COUNT(`crm_lead`.ID) as CNT
      from
        b_crm_lead as `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
        AND `uts`.".PROP_LEAD_DISPATCHER." IN ($list_user_ds)
        AND `crm_lead`.STATUS_ID NOT IN('".join("','",array_keys($arResult["JUNK"]))."')
      GROUP BY $groupby `crm_lead`.`STATUS_ID`
      ORDER BY DATE(`crm_lead`.DATE_CREATE)
    ";

    //echo $sql;

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $date = sprintf("%02d.%02d.%4d", $row["DAY"], $row["MONTH"], $row["YEAR"]);
        switch ($arResult["DEVIDE"])
        {
            case 1:
                $date = sprintf("%02d.%02d.%4d", $row["DAY"], $row["MONTH"], $row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"] + 1, $row["YEAR"]);
                $dates[0] = (strtotime($arResult["FROM"]) > $dates[0]) ? strtotime($arResult["FROM"]) : $dates[0];
                $dates[1] = (strtotime($arResult["TO"]) < $dates[1]) ? strtotime($arResult["TO"]) : $dates[1];
                $date = date("d.m.Y", $dates[0]) . " - " . date("d.m.Y", $dates[1]);
                break;
            case 3:
                $date = sprintf("%02d %4d", $row["MONTH"], $row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if ($arResult["FROM"] == $arResult["TO"])
                {
                    $date = $arResult["FROM"];
                }
                else
                {
                    $date = $arResult["FROM"] . " - " . $arResult["TO"];
                }
                break;
        }
        if (!$arResult["RESULT"][$date]["ALL"])
        {
            $arResult["RESULT"][$date]["ALL"] = 0;
        }
        $arResult["RESULT"][$date]["ALL"] += $row["CNT"];
    }

    // Считаем бракованных и недозвон

    $sql="
      select
        $select
        if(`crm_lead`.STATUS_ID IN('".join("','",array_keys($arResult["JUNK"]))."'),'JUNK','CANNOT_CONTACT') AS STATUS,
        COUNT(`crm_lead`.ID) as CNT
      from
        b_crm_lead as `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
        AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user_ds)
        AND (`crm_lead`.STATUS_ID IN('".join("','",array_keys($arResult["JUNK"]))."')  OR `crm_lead`.STATUS_ID IN('".join("','",array_keys($arResult["CANNOT_CONTACT"]))."'))
      GROUP BY $groupby `crm_lead`.`STATUS_ID`
      ORDER BY DATE(`crm_lead`.DATE_CREATE)
    ";

    //echo $sql;

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $date = sprintf("%02d.%02d.%4d", $row["DAY"], $row["MONTH"], $row["YEAR"]);
        switch ($arResult["DEVIDE"])
        {
            case 1:
                $date = sprintf("%02d.%02d.%4d", $row["DAY"], $row["MONTH"], $row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"] + 1, $row["YEAR"]);
                $dates[0] = (strtotime($arResult["FROM"]) > $dates[0]) ? strtotime($arResult["FROM"]) : $dates[0];
                $dates[1] = (strtotime($arResult["TO"]) < $dates[1]) ? strtotime($arResult["TO"]) : $dates[1];
                $date = date("d.m.Y", $dates[0]) . " - " . date("d.m.Y", $dates[1]);
                break;
            case 3:
                $date = sprintf("%02d %4d", $row["MONTH"], $row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if ($arResult["FROM"] == $arResult["TO"])
                {
                    $date = $arResult["FROM"];
                }
                else
                {
                    $date = $arResult["FROM"] . " - " . $arResult["TO"];
                }
                break;
        }
        if (!$arResult["RESULT"][$date][$row["STATUS"]])
        {
            $arResult["RESULT"][$date][$row["STATUS"]] = 0;
        }
        $arResult["RESULT"][$date][$row["STATUS"]] += $row["CNT"];
    }

    // Считаем всего соединенных

    $sql="
      select
        $select
        COUNT(`crm_lead`.ID) as CNT
      from
        b_crm_lead as `crm_lead`
        left join b_uts_crm_lead as `uts` on(`uts`.VALUE_ID=`crm_lead`.ID)
      WHERE
        DATE(`crm_lead`.DATE_CREATE) BETWEEN '$from' AND '$to'
        AND `crm_lead`.`ASSIGNED_BY_ID` IN ($list_user) AND `crm_lead`.`ASSIGNED_BY_ID` NOT IN ($list_user_ds)
        AND `uts`.".PROP_LEAD_DISPATCHER." IN ($list_user_ds)
        AND `crm_lead`.`STATUS_ID`!='DETAILS'
      GROUP BY $groupby `crm_lead`.`STATUS_ID`
      ORDER BY DATE(`crm_lead`.DATE_CREATE)
    ";

    //echo $sql;

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
    {
        $date = sprintf("%02d.%02d.%4d", $row["DAY"], $row["MONTH"], $row["YEAR"]);
        switch ($arResult["DEVIDE"])
        {
            case 1:
                $date = sprintf("%02d.%02d.%4d", $row["DAY"], $row["MONTH"], $row["YEAR"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"] + 1, $row["YEAR"]);
                $dates[0] = (strtotime($arResult["FROM"]) > $dates[0]) ? strtotime($arResult["FROM"]) : $dates[0];
                $dates[1] = (strtotime($arResult["TO"]) < $dates[1]) ? strtotime($arResult["TO"]) : $dates[1];
                $date = date("d.m.Y", $dates[0]) . " - " . date("d.m.Y", $dates[1]);
                break;
            case 3:
                $date = sprintf("%02d %4d", $row["MONTH"], $row["YEAR"]);
                break;
            case 4:
                $date = $row["YEAR"];
                break;
            case 0:
            default:
                if ($arResult["FROM"] == $arResult["TO"])
                {
                    $date = $arResult["FROM"];
                }
                else
                {
                    $date = $arResult["FROM"] . " - " . $arResult["TO"];
                }
                break;
        }
        if (!$arResult["RESULT"][$date]["ASSIGNED"])
        {
            $arResult["RESULT"][$date]["ASSIGNED"] = 0;
        }
        $arResult["RESULT"][$date]["ASSIGNED"] += $row["CNT"];
    }

}
$this->IncludeComponentTemplate();
?>