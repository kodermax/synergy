$(document).ready(function(){

	$(".searchLead .searchQuery").keyup(function(){		if(!$(this).hasClass("searchByEmail")){

			var value = parseInt($(this).val());

			if(isNaN(value)){				var newVal = "";
			}else{				var newVal = value;
			}

        	$(this).val(newVal);		}	});

	$(".searchBy input:radio").change(function(){
		var searchBy = $(this).val();
		if(searchBy == "phone"){
//			$(".searchLead .searchQuery").mask("99999999999");
			$(".searchLead .searchQuery").removeClass("searchByEmail").removeClass("bad");
		}else if(searchBy == "email"){
//			$(".searchLead .searchQuery").unmask();
			$(".searchLead .searchQuery").addClass("searchByEmail");
		}
	});

	$(".searchLead input.searchQuery").keyup(function(){
		var query = $(this).val();

		if($(this).hasClass("searchByEmail")){
			if(isValidEmail(query) == true){
	        	$(this).removeClass("bad");
	        	$(this).addClass("good");
			}else{
	        	$(this).removeClass("good");
	        	$(this).addClass("bad");
			}
		}

	});

	$(document).on("click",".searchLead .searchByContats .searchSubmit",function(){

		if($(this).parents(".searchByContats").find("input.searchQuery").val()){

			if($(this).parents(".searchLead").find("input:text.searchQuery").hasClass("bad")){
				alert("Некорректный e-mail");
			}else{

				var dataSearchAjax = {};
				dataSearchAjax["searchBy"] = $(".searchBy input:checked").val();
				dataSearchAjax["typeSearch"] = "searchByContats";
				dataSearchAjax["query"] = $(this).parents(".searchLead").find("input.searchQuery").val();

				$.ajax({
					type:'POST',
					url:componentPath+'/searchAjax.php',
					beforeSend:function(){
						$(".preloader").show();
					},
					success:function(data){
		            	$(".ajax-load").html(data);
						$(".preloader").hide();
					},
					data:dataSearchAjax
				});

			}

		}else{
			alert("Введите запрос");
		}

	});






	$(document).on("click",".searchLead .searchByFio .searchSubmit",function(){

		if($(this).parents(".searchByFio").find("input.searchByFio").val()){

			var dataSearchAjax = {};
			dataSearchAjax["typeSearch"] = "searchByFio";
			dataSearchAjax["query"] = $(this).parents(".searchByFio").find("input.searchByFio").val();

			console.log(dataSearchAjax);

			$.ajax({
				type:'POST',
				url:componentPath+'/searchAjax.php',
				beforeSend:function(){
					$(".preloader").show();
				},
				success:function(data){
	            	$(".ajax-load").html(data);
					$(".preloader").hide();
				},
				data:dataSearchAjax
			});

		}else{
			alert("Введите запрос");
		}

	});





	$(".typeSearch input:radio").change(function(){
		var value = $(this).val();

		if(value == "contacts"){        	$(".searchLead .searchByContats").show();
        	$(".searchLead .searchByFio").hide();		}else if(value == "fio"){        	$(".searchLead .searchByContats").hide();
        	$(".searchLead .searchByFio").show();		}
	});

});