<? //define("LOG_FILENAME",$_SERVER["DOCUMENT_ROOT"]."/crm/lead/search.log"); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<?

CModule::IncludeModule("crm");
CModule::IncludeModule("iblock");


if ($_POST["typeSearch"] == "searchByContats")
{
    $arFilter["!ASSIGNED_BY_ID"] = USER_ID_FREE_CONTACT;

    if ($_POST["searchBy"] == "email")
    {
        $email = str_replace(" ", "", $_POST["query"]);
        $email = mb_strtolower(trim($email));
        $arFilter["FM"]["EMAIL"]["VALUE"] = $email;
    }
    elseif ($_POST["searchBy"] == "phone")
    {
        $phone = preg_replace("/\D/", "", $_POST["query"]);
        if(preg_match("/^[78]\d{10}/",$phone))
        {
            $phone = "_".substr($phone, 1);
            $arFilter["FM"]["PHONE"]["%VALUE"]=$phone;
        }
        else
            $arFilter["FM"]["PHONE"]["VALUE"]=$phone;
    }
}
elseif ($_POST["typeSearch"] == "searchByFio")
{
    $query = $_POST["query"];

    $arQuery = explode(" ", $query);

    if (count($arQuery) == 2)
    {

        $query2 = $arQuery[1] . " " . $arQuery[0];

        $arFilter = Array("FULL_NAME" => Array("%" . $query . "%", "%" . $query2 . "%"));

    }
    elseif (count($arQuery) == 3)
    {

        $arQuery = explode(" ", $query);

        $query1 = $arQuery[0] . " " . $arQuery[1];
        $query2 = $arQuery[0] . " " . $arQuery[2];
        $query3 = $arQuery[1] . " " . $arQuery[0];
        $query4 = $arQuery[1] . " " . $arQuery[2];
        $query5 = $arQuery[2] . " " . $arQuery[0];
        $query6 = $arQuery[2] . " " . $arQuery[1];

        $arFilter = Array("FULL_NAME" => Array("%" . $query1 . "%", "%" . $query2 . "%", "%" . $query3 . "%", "%" . $query4 . "%", "%" . $query5 . "%", "%" . $query6 . "%"));

    }
    else
    {
        $arFilter = Array(
            "FULL_NAME" => "%" . $_POST["query"] . "%"
        );
    }


}
else
{
    $arFilter["ID"] = -1;
}

$noDeal=true;

$statuses = array();

$rsStatus = CCrmStatus::GetList();
while ($arStatus = $rsStatus->GetNext())
{
    $statuses[$arStatus["ENTITY_ID"]][$arStatus["STATUS_ID"]] = $arStatus["NAME"];
}

$arOrder = Array(
    "ID" => "DESC"
);

$arSelect = Array(
    "ID", "NAME", "LAST_NAME", "SECOND_NAME", "FULL_NAME", "ASSIGNED_BY_ID", "DATE_CREATE", "COMPANY_TITLE",
    "UF_CRM_1421764447","UF_CRM_1421763800" // класс и учебное заведение
);

$objContact = CCrmContact::GetList($arOrder, $arFilter, $arSelect, 30);

$UFs_CONTACT = array();

while ($arrContact = $objContact->Fetch())
{

    if ($arrContact["UF_CRM_1421764447"] && !$UFs_CONTACT["CLASS"][$arrContact["UF_CRM_1421764447"]])
    {
        $rsClass = CUserFieldEnum::GetList(array(), array("ID" => $arrContact["UF_CRM_1421764447"]));
        if ($arClass = $rsClass->GetNext())
        {
            $UFs_CONTACT["CLASS"][$arrContact["UF_CRM_1421764447"]] = $arClass["VALUE"];
            $arrContact["CLASS"] = $UFs_CONTACT["CLASS"][$arrContact["UF_CRM_1421764447"]];
        }
    }
    elseif ($arrContact["UF_CRM_1421764447"] && $UFs_CONTACT["CLASS"][$arrContact["UF_CRM_1421764447"]])
    {
        $arrContact["CLASS"] = $UFs_CONTACT["CLASS"][$arrContact["UF_CRM_1421764447"]];
    }

    if(is_array($arrContact["UF_CRM_1421763800"]))
        $arrContact["UF_CRM_1421763800"]=preg_replace("/\D/","",$arrContact["UF_CRM_1421763800"][0]);

    if ($arrContact["UF_CRM_1421763800"] && !$UFs_CONTACT["INST"][$arrContact["UF_CRM_1421763800"]])
    {
        if ($arInst = CCrmCompany::GetByID($arrContact["UF_CRM_1421763800"]))
        {
            $UFs_CONTACT["INST"][$arrContact["UF_CRM_1421763800"]] = $arInst["TITLE"];
            $arrContact["INST"] = $UFs_CONTACT["INST"][$arrContact["UF_CRM_1421763800"]];
        }
    }
    elseif ($arrContact["UF_CRM_1421763800"] && $UFs_CONTACT["INST"][$arrContact["UF_CRM_1421763800"]])
    {
        $arrContact["INST"] = $UFs_CONTACT["INST"][$arrContact["UF_CRM_1421763800"]];
    }

    $arContacts[$arrContact["ID"]] = $arrContact;
}

//AddMessage2Log($arContacts);

if ($arContacts)
{
    $objField = CCrmFieldMulti::GetList(array(), array("ENTITY_ID" => "CONTACT", "ELEMENT_ID" => array_keys($arContacts), "TYPE_ID" => array("EMAIL", "PHONE")));
    while ($arField = $objField->GetNext())
    {
        $arContacts[$arField["ELEMENT_ID"]][$arField["TYPE_ID"]][] = $arField;
    }

    $objDeal = CCrmDeal::GetList(array(), array("CONTACT_ID" => array_keys($arContacts), $arSelect = Array("ID", "TITLE", "ASSIGNED_BY_ID", "DATE_CREATE", "STAGE_ID","CONTACT_ID")));
    while ($arDeal = $objDeal->GetNext())
    {
        $arDeal["STAGE_NAME"]=$statuses["DEAL_STAGE"][$arDeal["STAGE_ID"]];
        $arContacts[$arDeal["CONTACT_ID"]]["DEAL"][] = $arDeal;
        $noDeal=false;
    }

}

$arSelect = Array(
    "ID", "NAME", "TITLE", "LAST_NAME", "SECOND_NAME", "FULL_NAME", "ASSIGNED_BY_ID", "DATE_CREATE", "STATUS_ID", "COMPANY_TITLE",
    "UF_CRM_1423056666","UF_CRM_1423056426" // Класс и учебное заведение
);

$objLead = CCrmLead::GetList($arOrder, $arFilter, $arSelect, 30);

$UFs_LEAD = array();

while ($arrLead = $objLead->Fetch())
{
    if ($arrLead["UF_CRM_1423056666"] && !$UFs_LEAD["CLASS"][$arrLead["UF_CRM_1423056666"]])
    {
        $rsClass = CUserFieldEnum::GetList(array(), array("ID" => $arrLead["UF_CRM_1423056666"]));
        if ($arClass = $rsClass->GetNext())
        {
            $UFs_LEAD["CLASS"][$arrLead["UF_CRM_1423056666"]] = $arClass["VALUE"];
            $arrLead["CLASS"] = $UFs_LEAD["CLASS"][$arrLead["UF_CRM_1423056666"]];
        }
    }
    elseif ($arrLead["UF_CRM_1423056666"] && $UFs_LEAD["CLASS"][$arrLead["UF_CRM_1423056666"]])
    {
        $arrLead["CLASS"] = $UFs_LEAD["CLASS"][$arrLead["UF_CRM_1423056666"]];
    }

    if(is_array($arrLead["UF_CRM_1423056426"]))
        $arrLead["UF_CRM_1423056426"]=preg_replace("/\D/","",$arrLead["UF_CRM_1423056426"][0]);

    if ($arrLead["UF_CRM_1423056426"] && !$UFs_LEAD["INST"][$arrLead["UF_CRM_1423056426"]])
    {
        if ($arInst = CCrmCompany::GetByID($arrLead["UF_CRM_1423056426"]))
        {
            $UFs_LEAD["INST"][$arrLead["UF_CRM_1423056426"]] = $arInst["TITLE"];
            $arrLead["INST"] = $UFs_LEAD["INST"][$arrLead["UF_CRM_1423056426"]];
        }
    }
    elseif ($arrLead["UF_CRM_1423056426"] && $UFs_LEAD["INST"][$arrLead["UF_CRM_1423056426"]])
    {
        $arrLead["INST"] = $UFs_LEAD["INST"][$arrLead["UF_CRM_1423056426"]];
    }

    $arrLead["STATUS_NAME"] = $statuses["STATUS"][$arrLead["STATUS_ID"]];

    $arLeads[$arrLead["ID"]] = $arrLead;
}
if ($arLeads)
{
    $objField = CCrmFieldMulti::GetList(array(), array("ENTITY_ID" => "LEAD", "ELEMENT_ID" => array_keys($arLeads), "TYPE_ID" => array("EMAIL", "PHONE")));
    while ($arField = $objField->GetNext())
    {
        $arLeads[$arField["ELEMENT_ID"]][$arField["TYPE_ID"]][] = $arField;
    }
}

?>
    <p><b>Контакты:</b></p>
<? if ($arContacts != "error" and count($arContacts) > 0): ?>

    <table>
        <tr>
            <td>
                <b>ID</b>
            </td>
            <td>
                <b>Контакт</b>
            </td>
            <td>
                <b>Дата создания</b>
            </td>
            <td>
                <b>Ответственный</b>
            </td>
            <td>
                <b>Подразделение ответственного</b>
            </td>
            <td>
                <b>Компания</b>
            </td>
            <td>
                <b>Учебное заведение</b>
            </td>
            <td>
                <b>Класс</b>
            </td>
            <td>
                <b>Телефон</b>
            </td>
            <td>
                <b>Email</b>
            </td>
            <td>
                <b>Сделки</b>
            </td>

        </tr>
        <? foreach ($arContacts as $arContact): ?>
            <? $objUserParams = $USER->GetById($arContact["ASSIGNED_BY_ID"]);

            unset($objDepartments);

            if ($userParams = $objUserParams->Fetch())
            {
                $objDepartments = CIBlockSection::GetNavChain(1, $userParams["UF_DEPARTMENT"][0]);
            }
            ?>
            <tr>
                <td><?= $arContact["ID"] ?></td>
                <td><a target="_blank"
                       href="/crm/contact/show/<?= $arContact["ID"] ?>/"><b><?= $arContact["NAME"] ?> <?= $arContact["SECOND_NAME"] ?> <?= $arContact["LAST_NAME"] ?></b></a>
                </td>
                <td align="center"><?= ConvertDateTime($arContact["DATE_CREATE"], "DD.MM.YYYY") ?></td>
                <td><a target="_blank"
                       href="/company/personal/user/<?= $userParams["ID"] ?>/"><?= $userParams["NAME"] ?> <?= $userParams["LAST_NAME"] ?></a>
                </td>
                <td>

                    <? while ($objDepartments && $arDepartment = $objDepartments->ExtractFields()):
                        ?>
                        <a target="_blank"
                           href="/company/structure.php?structure_UF_DEPARTMENT=<?= $arDepartment["ID"] ?>"><?= $arDepartment["NAME"] ?></a><? if ($arDepartment["ID"] != $userParams["UF_DEPARTMENT"][0]): ?> /<? endif; ?>
                    <? endwhile; ?>

                </td>
                <td><?= $arContact["COMPANY_TITLE"] ?></td>
                <td><a target="_blank"
                       href="/crm/company/show/<?= $arContact["UF_CRM_1421763800"] ?>/"><?= $arContact["INST"] ?></a>
                </td>
                <td><?= $arContact["CLASS"] ?></td>
                <td>
                    <? foreach ($arContact["PHONE"] as $ph): ?>
                        <?= $ph["VALUE"] ?><br/>
                    <? endforeach; ?>
                </td>
                <td>
                    <? foreach ($arContact["EMAIL"] as $ph): ?>
                        <?= $ph["VALUE"] ?><br/>
                    <? endforeach; ?>
                </td>
                <td>
                    <? foreach ($arContact["DEAL"] as $dl): ?>
                        <a target="_blank"
                           href="/crm/deal/show/<?= $dl["ID"] ?>/">
                        <?= $dl["TITLE"] ?>
                        </a><br/>
                    <? endforeach; ?>
                </td>
            </tr>
        <? endforeach; ?>
    </table>

<? else: ?>
    <p>Ничего не найдено</p>
<? endif; ?>


    <p><b>Лиды:</b></p>
<? if ($arLeads != "error" and count($arLeads) > 0): ?>
    <table>
        <tr>
            <td>
                <b>ID</b>
            </td>
            <td>
                <b>Лид</b>
            </td>
            <td>
                <b>ФИО</b>
            </td>
            <td>
                <b>Дата создания</b>
            </td>
            <td>
                <b>Статус</b>
            </td>
            <td>
                <b>Ответственный</b>
            </td>
            <td>
                <b>Подразделение ответственного</b>
            </td>
            <td>
                <b>Компания</b>
            </td>
            <td>
                <b>Учебное заведение</b>
            </td>
            <td>
                <b>Класс</b>
            </td>
            <td>
                <b>Телефон</b>
            </td>
            <td>
                <b>Email</b>
            </td>
        </tr>
        <? foreach ($arLeads as $arLead): ?>
            <? $objUserParams = $USER->GetById($arLead["ASSIGNED_BY_ID"]);

            unset($objDepartments);

            if ($userParams = $objUserParams->Fetch())
            {
                $objDepartments = CIBlockSection::GetNavChain(1, $userParams["UF_DEPARTMENT"][0]);
                $arAssigned[$arLead["ID"]] = $userParams;
            }
            ?>

            <tr>
                <td><?= $arLead["ID"] ?></td>
                <td><a target="_blank" href="/crm/lead/show/<?= $arLead["ID"] ?>/"><b><?= $arLead["TITLE"] ?></b></a>
                <td><?= $arLead["NAME"] ?> <?= $arLead["SECOND_NAME"] ?> <?= $arLead["LAST_NAME"] ?></a>
                </td>
                </td>
                <td align="center"><?= $arLead["DATE_CREATE"] ?></td>
                <td><?= $arLead["STATUS_NAME"] ?></td>
                <td><a target="_blank"
                       href="/company/personal/user/<?= $arLead["ASSIGNED_BY_ID"] ?>/"><?= $arAssigned[$arLead["ID"]]["NAME"] ?> <?= $arAssigned[$arLead["ID"]]["LAST_NAME"] ?></a>
                </td>
                <td>

                    <? while ($objDepartments && $arDepartment = $objDepartments->ExtractFields()): ?>
                        <a target="_blank"
                           href="/company/structure.php?structure_UF_DEPARTMENT=<?= $arDepartment["ID"] ?>"><?= $arDepartment["NAME"] ?></a><? if ($arDepartment["ID"] != $userParams["UF_DEPARTMENT"][0]): ?> /<? endif; ?>
                    <? endwhile; ?>

                </td>
                <td>
                    <?= $arLead["COMPANY_TITLE"] ?>
                </td>
                <td><a target="_blank"
                       href="/crm/company/show/<?= $arLead["UF_CRM_1423056426"] ?>/"><?= $arLead["INST"] ?></a>
                </td>
                <td>
                    <?= $arLead["CLASS"] ?>
                </td>
                <td>
                    <? foreach ($arLead["PHONE"] as $ph): ?>
                        <?= $ph["VALUE"] ?><br/>
                    <? endforeach; ?>
                </td>
                <td>
                    <? foreach ($arLead["EMAIL"] as $ph): ?>
                        <?= $ph["VALUE"] ?><br/>
                    <? endforeach; ?>
                </td>
            </tr>
        <? endforeach; ?>
    </table>

<? else: ?>
    <p>Ничего не найдено</p>
<? endif; ?>

<p><b>Сделки:</b></p>
<? if (count($arContacts) > 0 && !$noDeal): ?>
    <table>
        <tr>
            <td>
                <b>ID</b>
            </td>
            <td>
                <b>ФИО контакта</b>
            </td>
            <td>
                <b>Дата создания</b>
            </td>
            <td>
                <b>Стадия</b>
            </td>
            <td>
                <b>Ответственный</b>
            </td>
            <td>
                <b>Подразделение ответственного</b>
            </td>
            <td>
                <b>Название сделки</b>
            </td>
        </tr>
        <? foreach ($arContacts as $arContact): ?>
            <? foreach ($arContact["DEAL"] as $arDeal): ?>
            <? $objUserParams = $USER->GetById($arDeal["ASSIGNED_BY_ID"]);
                unset($objDepartments);
                if ($userParams = $objUserParams->Fetch())
                {
                    $objDepartments = CIBlockSection::GetNavChain(1, $userParams["UF_DEPARTMENT"][0]);
                    $arAssigned[$arDeal["ID"]] = $userParams;
                }

            ?>

            <tr>
                <td><?= $arDeal["ID"] ?></td>
                <td><a target="_blank"
                       href="/crm/contact/show/<?= $arContact["ID"] ?>/"><b><?= $arContact["NAME"] ?> <?= $arContact["SECOND_NAME"] ?> <?= $arContact["LAST_NAME"] ?></b></a>
                </td>
                <td align="center"><?= $arDeal["DATE_CREATE"] ?></td>
                <td><?= $arDeal["STAGE_NAME"] ?></td>
                <td><a target="_blank"
                       href="/company/personal/user/<?= $arDeal["ASSIGNED_BY_ID"] ?>/"><?= $arAssigned[$arDeal["ID"]]["NAME"] ?> <?= $arAssigned[$arDeal["ID"]]["LAST_NAME"] ?></a>
                </td>
                <td>

                    <? while ($objDepartments && $arDepartment = $objDepartments->ExtractFields()): ?>
                        <a target="_blank"
                           href="/company/structure.php?structure_UF_DEPARTMENT=<?= $arDepartment["ID"] ?>"><?= $arDepartment["NAME"] ?></a><? if ($arDepartment["ID"] != $userParams["UF_DEPARTMENT"][0]): ?> /<? endif; ?>
                    <? endwhile; ?>

                </td>
                <td>
                        <a target="_blank"
                           href="/crm/deal/show/<?= $arDeal["ID"] ?>/">
                            <?= $arDeal["TITLE"] ?>
                        </a>
                </td>

            </tr>
        <? endforeach; ?>
        <? endforeach; ?>
    </table>

<? else: ?>
    <p>Ничего не найдено</p>
<? endif; ?>
