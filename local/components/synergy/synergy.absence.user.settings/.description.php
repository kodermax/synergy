<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 10.11.2015
 * Time: 08:57
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Synergy: Настройки контроля дисциплины",
    "DESCRIPTION" => 'Настройки блока контроля дисциплины',
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "intranet",
        "NAME" => "Корпоративный портал",
        "CHILD" => array(
            "ID" => "timeman",
            "NAME" => "Учет рабочего времени",
        ),
    ),
);