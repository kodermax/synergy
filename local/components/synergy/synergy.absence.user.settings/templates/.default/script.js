/**
 * Created by Admin on 04.12.2015.
 */
$(function(){
    //$('.schedule-data-table').dataTable();
    $('#ajax_container').on("click", ".bx-user-info-name", function(){
        var tmpIBId = $(this).parents('.user_data').attr('rel');
        $('#User').text($(this).text());
        $('#hdn_UserName').val($(this).text()); // Сразу сохраним имя пользователя, чтобы потом черз php его не искать
        if (tmpIBId != "") {
            $.ajax({
                type: "POST",
                url: "/local/components/synergy/synergy.absence.user.settings/ajax.php",
                data: "ACTION=getabsencedata&ID=" + $(this).parents('.user_data').attr('rel'),
                success: function (prmData) {
                    obj = $.parseJSON(prmData);
                    var frm = $('#frm_iblock_edit');
                    frm.find('[name=PROPERTY_START_1]').val(obj.PROPERTY_START_1_VALUE);
                    frm.find('[name=PROPERTY_START_2]').val(obj.PROPERTY_START_2_VALUE);
                    frm.find('[name=PROPERTY_START_3]').val(obj.PROPERTY_START_3_VALUE);
                    frm.find('[name=PROPERTY_START_4]').val(obj.PROPERTY_START_4_VALUE);
                    frm.find('[name=PROPERTY_START_5]').val(obj.PROPERTY_START_5_VALUE);
                    frm.find('[name=PROPERTY_START_6]').val(obj.PROPERTY_START_6_VALUE);
                    frm.find('[name=PROPERTY_START_7]').val(obj.PROPERTY_START_7_VALUE);

                    frm.find('[name=PROPERTY_END_1]').val(obj.PROPERTY_END_1_VALUE);
                    frm.find('[name=PROPERTY_END_2]').val(obj.PROPERTY_END_2_VALUE);
                    frm.find('[name=PROPERTY_END_3]').val(obj.PROPERTY_END_3_VALUE);
                    frm.find('[name=PROPERTY_END_4]').val(obj.PROPERTY_END_4_VALUE);
                    frm.find('[name=PROPERTY_END_5]').val(obj.PROPERTY_END_5_VALUE);
                    frm.find('[name=PROPERTY_END_6]').val(obj.PROPERTY_END_6_VALUE);
                    frm.find('[name=PROPERTY_END_7]').val(obj.PROPERTY_END_7_VALUE);

                    $('#inp_SaltoID').val(obj.PROPERTY_SALTO_ID_VALUE);

                    $('#hdn_IBlockID').val(obj.ID);
                    $('#hdn_UserID').val(obj.PROPERTY_USER_VALUE);


                    if (obj.PROPERTY_STD_SCHEDULE_VALUE == 1){
                        $('#hdn_StdSchedule').val("1");
                        $('#chb_StdSchedule').prop("checked", "checked");

                        $('.table-shcedule').css('color', '#ccc');
                        $('.table-shcedule input:not(#chb_StdSchedule, #inp_SaltoID, #hdn_StdSchedule, #chb_NotNotify)').prop('disabled', true);
                        $('.bx-clock-icon').css('display', 'none');
                    } else{
                        $('#hdn_StdSchedule').val("0");
                        $('#chb_StdSchedule').removeAttr("checked");

                        $('.table-shcedule').css('color', '#000');
                        $('.table-shcedule input').prop('disabled', false);
                        $('.bx-clock-icon').css('display', 'inline-block');
                    }

                    if (obj.PROPERTY_DIS_ALL_NOTIFY_VALUE == 1){
                        $('#hdn_NotNotify').val("1");
                        $('#chb_NotNotify').prop("checked", "checked");
                    } else {
                        $('#hdn_NotNotify').val("0");
                    }
                    $('#lbl_StdSchedule').css('color', '#000');
                    $('.bottom-layer').fadeIn(200);
                    $('#frm-edit').fadeIn(200);
                }
            })
        } else {
            var frm = $('#frm_iblock_edit');
            frm.find('[name=PROPERTY_START_1]').val("");
            frm.find('[name=PROPERTY_START_2]').val("");
            frm.find('[name=PROPERTY_START_3]').val("");
            frm.find('[name=PROPERTY_START_4]').val("");
            frm.find('[name=PROPERTY_START_5]').val("");
            frm.find('[name=PROPERTY_START_6]').val("");
            frm.find('[name=PROPERTY_START_7]').val("");

            frm.find('[name=PROPERTY_END_1]').val("");
            frm.find('[name=PROPERTY_END_2]').val("");
            frm.find('[name=PROPERTY_END_3]').val("");
            frm.find('[name=PROPERTY_END_4]').val("");
            frm.find('[name=PROPERTY_END_5]').val("");
            frm.find('[name=PROPERTY_END_6]').val("");
            frm.find('[name=PROPERTY_END_7]').val("");

            $('#inp_SaltoID').val("");

            $('#hdn_IBlockID').val("");
            $('#hdn_UserID').val($(this).parents('.user_data').attr('data_user_id'));

            $('#hdn_StdSchedule').val("1");
            $('#chb_StdSchedule').prop("checked", true);

            $('.table-shcedule').css('color', '#ccc');
            $('.table-shcedule input:not(#chb_StdSchedule, #inp_SaltoID, [type=hidden])').prop('disabled', true);
            $('.bx-clock-icon').css('display', 'none');

            $('#lbl_StdSchedule').css('color', '#000');

            $('.bottom-layer').fadeIn(200);
            $('#frm-edit').fadeIn(200);
        }
        return false;
    });

    $('.frm-edit-close, .frm-button-cancel').click(function(){
        $('.bottom-layer').fadeOut(200);
        $('#frm-edit').fadeOut(200);
        $('#salto-result-div').hide();
        closeSaltoSearch();
        return false;
    });

    $('#chb_StdSchedule').click(function(){
        //alert("asdgsdag");
        if ($(this).prop("checked")){
            $('.table-shcedule').css('color', '#ccc');
            $('.table-shcedule input:not(#chb_StdSchedule, #inp_SaltoID, [type=hidden], #chb_NotNotify)').prop('disabled', true);
            $('.bx-clock-icon').css('display', 'none');
            $('#hdn_StdSchedule').val("1");
        } else{
            $('.table-shcedule').css('color', '#000');
            $('.table-shcedule input').prop('disabled', false);
            $('.bx-clock-icon').css('display', 'inline-block');
            $('#hdn_StdSchedule').val("0");
        }
        $('#lbl_StdSchedule').css('color', '#000');
    });

    $('#chb_NotNotify').click(function(){
        if ($(this).prop("checked")){
            $('#hdn_NotNotify').val("1");
        } else {
            $('#hdn_NotNotify').val("0");
        }
    });

    $('.frm-button-submit').click(function(){
        $('.table-shcedule input').prop('disabled', false);
        var fields = $('#frm_iblock_edit').serialize();
        $('.table-shcedule input:not(#chb_StdSchedule, #inp_SaltoID, [type=hidden], #chb_NotNotify)').prop('disabled', true);
        fields = 'ACTION=setabsencedata&' + fields;
        $.ajax({
            type: "POST",
            url: "/local/components/synergy/synergy.absence.user.settings/ajax.php",
            data: fields,
            success: function (prmData) {
                var obj = $.parseJSON(prmData);
                if (obj.RESULT == 1){
                    var tblRow = $('[data_user_id=' + obj.USERID + ']');
                    tblRow.removeClass('empty_id');
                    var frm = $('#frm_iblock_edit');
                    tblRow.children('.weekday_day_1').text(frm.find('[name=PROPERTY_START_1]').val() + " - " + frm.find('[name=PROPERTY_END_1]').val());
                    tblRow.children('.weekday_day_2').text(frm.find('[name=PROPERTY_START_2]').val() + " - " + frm.find('[name=PROPERTY_END_2]').val());
                    tblRow.children('.weekday_day_3').text(frm.find('[name=PROPERTY_START_3]').val() + " - " + frm.find('[name=PROPERTY_END_3]').val());
                    tblRow.children('.weekday_day_4').text(frm.find('[name=PROPERTY_START_4]').val() + " - " + frm.find('[name=PROPERTY_END_4]').val());
                    tblRow.children('.weekday_day_5').text(frm.find('[name=PROPERTY_START_5]').val() + " - " + frm.find('[name=PROPERTY_END_5]').val());
                    tblRow.children('.weekday_day_6').text(frm.find('[name=PROPERTY_START_6]').val() + " - " + frm.find('[name=PROPERTY_END_6]').val());
                    tblRow.children('.weekday_day_7').text(frm.find('[name=PROPERTY_START_7]').val() + " - " + frm.find('[name=PROPERTY_END_7]').val());
                    tblRow.children('.salto-column').text(frm.find('[name=inp_SaltoID]').val());

                    tblRow.attr("rel", obj.IBID);
                    //alert(obj.IBID);
                } else {
                    alert('Ошибка при обновлении информации.');
                }
                $('.bottom-layer').fadeOut(200);
                BX.closeWait();
            }
        });
        $('#frm-edit').fadeOut(200);
        $('#salto-result-div').hide();
        closeSaltoSearch();
        BX.showWait();
        return false;
    });

    $('#ajax_container').on("click", ".navigation-page-numb, .navigation-button, .generate-href", ajaxUpdate);
    $('.generate-href').click(ajaxUpdate);
    $('#inp_FindEmploye').change(ajaxUpdate);

    $('#salto-search').click(SaltoSearch);
    $('#salto-result-div').on('click', '.saltoref', function(){
        $('#inp_SaltoID').val($(this).text());
        closeSaltoSearch();
    });
});

function GetSQLList(){
    var ajaxRes;
    var tmpRes;
    var obj;
    tmpRes = confirm("Загрузить данные из DMS?");
    if (tmpRes) {
        BX.showWait();
        ajaxRes = $.ajax({
            type: "POST",
            url: "/local/components/synergy/synergy.absence.user.settings/ajax.php?ACTION=getSQLlist",
            success: function (prmData) {
                BX.closeWait();
                obj = JSON.parse(ajaxRes.responseText);
                if (obj.RESULT == -1) {
                    alert(obj.ERROR);
                } else{
                    alert(obj.MESSAGE);
                }
                location.reload();
            }
        });
    }
    return false;
}

function ClearList(){
    var ajaxRes;
    var tmpRes;
    var obj;
    tmpRes = confirm("Очистить список?");
    if (tmpRes) {
        BX.showWait();
        ajaxRes = $.ajax({
            type: "POST",
            url: "/local/components/synergy/synergy.absence.user.settings/ajax.php?ACTION=clearlist",
            success: function (prmData) {
                BX.closeWait();
                obj = JSON.parse(ajaxRes.responseText);
                if (obj.RESULT == -1) {
                    alert(obj.ERROR);
                } else{
                    alert(obj.MESSAGE);
                }
                location.reload();
            }
        });
    }
    return false;
}

function GetSaltoId(){
    var ajaxRes;
    var tmpRes;
    var obj;
    tmpRes = confirm("Загрузить идентификаторы Salto из DMS?");
    if (tmpRes) {
        BX.showWait();
        ajaxRes = $.ajax({
            type: "POST",
            url: "/local/components/synergy/synergy.absence.user.settings/ajax.php?ACTION=getsaltoid",
            success: function (prmData) {
                BX.closeWait();
                obj = JSON.parse(ajaxRes.responseText);
                if (obj.RESULT == -1) {
                    alert(obj.ERROR);
                } else{
                    alert(obj.MESSAGE);
                }
                location.reload();
            }
        });
    }
    return false;
}

function ExportSaltoId(){
    var ajaxRes;
    var tmpRes;
    var obj;
    tmpRes = confirm("Экспорт SaltoID в AD. Начать?");
    if (tmpRes) {
        BX.showWait();
        ajaxRes = $.ajax({
            type: "POST",
            url: "/local/components/synergy/synergy.absence.user.settings/ajax.php?ACTION=exportsaltoid",
            success: function (prmData) {
                BX.closeWait();
                obj = JSON.parse(ajaxRes.responseText);
                if (obj.RESULT == -1) {
                    alert(obj.ERROR);
                } else{
                    alert(obj.MESSAGE);
                    window.open(obj.LOGURL, 'лог-файл', 'scrollbars=yes')
                }
            }
        });
    }
    return false;
}

function ajaxUpdate(){
    var datastr;
    BX.showWait();
    var tmpDep = $('#department-filter-div').find('select');
    var tmpEmp = $('#inp_FindEmploye');
    datastr = "isajax=1";
    if (tmpDep.val() != ""){
        datastr = datastr + "&dep="+tmpDep.val();
    }
    if (tmpEmp.val() != ""){
        datastr = datastr + "&emp="+tmpEmp.val();
    }
    $.ajax({
        type: "POST",
        url: $(this).attr('href'),
        data: datastr,
        success: function($prmData){
            $('#ajax_container').html($prmData);
            BX.closeWait();
        }
    });
    return false;
}

function SaltoSearch(){
    var frmEditLeft, frmEditTop, frmEditWidth, frmEditHeight, frmEditLeftMargin;
    var frm = $('#frm-edit');
    var saltores = $('#salto-result-div');
    var saltosearch = $('#salto-search');

    frmEditLeft = frm.offset().left;
    frmEditTop = frm.offset().top - $(window).scrollTop() + 40;
    frmEditWidth = frm.width();
    frmEditHeight = frm.height();

    if (saltores.css('display') == 'none') {
        BX.showWait();
        datastr = '&emp='+$('#User').text();
        saltosearch.text('<<< Закрыть подбор');
        saltosearch.css('color', '#d20000');
        saltosearch.css('color', '#666');
        saltosearch.css('margin-left', '38px');
        //alert (datastr);
        $.ajax({
            type: "POST",
            url: "/local/components/synergy/synergy.absence.user.settings/ajax.php?ACTION=getsaltoidlist",
            data: datastr,
            success: function ($prmData) {
                saltores.css({
                    'top': frmEditTop + 'px',
                    'left': (frmEditLeft + frmEditWidth - saltores.width()) + 'px',
                    'height': '511px'
                    //'height': $('.frm-edit-body').height() + 'px'
                    //'height': '420px'
                });
                //saltores.offset({top: frmEditTop, left: frmEditLeft + frmEditWidth});
                saltores.html($prmData);
                saltores.show();
                saltores.animate({left: (frmEditLeft + frmEditWidth) + 'px'}, 300);
                BX.closeWait();
            }
        });
    } else {
        closeSaltoSearch();
    }
}

function closeSaltoSearch(){
    var frmEditLeft, frmEditWidth;
    var frm = $('#frm-edit');
    var saltores = $('#salto-result-div');
    var saltosearch = $('#salto-search');

    frmEditLeft = frm.offset().left;
    frmEditWidth = frm.width();
    saltores.animate({left: (frmEditLeft + frmEditWidth - saltores.width()) + 'px'}, 300, function(){
        $(this).hide();
    });
    saltosearch.text('Поиск информации в Salto...');
    saltosearch.css('color', '#2067b0');
    saltosearch.css('margin-left', '4px');
}

function ClearFields(){
    $('.table-shcedule input:text:not(#inp_SaltoID)').each(function(){
        $(this).val('');
    });
}
