<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 01.12.2015
 * Time: 15:37
 */

$APPLICATION->AddHeadScript('/local/components/synergy/synergy.absence.report.violations/templates/.default/jquery.dataTables.js');
$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/clock.css");

// Заголовок выводим только для НЕ аяксовых вызовов
if (!$arResult['isajax']){?>
<div class="table-params clearfix">
    <div id="department-filter-div" style="float: left; margin-top: 4px; width: 320px;">
        <p style="display: block; float: left; margin: 3px 5px 0 0">Подразделение:</p>
        <?
        $arUserFields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', 0, LANGUAGE_ID);
        $arDeptField = $arUserFields['UF_DEPARTMENT'];
        $arDeptField['FIELD_NAME'] = 'department';
        $arDeptField['MULTIPLE'] = 'N';
        $arDeptField['SETTINGS']['LIST_HEIGHT'] = 1;
        $arDeptField["ENTITY_VALUE_ID"] = 20;
        $arDeptField["VALUE"][] = $arResult['DEPARTMENT'];
        CIntranetUtils::ShowDepartmentFilter($arDeptField, false);
        ?>
    </div>
    <div style="float: left;  margin-top: 4px;">
        <p style="display: block; float: left; margin: 3px 5px 0 0">Сотрудник:</p>
        <input type="text" name="inp_FindEmploye" id="inp_FindEmploye" style="width: 140px">
    </div>
    <div style="float: left;">
            <span>
                <a class="generate-href" href="/company/schedule/index.php">Фильтр</a>
            </span>
    </div>
    <?if ((strtolower($USER->GetLogin()) == "mbochkov") || ($USER->GetID() == 1)){?>
    <div style="float: right; padding-top: 5px;">
        <a style="display: inline; font-weight: normal; font-size: 12px;" href="javascript:void(0)" onclick="ExportSaltoId()">Экспорт SaltoID в AD</a>
        <a style="display: inline; font-weight: normal; font-size: 12px; margin-left: 10px;" href="javascript:void(0)" onclick="GetSaltoId()">Загрузить SaltoID</a>
        <a style="display: inline; font-weight: normal; font-size: 12px; margin-left: 10px;" href="javascript:void(0)" onclick="GetSQLList()">Загрузить данные из SQL</a>
        <a style="display: inline; font-weight: normal; font-size: 12px; margin-left: 10px;"href="javascript:void(0)" onclick="ClearList()">Удалить все</a>
    </div>
    <?}?>

</div>
<div id="ajax_container">
<?}?>

<?
if ($arResult['isajax']){
    $GLOBALS['APPLICATION']->RestartBuffer();

}
?>
<table class="schedule-data-table">
<?if(!$arResult["NO_USER"]){
?>
    <thead>
    <tr>
        <th>Сотрудник/подразделение</th>
        <th class="weekday-header">Понедельник</th>
        <th class="weekday-header">Вторник</th>
        <th class="weekday-header">Среда</th>
        <th class="weekday-header">Четверг</th>
        <th class="weekday-header">Пятница</th>
        <th class="weekday-header">Суббота</th>
        <th class="weekday-header">Воскресенье</th>
        <th class="salto-header">SALTO ID</th>
    </tr>
    </thead>
    <tbody>
    <? if (count($arResult["ELEMENTS"]) > 0) { ?>
        <?
        $i = 1;
        foreach ($arResult["ELEMENTS"] as $key => $arElement) {
            ?>
            <tr class="user_data <?
            echo (($i % 2) == 0) ? " even" : " odd";
            if (empty($arElement['ID'])) {
                echo " empty_id";
            }

            ?>" rel="<?= $arElement['ID'] ?>" data_user_id="<?= $arElement['PROPERTY_USER_VALUE'] ?>">
                <td class="weekday_user">
                    <?
                    $APPLICATION->IncludeComponent("bitrix:main.user.link",
                        '',
                        array(
                            "ID" => $arElement['PROPERTY_USER_VALUE'],
                            "USE_THUMBNAIL_LIST" => "N",
                            "NAME_TEMPLATE" => "#LAST_NAME# #NAME#",
                        ),
                        false,
                        array("HIDE_ICONS" => "Y")
                    );?>
                </td>
                <td class="weekday_time weekday_day_1"><?= $arElement['PROPERTY_START_1_VALUE'] . " - " . $arElement['PROPERTY_END_1_VALUE'] ?></td>
                <td class="weekday_time weekday_day_2"><?= $arElement['PROPERTY_START_2_VALUE'] . " - " . $arElement['PROPERTY_END_2_VALUE'] ?></td>
                <td class="weekday_time weekday_day_3"><?= $arElement['PROPERTY_START_3_VALUE'] . " - " . $arElement['PROPERTY_END_3_VALUE'] ?></td>
                <td class="weekday_time weekday_day_4"><?= $arElement['PROPERTY_START_4_VALUE'] . " - " . $arElement['PROPERTY_END_4_VALUE'] ?></td>
                <td class="weekday_time weekday_day_5"><?= $arElement['PROPERTY_START_5_VALUE'] . " - " . $arElement['PROPERTY_END_5_VALUE'] ?></td>
                <td class="weekday_time weekday_day_6"><?= $arElement['PROPERTY_START_6_VALUE'] . " - " . $arElement['PROPERTY_END_6_VALUE'] ?></td>
                <td class="weekday_time weekday_day_7"><?= $arElement['PROPERTY_START_7_VALUE'] . " - " . $arElement['PROPERTY_END_7_VALUE'] ?></td>
                <td class="salto-column"><?= $arElement['PROPERTY_SALTO_ID_VALUE'] ?></td>
            </tr>
            <?
            $i++;
        }
        ?>
    <? } else { ?>
        <tr>
            <td class="weekday_user" colspan="9">Отсутствуют элементы для
                отображения <span style="color: red"> (возможно у вас недостаточно прав)</span>
            </td>
        </tr>
    <? } ?>
    </tbody>
</table>

<?
$navResult = new CDBResult();
$navResult->NavPageCount = $arResult['NavPageCount'];
$navResult->NavPageNomer = $arResult['NavPageNomer'];
$navResult->NavNum = 1;
$navResult->NavPageSize = $arResult['NavPageSize'];
$navResult->NavRecordCount = $arResult['NavRecordCount'];
$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array(
    'NAV_RESULT' => $navResult,
));
}?>

<?
// Форму выводим только для НЕ аяксовых вызовов
if (!$arResult['isajax']){?>
    </div>
<div class="bottom-layer"></div>
<div id="frm-edit">
    <div class="frm-edit-header">
        <p class="frm-edit-header-caption">Настройка данных сотрудника</p>
        <a href="javascript:void(0)" class="frm-edit-close" title="Закрыть форму"></a>
    </div>
    <div class="frm-edit-body">
        <?CUtil::InitJSCore(array('popup'));?>
        <form name="iblock_edit" action="" method="post" id="frm_iblock_edit">

                <table style="width: 100%">
                    <tr>
                        <td class="bold-label"><p id="User"></p></td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0 7px; border-top: 1px solid #eee; text-decoration: underline">Уведомления:</td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <input type="checkbox" id="chb_NotNotify">
                                <span style="color: #000; font-size: 12px;"> Не отправлять уведомления</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 15px 0 7px; border-top: 1px solid #eee; text-decoration: underline">График работы:</td>
                    </tr>
                </table>
            <table class="table-shcedule">
                <tr>
<!--                    <td></td>-->
<!--                    <td></td>-->
                    <td colspan="5">
                        <label id="lbl_StdSchedule"><input type="checkbox" name="chb_StdSchedule" id="chb_StdSchedule" style="color: #000;">
                        Стандартный график</label>
                        <input type="hidden" name="hdn_StdSchedule" id="hdn_StdSchedule">
                        <input type="hidden" name="hdn_UserID" id="hdn_UserID">
                        <input type="hidden" name="hdn_IBlockID" id="hdn_IBlockID">
                        <input type="hidden" name="hdn_UserName" id="hdn_UserName">
                        <input type="hidden" name="hdn_NotNotify" id="hdn_NotNotify">
                        <span style="color: #2067b0; cursor: pointer; text-decoration: underline; margin-left: 75px; font-size: 11px" onclick="ClearFields();">Очистить график</span>
                    </td>
                </tr>
                <tr>
                    <th style="width: 80px;"></th>
                    <th style="width: 30px;"></th>
                    <th style="width: 90px; font-weight: normal">Начало:</th>
                    <th style="width: 30px;"></th>
                    <th style="width: 90px; font-weight: normal">Окончание:</th>
                </tr>
                <tr>
                    <td>Понедельник:</td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_START_1",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['START_1']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_END_1",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['END_1']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                </tr>

                <tr>
                    <td>Вторник:</td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_START_2",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['START_2']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_END_2",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['END_2']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                </tr>

                <tr>
                    <td>Среда:</td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_START_3",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['START_3']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_END_3",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['END_3']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                </tr>

                <tr>
                    <td>Четверг:</td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_START_4",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['START_4']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_END_4",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['END_4']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                </tr>

                <tr>
                    <td>Пятница:</td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_START_5",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['START_5']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_END_5",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['END_5']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                </tr>

                <tr>
                    <td>Суббота:</td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_START_6",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['START_6']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_END_6",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['END_6']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                </tr>

                <tr>
                    <td>Воскресенье:</td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_START_7",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['START_7']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                    <td></td>
                    <td>
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.clock',
                            '',
                            array(
                                'INPUT_NAME' => "PROPERTY_END_7",
                                'INIT_TIME' => $arResult["ELEMENT_PROPERTIES"][$AllProps['END_7']][0]["~VALUE"],
                                'VIEW' => "select",
                                'SHOW_ICON' => true,
                            )
                        );?>
                    </td>
                </tr>
                <tr>
                    <td style="color: #000;">
                        SALTO ID:
                    </td>
                    <td></td>
                    <td colspan="3">
                        <input id="inp_SaltoID" name="inp_SaltoID" type="text" style="width: 41px" readonly>
                        <span id="salto-search" style="color: #2067b0; cursor: pointer; text-decoration: underline; margin-left: 4px; font-size: 11px">Поиск информации в Salto...</span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </form>
        <div class="frm-buttons">
            <a href="/" class="frm-button-submit">Сохранить</a>
            <a href="/" class="frm-button-cancel">Отмена</a>

        </div>
    </div>

</div>
<div id="salto-result-div">

</div>

<?} else {
    die();
}?>