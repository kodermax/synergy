<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 11.11.2015
 * Time: 17:09
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/skdimport.php");
CModule::IncludeModule("iblock");

// Выполняемое действие
$action = $_REQUEST['ACTION'];

// Ищем ID инфоблока 'schedule'
$ibDB = CIBLock::GetList(array(), array('CODE' => 'schedule'));
$tmpIB = $ibDB->fetch();
if (!empty($tmpIB)) $IB_ID = $tmpIB['ID'];


if ($action == "getSQLlist"){
// Получаем настройки подключения к базе
    $ConnSettings = GetDMSConnectionParams();
// Если настройоки получены, то пробуем подключиться
    if ($ConnSettings !== false){
        ini_set('mssql.charset', 'UTF-8');
        $dbh = mssql_connect($ConnSettings['HOST'], $ConnSettings['USER'], $ConnSettings['PASS']);
        mssql_select_db($ConnSettings['DBNAME'], $dbh);
        if ($dbh){
// Если подключение прошло успешно, то переходим к загрузке
            $sql = "SELECT USER_INFO, CONVERT(VARCHAR, START_TIME, 108), CONVERT(VARCHAR, END_TIME, 108), WEEKDAY_NUM FROM PERSON_TABEL WHERE USER_INFO LIKE '%synergy%'";
// 0 - Пользователь
// 1 - Начало рабочего дня
// 2 - Окончание рабочего дня
// 3 - Номер рабочего дня (1 - понедельник,... 7 - воскресенье)
//
            $rs = mssql_query($sql, $dbh);
// Проверим, что запрос не вернул ошибку
            if ($rs){
                $AddedQ = 0; // Количество добавленных записей
                while ($row = mssql_fetch_row($rs)) {
                    $arResult['RESULT'] = 1;
// Извлекаем логин пользователя из поля
                    if (preg_match("#synergy\\\(.*)\s*#i", $row[0], $matches)){
                        $login = $matches[1];
                        $dbUser = CUser::GetByLogin($matches[1]);
                        $arUser = $dbUser->fetch();
// Если нашли пользователя в битриксе, то продолжаем обработку, если не нашли, то пропускаем запись (м.б. он уволен уже)
                        if (!empty($arUser)){
// Ищем элемент инфоблока, соответствующую этому пользователю
                            $db_item = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IB_ID, 'PROPERTY_USER' => $arUser['ID']), false, false,
                                array('ID', 'IBLOCK_ID', 'PROPERTY_USER', 'PROPERTY_START_1', 'PROPERTY_START_2', 'PROPERTY_START_3', 'PROPERTY_START_4',
                                    'PROPERTY_START_5', 'PROPERTY_START_6', 'PROPERTY_START_7', 'PROPERTY_END_1', 'PROPERTY_END_2', 'PROPERTY_END_3',
                                    'PROPERTY_END_4', 'PROPERTY_END_5', 'PROPERTY_END_6', 'PROPERTY_END_7',));
                            $ar_item = $db_item->fetch();
// Если нашли запись, то надо обновить поля за соответствующий день
                            if (!empty($ar_item)){
                                unset($arProps);
// Восстанавливаем все сохраненные свойства
                                $arProps['USER'] = $ar_item['PROPERTY_USER_VALUE'];
                                $arProps['START_1'] = $ar_item['PROPERTY_START_1_VALUE'];
                                $arProps['END_1'] = $ar_item['PROPERTY_END_1_VALUE'];
                                $arProps['START_2'] = $ar_item['PROPERTY_START_2_VALUE'];
                                $arProps['END_2'] = $ar_item['PROPERTY_END_2_VALUE'];
                                $arProps['START_3'] = $ar_item['PROPERTY_START_3_VALUE'];
                                $arProps['END_3'] = $ar_item['PROPERTY_END_3_VALUE'];
                                $arProps['START_4'] = $ar_item['PROPERTY_START_4_VALUE'];
                                $arProps['END_4'] = $ar_item['PROPERTY_END_4_VALUE'];
                                $arProps['START_5'] = $ar_item['PROPERTY_START_5_VALUE'];
                                $arProps['END_5'] = $ar_item['PROPERTY_END_5_VALUE'];
                                $arProps['START_6'] = $ar_item['PROPERTY_START_6_VALUE'];
                                $arProps['END_6'] = $ar_item['PROPERTY_END_6_VALUE'];
                                $arProps['START_7'] = $ar_item['PROPERTY_START_7_VALUE'];
                                $arProps['END_7'] = $ar_item['PROPERTY_END_7_VALUE'];
                                $arProps['START_'.$row[3]] = $DB->FormatDate($row[1], "HH:MI:SS", "HH:MI");
                                $arProps['END_'.$row[3]] = $DB->FormatDate($row[2], "HH:MI:SS", "HH:MI");
                                $el = new CIBlockElement;
                                $res = $el->Update($ar_item['ID'], array('PROPERTY_VALUES' => $arProps));
                                if (!$res){
                                    $arResult['RESULT'] = -1;
                                    $arResult['ERROR'] = 'Ошибка Update: '.$el->LAST_ERROR;
                                    break;
                                }
                            } else{
// Если не нашли, то создаем новый элемент инфоблока
                                unset($arProps);
                                $arProps['USER'] = $arUser['ID'];
                                $arProps['START_'.$row[3]] = $DB->FormatDate($row[1], "HH:MI:SS", "HH:MI");
                                $arProps['END_'.$row[3]] = $DB->FormatDate($row[2], "HH:MI:SS", "HH:MI");
                                $FIO = $arUser['LAST_NAME']." ".$arUser['NAME'];
                                $el = new CIBlockElement;
                                $res = $el->Add(array('IBLOCK_ID' => $IB_ID, 'NAME' => $FIO, 'PROPERTY_VALUES' => $arProps));
                                if (!$res){
                                    $arResult['RESULT'] = -1;
                                    $arResult['ERROR'] = 'Ошибка при добавлении: '.$el->LAST_ERROR;
                                    break;
                                }
                                $AddedQ++;
                            }
                        }
                    }
                }
                if ($arResult['RESULT'] == -1){
                    echo json_encode($arResult);
                } else {
                    echo json_encode(array("RESULT" => "1", "MESSAGE" => "Добавлено " . $AddedQ . " записей."));
                }
            } else{
// Если запрос вернул ошибку, то возвращаем ошибку
                echo json_encode(array("RESULT" => "-1", "ERROR" => 'Ошибка при выполнении запроса к БД: '.mssql_get_last_message()));
            }
        } else{
// Если подключиться к БД не удалось, то возвращаем ошибку
            echo json_encode(array("RESULT" => "-1", "ERROR" => 'Не удалось подключиться к БД.'));
        }
    } else{
// Если настройки подключения к базе не получены, то взвращаем ошибку
        echo json_encode(array("RESULT" => "-1", "ERROR" => 'Не удалось получить настройки подключения к БД.'));
    }
} elseif ($action == "clearlist"){
    $db_item = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IB_ID), false, false, array('ID'));
    $DelQ = 0; // Количество удаленных записей
    $res = 1;
    while($ar_item = $db_item->fetch()){
        if (!CIBlockElement::Delete($ar_item['ID'])){
            $res = -1;
            break;
        }
        $DelQ++;
    }
    $arResult['RESULT'] = $res;
    if ($res == -1){
        $arResult['ERROR'] = 'Ошибка при удалении элементов.';
    } else{
        $arResult['MESSAGE'] = 'Удаление завершено. Удалено '.$DelQ.' элементов';
    }
    echo json_encode($arResult);
} elseif ($action == "getsaltoid"){
    $arResult['RESULT'] = 1;
    $dbh = GetDMSConnection();
    if ($dbh !== false) {
// Выбираем все данные профилей из последней записи USERPROFILE_DATA
        $sql = "
                DECLARE @idoc int;
                DECLARE @xmlProfiles xml;
                select top 1 @xmlProfiles = data from [USERPROFILE_DATA] order by [USERPROFILE_DATA_ID] desc;
                EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlProfiles;
                SELECT    *
                FROM openxml (@idoc, '/Data/User', 1) with(SaltoID nvarchar(63), AccountName nvarchar(127))
        ";
        $rs = mssql_query($sql, $dbh);
        $counter = 1;
        while ($row = mssql_fetch_row($rs)) {
            preg_match("#synergy\\\(.*)\s*$#i", $row[1], $matches);
            if (!empty($matches[1])){
                $dbUser = CUser::GetByLogin($matches[1]);
                $arUser = $dbUser->fetch();
                if (!empty($arUser)){
// Ищем элемент инфоблока, соответствующую этому пользователю
                    $db_item = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IB_ID, 'PROPERTY_USER' => $arUser['ID']), false, false,
                        array('ID', 'IBLOCK_ID'));
                    $ar_item = $db_item->fetch();
                    if (!empty($ar_item)){
// Если нашли элемент инфоблока, то обновляем поле SaltoID
                        CIBlockElement::SetPropertyValuesEx($ar_item['ID'], $IB_ID, array('SALTO_ID'=>$row[0]));
                        $counter++;
                    } else{
// Если не нашли элемент, то надо создать
                        $FIO = $arUser['LAST_NAME']." ".$arUser['NAME'];
                        $el = new CIBlockElement;
                        $res = $el->Add(array('IBLOCK_ID' => $IB_ID, 'NAME' => $FIO));
                        if (!$res){
                            $arResult['RESULT'] = -1;
                            $arResult['ERROR'] = 'Ошибка при добавлении: '.$el->LAST_ERROR;
                            break;
                        } else {
                            CIBlockElement::SetPropertyValuesEx($res, $IB_ID, array('SALTO_ID'=>$row[0], 'USER'=>$arUser['ID'],
                                'STD_SCHEDULE'=>1));
                            $counter++;
                        }
                    }
                }
            }
        }
        if ($arResult['RESULT'] == -1){
            echo json_encode($arResult);
        } else {
            echo json_encode(array("RESULT" => "1", "MESSAGE" => "Добавлено " . $counter . " записей."));
        }
    } else {
        echo json_encode(array("RESULT" => "-1", "ERROR" => 'Не удалось подключиться к БД.'));
    }
} elseif ($action == "getabsencedata"){
    $tmpID = $_REQUEST['ID'];
    if (!empty($tmpID)) {
// Ищем элемент инфоблока
        $db_item = CIBlockElement::GetList(array(), array('ID' => $tmpID), false, false,
            array('ID', 'IBLOCK_ID', 'PROPERTY_USER', 'PROPERTY_START_1', 'PROPERTY_START_2', 'PROPERTY_START_3', 'PROPERTY_START_4',
                'PROPERTY_START_5', 'PROPERTY_START_6', 'PROPERTY_START_7', 'PROPERTY_END_1', 'PROPERTY_END_2', 'PROPERTY_END_3',
                'PROPERTY_END_4', 'PROPERTY_END_5', 'PROPERTY_END_6', 'PROPERTY_END_7', 'PROPERTY_SALTO_ID', 'PROPERTY_STD_SCHEDULE', 'PROPERTY_DIS_ALL_NOTIFY'));
        $ar_item = $db_item->fetch();
        if (!empty($ar_item)){
            $ar_item['RESULT'] = 1;
            echo json_encode($ar_item);
        } else {
            $ar_item['RESULT'] = -1;
        }
    }

} elseif ($action == "setabsencedata"){
//    print_r($_REQUEST);
// Записываем параметры в массив
    $arFields = array();
    $arFields['USER'] = $_REQUEST['hdn_UserID'];
    $arFields['STD_SCHEDULE'] = array("VALUE"=>$_REQUEST['hdn_StdSchedule']);
    $arFields['DIS_ALL_NOTIFY'] = array("VALUE"=>$_REQUEST['hdn_NotNotify']);
    $arFields['SALTO_ID'] = array("VALUE"=>$_REQUEST['inp_SaltoID']);
    $arFields['START_1'] = $_REQUEST['PROPERTY_START_1'];
    $arFields['START_2'] = $_REQUEST['PROPERTY_START_2'];
    $arFields['START_3'] = $_REQUEST['PROPERTY_START_3'];
    $arFields['START_4'] = $_REQUEST['PROPERTY_START_4'];
    $arFields['START_5'] = $_REQUEST['PROPERTY_START_5'];
    $arFields['START_6'] = $_REQUEST['PROPERTY_START_6'];
    $arFields['START_7'] = $_REQUEST['PROPERTY_START_7'];

    $arFields['END_1'] = $_REQUEST['PROPERTY_END_1'];
    $arFields['END_2'] = $_REQUEST['PROPERTY_END_2'];
    $arFields['END_3'] = $_REQUEST['PROPERTY_END_3'];
    $arFields['END_4'] = $_REQUEST['PROPERTY_END_4'];
    $arFields['END_5'] = $_REQUEST['PROPERTY_END_5'];
    $arFields['END_6'] = $_REQUEST['PROPERTY_END_6'];
    $arFields['END_7'] = $_REQUEST['PROPERTY_END_7'];

// Проверим надо ли создать запись или отредактировать существующую
    if (empty($_REQUEST['hdn_IBlockID'])){
        $el = new CIBlockElement;
        $res = $el->Add(array("IBLOCK_ID"=>$IB_ID, "NAME"=>$_REQUEST['hdn_UserName']));
        if ($res){
            CIBlockElement::SetPropertyValuesEx($res, $IB_ID, $arFields);
            $arResult = array('RESULT'=>1, 'IBID'=>$res, 'USERID' => $_REQUEST['hdn_UserID']);
        } else {
            $arResult = array('RESULT'=>-1);
        }
    } else {
        CIBlockElement::SetPropertyValuesEx($_REQUEST['hdn_IBlockID'], $IB_ID, $arFields);
        $arResult = array('RESULT'=>1, 'IBID'=>$_REQUEST['hdn_IBlockID'], 'USERID' => $_REQUEST['hdn_UserID']);
    }
// Записываем значение SaltoID в AD
    SaveSaltoIDtoAD($arFields['USER'], $arFields['SALTO_ID']['VALUE']);

// Записываем расписание сотрудника в DMS
    ExportPersonSchedule($arFields['USER'], $arFields);
    echo json_encode($arResult);
} elseif ($action == "exportsaltoid"){
    $arResult = ExportSaltoIDToAD(false);
    echo json_encode($arResult);
} elseif ($action == "getsaltoidlist"){

    $employe = explode(' ', $_REQUEST['emp']);
    if (!empty($employe)) {
        $dbh = GetSaltoConnection();
        if ($dbh !== false) {
            $sql = "SELECT name, id_user from tb_Users where (firstname like'%".$employe[0]."%') and (name not like'%#%') and (status <> 3)";
            $rs = mssql_query($sql, $dbh);
            if ($rs !== false) {
                echo "<table id='saltoid_table'>";
                while ($row = mssql_fetch_row($rs)) {
                    $tmpStr = "<tr><td>".$row[0]."</td><td><span class='saltoref'>".$row[1]."</span></td>";
                    echo $tmpStr;
                }
                echo "</table>";
            } else {
                echo 'Ошибка выполнения запроса: ' . mssql_get_last_message();
            }
        } else {
            echo 'Ошибка подключения к SQL: ' . mssql_get_last_message();
        }
        $dbh = null;
    }
}
