<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 01.12.2015
 * Time: 15:37
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();


if (isset($_REQUEST['dep'])) {
    $department = $_REQUEST['dep']; // Подразделение для фильтрации
} else {
    $department = "";
}
if (isset($_REQUEST['emp'])) {
    $Employe = $_REQUEST['emp']; // Сотрудник (или часть) для фильтрации
} else {
    $Employe = "";
}
if (isset($_REQUEST['isajax'])) {
    $arResult['isajax'] = true;
} else {
    $arResult['isajax'] = false;
}
// Ищем ID инфоблока 'schedule'
$ibDB = CIBLock::GetList(array(), array('CODE' => 'schedule'));
$tmpIB = $ibDB->fetch();
if (!empty($tmpIB)) $IB_ID = $tmpIB['ID'];

if ((isAbsenceAdmin($USER->GetId()))) {
// Сформируем фильтр по подразделению (поместим в массив всех сотрудников, котоыре относятся к этому подразделению)
    $arUserFilter = array();
    if (!empty($department)) {
        $dbUserFilter = CIntranetUtils::GetDepartmentEmployees(array($department), true);
        while ($arUser = $dbUserFilter->fetch()) {
            $arUserFilter[] = $arUser['ID'];
        }
    }

// Выбираем все элементы инфоблока
    $db_item = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IB_ID), false, false,
        array('ID', 'IBLOCK_ID', 'PROPERTY_USER', 'PROPERTY_START_1', 'PROPERTY_START_2', 'PROPERTY_START_3', 'PROPERTY_START_4',
            'PROPERTY_START_5', 'PROPERTY_START_6', 'PROPERTY_START_7', 'PROPERTY_END_1', 'PROPERTY_END_2', 'PROPERTY_END_3',
            'PROPERTY_END_4', 'PROPERTY_END_5', 'PROPERTY_END_6', 'PROPERTY_END_7', 'PROPERTY_SALTO_ID', 'PROPERTY_STD_SCHEDULE'));

// Формируем массив, у которого ключами является ID пользователя, параллельно фильтруя по подразделению (если задано)
    $arUserList = array();
    $i = 0;
    while ($ar_item = $db_item->fetch()) {
        if ((in_array($ar_item['PROPERTY_USER_VALUE'], $arUserFilter)) || empty($arUserFilter)) {
            $db_tmp_User = CUser::GetByID($ar_item['PROPERTY_USER_VALUE']);
            $ar_tmp_User = $db_tmp_User->Fetch();
// Если установлен фильтр по сотруднику проверим соответствие фильтру
            if ((empty($Employe)) || (!empty($Employe) && (stripos($ar_tmp_User['LAST_NAME'] . " " . $ar_tmp_User['NAME'], $Employe) !== false))) {
                $arUserList['ID_' . $ar_item['PROPERTY_USER_VALUE']] = $ar_item;
                $arUserList['ID_' . $ar_item['PROPERTY_USER_VALUE']]['UNAME'] = $ar_tmp_User['LAST_NAME'] . " " . $ar_tmp_User['NAME'];
            }
        }
        $i++;
        if ($i > 5000) {
            echo ("Бесконечный цикл по инфоблоку.");
            break;
        }
    }
//print_r($arUserList);
// Теперь выберем всех пользователей из системы (также фильруем их по подразделению)
    $ar_sysUsers = array();
    $db_Users = CUser::GetList(($by = "ID"), ($order = "asc"), array(), array('SELECT' => array('UF_DEPARTMENT')));
    $i = 0;
    while ($ar_Users = $db_Users->fetch()) {
// Выбираем только тех пользователей, которые попадают под фильтр (или фильтр пустой) и у которых указано подразделение и указана Фамилия
        if (((in_array($ar_Users['ID'], $arUserFilter)) || empty($arUserFilter)) && (!empty($ar_Users['UF_DEPARTMENT'])) &&
            (!empty($ar_Users['LAST_NAME']))
        ) {
// Если установлен фильтр по сотруднику проверим соответствие фильтру
            if ((empty($Employe)) || (!empty($Employe) && (stripos($ar_Users['LAST_NAME'] . " " . $ar_Users['NAME'], $Employe) !== false))) {
                $ar_sysUsers['ID_' . $ar_Users['ID']] = array('PROPERTY_USER_VALUE' => $ar_Users['ID']);
                $ar_sysUsers['ID_' . $ar_Users['ID']]['UNAME'] = $ar_Users['LAST_NAME'] . " " . $ar_Users['NAME'];
            }
        }
        $i++;
        if ($i > 5000) {
            echo ("Бесконечный цикл по пользователям.");
            break;
        }
    }
// Получаем список пользователей без повторений (возможно сработает сразу правильно array_merge, но ХЗ
    $tmpArray = array_diff_key($ar_sysUsers, $arUserList);
    $arUserList = array_merge($arUserList, $tmpArray);
//print_r($ar_sysUsers);
// Сортируем пользователей по имени
    usort($arUserList, "uasort_cmp");
// Теперь просто формируем массив arRusult для передачи в шаблон

// Определяем параметры пагинатора
    $RecCount = count($arUserList);
    $PageSize = 30;
    $arResult['NavPageCount'] = ceil($RecCount / $PageSize);
    $arResult['NavPageNomer'] = (empty($_REQUEST['PAGEN_1']) ? 1 : $_REQUEST['PAGEN_1']);
    $arResult['NavNum'] = 1;
    $arResult['NavPageSize'] = $PageSize;
    $arResult['NavRecordCount'] = $RecCount;

    $counter = 1;
    $StartRec = 1 + ($arResult['NavPageNomer'] - 1) * $PageSize;
    $EndRec = $StartRec + $PageSize;
    foreach ($arUserList as $key => $arUser) {
        if (($counter >= $StartRec) && ($counter <= $EndRec)) {
            $arResult['ELEMENTS'][$key] = $arUser;
        }
        $counter++;
    }
    if (empty($arResult)) {
        $arResult['NO_USER'] = true;
    } else {
        $arResult['NO_USER'] = false;
    }

    if (isAbsenceAdmin($USER->GetId())) {
        $arResult['CAN_EDIT'] = true;
    } else {
        $arResult['CAN_EDIT'] = false;
    }
}
$this->IncludeComponentTemplate();

function uasort_cmp($a, $b){
    if ($a['UNAME'] < $b['UNAME']) return -1;
    if ($a['UNAME'] > $b['UNAME']) return 1;
    if ($a['UNAME'] == $b['UNAME']) return 0;
}