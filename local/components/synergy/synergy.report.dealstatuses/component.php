<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 30.09.15
 * Time: 12:14
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

global $APPLICATION, $USER;

CJSCore::Init('jquery');
$APPLICATION->AddHeadScript("/local/assets/chosen/chosen.jquery.min.js", true);
$APPLICATION->SetAdditionalCSS("/local/assets/chosen/chosen.min.css", true);

CModule::IncludeModule("crm");
CModule::IncludeModule("intranet");
CModule::IncludeModule("iblock");

$arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
$arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");

$arResult["MODE"]=($_REQUEST["mode"]=="tbl" || $_REQUEST["mode"]=="exl")?$_REQUEST["mode"]:null;
$arResult["PERIOD"]=$_REQUEST["period"];
$arResult["DEVIDE"]=$_REQUEST["devide"];

$arResult["PERIODS"] = Array(
    "cday"=>"Текущий день",
    "pday"=>"Предыдущий день",
    "cweek"=>"Текущая неделя",
    "pweek"=>"Предыдущая неделя",
    "cmonth"=>"Текущий месяц",
    "pmonth"=>"Предыдущий месяц",
    "cyear"=>"Текущий год",
    "pyear"=>"Предыдущий год",
    "interval"=>"Интервал"
);

$arResult["DEVIDES"]=array(
    "0"=>"За весь период",
    "1"=>"По дням",
    "2"=>"По неделям",
    "3"=>"По месяцам"
);

$arResult["TABLE_COLS"]=array(
    "NEW"   =>  "В обработке",
    "DETAILS"=>	"Уточнение информации",
    "PROPOSAL"=> "Предложение",
    "NEGOTIATION" => "Переговоры в процессе",
    "1" => "Требуется дополнительный звонок",
    "3" => "Назначена личная встреча",
    "4" => "Отправлены документы на ДО",
    "8" => "Договор",
    "WON"=> "Успешная сделка",
    "LOSE" => "Сделка проиграна"
);

if(intval($_REQUEST["dept"])==0)
{
    $rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_DEPARTMENT")));
    if($arUser=$rsUser->Fetch())
    {
        $arResult["DEPT"]=$arUser["UF_DEPARTMENT"][0];
    }
    else
        $arResult["DEPT"]=1;
}
else
    $arResult["DEPT"]=$_REQUEST["dept"];

$arResult["REC"]=$_REQUEST["rec"];

$rsDept=CIBlockSection::GetTreeList(array("IBLOCK_ID"=>1,"ACTIVE"=>"Y"));
while($arDept=$rsDept->GetNext())
{
    $arResult["DEPT_LIST"][]=$arDept;
}

switch($arResult["PERIOD"])
{
    case "cday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y");
        break;
    case "pday":
        $arResult["FROM"]=$arResult["TO"]=date("d.m.Y",strtotime("yesterday"));
        break;
    case "cweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday"));
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pweek":
        $arResult["FROM"]=date("d.m.Y",strtotime("last Monday -1 week"));
        $arResult["TO"]=date("d.m.Y",strtotime("last Sunday"));
        break;
    case "cmonth":
        $arResult["FROM"]="01.".date("m.Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pmonth":
        $arResult["FROM"]="01.".date("m.Y",strtotime("previous month"));
        $arResult["TO"]=date("d.m.Y",strtotime("01.".date("m.Y")." -1 day"));
        break;
    case "cyear":
        $arResult["FROM"]="01.01.".date("Y");
        $arResult["TO"]=date("d.m.Y");
        break;
    case "pyear":
        $arResult["FROM"]="01.01.".date("Y",strtotime("previous year"));
        $arResult["TO"]="31.12.".date("Y",strtotime("previous year"));
        break;
    case "interval":
        $arResult["FROM"]=((bool)strtotime($_REQUEST["from"]))?$_REQUEST["from"]:date("d.m.Y", strtotime("yesterday"));
        $arResult["TO"]=((bool)strtotime($_REQUEST["to"]))?$_REQUEST["to"]:date("d.m.Y");
        break;



}


if($arResult["MODE"])
{

    $rsDept=CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1,"ACTIVE"=>"Y","ID"=>$arResult["DEPT"]),false,array("ID","NAME"));
    $arResult["DEPT_DATA"][$arResult["DEPT"]]=$rsDept->Fetch();

    if($arResult["REC"]==1)
    {
        $rsParentSection = CIBlockSection::GetByID($arResult["DEPT"]);
        if ($arParentSection = $rsParentSection->GetNext())
        {
            $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
            $rsDept = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
            while ($arDept = $rsDept->GetNext())
            {
                $arResult["DEPT_DATA"][$arDept["ID"]]=$arDept;
            }
        }
    }

    $rsUser=CIntranetUtils::GetDepartmentEmployees(array_keys($arResult["DEPT_DATA"]));

    while($arUser=$rsUser->GetNext())
    {
        $arResult["USERS"][$arUser["ID"]]=$arUser;
    }

    $list_user=join(",",array_keys($arResult["USERS"]));
    if($list_user=="")
        $list_user="-1";

    $from=$DB->ForSql(ConvertDateTime($arResult["FROM"], "YYYY-MM-DD"));
    $to=$DB->ForSql(ConvertDateTime($arResult["TO"], "YYYY-MM-DD"));
    $groupby="";
    $select="";

    switch($arResult["DEVIDE"])
    {
        case 1:
            $groupby='`YEAR`,`MONTH`,`DAY`,';
            $select="DAYOFMONTH(`crm_deal`.DATE_CREATE) as `DAY`,
        MONTH(`crm_deal`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_deal`.DATE_CREATE) as `YEAR`,";
            break;
        case 2:
            $groupby='`YEAR`,`WEEK`,';
            $select="YEAR(`crm_deal`.DATE_CREATE) as `YEAR`,
        WEEK(`crm_deal`.DATE_CREATE) as `WEEK`,";
            break;
        case 3:
            $groupby='`YEAR`,`MONTH`,';
            $select="MONTH(`crm_deal`.DATE_CREATE) as `MONTH`,
        YEAR(`crm_deal`.DATE_CREATE) as `YEAR`,";
            break;
        case 4:
            $groupby='`YEAR`,';
            $select="YEAR(`crm_deal`.DATE_CREATE) as `YEAR`,";
            break;
    }

    $sql="
        SELECT
                        $select
                        `crm_deal`.`ASSIGNED_BY_ID` AS `ASSIGNED_BY_ID`,
                        `crm_deal`.`STAGE_ID` as `STAGE_ID`,
                        `status`.`NAME` as `STAGE_NAME`,
                        COUNT(`crm_deal`.`ASSIGNED_BY_ID`) AS `CNT`
                FROM
                        `b_crm_deal` `crm_deal`
                        LEFT JOIN `b_crm_status` `status` ON (`crm_deal`.STAGE_ID=`status`.STATUS_ID AND `status`.ENTITY_ID='DEAL_STAGE')
                WHERE
                        DATE(`crm_deal`.DATE_CREATE) BETWEEN '$from' AND '$to'  AND `crm_deal`.`ASSIGNED_BY_ID` IN ($list_user)
                GROUP BY
                        $groupby
                        `ASSIGNED_BY_ID`,
                        `STAGE_ID`
                ORDER BY `crm_deal`.DATE_CREATE, `crm_deal`.`ASSIGNED_BY_ID`       

        ";

    //echo $sql;

    $res=$DB->Query($sql);
    while($row=$res->Fetch())
        switch($arResult["DEVIDE"])
        {
            case 1:
                $date=sprintf("%02d.%02d.%4d",$row["DAY"],$row["MONTH"],$row["YEAR"]);
                $arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]][$row["STAGE_ID"]]=array("CNT"=>$row["CNT"],"NAME"=>$row["STAGE_NAME"]);
                break;
            case 2:
                $dates = getDatesByWeek($row["WEEK"]+1,$row["YEAR"]);
                $dates[0]=(strtotime($arResult["FROM"])>$dates[0])?strtotime($arResult["FROM"]):$dates[0];
                $dates[1]=(strtotime($arResult["TO"])<$dates[1])?strtotime($arResult["TO"]):$dates[1];
                $arResult["RESULT"][date("d.m.Y",$dates[0])." - ".date("d.m.Y",$dates[1])][$row["ASSIGNED_BY_ID"]][$row["STAGE_ID"]]=array("CNT"=>$row["CNT"],"NAME"=>$row["STAGE_NAME"]);
                break;
            case 3:
                $date=sprintf("%02d %4d",$row["MONTH"],$row["YEAR"]);
                $arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]][$row["STAGE_ID"]]=array("CNT"=>$row["CNT"],"NAME"=>$row["STAGE_NAME"]);
                break;
            case 4:
                $arResult["RESULT"][$row["YEAR"]][$row["ASSIGNED_BY_ID"]][$row["STAGE_ID"]]=array("CNT"=>$row["CNT"],"NAME"=>$row["STAGE_NAME"]);
                break;
            case 0:
            default:
                if($arResult["FROM"]==$arResult["TO"])
                    $date=$arResult["FROM"];
                else
                    $date=$arResult["FROM"]." - ".$arResult["TO"];
                $arResult["RESULT"][$date][$row["ASSIGNED_BY_ID"]][$row["STAGE_ID"]]=array("CNT"=>$row["CNT"],"NAME"=>$row["STAGE_NAME"]);
                break;
        }
}
$this->IncludeComponentTemplate();
?>