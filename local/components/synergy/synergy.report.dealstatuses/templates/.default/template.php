<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if($arResult["MODE"]!="exl"):?>
<script>
var devides=<? echo json_encode($arResult["DEVIDES"]);?>;
$(document).ready(
    function()
    {
        $("#period").change(
            function()
            {
                if($("#period").val()=="interval")
                    $("#interval").css('display', 'inline-block');
                else
                    $("#interval").hide();

                var max_devide=1;
                 switch($("#period").val())
                {
                    case "cmonth":
                    case "pmonth":
                        max_devide=2;
                        break;
                    case "cyear":
                    case "pyear":
                    case "interval":
                        max_devide=3;
                        break;
                }

                $("#devide").empty();
                for(var i=0; i<=max_devide; i++)
                {
                    $("#devide").append("<option value=\""+i+"\">"+devides[i]+"</option>");
                }
            }
        );
        $("#dept").chosen({no_results_text: "Ничего не найдено!"});
    }

);
</script>
<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
    <form method="POST" name="form1">
        <input type="hidden" name="mode" value="tbl">
        <div class="form-element">
            <label for="period">Период:</label>
            <select id="period" name="period">
                <? foreach($arResult["PERIODS"] as $k=>$v):?>
                    <option value="<?=$k?>"<?=($arResult["PERIOD"]==$k)?" selected":""?>><?=$v?></option>
                <? endforeach; ?>
            </select>
        <div id="interval" style="display: <?=($arResult["PERIOD"]=="interval")?"inline-block":"none"?>;">
            <?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
     "SHOW_INPUT" => "Y",
     "FORM_NAME" => "form1",
     "INPUT_NAME" => "from",
     "INPUT_NAME_FINISH" => "to",
     "INPUT_VALUE" => $arResult["FROM"],
     "INPUT_VALUE_FINISH" => $arResult["TO"],
     "SHOW_TIME" => "N",
     "HIDE_TIMEBAR" => "Y"
    )
);?>
        </div>
        </div>
         <div class="form-element">
            <label for="devide">Разбивка:</label>
            <select id="devide" name="devide">

            <?
             $max_devide=1;
            switch($arResult["PERIOD"])
            {
                case "cmonth":
                case "pmonth":
                    $max_devide=2;
                    break;
                case "cyear":
                case "pyear":
                case "interval":
                    $max_devide=3;
                    break;
            }
            ?>
                <? for($arDiv=0;$arDiv<=$max_devide; $arDiv++):?>
                    <option value="<?=$arDiv?>"<?=($arResult["DEVIDE"]==$arDiv)?" selected":""?>><?=$arResult["DEVIDES"][$arDiv]?></option>
                <? endfor; ?>
            </select>
        </div>
        <div class="form-element">
            <label for='dept'>Отдел:</label>
            <select id='dept' name='dept'>
              <? foreach ($arResult["DEPT_LIST"] as $arItem):?>
                  <? $sel=($arItem["ID"]==$arResult["DEPT"])?" selected":""; ?>
                  <option value="<?=$arItem["ID"]?>"<?=$sel?>><?=(str_repeat("&nbsp;",($arItem["DEPTH_LEVEL"]-1)*4)." ".$arItem["NAME"])?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-element">
            <input id="rec" name="rec" type="checkbox" value="1" <?=($arResult["REC"]==1)?" checked":""?>>
            <label for="rec"> Учитывать подотделы</label>
        </div>
        <div class="form-element">
            <input type="submit" value="Найти">
        </div>
    </form>
</div>
<? else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Отчет по статусам сделок</title>
</head>
<body>
<? endif;?>
<div class="result">
<? if($arResult["RESULT"]):?>

    <? if($arResult["MODE"]!="exl"):?>
    <form method="POST" target="_blank">
        <input name="from" type="hidden" value="<?=$arResult["FROM"]?>">
        <input name="to" type="hidden" value="<?=$arResult["TO"]?>">
        <input name="period" type="hidden" value="<?=$arResult["PERIOD"]?>">
        <input name="devide" type="hidden" value="<?=$arResult["DEVIDE"]?>">
        <input name="dept" type="hidden" value="<?=$arResult["DEPT"]?>">
        <input name="rec" type="hidden" value="<?=$arResult["REC"]?>">
        <input name="mode" type="hidden" value="exl">
        <button type="submit">Выгрузить в Excel</button>
    </form>
    <? endif;?>
    <? foreach($arResult["RESULT"] as $arDate=>$arUsers):?>
    <h3><?=$arDate?></h3>
    <table class="table-result">
		<tr>
		    <th>№</th>
			<th>Менеджер</th>
			<th>Количество созданных сделок за период</th>
			<? foreach($arResult["TABLE_COLS"] as $th):?>
			<th><?=$th?> (количество)</th>
			<th><?=$th?> (% от общего количества)</th>
			<? endforeach;?>
		</tr>
		<? $iN=1;?>
		<? foreach($arUsers as $arUserID=>$arUser): ?>
		<tr>
		    <td align="right"><?=$iN?></td>
		    <?
		        $allCnt=0;
		        foreach($arUser as $arItem)
		            $allCnt+=$arItem["CNT"]
		    ?>
		    <td><?=$arResult["USERS"][$arUserID]["NAME"]?> <?=$arResult["USERS"][$arUserID]["LAST_NAME"]?></td>
			<td align="right"><?=$allCnt?></td>
			<? foreach($arResult["TABLE_COLS"] as $arI=>$arN):?>
			<td align="right"><?=intval($arUser[$arI]["CNT"])?></td>
			<td align="right"><?=ceil(intval($arUser[$arI]["CNT"])/$allCnt*100)?>%</td>
			<? endforeach;?>
		</tr>
		<? $iN++;?>
        <? endforeach; ?>
	</table>
	<? endforeach;?>
<? elseif($arResult["MODE"]):?>
<b>По Вашему запросу ничего не найдено</b>
<? endif;?>
</div>
<? if($arResult["MODE"]=="exl"):?>
</body></html>
<? endif;?>