<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "BML: Печать протокола",
    "DESCRIPTION" => 'Печать протокола собрания/планерки',
    "ICON" => "/images/icon.gif",
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "intranet",
        "NAME" => "Корпоративный портал",
        "CHILD" => array(
            "ID" => "meeting",
            "NAME" => "Собрания и планерки",
        ),
    ),
);
?>