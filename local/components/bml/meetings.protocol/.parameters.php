<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
    'GROUPS' => array(
        'PARAMS' => array(
            'NAME' => "Параметры отображения",
            'SORT' => 150,
        ),
    ),
    'PARAMETERS' => array(
        'ID' => array(
            'NAME' => 'ID собрания',
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'PARENT' => 'BASE',
        ),
        'FORMAT' => array(
            'NAME' => "Формат протокола",
            'TYPE' => 'LIST',
            'MULTIPLE' => 'N',
            'ADDITIONAL_VALUES' => 'Y',
            'PARENT' => 'BASE',
            'VALUES' => array(
                'WBM' => 'Формат WBM',
                'CMN' => 'Общий шаблон',
                'RQS' => 'Из параметров запроса'
            ),
            'DEFAULT' => 'WBM',
        ),
        'PRIORITY' => array(
            'NAME' => "Формат приоритета",
            'TYPE' => 'LIST',
            'MULTIPLE' => 'N',
            'PARENT' => 'PARAMS',
            'VALUES' => array(
                'W' => 'Словами (низкий, средний, высокий)',
                'D' => 'Цифрами (3, 2, 1)',
            ),
            'DEFAULT' => 'W',
        ),
        'PRIORITY_TYPE' => array(
            'NAME' => "Приоритет из задач",
            'TYPE' => 'LIST',
            'MULTIPLE' => 'N',
            'PARENT' => 'PARAMS',
            'VALUES' => array(
                'MAX' => 'Самый высокий',
                'MIN' => 'Самый низкий',
            ),
            'DEFAULT' => 'MAX',
        ),
        'DEADLINE_FROM_TASK' => array(
            'NAME' => "Срок исполнения",
            'TYPE' => 'LIST',
            'MULTIPLE' => 'N',
            'PARENT' => 'PARAMS',
            'VALUES' => array(
                'NOT' => 'Не брать из задач (кроме повторяющихся)',
                'MIN' => 'Брать самый близкий',
                'MAX' => 'Брать самый дальний',
            ),
            'DEFAULT' => 'MIN',
        ),
        'COMMENT' => array(
            'NAME' => "Устанавливать статус",
            'TYPE' => 'LIST',
            'MULTIPLE' => 'N',
            'PARENT' => 'PARAMS',
            'VALUES' => array(
                'P' => 'Только из повестки (игнорировать задачи)',
                'T' => 'Только из задач (игнорировать повестку)',
                'PT' => 'Группировать из повестки и задач',
            ),
            'DEFAULT' => 'PT',
        ),
        'RESPONSIBLE' => array(
            'NAME' => "Добавлять ответственных из задач",
            'TYPE' => 'CHECKBOX',
            'MULTIPLE' => 'N',
            'PARENT' => 'PARAMS',
            'DEFAULT' => "Y",
        ),
        'MEMBERS' => array(
            'NAME' => "Добавлять соисполнителей как ответственных",
            'TYPE' => 'CHECKBOX',
            'MULTIPLE' => 'N',
            'PARENT' => 'PARAMS',
            'DEFAULT' => "Y",
        ),

        'CACHE_TIME'  =>  array('DEFAULT'=>3600),
    ),
);
?>