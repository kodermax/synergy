<?php
$arResult['START'] = microtime(true);
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
if (!CModule::IncludeModule("meeting"))
{
    $arResult['ERROR'] = "Модуль 'meeting' не установлен...";
    $this->IncludeComponentTemplate();
    return;
}


$tmpID = $arParams['ID'];
if (!$tmpID){
    $tmpID = $_REQUEST['ID'];
}
$arResult['ID'] = $tmpID;
if ($arParams['FORMAT'] == "RQS"){
    $arResult['FORMAT'] = $_REQUEST['FORMAT'];
} else
{
    $arResult['FORMAT'] = $arParams['FORMAT'];
}
if(!empty($tmpID)){
    $tmpQuery = CMeeting::GetList(array(),
        array('ID' => intval($tmpID)), false, false,
        array()
    );
    if (!empty($tmpQuery)){
        $Meeting = $tmpQuery->fetch();

//        print_r($Meeting);
//        echo("<br><br>");
        if (!empty($Meeting)){
// Если мы здесь, то собрание найдено
// Сначала проверим, что у пользователя есть доступ к собранию
            $tmpTaskUsers = CMeeting::GetUsers($Meeting['ID']);
            $tmpHasAccess = $USER->IsAdmin();
            $tmpHasAccess = $tmpHasAccess || array_key_exists($USER->GetID(), $tmpTaskUsers);
            if (!$tmpHasAccess){
                $arResult['ERROR'] = 'Нет доступа к задаче.';
                $this->IncludeComponentTemplate();
                return;
            } // Если нет доступа, то прерываем обработку
// Инициализируем переменные, т.к. без этого php может вести себя странно
            $arResponsible=array();
            $arMembers=array();

// Переходим к получению данных из собрания
            $arResult['TITLE'] = $Meeting['TITLE'];
            $arResult['DATE_START'] = $Meeting['DATE_START'];
// Выбираем все пункты повестки дня в массив $MeetingItems
            $MeetingItemsDB = CMeeting::GetItems(intval($tmpID));
            $MeetingItems = array();
            while ($tmpRes = $MeetingItemsDB->fetch()){
                $MeetingItems[] = $tmpRes;
//                print_r($tmpRes);
            }
            $MeetingItems2Level = $MeetingItems; // Необходимо для поиска вложенных элементов (подпунктов)
//            print_r($MeetingItems2Level);
// ******************************************

// Формируем $arResult['ITEMS'] по следующему принципу:
// $arResult['ITEMS'][i][0] - это пункт верхнего уровня
// $arResult['ITEMS'][i][j] - где j=1..N - это подпункты
            $i = 0;
            $arResult['MEETINGITEMS'] = $MeetingItems;
            foreach ($MeetingItems as $tmpMeetingItem)
            {
//                print_r($tmpMeetingItem);
// Если $tmpRes['INSTANCE_PARENT_ID'] == 0 значит это пункт верхнего уровня
                if ($tmpMeetingItem['INSTANCE_PARENT_ID'] == 0)
                {
                    $arResult['ITEMS'][$i][0] = $tmpMeetingItem;
                    $tmpInstanceID = $tmpMeetingItem['ID'];
// Выбираем подпункты
                    $j = 0;
                    foreach ($MeetingItems2Level as $MeetingItem2Level)
                    {
                        if (($MeetingItem2Level['INSTANCE_PARENT_ID'] == $tmpInstanceID) || ($MeetingItem2Level['ID'] == $tmpInstanceID))
                        {
                            $arResult['ITEMS'][$i][$j] = $MeetingItem2Level;
// Ищем ответственного
                            $tmpUserID = CMeetingInstance::GetResponsible($MeetingItem2Level['ID']);
//                            echo("$tmpUserID:  ");
//                            print_r($tmpUserID);
                            if (!empty($tmpUserID[0]))
                            {
                                $tmpUser = CUser::GetByID($tmpUserID[0]);
                                $tmpUserArray = $tmpUser->fetch();
                                $arResult['ITEMS'][$i][$j]['USER'] = $tmpUserArray['LAST_NAME'] . " " . $tmpUserArray['NAME'] . " " . $tmpUserArray['SECOND_NAME'];
                                $arResponsible[] = $tmpUserArray['ID']; // Сохраняем ответственного в массиве пользователей
                            }
// Ищем отчет о выполнении
                            $tmpReportDB = CMeetingReports::GetList(array(),
                                array('ITEM_ID' => intval($MeetingItem2Level['ITEM_ID']), 'INSTANCE_ID' => intval($MeetingItem2Level['ID'])), false, false,
                                array()
                            );
                            $tmpReport = $tmpReportDB->fetch();
                            $arResult['ITEMS'][$i][$j]['REPORT'] = strip_tags($tmpReport['REPORT']);
                            if ($arResult['ITEMS'][$i][$j]['DEADLINE'] != '00.00.0000')
                            {
                                $tmpDeadLine = $arResult['ITEMS'][$i][$j]['DEADLINE'];
                            }
                            $tmpStatus = $arResult['ITEMS'][$i][$j]['REPORT'];
// Переходим к анализу информации из связанных задач
// Получаем список привязанных задач
                            $tmpTasks = CMeetingItem::GetTasks($MeetingItem2Level['ITEM_ID']);
                            foreach ($tmpTasks as $tmpTask)
                            {
                                $Task = CTasks::GetByID($tmpTask, false, ['returnAsArray' => true]);
//                                print_r($Task);
//                                echo("<br>");
// Определяем приоритет
// Если не инициализирован, то устанавливаем из задачи
                                if (!isset($tmpPriority))
                                {
                                    $tmpPriority = $Task['PRIORITY'];
                                }
// В зависимости от параметров обновляем приоритет или оставляем тот, который был
                                if ($arParams['PRIORITY_TYPE'] == 'MIN')
                                {
                                    if ($Task['PRIORITY'] < $tmpPriority)
                                    {
                                        $tmpPriority = $Task['PRIORITY'];
                                    }
                                } else
                                {
                                    if ($Task['PRIORITY'] > $tmpPriority)
                                    {
                                        $tmpPriority = $Task['PRIORITY'];
                                    }
                                }
// Анализируем срок исполнения
// Логика такая: Если установлен срок в пункте протокола, то задачи вообще анализироваться не будут
                                if ($arParams['DEADLINE_FROM_TASK'] != 'NOT')
                                {
                                    if (!isset($tmpDeadLine)) // вот эта строчка блокирует анализ deadlin-а из задач, если устновлен срок в пункте прротокола
                                    {
                                        $tmpDeadLine = $Task['DEADLINE'];
                                    }
                                    if ($arParams['DEADLINE_FROM_TASK'] == 'MIN')
                                    {
                                        if ($DB->CompareDates($tmpDeadLine, $Task['DEADLINE']) == 1)
                                        {
                                            $tmpDeadLine = $Task['DEADLINE'];
                                        }
                                    } elseif ($arParams['DEADLINE_FROM_TASK'] == 'MAX')
                                    {

                                        if ($DB->CompareDates($tmpDeadLine, $Task['DEADLINE']) == -1)
                                        {
                                            $tmpDeadLine = $Task['DEADLINE'];
                                        }
                                    }
                                }
// Теперь выбираем пользовательское поле UF_REPEAT
                                if (!empty($Task['UF_REPEAT']))
                                {
                                    $tmpRepeatDB = CUserFieldEnum::GetList(array(), array("ID" => $Task['UF_REPEAT']));
// Проверяем, что задача периодическая
// Если периодическая, то пока что устанавливаем периодичность, игнорируя вычисленный ранее срок
                                    $tmpRepeat = $tmpRepeatDB->fetch();
                                    if ($tmpRepeat['XML_ID'] != 'UF_REPEAT_NO_REPEAT')
                                    {
                                        $arResult['ITEMS'][$i][$j]['DEADLINE'] = $tmpRepeat['VALUE'];
                                    }
//                                    print_r("UF_REPAET:" . $Task['UF_REPEAT']);
//                                    echo('<br>');

                                }
// Начинаем обработку комментариев
                                $tmpCommentsDB = CForumMessage::GetListEx(array("ID" => "ASC"), array("FORUM_ID" => $Task['FORUM_ID'], 'TOPIC_ID' => $Task['FORUM_TOPIC_ID'], "!PARAM1" => 'TK'));
                                while ($tmpComment = $tmpCommentsDB->fetch())
                                {
//                                    print_r($tmpComment);
                                    $tmpStatusFromTask = strip_tags($tmpComment['POST_MESSAGE']);
                                }
                                if (!empty($tmpStatusFromTask))
                                {
                                    (empty($tmpStatusFromTaskFull)) ? $tmpStatusFromTaskFull = $tmpStatusFromTask : $tmpStatusFromTaskFull = $tmpStatusFromTaskFull . "<br>" . $tmpStatusFromTask;
//                                    echo("----".$tmpStatusFromTaskFull);
                                }

// Выберем в массив пользователей ответственного из задачи
//                                (empty($tmpResponsible)) ? $tmpResponsible = $Task['RESPONSIBLE_LAST_NAME']." ".$Task['RESPONSIBLE_NAME'] : $tmpResponsible = $tmpResponsible . "<br>" . $Task['RESPONSIBLE_LAST_NAME']." ".$Task['RESPONSIBLE_NAME'];

// Если формат отчета "Общий отчет (план действий)", то необходимо, чтобы в поле ответственный был один сотрудник,
// а все остальные попадали бы в соисполнители
                                if ($arResult['FORMAT'] == 'CMN'){
                                    if ((empty($arResponsible)) && (!empty($Task['RESPONSIBLE_ID']))){
// Проверим, что этот сотрудник не был добавлен ранее
                                        if (array_search($Task['RESPONSIBLE_ID'], $arResponsible) === false) {
                                            $arResponsible[] = $Task['RESPONSIBLE_ID'];
                                        }
                                    } else{
                                        if (!empty($Task['RESPONSIBLE_ID'])) {
                                            if ((array_search($Task['RESPONSIBLE_ID'], $arResponsible) === false) &&
                                                (array_search($Task['RESPONSIBLE_ID'], $arMembers)) === false) {
                                                $arMembers[] = $Task['RESPONSIBLE_ID'];
                                            }
                                        }
                                    }

                                } else {
                                    if (!empty($Task['RESPONSIBLE_ID'])) {
// Проверим, что этот сотрудник не был добавлен ранее
                                        if (array_search($Task['RESPONSIBLE_ID'], $arResponsible) === false) {
                                            $arResponsible[] = $Task['RESPONSIBLE_ID'];
                                        }
                                    }
                                }
// Выбираем соисполнителей из задачи
                                $tmpMembersDB = CTaskMembers::GetList(array(), array("TASK_ID" => $Task['ID']));
                                while ($tmpMembers = $tmpMembersDB->fetch()){
// Проверим, что этот сотрудник не был добавлен ранее
                                    if ((array_search($tmpMembers['USER_ID'], $arResponsible) === false) &&
                                        (array_search($tmpMembers['USER_ID'], $arMembers)=== false)) {
                                        $arMembers[] = $tmpMembers['USER_ID'];
                                    }

                                }
//                                print_r($Task);
//                                    print_r($tmpRepeatDB->fetch());

                            } //Конец цикла обхода задач
//                            print_r($arResponsible);
// Если приорритет инициализирован, то устанавливаем элемент arResult в завимимости от того
// требуется ли указывать приоритет цифрами или словами.
                            if (isset($tmpPriority))
                            {
                                if ($arParams['PRIORITY'] == 'D')
                                {
                                    $arResult['ITEMS'][$i][$j]['PRIORITY'] = 3 - $tmpPriority;
                                } elseif ($arParams['PRIORITY'] == 'W')
                                {
                                    $tmpArray = ['Низкий', 'Средний', 'Высокий'];
                                    $arResult['ITEMS'][$i][$j]['PRIORITY'] = $tmpArray[$tmpPriority];
                                }
                            }
                            // Если $arResult['ITEMS'][$i][$j]['DEADLINE'] уже установлен - ЗНАЧИТ В НЕМ ЗНАЧЕНИЕ ПЕРИОДИЧНОСТИ (и тогда мы никаких дат не ставим)
                            if (($arParams['DEADLINE_FROM_TASK'] != 'NOT') && isset($tmpDeadLine) && empty($arResult['ITEMS'][$i][$j]['DEADLINE']))
                            {
                                $arResult['ITEMS'][$i][$j]['DEADLINE'] = $DB->FormatDate($tmpDeadLine, "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY");
                            }
// Если надо брать статус только из повестки, но в повестке отчет пустой, то все равно берем из задач
                            if (($arParams['COMMENT'] == 'P') && (empty($tmpStatus)))
                            {
                                $arResult['ITEMS'][$i][$j]['REPORT'] = $tmpStatusFromTaskFull;
                            }
                            if (($arParams['COMMENT'] == 'PT') && isset($tmpStatusFromTaskFull))
                            {
                                (empty($arResult['ITEMS'][$i][$j]['REPORT'])) ? $arResult['ITEMS'][$i][$j]['REPORT'] = $tmpStatusFromTaskFull : $arResult['ITEMS'][$i][$j]['REPORT'] = $arResult['ITEMS'][$i][$j]['REPORT'] . "<br>" . $tmpStatusFromTaskFull;
                            }
                            if (($arParams['COMMENT'] == 'T') && isset($tmpStatusFromTaskFull))
                            {
                                $arResult['ITEMS'][$i][$j]['REPORT'] = $tmpStatusFromTaskFull;
                            }
// Вырежем из отчета служебные идентификаторы объектов Bitrix, если они туда попали...
                            $arResult['ITEMS'][$i][$j]['REPORT'] = preg_replace("/\[(.*)\]/", "", $arResult['ITEMS'][$i][$j]['REPORT']);
// Иногда в текст попадает пробел, надо его убрать, если строка состоит только из пробела
                            if ($arResult['ITEMS'][$i][$j]['REPORT'] == " "){
                                $arResult['ITEMS'][$i][$j]['REPORT'] = "";
                            }
// Обрабатываем ответственного

                            if ($arParams['RESPONSIBLE'] == "Y"){
                                $arResult['ITEMS'][$i][$j]['USER'] = "";
                                if (($arParams['MEMBERS'] == "Y") || ($arResult['FORMAT'] == 'WBM')){
                                    if(isset($arMembers) && isset($arResponsible)) {
                                        $arResponsible = array_merge($arResponsible, $arMembers);
                                    }
//                                    echo("----------:  ");
//                                    print_r($arResponsible);
//                                    echo("<br>");
                                }
                                $arResponsible = array_unique($arResponsible);
                                $arMembers = array_unique($arMembers);
                                foreach ($arMembers as $tmpUserID2)
                                {
                                    $tmpUser = CUser::GetByID($tmpUserID2);
                                    $tmpUserArray = $tmpUser->fetch();
                                    $arResult['ITEMS'][$i][$j]['MEMBER'] = $arResult['ITEMS'][$i][$j]['MEMBER'].$tmpUserArray['LAST_NAME'] . " " . $tmpUserArray['NAME'] . " " . $tmpUserArray['SECOND_NAME'] . "<br>";
                                }
                                foreach ($arResponsible as $tmpUserID2)
                                {
                                    $tmpUser = CUser::GetByID($tmpUserID2);
                                    $tmpUserArray = $tmpUser->fetch();
                                    if (empty($arResult['ITEMS'][$i][$j]['USER'])){
                                        $arResult['ITEMS'][$i][$j]['USER'] = "<b>". $tmpUserArray['LAST_NAME'] . " " . $tmpUserArray['NAME'] . " " . $tmpUserArray['SECOND_NAME'] . "</b><br>";

                                    } else{
                                        $arResult['ITEMS'][$i][$j]['USER'] = $arResult['ITEMS'][$i][$j]['USER'] . $tmpUserArray['LAST_NAME'] . " " . $tmpUserArray['NAME'] . " " . $tmpUserArray['SECOND_NAME'] . "<br>";
                                    }
                                }
                            }

                            unset($tmpPriority);
                            unset($tmpDeadLine);
                            unset($tmpStatusFromTask);
                            unset($tmpStatusFromTaskFull);
                            $arResponsible=array();
                            $arMembers=array();
                            $j++;
                        }
                    }
                    $i++;
                }
            }
            $arResult['MEETING'] = $Meeting;
        } else{
            $arResult['ERROR'] = "Собрание не найдено. ID=".$tmpID;
        }
    } else{
        $arResult['ERROR'] = "Ошибка GetList. ID=".$tmpID;
    }

} else{
    $arResult['ERROR'] = "Не указан ID собрания.";
}

$this->IncludeComponentTemplate();