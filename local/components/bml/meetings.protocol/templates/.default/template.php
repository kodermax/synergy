<?php
$GLOBALS["APPLICATION"]->AddHeadString("<link rel=\"stylesheet\" href=\"/local/components/bml/meetings.protocol/templates/.default/print.css?".time()."\">", true);
//https://msk1-portaldev04/services/meeting/print-report.php?ID=11&clear_cache=Y
if ($arResult['ERROR'])
{
    echo('Ошибка: '.$arResult['ERROR']);
} else{
// Формат WBM
    if ($arResult['FORMAT'] == 'WBM'){?>
        <div class="bml-pr-wrapper">
        <p class="bml-pr-h1">УНИВЕРСИТЕТ "СИНЕРГИЯ"</p>
        <p class="bml-pr-h2" style="margin-bottom: 5px">Совещание</p>
        <p class="bml-pr-h2" style="margin-top: 5px"><b><?=$arResult['TITLE']?></b></p>
        <p class="bml-pr-main-date"><?=$DB->FormatDate($arResult['DATE_START'], "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY")?></p>
        <div class="pagebreak">
        <table class="bml-pr-table">
            <tr>
                <th>
                    №
                </th>
                <th>
                    Наименование задачи
                </th>
                <th>
                    Пр-т
                </th>
                <th>
                    Исполнители
                </th>
                <th>
                    Срок
                </th>
            </tr>
            </table>
            </div>
            <?
            $counter = 1;
            foreach($arResult['ITEMS'] as $tmpItems){
                if (count($tmpItems) > 1){
                ?>
                <div class="pagebreak">
                <table class="bml-pr-table">
                <tr>
                    <td colspan="5" class="bml-pr-table-td-level1">
                        <?=$tmpItems[0]['TITLE']?>
                    </td>
                </tr>
                </table>
                </div>
                <?}
                $i=0;
                foreach($tmpItems as $tmpItem){
                    if ((($i>0) && (count($tmpItems) > 1)) || (count($tmpItems) == 1)){
                    ?>
                        <div class="pagebreak">
                        <table class="bml-pr-table">
                        <tr>
                            <td><?=$counter; //№?></td>
                            <td><?=$tmpItem['TITLE'] //Наименование задачи?></td>
                            <td style="text-align: center"><?=$tmpItem['PRIORITY']//Приоритет?></td>
                            <td style="text-align: center"><?=$tmpItem['USER'] //Исполнители?></td>
                            <td style="text-align: center"><?=(($tmpItem['DEADLINE']!='00.00.0000') ? $tmpItem['DEADLINE'] : ''); //Срок?></td>
                        </tr>
                        </table>

                        <?
                        if (!empty($tmpItem['REPORT'])){
                        ?>
                            <table class="bml-pr-table">

                            <tr>
                                <td colspan="5" class="bml-pr-table-td-level2_last">
                                    <?="Статус:  ".$tmpItem['REPORT']?>
                                </td>
                            </tr>
                            </table>
                            <?
                        }?>
                        </div>
                        <?
                        $counter++;
                    }
                    $i++;
                }
            }
            ?>
<!--        </table>-->
        </div>
    <?
// Обычный формат
    } else{?>

        <div class="bml-pr-tpl2-wrapper">
            <p class="bml-pr-h1">УНИВЕРСИТЕТ "СИНЕРГИЯ"</p>

            <p class="bml-pr-tpl2-h2"><?=$arResult['TITLE']?></p>
            <p class="bml-pr-main-date"><?=$DB->FormatDate($arResult['DATE_START'], "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY")?></p>
            <div class="pagebreak">
            <table class="bml-pr-tpl2-table">
                <tr>
                    <th>
                        №
                    </th>
                    <th>
                        Действие
                    </th>
                    <th>
                        Пр-т
                    </th>
                    <th>
                        Ответственный
                    </th>
                    <th>
                        Соисполнители
                    </th>
                    <th>
                        Срок
                    </th>
                    <th>
                        Статус/Комментарий
                    </th>
                </tr>
            </table>
            </div>
                <?
                $counter = 1;
                foreach($arResult['ITEMS'] as $tmpItems){
                    ?>
                    <div class="pagebreak">
                    <table class="bml-pr-tpl2-table">
                    <tr class="bml-pr-table-tpl2-tr-level1">
                        <td><?=$counter; //№?></td>
                        <td><?=$tmpItems[0]['TITLE'] //Наименование задачи?></td>
<!--                        <td>--><?//=print_r($tmpItems)?><!--</td>-->
                        <td style="text-align: center"><?=$tmpItems[0]['PRIORITY']//Приоритет?></td>
                        <td><?print_r($tmpItems[0]['USER']) //Ответственный?></td>
                        <td><?=$tmpItems[0]['MEMBER']?></td>
                        <td style="text-align: center"><?=(($tmpItems[0]['DEADLINE']!='00.00.0000') ? $tmpItems[0]['DEADLINE']:''); //Срок?></td>
                        <td><?=$tmpItems[0]['REPORT']?></td>
                    </tr>
                    </table>
                    </div>
                    <?
                    $i=0;
                    foreach($tmpItems as $tmpItem){
                        if ($i>0){
                            ?>
                            <div class="pagebreak">
                            <table class="bml-pr-tpl2-table">
                            <tr>
                                <td><?=$counter.".".$i //№?></td>
                                <td><?=$tmpItem['TITLE'] //Наименование задачи?></td>
                                <td style="text-align: center"><?=$tmpItem['PRIORITY']//Приоритет?></td>
                                <td><?print_r($tmpItem['USER']) //Ответственный?></td>
                                <td><?=$tmpItem['MEMBER']?></td>
                                <td style="text-align: center"><?=(($tmpItem['DEADLINE']!='00.00.0000') ? $tmpItem['DEADLINE']:''); //Срок?></td>
                                <td><?=$tmpItem['REPORT']?></td>
                            </tr>
                            </table>
                            </div>
                            <?
//                            if (!empty($tmpItem['REPORT'])){
//                                ?>
<!--                                <tr>-->
<!--                                    <td colspan="5" class="bml-pr-table-td-level2_last">-->
<!--                                        --><?//="Статус:  ".$tmpItem['REPORT']?>
<!--                                    </td>-->
<!--                                </tr>-->
<!--                            --><?//
//                            }
                        }
                        $i++;
                    }
                    $counter++;
                }
                ?>
        </div>
    <?

    }
//    print_r($arResult['MEETINGITEMS']);
//    print_r($arResult['MEETING']);
//    echo ("<br>");
//    echo ("<br>");
//    print_r($arResult['ITEMS']);
//    echo ("<br>");
//    echo ("<br>");
//    foreach ($arResult['ITEMS'] as $her){
//        print_r($her);
//        echo ("<br>");
//        echo ("<br>");
//        echo ("<br>");
//    }
}
//echo 'Время построения отчета: '.(microtime(true) - $arResult['START']).' сек.';
?>