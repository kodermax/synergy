<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

if (!CModule::IncludeModule('intranet'))
    return;

$arParams['VIEW_START'] = isset($arParams['VIEW_START']) ? $arParams['VIEW_START'] : 'month';
if (!isset($arParams['FIRST_DAY']))
    $arParams['FIRST_DAY'] = 1;
else
{
    $arParams['FIRST_DAY'] = intval($arParams['FIRST_DAY']);
    if ($arParams['FIRST_DAY'] < 0 || $arParams['FIRST_DAY'] >= 7)
        $arParams['FIRST_DAY'] = 1;
}

if (!isset($arParams['DAY_START']))
    $arParams['DAY_START'] = 9;
else
{
    $arParams['DAY_START'] = intval($arParams['DAY_START']);
    if ($arParams['DAY_START'] < 0 || $arParams['DAY_START'] >= 24)
        $arParams['DAY_START'] = 9;
}

if (!isset($arParams['DAY_FINISH']))
    $arParams['DAY_FINISH'] = 18;
else
{
    $arParams['DAY_FINISH'] = intval($arParams['DAY_FINISH']);
    if ($arParams['DAY_FINISH'] < 0 || $arParams['DAY_FINISH'] >= 24)
        $arParams['DAY_FINISH'] = 18;
}

if ($arParams['DAY_FINISH'] < $arParams['DAY_START'])
{
    $tmp = $arParams['DAY_FINISH'];
    $arParams['DAY_FINISH'] = $arParams['DAY_START'];
    $arParams['DAY_START'] = $tmp;
}

if (!$arParams['SITE_ID'] && defined('SITE_ID'))
    $arParams['SITE_ID'] = SITE_ID;

$arParams['DAY_SHOW_NONWORK'] = $arParams['DAY_SHOW_NONWORK'] == 'Y' ? 'Y' : 'N';

$arFullControlsList = array('DATEPICKER', 'TYPEFILTER', 'SHOW_ALL');

if (!CModule::IncludeModule('extranet') || !CExtranet::IsExtranetSite($arParams['SITE_ID']))
    $arFullControlsList[] = 'DEPARTMENT';

if (!isset($arParams['FILTER_CONTROLS']) || !is_array($arParams['FILTER_CONTROLS']))
    $arParams['FILTER_CONTROLS'] = array('DATEPICKER', 'TYPEFILTER', 'DEPARTMENT');

$arResult['CONTROLS'] = array();
foreach ($arFullControlsList as $control)
{
    if (in_array($control, $arParams['FILTER_CONTROLS']))
        $arResult['CONTROLS'][$control] = 'on';
}

$arParams['DETAIL_URL_PERSONAL'] = isset($arParams['DETAIL_URL_PERSONAL']) ? $arParams['DETAIL_URL_PERSONAL'] : '/company/personal/user/#USER_ID#/calendar/?EVENT_ID=#EVENT_ID#';
$arParams['DETAIL_URL_DEPARTMENT'] = isset($arParams['DETAIL_URL_DEPARTMENT']) ? $arParams['DETAIL_URL_DEPARTMENT'] : '/company/structure.php?set_filter_structure=Y&structure_UF_DEPARTMENT=#ID#';

if (strlen($arParams['IBLOCK_TYPE']) <= 0)
    $arParams['IBLOCK_TYPE'] = COption::GetOptionString('intranet', 'iblock_type');
$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
if ($arParams['IBLOCK_ID'] <= 0)
    $arParams['IBLOCK_ID'] = COption::GetOptionInt('intranet', 'iblock_absence');

$arParams['IBLOCK_CHANGED'] = $arParams['IBLOCK_ID'] != COption::GetOptionInt('intranet', 'iblock_absence');

$arParams['CALENDAR_IBLOCK_ID'] = intval($arParams['CALENDAR_IBLOCK_ID']);
if ($arParams['CALENDAR_IBLOCK_ID'] !== -1)
{
    if ($arParams['CALENDAR_IBLOCK_ID'] <= 0)
        $arParams['CALENDAR_IBLOCK_ID'] = COption::GetOptionInt('intranet', 'iblock_calendar');
}

$arResult['ABSENCE_TYPES'] = array();
$dbTypeRes = CIBlockPropertyEnum::GetList(array("SORT"=>"ASC", "VALUE"=>"ASC"), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'PROPERTY_ID' => 'ABSENCE_TYPE'));
while ($arTypeValue = $dbTypeRes->GetNext())
{
    $arResult['ABSENCE_TYPES'][$arTypeValue['ID']] = $arTypeValue;
}

$arParams['NAME_TEMPLATE'] = empty($arParams['NAME_TEMPLATE']) ? CSite::GetNameFormat(false) : str_replace(array("#NOBR#","#/NOBR#"), array("",""), $arParams["NAME_TEMPLATE"]);

if (!$arParams['DATE_FORMAT'])
{
    $arParams['DATE_FORMAT'] = (
    isset($_SESSION['intranet_absence_calendar_date_format'])
        ? $_SESSION['intranet_absence_calendar_date_format']
        : CComponentUtil::GetDateFormatDefault()
    );
}
elseif (!$arParams['AJAX_CALL'])
{
    $_SESSION['intranet_absence_calendar_date_format'] = $arParams['DATE_FORMAT'];
}

if (!$arParams['DATETIME_FORMAT'])
{
    $arParams['DATETIME_FORMAT'] = (
    isset($_SESSION['intranet_absence_calendar_datetime_format'])
        ? $_SESSION['intranet_absence_calendar_datetime_format']
        : CComponentUtil::GetDateTimeFormatDefault()
    );
}
elseif (!$arParams['AJAX_CALL'])
{
    $_SESSION['intranet_absence_calendar_datetime_format'] = $arParams['DATETIME_FORMAT'];
}
//echo "РЕЗУЛЬТАТ!";

if ($arParams['AJAX_CALL'] == 'DATA')
{
    if (!check_bitrix_sessid())
        return;

    if ($arParams['IBLOCK_ID'] <= 0)
        return;

    $arParams['FILTER_SECTION_CURONLY'] = $arParams['FILTER_SECTION_CURONLY'] == 'Y' ? 'Y' : 'N';
    $arParams['DETAIL_URL'] = COption::GetOptionString('intranet', 'search_user_url', '/company/personal/user/#ID#/', $arParams['SITE_ID']);

    $arParams['CALLBACK'] = trim($arParams['CALLBACK']);
    $arParams['TS_START'] = date('U', $arParams['TS_START']);
    $arParams['TS_FINISH'] = date('U', $arParams['TS_FINISH']);

    $arParams['TYPES'] = $arParams['TYPES'] ? explode(',', $arParams['TYPES']) : array();

    $ar_depts = CIntranetUtils::GetUserDepartments($USER->GetId());

// BML устанавливаем департамент
// Было так и это работало:
//    if (!isset($_REQUEST['DEPARTMENT'])){
//        $ar_depts = CIntranetUtils::GetUserDepartments($USER->GetId());
//        if (!empty($ar_depts)) {
//            $arParams['DEPARTMENT'] = $ar_depts[0];
//        }
//    } else {
//        if (empty($_REQUEST['DEPARTMENT'])) {
//            $arParams['DEPARTMENT'] = $arParams['DEPARTMENT'] ? intval($arParams['DEPARTMENT']) : 0;
//        }
//    }
// После того, как мы добавили возможность открытия отчета по конкретному пользователю
// сделали так (я уже сам не очень понимаю как это работает)
// Идея примерно такая: если у нас нет фильтра по ФИО, то отрабатывает предыдущая логика, которая устанавливает департамент пользователя,
// в том случае, если департамент не указан, т.е. не передан в параметрах REQUEST (а это достигается за счет того, что мы
// вместо DEPARTMENT:'' сделали DEPARTMENT:null в файле /local/templates/bitrix24/components/bitrix/intranet.absence.calendar/.default/script.js
// Если же есть фильтр по ФИО, то никаких настроек мы не трогаем и по идее будет тот фильтр, который выставлен у клиента,
// Ну как-тьо так в общем....
    if (empty($_REQUEST['fio'])) {
        if (!isset($_REQUEST['DEPARTMENT'])) {
            $ar_depts = CIntranetUtils::GetUserDepartments($USER->GetId());
            if (!empty($ar_depts)) {
                $arParams['DEPARTMENT'] = $ar_depts[0];
            }
        } else {
            if (empty($_REQUEST['DEPARTMENT'])) {
                $arParams['DEPARTMENT'] = $arParams['DEPARTMENT'] ? intval($arParams['DEPARTMENT']) : 0;
            }
        }
    }
    if (!empty($_REQUEST['fio'])) {
        $tmpFIO = $_REQUEST['fio'];
    } else {
        $tmpFIO = "";
    }
//---------------------------------

    $arParams['SHORT_EVENTS'] = $arParams['SHORT_EVENTS'] == 'N' ? 'N' : 'Y';
    $arParams['USERS_ALL'] = $arParams['USERS_ALL'] == 'Y' ? 'Y' : 'N';

    $arResult['ERROR_CODE'] = '';
    if (strlen($arParams['CALLBACK']) <= 0)
        $arResult['ERROR_CODE'] = 'ERROR_NO_CALLBACK';
    elseif ($arParams['TS_START'] <= 0)
        $arResult['ERROR_CODE'] = 'ERROR_NO_TS_START';
    elseif ($arParams['TS_FINISH'] <= 0)
        $arResult['ERROR_CODE'] = 'ERROR_NO_TS_FINISH';

    $arResult['USERS'] = array();

    if ($arResult['ERROR_CODE'] == '')
    {
        $MODE = 0;
        if (count($arParams['TYPES']) <= 0)
        {
            $MODE = BX_INTRANET_ABSENCE_ALL;
        }
        else
        {
            foreach ($arParams['TYPES'] as $type)
            {
                if ($type == 'PERSONAL')
                    $MODE |= BX_INTRANET_ABSENCE_PERSONAL;
                else
                    $MODE |= BX_INTRANET_ABSENCE_HR;

                if ($MODE == BX_INTRANET_ABSENCE_ALL)
                    break;
            }
        }
// + BML ****************************
// Если пользователь относится к группе "Администраторы контроля посещаемости", то фильтровать не будем
// Иначе добавляем в фильтр самого пользователя и всех его подчиненных
        $isABSadmin = isAbsenceAdmin($USER->GetID());
        if (!$isABSadmin) {
// Изначально было так:
//            $arUserFilter[] = $USER->GetID();
//            $dbUserFilter = CIntranetUtils::GetSubordinateEmployees($USER->GetID(), CHECKTYPE);
//            while ($arUser = $dbUserFilter->fetch()) {
//                $arUserFilter[] = $arUser['ID'];
//            }
// После добавления функционала секретарей стало так
            $arUserFilter = GetSubordinateEmployees_ex($USER->GetID(), CHECKTYPE);

        } else{
            $arUserFilter = false; // НЕ фильтруем пользователей
        }
// **********************************
        $arIBlockElements = CIntranetUtils::GetAbsenceData(
            array(
                'CALENDAR_IBLOCK_ID' => $arParams['CALENDAR_IBLOCK_ID'],
                'ABSENCE_IBLOCK_ID' => $arParams['IBLOCK_ID'],
                'DATE_START' => ConvertTimeStamp($arParams['TS_START'], 'FULL'),
                'DATE_FINISH' => ConvertTimeStamp($arParams['TS_FINISH'], 'FULL'),
// Было                'USERS' => false,
// Стало
                'USERS' => $arUserFilter,
                'PER_USER' => true,
// +BML - выбираем дополнительные свойства
                'SELECT' => array('ID', 'IBLOCK_ID', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', 'NAME',
                                  'PREVIEW_TEXT', 'DETAIL_TEXT', 'PROPERTY_USER', 'PROPERTY_FINISH_STATE',
                                  'PROPERTY_STATE', 'PROPERTY_ABSENCE_TYPE', 'PROPERTY_SALTO_APPROVED', 'PROPERTY_SALTO_IMPORTED'),
// ***************************************
            ), $MODE
        );
//
        $arUserIDs = array_keys($arIBlockElements);

        $bLoadUsers = (count($arUserIDs) > 0) || ($arParams['USERS_ALL'] == 'Y');

        if ($bLoadUsers)
        {
            $arFilter = array('ACTIVE' => 'Y');

            if ($arParams['DEPARTMENT'] > 0)
                $arFilter['UF_DEPARTMENT'] =
                    $arParams['FILTER_SECTION_CURONLY'] == 'N'
                        ? CIntranetUtils::GetIBlockSectionChildren($arParams['DEPARTMENT'])
                        : array($arParams['DEPARTMENT']);
            elseif (!CModule::IncludeModule('extranet') || !CExtranet::IsExtranetSite($arParams['SITE_ID']))
                $arFilter["!UF_DEPARTMENT"] = false;

            if ($arParams['USERS_ALL'] == 'N')
            {
                $arFilter['ID'] = implode('|', array_keys($arIBlockElements));
            }

            $bExtranetInstalled = CModule::IncludeModule('extranet');
            $bExtranetSite = false || ($bExtranetInstalled && CExtranet::IsExtranetSite($arParams['SITE_ID']));

            if ($bExtranetInstalled && !$bExtranetSite && !CExtranet::IsIntranetUser($arParams['SITE_ID']))
                die();

            if ($bExtranetSite)
            {
                $arUsersInMyGroupsID = CExtranet::GetMyGroupsUsers($arParams['SITE_ID']);
                $arPublicUsersID = CExtranet::GetPublicUsers();
            }

            $obUser = new CUser();
            $dbUsers = $obUser->GetList(
                $sort_by = 'LAST_NAME',
                $sort_dir = 'asc',
                $arFilter,
                array(
                    'FIELDS' => array('ID', 'LOGIN', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PROFESSION', 'WORK_POSITION'),
                    'SELECT' => array('UF_DEPARTMENT')
                )
            );
            $arUsers = array();
            while ($arUser = $dbUsers->Fetch())
            {
// Проверим соответствие ФИО пользователя фильтру по ФИО (если он установлен)
// Если соответствие не найдено, то переходим к следующему пользователю
                if ($tmpFIO != ""){
                    if (strpos(strtoupper($arUser["LAST_NAME"]. " ". $arUser["NAME"]), strtoupper($tmpFIO)) === false){
                        continue;
                    }
                }
                if (!is_array($arUser['UF_DEPARTMENT']))
                    $arUser['UF_DEPARTMENT'] = array();

                if (!$bExtranetSite && count($arUser["UF_DEPARTMENT"]) <= 0)
                    continue;

                if ($bExtranetSite && !in_array($arUser["ID"], $arUsersInMyGroupsID) && !in_array($arUser["ID"], $arPublicUsersID) && $arUser["ID"] != $GLOBALS["USER"]->GetID())
                    continue;

                $arUsers[$arUser['ID']] = array(
                    'ID' => $arUser['ID'],
                    'LOGIN' => $arUser['LOGIN'],
                    'NAME' => $arUser['NAME'],
                    'LAST_NAME' => $arUser['LAST_NAME'],
                    'SECOND_NAME' => $arUser['SECOND_NAME'],
                    'PERSONAL_PROFESSION' => $arUser['PERSONAL_PROFESSION'] ? $arUser['PERSONAL_PROFESSION'] : $arUser['WORK_POSITION'],
                    'DATA' => is_array($arIBlockElements[$arUser['ID']]) ? $arIBlockElements[$arUser['ID']] : array(),
                    'DETAIL_URL' => str_replace(array('#ID#', '#USER_ID#'), $arUser['ID'], $arParams['DETAIL_URL']),
                );

                foreach ($arUsers[$arUser['ID']]['DATA'] as $key => $arEntry)
                {
                    if ($arEntry['ENTRY_TYPE'] == BX_INTRANET_ABSENCE_HR)
                    {
                        $arEntry['DATE_ACTIVE_FROM'] = MakeTimeStamp($arEntry['DATE_ACTIVE_FROM']);
                        $arEntry['DATE_ACTIVE_TO'] = MakeTimeStamp($arEntry['DATE_ACTIVE_TO']);

                        $arEntry['DATE_FROM'] = FormatDate(
                            $DB->DateFormatToPhp(CSite::GetDateFormat(
                                CIntranetUtils::IsDateTime($arEntry['DATE_ACTIVE_FROM']) ? 'FULL' : 'SHORT', $arParams['SITE_ID'], true
                            )),
                            $arEntry['DATE_ACTIVE_FROM']
                        );
                        $arEntry['DATE_TO'] = FormatDate(
                            $DB->DateFormatToPhp(CSite::GetDateFormat(
                                CIntranetUtils::IsDateTime($arEntry['DATE_ACTIVE_TO']) ? 'FULL' : 'SHORT', $arParams['SITE_ID'], true
                            )),
                            $arEntry['DATE_ACTIVE_TO']
                        );

                        $arEntry['TYPE'] = is_array($arResult['ABSENCE_TYPES'][$arEntry['PROPERTY_ABSENCE_TYPE_ENUM_ID']]) ? $arResult['ABSENCE_TYPES'][$arEntry['PROPERTY_ABSENCE_TYPE_ENUM_ID']]['XML_ID'] : '';

// BML определяем тип нарушения для визуализации через JS
// Всего есть три типа:
// 1 - опоздание (показываем в первой половине ячейки)
// 2 - ранний уход (показываем во второй половине ячейки)
// 3 - отсутствие целый день (показывается во всей ячейке)
// Логика определения описана в описании функции
                        $arEntry['VISUAL_TYPE'] = GetVisualViolationTypeByAbsenceId($arEntry['ID'], $arEntry);
// Добавим в массив время, чтобы проще было потом в JS его выводить
                        $arEntry['TIME_TO'] = $DB->FormatDate($arEntry['DATE_TO'], CSite::GetDateFormat('FULL'), "HH:MI");
                        $arEntry['TIME_FROM'] = $DB->FormatDate($arEntry['DATE_FROM'], CSite::GetDateFormat('FULL'), "HH:MI");
// Добавим в массив фактическое начало и конец рабочего дня
                        $arWorkTime = GetWorkTime($arUser['ID'], $arEntry['DATE_FROM']);
                        $arEntry['DATE_FACT_START'] = $DB->FormatDate($arWorkTime['START_TIME'], CSite::GetDateFormat('FULL'), "HH:MI");;
                        $arEntry['DATE_FACT_END'] = $DB->FormatDate($arWorkTime['END_TIME'], CSite::GetDateFormat('FULL'), "HH:MI");;
                    }
                    elseif ($arEntry['ENTRY_TYPE'] == BX_INTRANET_ABSENCE_PERSONAL)
                    {
                        //$arEntry['NAME'] .= ' - '.$arEntry['DATE_FROM'].'-'.$arEntry['DATE_TO'];

                        $arEntry['IBLOCK_ID'] = $arParams['CALENDAR_IBLOCK_ID'];

                        $arEntry['DATE_ACTIVE_FROM'] = $arEntry['DT_FROM_TS'] + date('Z') - date('Z', $arEntry['DT_FROM_TS']);
                        $arEntry['DATE_ACTIVE_TO'] = $arEntry['DT_TO_TS'] + date('Z') - date('Z', $arEntry['DT_TO_TS']);

                        $arEntry['DATE_FROM'] = FormatDate(
                            $DB->DateFormatToPhp(CSite::GetDateFormat(
                                CIntranetUtils::IsDateTime($arEntry['DATE_ACTIVE_FROM']) ? 'FULL' : 'SHORT', $arParams['SITE_ID'], true
                            )),
                            $arEntry['DATE_ACTIVE_FROM']
                        );
                        $arEntry['DATE_TO'] = FormatDate(
                            $DB->DateFormatToPhp(CSite::GetDateFormat(
                                CIntranetUtils::IsDateTime($arEntry['DATE_ACTIVE_TO']) ? 'FULL' : 'SHORT', $arParams['SITE_ID'], true
                            )),
                            $arEntry['DATE_ACTIVE_TO']
                        );

                        $arEntry['TYPE'] = 'PERSONAL';
                    }

                    if (!is_array($arParams['TYPES']) || count($arParams['TYPES'])<=0 || in_array($arEntry['TYPE'], $arParams['TYPES']))
                    {

                        if (
                            $arParams['SHORT_EVENTS'] == 'N'
                            && $arEntry['DATE_ACTIVE_TO'] > $arEntry['DATE_ACTIVE_FROM']
                            && date('Y-m-d', $arEntry['DATE_ACTIVE_FROM']) == date('Y-m-d', $arEntry['DATE_ACTIVE_TO'])
                        )
                            unset($arUsers[$arUser['ID']]['DATA'][$key]);
                        else
                            $arUsers[$arUser['ID']]['DATA'][$key] = $arEntry;
                    }
                    else
                        unset($arUsers[$arUser['ID']]['DATA'][$key]);
                }

                if ($arParams['USERS_ALL'] == 'N' && count($arUsers[$arUser['ID']]['DATA']) <= 0)
                    unset($arUsers[$arUser['ID']]);
                elseif(is_array($arUsers[$arUser['ID']]['DATA']))
                    $arUsers[$arUser['ID']]['DATA'] = array_values($arUsers[$arUser['ID']]['DATA']);

            }

            $arResult['USERS'] = $arUsers;
        }
    }

    $APPLICATION->RestartBuffer();
    Header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);
    if ($arResult['ERROR_CODE'] != '')
        echo 'alert(\''.CUtil::JSEscape($arResult['ERROR_CODE']).'\')';
    else
        echo $arParams['CALLBACK'].'('.CUtil::PhpToJsObject(array_values($arResult['USERS'])).', '.$arParams['CURRENT_DATA_ID'].')';
    die();
}
/////////////////////////////////////////
// TODO: make extranet checks here too //
/////////////////////////////////////////
elseif ($arParams['AJAX_CALL'] == 'INFO')
{
    if (!check_bitrix_sessid())
        return;

    $arParams['ID'] = intval($arParams['ID']);
    $arParams['TYPE'] = intval($arParams['TYPE']);
    $arParams['TYPE'] = $arParams['TYPE'] == 2 ? 2 : 1;

    $arParams['DETAIL_URL'] = COption::GetOptionString('intranet', 'search_user_url', '/user/#ID#/', $arParams['SITE_ID']);

    $bExtranetInstalled = CModule::IncludeModule('extranet');
    $bExtranetSite = $bExtranetInstalled && CExtranet::IsExtranetSite($arParams['SITE_ID']);

    if ($bExtranetInstalled && !$bExtranetSite && !CExtranet::IsIntranetUser($arParams['SITE_ID']))
        die();

    $calendar2 = false;
    if ($arParams['TYPE'] == 2)
    {
        $calendar2 = COption::GetOptionString("intranet", "calendar_2", "N") == "Y" && CModule::IncludeModule('calendar');
    }

    if ($arParams['TYPE'] == 1 || !$calendar2)
    {
        if ($arParams['IBLOCK_ID'] <= 0)
            $arParams['IBLOCK_ID'] = COption::GetOptionInt('intranet', $arParams['TYPE'] == 1 ? 'iblock_absence' : 'iblock_calendar');

        $arSelect = array('ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', 'NAME', 'PREVIEW_TEXT', 'DETAIL_TEXT');

        if ($arParams['TYPE'] == 1)
        {
            $arSelect = array_merge($arSelect, array('PROPERTY_USER', 'PROPERTY_FINISH_STATE', 'PROPERTY_STATE', 'PROPERTY_ABSENCE_TYPE', "PROPERTY_SALTO_APPROVED", "PROPERTY_SALTO_TYPE", "DETAIL_TEXT"));
        }
        else
        {
            $arSelect = array_merge($arSelect, array('PROPERTY_PERIOD_TYPE', 'PROPERTY_PERIOD_COUNT', 'PROPERTY_EVENT_LENGTH', 'PROPERTY_PERIOD_ADDITIONAL'));
        }

        $dbRes = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arParams['ID']), false, false, $arSelect);
        $arResult['ENTRY'] = $dbRes->Fetch();

        $arResult['ENTRY']['DATE_ACTIVE_FROM'] = MakeTimeStamp($arResult['ENTRY']['DATE_ACTIVE_FROM']);
        $arResult['ENTRY']['DATE_ACTIVE_TO'] = MakeTimeStamp($arResult['ENTRY']['DATE_ACTIVE_TO']);
    }
    else
    {
        $arEvent = CCalendarEvent::GetById($arParams['ID']);

        $arResult['ENTRY'] = array(
            'ID' => $arEvent['ID'],
            'NAME' => $arEvent['NAME'],
            'PREVIEW_TEXT' => '',
            'DETAIL_TEXT' => $arEvent['DESCRIPTION'],
            'DATE_ACTIVE_FROM' => $arEvent['DT_FROM_TS'] + date('Z') - date('Z', $arEvent['DT_FROM_TS']),
            'DATE_ACTIVE_TO' => $arEvent['DT_TO_TS'] + date('Z') - date('Z', $arEvent['DT_TO_TS']),
            'USER' => $arEvent['CREATED_BY'],
            'PROPERTY_PERIOD_TYPE_VALUE' => 'NONE', // check!
        );

        if ($arEvent['RRULE'])
        {
            $arRRule = array();
            $arRRuleStr = explode(';', $arEvent['RRULE']);
            foreach ($arRRuleStr as $str)
            {
                list($param, $value) = explode('=', $str);
                $arRRule[$param] = $value;
            }

            $arResult['ENTRY']['PROPERTY_PERIOD_TYPE_VALUE'] = $arRRule['FREQ'];
        }
    }

    if ($arResult['ENTRY'])
    {
        $arResult['ENTRY']['DATE_ACTIVE_FROM'] = FormatDate(
            $DB->DateFormatToPhp(CSite::GetDateFormat(
                CIntranetUtils::IsDateTime($arResult['ENTRY']['DATE_ACTIVE_FROM']) ? 'FULL' : 'SHORT', $arParams['SITE_ID'], true
            )),
            $arResult['ENTRY']['DATE_ACTIVE_FROM']
        );
        $arResult['ENTRY']['DATE_ACTIVE_TO'] = FormatDate(
            $DB->DateFormatToPhp(CSite::GetDateFormat(
                CIntranetUtils::IsDateTime($arResult['ENTRY']['DATE_ACTIVE_TO']) ? 'FULL' : 'SHORT', $arParams['SITE_ID'], true
            )),
            $arResult['ENTRY']['DATE_ACTIVE_TO']
        );

        if ($arParams['TYPE'] == 1)
        {
            $arResult['ENTRY']['TYPE'] =
                is_array($arResult['ABSENCE_TYPES'][$arResult['ENTRY']['PROPERTY_ABSENCE_TYPE_ENUM_ID']])
                    ? $arResult['ABSENCE_TYPES'][$arResult['ENTRY']['PROPERTY_ABSENCE_TYPE_ENUM_ID']]['XML_ID']
                    : '';

            $dbRes = CUser::GetByID($arResult['ENTRY']['PROPERTY_USER_VALUE']);
        }
        else
        {
            $arResult['ENTRY']['TYPE'] = 'PERSONAL';

            if (!$arResult['ENTRY']['USER'])
            {
                $dbRes = CIBlockSection::GetByID($arResult['ENTRY']['IBLOCK_SECTION_ID']);
                $arSection = $dbRes->Fetch();
                $dbRes = CUser::GetByID($arSection['CREATED_BY']);
            }
            else
            {
                $dbRes = CUser::GetByID($arResult['ENTRY']['USER']);
            }
        }

        if ($arUser = $dbRes->Fetch())
        {
            if (!is_array($arUser['UF_DEPARTMENT']))
                $arUser['UF_DEPARTMENT'] = array();

            if (!$bExtranetSite && count($arUser["UF_DEPARTMENT"]) <= 0)
                die();

            if ($bExtranetSite)
            {
                $arUsersInMyGroupsID = CExtranet::GetMyGroupsUsers($arParams['SITE_ID']);
                $arPublicUsersID = CExtranet::GetPublicUsers();
            }

            if ($bExtranetSite && !in_array($arUser["ID"], $arUsersInMyGroupsID) && !in_array($arUser["ID"], $arPublicUsersID) && $arUser["ID"] != $GLOBALS["USER"]->GetID())
                die();

            if (!$arUser['PERSONAL_PHOTO'])
            {
                switch ($arUser['PERSONAL_GENDER'])
                {
                    case "M":
                        $suffix = "male";
                        break;
                    case "F":
                        $suffix = "female";
                        break;
                    default:
                        $suffix = "unknown";
                }
                $arUser['PERSONAL_PHOTO'] = COption::GetOptionInt("socialnetwork", "default_user_picture_".$suffix, false, $arParams['SITE_ID']);
            }

            $arResult['USER'] = array(
                'ID' => $arUser['ID'],
                'LOGIN' => $arUser['LOGIN'],
                'NAME' => $arUser['NAME'],
                'LAST_NAME' => $arUser['LAST_NAME'],
                'SECOND_NAME' => $arUser['SECOND_NAME'],
                'PERSONAL_PROFESSION' => $arUser['PERSONAL_PROFESSION'],
                'PERSONAL_PHOTO' => $arUser['PERSONAL_PHOTO'],
                'WORK_POSITION' => $arUser['WORK_POSITION'],
                'UF_DEPARTMENT' => $arUser['UF_DEPARTMENT'],
                'DETAIL_URL' => str_replace(array('#ID#', '#USER_ID#'), $arUser['ID'], $arParams['DETAIL_URL'])
            );

            if ($arResult['USER']['PERSONAL_PHOTO'])
            {
                $arImage = CIntranetUtils::InitImage($arResult['USER']['PERSONAL_PHOTO'], 100);
                $arResult['USER']['PERSONAL_PHOTO'] = $arImage['IMG'];
            }

            if (is_array($arResult['USER']['UF_DEPARTMENT']) && count($arResult['USER']['UF_DEPARTMENT']) > 0)
            {
                $dbRes = CIBlockSection::GetList(array('SORT' => 'ASC'), array('ID' => $arResult['USER']['UF_DEPARTMENT']));
                $arResult['USER']['UF_DEPARTMENT'] = array();
                while ($arSect = $dbRes->Fetch())
                {
                    $arResult['USER']['UF_DEPARTMENT'][] = array('ID' => $arSect['ID'], 'NAME' => $arSect['NAME']);
                }
            }
// BML
            $arResult['ENTRY']['CANDELETE'] = CanEdit($arResult['ENTRY']['ID'], $USER->GetID(), "delete");
            $arResult['ENTRY']['CANOPENEDITFORM'] = CanEdit($arResult['ENTRY']['ID'], $USER->GetID(), "openeditform");
            $APPLICATION->RestartBuffer();
            Header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);

            echo CUtil::PhpToJsObject(array('USER' => $arResult['USER'], 'ENTRY' => $arResult['ENTRY']));
        }
    }

    die();
}

$arParams['bAdmin'] = CIBlockRights::UserHasRightTo($arParams['IBLOCK_ID'], $arParams['IBLOCK_ID'], 'element_edit');

$arUserFields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', 0, LANGUAGE_ID);
$arResult['UF_DEPARTMENT_field'] = $arUserFields['UF_DEPARTMENT'];
$arResult['UF_DEPARTMENT_field']['FIELD_NAME'] = 'department';
$arResult['UF_DEPARTMENT_field']['MULTIPLE'] = 'N';
$arResult['UF_DEPARTMENT_field']['SETTINGS']['LIST_HEIGHT'] = 1;

$APPLICATION->SetAdditionalCSS('/bitrix/themes/.default/pubstyles.css');
$APPLICATION->AddHeadScript('/bitrix/js/main/utils.js');
CJSCore::Init(array('window', 'ajax', 'date'));

if ($arParams['bAdmin'])
    CJSCore::Init(array('popup'));

$this->IncludeComponentTemplate();
?>