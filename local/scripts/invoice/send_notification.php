<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 10.12.15
 * Time: 11:02
 */

/* Необходимо создать таблицу
CREATE TABLE `dh_checked_invoices` (
  `INVOICE_ID` int(11) NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  PRIMARY KEY (`INVOICE_ID`,`DATE_CREATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

Создать тип почтового события DH_CHECKED_INVOICES
Создать почтовый шаблон с единственной переменной в теле #CONTENT#

Добавить скрипт в крон
0 9-18 * * 1-5 php /home/bitrix/www/local/scripts/invoice/send_notification.php
*/


define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
set_time_limit(0);
$_SERVER["DOCUMENT_ROOT"] = (!$_SERVER["DOCUMENT_ROOT"]) ? "/home/bitrix/www" : $_SERVER["DOCUMENT_ROOT"];
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/local/scripts/invoice/log.log");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$sql="SELECT
	b_sale_order.ID,
	b_sale_order.STATUS_ID,
	b_sale_order.PAYED,
	b_sale_order.CANCELED,
	b_crm_quote.ID as P_ID,
	b_sale_order.PRICE,
	b_sale_order.CURRENCY,
	b_sale_order.DATE_BILL,
	b_crm_contact.SECOND_NAME as CONTACT_SECOND_NAME,
	b_crm_contact.`NAME` as CONTACT_NAME,
	b_crm_contact.LAST_NAME as CONTACT_LAST_NAME,
	m1.`VALUE`,
	m1.ENTITY_ID,
	m1.TYPE_ID,
	b_user.LAST_NAME,
	b_user.`NAME`,
	b_user.SECOND_NAME,
	b_sale_order.DATE_BILL
FROM b_sale_order INNER JOIN b_uts_order ON b_sale_order.ID = b_uts_order.VALUE_ID
	 INNER JOIN b_crm_quote ON b_uts_order.UF_QUOTE_ID = b_crm_quote.ID
	 LEFT JOIN b_crm_contact ON b_uts_order.UF_CONTACT_ID = b_crm_contact.ID
	 LEFT JOIN b_crm_field_multi as m1 ON m1.ENTITY_ID = 'CONTACT' and b_uts_order.UF_CONTACT_ID = m1.ELEMENT_ID
	 LEFT JOIN b_user on b_user.id=b_sale_order.RESPONSIBLE_ID
WHERE b_crm_quote.ASSIGNED_BY_ID=1215 AND
    (b_sale_order.STATUS_ID='P' or (b_sale_order.PRICE<=1 AND b_sale_order.STATUS_ID='A'))
    AND b_sale_order.ID NOT IN (SELECT INVOICE_ID FROM dh_checked_invoices)
ORDER BY b_sale_order.ID";

$res=$DB->Query($sql);

$invoices=[];

while($row=$res->Fetch())
{
    if($invoices[$row["ID"]])
    {
        if($invoices[$row["ID"]][$row["TYPE_ID"]])
            $invoices[$row["ID"]][$row["TYPE_ID"]].=", ".$row["VALUE"];
        else
            $invoices[$row["ID"]][$row["TYPE_ID"]]=$row["VALUE"];
    }
    else
    {
        $invoices[$row["ID"]]=
            [
                "P_ID"=>$row["P_ID"],
                "PRICE"=>intval($row["PRICE"]),
                "CONTACT_LAST_NAME"=>$row["CONTACT_LAST_NAME"],
                "CONTACT_NAME"=>$row["CONTACT_NAME"]." ".$row["CONTACT_SECOND_NAME"],
                "RESPONSIBLE"=>$row["LAST_NAME"]." ".$row["NAME"]." ".$row["SECOND_NAME"],
                "DATE"=>$DB->FormatDate($row["DATE_BILL"],"YYYY-MM-DD HH:MI:SS","DD.MM.YYYY"),
                "DATE_BILL"=>$row["DATE_BILL"],
                $row["TYPE_ID"]=>$row["VALUE"]
            ];
    }
}

$output="";

if(count($invoices)>=1)
{
    $output="<table border='1'>
    <tr>
        <th>№</th>
        <th>Фамилия</th>
        <th>Имя Отчество</th>
        <th>Email</th>
        <th>Телефон</th>
        <th>№ предложения</th>
        <th>Сумма</th>
        <th>Менеджер</th>
        <th>Дата</th>
</tr>
";

    foreach($invoices as $i_id=>$i_data)
    {
        $output.="
        <tr>
            <td>{$i_id}</td>
            <td>{$i_data["CONTACT_LAST_NAME"]}&nbsp;</td>
            <td>{$i_data["CONTACT_NAME"]}&nbsp;</td>
            <td>{$i_data["EMAIL"]}&nbsp;</td>
            <td>{$i_data["PHONE"]}&nbsp;</td>
            <td>{$i_data["P_ID"]}</td>
            <td>{$i_data["PRICE"]}</td>
            <td>{$i_data["RESPONSIBLE"]}</td>
            <td>{$i_data["DATE"]}</td>
        </tr>
        ";

        $DB->Query("insert into dh_checked_invoices values($i_id,now())");

    }

    $output.="</table>";

    CEvent::SendImmediate("DH_CHECKED_INVOICES","s1",["CONTENT"=>$output],"N");
}




