<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 12.10.15
 * Time: 16:46
 */
$_SERVER["DOCUMENT_ROOT"] = (!$_SERVER["DOCUMENT_ROOT"]) ? "/home/bitrix/www" : $_SERVER["DOCUMENT_ROOT"];
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/local/scripts/28594/importUsersFromSP.log");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require("SoapClientAuth.php");

echo "<pre>";

$i=0;

$soapClient = new SoapClientAuth('http://portal.synergy.local/_vti_bin/userprofileservice.asmx?wsdl',
    array(
        'login' => '-',
        'password' => '-'
    ));

ob_flush();
flush();

$users=new CUser();

$rsUsers=$users->GetList(($by="login"), ($order="asc"),array("ACTIVE"=>"Y"),array("SELECT"=>array("UF_*")));
while($arUser=$rsUsers->GetNext())
{

    // UF_AREA : "Сокол"=>77971, "Семеновская"=>77972

    try
    {
        $result=$soapClient->GetUserProfileByName(array("AccountName"=>$arUser["LOGIN"]));
        echo $arUser["LOGIN"]."-".$arUser["PERSONAL_MOBILE"]."-".$arUser["UF_AREA"]."-".$arUser["UF_CABINET"]."-".$arUser["WORK_PHONE"]."<br>";
        $arUpdate=[];
        $arFields=[];
        foreach($result->GetUserProfileByNameResult->PropertyData as $key=>$value)
        {

            $field_name=trim($value->Name);
            $field_value=trim($value->Values->ValueData->Value);

            if($field_name=="CellPhone" && $field_value!="" && trim($arUser["PERSONAL_MOBILE"])=="")
            {
                $arFields[$field_name]=$field_value;
                $arUpdate["PERSONAL_MOBILE"]=$field_value;
            }

            /*if($field_name=="WorkPhone" && $field_value!="" && trim($arUser["WORK_PHONE"])=="")
            {
                $arFields[$field_name]=$field_value;
                $arUpdate["WORK_PHONE"]=$field_value;
            }*/

            if($field_name=="Office" && $field_value!="" && trim($arUser["UF_AREA"])=="" && trim($arUser["UF_CABINET"])=="")
            {
                $arFields[$field_name]=$field_value;
                $field_value=str_replace(array("кабинет","каб."),"",$field_value);

                $arArea=preg_split("/,/",$field_value,2);
                if(trim($arArea[0])=="Сокол")
                    $arUpdate["UF_AREA"]=77971;
                elseif(trim($arArea[0])=="Семеновская")
                    $arUpdate["UF_AREA"]=77972;

                if(trim($arArea[1])!="" && $arUpdate["UF_AREA"])
                    $arUpdate["UF_CABINET"]=trim($arArea[1]);
                elseif(!$arUpdate["UF_AREA"])
                {
                    $arUpdate["UF_CABINET"]=trim($arFields[$field_name]);
                }
            }

        }

        if(count($arUpdate)>0)
        {
            AddMessage2Log($arUser["LOGIN"]."-".$arUser["PERSONAL_MOBILE"]."-".$arUser["UF_AREA"]."-".$arUser["UF_CABINET"]."-".$arUser["WORK_PHONE"]);
            AddMessage2Log($arFields);
            AddMessage2Log($arUpdate);
            $users->Update($arUser["ID"], $arUpdate);
            AddMessage2Log($users->LAST_ERROR);
        }

    }
    catch (Exception $e)
    {
        //echo $e->getCode(), "\n";
    }

    ob_flush();
    flush();




}

echo "</pre>";




