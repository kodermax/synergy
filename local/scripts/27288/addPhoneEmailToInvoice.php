<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 05.10.15
 * Time: 14:39
 */
$_SERVER["DOCUMENT_ROOT"] = (!$_SERVER["DOCUMENT_ROOT"]) ? "/home/bitrix/www" : $_SERVER["DOCUMENT_ROOT"];
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/local/scripts/27288/addPhoneEmailToInvoice.log");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$res = $DB->Query("SELECT VALUE_ID, UF_CONTACT_ID FROM b_uts_order WHERE UF_CONTACT_ID IS NOT NULL AND UF_CONTACT_ID!=0");
while($row = $res->Fetch())
{
    AddMessage2Log($row);
    $resC = $DB->Query("select `VALUE`,`TYPE_ID` from b_crm_field_multi where ELEMENT_ID=".$row["UF_CONTACT_ID"]." AND (TYPE_ID='PHONE' OR TYPE_ID='EMAIL') AND ENTITY_ID='CONTACT'");
    $contacts = array();
    while($rowC = $resC->Fetch())
    {
        $contacts[$rowC["TYPE_ID"]][]=$rowC["VALUE"];
    }

    $arFields=array();

    $arFields["UF_CRM_1444048570"]="'".$DB->ForSql(join(", ",array_unique($contacts["PHONE"])))."'"; //телефон
    $arFields["UF_CRM_1444048582"]="'".$DB->ForSql(join(", ",array_unique($contacts["EMAIL"])))."'"; //email

    AddMessage2Log($arFields);

    $DB->Update("b_uts_order",$arFields,"WHERE VALUE_ID=".$row["VALUE_ID"]);

}

echo "End";
