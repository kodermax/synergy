<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 22.09.15
 * Time: 10:25
 */
$_SERVER["DOCUMENT_ROOT"] = (!$_SERVER["DOCUMENT_ROOT"]) ? "/home/bitrix/www" : $_SERVER["DOCUMENT_ROOT"];
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/local/scripts/16965/delDoubles.log");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');
CModule::IncludeModule('crm');

$objLead = new CCrmLead;

if ($USER->IsAdmin()):

    $arOrder = Array("DATE_CREATE" => "asc");
    $arFilter = Array(
        "%STATUS_CONVERTED" => "N",
        ">=DATE_CREATE" => "29.06.2015",
        "<DATE_CREATE" => "30.06.2015",
        "ASSIGNED_BY_ID" => 37,
        "!STATUS_ID" => Array(
            "0" => "JUNK",
            "1" => "CONVERTED",
        ),
    );

    $arSelect = Array("ID", "DATE_CREATE", "DATE_MODIFY", "TITLE", "NAME", "LAST_NAME", "PHONE_WORK", "EMAIL_WORK", "STATUS_ID", "ASSIGNED_BY_ID", "ACTIVE");

    $rs = $objLead->GetList($arOrder, $arFilter, $arSelect);

    $i = 0;

    while ($ar = $rs->GetNext())
    {
        $i++;
        AddMessage2Log(print_r($ar, TRUE));
        $objLead->Delete($ar["ID"]);
    }
    echo "Found: " . $i . " records";
else:
    echo "Denied";
endif;
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>
