<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 22.09.15
 * Time: 14:53
 */
$_SERVER["DOCUMENT_ROOT"] = (!$_SERVER["DOCUMENT_ROOT"]) ? "/home/bitrix/www" : $_SERVER["DOCUMENT_ROOT"];
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/local/scripts/17556/addDeal.log");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');
CModule::IncludeModule('crm');

if ($USER->IsAdmin()):

    $assigned_by = 429;

    $arOrder = Array("DATE_MODIFY" => "asc");
    $arFilter = Array(
        ">=DATE_MODIFY" => "01.01.2015",
        //"<DATE_MODIFY" => "15.07.2015",
        "ASSIGNED_BY_ID" => $assigned_by,
        //"ID" => 395686
    );

    $arSelect = Array("ID", "DATE_CREATE", "DATE_MODIFY", "COMPANY_TITLE", "NAME", "LAST_NAME", "ASSIGNED_BY_ID");

    $rs = CCrmContact::GetList($arOrder, $arFilter, $arSelect);

    $arContacts=array();


    while ($ar = $rs->GetNext())
    {
        $arContacts[]=$ar;
    }

    AddMessage2Log(print_r($arContacts, TRUE));

    echo "Found: " . count($arContacts) . " contacts<br>";

    $objDeal = new CCrmDeal();

    $noDeal=0;

    foreach ($arContacts as $i => $arC)
    {
        $arFilter = Array(
            "CONTACT_ID" => $arC["ID"],
        );
        $arSelect = Array("ID", "DATE_CREATE", "DATE_MODIFY", "COMPANY_ID", "TITLE", "STAGE_ID", "TYPE_ID");
        $rs = $objDeal->GetList($arOrder, $arFilter, $arSelect);
        if($ar = $rs->Fetch())
        {
            AddMessage2Log($arC["ID"]."\n".print_r($ar, TRUE));
        }
        else
        {
            AddMessage2Log($arC["ID"].": NONE\n");
            $noDeal++;
            $arFields = array(
                "TITLE" => "Поступление на ВПО/СПО",
                "COMPANY_ID"=> $arC["COMPANY_ID"],
                "CONTACT_ID" => $arC["ID"],
                "TYPE_ID" => "SALE",
                "ASSIGNED_BY_ID" => $assigned_by
            );
            if(!$objDeal->Add($arFields))
            {
                AddMessage2Log($objDeal->LAST_ERROR);
            }
        }
    }
    echo "Found: " . $noDeal . " contacts without deal<br>";

else:
    echo "Denied";
endif;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");