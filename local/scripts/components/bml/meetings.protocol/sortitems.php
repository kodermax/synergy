<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 16.10.2015
 * Time: 16:08
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if ((!CModule::IncludeModule("meeting") || (!CModule::IncludeModule("tasks"))))
{
    return;
}

//echo ("REQUEST: ".$_REQUEST['ID']);
$tmpID = $_REQUEST['ID'];
if (!empty($tmpID))
{
    $tmpQuery = CMeeting::GetList(array(),
        array('ID' => intval($tmpID)), false, false,
        array()
    );
    $Meeting = $tmpQuery->fetch();
    if (!empty($Meeting)){
// Если мы здесь, то собрание найдено
// Выбираем все пункты повестки дня в массив $MeetingItems
        $MeetingItemsDB = CMeeting::GetItems(intval($tmpID));
        $MeetingItems = array();
        while ($tmpRes = $MeetingItemsDB->fetch()){
            $MeetingItems[] = $tmpRes;
        }
        $MeetingItems2Level = $MeetingItems; // Необходимо для поиска вложенных элементов (подпунктов)
// Анализируем все найденные пункты
        foreach ($MeetingItems as $tmpMeetingItem)
        {
            $tmpInstanceID = $tmpMeetingItem['ID'];
// Если $tmpMeetingItem['INSTANCE_PARENT_ID'] == 0 значит это пункт верхнего уровня
            if ($tmpMeetingItem['INSTANCE_PARENT_ID'] == 0)
            {
// Выбираем подпункты
                $j = 0;
                foreach ($MeetingItems2Level as $MeetingItem2Level)
                {

// Если $MeetingItem2Level['ID'] == $tmpInstanceID - значит это подпункт текущего пункта
                    if($MeetingItem2Level['INSTANCE_PARENT_ID'] == $tmpInstanceID){
                        $SubItems[$j] = $MeetingItem2Level; // Добавляем пункт в массив текущих подпунктов
// Теперь надо найти минимальные даты DeadLine
// Сначала берем дату из самого пункта
                        $SubItems[$j]['DL'] = $SubItems[$j]['DEADLINE'];
//                        echo ($SubItems[$j]['DL'] );
//                        echo ("<br>");
// Теперь пробежимся по связанным задачам (посмотрим, что там с датами)
                        $tmpTasks = CMeetingItem::GetTasks($MeetingItem2Level['ITEM_ID']);
                        foreach ($tmpTasks as $tmpTask)
                        {
                            $Task = CTasks::GetByID($tmpTask, false, ['returnAsArray' => true]);
// Сначала посмотрим, не является ли задача периодической, для этого смотрим пользовательское поле UF_REPEAT
                            if (!empty($Task['UF_REPEAT']))
                            {
                                $tmpRepeatDB = CUserFieldEnum::GetList(array(), array("ID" => $Task['UF_REPEAT']));
// Проверяем, что задача периодическая
// Если периодическая, то устанавливаем заведомо более раннюю дату, чтобы они оказались сверху
                                $tmpRepeat = $tmpRepeatDB->fetch();
                                if ($tmpRepeat['XML_ID'] == 'UF_REPEAT_DAY')
                                {
                                    $SubItems[$j]['DL'] = "01.01.1990"; // Сначала будем показывать ежедневные
                                }
                                elseif ($tmpRepeat['XML_ID'] == 'UF_REPEAT_WEEK'){
                                    $SubItems[$j]['DL'] = "01.01.1991"; // Потом еженедельные
                                }
                                elseif ($tmpRepeat['XML_ID'] == 'UF_REPEAT_2WEEK'){
                                    $SubItems[$j]['DL'] = "01.01.1992"; // Потом с периодичностью раз в две недели
                                }
                                elseif ($tmpRepeat['XML_ID'] == 'UF_REPEAT_2WEEK'){
                                    $SubItems[$j]['DL'] = "01.01.1993"; // И наконец ежемесячные последними
                                }
                            }
// Теперь анализируем дату из задачи при условии, что она не пустая
                            if(!empty($Task['DEADLINE']))
                            {
                                if ((empty($SubItems[$j]['DL'])) || ($SubItems[$j]['DL'] == "00.00.0000"))
                                {
                                    $SubItems[$j]['DL'] = $Task['DEADLINE'];
                                } else
                                {
                                    if ($DB->CompareDates($SubItems[$j]['DL'], $Task['DEADLINE']) == 1)
                                    {
                                        $SubItems[$j]['DL'] = $Task['DEADLINE'];
                                    }
                                }
                            }

                        }
// Если после анализа всех задач так и не удалось установить дату, то перемещаем задачу в самый низ
                        if ((empty($SubItems[$j]['DL'])) || ($SubItems[$j]['DL'] == "00.00.0000")){
                            $SubItems[$j]['DL'] = "31.12.2099"; // До 2100 года эта система все равно не доживет...
                        }
// Конвертируем в TimeStamp, чтобы отсортировать массив
                        $SubItems[$j]['DLTS'] = MakeTimeStamp($SubItems[$j]['DL']);
                        $j++;
                    }
                }
// На этом этапе имеем все подпункты текущего пункта в массиве $SubItems
// Сортируем массив по TimeStamp
                usort($SubItems, 'compare');
                $k = 0;
                try
                {
                    $DB->StartTransaction();
                    foreach ($SubItems as $tmpItem)
                    {
                        CMeetingInstance::Update($tmpItem['ID'], array('SORT' => ($k + 1) * 100));
                        $k++;
                    }
                    $DB->Commit();
                } catch(Exception $ex){
                    $DB->Rollback();
                    $resError = "Ошибка при обновлении пунктов собрания (".$ex->getMessage().")";
                }
//                print_r($SubItems);
//echo("<br>");
//echo("<br>");
            }

            unset($SubItems); // Очищаем $SubItems перед следующим проходом
        }
    } else{
        $resError = "Собрание не найдено.";
    }
} else{
    $resError = "Не указан ID.";
}

if (empty($resError)){
    echo("Обработка успешно завершена.");
} else {
    echo("ОШИБКА: ".$resError);
}

function compare ($x, $y)
{
    if ($x['DLTS'] == $y['DLTS'])
        return 0;
    else if ($x['DLTS'] < $y ['DLTS'])
        return -1;
    else
        return 1;
}
