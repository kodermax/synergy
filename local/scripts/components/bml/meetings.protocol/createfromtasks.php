<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 19.10.2015
 * Time: 16:34
 */

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if ((!CModule::IncludeModule("meeting") || (!CModule::IncludeModule("tasks"))))
{
    return;
}


$MeetingID = $_REQUEST['ID'];
if (!empty($MeetingID)) {
    $tmpQuery = CMeeting::GetList(array(),
        array('ID' => intval($MeetingID)), false, false,
        array()
    );
    $Meeting = $tmpQuery->fetch();
    if (!empty($Meeting)) {
// Если мы здесь, то собрание найдено
        if(!empty($Meeting['GROUP_ID'])){
            if (CModule::IncludeModule('socialnetwork')) {
                $userRoleInGroup = CSocNetUserToGroup::GetUserRole($USER->GetID(), $Meeting['GROUP_ID']);
                if (!(($userRoleInGroup == SONET_ROLES_MODERATOR) || ($userRoleInGroup == SONET_ROLES_OWNER) || ($USER->isAdmin()))){
                    $resError = "ОШИБКА: Нет прав на выполнение...";
                    echo($resError);
                    return;
                }
            }
// Выбираем все задачи, которые:
// 1. Принадлежат той же группе, что и собрание
// 2. В зависимости от $_REQUEST['ft'] определяем надо ли брать только прошлые задачи или включать также будущие
// 3. У которых статус не равен CTasks::STATE_COMPLETED = 5
//    здесь возможно придется еще какие-то статусы убирать из отбора
// Сортируем по сроку выполнения по возрастанию, чтобы в конце оказались задачи из будущего
            if (isset($_REQUEST['ft']) && ($_REQUEST['ft'] == 0)) {
                $Tasks = CTasks::GetList(array('DEADLINE' => 'asc'), array('GROUP_ID' => $Meeting['GROUP_ID'],
                        '<=DEADLINE' => date('d.m.Y', MakeTimeStamp($Meeting['DATE_START'])) . "23:59",
                        '!REAL_STATUS' => '5'),
                    array());
            } else {
                $Tasks = CTasks::GetList(array('DEADLINE' => 'asc'), array('GROUP_ID' => $Meeting['GROUP_ID'],
                        '!REAL_STATUS' => '5'),
                    array());
            }
// Берем из параметров запроса тип формирования (линейный или двухуровневый)
// По умолчанию двухуровневый
            if (isset($_REQUEST['l2']) && ($_REQUEST['l2'] == 0)) {
                $use2level = false;
            } else {
                $use2level = true;
            }
            if (isset($_REQUEST['chdl']) && ($_REQUEST['chdl'] == 0)) {
                $changeDL = false;
            } else {
                $changeDL = true;
            }

// Обходим все задачи
// Это первый проход, чтобы выбрать все тэги
// Необходимо для того, чтобы можно было создавать пункты верхнего уровня в протоколе
// в заданной последовательности
            $arTags = array();
            $arTasks = array();
            while($Task=$Tasks->fetch()){
//                print_r($Task);
// Получаем тэги задачи (если требуется двухуровневая структура)
                if ($use2level == true) {
                    $TaskTagDB = CTaskTags::GetList(array(), array('TASK_ID' => $Task['ID']));
                    while ($TaskTag = $TaskTagDB->GetNext()) {
// Если в массиве $arTags нет элемента со значением, соответствующим тэгу задачи, то добавляем элемент в массив
                        if (!array_key_exists($TaskTag['NAME'], $arTags)) {
// ВАЖНО: Мы игнорируем тэги, которые не соответствуют формату "[NNN] Описание", где NNN -любое число
                            if (preg_match("/^\s*\[(.*)\]\s*/", $TaskTag['NAME'], $tmpMatches)) {
                                $arTags[$TaskTag['NAME']] = (int)$tmpMatches[1]; // Если соответствует, то добавляем в массив
                            }
                        }
// Если тэг соответствект формату "[NNN] Описание", то добавляем его к задаче
                        if (preg_match("/^\s*\[(.*)\]\s*/", $TaskTag['NAME'], $tmpMatches)) {
                            $Task['ALL_TAGS'][] = $TaskTag['NAME'];
                        }
                    }
                } else {
// Обходной путь чтобы не менять весь код, т.к. изначально все было заточено под двухуровневую структуру
// Поэтому создаем фейковый тэг (информация из него все равно никуда не попадет)
                    $Task['ALL_TAGS'][] = "TAG";
                }
// Получим пользовательское поле "Периодичность" и добавим к задаче, чтобы потом просто анализирвоать его
// Еще раз получаем задачу, т.к. GetList не возвращаети пользоввательские поля
                $tmpTask = CTasks::GetByID($Task['ID'], false, ['returnAsArray' => true]);
                if (!empty($tmpTask['UF_REPEAT'])) {
                    $tmpRepeatDB = CUserFieldEnum::GetList(array(), array("ID" => $tmpTask['UF_REPEAT']));
                    $tmpRepeat = $tmpRepeatDB->fetch();
                    $Task['UF_REPEAT_XML_ID'] = $tmpRepeat['XML_ID'];
                }
// Помещаем задачу в массив
                $arTasks[] = $Task;
//                echo("<br>");
//                print_r($Task);
//                echo("<br>");
            }
// Сортируем массив тэгов
            asort($arTags);
// Анализируем тэги: ищем созданные пункты верхнего уровня, название которых соответстует тэгам
// Если нашли, то присваиваем соответствующему эл-ту массива ID пункта
// Если не нашли, то создаем пункт и присваиваем ID
//            echo("<br>");
//            echo("<br>arTags до: ");
//            print_r($arTags);
            foreach($arTags as $key => $val){
                $strTag = preg_replace("/^\s*\[(.*)\]\s*/", "", $key);
                $tmpItemsDB = CMeeting::GetItems($MeetingID);
                $ff = 0; //Флаг FindFlag: 0 - если эл-т не найден, 1 - если найден
                while ($tmpItem = $tmpItemsDB->fetch()){
// Проверяем на соответствие только пункты верхнего уровня ($tmpItem['INSTANCE_PARENT_ID'] = 0)
                    if (($tmpItem['TITLE'] == $strTag) && ($tmpItem['INSTANCE_PARENT_ID'] == 0)){
                        $ff = 1;
                        $arTags[$key] = $tmpItem['ID'];
                        break;
                    }
                }
// Если не нашли пункт верхнего уровня, то надо его создать
// Только при условии $use2level == true
                if(($ff == 0) && ($use2level == true)){
// Сначала создаем Item
                    $it = new CMeetingItem;
                    $item_id = $it->Add(array('TITLE'=>$strTag));
// Теперь создаем Instance
                    $inst = new CMeetingInstance;
                    $arTags[$key] = $inst->Add(array('TITLE'=>$strTag, 'MEETING_ID' => $MeetingID, 'INSTANCE_PARENT_ID' => 0,
                                                                'INSTANCE_TYPE' => 'T', 'ORIGINAL_TYPE' => 'A', 'SORT' => $arTags[$key] * 100,
                                                                'DEADLINE' => "00.00.0000", 'ITEM_ID' => $item_id));
                }
//                echo($strTag."<br>");
            }
//            echo("<br>arTags: ");
//            print_r($arTags);
// Переходим к созданию основных пунктов повестки
            foreach($arTasks as $Task){
                $UpdateDeadLine = false; // Если произойдет какая-либо ошибка - дедлайн задачи не будет обновлен
// На основании даты задачи и периодичности решаем, стоит ли добавлять задачу к повестке
                $mDate = MakeTimeStamp($Meeting['DATE_START']);
                $tDate = MakeTimeStamp($Task['DEADLINE']);
// В дополнительные переменные обрезаем время у даты собрания и деты задачи и преобразуем их в timestamp
// Необходимо для принятия решения об установке даты в Instance и обновления даты задачи
                $mDate_notime = MakeTimeStamp($DB->FormatDate($Meeting['DATE_START'], CSite::GetDateFormat("FULL"), 'DD.MM.YYYY'), "DD.MM.YYYY");
                $tDate_notime = MakeTimeStamp($DB->FormatDate($Task['DEADLINE'], CSite::GetDateFormat("FULL"), 'DD.MM.YYYY'), "DD.MM.YYYY");

// Разница между датой собрания и deadline текущей задачи (выражено в днях)
                $DateDiff = ($mDate - $tDate) / (3600 * 24);
//                echo("<br>DateDiff: ".$DateDiff."  ---   XML_ID: ".$Task['UF_REPEAT_XML_ID']);

                $AddFlag = 0;
// 0. Если дата задачи равна дате собрания - добавляем ее в повестку, независимо от периодичности
//    Это необходимо для корректного переформирования повестки
// Было так:
//                if ($mDate == $tDate){
// Стало так:
                if ((date('d.m.Y', $mDate)) == (date('d.m.Y', $tDate))){
                    $AddFlag = 1;
                }
// 1. Если задача непериодическая или имеет периодичность ежедневно - значит добавляем ее к протоколу
// 1.1. Если задача еженедельная то тоже всегда попадает (сделал по просьбе В. Полякова)
                if ((empty($Task['UF_REPEAT_XML_ID'])) || ($Task['UF_REPEAT_XML_ID'] == 'UF_REPEAT_NO_REPEAT') ||
                    ($Task['UF_REPEAT_XML_ID'] == 'UF_REPEAT_DAY') || ($Task['UF_REPEAT_XML_ID'] == 'UF_REPEAT_WEEK')){
                    $AddFlag = 1;
                }
// 2. Если задача еженедельная, то добавляем ее только в том случае, если с момента ее DEADLINE до даты собрания прошло более 6 дней
//                elseif ($Task['UF_REPEAT_XML_ID'] == 'UF_REPEAT_WEEK') {
//                    if ($DateDiff > 6)
//                        $AddFlag = 1;
//                }
// 3. Если задача раз в две неделия, то добавляем ее только в том случае, если с момента ее DEADLINE до даты собрания прошло более 13 дней
                elseif ($Task['UF_REPEAT_XML_ID'] == 'UF_REPEAT_2WEEK'){
                    if ($DateDiff > 13)
                        $AddFlag = 1;
                }
// 3. Если задача ежемесячная, то добавляем ее только в том случае, если с момента ее DEADLINE до даты собрания прошло более 27 дней
                elseif ($Task['UF_REPEAT_XML_ID'] == 'UF_REPEAT_MONTH'){
                    if ($DateDiff > 27)
                        $AddFlag = 1;
                }
                $tmpSort = 10000;
                if ($AddFlag == 1) {
                    foreach ($Task['ALL_TAGS'] as $strTag) {
//Проверим наличие в повестке (в данной группе) пункта, к которому уже привязана текущая задача
// Только в случае формирования двухуровневой структуры
                        if ($use2level == true) {
                            $tmpInstDB = CMeetingInstance::GetList(array(), array('MEETING_ID' => $MeetingID, 'INSTANCE_PARENT_ID' => $arTags[$strTag], 'TASK_ID' => $Task['ID']),
                                false, false, array());
// в случае формирования линейной структуры просто проверим, что задача уже есть в собрании
                        } else {
                            $tmpInstDB = CMeetingInstance::GetList(array(), array('MEETING_ID' => $MeetingID, 'TASK_ID' => $Task['ID']),
                                false, false, array());
                        }
                        $tmpInst = $tmpInstDB->fetch();
// Если нашли, то надо обновить соответствующий Instance (ну и Item соответственно)
                        if ($tmpInst){
// Определяем какую дату ставить у Instance:
// Если дата задачи меньше даты собрания - значит дату собрания
// Если дата задачи больше даты собрания (задача из будущего) - значит дату задачи

                            if ($tDate_notime < $mDate_notime) {
                                $InstDateTime = $Meeting['DATE_START'];
                            } else{
                                $InstDateTime = $Task['DEADLINE'];
                            }
                            $inst_id = CMeetingInstance::Update($tmpInst['ID'], array('TITLE' => $Task['TITLE'], 'DEADLINE' => $InstDateTime));
                            if ($inst_id) {
                                $item_id = CMeetingItem::Update($tmpInst['ITEM_ID'], array('TITLE' => $Task['TITLE']));
                                if ($item_id){
                                    CMeetingInstance::SetResponsible(array('INSTANCE_ID' => $inst_id, 'ITEM_ID' => $item_id, 'MEETING_ID' => $Meeting['ID']),
                                        array('USER_ID' => $Task['RESPONSIBLE_ID']), true);
                                } else
                                {
                                    $resError = "Ошибка обновления Item.";
                                    break;
                                }
                            } else{
                                $resError = "Ошибка обновления Instance.";
                                break;
                            }
                            $UpdateDeadLine = true; // Необходимо обновить дедлайн задачи
                        } else {
// Если не нашли, то создаем новый
// Сначала создаем Item
                            $it = new CMeetingItem;
                            $item_id = $it->Add(array('TITLE' => $Task['TITLE']));
                            if ($item_id) {
// Привязываем задачу к Item
                                $tsk = $it->AddTask($item_id, $Task['ID']);
// Теперь создаем Instance
                                $inst = new CMeetingInstance;
// Получим пользовательское поле "UF_TSK_M_ITEM_SORT"
// Еще раз получаем задачу, т.к. GetList не возвращаети пользоввательские поля
                                $tmpTask = CTasks::GetByID($Task['ID'], false, ['returnAsArray' => true]);
                                $TaskSort = $tmpTask['UF_TSK_M_ITEM_SORT'];
                                if (!$TaskSort){
                                    $TaskSort = $tmpSort;
                                    $tmpSort+=100;
                                }

// Определяем какую дату ставить у Instance:
// Если дата задачи меньше даты собрания - значит дату собрания
// Если дата задачи больше даты собрания (задача из будущего) - значит дату задачи
                                if ($tDate_notime < $mDate_notime) {
                                    $InstDateTime = $Meeting['DATE_START'];
                                } else{
                                    $InstDateTime = $Task['DEADLINE'];
                                }
// Добавляем
                                if ($use2level == true) {
                                    $inst_id = $inst->Add(array('TITLE' => $Task['TITLE'], 'MEETING_ID' => $MeetingID, 'INSTANCE_PARENT_ID' => $arTags[$strTag],
                                        'INSTANCE_TYPE' => 'T', 'ORIGINAL_TYPE' => 'A', 'SORT' => $TaskSort,
                                        'DEADLINE' => $InstDateTime, 'ITEM_ID' => $item_id, 'TASK_ID' => $Task['ID']));
                                } else {
                                    $inst_id = $inst->Add(array('TITLE' => $Task['TITLE'], 'MEETING_ID' => $MeetingID,
                                        'INSTANCE_TYPE' => 'T', 'ORIGINAL_TYPE' => 'A', 'SORT' => $TaskSort,
                                        'DEADLINE' => $InstDateTime, 'ITEM_ID' => $item_id, 'TASK_ID' => $Task['ID']));
                                }
                                if ($inst_id) {
// Добавляем ответственного
                                    CMeetingInstance::SetResponsible(array('INSTANCE_ID' => $inst_id, 'ITEM_ID' => $item_id, 'MEETING_ID' => $Meeting['ID']),
                                        array('USER_ID' => $Task['RESPONSIBLE_ID']), true);
//                                    echo("tmpC:  ".$tmpC." Resp: ".$Task['RESPONSIBLE_ID']);
//                                    print_r(CMeetingInstance::GetResponsible($inst_id));
//                                    echo("<br>");
                                } else
                                {
                                    $resError = "Ошибка создания Instance.";
                                    break;
                                }

                            } else {
                                $resError = "Ошибка создания Item.";
                                break;
                            }
                            $UpdateDeadLine = true; // Необходимо обновить дедлайн задачи
                        }
                    }
                    if ((!$resError) && ($changeDL)) {
// Меняем DeadLine задачи, устанавливая его равным дате собрания (включая время)
// Только в случае, если дата задачи раньше даты собрания
// Обновляем дату только в том случае, если дата задачи раньше или равна дате собрания
                        if (($tDate <= $mDate) && $UpdateDeadLine) {
// Срочный fix после апдейта битрикса
//                            $tmpTask = CTaskItem::getInstance($Task['ID'], $USER->GetId());
// Попробуем отловить глюк
                            try {
                                $taskuserid = 1;//$USER->GetId()
                                $tmpTask = CTaskItem::getInstance($Task['ID'], $taskuserid);
                                $tmpTask->update(array('DEADLINE' => $Meeting['DATE_START']));
                            } catch (Exception $e){
                                $allowedactions = $tmpTask->getAllowedActions(false);
                                $allowedactions_str = implode(" | ", $allowedactions);
                                StringToLog($fh, "Задача ID: ".$Task['ID'].". Права доступа: ".$allowedactions_str);
                                $fh= fopen('/home/bitrix/www/local/modules/intranet/tools/logs/createfromtask'.time().'.log', 'a');
                                StringToLog($fh, date('y.m.Y H:i')." - Исключение при обработке задачи: ID - ".$Task['ID']." Exception: ".$e->getMessage()."USER_ID = ".$taskuserid." | Текущий пользователь системы: ".$USER->GetLogin());
                                fclose($fh);
                            }
                        }
//**********************************************************************
                    }
                }
            }
            if (!$resError){
//                require_once "sortitems.php";
                echo("Обработка успешно завершена!");
            } else {
                echo("ОШИБКА: ".$resError);
            }
        } else{
            $resError = "Не указана привязка собрания к группе. Обработка прервана.";
            echo("ОШИБКА: ".$resError);
        }
    }
}
