<?php
/**
 * Created by PhpStorm.
 * User: MKarpychev
 * Date: 22.10.2015
 * Time: 11:18
 */
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
set_time_limit(0);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$user = new CUser;
$by = 'LAST_NAME';
$order = 'ASC';
$list = CUser::GetList($by, $order,['ACTIVE'=>'Y','!UF_DEPARTMENT' => false],['FIELDS'=>['ID','LOGIN','NAME','LAST_NAME']]);
while($row =  $list->GetNext()) {

    //print $row['LOGIN'].': '.$row['LAST_NAME'] . ' ' . $row['NAME'] . "<br/>";
    $user->Update($row['ID'], ['UF_DEPARTMENT' => []]);
}
print "done!";
?>