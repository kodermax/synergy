<?php

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . "/../../..");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
global $DBType;
$DBType = "mysql";
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$user = new CUser;
$list = CUser::GetList($by = 'ID', $order = 'ASC', [],['SELECT' => ['UF_1C_CODE'],'FIELDS'=>['ID']]);
while($row = $list->GetNext()){
    if (strlen($row['UF_1C_CODE'])) {
        $user->Update($row['ID'],['XML_ID' => $row['UF_1C_CODE']]);
    }
}
print "ok";