function callInfinity(callTo) {
  var userId = BX.userId;
  BX.showWait();
  $.get("/api/users/" + userId + "/getExtension", function (data) {
    var ext = data.result;
    if (!ext) {
      alert('Вы не можете звонить. У вас нет внутреннего номера.')
    }
    else {
      $.post("/api/infinity/call", {'phone': callTo, 'extension': ext}, function (data) {
      });
    }
    BX.closeWait();
  });
}