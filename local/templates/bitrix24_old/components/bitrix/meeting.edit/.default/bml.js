/**
 * Created by Admin on 12.10.2015.
 */
$(document).ready(function() {
    //jQuery(".bml-hover-div").css({'display':'none'});
    jQuery("#bml-print-ref").mouseenter(function () {
        //jQuery("#bml-hover-div").css({'display':'block'});
        jQuery("#select-format-hover-div").fadeIn(500);
        return false;
    });
    //jQuery("#bml-hover-div").mouseleave (function () {
    //    jQuery("#bml-hover-div").css({'display':'none'});
    //});
    jQuery("#bml-print-ref").mouseleave (function () {
        //jQuery("#bml-hover-div").css({'display':'none'});
        jQuery("#select-format-hover-div").fadeOut(500);
        return false;
    });
});

function SortItemsRefClick(prmID)
{
    var ajaxRes;
    var tmpRes;
    tmpRes = confirm("Обработка приведет к изменению порядка пунктов повестки собрания. Выполнить обработку?");
    if (tmpRes) {
        ajaxRes = $.ajax({
            type: "POST",
            url: "/local/scripts/components/bml/meetings.protocol/sortitems.php",

            data: "ID=" + prmID,
            success: function (prmData) {
                alert(ajaxRes.responseText);
                location.reload();
            }
        });
    }
    return false;
}

function FindTasksRefClick(prmID)
{
    var ajaxRes;
    var tmpRes;
    if ($('#level2-structure').prop('checked')){
        tmp2level = 1;
    } else {
        tmp2level = 0;
    }
    if ($('#futuretasks').prop('checked')){
        futuretasks = 1;
    } else {
        futuretasks = 0;
    }
    if ($('#changedl').prop('checked')){
        changedl = 1;
    } else {
        changedl = 0;
    }

    tmpRes = confirm("Обработка приведет к изменению контрольного срока задач, попавших в собрание. Выполнить обработку?");
    jQuery("#select-settings-hover-div").fadeIn(500);
    if (tmpRes) {
        ajaxRes = $.ajax({
            type: "POST",
            url: "/local/scripts/components/bml/meetings.protocol/createfromtasks.php",

            data: "ID=" + prmID + "&l2=" + tmp2level + "&ft=" + futuretasks + "&chdl=" + changedl,
            success: function (prmData) {
                alert(ajaxRes.responseText);
                location.reload();
            }
        });
    }
    $('#select-settings-hover-div').fadeOut(300);
    return false;
}
