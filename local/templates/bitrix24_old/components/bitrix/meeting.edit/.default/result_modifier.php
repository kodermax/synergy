<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21.10.2015
 * Time: 12:00
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (CModule::IncludeModule('socialnetwork')) {
    $userRoleInGroup = CSocNetUserToGroup::GetUserRole($USER->GetID(), $arResult['MEETING']['GROUP_ID']);
    if (($userRoleInGroup == SONET_ROLES_MODERATOR) || ($userRoleInGroup == SONET_ROLES_OWNER) || ($USER->isAdmin())){
        $arResult['CAN_CREATE_FROM_TASKS'] = true;
    }
}