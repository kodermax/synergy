<?

// Получим все пользовательские поля у которых название UF_DISK_FILE_DESCR
//$dbVar = CUserTypeEntity::GetList(array(), array('FIELD_NAME' => 'UF_DISK_FILE_DESCR'));
//while ($stor = $dbVar->fetch()){
//    $storages[] = $stor['ENTITY_ID'];
//}
//if (!empty($arParams["~GRID_ID"])) {
//// Проверяем, есть ли у нас пользовательское поле для данного стораджа
//    $matches = array();
//    $res = preg_match('#folder_list_(.*)#', $arParams["~GRID_ID"], $matches);
//    $storID = $matches[1];
//    if (!empty($storID)) {
//        if (in_array("DISK_FILE_".$storID, $storages)){
//            $arParams['~HEADERS'][]=array("id"=>'UF_DISK_FILE_'.$storID, "name"=>'Описание', "default"=>1, "sort"=>'UF_DISK_FILE_'.$storID);
//// Теперь вытаскиваем значение пользовательского поля для каждого файла
//            CModule::IncludeModule('disk');
//            foreach($arParams["~ROWS"] as $key=>$row) {
//                $fileID = $arParams["~ROWS"][$key]["data"]["ID"];
//                $file = \Bitrix\Disk\File::loadById($fileID);
//                if (!empty($file)) {
//                    $ufFields = \Bitrix\Disk\Driver::getInstance()->getUserFieldManager()->getFieldsForObject($file);
//                    $arParams["~ROWS"][$key]["columns"]["UF_DISK_FILE_".$storID] = $ufFields['UF_DISK_FILE_DESCR']['VALUE'];
////                    print_r($ufFields); echo "<br><br>";
//                }
////                echo $fileID."<br>";
////
//            }
//        }
//    }
//}

// Общий алгоритм

CModule::IncludeModule('disk');
foreach($arParams["~ROWS"] as $key=>$row) {
    $fileID = $arParams["~ROWS"][$key]["data"]["ID"];
    $file = \Bitrix\Disk\File::loadById($fileID);
    if (!empty($file)) {
        $ufFields = \Bitrix\Disk\Driver::getInstance()->getUserFieldManager()->getFieldsForObject($file);
// Далее смотрим каждое поле
        foreach($ufFields as $fName=>$Field){
// Анализируем ТОЛЬКО строковые поля
            if ($Field['USER_TYPE_ID'] == 'string') {
// Проверяем, добавлено ли данное поле в заголовок, если его там нет - то добавляем
                $tmpHeader = array("id" => $Field['FIELD_NAME'], "name" => $Field['LIST_COLUMN_LABEL'], "sort" => $Field['FIELD_NAME']);
//                print_r($tmpHeader); echo "<br><br>";
                if (!in_array($tmpHeader, $arParams['~HEADERS'])) {
                    $arParams['~HEADERS'][] = $tmpHeader;
                }
// Добавляем значение в массив ROWS
                $arParams["~ROWS"][$key]["columns"][$fName] = $ufFields[$fName]['VALUE'];
            }
        }
        $arParams["~ROWS"][$key]["columns"]["UF_DISK_FILE_"] = $Field['VALUE'];
//        print_r($ufFields); echo "<br><br>";
    }
}