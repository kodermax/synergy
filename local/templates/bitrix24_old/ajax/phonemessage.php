<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("im");

$objIMess = new CIMMessenger;


$callFrom = trim(intval($_REQUEST["callFrom"]));
$callTo = trim(intval($_REQUEST["callTo"]));


$arFilter = Array(
	"UF_PHONE_INNER" => Array($callFrom,$callTo)
);

$arSelect = Array(
	"FIELDS" => Array("ID", "NAME", "LAST_NAME"),
	"SELECT" => Array("UF_PHONE_INNER")
);

$objUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter, $arSelect);
while($arUser = $objUser->Fetch()){
	$arResult[$arUser["UF_PHONE_INNER"]] = $arUser;
}

$message .= "Вам входщий вызов от пользователя ";

if(strlen($arResult[$callFrom]["NAME"]) > 0){
	$message .= $arResult[$callFrom]["NAME"]." ".$arResult[$callFrom]["LAST_NAME"];
}else{
	$message .= "с телефонным номером ".$callFrom;
}


if($arResult[$callFrom]["ID"]){	$callFromMessage = $arResult[$callFrom]["ID"];}else{	$callFromMessage = 285;}


$arFieldsMessage = Array(
	"AUTHOR_ID" => $callFromMessage,
	"FROM_USER_ID" => $callFromMessage,
	"TO_USER_ID" => $arResult[$callTo]["ID"],
	"MESSAGE" => $message,
	"MESSAGE_TYPE" => "P",
);

$objMessage = $objIMess->Add($arFieldsMessage);




if($callFrom){
	$arLeads = SynergyLead::findLeadByPhone($callFrom);
	$arContacts = SynergyContact::findContactByPhone($callFrom);
}
?>


<div id="tables">

<?if($arContacts != "error"){?>

	<table align="center" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="5">
				<p>Контакты:</p>
			</td>
		</tr>
		<tr class="header">
			<td>ID</td>
			<td>Контакт</td>
			<td>Дата создания</td>
			<td>Ответственный</td>
			<td>Подразделение</td>
		</tr>
	<?foreach($arContacts as $arContact){
		$objUserParams = $USER->GetById($arContact["ASSIGNED_BY_ID"]);

		if($userParams = $objUserParams->Fetch()){
			$arAssigned[$arContact["ID"]] = $userParams;
			$objDepartments = CIBlockSection::GetNavChain(1, $userParams["UF_DEPARTMENT"][0]);
		}
		?>
		<tr>
			<td><?=$arContact["ID"]?></td>
			<td><a href="/crm/contact/show/<?=$arContact["ID"]?>/"><?=$arContact["LAST_NAME"]?> <?=$arContact["NAME"]?> <?=$arContact["SECOND_NAME"]?></a></td>
			<td><?=$arContact["DATE_CREATE"]?></td>
			<td><?=$arAssigned[$arContact["ID"]]["NAME"]?> <?=$arAssigned[$arContact["ID"]]["LAST_NAME"]?></td>
			<td>

			<?while($arDepartment = $objDepartments->ExtractFields()):
			?>
			<a target="_blank" href="/company/structure.php?structure_UF_DEPARTMENT=<?=$arDepartment["ID"]?>"><?=$arDepartment["NAME"]?></a><?if($arDepartment["ID"] != $userParams["UF_DEPARTMENT"][0]):?> /<?endif;?>
			<?endwhile;?>

			</td>
		</tr>
	<?}?>
	</table>

<?}?>





<?if($arLeads != "error"){?>

	<table align="center" border="0" cellspacing="0" cellpadding="5">

		<tr>
			<td colspan="6">
				<p>Лиды:</p>
			</td>
		</tr>
		<tr class="header">
			<td>ID</td>
			<td>Лид</td>
			<td>Дата создания</td>
			<td>Статус</td>
			<td>Ответственный</td>
			<td>Подразделение</td>
		</tr>	<?foreach($arLeads as $arLead){		$objUserParams = $USER->GetById($arLead["ASSIGNED_BY_ID"]);

		if($userParams = $objUserParams->Fetch()){
			$objDepartments = CIBlockSection::GetNavChain(1, $userParams["UF_DEPARTMENT"][0]);
			$arAssigned[$arLead["ID"]] = $userParams;
		}

		$leadStatus = $arLead["STATUS_ID"];

		$strSql = "SELECT CS.ID, CS.ENTITY_ID, CS.STATUS_ID, CS.NAME, CS.NAME_INIT, CS.SORT, CS.SYSTEM
            FROM
                b_crm_status CS
            WHERE STATUS_ID = '".$leadStatus."'	and ENTITY_ID = 'STATUS'";

		$res = $DB->Query($strSql);

		if($status = $res->Fetch()){
			$statusName = $status["NAME"];
		}

		?>		<tr>
			<td><?=$arLead["ID"]?></td>
			<td><a href="/crm/lead/show/<?=$arLead["ID"]?>/"><?=$arLead["TITLE"]?></a></td>
			<td><?=$arLead["DATE_CREATE"]?></td>
			<td><?=$statusName?></td>
			<td><?=$arAssigned[$arLead["ID"]]["NAME"]?> <?=$arAssigned[$arLead["ID"]]["LAST_NAME"]?></td>
			<td>
			<?while($arDepartment = $objDepartments->ExtractFields()):
			?>
			<a target="_blank" href="/company/structure.php?structure_UF_DEPARTMENT=<?=$arDepartment["ID"]?>"><?=$arDepartment["NAME"]?></a><?if($arDepartment["ID"] != $userParams["UF_DEPARTMENT"][0]):?> /<?endif;?>
			<?endwhile;?>
			</td>
		</tr>	<?}?>
	</table>
	<br /><br />
<?}?>


</div>





<style>
body {    height: 100%;
    text-align: center;}

body:before {
    height: 100%;
    content: '';
    display: inline-block;
    vertical-align: middle;
    margin-right: -0.25em; /* Adjusts for spacing */
}


#tables {	display: inline-block;
	vertical-align: middle;
	margin: auto;
}

table {	border-right:1px solid #ccc;
	border-top:1px solid #ccc;
	border-radius:4px;
	width:900px;
	margin-bottom:30px;
}

table td {	border-left:1px solid #ccc;
	border-bottom:1px solid #ccc;
	font-size:14px;
	padding:5px 15px;
	text-align:center;}

table tr.header td {	font-weight:bold;
	background: #e4e4e4;}
</style>