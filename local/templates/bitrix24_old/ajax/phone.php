<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

function removeSymbolPhone($phone){
	$phone = str_replace(" ","",$phone);
	$phone = str_replace("+","",$phone);
	$phone = str_replace("-","",$phone);
	$phone = str_replace("(","",$phone);
	$phone = str_replace(")","",$phone);
	$phone = intval($phone);
	return $phone;
}


$userId = $USER->GetId();
$objUser = $USER->GetById($userId);

if($arrUser = $objUser->Fetch()){
	$callFrom = $arrUser["UF_PHONE_INNER"];
}


$callFrom = removeSymbolPhone($callFrom);
$callTo = removeSymbolPhone($_POST["callTo"]);



if(isset($callFrom) && isset($callTo) && $callFrom != 0){

	$url = "http://pbx1.synergy.local:10080/call/make/?Extension=".$callFrom."&Number=".$callTo;

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	$response = curl_exec($ch);

	curl_close($ch);


	$arResponse = Array(
		"response" => true,
		"callFrom" => $callFrom,
		"callTo" => $callTo,
	);

}else{
	$arResponse = Array(
		"response" => false,
	);
}


echo json_encode($arResponse);

?>