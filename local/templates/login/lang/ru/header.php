<?
$MESS["BITRIX24_MOTTO"] = "Социальный интранет";
$MESS["BITRIX24_URL"] = "http://corp.bitrix24.ru";
$MESS["BITRIX24_TITLE"] = "Университет &laquo;Синергия&raquo;";
$MESS["BITRIX24_LANG_INTERFACE"] = "Язык";
$MESS["BITRIX24_LANG_RU"] = "Русский";
$MESS["BITRIX24_LANG_EN"] = "Английский";
$MESS["BITRIX24_LANG_DE"] = "Немецкий";
$MESS["BITRIX24_LANG_UA"] = "Украинский";
$MESS["BITRIX24_LANG_LA"] = "Испанский";
$MESS["BITRIX24_COPYRIGHT"] = "&copy; 1988-#CURRENT_YEAR# Университет &laquo;Синергия&raquo;, Корпоративный портал.";
$MESS["BITRIX24_COPYRIGHT_B24"] = "&copy; 1988-#CURRENT_YEAR# Университет &laquo;Синергия&raquo;, Корпоративный портал.";
?>