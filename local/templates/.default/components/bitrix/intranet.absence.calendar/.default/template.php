<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

$APPLICATION->SetPageProperty("BodyClass", "page-one-column");

if ($arParams['bAdmin']):
	$arAbsenceParams["MESS"] = array(
		"INTR_ABSENCE_TITLE" => GetMessage("INTR_ABSENCE_TITLE"),
		"INTR_ABSENCE_BUTTON" => GetMessage("INTR_ABSENCE_BUTTON"),
		"INTR_CLOSE_BUTTON" => GetMessage("INTR_CLOSE_BUTTON"),
		"INTR_LOADING" => GetMessage("INTR_LOADING"),
	);
	$arAbsenceParams["IBLOCK_ID"] = $arParams['IBLOCK_ID'];
endif;

$BGCOLORS = array(
//	'#f1e28f',
//	'#a0c4f1',
//	'#e3a5d8',
//	'#fe3737',
//	'#f2a584',
//	'#c9e079',
//	'#d3d3d3',
//	'#a9b8bf',
//	'#9ddbd3',
//
//	'#43d2d7',
//	'#43d795',
//	'#92d743',
//	'#d68085',
//	'#d680c1',
//	'#adaae7',
//	'#aad5e7',
//	'#aae7cb',
//	'#aee7aa',
//	'#d9eccc',
//	'#ece6cc'
	'#999',
	'#999',
	'#999',
	'#999',
	'#55fb55',
	'#999',
	'#999',
	'#999',
	'#999',
	'#999',
	'#999',

//	'#43d795',
//	'#92d743',
//	'#d68085',
//	'#d680c1',
//	'#adaae7',
//	'#aad5e7',
//	'#aae7cb',
//	'#aee7aa',
//	'#d9eccc',
//	'#ece6cc'
//
);
$c_id = 0;
$TYPE_BGCOLORS = array();
$TYPES = array();
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "CODE"=>"ABSENCE_TYPE"));
while($enum_fields = $property_enums->GetNext())
{
	$TYPES[] = "{NAME:'".$enum_fields['XML_ID']."',TITLE:'".$enum_fields["VALUE"]."'}";
	if (!isset($TYPE_BGCOLORS[$enum_fields['XML_ID']]))
	{
		$TYPE_BGCOLORS[] = "'".$enum_fields['XML_ID']."':'".$BGCOLORS[$c_id++]."'";

		if (!isset($BGCOLORS[$c_id]))
		{
			$c_id = 0;
		}
	}
}
$TYPES = join(',', $TYPES);
$TYPE_BGCOLORS = join(',', $TYPE_BGCOLORS);
?>
<script type="text/javascript">
function GetAbsenceDialog(abcenseID)
{
	<?
	$arAbsenceParams["MESS"]["INTR_ABSENCE_TITLE"] = GetMessage("INTR_ABSENCE_TITLE_EDIT");
	$arAbsenceParams["ABSENCE_ELEMENT_ID"] = "#ABSENCE_ID#";
	?>
	var dialog = "<?="BX.AbsenceCalendar.ShowForm(".CUtil::PhpToJSObject($arAbsenceParams).")"?>";
	return dialog.replace("#ABSENCE_ID#", abcenseID);
}
jsBXAC.Init(
	{
// BML Меняем обработчик...
// Было:		'LOADER': '/bitrix/components/bitrix/intranet.absence.calendar/ajax.php',
// Стало
		'LOADER': '/local/components/bml/intranet.absence.calendar/ajax.php',
// Дальше ничего не меняли
		'NAME_TEMPLATE': '<?echo CUtil::JSEscape($arParams['NAME_TEMPLATE'])?>',
		'SERVER_TIMEZONE_OFFSET': <?echo date('Z')?>,
		'FIRST_DAY': <?echo $arParams['FIRST_DAY']?>,
		'DAY_START': <?echo $arParams['DAY_START']?>,
		'DAY_FINISH': <?echo $arParams['DAY_FINISH']?>,
		'DAY_SHOW_NONWORK': <?echo $arParams['DAY_SHOW_NONWORK'] == 'Y' ? 'true' : 'false'?>,
		'DETAIL_URL_PERSONAL': '<?echo CUtil::JSEscape($arParams['DETAIL_URL_PERSONAL'])?>',
		'DETAIL_URL_DEPARTMENT': '<?echo CUtil::JSEscape($arParams['DETAIL_URL_DEPARTMENT'])?>',
		'CONTROLS': <?echo CUtil::PhpToJsObject($arResult['CONTROLS'])?>,
		'SITE_ID': '<?echo SITE_ID?>',
		'IBLOCK_ID': <?echo intval($arParams['IBLOCK_ID'])?>,
		'CALENDAR_IBLOCK_ID': <?echo intval($arParams['CALENDAR_IBLOCK_ID'])?>,
		'MONTHS': [<?for($i=1;$i<13;$i++){echo ($i==1 ? '' : ','),"'",CUtil::JSEscape(GetMessage('IAC_MONTH_'.$i)),"'";}?>],
		'MONTHS_R': [<?for($i=1;$i<13;$i++){echo ($i==1 ? '' : ','),"'",CUtil::JSEscape(GetMessage('IAC_MONTH_R_'.$i)),"'";}?>],
		'DAYS': [<?for($i=1;$i<8;$i++){echo ($i==1 ? '' : ','),"'",CUtil::JSEscape(GetMessage('IAC_DAY_'.$i)),"'";}?>],
		'DAYS_FULL': [<?for($i=1;$i<8;$i++){echo ($i==1 ? '' : ','),"'",CUtil::JSEscape(GetMessage('IAC_DAY_FULL_'.$i)),"'";}?>],
		'TYPE_BGCOLORS': {<?=$TYPE_BGCOLORS?>},
		'TYPES': [<?=$TYPES?>],
		'MESSAGES': {
			'IAC_MAIN_TITLE': '<?echo CUtil::JSEscape(GetMessage('INTR_IAC_MAIN_TITLE'))?>',
			'IAC_FILTER_TYPEFILTER': '<?echo CUtil::JSEscape(GetMessage('IAC_FILTER_TYPEFILTER'))?>',
			'IAC_FILTER_TYPEFILTER_ALL': '<?echo CUtil::JSEscape(GetMessage('IAC_FILTER_TYPEFILTER_ALL'))?>',
			'IAC_FILTER_SHOW_ALL': '<?echo CUtil::JSEscape(GetMessage('IAC_FILTER_SHOW_ALL'))?>',
			'IAC_FILTER_DEPARTMENT': '<?echo CUtil::JSEscape(GetMessage('IAC_FILTER_DEPARTMENT'))?>',
			'INTR_ABSC_TPL_FILTER_ON': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_FILTER_ON'))?>',
			'INTR_ABSC_TPL_FILTER_OFF': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_FILTER_OFF'))?>',
			'INTR_ABSC_TPL_PERSONAL_LINK_TITLE': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_PERSONAL_LINK_TITLE'))?>',
			'INTR_ABSC_TPL_WARNING_MONTH': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_WARNING_MONTH'))?>',
			'INTR_ABSC_TPL_REPEATING_EVENT': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_REPEATING_EVENT'))?>',
			'INTR_ABSC_TPL_REPEATING_EVENT_DAILY': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_REPEATING_EVENT_DAILY'))?>',
			'INTR_ABSC_TPL_REPEATING_EVENT_WEEKLY': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_REPEATING_EVENT_WEEKLY'))?>',
			'INTR_ABSC_TPL_REPEATING_EVENT_MONTHLY': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_REPEATING_EVENT_MONTHLY'))?>',
			'INTR_ABSC_TPL_REPEATING_EVENT_YEARLY': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_REPEATING_EVENT_YEARLY'))?>',
			'INTR_ABSC_TPL_INFO_CLOSE': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_INFO_CLOSE'))?>'
		},
		'ERRORS': {
			'ERR_NO_VIEWS_REGISTERED': '<?echo CUtil::JSEscape(GetMessage('IAC_ERR_NO_VIEWS_REGISTERED'))?>',
			'ERR_VIEW_NOT_REGISTERED': '<?echo CUtil::JSEscape(GetMessage('IAC_ERR_VIEW_NOT_REGISTERED'))?>',
			'ERR_WRONG_LAYOUT': '<?echo CUtil::JSEscape(GetMessage('IAC_ERR_WRONG_LAYOUT'))?>',
			'ERR_WRONG_HANDLER': '<?echo CUtil::JSEscape(GetMessage('IAC_ERR_WRONG_HANDLER'))?>',
			'ERR_RUNTIME_NO_VIEW': '<?echo CUtil::JSEscape(GetMessage('IAC_ERR_RUNTIME_NO_VIEW'))?>'
		}
	}
);

//jsBXAC.RegisterView({ID:'day',NAME:'<?//echo CUtil::JSEscape(GetMessage('IAC_VIEW_DAY'))?>//',SORT:100});
jsBXAC.RegisterView({ID:'week',NAME:'<?echo CUtil::JSEscape(GetMessage('IAC_VIEW_WEEK'))?>',SORT:200});
//jsBXAC.RegisterView({ID:'month',NAME:'<?//echo CUtil::JSEscape(GetMessage('IAC_VIEW_MONTH'))?>//',SORT:300});

BX.ready(function() {
	jsBXAC.Show(document.getElementById('bx_calendar_layout'), '<?echo $arParams['VIEW_START']?>');
});

<?
// BML закомментировано. Права даем всем, потом сами разрулим доступ
//if ($arParams['bAdmin']):
?>
var jsBXCalendarAdmin = {
	'LANG': '<?echo LANGUAGE_ID?>',
	'IBLOCK_TYPE': '<?echo CUtil::JSEscape($arParams['IBLOCK_TYPE'])?>',
	'IBLOCK_ID': '<?echo $arParams['IBLOCK_ID']?>',
	'EDIT': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_EDIT'))?>',
	'DELETE': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_DELETE'))?>',
	'DELETE_CONFIRM': '<?echo CUtil::JSEscape(GetMessage('INTR_ABSC_TPL_DELETE_CONFIRM'))?>'
}
<?
//endif;
// BML добавлено:
if ($arResult['ABSENCE_ADMIN']){
	$tmpAdmin = 1;
} else {
	$tmpAdmin = 0;
}
?>
var isAbsenceAdmin=<?=(IsAbsenceAdmin($USER->GetId())?1:0);?>;
</script>

<?
if ($arParams['bAdmin'] && $USER->IsAuthorized()):
	$this->SetViewTarget('pagetitle', 100);?>
	<div class="absence-title-buttons">
<!-- BML - убрали кнопку "Добавить"
<!--		<a href="javascript:void(0)" onclick="--><?//="BX.AbsenceCalendar.ShowForm(".CUtil::PhpToJSObject($arAbsenceParams).")"?><!--" class="absence-title-button">-->
<!--			<i class="absence-title-button-create-icon"></i><span class="absence-link">--><?//=GetMessage('INTR_ABSC_TPL_ADD_ENTRY')?><!--</span>-->
<!--		</a>-->
	</div><?
	$this->EndViewTarget();
endif;
?>


<div id="bx_calendar_conrol_departments" style="display: none;"><?
// BML Определяем подразделение пользователя
// Получаем подразделение
$ar_depts = CIntranetUtils::GetUserDepartments($USER->GetId());
// Важно: Эта конструкция empty($_REQUEST['user'] добавлена, чтобы не выставлять фильтр по подразделению,
// если необходимо показать данные по определенному пользователю
if (!empty($ar_depts) && empty($_REQUEST['user'])) {
	$arResult['UF_DEPARTMENT_field']["VALUE"][] = $ar_depts[0];
	$arResult['UF_DEPARTMENT_field']["ENTITY_VALUE_ID"] = 1; // Я не знаю что это за параметр, но установив его больше нуля все работает
} else {
	$arResult['UF_DEPARTMENT_field']["VALUE"][] = "";
	$arResult['UF_DEPARTMENT_field']["ENTITY_VALUE_ID"] = 1;
}

	CIntranetUtils::ShowDepartmentFilter($arResult['UF_DEPARTMENT_field'], false);

?>
</div><div id="bx_calendar_control_datepicker" style="display: none"><input type="hidden" id="bx_goto_date" name="bx_goto_date" value="<?echo ConvertTimeStamp();?>" /><img src="/bitrix/js/main/core/images/calendar-icon.gif" class="calendar-icon" onclick="BX.calendar({node:this, field:'bx_goto_date', bTime: false, callback: jsBXAC.InsertDate});" onmouseover="BX.addClass(this, 'calendar-icon-hover');" onmouseout="BX.removeClass(this, 'calendar-icon-hover');" border="0"/></div>
<div id="bx_calendar_layout" style="min-height: 160px;">
</div>
<?
	if (!empty($_REQUEST['user'])){
		$dbUser = CUser::GetByID($_REQUEST['user']);
		$arUser = $dbUser->fetch();
		if (!empty($arUser)){
			$UserName = $arUser['LAST_NAME']." ".$arUser['NAME'];
		?>
		<div>
			<input type="hidden" id="hdn_user" value="<?=$UserName?>">
		</div>
	<?}
		}
?>

<script type="text/javascript">
	var singlePopup;

	function onEmployeSelect(arUser){
		BX('emloyee-href').innerHTML = BX.util.htmlspecialchars(arUser.name);
		$('#clearassist').css('display', 'inline-block');
		BX('userid').value = BX.util.htmlspecialchars(arUser.id);
		singlePopup.close();
	}

	function ShowSingleSelector(e) {
		if(!e) e = window.event;
		if (!singlePopup) {
			singlePopup = new BX.PopupWindow("single-employee-popup", BX('emloyee-href'), {
				offsetTop: 1,
				autoHide: true,
				content: BX("employee_selector_content"),
				zIndex: 3000
			});
		} else{
			singlePopup.setContent(BX("employee_selector_content"));
			singlePopup.setBindElement(this);
		}

		if (singlePopup.popupContainer.style.display != "block"){
			singlePopup.show();
		}
		return BX.PreventDefault(e);
	}

	function SaveSettings(){
		ajaxRes = $.ajax({
			type: "POST",
			url: "/local/components/synergy/intranet.absence.calendar/ajax-proc.php",
			data: "action=saveabssettings&userassistant=" + BX('userid').value +
			      "&dis_notify1=" + ($('#cb_dis_notify1').attr("checked") ? 1 : 0) +
				  "&dis_notify2=" + ($('#cb_dis_notify2').attr("checked") ? 1 : 0) +
				  "&dis_notify3=" + ($('#cb_dis_notify3').attr("checked") ? 1 : 0),
			success: function (prmData) {
				obj = $.parseJSON(prmData);
				if (obj.res == -1){
					alert(obj.errortxt);
				} else {
					$('#frm-settings').fadeOut(300);
				}
			}
		});
		return false;
	}

	function ClearAssistant(){
		$('#userid').val("");
		$('#emloyee-href').text("Выберите сотрудника");
		$('#clearassist').css('display', 'none');
	}
</script>
<div>
	<p style="margin: 10px 0 5px; font-size: 13px; font-weight: bold; color: #444;">Обозначения:</p>
	<table style="border-collapse: separate; border-spacing: 1px 4px;">
		<tr>
			<td style="width: 100px; background-color: #55FB55;">
				&nbsp;
			</td>
			<td style="padding: 1px 0 3px 10px; font-size: 12px; color: #444;">
				Отпуск
			</td>
		</tr>
		<tr>
			<td style="width: 100px; background-color: #FDFD55;">
				&nbsp;
			</td>
			<td style="padding: 1px 0 3px 10px; font-size: 12px; color: #444;">
				Утверждено руководителем
			</td>
		</tr>
		<tr>
			<td style="width: 100px; background-color: #FB5555">
				&nbsp;
			</td>
			<td style="padding: 1px 0 3px 10px; font-size: 12px; color: #444;">
				Не утверждено руководителем
			</td>
		</tr>
	</table>
</div>
<div id="frm-settings">
	<div class="frm-settings-header">
		<p class="frm-settings-header-caption">Настройки</p>
		<a href="/" class="frm-settings-close" title="Закрыть форму"></a>
	</div>
	<div class="frm-settings-body">
		<?CUtil::InitJSCore(array('popup'));?>
		<?
// Извлекаем заместителя
		$dbuser = CUser::GetById($USER->GetId());
		$aruser = $dbuser->fetch();
		$userassistant = "";
		if (!empty($aruser)){
			$userassistant = $aruser['UF_USER_ASSISTANT'];
			if (!empty($userassistant)) {
				$dbassist = CUser::GetById($userassistant);
				$arassist = $dbassist->fetch();
				$userassistantName = $arassist['LAST_NAME'] . " " . $arassist['NAME'];
			}
		}
		?>
		<input type="hidden" id="userid" name="userid" value="$userassistant">
		<span>Заместитель: </span>
		<a href="javascript:void(0)" onclick="ShowSingleSelector()" id="emloyee-href">
			<?=(empty($userassistantName)) ? "Выберите сотрудника" : $userassistantName?></a>
		<?if (!empty($userassistantName)) {
			$displayclearass = "inline-block";
		} else{
			$displayclearass = "none";
		}

	?>
		<span id="clearassist" style='display: <?=$displayclearass?>' onclick='return ClearAssistant();' title="Очистить поле"></span>

		<?$name = $APPLICATION->IncludeComponent(
			"bitrix:intranet.user.selector.new", ".default", array(
				"MULTIPLE" => "N",
				"NAME" => "employee",
				"VALUE" => $userassistant,
				"POPUP" => "Y",
				"ON_SELECT" => "onEmployeSelect",
				"SITE_ID" => SITE_ID
			), null, array("HIDE_ICONS" => "Y")
		);
		?>
		<div id="frm-checkboxes">
			<?
// Прочитаем из инфоблока настройки уведомлений
			$dbEl = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>SCHEDULE_IBLOCK_CODE, 'PROPERTY_USER'=>$USER->GetId()), false, false,
				    array('ID', 'IBLOCK_ID', 'PROPERTY_DIS_NOTIFY_1', 'PROPERTY_DIS_NOTIFY_2', 'PROPERTY_DIS_NOTIFY_3'));
			$arEl = $dbEl->fetch();
			if (!empty($arEl)){
				$dis_notify1 = $arEl['PROPERTY_DIS_NOTIFY_1_VALUE'];
				$dis_notify2 = $arEl['PROPERTY_DIS_NOTIFY_2_VALUE'];
				$dis_notify3 = $arEl['PROPERTY_DIS_NOTIFY_3_VALUE'];
			} else {
				$dis_notify1 = 0;
				$dis_notify2 = 0;
				$dis_notify3 = 0;
			}
			?>
			<p>Отключить уведомления:</p>
			<label>
				<input type="checkbox" name="dis_notify1" id="cb_dis_notify1" <?=($dis_notify1 == 1) ? "checked" : ""?>>
				<span>О моих нарушениях</span>
			</label>
			<label>
				<input type="checkbox" name="dis_notify2" id="cb_dis_notify2" <?=($dis_notify2 == 1) ? "checked" : ""?>>
				<span>О нарушениях подчиненных</span>
			</label>
			<label>
				<input type="checkbox" name="dis_notify3" id="cb_dis_notify3" <?=($dis_notify3 == 1) ? "checked" : ""?>>
				<span>О нарушениях контролируемых сотрудников</span>
			</label>

		</div>
		<div class="frm-buttons">
			<a href="/" class="frm-button-submit" onclick="return SaveSettings();">Сохранить</a>
			<a href="/" class="frm-button-cancel" onclick="$('#frm-settings').fadeOut(300); return false;">Отмена</a>

		</div>
	</div>

</div>
