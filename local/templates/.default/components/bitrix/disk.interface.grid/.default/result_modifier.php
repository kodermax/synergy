<?

// Получим все пользовательские поля у которых название UF_DISK_FILE_DESCR
//$dbVar = CUserTypeEntity::GetList(array(), array('FIELD_NAME' => 'UF_DISK_FILE_DESCR'));
//while ($stor = $dbVar->fetch()){
//    $storages[] = $stor['ENTITY_ID'];
//}
//if (!empty($arParams["~GRID_ID"])) {
//// Проверяем, есть ли у нас пользовательское поле для данного стораджа
//    $matches = array();
//    $res = preg_match('#folder_list_(.*)#', $arParams["~GRID_ID"], $matches);
//    $storID = $matches[1];
//    if (!empty($storID)) {
//        if (in_array("DISK_FILE_".$storID, $storages)){
//            $arParams['~HEADERS'][]=array("id"=>'UF_DISK_FILE_'.$storID, "name"=>'Описание', "default"=>1, "sort"=>'UF_DISK_FILE_'.$storID);
//// Теперь вытаскиваем значение пользовательского поля для каждого файла
//            CModule::IncludeModule('disk');
//            foreach($arParams["~ROWS"] as $key=>$row) {
//                $fileID = $arParams["~ROWS"][$key]["data"]["ID"];
//                $file = \Bitrix\Disk\File::loadById($fileID);
//                if (!empty($file)) {
//                    $ufFields = \Bitrix\Disk\Driver::getInstance()->getUserFieldManager()->getFieldsForObject($file);
//                    $arParams["~ROWS"][$key]["columns"]["UF_DISK_FILE_".$storID] = $ufFields['UF_DISK_FILE_DESCR']['VALUE'];
////                    print_r($ufFields); echo "<br><br>";
//                }
////                echo $fileID."<br>";
////
//            }
//        }
//    }
//}

// Общий алгоритм

CModule::IncludeModule('disk');
global $USER_FIELD_MANAGER;
$i = 1; // Счет чик итераций (необходим для того, чтобы выбрать из базы значения для полей типа список только один раз
foreach($arParams["~ROWS"] as $key=>$row) {
    $fileID = $arParams["~ROWS"][$key]["data"]["ID"];
    $file = \Bitrix\Disk\File::loadById($fileID);
    if (!empty($file)) {
        $fieldManager = \Bitrix\Disk\Driver::getInstance()->getUserFieldManager();
        $ufFields = $fieldManager->getFieldsForObject($file);
// Следующий блок if выполнится только один раз
// В нем ищутся ВСЕ пользовательские поля типа список, из них выбираются значения и сохранияются в массиве
// Чтобы при следующих проходах цикла нам не обращаться к БД
        if ($i == 1) {
            $storId = $fieldManager->getUfEntityName($file);
            $storFields = $USER_FIELD_MANAGER->GetUserFields($storId);
//            echo "storId: " . $storId . "<br>";
//            print_r($storFields);
//            echo "<br><br>";
            foreach($storFields as $fName=>$storField){
// Если тип поля список, то найдем все возможные значения списка
                if ($storField['USER_TYPE_ID'] == 'enumeration'){
                    $db_list = CUserFieldEnum::GetList(array(), array('USER_FIELD_ID'=>$storField['ID']));
//                    echo "Список:<br>";
                    while($ar_list = $db_list->fetch()){
                        $stringLists[$fName][$ar_list['ID']] = $ar_list['VALUE'];
                    }
//                    print_r($stringLists);
//                    echo "<br><br>";
                }
            }
            $i++;
        }
// Далее смотрим каждое поле
        foreach($ufFields as $fName=>$Field){
// Анализируем ТОЛЬКО строковые поля и поля типа список
            if (($Field['USER_TYPE_ID'] == 'string') || ($Field['USER_TYPE_ID'] == 'enumeration')) {
// Проверяем, добавлено ли данное поле в заголовок, если его там нет - то добавляем
                $tmpHeader = array("id" => $Field['FIELD_NAME'], "name" => $Field['LIST_COLUMN_LABEL'], "sort" => $Field['FIELD_NAME']);
//                print_r($tmpHeader); echo "<br><br>";
                if (!in_array($tmpHeader, $arParams['~HEADERS'])) {
                    $arParams['~HEADERS'][] = $tmpHeader;
                }
// Добавляем значение в массив ROWS
                if ($Field['USER_TYPE_ID'] == 'string') {
                    $arParams["~ROWS"][$key]["columns"][$fName] = $ufFields[$fName]['VALUE'];
                } elseif ($Field['USER_TYPE_ID'] == 'enumeration'){
                    $arParams["~ROWS"][$key]["columns"][$fName] = $stringLists[$fName][$ufFields[$fName]['VALUE']];
                }
            }
        }
//        $arParams["~ROWS"][$key]["columns"]["UF_DISK_FILE_"] = $Field['VALUE'];
//        print_r($ufFields); echo "<br><br>";
    }
}