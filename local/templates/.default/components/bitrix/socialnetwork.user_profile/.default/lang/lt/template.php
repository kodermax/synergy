<?
$MESS["SONET_MANAGERS"] = "Vadovas";
$MESS["SONET_SUBORDINATE"] = "Pavaldiniai";
$MESS["SONET_GROUPS"] = "Grupės";
$MESS["SONET_ACTIVITY_STATUS"] = "Statusas";
$MESS["SONET_ACTIONS"] = "Veiksmai";
$MESS["SONET_EDIT_PROFILE"] = "Redaguoti profilį";
$MESS["SONET_DEACTIVATE"] = "Nutraukti";
$MESS["SONET_RESTORE"] = "Atleisti";
$MESS["SONET_REINVITE"] = "Pakviesti dar kartą";
$MESS["SONET_DELETE"] = "Pašalinti";
$MESS["SONET_EXTRANET_TO_INTRANET"] = "Perkelti į intranetą";
$MESS["SONET_USER_extranet"] = "Extranet";
$MESS["SONET_USER_fired"] = "Neveiklus";
$MESS["SONET_USER_invited"] = "Pakviestas";
$MESS["SONET_REINVITE_ACCESS"] = "Kvietimas buvo išsiųstas";
$MESS["SONET_ERROR_DELETE"] = "Klaida! Naudotojas nebuvo pašalintas.";
$MESS["SONET_USER_admin"] = "Administratorius";
$MESS["SOCNET_CONFIRM_FIRE"] = "Darbuotojas negalės prisijungti prie portalo, nebus rodomas įmonės struktūroje. Tačiau visi jo arba jos asmens duomenys (failai, pranešimai, užduotys ir tt) išliks nepakitę. \\n\\nAr tikrai norite uždrausti prieigą šiam darbuotojui?";
$MESS["SOCNET_CONFIRM_RECOVER"] = "Darbuotojas galės prisijungti prie portalo ir bus rodomas įmonės struktūroje. \\n\\nAr tikrai norite suteikti prieigą šiam darbuotojui? ";
$MESS["SOCNET_CONFIRM_DELETE"] = "Minėtasis bus pašalintas negrįžtamai. \\n\\nAr tikrai norite pašalinti šį darbuotoją?";
$MESS["SONET_USER_ABSENCE"] = "Neatvykimas";
$MESS["SONET_SEND_MESSAGE"] = "Siųsti žinutę";
$MESS["SONET_VIDEO_CALL"] = "Vaizdo skambutis";
$MESS["SONET_CONTACT_TITLE"] = "Kontaktinė informacija";
$MESS["SONET_CONTACT_EMPTY"] = "Nėra kontaktinės informacijos";
$MESS["SONET_COMMON_TITLE"] = "Bendra informacija";
$MESS["SONET_COMMON_EMPTY"] = "Nėra bendros informacijos";
$MESS["SONET_ADDITIONAL_TITLE"] = "Papildoma informacija";
$MESS["SONET_ADDITIONAL_EMPTY"] = "Nėra papildomos informacijos";
$MESS["SONET_C38_TP_NO_PERMS"] = "Jūs neturite leidimo peržiūrėti šio naudotojo profilį.";
$MESS["BX24_TITLE"] = "Perkelti iš extraneto į intranetą";
$MESS["BX24_BUTTON"] = "Perkelti ";
$MESS["BX24_CLOSE_BUTTON"] = "Uždaryti";
$MESS["BX24_LOADING"] = "kraunama";
$MESS["SONET_SONET_ADMIN_ON"] = "Administratoriaus režimas";
$MESS["SONET_PASSWORDS"] = "Aplikaijos slaptažodis";
$MESS["SONET_SECURITY"] = "Saugumas";
$MESS["SONET_OTP_AUTH"] = "Dviejų etapų autentifikavimas ";
$MESS["SONET_OTP_NO_DAYS"] = "visada";
$MESS["SONET_OTP_ACTIVE"] = "Įjungti";
$MESS["SONET_OTP_NOT_ACTIVE"] = "Išjungti";
$MESS["SONET_OTP_CHANGE_PHONE"] = "Nustatyti naujam telefonui";
$MESS["SONET_OTP_ACTIVATE"] = "Įjungti";
$MESS["SONET_OTP_DEACTIVATE"] = "Išjungti";
$MESS["SONET_OTP_PROROGUE"] = "Atidėti";
$MESS["SONET_OTP_LEFT_DAYS"] = "(bus įjungtas #NUM#)";
$MESS["SONET_PASSWORDS_SETTINGS"] = "Nustatymai ";
$MESS["SONET_OTP_SUCCESS_POPUP_TEXT"] = "<b>Sveikiname!</b>
<br/><br/>
Jūs nustatėte savo paskyros dviejų etapų autentifikavimą.
<br/><br/>
Dviejų etapų autentifikavvimas reiškia, kad jums teks išlaikyti dviejų lygių
patikrinimą. Pirmasis reikalauja jūsų pagrindinio slaptažodžio. Antrasis etapas apima vieną kartą atsiųstą kodą į jūsų mobilųjį telefoną.
<br/><br/>
Atkreipkite dėmesį, kad jūs turite naudoti aplikacijos slaptažodžius dėl saugaus keitimosi duomenimis (pavyzdžiui, su išorės kalendoriumi). Jūs rasite įrankius tvarkyti aplikacijos slaptažodžius jūsų naudotojo profilyje.";
$MESS["SONET_OTP_SUCCESS_POPUP_CLOSE"] = "Uždaryti";
$MESS["SONET_OTP_SUCCESS_POPUP_PASSWORDS"] = "Konfigūruoti aplikacijos slaptažodižius";
$MESS["SONET_OTP_NOT_EXIST"] = "Nesukonfigūruota";
$MESS["SONET_OTP_SETUP"] = "Konfigūruoti ";
?>