<?php
/**
 * Created by PhpStorm.
 * User: MBochkov
 * Date: 14.01.2016
 * Time: 10:06
 */

define('ENTITY_TYPE', 'ABSENCE');
define('EVENT_ID', 'news');
require_once ($_SERVER["DOCUMENT_ROOT"] . "/local/modules/intranet/tools/abs_functions.php");

AddEventHandler('socialnetwork', 'OnFillSocNetLogEvents', array('CAbsenceLogHandlers', 'OnFillSocNetLogEvents'));
AddEventHandler('socialnetwork', 'OnFillSocNetFeaturesList', array('CAbsenceLogHandlers', 'OnFillSocNetFeaturesList'));
AddEventHandler('socialnetwork', 'OnFillSocNetAllowedSubscribeEntityTypes', array('CAbsenceLogHandlers', 'OnFillSocNetAllowedSubscribeEntityTypes'));

class CAbsenceLogHandlers
{
    public static function GetIBlockByID($ID)
    {
//        if (CModule::IncludeModule('iblock'))
//            if ($arInfo = CIBlock::GetByID($ID)->GetNext())
                return array('NAME_FORMATTED' => "Нарушение трудовой дисциплины", '~NAME_FORMATTED' => "Нарушение трудовой дисциплины", 'URL' => "/company/absence.php");
    }

    public static function ShowEntityLink($arEntityDesc, $strEntityURL, $arParams)
    {
        return "ShowEntityLink";
    }

    public static function OnFillSocNetLogEvents(&$arSocNetLogEvents)
    {
        $arSocNetLogEvents[EVENT_ID] = array(
            'ENTITIES'    =>    array(
                ENTITY_TYPE => array(
                    'TITLE'             => 'Нарушение трудовой дисциплины',
                    'TITLE_SETTINGS'    => 'Нарушение трудовой дисциплины',
                    'TITLE_SETTINGS_1'    => 'Уведомления о нарушениях трудовой дисциплины',
                    'TITLE_SETTINGS_2'    => 'Уведомления о нарушениях трудовой дисциплины',
                ),
            ),
            'CLASS_FORMAT'    => 'CAbsenceLogHandlers',
            'METHOD_FORMAT'    => 'FormatEvent'
        );
    }

    public static function OnFillSocNetFeaturesList(&$arSocNetFeaturesSettings)
    {
        $arSocNetFeaturesSettings[EVENT_ID]['subscribe_events'][EVENT_ID]['ENTITIES'][ENTITY_TYPE] = array();
    }

    public static function OnFillSocNetAllowedSubscribeEntityTypes(&$arSocNetAllowedSubscribeEntityTypes)
    {
        $arSocNetAllowedSubscribeEntityTypes[] = ENTITY_TYPE;

        global $arSocNetAllowedSubscribeEntityTypesDesc;
        $arSocNetAllowedSubscribeEntityTypesDesc[ENTITY_TYPE] = array(
            'TITLE_LIST'        => 'Записи трудовой дисциплины',
        );
    }

    public static function FormatEvent($arFields, $arParams, $bMail = false)
    {
        global $APPLICATION;

        $dbAbsenceFields = CIBlockElement::GetList(array(), array('ID' => $arFields['ENTITY_ID']), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_USER', 'DETAIL_TEXT'));
        $arAbsenceFields = $dbAbsenceFields->fetch();

        $arEventParams = unserialize(strlen($arFields['~PARAMS']) > 0 ? $arFields['~PARAMS'] : $arFields['PARAMS']);

        $arResult = array('EVENT' => $arFields, 'ENTITY' => array(), 'URL' => '');
        $arResult['ENTITY']['FORMATTED'] = array();
        $arResult['ENTITY']['FORMATTED']['NAME'] = "Контроль трудовой дисциплины";
        $title = $arFields['TITLE'];//'Добавлены записи о нарушении трудовой дисциплины';
// Формируем сообщение
        if (!empty($arAbsenceFields)) {
            ob_start();
            $APPLICATION->IncludeComponent("bitrix:main.user.link",
                '',
                array(
                    "ID" => $arAbsenceFields['PROPERTY_USER_VALUE'],
                    "USE_THUMBNAIL_LIST" => "N",
                    "NAME_TEMPLATE" => "#LAST_NAME# #NAME#",
                ), false, array("HIDE_ICONS" => "Y"));
            echo "<style>.bx-user-info-online-cell{display: none}</style>";
            $message = ob_get_contents();
            ob_end_clean();
            $message = "<b>Сотрудник:</b>" . $message . $arAbsenceFields["DETAIL_TEXT"]."<br>".$arFields["MESSAGE"];
        } else {
            $message = "ОШИБКА: Не удалось определить сотрудника.";
        }
        $arResult['EVENT_FORMATTED'] = array(
            'TITLE_24'            => $title,
            'MESSAGE'        => $message,
//            'SHORT_MESSAGE'        => $arFields["SHORT_MESSAGE"],
            'IS_IMPORTANT'    => true
        );

        $arResult['EVENT_FORMATTED']['IS_MESSAGE_SHORT'] = CSocNetLog::FormatEvent_IsMessageShort($arFields['MESSAGE'], $arFields['SHORT_MESSAGE']);
//        $arResult['EVENT_FORMATTED']['URL'] = $arFields['URL'];
//        $arResult['EVENT']['USER_ID'] = '';
        return $arResult;
    }
}