<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 25.12.2015
 * Time: 17:54
 */

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$dbUsers = CUser::GetList(($by="personal_country"), ($order="desc"), array(), array('SELECT'=>array('ID', 'UF_DEPARTMENT', 'LOGIN')));
while ($arUsers=$dbUsers->fetch()){
    if (empty($arUsers['UF_DEPARTMENT'])){
        echo $arUsers['LOGIN']."<br>";
    }
}