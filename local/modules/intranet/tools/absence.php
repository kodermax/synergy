<?
define("PUBLIC_AJAX_MODE", true);
define("NO_AGENT_CHECK", true);
define("DisableEventsCheck", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/tools/absence.php");


$arParams = $_REQUEST['arParams'];

$iblockID = isset($_REQUEST['IBLOCK_ID'])
	? intval($_REQUEST['IBLOCK_ID'])
	: (is_array($arParams)
		? intval($arParams['IBLOCK_ID'])
		: 0
	);
if ($iblockID <= 0)
	$iblockID = COption::GetOptionInt("intranet", "iblock_absence");

$bIblockChanged = $iblockID != COption::GetOptionInt('intranet', 'iblock_absence');


function AddAbsence($arFields)
{
	global $iblockID;
	if (CModule::IncludeModule('iblock'))
	{
		$PROP = array();

		$db_absence = CIBlockProperty::GetList(Array(), Array("CODE"=>"ABSENCE_TYPE", "IBLOCK_ID"=>$iblockID));
		if ($ar_absence = $db_absence->Fetch())
		{
			$PROP[$ar_absence['ID']] = array($arFields["ABSENCE_TYPE"]);
		}

		$db_user = CIBlockProperty::GetList(Array(), Array("CODE"=>"USER", "IBLOCK_ID"=>$iblockID));
		if ($ar_user = $db_user->Fetch())
		{
			$PROP[$ar_user['ID']] = array($arFields["USER_ID"]);
		}
// + BML Сохраняем поле "Утверждено"
		$db_saltoapproved = CIBlockProperty::GetList(Array(), Array("CODE"=>"SALTO_APPROVED", "IBLOCK_ID"=>$iblockID));
		if ($ar_saltoapproved = $db_saltoapproved->Fetch())
		{
			$PROP[$ar_saltoapproved['ID']] = array($arFields["SALTO_APPROVED"]);
		}
// ****************************************

		$arNewFields = array(
			"NAME" => $arFields["NAME"],
			"PROPERTY_VALUES"=> $PROP,
			"ACTIVE_FROM" => $arFields["ACTIVE_FROM"],
			"ACTIVE_TO" => $arFields["ACTIVE_TO"],
			"IBLOCK_ID" => $iblockID
		);

		$element = new CIBlockElement;
		$ID = $element->Add($arNewFields);
	}
	if(!$ID)
	{
		$arErrors = preg_split("/<br>/", $element->LAST_ERROR);
		return $arErrors;
	}
	else
	{
		return $ID;
	}
}
function EditAbsence($arFields)
{
	global $iblockID;
	if (CModule::IncludeModule('iblock'))
	{
		$PROP = array();

		$db_absence = CIBlockProperty::GetList(Array(), Array("CODE"=>"ABSENCE_TYPE", "IBLOCK_ID"=>$iblockID));
		if ($ar_absence = $db_absence->Fetch())
		{
			$PROP[$ar_absence['ID']] = array($arFields["ABSENCE_TYPE"]);
		}

		$db_user = CIBlockProperty::GetList(Array(), Array("CODE"=>"USER", "IBLOCK_ID"=>$iblockID));
		if ($ar_user = $db_user->Fetch())
		{
			$PROP[$ar_user['ID']] = array($arFields["USER_ID"]);
		}
// + BML Сохраняем дополнительные свойства
		$db_element = CIBlockElement::GetList(array(), array("ID" => $arFields["absence_element_id"], "IBLOCK_ID" => $iblockID), false, false,
			array("ID", "IBLOCK_ID", "PROPERTY_SALTO_TYPE", "PROPERTY_SALTO_IMPORTED", "PROPERTY_SALTO_APPROVED", "PROPERTY_VIOLATION_ID"));
		$ar_element = $db_element->fetch();
// Сохраняем поле "Утверждено" (если оно не пришло в параметрах запроса, то тащим его из БД)
		if (!empty($arFields["SALTO_APPROVED"])) {
			$db_saltoapproved = CIBlockProperty::GetList(Array(), Array("CODE" => "SALTO_APPROVED", "IBLOCK_ID" => $iblockID));
			if ($ar_saltoapproved = $db_saltoapproved->Fetch()) {
				$PROP[$ar_saltoapproved['ID']] = array($arFields["SALTO_APPROVED"]);
			}
		} else {
			if ($ar_element){
				$db_prop = CIBlockProperty::GetList(Array(), Array("CODE"=>"SALTO_APPROVED", "IBLOCK_ID"=>$iblockID));
				if ($ar_prop = $db_prop->Fetch())
				{
					$PROP[$ar_prop['ID']] = array($ar_element['PROPERTY_SALTO_APPROVED_VALUE']);
				}
			}
		}
// Сохраняем поля SALTO_TYPE, SALTO_IMPORTED и VIOLATION_ID
		if ($ar_element){
			$db_prop = CIBlockProperty::GetList(Array(), Array("CODE"=>"SALTO_TYPE", "IBLOCK_ID"=>$iblockID));
			if ($ar_prop = $db_prop->Fetch())
			{
				$PROP[$ar_prop['ID']] = array($ar_element['PROPERTY_SALTO_TYPE_ENUM_ID']);
			}
			$db_prop = CIBlockProperty::GetList(Array(), Array("CODE"=>"SALTO_IMPORTED", "IBLOCK_ID"=>$iblockID));
			if ($ar_prop = $db_prop->Fetch())
			{
				$PROP[$ar_prop['ID']] = array($ar_element['PROPERTY_SALTO_IMPORTED_VALUE']);
			}
			$db_prop = CIBlockProperty::GetList(Array(), Array("CODE"=>"VIOLATION_ID", "IBLOCK_ID"=>$iblockID));
			if ($ar_prop = $db_prop->Fetch())
			{
				$PROP[$ar_prop['ID']] = array($ar_element['PROPERTY_VIOLATION_ID_VALUE']);
			}
		}

// ****************************************

		$arNewFields = array(
			"NAME" => $arFields["NAME"],
			"PROPERTY_VALUES"=> $PROP,
//			"ACTIVE_FROM" => $arFields["ACTIVE_FROM"],
//			"ACTIVE_TO" => $arFields["ACTIVE_TO"],
			"IBLOCK_ID" => $iblockID
		);

		if (!empty($arFields["ACTIVE_FROM"])) $arNewFields['ACTIVE_FROM'] = $arFields["ACTIVE_FROM"];
		if (!empty($arFields["ACTIVE_TO"])) $arNewFields['ACTIVE_TO'] = $arFields["ACTIVE_TO"];

		$element = new CIBlockElement;
		$ID = $element->Update(intval($arFields["absence_element_id"]), $arNewFields);
	}
	if(!$ID)
	{
		$arErrors = preg_split("/<br>/", $element->LAST_ERROR);
		return $arErrors;
	}
	else
	{
// Обновляем запись в DMS (MSSQL)
		UpdateDMSRecotd(intval($arFields["absence_element_id"]), $iblockID);
		return $ID;
	}
}

function DeleteAbsence($absenceID)
{
	global $USER;
	if ((CModule::IncludeModule('iblock')) && (CModule::IncludeModule('intranet')))
	{
// Проверим права пользователя
		$CanDelete = CanEdit($absenceID, $USER->GetID(), "delete");
		if ($CanDelete) {
			CIBlockElement::Delete(intval($absenceID));
			echo json_encode(array("RESULT" => "1", "USERNAME" => $USER->GetFullName()));
		} else {
			echo json_encode(array("RESULT" => "-1", "USERNAME" => $USER->GetFullName()));
		}
	}
}

if(!CModule::IncludeModule('iblock'))
{
	echo GetMessage("INTR_ABSENCE_BITRIX24_MODULE");
}
else
{
	if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET["action"]) && $_GET["action"] == "delete" && check_bitrix_sessid())
	{
		if(CIBlockElementRights::UserHasRightTo($iblockID, intval($_GET["absenceID"]), "element_delete")){
			DeleteAbsence($_GET["absenceID"]);
		} else {
			echo json_encode(array("RESULT" => "-2"));
		}
		die();
	}

	$ID = 1;
	if($_SERVER["REQUEST_METHOD"] === "POST" && check_bitrix_sessid())
	{
		if (isset($_POST['absence_element_id']) && CIBlockElementRights::UserHasRightTo($iblockID, intval($_POST['absence_element_id']), 'element_edit'))
		{
			$ID = EditAbsence($_POST);
		}
		elseif(!isset($_POST['absence_element_id']) && CIBlockSectionRights::UserHasRightTo($iblockID, 0, "section_element_bind"))
		{
			$ID = AddAbsence($_POST);
		}
		else
		{
			die('error:<li>'.GetMessage('INTR_USER_ERR_NO_RIGHT').'</li>');
		}

		if(is_array($ID))
		{
			$arErrors = $ID;
			foreach ($arErrors as $key => $val) {if (strlen($val) <= 0) unset($arErrors[$key]);}
			$ID = 0;
			die('error:<li>'.implode('</li><li>', $arErrors)).'</li>';
		}
		elseif (isset($_POST['absence_element_id']))
			die("close");
	}
?><div style="width:450px;padding:5px;"><?

	if ($ID>1)
	{
	?>

	<p><?=GetMessage("INTR_ABSENCE_SUCCESS")?></p>
	<form method="POST" action="<?echo "/local/modules/intranet/tools/absence.php".($bIblockChanged?"?IBLOCK_ID=".$iblockID:"")?>" id="ABSENCE_FORM">
		<input type="hidden" name="reload" value="Y">
	</form><?
	}
	else
	{
		$arElement = array();
		if (isset($arParams["ABSENCE_ELEMENT_ID"]) && intval($arParams["ABSENCE_ELEMENT_ID"])>0)
		{
// ********** BML закомментировано
//			$rsElement = CIBlockElement::GetList(array(), array("ID" => intval($arParams["ABSENCE_ELEMENT_ID"]), "IBLOCK_ID" => $iblockID), false, false, array("ID", "NAME", "ACTIVE_FROM", "ACTIVE_TO", "IBLOCK_ID", "PROPERTY_ABSENCE_TYPE", "PROPERTY_USER"));
//*******************************
// ********** + BML
			$rsElement = CIBlockElement::GetList(array(), array("ID" => intval($arParams["ABSENCE_ELEMENT_ID"]), "IBLOCK_ID" => $iblockID), false, false, array("ID", "NAME", "ACTIVE_FROM", "ACTIVE_TO", "IBLOCK_ID", "PROPERTY_ABSENCE_TYPE", "PROPERTY_USER", "PROPERTY_SALTO_APPROVED", "PROPERTY_SALTO_TYPE", "PROPERTY_SALTO_IMPORTED", "DETAIL_TEXT"));
//*******************************
			$arElement = $rsElement->Fetch();
		}

		$controlName = "Single_" . RandString(6);
	?>
	<form method="POST" action="<?echo "/local/modules/intranet/tools/absence.php"?>" id="ABSENCE_FORM">
		<?if (isset($_POST['absence_element_id']) || isset($arElement["ID"])):?>
		<input type="hidden" value="<?=(isset($_POST['absence_element_id'])) ? htmlspecialcharsbx($_POST['absence_element_id']) : $arElement['ID']?>" name="absence_element_id"><?
		endif;?>
<?
if ($bIblockChanged):
?>
		<input type="hidden" name="IBLOCK_ID" value="<?=$iblockID?>" />
<?
endif;
?>

		<table width="100%" cellpadding="5">
			<tr valign="bottom">
				<td colspan="2">
					<?if (CanEdit($arParams["ABSENCE_ELEMENT_ID"], $USER->GetID(), "canchangeuser")){?>
					<div style="font-size:14px;font-weight:bold;padding-bottom:8px"><label for="USER_ID"><?=GetMessage("INTR_ABSENCE_USER")?></label></div>
					<?} else {?>
					<div style="font-size:14px;font-weight:bold;padding-bottom:8px"><label for="USER_ID">Сотрудник</label></div>
					<?}
					$UserName = "";
					if (isset($_POST['USER_ID']) || isset($arElement["PROPERTY_USER_VALUE"]))
					{
						$UserID = isset($_POST['USER_ID']) ? $_POST['USER_ID'] : $arElement["PROPERTY_USER_VALUE"];
						$dbUser = CUser::GetList($b="", $o="", array("ID" => intval($UserID)));
						if ($arUser = $dbUser->Fetch())
							$UserName = CUser::FormatName(CSite::GetNameFormat(false), $arUser, true);
					}
					?>
					<input type="hidden" id="user_id" value="<?if (isset($_POST["USER_ID"])) echo htmlspecialcharsbx($_POST["USER_ID"]); elseif (isset($arElement["PROPERTY_USER_VALUE"])) echo htmlspecialcharsbx($arElement["PROPERTY_USER_VALUE"]);?>" name="USER_ID" style="width:35px;font-size:14px;border:1px #c8c8c8 solid;">
					<span id="uf_user_name"><?=$UserName?></span>

					<?
					if (CanEdit($arParams["ABSENCE_ELEMENT_ID"], $USER->GetID(), "changeuser")){
					CUtil::InitJSCore(array('popup'));?>
					<a href="javascript:void(0)" onclick="ShowSingleSelector" id="single-user-choice"><?=GetMessage("INTR_USER_CHOOSE")?></a>
					<script type="text/javascript" src="/bitrix/components/bitrix/intranet.user.selector.new/templates/.default/users.js"></script>
					<script type="text/javascript">BX.loadCSS('/bitrix/components/bitrix/intranet.user.selector.new/templates/.default/style.css');</script>
					<script type="text/javascript">// user_selector:
						var multiPopup, singlePopup, taskIFramePopup;
						function onSingleSelect(arUser)
						{
							BX("user_id").value = arUser.id;
							BX("uf_user_name").innerHTML = BX.util.htmlspecialchars(arUser.name);
							singlePopup.close();
						}

						function ShowSingleSelector(e) {

							if(!e) e = window.event;

							if (!singlePopup)
							{
								// ��������� �� ������� �����, �� ������ ���������� ��� ���� ������� - ������� div � id = $arParams["NAME"]."_selector_content"
								singlePopup = new BX.PopupWindow("single-employee-popup", this, {
									offsetTop : 1,
									autoHide : true,
									content : BX("<?=CUtil::JSEscape($controlName)?>_selector_content"), // div � ��������� ��� ������
									zIndex: 3000
								});
							}
							else
							{
								singlePopup.setContent(BX("<?=CUtil::JSEscape($controlName)?>_selector_content"));
								singlePopup.setBindElement(this);
							}

							if (singlePopup.popupContainer.style.display != "block")
							{
								singlePopup.show();
							}

							return BX.PreventDefault(e);
						}

						function Clear()
						{
							O_<?=CUtil::JSEscape($controlName)?>.setSelected();
						}

						BX.ready(function() {
							BX.bind(BX("single-user-choice"), "click", ShowSingleSelector);
							BX.bind(BX("clear-user-choice"), "click", Clear);
						});
					</script>
					<?$name = $APPLICATION->IncludeComponent(
							"bitrix:intranet.user.selector.new", ".default", array(
								"MULTIPLE" => "N",
								"NAME" => $controlName,
								"VALUE" => 1,
								"POPUP" => "Y",
								"ON_SELECT" => "onSingleSelect",
								"SITE_ID" => SITE_ID
							), null, array("HIDE_ICONS" => "Y")
						);
					}?>
				</td>
			</tr>
			<tr valign="bottom">
				<td>
					<div style="font-size:14px;font-weight:bold;padding-bottom:8px"><label for="ABSENCE_TYPE"><?=GetMessage("INTR_ABSENCE_TYPE")?></label></div>
					<select name="ABSENCE_TYPE" id="absence_type"
						<?
						 	if (!CanEdit($arParams["ABSENCE_ELEMENT_ID"], $USER->GetID(), "approve")){
								echo " onmousedown='return(false)' onkeyup='return(false);' onkeydown='return(false);' ".
								"style='width:100%;font-size:14px;border:1px #c8c8c8 solid; background-color: #f8fafb'";
							} else {
								echo " style='width:100%;font-size:14px;border:1px #c8c8c8 solid;'";
							}
						?>
						>
						<!--<option value="0">--><?//=GetMessage("INTR_ABSENCE_NO_TYPE")?><!--</option>-->
						<?
						$property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $iblockID, "CODE" => "ABSENCE_TYPE"));
						if (CanEdit($arParams["ABSENCE_ELEMENT_ID"], $USER->GetID(), "approve")) {
							while ($enum_fields = $property_enums->Fetch()) {
								?>
								<option
								value="<? echo $enum_fields["ID"] ?>" <? if (isset($_POST['ABSENCE_TYPE']) && $_POST['ABSENCE_TYPE'] == $enum_fields["ID"] || isset($arElement["PROPERTY_ABSENCE_TYPE_ENUM_ID"]) && $arElement["PROPERTY_ABSENCE_TYPE_ENUM_ID"] == $enum_fields["ID"])
									echo "selected" ?>><?= $enum_fields["VALUE"] ?></option><?
							}
						} else {
							while ($enum_fields = $property_enums->Fetch()) {
								if (isset($_POST['ABSENCE_TYPE']) && $_POST['ABSENCE_TYPE'] == $enum_fields["ID"] ||
									isset($arElement["PROPERTY_ABSENCE_TYPE_ENUM_ID"]) && $arElement["PROPERTY_ABSENCE_TYPE_ENUM_ID"] == $enum_fields["ID"]){
									?>
									<option value="<? echo $enum_fields["ID"] ?>" selected><?= $enum_fields["VALUE"] ?></option>
									<?
								}
								?>
								 <?
							}
						}
						?>
					</select>
				</td>
			</tr>
			<tr valign="bottom">
				<td>
					<div style="font-size:14px;font-weight:bold;padding-bottom:8px"><label for="NAME"><?=GetMessage("INTR_ABSENCE_NAME")?></label></div>
					<input type="text" value="<?if (isset($_POST['NAME'])) echo htmlspecialcharsbx($_POST['NAME']); elseif (isset($arElement["NAME"])) echo htmlspecialcharsbx($arElement["NAME"]);?>" name="NAME" id="NAME" style="width:100%;font-size:14px;border:1px #c8c8c8 solid;">
				</td>
			</tr>
			<tr>
				<td>
					<div style="font-size:14px;font-weight:bold;padding-bottom:8px"><?=GetMessage("INTR_ABSENCE_PERIOD")?></div>
				</td>
			</tr>
			<tr valign="bottom" >
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td  width="100px">
								<label for="ACTIVE_FROM"><?=GetMessage("INTR_ABSENCE_ACTIVE_FROM")?></label>
							</td>
							<td>
								<?
								$input_value_from = "";
								if (isset($arElement["ACTIVE_FROM"]) || isset($_POST["ACTIVE_FROM"]))
									$input_value_from = (isset($arElement["ACTIVE_TO"])) ? htmlspecialcharsbx(FormatDateFromDB($arElement["ACTIVE_FROM"])) : htmlspecialcharsbx(FormatDateFromDB($_POST["ACTIVE_FROM"]));
								if (CanEdit($arParams["ABSENCE_ELEMENT_ID"], $USER->GetID(), "changedate")) {
									$APPLICATION->IncludeComponent("bitrix:main.calendar", "", Array(
											"SHOW_INPUT" => "Y",
											"FORM_NAME" => "",
											"INPUT_NAME" => "ACTIVE_FROM",
											"INPUT_VALUE" => $input_value_from,
											"SHOW_TIME" => "Y",
											"HIDE_TIMEBAR" => "Y"
										)
									);
								} else {
									?>
									<p style="margin:0"><?= $input_value_from ?></p>
								<? } ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr valign="bottom">
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="100px">
								<label for="ACTIVE_TO"><?=GetMessage("INTR_ABSENCE_ACTIVE_TO")?></label>
							</td>
							<td>
							<?
							$input_value_to = "";
							if (isset($arElement["ACTIVE_TO"]) || isset($_POST["ACTIVE_TO"]))
								$input_value_to = (isset($arElement["ACTIVE_TO"])) ? htmlspecialcharsbx(FormatDateFromDB($arElement["ACTIVE_TO"])) : htmlspecialcharsbx(FormatDateFromDB($_POST["ACTIVE_TO"]));
							if (CanEdit($arParams["ABSENCE_ELEMENT_ID"], $USER->GetID(), "changedate")) {
								$APPLICATION->IncludeComponent("bitrix:main.calendar", "", Array(
										"SHOW_INPUT" => "Y",
										"FORM_NAME" => "",
										"INPUT_NAME" => "ACTIVE_TO",
										"INPUT_VALUE" => $input_value_to,
										"SHOW_TIME" => "Y",
										"HIDE_TIMEBAR" => "Y"
									)
								);
							} else {
								?>
								<p style="margin:0"><?= $input_value_from ?></p>
							<? } ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
<!--// ******* + BML *******************-->
			<?
// Это блок будем показывать только для импортированных записей
			if ($arElement['PROPERTY_SALTO_IMPORTED_VALUE']) {
				?>
				<tr>
					<td width="100px">
						<div style="font-size:14px;font-weight:bold;padding-top:8px; border-top: 1px solid #ccc">SALTO <?=BX_ROOT?>
						</div>
					</td>
				</tr>
				<tr style="line-height: 2px">
					<td style="padding: 0 5px;">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="100px" style="height: 10px">
									<p>Тип отсутствия:</p>
								</td>
								<td style="height: 10px">
									<?
									$property_enums = CIBlockPropertyEnum::GetList(Array("IBLOCK_ID" => $iblockID, "CODE" => "SALTO_TYPE"));
									while ($enum_fields = $property_enums->Fetch()) {
										if (isset($arElement["PROPERTY_SALTO_TYPE_ENUM_ID"]) && $arElement["PROPERTY_SALTO_TYPE_ENUM_ID"] == $enum_fields["ID"])
											$tmpSaltoType = $enum_fields["VALUE"];
									}?>
									<p style="line-height: 2px"><?= $tmpSaltoType ?></p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr valign="bottom">
					<td style="padding: 0 5px;">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="100px" style="height: 10px">
									<p>Описание:</p>
								<td
								<td>
									<p><?= preg_replace("#^(.*\";)#", "", $arElement["DETAIL_TEXT"], 1); ?></p>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr style="line-height: 10px">
					<td style="padding: 0 5px;">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="100px">
									<label for="SALTO_APPROVED">Утверждено:</label>
								</td>
								<td>
									<?
// Если пользователь имеет право на утверждение и запись еще не утверждена, то показываем checkbox
									if((CanEdit($arParams["ABSENCE_ELEMENT_ID"], $USER->GetID(), "approve")) && !((isset($arElement["PROPERTY_SALTO_APPROVED_VALUE"])) && ($arElement["PROPERTY_SALTO_APPROVED_VALUE"]))){
									?>
									<input type="checkbox" name="SALTO_APPROVED"
										   value="1" <? if ((isset($arElement["PROPERTY_SALTO_APPROVED_VALUE"])) && ($arElement["PROPERTY_SALTO_APPROVED_VALUE"]) == 1)
										echo "checked" ?>>
									<?} else {
										if ($arElement["PROPERTY_SALTO_APPROVED_VALUE"]){
											?>
											<div style="background: #fdfd55; margin-top: 1px; padding:5px; text-align: center; font-size: 13px;"><b>Утверждено руководителем<b></div>
											<?
										} else {
											?>
											<div style="background: #fd5555; margin-top: 1px; padding:5px; text-align: center; font-size: 13px;"><b>Не утверждено руководителем</b></div>
											<?
										}
									}?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<? } ?>
			<!--//**********************************-->
		</table>
		<?echo bitrix_sessid_post()?>
	</form>
<?
	}
?>
	<script type="text/javascript">
		var myBX;
		if(window.BX)
			myBX = window.BX;
		else if (window.top.BX)
			myBX = window.top.BX;
		else
			myBX = null;

		var myButtons = myBX.AbsenceCalendar.popup.buttons;
		var myTitle = myBX.AbsenceCalendar.popup.titleBar;
		<?if(isset($arParams["ABSENCE_ELEMENT_ID"]) && intval($arParams["ABSENCE_ELEMENT_ID"])>0 || isset($_POST['absence_element_id'])):?>
			myButtons[0].setName('<?echo GetMessage("INTR_ABSENCE_EDIT")?>');
			myTitle.innerHTML = '<span><?echo GetMessage("INTR_EDIT_TITLE")?></span>';
		<?elseif ($ID > 1):?>
			myButtons[0].setName('<?echo GetMessage("INTR_ABSENCE_ADD_MORE")?>');
			myTitle.innerHTML = '<span><?echo GetMessage("INTR_ADD_TITLE")?></span>';
		<?else:?>
			myButtons[0].setName('<?echo GetMessage("INTR_ABSENCE_ADD")?>');
			myTitle.innerHTML = '<span><?echo GetMessage("INTR_ADD_TITLE")?></span>';
		<?endif?>

		myButtons = null;
		myBX = null;
		myTitle = null;
	</script>
</div>
<?
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
?>
