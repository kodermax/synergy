<?php

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/intranet/tools/abs_functions.php");

// Ф-ция импортирует отсутствия из базы DMS
// $prmCurDay - True - значит импортируем за предыдущий день (только если не передан GET параметр DATE)
//              False - импортируем за вчерашний день (только если не передан GET параметр DATE)
function SKD_import_data($prmCurDay = false)
{
    global $DB;

    CModule::IncludeModule("iblock");
    CModule::IncludeModule("socialnetwork");

    $Date = $_REQUEST['DATE'];
    $iblock_code = 'absence';
    $clear_table = false; // True - значит очистим данные за импортируемую дату
                          // Т.к. решили загружать данные каждый час - не будем удалять предыдущие записи
                          // Алгоритм не должен создавать дубликатов
    $logfile = __DIR__."/logs/import-".date('Y-m-d').".log";
// Зададим рабочий график "по умолчанию"
// Т.к. GetStdWorkTime возвращает пустое значение для субботы и вокресенья, то в качестве даты мы передадим
// 01.01.2001 - это понедельник
    $worktime = GetStdWorkTime("01.01.2001");
    $defaultStart = $worktime['START_TIME'].":00"; // Время начала работы сотрудника по-умолчанию (добавляем секунды для "единообразия" с MSSQL)
    $defaultEnd = $worktime['END_TIME'].":00"; // Время окончания работы сотрудника по-умолчанию (добавляем секунды для "единообразия" с MSSQL)

// Если дата пустая, то устанавливаем дату в зависимости от параметра $prmCurDay
    if (empty($Date)) {
        if ($prmCurDay === true){
            $Date = date('d.m.Y');
        } else {
            $Date = date('d.m.Y', time() - 86400);
        }
    }
// Проверим $Date
    if (!$DB->isDate($Date, 'DD.MM.YYYY')) {
//        echo('Ошибка передачи параметра.'); // Это надо в лог писать
        return;
    } else {
// Отформатируем дату
        $DateSQL = $DB->FormatDate($Date, 'DD.MM.YYYY', "MM/DD/YYYY");
        $Date = $DB->FormatDate($Date, 'DD.MM.YYYY', "DD.MM.YYYY");
    }

// Ищем ID инфоблока по коду
    $ibDB = CIBLock::GetList(array(), array('CODE' => $iblock_code));
    $tmpIB = $ibDB->fetch();
    $IB_ID = $tmpIB['ID'];

// Заполняем массив типов отсутствий
    $listDB = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $IB_ID, 'CODE' => 'ABSENCE_TYPE'));
    while ($list = $listDB->fetch()) {
        $arAbsenceType[$list['XML_ID']] = $list['ID'];
    }

// Заполняем массив типов отсутствий SALTO
    $listDB = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $IB_ID, 'CODE' => 'SALTO_TYPE'));
    while ($list = $listDB->fetch()) {
        $arAbsenceTypeSALTO[$list['XML_ID']] = $list['ID'];
    }

// Определяем тип импорта: промежуточный или окончательный
// Если дата текущая или более поздняя - то импорт промежуточный
// Если дата вчерашняя или раньше - импорт окончательный
    $Now = date('d.m.Y');
    $tsDate = MakeTimeStamp($Date, 'DD.MM.YYYY');
    $tsNow = MakeTimeStamp($Now, 'DD.MM.YYYY');
    if ($tsDate >= $tsNow){
        $FullImport = false;
        $logfile = __DIR__."/logs/import-".date('Y-m-d--H-i-s').".log";

    } else {
        $FullImport = true;
        $logfile = __DIR__."/logs/import-full-".date('Y-m-d').".log";
    }

// Создаем лог-файл
    $fh=fopen($logfile, "w");
    StringToLog($fh, "Начат импорт из MSSQL...");
    StringToLog($fh, "Стандартное рабочее время определно как: ".$defaultStart."-".$defaultEnd);
    if ($FullImport){
        StringToLog($fh, "Тип импорта: окончательный");
    } else {
        StringToLog($fh, "Тип импорта: промежуточный");
    }

// Инициализируем подключение к БД
    $dbh = GetDMSConnection();
    if ($dbh !== false) {

// Чистим данные
        if ($clear_table == true) {
            unset($Filter);
            $Filter['>=DATE_ACTIVE_FROM'] = $DB->FormatDate($Date . " 00:00:00", "DD.MM.YYYY HH:MI:SS", CSite::GetDateFormat('FULL'));
            $Filter['<=DATE_ACTIVE_FROM'] = $DB->FormatDate($Date . " 23:59:59", "DD.MM.YYYY HH:MI:SS", CSite::GetDateFormat('FULL'));
            $Filter['>=DATE_ACTIVE_TO'] = $DB->FormatDate($Date . " 00:00:00", "DD.MM.YYYY HH:MI:SS", CSite::GetDateFormat('FULL'));
            $Filter['<=DATE_ACTIVE_TO'] = $DB->FormatDate($Date . " 23:59:59", "DD.MM.YYYY HH:MI:SS", CSite::GetDateFormat('FULL'));

            $Filter['PROPERTY_SALTO_IMPORTED'] = "1";
            $ibListDB = CIBlockElement::GetList(array(), $Filter, false, false, array('ID'));
            while ($aribList = $ibListDB->fetch()) {
                CIBlockElement::Delete($aribList['ID']);
            }
        }

        $sql = "SELECT VIOLATION.user_info, violation_type_id, CONVERT(VARCHAR, viol_date, 104), CONVERT(VARCHAR, check_time, 108),
        comment, CONVERT(VARCHAR, compare_start, 108), CONVERT(VARCHAR, compare_end, 108), CONVERT(VARCHAR(36),VIOLATION.user_sid),
        is_first_check, is_last_check, violation_id, flag
        FROM VIOLATION LEFT JOIN TIME_RECORD ON VIOLATION.TIME_RECORD_ID=TIME_RECORD.TIME_RECORD_ID
        WHERE viol_date='" . $DateSQL . "'
        ORDER BY violation_id";
        $rs = mssql_query($sql, $dbh);
// 0 - User_info - оттуда вытаскиваем логин
// 1 - Тип нарушения
// 2 - Дата нарушения
// 3 - Время нарушения из таблицы TIME_RECORD (может быть пустым)
// 4 - Описание
// 5 - Время начала рабочего дня (предположительно)
// 6 - Время окончания рабочего дня (предположительно)
// 7 - user_sid - необходим для отбора отработанного времени
// 8 - is_first_check - Если 1 - то check_time - это время приходы сотрудника
// 9 - is_last_check - Если 1 - то check_time - это время ухода сотрудника
// 10 - violation_id - Идентификатор записи (необходим для двусторонней синхронизации)
// 11 - Состояние утверждения (НО!!! 1 - это в DMS не утверждено, а 0 - утверждено. Т.Е. НАОБОРОТ)

        $matches = array();

//echo($matches[1]);
//print_r($arUser);

//echo("rs:".$rs);
        StringToLog($fh, "SQL: " . mssql_get_last_message());
//print_r(error_get_last());
        while ($row = mssql_fetch_row($rs)) {
            $detail = '';
// Ищем элемент инфоблока с таким-же violation_id
// Если находим и импорт промежуточный, то просто переходим к следующей строке выборки
// Никаких обновлений не делаем. По логике с обновлением успешно справится SKD_import_updates
// Если же импорт окончательный, то все равно обрабатываем запись, т.к. надо корректно определить время ухода
            $absID = 0;
            if (!empty($row[10])) {
                $db_absense = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IB_ID, 'PROPERTY_VIOLATION_ID' => $row[10]));
                $ar_absense = $db_absense->GetNext();
                if (!empty($ar_absense)) {
                    $absID = $ar_absense['ID']; // Соханяем ID элемента, чтобы потом просто его обновить
                    StringToLog($fh, "Поиск элемента инфоблока с violation_id=".$row[10]."Элемент найден. Переходим к следующей строке.");
                    if ($FullImport === false) {
                        continue;
                    }
                } else {
                    StringToLog($fh, "Поиск элемента инфоблока с violation_id=".$row[10]."Элемент НЕ найден. Создаем элемент...");
                }
            }
//    print_r($row);
            $tmpStr = preg_match("#synergy\\\(.*)\s,(.*)#", $row[0], $matches);
            $userDB = CUser::GetByLogin($matches[1]);
            $arUser = $userDB->fetch();
// Если пользователь не найден, то добавляем его в список ненайденных пользователей
            if (empty($arUser)) {
                    StringToLog($fh, "Пользователь не найден: ".$row[0]);
                    $arNotFound[] = $row[0];
            } else {
// Поищем подразделения пользователя
// Проверим, что у пользователя есть подразделения
                if (empty($arUser['UF_DEPARTMENT'])) {
                    StringToLog($fh, "У пользователя не задано подразделение: ".$row[0]);
                    $arDepNotFound[] = $row[0];
                } else {
// Если есть пользователь и у него есть подразделение создаем запись "отсутствие"...
                    $viol_name = $row[4];
                    if (empty($viol_name)) {
                        $viol_name = "импорт из СКД";
                    }
                    $arProps['USER'] = array('VALUE' => $arUser['ID']);
                    $arProps['SALTO_IMPORTED'] = array('1');
                    $arProps['VIOLATION_ID'] = $row[10]; // Сохраняем VIOLATION_ID для последующей синхронизации
                    $arProps['SALTO_APPROVED'] = abs($row[11] - 1); // Сохраняем состояние утверждения из DMS (в DMS все наоборот, поэтому инвертируем значение)
// Определяем тип отсутствия ABSENCE_TYPE
// Первоначально было так:
//          $arProps['ABSENCE_TYPE'] = array('VALUE' => $absence_type_id);
// Теперь стало так:
                    switch ($row[1]) {
                        case 1:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_LATE']; // Опоздание
                            break;
                        case 2:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_LEAVING']; // Ранний уход
                            break;
                        case 3:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_ABSENT']; // Отсутствие на рабочем месте
                            break;
                        case 4:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_ILLNESS']; // Болезнь
                            break;
                        case 5:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_VACATION']; // Отпуск
                            break;
                        case 6:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_ASSIGNMENT']; // Командировка
                            break;
                        case 7:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_MEETING']; // Встреча
                            break;
                        case 8:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_OTHER']; // Другое
                            break;
                        case 9:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_HOMEWORK']; // Работа дома
                            break;
                    }
// Устанавливаем тип отсутствия SALTO
                    $arProps['SALTO_TYPE'] = $arAbsenceTypeSALTO["SALTO_" . $row[1]];

// Посмотрим график сотрудника
// Сначала смотрим что мы вытащили из MSSQL (SALTO)
                    $grStart = $row[5]; // Начало рабочего дня
                    $grEnd = $row[6]; // Окончание рабочего дня
// Если не удалось получить рабочее время из SQL, то устанавливаем сотруднику рабочее время "по-умолчанию"
                    if (empty($grStart)) {
                        $grStart = $defaultStart;
                    }
                    if (empty($grEnd)) {
                        $grEnd = $defaultEnd;
                    }

// Получаем начало и конец рабочего дня сотрудника по графику из инфоблока schedule
                    $worktime = GetScheduleWorkTime($arUser['ID'], $Date);
                    if (!empty($worktime['START_TIME'])) {
                        $grStartBX = $worktime['START_TIME'] . ":00";
                    }
                    if (!empty($worktime['END_TIME'])) {
                        $grEndBX = $worktime['END_TIME'] . ":00";
                    }

// Теперь сравним информацию из SQL и BX, чтобы при несовпадении занести информацию в лог
                    if (($grStartBX != $grStart) || ($grEndBX != $grEnd)){
                        $arTimeMismatch[] = $arUser['LAST_NAME']." ".$arUser['NAME']."(".$arUser['LOGIN']."): DMS -  ".$grStart."-".$grEnd.
                                          " | BX - ".$grStartBX."-".$grEndBX;
                    }

// Далее будем работать только с информацией из Битрикса (если не будет поддерживаться актуальность графиков,
// то просто надо закомментировать следующие 2 строки)
                    $grStart = $grStartBX;
                    $grEnd = $grEndBX;

// Добавляем график в комментариц
                    $detail = $detail . "; График: " . $DB->FormatDate($grStart, 'HH:MI:SS', 'HH:MI') . "-" . $DB->FormatDate($grEnd, 'HH:MI:SS', 'HH:MI');

// Выберем из БД время, отработанное сотрудником за день
                    $sql = "SELECT workminutes FROM WORKTIME WHERE (user_sid='" . $row[7] . "') and (workdate='" . $DateSQL . "')";
//        echo $sql."<br>";
                    $rs2 = mssql_query($sql, $dbh);
                    $wd_row = mssql_fetch_row($rs2);
                    if ($wd_row) {
                        $wdH = intval($wd_row[0] / 60); // Количество часов
                        if (strlen($wdH) < 2)
                            $wdH = '0' . $wdH;
                        $wdM = $wd_row[0] % 60; // Количество минут
                        if (strlen($wdM) < 2)
                            $wdM = '0' . $wdM;
                        $detail = $detail . "; Отработано: " . $wdH . ":" . $wdM . " ч.";
                    }
// Выберем из БД время фактического прихода и ухода
                    $sql = "SELECT CONVERT(VARCHAR, check_time, 108), is_first_check, is_last_check FROM TIME_RECORD WHERE (user_sid='" . $row[7] . "') and (CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, check_time)))='" . $DateSQL . "')";
                    $rs2 = mssql_query($sql, $dbh);
// 0 - дата/время записи
// 1 - флаг первой отметки
// 2 - флаг последней отметки
                    unset($trStart);
                    unset($trEnd);
                    while ($tr_row = mssql_fetch_row($rs2)) {
                        if ($tr_row[1] == 1) {
                            $trStart = $tr_row[0];
                        }
                        if ($tr_row[2] == 1) {
                            $trEnd = $tr_row[0];
                        }
                    }

// Анализируем типы нарушений и в зависимости от типа формируем время
                    switch ($row[1]) {
// Опоздание
                        case 1:
                            $violStart = $row[2] . " " . $row[5];
                            $violEnd = $row[2] . " " . $row[3];
                            $tmpReason = '"опоздание"';
                            break;
// Ранний уход
                        case 2:
                            $violStart = $row[2] . " " . $row[3];
                            $violEnd = $row[2] . " " . $row[6];
                            $tmpReason = '"ранний уход"';
                            break;
// Отсутствие на рабочем месте
                        case 3:
                            $violStart = $row[2];
                            $violEnd = $row[2];
                            $tmpReason = '"отсутствие"';
                            break;
// Все остальные случаи
                        default:
                            if ($row[1] == 4) {
                                $tmpReason = '"болезнь"';
                            } elseif ($row[1] == 5) {
                                $tmpReason = '"отпуск"';
                            } elseif ($row[1] == 6) {
                                $tmpReason = '"командировка"';
                            } elseif ($row[1] == 7) {
                                $tmpReason = '"встреча"';
                            } elseif ($row[1] == 8) {
                                $tmpReason = '"другое"';
                            } elseif ($row[1] == 9) {
                                $tmpReason = '"работает дома"';
                            }
// Анализируем тип записи в таблице TIME_RECORD (столбцы $row[8] и $row[9] и в зависмости от них устанавливаем время опоздания)
                            if ($row[8] == 1) {
                                $violStart = $row[2] . " " . $grStart;
                                $violEnd = $row[2] . " " . $trStart;
                            }
                            if ($row[9] == 1) {
                                $violStart = $row[2] . " " . $trEnd;
                                $violEnd = $row[2] . " " . $grEnd;
                            }
                            if (empty($row[3])) {
// Если запись в таблице TIME_RECORD отсутствует, то время начала и окончания отстутсвия выставляем целый день
                                $violStart = $row[2];
                                $violEnd = $row[2];
// Если надо устанавливать начало и окончание рабочего дня, то надо раскомментировать следующие строки
//                    $violStart = $row[2]." ".$grStart;
//                    $violEnd = $row[2]." ".$grEnd;
                            }
                            break;
                    }
                    if (isset($trStart) && isset($trEnd)) {
                        $detail = $detail . "(" . $DB->FormatDate($trStart, 'HH:MI:SS', 'HH:MI') . " - " . $DB->FormatDate($trEnd, 'HH:MI:SS', 'HH:MI') . ")";
                    }

                    $detail = "Причина: " . $tmpReason . $detail;
//            $detail = $detail . "User: " . $row[0];

// Добавляем элемент инфоблока
//                echo($arUser['LOGIN'] . "----" . $arUser['NAME'] . "-----" . $detail . "    ID: " . $arUser['ID'] . "     ");
// Если $absID = 0, значит такого элемента еще нет и надо добавить
                    if ($absID == 0) {
                        $el = new CIBlockElement;
                        $res = $el->Add(array('IBLOCK_ID' => $IB_ID, 'ACTIVE' => 'Y', 'ACTIVE_FROM' => $violStart,
                            'ACTIVE_TO' => $violEnd, 'NAME' => $viol_name,
                            'PROPERTY_VALUES' => $arProps, 'DETAIL_TEXT' => $detail));
                        if (!$res) {
                            StringToLog($fh, $arUser['LOGIN'] . "----" . $arUser['NAME'] . "-----" . $detail . "    ID: " . $arUser['ID'] . "     " . "Ошибка добавления. Error:  " . $el->LAST_ERROR);
                        } else {
                            StringToLog($fh, $arUser['LOGIN'] . "----" . $arUser['NAME'] . "-----" . $detail . "    ID: " . $arUser['ID'] . "     " . "Добавлено ОК!");
// Сообщение отправляем только сотрудникам IT-департамента. При внедрении убрать цикл и проверки
                            $MessageSend = false;
                            foreach ($arUser['UF_DEPARTMENT'] as $dep) {
//                                if (in_array($dep, [7, 2814, 2758, 2324, 2326, 2325])) {
                                if (in_array($dep, [4207, 4208, 4211, 4212, 4214, 4215, 4216, 4217, 4218, 4219, 4221, 4222, 4220, 4223, 4224, 4225, 4230, 4226, 4227, 4228, 4229, 4209, 4210, 4213])) {
                                    $NotifyRes = SendAbsenceIMMessage("Необходимо <a href='/company/absence.php'>добавить комментарий и подтвердить факт нарушения</a>.",
                                        "Зафиксировано нарушение трудовой дисциплины", $res);
                                    StringToLog($fh, "Отправка IM-сообщений: " . implode(' | ', $NotifyRes));
                                    $NotifyRes = AddAbsenceToLog("Необходимо добавить комментарий и подтвердить факт нарушения. Для этого пройдите по ссылке: <a href='/company/absence.php'>контроль трудовой дисциплины</a>",
                                        "Добавлена запись о нарушении трудовой дисциплины", $res);
                                    StringToLog($fh, "Отправка LiveFeed-сообщений: " . implode(' | ', $NotifyRes));
                                    $MessageSend = true;
                                    break;
                                }
                            }
                            if (!$MessageSend) {
                                StringToLog($fh, "Сообщение не отправляем. Подразделения: " . json_encode($arUser['UF_DEPARTMENT']));
                            }
// Окончание куска для beta-теста
                        }
                    } else {
                        $el = new CIBlockElement;
                        $res = $el->Update($absID, array('DETAIL_TEXT' => $detail));
                        if (!$res) {
                            StringToLog($fh, "Обновление... AbsenceID: ". $absID." Detail:" . $detail. ". Результат: ОШИБКА - " . $el->LAST_ERROR);
                        } else {
                            StringToLog($fh, "Обновление... AbsenceID: ". $absID." Detail:" . $detail. ". Результат: ОК");
                        }

                    }
                }
            }
        }
        StringToLog($fh, "--------------------------");
        StringToLog($fh, "");
        StringToLog($fh, "Не найдены следующие сотрудники:");
        foreach ($arNotFound as $el) {
            StringToLog($fh, $el);
        }

        StringToLog($fh, "--------------------------");
        StringToLog($fh, "");
        StringToLog($fh, "Не указаны подразделения:");
        foreach ($arDepNotFound as $el) {
            StringToLog($fh, $el);
        }

        StringToLog($fh, "--------------------------");
        StringToLog($fh, "");
        StringToLog($fh, "Несовпадение рабочего графика:");
        foreach ($arTimeMismatch as $el) {
            StringToLog($fh, $el);
        }


//$res = CIBlockElement::GetList(array(), array('IBLOCK_CODE' => 'absence'), false, false, array());
//while($ar = $res->fetch()){
//    print_r($ar);
//    echo("<br>Props:<br>");
//    $PropsDB = CIBlockElement::GetProperty($ar['IBLOCK_ID'], $ar['ID'], array(), array());
//    while($Props = $PropsDB->fetch()){
//        print_r($Props);
//        echo("<br><br>");
//    };
//
//}
    } else {
        StringToLog($fh, "Ошибка подключения к MSSQL: ".mssql_get_last_message());
    }
    $dbh = null;
    fclose($fh);
}
// --------------------------------------------------------------

function SKD_import_updates(){
    global $DB;
    $logfile = __DIR__."/logs/import-updates-".date('Y-m-d').".log";
// Ищем ID инфоблока по коду
    $iblock_code = 'absence';
    $ibDB = CIBLock::GetList(array(), array('CODE' => $iblock_code));
    $tmpIB = $ibDB->fetch();
    $IB_ID = $tmpIB['ID'];

// Заполняем массив типов отсутствий
    $listDB = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $IB_ID, 'CODE' => 'ABSENCE_TYPE'));
    while ($list = $listDB->fetch()) {
        $arAbsenceType[$list['XML_ID']] = $list['ID'];
    }

// Заполняем массив типов отсутствий SALTO
    $listDB = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $IB_ID, 'CODE' => 'SALTO_TYPE'));
    while ($list = $listDB->fetch()) {
        $arAbsenceTypeSALTO[$list['XML_ID']] = $list['ID'];
    }

// Создаем лог-файл
    $fh=fopen($logfile, "a");
    StringToLog($fh, "Начат импорт из MSSQL...");

// Инициализируем подключение к БД
    $dbh = GetDMSConnection();
    if ($dbh !== false) {
// Выбираем все изменившиеся записи
        $sql = "SELECT violation_id, CONVERT(VARCHAR, violation_date, 104)  FROM violation_updates ORDER BY update_id ASC";
        $rs = mssql_query($sql, $dbh);
        while ($row = mssql_fetch_row($rs)) {
            StringToLog($fh, "Начата обработка записи: violation_id=".$row[0]);
            $tmpid = $row[0];
            $tmpviol_date = $row[1];
            if (!empty($tmpid)) {
                $sql = "SELECT flag, violation_type_id, comment, user_info FROM VIOLATION WHERE violation_id=" . $tmpid;
                StringToLog($fh, "Поиск записи: SQL= ".$sql);
                $rs2 = mssql_query($sql, $dbh);
                $row2 = mssql_fetch_row($rs2);
                if (!empty($row2)){
// Инвертируем $tmpApproved, т.к. в DMS значения наоборот
                    $tmpApproved = abs($row2[0] - 1);
                    $tmpviolation_type_id = $row2[1];
                    $tmpcomment = $row2[2];
                    StringToLog($fh, "Поиск записи: ОК. Сотрудник: ".$row2[3]."| Дата нарушения: ".$tmpviol_date);
                    StringToLog($fh, "Поля: tmpApproved(инвертировано)=".$tmpApproved."| tmpviolation_type_id=".$tmpviolation_type_id.
                                     "| tmpcomment='".$tmpcomment."'");
                    if (empty($tmpcomment) || ($tmpcomment == '')){
                        $tmpcomment = "Импорт из СКД";
                        StringToLog($fh, "Комментарий в базе пустой. Присвоено значение '".$tmpcomment."'");
                    }
                    unset($arProps);
                    switch ($tmpviolation_type_id) {
                        case 1:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_LATE']; // Опоздание
                            $tmpReason = '"опоздание"';
                            break;
                        case 2:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_LEAVING']; // Ранний уход
                            $tmpReason = '"ранний уход"';
                            break;
                        case 3:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_ABSENT']; // Отсутствие на рабочем месте
                            $tmpReason = '"отсутствие"';
                            break;
                        case 4:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_ILLNESS']; // Болезнь
                            $tmpReason = '"болезнь"';
                            break;
                        case 5:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_VACATION']; // Отпуск
                            $tmpReason = '"отпуск"';
                            break;
                        case 6:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_ASSIGNMENT']; // Командировка
                            $tmpReason = '"командировка"';
                            break;
                        case 7:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_MEETING']; // Встреча
                            $tmpReason = '"встреча"';
                            break;
                        case 8:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_OTHER']; // Другое
                            $tmpReason = '"другое"';
                            break;
                        case 9:
                            $arProps['ABSENCE_TYPE'] = $arAbsenceType['SYN_HOMEWORK']; // Работа дома
                            $tmpReason = 'работа дома';
                            break;
                    }
                    $tmpReason = "Причина: " . $tmpReason . ";";
                    StringToLog($fh, "Определение причины: '".$tmpReason."'");
// Устанавливаем тип отсутствия SALTO
                    $arProps['SALTO_TYPE'] = $arAbsenceTypeSALTO["SALTO_" . $tmpviolation_type_id];
// Находим нужный нам инфоблок
                    $db_absense = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $IB_ID,'PROPERTY_VIOLATION_ID' => $tmpid));
                    $ar_absense = $db_absense->GetNext();
                    if (!empty($ar_absense)){
                        StringToLog($fh, "Поиск элемента инфоблока: ОК");
// Берем детальное описание
                        $detail = $ar_absense['~DETAIL_TEXT'];
                        StringToLog($fh, "~DETAIL_TEXT из инфоблока: ".$detail);
// Заменяем причину в описании
                        $detail = preg_replace("/^Причина:\s\".*\";/U", $tmpReason, $detail);
                        StringToLog($fh, "Новый DETAIL_TEXT: ".$detail);
                        $el = new CIBlockElement;
                        $updres = $el->Update($ar_absense['ID'], array('DETAIL_TEXT' => $detail, 'NAME' => $tmpcomment));
// Установим св-ва ABSENCE_TYPE
                        if ($updres) {
                            StringToLog($fh, "Обновление элемента инфоблока: ОК. Обновляем свойства.");
                            CIBlockElement::SetPropertyValuesEx($ar_absense['ID'], $IB_ID,
                                array("ABSENCE_TYPE" => $arProps['ABSENCE_TYPE'],
// SALTO_TYPE - не меняем - оставляем первоначально-импортированную информацию
// Если потребуется менять - надо раскомментровать следующуюс cтроку
//                                    "SALTO_TYPE" => $arProps['SALTO_TYPE'],
                                    "SALTO_APPROVED" => $tmpApproved));
                            StringToLog($fh, "Обновление свойств выполнено");
                            $sql = "DELETE FROM violation_updates WHERE violation_id=".$tmpid;
                            $rs3 = mssql_query($sql, $dbh);
                            if ($rs3 === false){
                                StringToLog($fh, "Удаление записи из violation_updates: ошибка. mssql сообщает- ".mssql_get_last_message());
                            } else{
                                StringToLog($fh, "Удаление записи из violation_updates: ОК.");

                            }
                        } else{
                            StringToLog($fh, "Обновление элемента инфоблока: ошибка. BITRIX сообщает- ".$el->LAST_ERROR);
                        }
                    } else {
// Если не нашли запись в битриксе пишем в лог
                        StringToLog($fh, "Поиск элемента инфоблока: ошибка. violation_id=". $tmpid);
                    }
                } else {
                    StringToLog($fh, "Поиск записи: ошибка. Запись не найдена в таблице. mssql сообщает - ".mssql_get_last_message());
                }
            }
            StringToLog($fh, "------------------------------------------");
            StringToLog($fh, ""); // Добавляем разделитель
        }
// Очистка таблицы violation_updates
// По идее в таблице на данный момент остались только те записи, которые не могут быть загружены в битрикс
// Т.е. можно чистить таблице полностью
// В целях отладки будем очищать только записи старше 3-х суток (чтобы можно было посмотреть, что тут остается
        $tmpDate = date('Y-m-d', time() - 86400 * 3);

        $sql = "DELETE FROM violation_updates WHERE violation_date <= '".$tmpDate."'";
        $rs3 = mssql_query($sql, $dbh);
        if ($rs3 !== false){
            StringToLog($fh, "Удаление устаревших записей (".$sql."): ОК");
        } else{
            StringToLog($fh, "Удаление устаревших записей (".$sql."): ошибка. mssql сообщает - ".mssql_get_last_message());
        }
    } else {
        StringToLog($fh, "Подключения к MSSQL: ошибка. mssql сообщает - ".mssql_get_last_message());
    }
    $dbh = null;
    fclose($fh);
}
// --------------------------------------------------------------

function SKD_import_worktime(){
    global $DB;
    $time_start = microtime(1); // Посчитаем время авполнения
    $logfile = __DIR__."/logs/import-worktime-".date('Y-m-d').".log";
    $Date = $_REQUEST['DATE'];
// Если дата пустая, то устанавливаем вчерашнюю дату
    if (empty($Date)) {
        $Date = date('d.m.Y', time() - 86400);
    }

// Создаем лог-файл
    $fh = fopen($logfile, "w");
    StringToLog($fh, "Начат импорт из MSSQL...");

// Ищем ID инфоблока по коду
    $iblock_code = 'worktime';
    $ibDB = CIBLock::GetList(array(), array('CODE' => $iblock_code));
    $tmpIB = $ibDB->fetch();
    if (!empty($tmpIB)) {
        $IB_ID = $tmpIB['ID'];
        StringToLog($fh, "Поиск инфоблока('".$iblock_code."'): ОК - ID: " . $IB_ID);
    } else {
        StringToLog($fh, "Поиск инфоблока('".$iblock_code."'): ОШИБКА - инфоблок не найден");
    }

// Проверим $Date
    if ($DB->isDate($Date, 'DD.MM.YYYY')) {
        $DateSQL = $DB->FormatDate($Date, 'DD.MM.YYYY', 'YYYY-MM-DD');
// Инициализируем подключение к БД
        $dbh = GetDMSConnection();
        if ($dbh !== false) {
// Получаем отработанные минуты и всех пользователей, зафиксированных в системе на этот день
            $sql = "SELECT USER_INFO, WORKMINUTES FROM worktime WHERE CONVERT(VARCHAR, workdate, 104)='".$Date."'";
            $rs = mssql_query($sql, $dbh);
            $i = 0; // Счетчик для режима тестирования
            $arNotFound = array(); // инициализируем массив для пользователей которые не найдены в битриксе
            while ($row = mssql_fetch_row($rs)) {
                preg_match("#^synergy\\\(.*)\s,(.*)#", $row[0], $matches);
                $StartTime = "";
                $EndTime = "";
                StringToLog($fh, "Начата обработка данных по сотруднику: ".$row[0]);
                if (!empty($matches[1])){
                    StringToLog($fh, "Определение логина: ОК - ".$matches[1]);
                    $dbUser = CUser::GetByLogin($matches[1]);
                    $arUser = $dbUser->fetch();
                    if (!empty($arUser)){
                        StringToLog($fh, "Определение пользователя: ОК - ID: ".$arUser['ID']);
// Получим время прихода и ухода
                        $sql = "SELECT user_info, is_first_check, is_last_check, CONVERT(VARCHAR, check_time, 104), CONVERT(VARCHAR, check_time, 108) FROM time_record where (check_time >='".$DateSQL." 00:00:00') and (check_time <='".$DateSQL." 23:59:59')".
                            " and (user_info='".$row[0]."') and ((is_first_check=1) or (is_last_check=1))";
// 0 - user_info
// 1 - is_first_check
// 2 - is_last_check
// Дата в формате dd.mm.yyyy
// Время в формате hh:mm:ss

                        $rs2 = mssql_query($sql, $dbh);
                        $row2 = mssql_fetch_row($rs2);
                        if ($row2) {
                            $j = 0;
                            do {
                                if ($row2[1] == 1) {
                                    $StartTime = $row2[3]." ".$row2[4];
                                }
                                if ($row2[2] == 1) {
                                    $EndTime = $row2[3]." ".$row2[4];
                                }
                                $j++;
                            } while ($row2 = mssql_fetch_row($rs2));
                            StringToLog($fh, "Поиск записей в таблице time_record: ОК - найдено ".$j." записей");
// Теперь полученную инфомрацию записываем в элемент инфоблока
// Сначала поищем такой же элемент инфоблока в базе по пользователю и дате
                            $arFilter['>=DATE_ACTIVE_FROM'] = $Date . " 00:00";
                            $arFilter['<=DATE_ACTIVE_FROM'] = $Date . " 23:59";
                            $arFilter['PROPERTY_USER'] = $arUser['ID'];
                            $arFilter['IBLOCK_ID'] = $IB_ID;
                            $dbElem = CIBlockElement::GetList(array(), $arFilter, false, false);
                            $arElem = $dbElem->fetch();
// Если элемент не найден - создаем
                            if (empty($arElem)) {
                                $ibElem = new CIblockElement;
                                $arFields['IBLOCK_ID'] = $IB_ID;
                                $arFields['NAME'] = $arUser['LAST_NAME'] . " " . $arUser['NAME'];
                                $arFields['ACTIVE_FROM'] = $StartTime;
                                $arFields['ACTIVE_TO'] = $EndTime;
// Получаем плановое время (по ГРАФИКУ) начала и окончания рабочего дня
                                $planTime = GetScheduleWorkTime($arUser['ID'], $Date."01:00:00"); //Для 100% корректной работы ф-ции добавим время
                                $planStart = $Date." ".$planTime['START_TIME'];
                                $planEnd = $Date." ".$planTime['END_TIME'];
                                $arFields['PROPERTY_VALUES'] = array('USER' => $arUser['ID'], 'PLAN_START' => $planStart, 'PLAN_END' => $planEnd);
                                $res = $ibElem->Add($arFields);
                                if ($res) {
                                    StringToLog($fh, "Добавление элементв инфоблока: ОК (" . $arFields['NAME'] . " Старт: " . $StartTime . " Конец: " . $EndTime . ")");
                                } else {
                                    StringToLog($fh, "Добавление элементв инфоблока: ОШИБКА (" . $ibElem->LAST_ERROR . ")");
                                }
                            } else {
                                StringToLog($fh, "Добавление элемента инфоблока: элемент уже найден. Обновление производиться не будет.");
                            }
                        } else {
                            StringToLog($fh, "Поиск записей в таблице time_record: ОШИБКА (записи НЕ найдены)");
                        }
                    } else {
                        StringToLog($fh, "Определение пользователя: ОШИБКА (пользователь не найден в Bitrix - ".$row[0]);
                        $arNotFound[] = $row[0];
                    }
                } else {
                    StringToLog($fh, "Определение логина: ОШИБКА. Поле user_info - ".$row[0]);
                }
                StringToLog($fh, "------------------------------------------");
                StringToLog($fh, ""); // Добавляем разделитель
// Ограничение для тестирования (в режиме отладки раскомментировать)
//                $i++;
//                if ($i>30)break;
            }
        } else {
            StringToLog($fh, "Подключения к MSSQL: ошибка. mssql сообщает - " . mssql_get_last_message());
        }
    } else {
        StringToLog($fh, "Ошибка определения даты - " . $Date);
    }
    $time_end = microtime(1); // Посчитаем время выполнения
    $time = $time_end - $time_start;
    StringToLog($fh, "------------------------------------------");
    StringToLog($fh, "------------------------------------------");
    StringToLog($fh, "Импорт завершен.");
    StringToLog($fh, "Время выполнения: ".number_format($time, 2)." сек.");
    StringToLog($fh, ""); // Добавляем разделитель

    if (!empty($arNotFound)) {
        StringToLog($fh, "Пользователи, не найденные в битрикс:");
        foreach ($arNotFound as $Elem) {
            StringToLog($fh, $Elem);
        }
    }
    $dbh = null;
    fclose($fh);
}
// --------------------------------------------------------------

function ShowADUsers(){
    $arrAttr = array("samaccountname",
                     "modifytimestamp",
                     "countrycode",
//                     "objectguid",
//                     "objectsid",
                     "whenchanged");
    CModule::IncludeModule("ldap");
    $tmpParams = GetADConnectionParams();
    $arLDAPSrv['SERVER'] = $tmpParams['HOST'];
    $arLDAPSrv['ADMIN_LOGIN'] = $tmpParams['USER'];
    $arLDAPSrv['ADMIN_PASSWORD'] = $tmpParams['PASS'];
    $arLDAPSrv['PORT'] = $tmpParams['PORT'];
    $arLDAPSrv['BASE_DN'] = $tmpParams['BASE_DN'];
    echo "<br>";
    $conn = ldap_connect($arLDAPSrv['SERVER'], $arLDAPSrv['PORT']);
    if ($conn !== false){
        ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($conn, LDAP_OPT_SIZELIMIT, 10000);
        ldap_set_option($conn, LDAP_OPT_TIMELIMIT, 100);
        ldap_set_option($conn, LDAP_OPT_TIMEOUT, 5);
        ldap_set_option($conn, LDAP_OPT_NETWORK_TIMEOUT, 5);
        echo "Подключились успешно <br>";
        $res = ldap_bind($conn, $arLDAPSrv['ADMIN_LOGIN'], $arLDAPSrv['ADMIN_PASSWORD']);
        if ($res){
            echo "Присоединились (bind) успешно <br>";
        } else {
            echo "Присоединение (bind) ошибка <br>";
        }
        $search = ldap_search($conn, $arLDAPSrv['BASE_DN'], "(&(objectClass=user)(objectCategory=PERSON))", $arrAttr);
//        $search = ldap_search($conn, $arLDAPSrv['BASE_DN'], '(samaccountname=mbochkov)', array("samaccountname", "extensionAttribute3"));
//        echo "search: ".$search;
        if ($search){
            $entries = ldap_get_entries($conn, $search);
            echo "Записи найдены: ".(count($entries) - 1)."<br>";
//            print_r($entries);
            echo "<br>";
            $i = 0;
            foreach($entries as $key=>$val){
//                echo $val['samaccountname']. -- $val['extensionAttribute4'][0]."<br>";
//                echo "Значение ключа: ".$key."<br>";
                if ($i > 0) {
                    foreach($arrAttr as $curAttr) {
                        if (isset($entries[$key][$curAttr][0])) {
                            if ($curAttr == "objectguid"){
                                $tmpVal = bin_to_str_guid($entries[$key][$curAttr][0]);
                            } elseif ($curAttr == "objectsid") {
                                $tmpVal = bin_to_str_sid($entries[$key][$curAttr][0]);
                            } else {
                                $tmpVal = $entries[$key][$curAttr][0];
                            }
                            echo $curAttr. ': ' . $tmpVal . " | ";
                        } else {
                            echo $curAttr. ': НЕ ЗАДАН | ';
                        }
                    }
                    echo "<br>";
                }
                $i++;
            }
        } else {
            echo "Ничего не нашли!<br>";
        }
    } else {
        echo "Ошибка подключения! <br>";
    }
}
// --------------------------------------------------------------

// Ф-ция экспортирует SaltoID всех пользователей в AD
// Экспорт идет в поле countrycode
// Если $prmEcho = true - то ф-ция выводит часть работы на экран
// Иначе на экран ничего не выводится и результат работы возвращается в виде массива
function ExportSaltoIDToAD($prmEcho = true){
    $logfile = __DIR__."/logs/export-AD-".date('Y-m-d').".log";
    $AttrName = "countrycode";
    CModule::IncludeModule("ldap");
// Берем подключение из настроек модуля LDAP (вообще нам из этих настроек нужен только BASE_DN
//    $dbSrv = CLdapServer::GetList();
//    $arLDAPSrv = $dbSrv->fetch();
// Основные настройки берем из элемента инфоблока "Настройки информационных систем"
    $tmpParams = GetADConnectionParams();
    $arLDAPSrv['SERVER'] = $tmpParams['HOST'];
    $arLDAPSrv['ADMIN_LOGIN'] = $tmpParams['USER'];
    $arLDAPSrv['ADMIN_PASSWORD'] = $tmpParams['PASS'];
    $arLDAPSrv['PORT'] = $tmpParams['PORT'];
    $arLDAPSrv['BASE_DN'] = $tmpParams['BASE_DN'];
// Создаем лог-файл
    $fh = fopen($logfile, "w");
    StringToLog($fh, "Начат экспорт SALTO-ID из битрикс в AD....");
    StringToLog($fh, "Сервер: ". $arLDAPSrv['SERVER']." Порт: ". $arLDAPSrv['PORT']." Пользователь: ". $arLDAPSrv['ADMIN_LOGIN']);
    StringToLog($fh, "");

    $conn = ldap_connect($arLDAPSrv['SERVER'], $arLDAPSrv['PORT']);
    if ($conn !== false){
        StringToLog($fh, "Подключение к AD: ОК");
        ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($conn, LDAP_OPT_SIZELIMIT, 10000);
        ldap_set_option($conn, LDAP_OPT_TIMELIMIT, 100);
        ldap_set_option($conn, LDAP_OPT_TIMEOUT, 5);
        ldap_set_option($conn, LDAP_OPT_NETWORK_TIMEOUT, 5);
//        $dn = $arLDAPSrv['BASE_DN'].','.'';
        $res = ldap_bind($conn, $arLDAPSrv['ADMIN_LOGIN'], $arLDAPSrv['ADMIN_PASSWORD']);
        if ($res){
            StringToLog($fh, "Bind: ОК");
            StringToLog($fh, "");
// Выбираем всех пользователей Bitrix с установленным SaltoID
            $dbEl = CIBlockElement::GetList(array(), array('!PROPERTY_SALTO_ID'=>false, 'IBLOCK_CODE'=>SCHEDULE_IBLOCK_CODE), false, false,
                array('ID', 'IBLOCK_ID', 'PROPERTY_USER', 'PROPERTY_SALTO_ID'));
            $counter = 0;
            $success = 0;
            $error = 0;
            while ($arEl=$dbEl->fetch()){
                $dbUser = CUser::GetByID($arEl['PROPERTY_USER_VALUE']);
                $arUser = $dbUser->fetch();
                StringToLog($fh, "Сотрудник: Логин - ".$arUser['LOGIN']." | SaltoID - ".$arEl['PROPERTY_SALTO_ID_VALUE']);
                $search = ldap_search($conn, $arLDAPSrv['BASE_DN'], '(samaccountname='.$arUser['LOGIN'].")", array("samaccountname", $AttrName));
                if ($search){
                    StringToLog($fh, "Поиск в AD: ОК");
                    $entries = ldap_get_entries($conn, $search);
//                    print_r($entries);
                    if (!empty($entries)){
                        StringToLog($fh, "get_entries: ОК");
                        StringToLog($fh, "samaccountname: ".$entries[0]['samaccountname'][0]);
                        StringToLog($fh, $AttrName.": ".$entries[0][$AttrName][0]);
                        $dn = $entries[0]['dn'];
                        StringToLog($fh, "DN: ".$dn);
                        if (!empty($arEl['PROPERTY_SALTO_ID_VALUE'])) {
// Сначала очищаем атрибут (устанавливаем нулевое значение), чтобы зафиксировалась обновление записи (необходимо для синхронизации с sharepoint
                            $res = ldap_mod_replace($conn, $dn, array($AttrName => 0));
                            if ($res){
                                StringToLog($fh, "Очистка атрибута AD: OK");
                            } else {
                                StringToLog($fh, "Очистка атрибута AD: ОШИБКА");
// Счетчик ошибок не увеличиваем, т.к. очистка атрибута - это вспомогательная операция
                            }
// Теперь устанавливаем значение атрибута
                            $res = ldap_mod_replace($conn, $dn, array($AttrName => $arEl['PROPERTY_SALTO_ID_VALUE']));
                            if ($res){
                                StringToLog($fh, "Установка атрибута AD: OK");
                                $success++;
                            } else {
                                StringToLog($fh, "Установка атрибута AD: ОШИБКА");
                                $error++;
                            }
                        }
                    } else {
                        StringToLog($fh, "get_entries: ОШИБКА");
                        $error++;
                    }
                } else {
                    StringToLog($fh, "Поиск в AD: ОШИБКА");
                    $error++;
                }
// Печатаем на экране имя обработанного пользователя
                if ($prmEcho){
                    echo $arUser['LOGIN']."<br>";
                }
                StringToLog($fh, "------------------------------------------");
                StringToLog($fh, ""); // Добавляем разделитель
                $counter++;
// Ограничение для режима тестирования
//                if ($counter >= 2){
//                    break;
//                }
            }
            $arResult['RESULT'] = 1;
            $arResult['MESSAGE'] = "Обработка завершена. Обработано ".$counter." записей. Обновлено: ".$success." Ошибок обновления: ".$error;
            ldap_close($conn);
        } else {
            StringToLog($fh, "Bind: ОШИБКА");
            $arResult['RESULT'] = -1;
            $arResult['ERROR'] = "Bind: ОШИБКА";
        }
    } else {
        StringToLog($fh, "Подключение к AD: ОШИБКА!");
        $arResult['RESULT'] = -1;
        $arResult['ERROR'] = "Подключение к AD: ОШИБКА!";
    }
    $arResult['LOGURL'] = "http://".$_SERVER['SERVER_NAME'].'/local/modules/intranet/tools/logs/export-AD-'.date('Y-m-d').".log";
    return $arResult;
}
// --------------------------------------------------------------

// Служебная ф-ция для конвертации guid
function bin_to_str_guid($object_guid) {
    $hex_guid = bin2hex($object_guid);
    $hex_guid_to_guid_str = '';
    for($k = 1; $k <= 4; ++$k) {
        $hex_guid_to_guid_str .= substr($hex_guid, 8 - 2 * $k, 2);
    }
    $hex_guid_to_guid_str .= '-';
    for($k = 1; $k <= 2; ++$k) {
        $hex_guid_to_guid_str .= substr($hex_guid, 12 - 2 * $k, 2);
    }
    $hex_guid_to_guid_str .= '-';
    for($k = 1; $k <= 2; ++$k) {
        $hex_guid_to_guid_str .= substr($hex_guid, 16 - 2 * $k, 2);
    }
    $hex_guid_to_guid_str .= '-' . substr($hex_guid, 16, 4);
    $hex_guid_to_guid_str .= '-' . substr($hex_guid, 20);

    return strtoupper($hex_guid_to_guid_str);
}
// --------------------------------------------------------------

// Служебная ф-ция для конвертации sid
function bin_to_str_sid($binsid) {
    $hex_sid = bin2hex($binsid);
    $rev = hexdec(substr($hex_sid, 0, 2));
    $subcount = hexdec(substr($hex_sid, 2, 2));
    $auth = hexdec(substr($hex_sid, 4, 12));
    $result    = "$rev-$auth";

    for ($x=0;$x < $subcount; $x++) {
        $subauth[$x] =
            hexdec(little_endian(substr($hex_sid, 16 + ($x * 8), 8)));
        $result .= "-" . $subauth[$x];
    }

    // Cheat by tacking on the S-
    return 'S-' . $result;
}
// --------------------------------------------------------------

// Converts a little-endian hex-number to one, that 'hexdec' can convert
function little_endian($hex) {
    for ($x = strlen($hex) - 2; $x >= 0; $x = $x - 2) {
        $result .= substr($hex, $x, 2);
    }
    return $result;
}