<?
// Переменная, которая определяет тип проверки прав доступа
// false - Удалять и редактировать может только непосредственный начальник
// true - Удалять и редактировать может начальник любого уровня
define('CHECKTYPE', true);
define('SCHEDULE_IBLOCK_CODE', 'schedule');
define('WORKTIME_IBLOCK_CODE', 'worktime');
define('ABSENCE_IBLOCK_CODE', 'absence');
define('ABSENCE_SETTINGS_IBLOCK_CODE', 'ABS_Settings');


// Ф-ция возвращает массив CUSER по ID пользователя
function GetUserByID($prmUser){
    $dbUser = CUser::GetList(($by="personal_country"), ($order="desc"), array('ID' => $prmUser));
    $arUser = $dbUser->fetch();
    if (!empty($arUser)) {
        return $arUser;
    } else {
        return false;
    }
}
//----------------------------------------------------------------------------------------------------------------------

// Ф-ция генерирует короткое ФИО с инициалами по полному ФИО (Иванов И.А.)
function GenerateShortFIO($prmFullFIO){
    $tmpFIO = explode(" ", $prmFullFIO);
    $f = $tmpFIO[0];
    if (!empty($tmpFIO[1])) {
        $i = strtoupper(substr($tmpFIO[1], 0, 1)).".";
    }
    if (!empty($tmpFIO[2])) {
        $o = strtoupper(substr($tmpFIO[2], 0, 1)).".";
    }
    return $f." ".$i." ".$o;
}
//----------------------------------------------------------------------------------------------------------------------

// Функция проверяет - может ли пользователь выполнять определенное действие над записью "Отсутствие"
// $prmAbsenceID - ID элемента инфоблока
// $prmUserID - ID пользователя, права которого требуется проверить
// $prmAction - одно из следующих действий
//   "delete" - удаление
//   "openeditform" - открывать форму редактирования
//   "edit" - редактирование ()
//   "approve" - утверждение
//   "unapprove" - отменить утверждение
//   "changeuser" - изменять пользователя в форме редактирования
//   "changedate" - изменять дату время отсутствия
function CanEdit($prmAbsenceID, $prmUserID, $prmAction)
{
    global $CheckType;
    global $USER;

// Все разрешено только админам контрля посещаемости и пользователю с ID = 1
    if ((IsAbsenceAdmin($prmUserID)) || ($prmUserID == 1)){
        return true;
    }
    $CheckUserID = $prmUserID;
    if (empty($CheckUserID)){
        $CheckUserID = $USER->GetID();
    }
// Находим нужный инфоблок
    $ibDB = CIBlockElement::GetList(array(), array('ID'=>$prmAbsenceID), false, false, array('PROPERTY_USER', 'PROPERTY_SALTO_IMPORTED', 'PROPERTY_SALTO_APPROVED'));
    $arIB = $ibDB->fetch();
    $result = false;
    switch($prmAction){
        case "openeditform":
// Может открывать форму редактирования импортированных записей в следующих случаях
// Сам сотрудник - если запись не утверждена
// Руководитель всегда
            if ($arIB['PROPERTY_SALTO_IMPORTED_VALUE']){
                if (($arIB['PROPERTY_USER_VALUE'] == $prmUserID) && (!$arIB['PROPERTY_SALTO_APPROVED_VALUE'])){
                    $result = true;
                } elseif (($arIB['PROPERTY_USER_VALUE'] == $prmUserID) && ($arIB['PROPERTY_SALTO_APPROVED_VALUE'])){
                    $result = false;
                } else {
                    $tmpUserID = $arIB['PROPERTY_USER_VALUE'];
//                    $UserDB = CUser::GetByID($tmpUserID);
//                    $arUser = $UserDB->fetch();
//                    $SubordinateUsersDB =  CIntranetUtils::GetSubordinateEmployees($CheckUserID, CHECKTYPE);
//                    $result = false;
//                    while($arSubordinateUsers = $SubordinateUsersDB->fetch()){
//                        if($arSubordinateUsers['ID'] == $arUser['ID']){
//                            $result = true;
//                            break;
//                        }
//                    }
                    $arSubordinateUsers = GetSubordinateEmployees_ex($CheckUserID, CHECKTYPE, false);
                    $result = in_array($tmpUserID, $arSubordinateUsers);
                }
            } else {
// Если запись не импортировано, то удаление будет разрешено только админам
// В Битриксе БЫЛА такая логика - потом можно будет изменить
                $result = false;
            }
            break;
        case "delete":
// Удалять могут только админы
            $result = false;
            break;
//            $tmpUserID = $arIB['PROPERTY_USER_VALUE'];
//            $UserDB = CUser::GetByID($tmpUserID);
//            $arUser = $UserDB->fetch();
//            $SubordinateUsersDB =  CIntranetUtils::GetSubordinateEmployees($CheckUserID, CHECKTYPE);
//            $result = false;
//            while($arSubordinateUsers = $SubordinateUsersDB->fetch()){
//                if($arSubordinateUsers['ID'] == $arUser['ID']){
//                    $result = true;
//                    break;
//                }
//            }
//            break;
        case "changeuser":
// Изменять пользователя может только админ
            $result = false;
            break;
        case "changedate":
// Изменять дату может только админ (пока что так сделаем)
            $result = false;
            break;
// Утверждать может только руководитель или его секретарь
        case "approve":
            $tmpUserID = $arIB['PROPERTY_USER_VALUE'];
//            $UserDB = CUser::GetByID($tmpUserID);
//            $arUser = $UserDB->fetch();
//            $SubordinateUsersDB =  CIntranetUtils::GetSubordinateEmployees($CheckUserID, CHECKTYPE);
//            $result = false;
//            while($arSubordinateUsers = $SubordinateUsersDB->fetch()){
//                if($arSubordinateUsers['ID'] == $arUser['ID']){
//                    $result = true;
//                    break;
//                }
//            }
            $arSubordinateUsers = GetSubordinateEmployees_ex($CheckUserID, CHECKTYPE, false);
            $result = in_array($tmpUserID, $arSubordinateUsers);
            break;
    }
    return $result;
}
//----------------------------------------------------------------------------------------------------------------------

// Временная фуекци-заменитель CanEdit
// После запуска в экплутацию - удалить
function CanEdit_ex($prmAbsenceID, $prmUserID, $prmAction)
{
    global $CheckType;
    global $USER;

// Все разрешено только админам контрля посещаемости и пользователю с ID = 1
    if ((IsAbsenceAdmin($prmUserID)) || ($prmUserID == 1)){
        return true;
    }
    $CheckUserID = $prmUserID;
    if (empty($CheckUserID)){
        $CheckUserID = $USER->GetID();
    }
// Находим нужный инфоблок
    $ibDB = CIBlockElement::GetList(array(), array('ID'=>$prmAbsenceID), false, false, array('PROPERTY_USER', 'PROPERTY_SALTO_IMPORTED', 'PROPERTY_SALTO_APPROVED'));
    $arIB = $ibDB->fetch();
    $result = false;
    switch($prmAction){
        case "openeditform":
// Может открывать форму редактирования импортированных записей в следующих случаях
// Сам сотрудник - если запись не утверждена
// Руководитель всегда
            if ($arIB['PROPERTY_SALTO_IMPORTED_VALUE']){
                if (($arIB['PROPERTY_USER_VALUE'] == $prmUserID) && (!$arIB['PROPERTY_SALTO_APPROVED_VALUE'])){
                    $result = true;
                } elseif (($arIB['PROPERTY_USER_VALUE'] == $prmUserID) && ($arIB['PROPERTY_SALTO_APPROVED_VALUE'])){
                    $result = false;
                } else {
                    $tmpUserID = $arIB['PROPERTY_USER_VALUE'];
//                    $UserDB = CUser::GetByID($tmpUserID);
//                    $arUser = $UserDB->fetch();
//                    $SubordinateUsersDB =  CIntranetUtils::GetSubordinateEmployees($CheckUserID, CHECKTYPE);
//                    $result = false;
//                    while($arSubordinateUsers = $SubordinateUsersDB->fetch()){
//                        if($arSubordinateUsers['ID'] == $arUser['ID']){
//                            $result = true;
//                            break;
//                        }
//                    }
                    $arSubordinateUsers = GetSubordinateEmployees_ex($CheckUserID, CHECKTYPE, false);
                    $result = in_array($tmpUserID, $arSubordinateUsers);
                }
            } else {
// Если запись не импортировано, то удаление будет разрешено только админам
// В Битриксе БЫЛА такая логика - потом можно будет изменить
                $result = false;
            }
            break;
        case "delete":
// Удалять могут только админы
            $result = false;
            break;
//            $tmpUserID = $arIB['PROPERTY_USER_VALUE'];
//            $UserDB = CUser::GetByID($tmpUserID);
//            $arUser = $UserDB->fetch();
//            $SubordinateUsersDB =  CIntranetUtils::GetSubordinateEmployees($CheckUserID, CHECKTYPE);
//            $result = false;
//            while($arSubordinateUsers = $SubordinateUsersDB->fetch()){
//                if($arSubordinateUsers['ID'] == $arUser['ID']){
//                    $result = true;
//                    break;
//                }
//            }
//            break;
        case "changeuser":
// Изменять пользователя может только админ
            $result = false;
            break;
        case "changedate":
// Изменять дату может только админ (пока что так сделаем)
            $result = false;
            break;
// Утверждать может только руководитель или его секретарь
        case "approve":
            $tmpUserID = $arIB['PROPERTY_USER_VALUE'];
//            $UserDB = CUser::GetByID($tmpUserID);
//            $arUser = $UserDB->fetch();
//            $SubordinateUsersDB =  CIntranetUtils::GetSubordinateEmployees($CheckUserID, CHECKTYPE);
//            $result = false;
//            while($arSubordinateUsers = $SubordinateUsersDB->fetch()){
//                if($arSubordinateUsers['ID'] == $arUser['ID']){
//                    $result = true;
//                    break;
//                }
//            }
            $arSubordinateUsers = GetSubordinateEmployees_ex($CheckUserID, CHECKTYPE, false);
            $result = in_array($tmpUserID, $arSubordinateUsers);
            break;
    }
    return $result;
}
//----------------------------------------------------------------------------------------------------------------------

// Ф-ция проверяет принадлежит ли пользователь к группе "Администраторы контроля посещаемости"
// $prmUserID - ID пользователя, которого требуется проверить
function isAbsenceAdmin($prmUserID)
{
    $result = false;
    $dbAdminGR = CGroup::GetList(($by = 'id'), ($order = 'asc'), array('STRING_ID' => 'ABSENCE_ADMIN'));
    $arAdminGR = $dbAdminGR->fetch();
    if (!empty($arAdminGR)){
        $arUserGR = CUser::GetUserGroup($prmUserID);
        $result = in_array($arAdminGR['ID'], $arUserGR);
    }
    return $result;
}
//----------------------------------------------------------------------------------------------------------------------

// Ф-ция получает настройки подключения к MSSQL (база DMS)
// Возвращает массив с ключами:
//   HOST
//   DBNAME
//   USER
//   PASS
function GetDMSConnectionParams(){
    $Filter['IBLOCK_CODE'] = 'IS_Settings';
    $Filter['CODE'] = 'IS_SETTINGS_DMS';
    $dbSettings = CIBlockElement::GetList(array(), $Filter, false, false, array('PROPERTY_HOST', 'PROPERTY_LOGIN', 'PROPERTY_PASS', 'PROPERTY_DB'));
    $arSettings = $dbSettings->fetch();
    if (!empty($arSettings)) {
        $arResult['HOST'] = $arSettings['PROPERTY_HOST_VALUE'];
        $arResult['DBNAME'] = $arSettings['PROPERTY_DB_VALUE'];
        $arResult['USER'] = $arSettings['PROPERTY_LOGIN_VALUE'];
        $arResult['PASS'] = $arSettings['PROPERTY_PASS_VALUE'];
        return $arResult;
    } else{
        return false;
    }
}
//----------------------------------------------------------------------------------------------------------------------

// Функция осуществляет подключение к БД и в случае удачного подключения возвращает указатель на БД
// Если подключение не удалось - возвращается false
function GetDMSConnection(){
// Получим настройки подключения к БД DMS
    $Filter['IBLOCK_CODE'] = 'IS_Settings';
    $Filter['CODE'] = 'IS_SETTINGS_DMS';
    $dbSettings = CIBlockElement::GetList(array(), $Filter, false, false, array('PROPERTY_HOST', 'PROPERTY_LOGIN', 'PROPERTY_PASS', 'PROPERTY_DB'));
    $arSettings = $dbSettings->fetch();
    if (!empty($arSettings)) {
        ini_set('mssql.charset', 'UTF-8');
        $dbh = mssql_connect($arSettings['PROPERTY_HOST_VALUE'], $arSettings['PROPERTY_LOGIN_VALUE'], $arSettings['PROPERTY_PASS_VALUE']);
        if ($dbh !== false) {
            $res = mssql_select_db($arSettings['PROPERTY_DB_VALUE'], $dbh);
            if ($res){
                $result = $dbh;
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }
    } else {
        $result = false;
    }
    return $result;
}
// --------------------------------------------------------------

// Ф-ция возвращает параметры подключения к AD
// Возвращает массив с ключами:
//   HOST
//   PORT
//   USER
//   PASS
//   BASE_DN - берем из свойства DB !!!!
function GetADConnectionParams(){
    $Filter['IBLOCK_CODE'] = 'IS_Settings';
    $Filter['CODE'] = 'IS_SETTINGS_AD';
    $dbSettings = CIBlockElement::GetList(array(), $Filter, false, false, array('PROPERTY_HOST', 'PROPERTY_LOGIN', 'PROPERTY_PASS', 'PROPERTY_PORT', 'PROPERTY_DB'));
    $arSettings = $dbSettings->fetch();
    if (!empty($arSettings)) {
        $arResult['HOST'] = $arSettings['PROPERTY_HOST_VALUE'];
        $arResult['USER'] = $arSettings['PROPERTY_LOGIN_VALUE'];
        $arResult['PASS'] = $arSettings['PROPERTY_PASS_VALUE'];
        $arResult['PORT'] = $arSettings['PROPERTY_PORT_VALUE'];
        $arResult['BASE_DN'] = $arSettings['PROPERTY_DB_VALUE'];
        return $arResult;
    } else{
        return false;
    }
}
//----------------------------------------------------------------------------------------------------------------------

// Функция осуществляет подключение к БД Sharepoint и в случае удачного подключения возвращает указатель на БД
// Если подключение не удалось - возвращается false
function GetSPConnection(){
// Получим настройки подключения к БД DMS
    $Filter['IBLOCK_CODE'] = 'IS_Settings';
    $Filter['CODE'] = 'IS_SETTINGS_SHAREPOINT';
    $dbSettings = CIBlockElement::GetList(array(), $Filter, false, false, array('PROPERTY_HOST', 'PROPERTY_LOGIN', 'PROPERTY_PASS', 'PROPERTY_DB'));
    $arSettings = $dbSettings->fetch();
    if (!empty($arSettings)) {
        ini_set('mssql.charset', 'UTF-8');
        $dbh = mssql_connect($arSettings['PROPERTY_HOST_VALUE'], $arSettings['PROPERTY_LOGIN_VALUE'], $arSettings['PROPERTY_PASS_VALUE']);
        if ($dbh !== false) {
            $res = mssql_select_db($arSettings['PROPERTY_DB_VALUE'], $dbh);
            if ($res){
                mysql_query("SET NAMES utf8", $dbh);
                $result = $dbh;
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }
    } else {
        $result = false;
    }
    return $result;
}
// --------------------------------------------------------------

// Функция осуществляет подключение к БД Salto и в случае удачного подключения возвращает указатель на БД
// Если подключение не удалось - возвращается false
function GetSaltoConnection(){
// Получим настройки подключения к БД Salto
    $Filter['IBLOCK_CODE'] = 'IS_Settings';
    $Filter['CODE'] = 'IS_SETTINGS_SALTO';
    $dbSettings = CIBlockElement::GetList(array(), $Filter, false, false, array('PROPERTY_HOST', 'PROPERTY_LOGIN', 'PROPERTY_PASS', 'PROPERTY_DB'));
    $arSettings = $dbSettings->fetch();
    if (!empty($arSettings)) {
        ini_set('mssql.charset', 'UTF-8');
        $dbh = mssql_connect($arSettings['PROPERTY_HOST_VALUE'], $arSettings['PROPERTY_LOGIN_VALUE'], $arSettings['PROPERTY_PASS_VALUE']);
        if ($dbh !== false) {
            $res = mssql_select_db($arSettings['PROPERTY_DB_VALUE'], $dbh);
            if ($res){
//                mysql_query("SET NAMES utf8", $dbh);
                $result = $dbh;
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }
    } else {
        $result = false;
    }
    return $result;
}
// --------------------------------------------------------------

// Ф-ция возвращает руководителя (ТОЛЬКО ОДНОГО) сотрудника, переданного в качестве параметра
// $prmUserID - сотрудник, у которого требуется найти руководителя
// Если сотрудник принадлежит нескольким подразделениям (и соответственно у него несколько руководителей), то будет
// возвращен первый найденный руководитель
// Результат - массив с полями CUSER
function GetUserManager($prmUserID){
    $ar_depts = CIntranetUtils::GetUserDepartments($prmUserID);
    if (!empty($ar_depts)) {
// Получаем руководителя подразделения, в котором работает сотрудник
        $tmp_manager = CIntranetUtils::GetDepartmentManager(array($ar_depts[0]));
//        echo "<br>";

        if (!empty($tmp_manager)) {
// Если руководитель подразделения не совпадает с сотрудником, то его и возвращаем
            $ar_manager = reset($tmp_manager);
            if ($ar_manager['ID'] != $prmUserID) {
                return $ar_manager;
            } else {
                $db_department = CIBlockSection::GetByID($ar_depts[0]);
                $ar_department = $db_department->GetNext();
                if (!empty($ar_department)) {
                    $tmp_manager = CIntranetUtils::GetDepartmentManager(array($ar_department['IBLOCK_SECTION_ID']));
                    $ar_manager = reset($tmp_manager);
                    return $ar_manager;
                }
            }
        } else{
// Если менеджер пустой, значит надо искать вышестоящее подразделение
            return false;
        }
    } else {
        return false;
    }
}
//----------------------------------------------------------------------------------------------------------------------

// Ф-ция возвращает заместителя сотрудника
// $prmUserID - сотрудник, чьего заместителя требуется вернуть
// Если заместитель найден, то возвращается массив с полями CUSER
// Если заместитель не установлен, возвращается false
function GetUserAssistant($prmUserID){
    $db_UserList = CUser::GetList(($by="ID"), ($order="asc"), array('ID'=>$prmUserID), array('SELECT'=>array('UF_USER_ASSISTANT')));
    $ar_UserList = $db_UserList->fetch();
    if ((!empty($ar_UserList)) && (!empty($ar_UserList['UF_USER_ASSISTANT']))){
        $db_UserAssistantList = CUser::GetList(($by="ID"), ($order="asc"), array('ID'=>$ar_UserList['UF_USER_ASSISTANT']));
        $ar_UserAssistantList = $db_UserAssistantList->fetch();
        if (!empty($ar_UserAssistantList)) {
            return $ar_UserAssistantList;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
//----------------------------------------------------------------------------------------------------------------------


// Функция обновляет соответствующую  запись в DMS
// $prmID - элемент инфоблока, соответствующую которому запись требуется обновить
// $prmIBlockID - ID инфоблока
function UpdateDMSRecotd($prmID, $prmIBlockID){
    global $USER, $DB;
    $logfile = __DIR__."/logs/import-updates-bitrix".date('Y-m-d').".log";

// Создаем лог-файл
    $fh=fopen($logfile, "a");
    StringToLog($fh, "Начато обновление записи в DMS");
    StringToLog($fh, "Текущий пользователь: ID-".$USER->GetId()." Login-".$USER->GetLogin());

    $db_element = CIBlockElement::GetList(array(), array("ID" => $prmID, "IBLOCK_ID" => $prmIBlockID), false, false,
        array("ID", "IBLOCK_ID", "NAME", 'DATE_ACTIVE_FROM', 'PROPERTY_USER', "PROPERTY_SALTO_APPROVED", "PROPERTY_ABSENCE_TYPE", "PROPERTY_VIOLATION_ID"));
    $ar_element = $db_element->fetch();
    if ((!empty($ar_element)) && (!empty($ar_element['PROPERTY_VIOLATION_ID_VALUE']))){
        StringToLog($fh, "GetList ОК: Элемент инфоблока найден: prmID-".$prmID." prmIBlockID-".$prmIBlockID);
// Запихиваем в лог информацию об отсутствии (нас далее будет интересовать сотрудник и дата)
        $tmpUserId = $ar_element['PROPERTY_USER_VALUE'];
        $tmpUserLogin_db = CUser::GetById($tmpUserId);
        $tmpUserLogin_ar = $tmpUserLogin_db->fetch();
        StringToLog($fh, "Данные элемента инфоблока: UserID-".$tmpUserId."| ФИО: ".$tmpUserLogin_ar['LAST_NAME']." ".$tmpUserLogin_ar['NAME'].
            "| Дата: ".$DB->FormatDate($ar_element['DATE_ACTIVE_FROM'], "DD.MM.YYYY HH:MI", "DD.MM.YYYY"));
        $dbh = GetDMSConnection();
        if ($dbh !== false){
            StringToLog($fh, "GetDMSConnection ОК: Подключение к базе DMS выполнено.");
            $arEnum = CIBlockPropertyEnum::GetByID($ar_element["PROPERTY_ABSENCE_TYPE_ENUM_ID"]);
            switch ($arEnum["XML_ID"]){
                case 'SYN_LATE':
                    $dmsViolType = 1; // опоздание
                    break;
                case 'SYN_LEAVING':
                    $dmsViolType = 2; // Ранний уход
                    break;
                case 'SYN_ABSENT':
                    $dmsViolType = 3; // отсутствие на рабочем месте
                    break;
                case 'SYN_ILLNESS':
                    $dmsViolType = 4; // болезнь
                    break;
                case 'SYN_VACATION':
                    $dmsViolType = 5; // отпуск
                    break;
                case 'SYN_ASSIGNMENT':
                    $dmsViolType = 6; // командировка
                    break;
                case 'SYN_MEETING':
                    $dmsViolType = 7; // встреча
                    break;
                case 'SYN_OTHER':
                    $dmsViolType = 8; // другое
                    break;
                case 'SYN_HOMEWORK':
                    $dmsViolType = 9; // работа дома
                    break;
            }
            StringToLog($fh, "CIBlockPropertyEnum::GetByID ОК: arEnum[XML_ID]-".$arEnum["XML_ID"]." dmsViolType-".$dmsViolType);
// Инвертируем флаг утверждения
            $dmsApproved = abs($ar_element["PROPERTY_SALTO_APPROVED_VALUE"] - 1);
// Сохраняем VIOLATION_ID
            $dmsViolID = $ar_element["PROPERTY_VIOLATION_ID_VALUE"];
// Обрабатываем описание (заменяем апострофы)
            $dmsComment = preg_replace("/'/", "''", $ar_element["NAME"]);
            StringToLog($fh, "Значение полей перед сохранением: dmsApproved-".$dmsApproved."| dmsComment-".$dmsComment.
                "| dmsViolType-".$dmsViolType."| dmsViolID-".$dmsViolID);
            $sql = "UPDATE violation SET flag=".$dmsApproved.", comment='".$dmsComment."', violation_type_id=".$dmsViolType.
            " WHERE violation_id=".$dmsViolID;
            StringToLog($fh, "Запрос: ".$sql);
            $res = mssql_query($sql, $dbh);
            if($res !== false){
                StringToLog($fh, "Update ОК: Запрос успешно выполнен!");
            } else{
                StringToLog($fh, "Update ошибка: ".mssql_get_last_message());
            }
        } else{
            StringToLog($fh, "GetDMSConnection ошибка: Подключение к базе DMS НЕ ВЫПОЛНЕНО: ".mssql_get_last_message());
        }
    } else {
        StringToLog($fh, "GetList ошибка: Элемент инфоблока НЕ НАЙДЕН: prmID-".$prmID." prmIBlockID-".$prmIBlockID);
    }
    StringToLog($fh, "------------------------------------------");
    StringToLog($fh, ""); // Добавляем разделитель
    $dbh = null;
}
//----------------------------------------------------------------------------------------------------------------------

// Ф-ция пролучает всех подчиненных пользователя, учитывая подразделения тех сотрудников
// для которых пользователь установлен как секретарь
// $prmUserID - пользователь для которого требуется найти подчиненных
// $prmRec - рекурсивно (т.е. всех подчиненных) или нет (только непосредственных)
// $prmIncludeUser - Включать самого пользователя в результат (по умолчанию true)
function GetSubordinateEmployees_ex($prmUserID, $prmRec, $prmIncludeUser = true){
    $arResult = array();
// Сначала добавляем в массив самого пользователя (если надо)
    if ($prmIncludeUser) {
        $arResult[] = $prmUserID;
    }

// Добавляем к результату всех подчиненных пользователя
    $db_Users = CIntranetUtils::GetSubordinateEmployees($prmUserID, $prmRec, "N");
    while($ar_User = $db_Users->fetch()){
        if (!in_array($ar_User['ID'], $arResult)){
            $arResult[] = $ar_User['ID'];
        }
    }

// Переходим к анализу сотрудников, у которых пользователь установлен как секретарь
    $db_UserList = CUser::GetList(($by="LAST_NAME"), ($order="asc"), array('UF_USER_ASSISTANT'=>$prmUserID), array("SELECT"=>array("ID")));
    while($ar_MainUser = $db_UserList->fetch()) {
// Для каждого найденного сотрудника выбираем всех подчиненных
        $db_Users = CIntranetUtils::GetSubordinateEmployees($ar_MainUser["ID"], $prmRec, "N");
        while($ar_User = $db_Users->fetch()){
            if (!in_array($ar_User['ID'], $arResult)){
                $arResult[] = $ar_User['ID'];
            }
        }
    }
    return $arResult;
}
//----------------------------------------------------------------------------------------------------------------------

function StringToLog($fh, $prmStr)
{
    if (!empty($fh)){
        fwrite($fh, date("d.m.Y H:i").' --- '.$prmStr."\n");
    }
}
// --------------------------------------------------------------

// Ф-ция определяет тип нарушения для визуализации через JS
// Всего есть три типа:
// 0 - ошибка при определении типа (не отображать)
// 1 - опоздание (показываем в первой половине ячейки)
// 2 - ранний уход (показываем во второй половине ячейки)
// 3 - отсутствие целый день (показывается во всей ячейке)

function GetVisualViolationTypeByAbsenceId($prmID, &$arDebug = false){
    global $DB;
// Получаем необходимый элемент инфоблока
    $dbAbsence = CIBlockElement::GetList(array(), array('ID'=>$prmID), false, false,
        array("ID", "IBLOCK_ID", "DATE_ACTIVE_FROM", "DATE_ACTIVE_TO", 'PROPERTY_USER', 'PROPERTY_ABSENCE_TYPE'));
    $arAbsence = $dbAbsence->fetch();
// Если элемент инфоблока найден, то продолжаем обработку
    if (!empty($arAbsence)){
// Сначала попытаемся определить на основании типа отсутствия (опоздание и ранний уход)
        $arEnum = CIBlockPropertyEnum::GetByID($arAbsence["PROPERTY_ABSENCE_TYPE_ENUM_ID"]);
        if ($arEnum["XML_ID"] == 'SYN_LATE'){
// опоздание
            $result = 1;
        } elseif ($arEnum["XML_ID"] == 'SYN_LEAVING'){
// Ранний уход
            $result = 2;
// Если причина другая, то будем определять по времени
        } else {
// Получаем рабочее время сотрудника по графику
            $WorkTime = GetScheduleWorkTime($arAbsence['PROPERTY_USER_VALUE'], $arAbsence['DATE_ACTIVE_FROM'], $arDebug);
            $StartWD = $WorkTime['START_TIME'];
            $EndWD = $WorkTime['END_TIME'];
//            $arDebug['START_TIME'] = $StartWD;
//            $arDebug['END_TIME'] = $EndWD;

// Извлекаем время начала и окончания нарушения
            $StartAbsence = $DB->FormatDate($arAbsence["DATE_ACTIVE_FROM"], CSite::GetDateFormat('FULL'), "HH:MI");
            $EndAbsence = $DB->FormatDate($arAbsence["DATE_ACTIVE_TO"], CSite::GetDateFormat('FULL'), "HH:MI");
// Определяем тип нарушения
// 1. Если начало нарушения совпадает с началом рабочего дня, а конец нарушения НЕ совпадает с концом рабочего дня
// значит это опоздание
            if (($StartAbsence == $StartWD) && ($EndAbsence != $EndWD)) {
                $result = 1;
            } elseif (($StartAbsence != $StartWD) && ($EndAbsence == $EndWD)) {
// 1. Если начало нарушения НЕ совпадает с началом рабочего дня, а конец нарушения совпадает с концом рабочего дня
// значит это ранний уход
                $result = 2;
            } else {
// Во всех остальных случаях определяем как отсутствие в течении всего дня
                $result = 3;
            }
        }
    } else{
// Если элемент инфоблока НЕ найден, то возвращаем 0
        $result = 0;
    }
    return $result;
}
// --------------------------------------------------------------

// Ф-ция возвращает массив, содержащий время начала и окончания рабочего дня для сотрудника ПО ГРАФИКУ за определенную дату
function GetScheduleWorkTime($prmUser, $prmDate, &$arDebug = false){
    global $DB;
    $dbSchedule = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>SCHEDULE_IBLOCK_CODE, 'PROPERTY_USER'=>$prmUser), false, false,
        Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_START_1", "PROPERTY_START_1", "PROPERTY_START_2", "PROPERTY_START_3",
            "PROPERTY_START_4", "PROPERTY_START_5", "PROPERTY_START_6", "PROPERTY_START_7",
            "PROPERTY_END_1", "PROPERTY_END_2", "PROPERTY_END_3", "PROPERTY_END_4", "PROPERTY_END_5", "PROPERTY_END_6", "PROPERTY_END_7",
            "PROPERTY_STD_SCHEDULE"));
    $arSchedule = $dbSchedule->fetch();
// Определяем день недели по дате
// Дата у нас в формате Битрикса
    $tmpTS = MakeTimeStamp($prmDate);
// date - возвращает цифру от 0 до 6 (0 - воскресенье), а нам надо от 1 до 7 (1 - понедельник)
    $WeekDay = date("w", $tmpTS);
    if ($WeekDay == 0)$WeekDay = 7;
    if (!empty($arSchedule)){
// Если установлен флаг "стандартный график", то получаем значения стандартного расписания
        if ($arSchedule['PROPERTY_STD_SCHEDULE_VALUE'] == 1){
            $WorkDay = GetStdWorkTime($prmDate);
            $StartWD = $WorkDay['START_TIME'];
            $EndWD = $WorkDay['END_TIME'];
        } else {
// Если НЕ установлен флаг "стандартный график", то извлекаем значения из полей
            $StartWD = $arSchedule['PROPERTY_START_' . $WeekDay.'_VALUE'];
            $EndWD = $arSchedule['PROPERTY_END_' . $WeekDay.'_VALUE'];
        }
    } else {
// Если не нашли элемент инфоблока, то получаем значения стандартного расписания
        $WorkDay = GetStdWorkTime($prmDate);
        $StartWD = $WorkDay['START_TIME'];
        $EndWD = $WorkDay['END_TIME'];
    }
//    $arDebug['STARTWD'] = $StartWD;
//    $arDebug['STARTWD2'] = $StartWD;
    return array('START_TIME'=>$StartWD, 'END_TIME'=>$EndWD);
}
// --------------------------------------------------------------

// Ф-цтя возвращает стандартный рабочий график (время начала и окончания рабочего дня)
// $prmDate - дата, на которую надо определить грфик
// TODO - здесь надо будет делать анализ производственного календаря
function GetStdWorkTime($prmDate){
    $tmpTS = MakeTimeStamp($prmDate);
    $WeekDay = date("w", $tmpTS);
    if (($WeekDay == 0) || ($WeekDay == 6)){
        $StartWD = "";
        $EndWD = "";
    } else {
        $StartWD = "09:00";
        $EndWD = "18:00";
    }
    return array('START_TIME'=>$StartWD, 'END_TIME'=>$EndWD);
}
// --------------------------------------------------------------

// Ф-ция возвращает время фактическое начала и окончания рабочего дня сотрудника, а также кол-во отработанных минут
// в виде массива со следующими индексами:
// START_TIME - начало рабочего дня
// END_TIME - окончание
// WORKTIME - количество отработанных минут
function GetWorkTime($prmUser, $prmDate){
    global $DB;
    $tmpDate = $DB->FormatDate($prmDate, CSite::GetDateFormat('FULL'), "DD.MM.YYYY");
    $arFilter['>=DATE_ACTIVE_FROM'] = $tmpDate . " 00:00";
    $arFilter['<=DATE_ACTIVE_FROM'] = $tmpDate . " 23:59";
    $arFilter['IBLOCK_CODE'] = WORKTIME_IBLOCK_CODE;
    $arFilter['PROPERTY_USER'] = $prmUser;
    $dbWorktime = CIBlockElement::GetList(array(), $arFilter, false, false,
        Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_USER", "DATE_ACTIVE_FROM", "DATE_ACTIVE_TO"));
    $arWorktime = $dbWorktime->fetch();
    if (!empty($arWorktime)){
        if ($DB->IsDate($arWorktime['DATE_ACTIVE_FROM'])) {
            $arResult['START_TIME'] = $arWorktime['DATE_ACTIVE_FROM'];
        } else {
            $arResult['START_TIME'] = "01.01.2000 00:00";
        }
        if ($DB->IsDate($arWorktime['DATE_ACTIVE_TO'])) {
            $arResult['END_TIME'] = $arWorktime['DATE_ACTIVE_TO'];
        } else {
            $arResult['END_TIME'] = "01.01.2000 00:00";
        }
        $tmpStart = MakeTimeStamp($arResult['START_TIME']);
        $tmpEnd = MakeTimeStamp($arResult['END_TIME']);
        $arResult['WORKTIME'] = round(($tmpEnd - $tmpStart) / 60);
    } else{
        $arResult['START_TIME'] = "01.01.2000 00:00";
        $arResult['END_TIME'] = "01.01.2000 00:00";
        $arResult['WORKTIME'] = 0;
    }
    return $arResult;

}
// --------------------------------------------------------------

// Ф-ция сохраняет в AD SaltoID пользователя
// $prmUser - ID пользователя, информацию по которому надо сохранить
// $prmSaltoID - Новый SaltoID
function SaveSaltoIDtoAD($prmUser, $prmSaltoID){
    global $USER, $DB;
    $logfile = __DIR__."/logs/export-user-AD.log";
    $AttrName = "countrycode";

// Основные настройки подключения к AD берем из элемента инфоблока "Настройки информационных систем"
    $tmpParams = GetADConnectionParams();
    $arLDAPSrv['SERVER'] = $tmpParams['HOST'];
    $arLDAPSrv['ADMIN_LOGIN'] = $tmpParams['USER'];
    $arLDAPSrv['ADMIN_PASSWORD'] = $tmpParams['PASS'];
    $arLDAPSrv['PORT'] = $tmpParams['PORT'];
    $arLDAPSrv['BASE_DN'] = $tmpParams['BASE_DN'];

// Создаем лог-файл
    $fh=fopen($logfile, "a");
    StringToLog($fh, "Начато обновление записи в AD");
    StringToLog($fh, "Сервер: ". $arLDAPSrv['SERVER']." Порт: ". $arLDAPSrv['PORT']." Пользователь: ". $arLDAPSrv['ADMIN_LOGIN']);
    StringToLog($fh, "Текущий пользователь: ID-".$USER->GetId()." Login-".$USER->GetLogin());
// Ищем пользователя
    $dbUser = CUser::GetById($prmUser);
    $arUser = $dbUser->fetch();
    if (!empty($arUser)) {
        StringToLog($fh, "Пользователь для обновления: ".$arUser['LOGIN']." ".$arUser['LAST_NAME']." ".$arUser['NAME']);
        StringToLog($fh, "Новое значение SaltoID: ".$prmSaltoID);
        $conn = ldap_connect($arLDAPSrv['SERVER'], $arLDAPSrv['PORT']);
        if ($conn !== false) {
            StringToLog($fh, "Подключение к AD: ОК");
            ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);
            ldap_set_option($conn, LDAP_OPT_SIZELIMIT, 10000);
            ldap_set_option($conn, LDAP_OPT_TIMELIMIT, 100);
            ldap_set_option($conn, LDAP_OPT_TIMEOUT, 5);
            ldap_set_option($conn, LDAP_OPT_NETWORK_TIMEOUT, 5);
//            $dn = $arLDAPSrv['BASE_DN'] . ',' . '';
            $res = ldap_bind($conn, $arLDAPSrv['ADMIN_LOGIN'], $arLDAPSrv['ADMIN_PASSWORD']);
            if ($res) {
                StringToLog($fh, "Bind: ОК");
                $search = ldap_search($conn, $arLDAPSrv['BASE_DN'], '(samaccountname='.$arUser['LOGIN'].")", array("samaccountname", $AttrName));
                if ($search) {
                    StringToLog($fh, "Поиск в AD: ОК");
                    $entries = ldap_get_entries($conn, $search);
                    if (!empty($entries)) {
                        StringToLog($fh, "get_entries: ОК");
                        StringToLog($fh, "samaccountname: " . $entries[0]['samaccountname'][0]);
                        StringToLog($fh, $AttrName.": " . $entries[0][$AttrName][0]);
                        $dn = $entries[0]['dn'];
                        StringToLog($fh, "DN: ".$dn);
                        if (!empty($prmSaltoID)) {
                            $res = ldap_mod_replace($conn, $dn, array($AttrName => $prmSaltoID));
                            if ($res){
                                StringToLog($fh, "Установка атрибута AD: OK");
                            } else {
                                StringToLog($fh, "Установка атрибута AD: ОШИБКА");
                            }
                        } else{
                            $res = ldap_mod_replace($conn, $dn, array($AttrName => 0));
                            if ($res){
                                StringToLog($fh, "Очистка атрибута AD: OK");
                            } else {
                                StringToLog($fh, "Очистка атрибута AD: ОШИБКА");
                            }
                        }
                    } else {
                        StringToLog($fh, "get_entries: ОШИБКА");
                    }
                } else {
                    StringToLog($fh, "Поиск в AD: ОШИБКА");
                }
                ldap_close($conn);
            } else {
                StringToLog($fh, "Bind: ОШИБКА");
            }
        } else {
            StringToLog($fh, "Подключение к AD: ОШИБКА!");
        }
    } else {
        StringToLog($fh, "Пользователь не найден. ID - ".$prmUser);
    }
    StringToLog($fh, "------------------------------------------");
    StringToLog($fh, ""); // Добавляем разделитель
    fclose($fh);
}
// --------------------------------------------------------------

// Ф-ция экспортирует график сотрудника в DMS в таблицу Person_table
// $prmUser - сотрудник, чей график необходимо экспортировать
// $prmFields - массив со значениями времени начала и окончания рабочего дня
// Если у сотрудника в битриксе установлен стандартный график, то в DMS все записи из таблицы будут удалены
function ExportPersonSchedule($prmUser, $prmFields){
    global $USER, $DB;
    $logfile = __DIR__."/logs/export-user-shedule.log";
    $weekdays = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];

// Создаем лог-файл
    $fh = fopen($logfile, "a");
    StringToLog($fh, "Начат экспорт расписания в DMS...");
    StringToLog($fh, "Текущий пользователь: ".$USER->GetLogin()."(".$USER->GetFullName().")");
    StringToLog($fh, "ID пользователя (кого редактируем): " . $prmUser);

    $dbUser = CUser::GetById($prmUser);
    $arUser = $dbUser->fetch();
    if (!empty($arUser)) {
        StringToLog($fh, "Пользователь (кого редактируем): " . $arUser['LAST_NAME']." ".$arUser['NAME']);
        $dbhDMS = GetDMSConnection();
        if ($dbhDMS !== false){
            $dbhSP = GetSPConnection();
            if ($dbhSP !== false){
// Ищем UserProfile_GUID
                $ntname = "synergy\\".$arUser['LOGIN'];
                $sql = "SELECT CONVERT(VARCHAR(36), userid), preferredname FROM UserProfile_Full WHERE ntname='".$ntname."'";
                $rs = mssql_query($sql, $dbhSP);
                if($rs !== false){
                    $row = mssql_fetch_row($rs);
                    if (!empty($row)) {
                        $guid = $row[0];
                        $userinfo = $row[1]." , " . $ntname;
//                        $sql = "INSERT INTO person_tabel ([user_sid], weekday_num) VALUES('".$guid."', 2)";
                        $sql = "DELETE FROM person_tabel WHERE [user_sid]='".$guid."'";
                        $res = mssql_query($sql, $dbhDMS);
                        if ($res !== false){
                            StringToLog($fh, "Удаление записей из person_tabel: OK");
// Если установлен стандартный график, то ничего не делаем (удалили записи и все...)
                            if ($prmFields['STD_SCHEDULE']['VALUE'] == 0) {
// Теперь в цикле выполним 7 запросов на добавление записей
                                for ($i = 1; $i <= 7; $i++) {
                                    StringToLog($fh, "Обработка дня недели: " . $i . " (" . $weekdays[$i - 1] . ")");
                                    if (!empty($prmFields['START_' . $i]) && !empty($prmFields['END_' . $i])) {
                                        $starttime = '1900-01-01 ' . $prmFields['START_' . $i];
                                        $endtime = '1900-01-01 ' . $prmFields['END_' . $i];
// Проверим установленное время на корректность
                                        if ($DB->IsDate($starttime, 'YYYY-MM-DD HH:MI:SS') && $DB->IsDate($endtime, 'YYYY-MM-DD HH:MI:SS')) {
                                            StringToLog($fh, "Проверка формата времени: OK");
                                            $sql = "INSERT INTO person_tabel ([user_sid], [user_info], [start_time], [end_time], [weekday_num], [is_not_work]) VALUES ('" .
                                                $guid . "', '" .
                                                $userinfo . "', '" .
                                                $starttime . "', '" .
                                                $endtime . "', " .
                                                $i . ", " .
                                                "0)";
                                        } else {
                                            StringToLog($fh, "Проверка формата времени: ОШИБКА. StartTime: '" . $starttime . "' EndTime: '" . $endtime . "'");
                                            continue;
                                        }
                                    } else {
                                        StringToLog($fh, "День недели определен как выходной");
                                        $sql = "INSERT INTO person_tabel ([user_sid], [user_info], [start_time], [end_time], [weekday_num], [is_not_work]) VALUES ('" .
                                            $guid . "', '" .
                                            $userinfo . "', NULL, NULL, " .
                                            $i . ", 1)";
                                    }
                                    $res = mssql_query($sql, $dbhDMS);
                                    if ($res !== false) {
                                        StringToLog($fh, "Добавление: OK");
                                    } else {
                                        StringToLog($fh, "Добавление: ОШИБКА. SQL-запрос - '" . $sql . "' Ошибка - " . mssql_get_last_message());
                                    }

                                }
                                StringToLog($fh, "Обработка закончена.");
                            } else {
                                StringToLog($fh, "Сотруднику установлен стандартный график. Обработка закончена.");
                            }
                        } else {
                            StringToLog($fh, "Удаление записей из person_tabel: ОШИБКА. SQL-запрос - '".$sql."' Ошибка - ".mssql_get_last_message());
                        }
                    } else {
                        StringToLog($fh, "Поиск GUID профиля: ОШИБКА. Профиль не найден. NTName - '".$ntname."'");
                    }

                } else {
                    StringToLog($fh, "Поиск GUID профиля: ОШИБКА. Ошибка при выполнении SELECT. NTName - '".$ntname."'");
                }
            } else {
                StringToLog($fh, "Ошибка подключения к SharePoint.");
            }
        } else {
            StringToLog($fh, "Ошибка подключения к DMS.");
        }
    } else {
        StringToLog($fh, "Пользователь не найден. ID - ".$prmUser);
    }
    StringToLog($fh, "------------------------------------------");
    StringToLog($fh, ""); // Добавляем разделитель
    fclose($fh);
}
// --------------------------------------------------------------

// Ф-ция возвращает массив с названиями лог-файлов по разделам
// IMPORTFULL - файлы типа import-full-2016-01-14.log - создаются ф-цией SKD_import_data. Содержат информацию о ходе импорта всех найденных за определенную дату отсутствий из DMS (окончательный импорт)
// IMPORT_UPD -  файлы типа import-updates-2015-12-01.log - создаются ф-цией SKD_import_updates. Содержат информацию о ходе импорта обновлений (когда запись редактируется в DMS)
// WORKTIME - файлы типа import-worktime-2015-12-11.log - создаются ф-цией SKD_import_worktime. Содержат информацию о ходе импорта фактически отработанного сотрудниками времени за определенную дату
// IDTOADALL - файлы типа export-AD-2015-12-18.log - создаются ф-цией ExportSaltoIDToAD. Содержат информацию о ходе экспорта SaltoID по ВСЕМ пользователям в AD (вообще это механизм разовый)
// UPDDMS - файлы типа import-updates-bitrix2015-12-01.log - создаются ф-цией UpdateDMSRecotd. Содержат информацию о ходе ЭКСПОРТА отдельных записей из битрикс в DMS (когда запись обновляется в BX и сразу уходит в DMS)
// IDTOAD - файлы типа export-user-AD.log - создаются ф-цией SaveSaltoIDtoAD. Содержат информацию о ходе ЭКСПОРТА SaltoID в AD при редактировании отдельной записи (настройки пользователя)
// SCHEDULE - файлы типа export-user-shedule.log - создаются ф-цией ExportPersonSchedule. Содержат информацию о ходе ЭКСПОРТА нестандартного рабочего графика отдельного сотрудника в DMS
function ShowDMSexchangeLogs(){
    $path = $_SERVER['DOCUMENT_ROOT'] . "/local/modules/intranet/tools/logs";
    $result['IMPORTFULL'] = glob($path."/import-full-????-??-??.log");
    $result['IMPORTINTERM'] = glob($path."/import-????-??-??*.log");
    $result['IMPORT_UPD'] = glob($path."/import-updates-????-??-??.log");
    $result['WORKTIME'] = glob($path."/import-worktime-????-??-??.log");
    $result['IDTOADALL'] = glob($path."/export-AD-????-??-??.log");
    $result['UPDDMS'] = glob($path."/import-updates-bitrix????-??-??.log");
    $result['IDTOAD'] = glob($path."/export-user-AD.log");
    $result['SCHEDULE'] = glob($path."/export-user-shedule.log");
    return $result;
}
// --------------------------------------------------------------

// Ф-ция определяет требуется ли определенному сотруднику отсылать оповещение определенного типа
// $prmUserID - пользователь
// $prmNotifyType - тип оповещения:
// 1 - о своем нарушении
// 2 - о нарушении подчиненного
// 3 - о нарушении контролируемого сотрудника (если пользователь секретарь его руководителя)
function GetUserAbsenceNotificationStatus($prmUserID, $prmNotifyType){
    $dbEl = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>SCHEDULE_IBLOCK_CODE, 'PROPERTY_USER'=>$prmUserID), false, false,
        array('ID', 'IBLOCK_ID', 'PROPERTY_DIS_NOTIFY_1', 'PROPERTY_DIS_NOTIFY_2', 'PROPERTY_DIS_NOTIFY_3', 'PROPERTY_DIS_ALL_NOTIFY'));
    $arEl = $dbEl->fetch();
    if (!empty($arEl)){
// Если элемент найден, то читаем свойства
        if ($arEl['PROPERTY_DIS_ALL_NOTIFY_VALUE'] != 0){
            return false;
        } else {
//            echo 'PROPERTY_DIS_NOTIFY_'.$prmNotifyType."_VALUE: ".$arEl['PROPERTY_DIS_NOTIFY_'.$prmNotifyType."_VALUE"]."<br>";
            if ($arEl['PROPERTY_DIS_NOTIFY_'.$prmNotifyType."_VALUE"] != 0){
                return false;
            } else {
                return true;
            }
        }
    } else {
// Если не найден, то разрешаем отправку уведомлений
        return true;
    }
}
// --------------------------------------------------------------

// Ф-ция возвращает массив, содержащий ID пользователя, его руководителя и заместителя руководителя
// Этот массив используется для рассылки уведомлений
function GetAbsenceNotifyRecipients($prmUserID){
    if (!empty($prmUserID)) {
        $tmpUsers = array();
        if (GetUserAbsenceNotificationStatus($prmUserID, 1)) {
            $tmpUsers[] = $prmUserID;
        }
        $tmpManager = GetUserManager($prmUserID);
//        echo "<br>Менеджер:".$tmpManager['LAST_NAME']."<br>";
        if (!empty($tmpManager)) {
            if (GetUserAbsenceNotificationStatus($tmpManager['ID'], 2)) {
                $tmpUsers[] = intval($tmpManager['ID']);
            }
            $tmpAssistant = GetUserAssistant($tmpManager['ID']);
//            echo "<br>Секретарь начальника:".$tmpAssistant['LAST_NAME']."<br>";
            if (!empty($tmpAssistant)) {
                if (GetUserAbsenceNotificationStatus($tmpAssistant['ID'], 3)) {
                    $tmpUsers[] = intval($tmpAssistant['ID']);
                }
            }
        }
        $tmpUsers = array_unique($tmpUsers);
        return $tmpUsers;
    } else {
        return array();
    }
}
// --------------------------------------------------------------

function SendAbsenceIMMessage($prmMessage, $prmTitle, $prmAbsenceID, $prmUsers = false){
    global $USER, $APPLICATION;
    CModule::IncludeModule('im');
    $dbAbsenceFields = CIBlockElement::GetList(array(), array('ID' => $prmAbsenceID), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_USER', 'DETAIL_TEXT'));
    $arAbsenceFields = $dbAbsenceFields->fetch();
    if (!empty($arAbsenceFields)) {
        if ($prmUsers !== false) {
            $NotyfyUsers = $prmUsers;
        } else {
            if (!empty($arAbsenceFields)) {
                $NotyfyUsers = GetAbsenceNotifyRecipients($arAbsenceFields['PROPERTY_USER_VALUE']);
            }
        }
        if (!empty($arAbsenceFields['PROPERTY_USER_VALUE'])) {
            $tmpUser = GetUserByID($arAbsenceFields['PROPERTY_USER_VALUE']);
            $message = "Сотрудник: " . $tmpUser['LAST_NAME'] . " " . $tmpUser['NAME'].". ".$prmMessage;
            if (!empty($NotyfyUsers)) {
                foreach ($NotyfyUsers as $user) {
                    $arFields = array(
                        "MESSAGE_TYPE" => "S", # P - private chat, G - group chat, S - notification
                        "TO_USER_ID" => $user,
                        "MESSAGE" => $message,
                        "FROM_USER_ID" => 1,
                        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,  # 1 - confirm, 2 - notify single from, 4 - notify single
                        "NOTIFY_MODULE" => "intranet", # module id sender (ex: xmpp, main, etc)
                        "NOTIFY_EVENT" => "IM_GROUP_ABSENCE", # module event id for search (ex, IM_GROUP_INVITE)
                        "NOTIFY_TITLE" => $prmTitle, # notify title to send email
                    );
// Блокируем дублирование получателя в письме, изменяя настройку главного модуля fill_to_mail
                    $tmpOpt = COption::GetOptionString('main', 'fill_to_mail');
                    COption::SetOptionString('main', 'fill_to_mail', 'N');
                    if (CIMMessenger::Add($arFields)){
                        $result[] = $user;
                    } else {
                        $result[] = 'ошибка: '.$user;
                    }
// Возвращаем настройку модуля
                    COption::SetOptionString('main', 'fill_to_mail', $tmpOpt);
                }
            } else {
                $result[] = 'Ошибка: NotyfyUsers - пустой.';
            }
        } else {
            $result[] = 'Ошибка: arAbsenceFields[PROPERTY_USER_VALUE] - пустой.';
        }
    } else {
        $result[] = 'Ошибка: Не найден элемент инфоблока: '.$prmAbsenceID;
    }
//    return json_encode($result);
    return $result;
}
// --------------------------------------------------------------

function AddAbsenceToLog($prmMessage, $prmTitle, $prmAbsenceID, $prmUsers = false)
{
    global $APPLICATION;
    $result = array();
    if (CModule::IncludeModule('socialnetwork') && CModule::IncludeModule('iblock'))
    {
        $arSoFields = Array(
            'ENTITY_TYPE' => ENTITY_TYPE,
            'ENTITY_ID' => $prmAbsenceID,
            'EVENT_ID' => EVENT_ID,
            'USER_ID' => 1,
            '=LOG_DATE' => $GLOBALS['DB']->CurrentTimeFunction(),
            'TITLE' => $prmTitle,
            'MESSAGE' => $prmMessage,
            'MODULE_ID' => 'intranet',
            'CALLBACK_FUNC' => false,
            'TMP_ID' => false,
//            'PARAMS' => serialize(array(
//                'ENTITY_NAME' => "Это у нас ENTITY_NAME",
//                'ENTITY_URL' => "/company/absence.php"
//            ))
        );
        $logID = CSocNetLog::Add($arSoFields, false);
        if (intval($logID) > 0) {
            $result[] = $logID;
            CSocNetLog::Update($logID, array("TMP_ID" => $logID));
            if ($prmUsers !== false){
                $NotyfyUsers = $prmUsers;
            } else {
                $dbAbsenceFields = CIBlockElement::GetList(array(), array('ID' => $prmAbsenceID), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_USER', 'DETAIL_TEXT'));
                $arAbsenceFields = $dbAbsenceFields->fetch();
                if (!empty($arAbsenceFields)){
                    $NotyfyUsers = GetAbsenceNotifyRecipients($arAbsenceFields['PROPERTY_USER_VALUE']);
                }
            }
            if (!empty($NotyfyUsers)){
                foreach ($NotyfyUsers as $user){
                    if (CSocNetLogRights::Add($logID, "U".$user)){
                        $result[] = $user;
                    } else {
                        $result[] = 'ошибка: '.$user;
                    }
                }
            }
            CSocNetLog::SendEvent($logID, "SONET_NEW_EVENT");
        } else {
            $result[] = 'Ошибка добавления: '.$APPLICATION->LAST_ERROR;
        }
    } else {
        $result[] = 'Не подключен модуль';
    }
//    return json_encode($result);
    return $result;
}
// --------------------------------------------------------------

// Ф-ция получает настройки для печатной формы служебной записки
// $prmSetting - название настройки (если не указано, то берется "main_setting"
// возвращаем массив с полями, если нашли элемент - иначе false
function GetMemoSettings($prmSetting = false){
    global $USER;
// Находим нужный инфоблок
    if ($prmSetting === false){
        $prmIBcode = 'main_setting';
    } else {
        $prmIBcode = $prmSetting;
    }
    $ibDB = CIBlockElement::GetList(array(), array('CODE'=>$prmIBcode, 'IBLOCK_CODE'=>ABSENCE_SETTINGS_IBLOCK_CODE), false, false, array('PROPERTY_MEMO_AUTHOR_CURRENT', 'PROPERTY_MEMO_AUTHOR',
            'PROPERTY_MEMO_AUTHOR_POSITION', 'PROPERTY_MEMO_APPROVE', 'PROPERTY_MEMO_APPROVE_POSITION', 'PROPERTY_MEMO_RECIPIENT', 'PROPERTY_MEMO_RECIPIENT_2'));
    $arIB = $ibDB->fetch();
    if (!empty($arIB)){
        if ($arIB['PROPERTY_MEMO_AUTHOR_CURRENT_VALUE'] == 1){
            $tmpUser = GetUserByID($USER->GetID());
            if ($tmpUser !== false){
                $result['MEMO_AUTHOR_FIO'] = GenerateShortFIO($tmpUser['LAST_NAME']." ".$tmpUser['NAME']." ".$tmpUser['SECOND_NAME']);
                $result['MEMO_AUTHOR_POSITION'] = $tmpUser['WORK_POSITION'];
            }
        } else {
            $result['MEMO_AUTHOR_FIO'] = $arIB['PROPERTY_MEMO_AUTHOR_VALUE'];
            $result['MEMO_AUTHOR_POSITION'] = $arIB['PROPERTY_MEMO_AUTHOR_POSITION_VALUE'];
        }
        $result['MEMO_APPROVE_FIO'] = $arIB['PROPERTY_MEMO_APPROVE_VALUE'];
        $result['MEMO_APPROVE_POSITION'] = $arIB['PROPERTY_MEMO_APPROVE_POSITION_VALUE'];

        $result['MEMO_RECIPIENT_FIO'] = $arIB['PROPERTY_MEMO_RECIPIENT_VALUE'];
        $result['MEMO_RECIPIENT_FIO_2'] = $arIB['PROPERTY_MEMO_RECIPIENT_2_VALUE'];
        return $result;
    } else {
        return false;
    }
}
// --------------------------------------------------------------

// Ф-ция "вытаскивает" из Salto время начала рабочего дня сотрудника
// $prmUserID - сотрудник
// $prmDate - дата, за которую надо получить информацию в формате DD.MM.YYYY HH:MM:SS
function GetWorkDayStartFromSalto($prmUserID, $prmDate){
    $dbh = GetSaltoConnection();
    $result = false;
    if ($dbh !== false){
// Получим SaltoID пользователя
        $dbSchedule = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>SCHEDULE_IBLOCK_CODE, 'PROPERTY_USER'=>$prmUserID), false, false,
            Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_SALTO_ID"));
        $arSchedule = $dbSchedule->fetch();
        if (!empty($arSchedule)){
            $salotoId = $arSchedule['PROPERTY_SALTO_ID_VALUE'];
            if (!empty($salotoId)) {
                $sql = "SELECT card_user_id, NCopy FROM vwPortalCards where id_user=".$salotoId;
                $res = mssql_query($sql, $dbh);
                if($res !== false) {
                    $row = mssql_fetch_row($res);
                    if (!empty($row)){
// Разберемся с датой
                        $ts = MakeTimeStamp($prmDate);
                        $datestart = date('Y-m-d', $ts)." 00:00:00";
                        $dateend = date('Y-m-d', $ts)." 23:59:59";
// Сформируем корректный фильтр по всем парам card_user_id - NCopy
                        do{
                            if (!empty($filter)) {
                                $filter = $filter . " or ((id_user=" . $row[0] . ") and (NCopy=" . $row[1] . "))";
                            } else{
                                $filter = "((id_user=" . $row[0] . ") and (NCopy=" . $row[1] . "))";
                            }
                        }while($row = mssql_fetch_row($res));
                        $filter = "(".$filter.")";
                        $sql = "SELECT CONVERT(VARCHAR, dt_Audit, 108) FROM tb_LockAuditTrail WHERE ".$filter.
                               " and (dt_Audit >'".$datestart."') and (dt_Audit <='".$dateend."') order by dt_Audit asc";
                        $res = mssql_query($sql, $dbh);
                        if ($res !== false){
                            $row = mssql_fetch_row($res);
                            if (!empty($row)){
                                $result = $row[0];
                            }
                        }
                    }
                }
            }
        }

    }
    return $result;
}
