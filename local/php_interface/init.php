<?
use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/classes.php");

require($_SERVER["DOCUMENT_ROOT"] . "/local/lib/autoload.php");

require($_SERVER["DOCUMENT_ROOT"] . "/local/modules/intranet/tools/skdimport.php");

require($_SERVER["DOCUMENT_ROOT"] . "/local/modules/intranet/tools/absenceloghandlers.php");

global $APPLICATION;
$path_site_temp = "/local/templates/bitrix24_old/";
//echo '<div style="display: none">'.$path_site_temp.'</div>';

// * Synergy custom
// Данная кастомизация осталась от Ромы
CJSCore::Init('jquery'); // Без этой строки не работает фильтр в CRM
$content = '<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MXN6WP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'GTM-MXN6WP\');</script>
<!-- End Google Tag Manager -->
';
Asset::getInstance()->addString($content, false, \Bitrix\Main\Page\AssetLocation::AFTER_JS);
Asset::getInstance()->addJs('/local/assets/call.js');
Asset::getInstance()->addCss('/local/assets/css/style.css');

AddEventHandler("main", "OnAfterUserAuthorize", "OnAfterUserAuthorizeHandler");

//Lead
AddEventHandler('crm', 'OnAfterCrmLeadAdd', ['\Synergy\Crm\Lead', 'onAfterCrmLeadAdd']);
AddEventHandler('crm', 'OnAfterCrmLeadUpdate', ['\Synergy\Crm\Lead', 'onAfterCrmLeadUpdate']);
AddEventHandler('crm', 'OnAfterCrmLeadDelete', ['\Synergy\Crm\Lead', 'onAfterCrmLeadDelete']);
AddEventHandler('crm', 'OnBeforeCrmLeadUpdate', 'UpdateLeadAssignedDate');

//Contact
AddEventHandler('crm', 'OnAfterCrmContactAdd', ['\Synergy\Crm\Contact', 'onAfterCrmContactAdd']);
AddEventHandler('crm', 'OnAfterCrmContactUpdate', ['\Synergy\Crm\Contact', 'onAfterCrmContactUpdate']);
AddEventHandler('crm', 'OnAfterCrmContactDelete', ['\Synergy\Crm\Contact', 'onAfterCrmContactDelete']);

//Deal
AddEventHandler('crm', 'OnAfterCrmDealAdd', ['\Synergy\Crm\Deal', 'onAfterCrmDealAdd']);
AddEventHandler('crm', 'OnAfterCrmDealUpdate', ['\Synergy\Crm\Deal', 'onAfterCrmDealUpdate']);
AddEventHandler('crm', 'OnAfterCrmDealDelete', ['\Synergy\Crm\Deal', 'onAfterCrmDealDelete']);

AddEventHandler('crm', 'OnBeforeCrmInvoiceAdd', 'SynergyInvoiceUpdate');
AddEventHandler('crm', 'OnBeforeCrmInvoiceUpdate', 'SynergyInvoiceUpdate');

//Meetings
AddEventHandler('meeting', 'OnBeforeMeetingUpdate', 'MeetingUpdate');

//Tasks
AddEventHandler('tasks', 'OnBeforeTaskAdd', 'TaskAdd');

//Page  load
AddEventHandler("main", "OnBeforeProlog", "OnBeforeProlog", 50);

function OnBeforeProlog(){
    Asset::getInstance()->addString("<script> BX.userId = ".$GLOBALS['USER']->GetID().";</script>", false, \Bitrix\Main\Page\AssetLocation::AFTER_JS);
}
//-----------------------------------------------------------------------------
function SKD_update_agent(){
    SKD_import_updates();
    return "SKD_update_agent();";
}
//-----------------------------------------------------------------------------

function SKD_import_agent(){
    SKD_import_data();
    return "SKD_import_agent();";
}
//-----------------------------------------------------------------------------

function SKD_import_agent_cur_day(){
    SKD_import_data(true);
    return "SKD_import_agent_cur_day();";
}
//-----------------------------------------------------------------------------

function SKD_worktime_agent(){
    SKD_import_worktime();
    return "SKD_worktime_agent();";
}
//-----------------------------------------------------------------------------

function TaskAdd(&$arFields){
    if ((!isset($arFields['STATUS'])) || ($arFields['STATUS'] == 2)){
        $arFields['STATUS'] = 1;
    }
}
//-----------------------------------------------------------------------------

function MeetingUpdate(&$arFields)
{
    global $USER;
    if ((!CModule::IncludeModule("meeting") || (!CModule::IncludeModule("tasks")))) {
        return false;
    }
    if (!empty($arFields['ID'])) {
        $mDB = CMeeting::GetByID($arFields['ID']);
        $Meeting = $mDB->fetch();
        if (($arFields['CURRENT_STATE'] == CMeeting::STATE_CLOSED) && ($Meeting['CURRENT_STATE'] != CMeeting::STATE_CLOSED)) {
            $miDB = CMeeting::GetItems($arFields['ID']);
            $fh= fopen('/home/bitrix/www/local/modules/intranet/tools/logs/MeetingUpdate'.time().'.log', 'a');
            while ($MeetingItem = $miDB->fetch()) {
                $taskuserid = 1;//$USER->GetId()
                if ($MeetingItem['TASK_ID']) {
                    try {
                        $tmpTask = CTaskItem::getInstance($MeetingItem['TASK_ID'], $taskuserid);
                        $allowedactions = $tmpTask->getAllowedActions(false);
                        $allowedactions_str = implode(" | ", $allowedactions);
                        StringToLog($fh, "Задача ID: ".$MeetingItem['TASK_ID'].". Права доступа: ".$allowedactions_str);
                        $tmpTask->update(array('UF_TSK_M_ITEM_SORT' => $MeetingItem['SORT']));
                        StringToLog($fh, date('y.m.Y H:i') . " - UPDATE ОК: ID - " . $MeetingItem['TASK_ID'] . "USER_ID = " . $taskuserid . " | Текущий пользователь системы: " . $USER->GetLogin());
                    } catch (Exception $e) {
                        StringToLog($fh, date('y.m.Y H:i') . " - Исключение при обработке задачи: ID - " . $MeetingItem['TASK_ID'] . " Exception: " . $e->getMessage() . "USER_ID = " . $taskuserid . " | Текущий пользователь системы: " . $USER->GetLogin());
                    }

                }
// Добавляем обработку всех задач, которые есть в списке првязанных задач
                $ItemTasks = CMeetingItem::GetTasks($MeetingItem['ITEM_ID']);
                foreach ($ItemTasks as $ItemTask){
                    try {
                        $Task = CTasks::GetByID($ItemTask, false, ['returnAsArray' => true]);
                        $tmpTask = CTaskItem::getInstance($Task['ID'], $taskuserid);
                        $allowedactions = $tmpTask->getAllowedActions(false);
                        $allowedactions_str = implode(" | ", $allowedactions);
                        StringToLog($fh, "Задача ID: ".$Task['ID'].". Права доступа: ".$allowedactions_str);
                        $tmpTask->update(array('UF_TSK_M_ITEM_SORT' => $MeetingItem['SORT']));
                        StringToLog($fh, date('y.m.Y H:i')." - UPDATE ОК: ID - ".$Task['ID']."USER_ID = ".$taskuserid." | Текущий пользователь системы: ".$USER->GetLogin());
                    } catch (Exception $e){
                        StringToLog($fh, date('y.m.Y H:i') . " - Исключение при обработке задачи: ID - " . $Task['ID'] . " Exception: " . $e->getMessage() . "USER_ID = " . $taskuserid . " | Текущий пользователь системы: " . $USER->GetLogin());
                    }

                }

            }
            fclose($fh);
        }
    }
    return true;
}
//-----------------------------------------------------------------------------

function SynergyInvoiceUpdate(&$arFields)
{
    global $DB;
    if (intval($arFields["UF_CONTACT_ID"]) != 0) {
        $contacts = array();
        $res = $DB->Query("select `VALUE`,`TYPE_ID` from b_crm_field_multi where ELEMENT_ID=" . $arFields["UF_CONTACT_ID"] . " AND (TYPE_ID='PHONE' OR TYPE_ID='EMAIL') AND ENTITY_ID='CONTACT'");
        while ($row = $res->Fetch()) {
            $contacts[$row["TYPE_ID"]][] = $row["VALUE"];
        }
        $arFields["UF_CRM_1444048570"] = join(", ", array_unique($contacts["PHONE"])); //телефон
        $arFields["UF_CRM_1444048582"] = join(", ", array_unique($contacts["EMAIL"])); //email
    }

    //AddMessage2Log($arFields);
}

function UpdateLeadAssignedDate(&$arFields)
{
    global $DB;

    if($arFields["STATUS_ID"]=="ASSIGNED" && intval($arFields["ID"])>0)
    {
        $res = $DB->Query("SELECT b_crm_lead.STATUS_ID as `STATUS_ID`, b_uts_crm_lead.".PROP_LEAD_ASSIGNED_DATE." as `ASSIGNED_DATE`
        FROM b_crm_lead LEFT JOIN b_uts_crm_lead ON b_crm_lead.ID = b_uts_crm_lead.VALUE_ID WHERE b_crm_lead.ID = ".$arFields["ID"]);
        if($row=$res->Fetch())
        {
            if($row["STATUS_ID"]=="NEW" && $row["ASSIGNED_DATE"]=="")
            {
                $arFields[PROP_LEAD_ASSIGNED_DATE]=ConvertTimeStamp(time(),"FULL");
            }
        }
    }


}


function o($array)
{

    global $USER;

    if ($USER->IsAdmin()) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }
}

function OnAfterUserAuthorizeHandler()
{

    // Запись авторизации Азаренко

    global $USER;

    if ($USER->GetId() == 459) {

        CModule::IncludeModule('iblock');

        $el = new CIBlockElement;

        $arParams = Array(
            "IBLOCK_ID" => 81,
            "NAME" => "Авторизация Азаренко",
            "PROPERTY_VALUES" => Array(
                "IP" => $_SERVER["HTTP_X_REAL_IP"]
            )
        );

        $el->Add($arParams, false, false);
    }


}
/*
 * Функция выводит в форме редактирования сущности crm multi_field поля.
 * Переопределяем её для обработки телефонов
 */
function __CrmFieldMultiEditRenderItem($item, $mnemonic, $typeID, $referenceData, $editorID)
{
    $itemID = isset($item['ID']) ? $item['ID'] : '';
    $itemVal = isset($item['VALUE']) ? $item['VALUE'] : '';
    ?>
    <div class="bx-crm-edit-fm-item">
    <input
        type="text" <? if ($typeID === 'PHONE'): ?> onkeypress='return event.charCode >= 48 && event.charCode <= 57' <? endif;?> class="bx-crm-edit-input"
        name="<?= htmlspecialcharsbx($mnemonic) ?>[<?= htmlspecialcharsbx($typeID) ?>][<?= htmlspecialcharsbx($itemID) ?>][VALUE]"
        value="<?= htmlspecialcharsbx($itemVal) ?>"><?
    echo SelectBoxFromArray(
        CUtil::JSEscape($mnemonic) . '[' . htmlspecialcharsbx($typeID) . '][' . htmlspecialcharsbx($itemID) . '][VALUE_TYPE]',
        $referenceData,
        isset($item['VALUE_TYPE']) ? $item['VALUE_TYPE'] : '',
        '',
        "class='bx-crm-edit-input bx-crm-edit-input-small'"
    );
    ?>
    <div class="delete-action"
         onclick="BX.CrmFieldMultiEditor.items['<?= CUtil::addslashes($editorID) ?>'].deleteItem('<?= CUtil::addslashes($itemID) ?>');"
         title="<?= GetMessage('CRM_STATUS_LIST_DELETE') ?>"></div>
    </div><?
}?>