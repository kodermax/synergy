<?
require($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/classes.php");
//define("LOG_FILENAME",$_SERVER["DOCUMENT_ROOT"]."/local/php_interface/init.log");

global $APPLICATION;

$path_site_temp = "/bitrix/templates/bitrix24/";
//echo '<div style="display: none">'.$path_site_temp.'</div>';

// * Synergy custom
// Данная кастомизация осталась от Ромы
CJSCore::Init('jquery'); // Без этой строки не работает фильтр в CRM
$APPLICATION->AddHeadScript($path_site_temp."js/jquery-ui-1.11.4.custom/jquery-ui.min.js", true);
$APPLICATION->SetAdditionalCSS($path_site_temp."js/jquery-ui-1.11.4.custom/jquery-ui.min.css", true);
$APPLICATION->AddHeadScript($path_site_temp."js/jscrollpane/jquery.jscrollpane.min.js", true);
$APPLICATION->SetAdditionalCSS($path_site_temp."js/jscrollpane/jquery.jscrollpane.css", true);
$APPLICATION->SetAdditionalCSS("/bitrix/css/synergy.css", true);
// Включает обработку звонков из Битрикс
$APPLICATION->AddHeadScript($path_site_temp."js/scripts.js", true);
// * End Synergy custom


function o($array){

	global $USER;

	if($USER->IsAdmin()){
		echo "<pre>";print_r($array);echo "</pre>";
	}
}

AddEventHandler("main", "OnAfterUserAuthorize", "OnAfterUserAuthorizeHandler");

function OnAfterUserAuthorizeHandler(){

	// Запись авторизации Азаренко

	global $USER;

	if($USER->GetId() == 459){

		CModule::IncludeModule('iblock');

		$el = new CIBlockElement;

		$arParams = Array(
			"IBLOCK_ID" => 81,
			"NAME" => "Авторизация Азаренко",
			"PROPERTY_VALUES" => Array(
				"IP" => $_SERVER["HTTP_X_REAL_IP"]
			)
		);

		$el->Add($arParams, false, false);
	}


}

// Обработчик лида при его создании

AddEventHandler('crm','OnAfterCrmLeadAdd','SynergyAddLead');

function SynergyAddLead(&$arFields){

	$email = mb_strtolower(trim($arFields["FM"]["EMAIL"]["n1"]["VALUE"]));
	$phone = preg_replace("/\D/","",$arFields["FM"]["PHONE"]["n1"]["VALUE"]);
	$land = $arFields[PROP_LEAD_LAND];

	$sbsArray = Array("SBS", "Школа Бизнеса");

	/* Распределение для Школы Бизнеса */
	if($arFields["ID"] > 0 && $arFields["ASSIGNED_BY_ID"] == USER_ID_LANDER && in_array($arFields["SOURCE_DESCRIPTION"], $sbsArray) == 1){
		if(strlen($email) > 0){
			$arContact = SynergyContact::findContactByEmail($email, $arFields["ID"], "Школа Бизнеса");
			$assignedId = $arContact[0]["ASSIGNED_BY_ID"];
		}
		if($arContact != "error"){
			if ($assignedId > 0) {
				SynergyLead::leadChangeAssigned($arFields["ID"], $assignedId);
			}
		}
	}


	/* Распределение для Университета */
	if($arFields["ID"] > 0 && $arFields["ASSIGNED_BY_ID"] == USER_ID_LANDER && $arFields["SOURCE_DESCRIPTION"] == "Университет"){

		$finish = false;

		if(strlen($phone) > 0){
			$arContact = SynergyContact::findContactByPhone($phone, $arFields["ID"]);
			if($arContact != "error"){
				$assignedId = $arContact[0]["ASSIGNED_BY_ID"];
				if ($assignedId > 0) {
					SynergyLead::leadChangeAssigned($arFields["ID"], $assignedId);
					$finish = true;
				}

			}
		}

		if($finish == false && strlen($email) > 0){
			$arContact = SynergyContact::findContactByEmail($email, $arFields["ID"]);
			if($arContact != "error"){
				$assignedId = $arContact[0]["ASSIGNED_BY_ID"];
				if ($assignedId > 0) {
					SynergyLead::leadChangeAssigned($arFields["ID"], $assignedId);
					$finish = true;
				}
			}
		}

		if($finish == false && strlen($phone) > 0){
			$arLead = SynergyLead::findLeadByPhone($phone, $arFields["ID"], "Университет");
			if($arLead != "error"){
				SynergyLead::assignParentLead($arLead[0]["ID"], $arFields["ID"]);
				$finish = true;
			}
		}

		if($finish == false && strlen($email) > 0){
			$arLead = SynergyLead::findLeadByEmail($email, $arFields["ID"], "Университет");
			if($arLead != "error"){
				SynergyLead::assignParentLead($arLead[0]["ID"], $arFields["ID"]);
			}
		}

	}

	if($arFields["ID"] > 0){

		if(strlen($phone) > 0){
			$arLead = SynergyLead::findLeadByPhoneToday($arFields["ID"], $phone, $land);

			if($arLead != "error"){
				SynergyLead::leadChangeStatus($arFields["ID"], 5);
			}else{
				$arLeadDetails = SynergyLead::findLeadByPhoneToday($arFields["ID"], $phone);
				if($arLeadDetails != "error"){
					SynergyLead::leadChangeStatus($arFields["ID"], "DETAILS");
				}
			}
		}

	}

	if(isset($arFields["ASSIGNED_BY_ID"]) == 1 && $arFields["ASSIGNED_BY_ID"] != USER_ID_LANDER){

		global $USER;

		$objUser = $USER->GetById($arFields["ASSIGNED_BY_ID"]);
		if($userParams = $objUser->Fetch()){
			SynergyLead::changeLeadDepartment($arFields["ID"], $userParams["UF_DEPARTMENT"][0]);
		}
	}

}


AddEventHandler('crm', 'OnAfterCrmContactAdd', 'SynergyContactAdd');

function SynergyContactAdd(&$arFields){
	if(isset($arFields["ASSIGNED_BY_ID"]) == 1 && $arFields["ASSIGNED_BY_ID"] != USER_ID_LANDER){
		global $USER;

		$objUser = $USER->GetById($arFields["ASSIGNED_BY_ID"]);
		if($userParams = $objUser->Fetch()){
			SynergyContact::changeContactDepartment($arFields["ID"], $userParams["UF_DEPARTMENT"][0]);
		}
	}
}



AddEventHandler('crm', 'OnAfterCrmContactUpdate', 'SynergyContactUpdate');

function SynergyContactUpdate(&$arFields){
	if(isset($arFields["ASSIGNED_BY_ID"]) == 1 && $arFields["ASSIGNED_BY_ID"] != USER_ID_LANDER){
		global $USER;

		$objUser = $USER->GetById($arFields["ASSIGNED_BY_ID"]);
		if($userParams = $objUser->Fetch()){

			if(in_array(5, $userParams["UF_DEPARTMENT"]) == 1){
				SynergyContact::changeContactSource($arFields["ID"]);
			}

			SynergyContact::changeContactDepartment($arFields["ID"], $userParams["UF_DEPARTMENT"][0]);
		}
	}
}



AddEventHandler('crm', 'OnAfterCrmLeadUpdate', 'SynergyLeadUpdate');

function SynergyLeadUpdate(&$arFields){

	if(isset($arFields["ASSIGNED_BY_ID"]) == 1 && $arFields["ASSIGNED_BY_ID"] != USER_ID_LANDER){
		global $USER;

		$objUser = $USER->GetById($arFields["ASSIGNED_BY_ID"]);
		if($userParams = $objUser->Fetch()){
			SynergyLead::changeLeadDepartment($arFields["ID"], $userParams["UF_DEPARTMENT"][0]);
		}
	}

	if($arFields["STATUS_ID"] == "CONVERTED"){

		$arLead = SynergyLead::getSynergyLeadById($arFields["ID"]);
		$arDeal = SynergyDeal::getSynergyDealByLead($arFields["ID"]);
		$arContact = SynergyContact::getSynergyContactByLead($arFields["ID"]);

		CModule::IncludeModule('crm');

		if(isset($arDeal)){
			$deal = new CCrmDeal;

			$arDealUpdate[PROP_DEAL_PAGEFROM] = $arLead[PROP_LEAD_PAGEFROM];
			$arDealUpdate[PROP_DEAL_LAND] = $arLead[PROP_LEAD_LAND];
			$arDealUpdate[PROP_DEAL_SOURCE] = $arLead[PROP_LEAD_SOURCE];
			$arDealUpdate[PROP_DEAL_CAMPANY] = $arLead[PROP_LEAD_CAMPANY];
			$arDealUpdate[PROP_DEAL_MEDIUM] = $arLead[PROP_LEAD_MEDIUM];
			$arDealUpdate[PROP_DEAL_TERM] = $arLead[PROP_LEAD_TERM];
			$arDealUpdate[PROP_DEAL_REFER] = $arLead[PROP_LEAD_REFER];
			$arDealUpdate[PROP_DEAL_IP] = $arLead[PROP_LEAD_IP];
			$arDealUpdate[PROP_DEAL_UTC] = $arLead[PROP_LEAD_UTC];
			$arDealUpdate[PROP_DEAL_COUNTRY] = $arLead[PROP_LEAD_COUNTRY];
			$arDealUpdate[PROP_DEAL_CITY] = $arLead[PROP_LEAD_CITY];


			$arDealUpdate[PROP_DEAL_BALLHISTORY] = $arLead[PROP_LEAD_BALLHISTORY];
			$arDealUpdate[PROP_DEAL_BALLRUSSIAN] = $arLead[PROP_LEAD_BALLRUSSIAN];
			$arDealUpdate[PROP_DEAL_BALLMATH] = $arLead[PROP_LEAD_BALLMATH];
			$arDealUpdate[PROP_DEAL_BALLOBSHTESTV] = $arLead[PROP_LEAD_BALLOBSHTESTV];
			$arDealUpdate[PROP_DEAL_SERTIFICAT] = $arLead[PROP_LEAD_SERTIFICAT];
			$arDealUpdate[PROP_DEAL_DATE_ISSUE_SERTIFICAT] = $arLead[PROP_LEAD_DATE_ISSUE_SERTIFICAT];

			$deal->Update($arDeal["ID"], $arDealUpdate);
		}

		if($arContact["ID"] > 0 && strlen($arLead["SOURCE_DESCRIPTION"]) > 0){
			$contact = new CCrmContact;

			$arContactUpdate[PROP_CONTACT_SOURCE_DESCRIPTION] = $arLead["SOURCE_DESCRIPTION"];
			$arContactUpdate[PROP_CONTACT_CITY] = $arLead[PROP_LEAD_CITY];

			$contact->Update($arContact["ID"], $arContactUpdate);
		}

	}

}

AddEventHandler('crm', 'OnBeforeCrmInvoiceAdd', 'SynergyInvoiceUpdate');
AddEventHandler('crm', 'OnBeforeCrmInvoiceUpdate', 'SynergyInvoiceUpdate');

function SynergyInvoiceUpdate(&$arFields)
{
    global $DB;
    if(intval($arFields["UF_CONTACT_ID"])!=0)
    {
        $contacts = array();
        $res = $DB->Query("select `VALUE`,`TYPE_ID` from b_crm_field_multi where ELEMENT_ID=".$arFields["UF_CONTACT_ID"]." AND (TYPE_ID='PHONE' OR TYPE_ID='EMAIL') AND ENTITY_ID='CONTACT'");
        while($row = $res->Fetch())
        {
            $contacts[$row["TYPE_ID"]][]=$row["VALUE"];
        }
        $arFields["UF_CRM_1444048570"]=join(", ",array_unique($contacts["PHONE"])); //телефон
        $arFields["UF_CRM_1444048582"]=join(", ",array_unique($contacts["EMAIL"])); //email
    }

	//AddMessage2Log($arFields);
}
/** Пытались улучшить производительность. Пока не нужно
AddEventHandler('main','OnAfterEpilog','WriteTime');
function WriteTime()
{
$file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/timelog.txt';
if (filesize($file)>1024*1024*10) // Очистка если больше 1Мб
fclose(fopen($file,'w'));
file_put_contents($file, (microtime(1) - PAGE_START_TIME)."\t".$_SERVER['REQUEST_URI']."\n", FILE_APPEND | LOCK_EX);
}
 **/
?>