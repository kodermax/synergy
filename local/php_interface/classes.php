<?
require($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/defines.php");
//define("LOG_FILENAME",$_SERVER["DOCUMENT_ROOT"]."/crm/lead/search.log");
/*
created by
Sapozhnikov Roman
*/

class SynergyLead
{
    function __construct()
    {
        CModule::IncludeModule('crm');
    }

    /* Возвращает параметры лида по его Id */
    public function getSynergyLeadById($leadId)
    {
        $arFilter = Array(
            "ID" => $leadId,
        );
        $objLead = CCrmLead::GetList(false, $arFilter, Array(), 1);
        if ($arrLead = $objLead->Fetch())
        {
            $arFields["LEAD"] = $arrLead;
        }
        $dbResMultiFields = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $leadId)
        );
        $arFields['FM'] = array();
        while ($arMultiFields = $dbResMultiFields->Fetch())
        {
            $arFields['FM'][$arMultiFields["TYPE_ID"]] = $arMultiFields;
        }
        $arResult["LEAD"] = array_merge($arFields["LEAD"], $arFields['FM']);
        return $arResult["LEAD"];
    }

    /* Поиск лида по email, в случае нахождения/не нахождения совпадения, у лида изменяется свойство "Старый/новый" */
    public function findLeadByEmail($email, $leadId, $source)
    {
        $objLead = new CCrmLead;
        if ($email)
        {
            $email = str_replace(" ", "", $email);
            $email = mb_strtolower(trim($email));
            global $DB;
            $query = "select t1.ID, t1.TITLE, t1.DATE_CREATE, t1.STATUS_ID, t1.ASSIGNED_BY_ID, t1.SOURCE_DESCRIPTION, t1.COMPANY_TITLE, t2.VALUE, t2.TYPE_ID from b_crm_lead t1
            	inner join b_crm_field_multi t2 on t1.ID = t2.ELEMENT_ID
		where t1.ASSIGNED_BY_ID != " . USER_ID_FREE_CONTACT . " and t2.TYPE_ID = 'EMAIL' and t2.VALUE = '" . $email . "' AND t2.ENTITY_ID='LEAD'";
            if ($source)
            {
                $query .= " and t1.SOURCE_DESCRIPTION = '" . $source . "'";
            }
            if ($leadId)
            {
                $query .= " and t1.ID != '" . $leadId . "'";
            }
            $query .= " order by t1.DATE_CREATE ASC";
            $leadObjEmail = $DB->Query($query);
            while ($leadArrEmail = $leadObjEmail->Fetch())
            {
                $arLead[] = $leadArrEmail;
            }
            if (count($arLead) > 0)
            {
                if ($leadId)
                {
                    $arUpdateLeadParams[PROP_LEAD_NEWOLD] = "old";
                    $objLead->Update($leadId, $arUpdateLeadParams, true, true);
                }
                return $arLead;
            }
            else
            {
                if ($leadId)
                {
                    $arUpdateLeadParams[PROP_LEAD_NEWOLD] = "new";
                    $objLead->Update($leadId, $arUpdateLeadParams, true, true);
                }
                return "error";
            }
        }
        else
        {
            return "error";
        }
    }

    /* Поиск лидов по телефону */
    public function findLeadByPhone($phone, $leadId, $source)
    {
        $objLead = new CCrmLead;
        $phone = preg_replace("/\D/", "", $phone);
        if ($phone)
        {
            $phone2=$phone;
            if (substr($phone, 0, 1) == 7)
                $phone2 = "8" . substr($phone, 1);
            elseif(substr($phone, 0, 1) == 8)
                $phone2 = "7" . substr($phone, 1);
            global $DB;
            $query = "select t1.ID, t1.TITLE, t1.DATE_CREATE, t1.STATUS_ID, t1.ASSIGNED_BY_ID, t1.SOURCE_DESCRIPTION, t1.COMPANY_TITLE, t2.VALUE, t2.TYPE_ID from b_crm_lead t1
		inner join b_crm_field_multi t2 on t1.ID = t2.ELEMENT_ID
		where t1.ASSIGNED_BY_ID != " . USER_ID_FREE_CONTACT . " and t2.TYPE_ID = 'PHONE' and (t2.VALUE = '" . $phone . "' OR t2.VALUE = '" . $phone2 . "') AND t2.ENTITY_ID='LEAD'";
            if ($source)
            {
                $query .= " and t1.SOURCE_DESCRIPTION = '" . $source . "'";
            }
            if ($leadId)
            {
                $query .= " and t1.ID != '" . $leadId . "'";
            }
            $query .= " order by t1.DATE_CREATE ASC";

            $leadObjPhone = $DB->Query($query);
            while ($arLeadPhone = $leadObjPhone->Fetch())
            {
                $arLead[] = $arLeadPhone;
            }
            if (count($arLead) == 0)
            {
                $arLead = "error";
            }

            if ($arLead != "error")
            {

                if ($leadId)
                {
                    $arUpdateLeadParams[PROP_LEAD_NEWOLD] = "old";
                    $objLead->Update($leadId, $arUpdateLeadParams, true, true);
                }

                return $arLead;
            }
            else
            {

                if ($leadId)
                {
                    $arUpdateLeadParams[PROP_LEAD_NEWOLD] = "new";
                    $objLead->Update($leadId, $arUpdateLeadParams, true, true);
                }

                return "error";
            }

        }
        else
        {
            return "error";
        }

    }


    public function findLeadByPhoneToday($leadId, $phone, $land)
    {

        $phone = preg_replace("/\D/", "", $phone);

        if ($land)
        {
            $arFilter = array(
                "!ASSIGNED_BY_ID" => USER_ID_FREE_CONTACT,
                PROP_LEAD_LAND => $land,
                "!ID" => $leadId,
                ">=DATE_CREATE" => date("d.m.Y"),
                "FM" => array("PHONE" => array("VALUE" => $phone))
            );
        }
        else
        {
            $arFilter = array(
                "!ASSIGNED_BY_ID" => USER_ID_FREE_CONTACT,
                "!ID" => $leadId,
                ">=DATE_CREATE" => date("d.m.Y"),
                "FM" => array("PHONE" => array("VALUE" => $phone))
            );
        }


        $arSelect = Array("ID", "TITLE", "DATE_CREATE", "ASSIGNED_BY_ID", PROP_LEAD_LAND);

        $objLead = CCrmLead::GetList(false, $arFilter, $arSelect, 1);

        if ($arrLead = $objLead->Fetch())
        {
            return $arrLead;
        }
        else
        {
            return "error";
        }

    }

    public function leadChangeStatus($leadId, $status)
    {

        $lead = new CCrmLead;

        $arLeadParams = Array(
            "STATUS_ID" => $status
        );

        $response = $lead->Update($leadId, $arLeadParams);

        return $response;

    }

    /* Конвертация лида, создание сделки и привязка к контакту */
    public function leadConverted($leadId, $contactId, $assignedId)
    {

        $rsLead = CCrmLead::GetList(false, array("ID" => $leadId));

        if ($arLead = $rsLead->Fetch())
        {

            $CCrmDeal = new CCrmDeal;

            if ($assignedId)
            {
                $arLead["ASSIGNED_BY_ID"] = $assignedId;
                $arLead["ASSIGNED_BY"] = $assignedId;
            }

            $iDealId = $CCrmDeal->Add($arLead, false);

            $arParams = Array('STATUS_ID' => 'CONVERTED', 'CONTACT_ID' => $contactId);

            $objLead = new CCrmLead;
            $idLeadUpdate = $objLead->Update($leadId, $arParams, true, true);

            echo $idLeadUpdate;
        }
        else
        {
            echo "error";
        }
    }


    /* Изменение ответственного и диспетчера в лиде */
    public function leadChangeAssigned($leadId, $assignedId)
    {

        $rsLead = CCrmLead::GetList(false, array("ID" => $leadId), false, 1);

        if ($arLead = $rsLead->Fetch())
        {

            if ($assignedId)
            {
                $arParams["ASSIGNED_BY_ID"] = $assignedId;
                $arParams[PROP_LEAD_DISPATCHER] = $assignedId;
            }

            $objLead = new CCrmLead;
            $idLeadUpdate = $objLead->Update($leadId, $arParams, true, true);

            echo $idLeadUpdate;
        }
        else
        {
            echo "error";
        }
    }

    /* смена подразделения лида */
    public function changeLeadDepartment($leadId, $depId)
    {

        CModule::IncludeModule("iblock");

        $lead = new CCrmLead;

        $arSelect = Array("ID", "NAME");

        $nav = CIBlockSection::GetNavChain(false, $depId, $arSelect);

        while ($arNav = $nav->Fetch())
        {
            $arLeadParams[PROP_LEAD_DEPARTMENT_STRUCTURE][] = $arNav["ID"];
        }

        $lead->Update($leadId, $arLeadParams, false, false);
    }

    /* Равномерное распределение лидов между указанными ответственными */
    public function allocationLead($leadId, $stringArrayLands, $arrayAssignedUsers, $mba = false)
    {

        if (date("H") >= 9 && date("H") < 20 && $mba == false && date("D") != "Sat" && date("D") != "Sun")
        {
            $i = 1;
            foreach ($arrayAssignedUsers as $userId)
            {
                if (CUser::IsOnline($userId) == 1)
                {
                    $arAssigned[$i]["USER_ID"] = $userId;
                    $arAssigned[$i]["QUEUE"] = $i;
                    $i++;
                }
            }
        }
        else
        {
            $i = 1;
            foreach ($arrayAssignedUsers as $userId)
            {
                $arAssigned[$i]["USER_ID"] = $userId;
                $arAssigned[$i]["QUEUE"] = $i;
                $i++;
            }
        }

        $arSort = Array(
            PROP_LEAD_NUMBERCREATE => "DESC",
            "DATE_MODIFY" => "DESC"
        );

        $arLands = explode("; ", $stringArrayLands);

        $arParams = Array(
            PROP_LEAD_WITHOUT_QUEUE => "",
            PROP_LEAD_LAND => $arLands
        );

        $arSelect = Array(
            "ID",
            "TITLE",
            "ASSIGNED_BY_ID",
            PROP_LEAD_LAND,
            PROP_LEAD_QUEUE,
            PROP_LEAD_NUMBERCREATE
        );

        $nPageTop = 1;

        $objLeadAssigned = CCrmLead::GetList($arSort, $arParams, $arSelect, $nPageTop);

        if ($arrLeadAssigned = $objLeadAssigned->Fetch())
        {
            $queueLastLead = $arrLeadAssigned[PROP_LEAD_QUEUE];
            $numberLastChange = $arrLeadAssigned[PROP_LEAD_NUMBERCREATE];
            $numberChange = $numberLastChange + 1;
        }
        else
        {
            $queueLastLead = count($arAssigned);
            $numberChange = 1;
        }

        if ($queueLastLead > count($arAssigned))
        {
            $queueLastLead = 1;
        }

        if ($queueLastLead == count($arAssigned))
        {
            $nextQueue = 1;
        }
        else
        {
            $nextQueue = $queueLastLead + 1;
        }

        $objLead = new CCrmLead;

        $userAssignedId = $arAssigned[$nextQueue]["USER_ID"];

        $arParamsUpdateLead = Array(
            "ASSIGNED_BY_ID" => $userAssignedId,
            PROP_LEAD_DISPATCHER => $userAssignedId,
            PROP_LEAD_QUEUE => $nextQueue,
            PROP_LEAD_NUMBERCREATE => $numberChange
        );

        $objLead->Update($leadId, $arParamsUpdateLead, true, true);

        $objUser = CUser::GetList(($by = "personal_country"), ($order = "desc"), Array("ID" => $userAssignedId));

        if ($arrUser = $objUser->Fetch())
        {
            $email = $arrUser["EMAIL"];
        }

        $arFilter = Array(
            "ID" => $leadId
        );

        $arSelect = Array("ID", "TITLE");

        $objLead = CCrmLead::GetList(false, $arFilter, $arSelect, 1);

        if ($arrLead = $objLead->Fetch())
        {

            /* Send message to email */

            $arEventFields = Array(
                "ID" => $leadId,
                "TITLE" => $arrLead["TITLE"],
                "USER_EMAIL" => $email
            );

            CEvent::Send("BP_SEND", "s1", $arEventFields);


            /* Send message by Bitrix web-messanger */

            CModule::IncludeModule("im");
            $objIMess = new CIMMessenger;

            $arFieldsMessage = Array(
                "AUTHOR_ID" => USER_ID_LANDER,
                "FROM_USER_ID" => USER_ID_LANDER,
                "TO_USER_ID" => $userAssignedId,
                "MESSAGE" => "Вам назначен лид на обработку https://corp.synergy.ru/crm/lead/show/" . $leadId . "/",
                "MESSAGE_TYPE" => "P",
            );

            $objMessage = $objIMess->Add($arFieldsMessage);

        }

    }


    public function allocationLead2($leadId, $sectionId)
    {

        CModule::IncludeModule("iblock");

        $arFilter = Array(
            "IBLOCK_ID" => IBLOCK_ID_ALLOCATION,
            "ID" => $sectionId,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => 'N'
        );

        $arSelect = Array("ID", "NAME", "UF_DISPATCHER", "UF_ONLINE");

        $objAllocation = CIBlockSection::GetList(false, $arFilter, false, $arSelect, false);

        if ($arrAllocation = $objAllocation->Fetch())
        {
            $arrayAssignedUsers = $arrAllocation["UF_DISPATCHER"];
            if ($arrAllocation["UF_ONLINE"] == 1)
            {
                $checkOnline = true;
            }
            else
            {
                $checkOnline = false;
            }
        }


        $arSort = Array("ID" => "ASC");

        $arFilter = Array(
            "IBLOCK_ID" => IBLOCK_ID_ALLOCATION,
            "SECTION_ID" => $sectionId,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => 'N'
        );

        $arSelect = Array("ID", "NAME");

        $objLands = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
        while ($arrLands = $objLands->Fetch())
        {
            $arLands[] = $arrLands["NAME"];
        }


        $zapros_96 = CIBlockElement::GetList(
            array(),
            array(
                "IBLOCK_ID" => 96, // делаем выборку из инфоблока с id96
                "ACTIVE" => "Y", // выбираем только активные элементы
                "CHECK_PERMISSIONS" => 'N'
            ),
            false,
            array(),
            array("ID", "IBLOCK_ID", "PROPERTY_*") // в элементах выбираем все доп свойства
        );
        while ($object_96 = $zapros_96->GetNextElement())
        {
            $fields_96_tmp = $object_96->GetFields();
            $property_96 = $object_96->GetProperties();
            $raspr_lead[$fields_96_tmp["ID"]] = $property_96; // формируем массив их выбраного
        }
        foreach ($raspr_lead as $raspr_lead_key => $raspr_lead_temp)
        {
            foreach ($raspr_lead_temp["employees"]["VALUE"] as $user_tmp)
            {
                foreach ($arrayAssignedUsers as $userId)
                {
                    if ($userId == $user_tmp)
                    { // сравниваем id пользователей из распределения и инфоблока 96
                        $rsUser = CUser::GetByID($user_tmp);
                        $arsUser = $rsUser->Fetch();
                        if (CUser::IsOnline($user_tmp) == 1)
                        { // смотри, он-лайн пользователь или нет
                            $online = 'Y';
                        }
                        else
                        {
                            $online = 'N';
                        }
                        $raspred[$user_tmp] = array( // записываем массив пользователь -> партаметры
                            "param_id" => $raspr_lead_key,
                            "user_id" => $user_tmp,
                            "user_login" => $arsUser["LOGIN"],
                            "time_s" => $raspr_lead_temp["time_s"]["VALUE"],
                            "time_po" => $raspr_lead_temp["time_po"]["VALUE"],
                            "online" => $online,
                            "day" => $raspr_lead_temp["day_week"]["VALUE_XML_ID"]
                        );

                    }
                }
            }
        }

        $shiftHourStart = 9;

        if (((int)date('H') >= $shiftHourStart && (int)date('H') < 21) || ((int)date('H') >= 23 && (int)date('H') < 7))
        { // рабочее или ночное время
            foreach ($arrayAssignedUsers as $userId)
            {
                foreach ($raspred[$userId]["day"] as $day)
                { //пробегаемся по дням недели
                    if (
                        $raspred[$userId]["time_s"] <= (int)date("H") && // часы с которых начинает работать пользователь
                        $raspred[$userId]["time_po"] > (int)date("H") && // часы когда заканчивает работать пользователь
                        $raspred[$userId]["online"] == 'Y' && // он-лайн или офлайн пользователь
                        $day == date("D") // дни недели
                    )
                    {
                        $nevs['on'][$raspred[$userId]["user_id"]] = array( // записываем пользователей в массив которые удволетворяют условиям он-лайн
                            "USER_ID" => $raspred[$userId]["user_id"]
                        );
                    }
                    else
                    {
                        if (($userId != 2565) && ($userId != 435))
                        { // исключаем двух сотрудников, на которых нельзя распределять время в нерабочее время
                            $nevs['off'][$raspred[$userId]["user_id"]] = array(
                                "USER_ID" => $raspred[$userId]["user_id"]
                            );
                        }
                    }
                }
            }
        }

        if (!empty($nevs['on']))
        { // сначало раздаем лиды пользователям которые в стети и которые удволетворяют условиям
            $i = 1;
            foreach ($nevs['on'] as $us_id)
            {
                $arAssigned[$i]["USER_ID"] = $us_id["USER_ID"];
                $arAssigned[$i]["QUEUE"] = $i;
                $i++;
            }
        }
        elseif (!empty($nevs['off']))
        { // если не рабочее время или не удволетворяет условиям раздаем лиды всем кроме двух сотрудников
            $i = 1;
            foreach ($nevs['off'] as $us_id)
            {
                $arAssigned[$i]["USER_ID"] = $us_id["USER_ID"];
                $arAssigned[$i]["QUEUE"] = $i;
                $i++;
            }
        }
        if (empty($nevs))
        {
            if ((int)date("H") >= $shiftHourStart && (int)date("H") < 21 && $checkOnline == true && date("D") != "Sat" && date("D") != "Sun")
            {
                $i = 1;
                foreach ($arrayAssignedUsers as $userId)
                {
                    if (CUser::IsOnline($userId) == 1)
                    {
                        $arAssigned[$i]["USER_ID"] = $userId;
                        $arAssigned[$i]["QUEUE"] = $i;
                        $i++;
                    }
                }
                if ($i == 1)
                {
                    foreach ($arrayAssignedUsers as $userId)
                    {
                        if (in_array($userId, array(2565, 435)))
                        {
                            continue;
                        }
                        $arAssigned[$i]["USER_ID"] = $userId;
                        $arAssigned[$i]["QUEUE"] = $i;
                        $i++;
                    }

                }
            }
            else
            {
                $i = 1;
                $dutyEmp = null;
                if (date("D") != "Sat" && date("D") != "Sun")
                {
                    if ((int)date("H") >= 7 && (int)date("H") < $shiftHourStart)
                    {
                        $dutyEmp = 2836;
                    }
                    if ((int)date("H") >= 21 && (int)date("H") <= 23)
                    {
//                    		$dutyEmp=435;
                    }
                }
                if (!empty($dutyEmp) && $sectionId == 2432)
                {
                    $arAssigned[$i]["USER_ID"] = $dutyEmp;
                    $arAssigned[$i]["QUEUE"] = $i;
                    $i++;
                }
                else
                {
                    foreach ($arrayAssignedUsers as $userId)
                    {
                        if (in_array($userId, array(2565, 435)))
                        {
                            continue;
                        }
                        $arAssigned[$i]["USER_ID"] = $userId;
                        $arAssigned[$i]["QUEUE"] = $i;
                        $i++;
                    }
                }
            }
        }

        $arSort = Array(
            PROP_LEAD_NUMBERCREATE => "DESC",
            "DATE_MODIFY" => "DESC"
        );

        $arParams = Array(
            PROP_LEAD_WITHOUT_QUEUE => "",
            PROP_LEAD_LAND => $arLands
        );

        $arSelect = Array(
            "ID",
            "TITLE",
            "ASSIGNED_BY_ID",
            PROP_LEAD_LAND,
            PROP_LEAD_QUEUE,
            PROP_LEAD_NUMBERCREATE
        );

        $strLands = "(";

        foreach ($arLands as $kL => $arL)
        {
            if ($kL + 1 < count($arLands))
            {
                $strLands .= "'" . $arL . "',";
            }
            else
            {
                $strLands .= "'" . $arL . "'";
            }
        }

        $strLands .= ")";


        $query = "select t1.ID, t1.TITLE, t1.ASSIGNED_BY_ID, t2." . PROP_LEAD_LAND . ", t2." . PROP_LEAD_QUEUE . ", t2." . PROP_LEAD_NUMBERCREATE . " from b_crm_lead t1
inner join b_uts_crm_lead t2 on t1.ID = t2.VALUE_ID
where t2." . PROP_LEAD_QUEUE . " != '' and t2." . PROP_LEAD_LAND . " in " . $strLands . "
order by t2." . PROP_LEAD_NUMBERCREATE . " desc, ID desc
limit 0,100";


        global $DB;

        $objLeadAssigned = $DB->Query($query);

//		$objLeadAssigned = CCrmLead::GetList($arSort, $arParams, $arSelect, 1);

        if ($arrLeadAssigned = $objLeadAssigned->Fetch())
        {
            $queueLastLead = $arrLeadAssigned[PROP_LEAD_QUEUE];
            $numberLastChange = $arrLeadAssigned[PROP_LEAD_NUMBERCREATE];
            $numberChange = $numberLastChange + 1;

        }
        else
        {
            $queueLastLead = count($arAssigned);
            $numberChange = 1;
        }

        if ($queueLastLead > count($arAssigned))
        {
            $queueLastLead = 1;
        }

        if ($queueLastLead == count($arAssigned))
        {
            $nextQueue = 1;
        }
        else
        {
            $nextQueue = $queueLastLead + 1;
        }
        $objLead = new CCrmLead(false);

        //$nextQueue = rand(1, count($arAssigned));

        $userAssignedId = $arAssigned[$nextQueue]["USER_ID"];

        $arParamsUpdateLead = Array(
            "ASSIGNED_BY_ID" => $userAssignedId,
            'MODIFY_BY_ID' => USER_ID_LANDER,
            PROP_LEAD_DISPATCHER => $userAssignedId,
            PROP_LEAD_QUEUE => $nextQueue,
            PROP_LEAD_NUMBERCREATE => $numberChange
        );

        $objLead->Update($leadId, $arParamsUpdateLead, true, true,['REGISTER_SONET_EVENT' => true]);

        $objUser = CUser::GetList(($by = "personal_country"), ($order = "desc"), Array("ID" => $userAssignedId));

        if ($arrUser = $objUser->Fetch())
        {
            $email = $arrUser["EMAIL"];
        }

        $arFilter = Array(
            "ID" => $leadId,
            "CHECK_PERMISSIONS" => 'N'
        );

        $arSelect = Array("ID", "TITLE");

        $objLead = CCrmLead::GetList(false, $arFilter, $arSelect, 1);

        if ($arrLead = $objLead->Fetch())
        {

            /* Send message to email */

            $arEventFields = Array(
                "ID" => $leadId,
                "TITLE" => $arrLead["TITLE"],
                "USER_EMAIL" => $email
            );

            CEvent::Send("BP_SEND", "s1", $arEventFields);


            /* Send message by Bitrix web-messanger */

            CModule::IncludeModule("im");
            $objIMess = new CIMMessenger;

            $arFieldsMessage = Array(
                "AUTHOR_ID" => USER_ID_LANDER,
                "FROM_USER_ID" => USER_ID_LANDER,
                "TO_USER_ID" => $userAssignedId,
                "MESSAGE" => "Вам назначен лид на обработку https://corp.synergy.ru/crm/lead/show/" . $leadId . "/",
                "MESSAGE_TYPE" => "P",
            );

            $objMessage = $objIMess->Add($arFieldsMessage);

        }

    } // allocationLead2 END


    public function allocationLeadDubai($leadId, $sectionId)
    {

        CModule::IncludeModule("iblock");

        $arFilter = Array(
            "IBLOCK_ID" => IBLOCK_ID_ALLOCATION,
            "ID" => $sectionId,
            "ACTIVE" => "Y"
        );

        $arSelect = Array("ID", "NAME", "UF_DISPATCHER", "UF_ONLINE");

        $objAllocation = CIBlockSection::GetList(false, $arFilter, false, $arSelect, false);

        if ($arrAllocation = $objAllocation->Fetch())
        {
            $arrayAssignedUsers = $arrAllocation["UF_DISPATCHER"];
            if ($arrAllocation["UF_ONLINE"] == 1)
            {
                $checkOnline = true;
            }
            else
            {
                $checkOnline = false;
            }
        }
        $i = 1;
        foreach ($arrayAssignedUsers as $userId)
        {
            $arAssigned[$i]["USER_ID"] = $userId;
            $arAssigned[$i]["QUEUE"] = $i;
            $i++;
        }

        $arSort = Array("ID" => "ASC");

        $arFilter = Array(
            "IBLOCK_ID" => IBLOCK_ID_ALLOCATION,
            "SECTION_ID" => $sectionId,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => 'N'
        );

        $arSelect = Array("ID", "NAME");

        $objLands = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
        while ($arrLands = $objLands->Fetch())
        {
            $arLands[] = $arrLands["NAME"];
        }

        $arSort = Array(
            PROP_LEAD_NUMBERCREATE => "DESC",
            "DATE_MODIFY" => "DESC"
        );

        $arParams = Array(
            PROP_LEAD_WITHOUT_QUEUE => "",
            PROP_LEAD_LAND => $arLands
        );

        $arSelect = Array(
            "ID",
            "TITLE",
            "ASSIGNED_BY_ID",
            PROP_LEAD_LAND,
            PROP_LEAD_QUEUE,
            PROP_LEAD_NUMBERCREATE
        );

        $strLands = "(";

        foreach ($arLands as $kL => $arL)
        {
            if ($kL + 1 < count($arLands))
            {
                $strLands .= "'" . $arL . "',";
            }
            else
            {
                $strLands .= "'" . $arL . "'";
            }
        }

        $strLands .= ")";
        $query = "select t1.ID, t1.TITLE, t1.ASSIGNED_BY_ID, t2." . PROP_LEAD_LAND . ", t2." . PROP_LEAD_QUEUE . ", t2." . PROP_LEAD_NUMBERCREATE . " from b_crm_lead t1
inner join b_uts_crm_lead t2 on t1.ID = t2.VALUE_ID
where t2." . PROP_LEAD_QUEUE . " != '' and t2." . PROP_LEAD_LAND . " in " . $strLands . "
order by t2." . PROP_LEAD_NUMBERCREATE . " desc, ID desc
limit 0,100";
        global $DB;
        $objLeadAssigned = $DB->Query($query);
        if ($arrLeadAssigned = $objLeadAssigned->Fetch())
        {
            $queueLastLead = $arrLeadAssigned[PROP_LEAD_QUEUE];
            $numberLastChange = $arrLeadAssigned[PROP_LEAD_NUMBERCREATE];
            $numberChange = $numberLastChange + 1;

        }
        else
        {
            $queueLastLead = count($arAssigned);
            $numberChange = 1;
        }

        if ($queueLastLead > count($arAssigned))
        {
            $queueLastLead = 1;
        }

        if ($queueLastLead == count($arAssigned))
        {
            $nextQueue = 1;
        }
        else
        {
            $nextQueue = $queueLastLead + 1;
        }



        $objLead = new CCrmLead(false);

        //$nextQueue = rand(1, count($arAssigned));

        $userAssignedId = $arAssigned[$nextQueue]["USER_ID"];

        $arParamsUpdateLead = Array(
            "ASSIGNED_BY_ID" => $userAssignedId,
            "MODIFY_BY_ID" => USER_ID_LANDER,
            PROP_LEAD_DISPATCHER => $userAssignedId,
            PROP_LEAD_QUEUE => $nextQueue,
            PROP_LEAD_NUMBERCREATE => $numberChange
        );

        $objLead->Update($leadId, $arParamsUpdateLead, true, true,['REGISTER_SONET_EVENT' => true]);

        $objUser = CUser::GetList(($by = "personal_country"), ($order = "desc"), Array("ID" => $userAssignedId));

        if ($arrUser = $objUser->Fetch())
        {
            $email = $arrUser["EMAIL"];
        }

        $arFilter = Array(
            "ID" => $leadId,
            "CHECK_PERMISSIONS" => 'N'
        );

        $arSelect = Array("ID", "TITLE");

        $objLead = CCrmLead::GetList(false, $arFilter, $arSelect, 1);

        if ($arrLead = $objLead->Fetch())
        {

            /* Send message to email */

            $arEventFields = Array(
                "ID" => $leadId,
                "TITLE" => $arrLead["TITLE"],
                "USER_EMAIL" => $email
            );
            CEvent::Send("BP_SEND", "s1", $arEventFields);


            /* Send message by Bitrix web-messanger */

            CModule::IncludeModule("im");
            $objIMess = new CIMMessenger;

            $arFieldsMessage = Array(
                "AUTHOR_ID" => USER_ID_LANDER,
                "FROM_USER_ID" => USER_ID_LANDER,
                "TO_USER_ID" => $userAssignedId,
                "MESSAGE" => "Вам назначен лид на обработку https://corp.synergy.ru/crm/lead/show/" . $leadId . "/",
                "MESSAGE_TYPE" => "P",
            );

            $objMessage = $objIMess->Add($arFieldsMessage);

        }

    } // allocationLeadDubai END


    public function startLeadBP($leadId, $workflowId)
    {
        CModule::IncludeModule("bizproc");
        CBPDocument::StartWorkflow($workflowId, array("crm", "CCrmDocumentLead", "LEAD_" . $leadId));
    }

    public function assignParentLead($parentLeadId, $childrenLeadId)
    {

        $lead = new CCrmLead;

        $arParamsParentLead = Array(
            PROP_LEAD_CHILDREN_LEAD => $childrenLeadId
        );

        $arParamsChildrenLead = Array(
            PROP_LEAD_PARENT_LEAD => $parentLeadId
        );

        $lead->Update($parentLeadId, $arParamsParentLead);
        $lead->Update($childrenLeadId, $arParamsChildrenLead);
    }

}


class SynergyContact
{

    function __construct()
    {
        CModule::IncludeModule('crm');
    }

    /* Возвращает параметры лида по его Id */
    public function getSynergyContactById($contactId)
    {

        $arFilter = Array(
            "ID" => $contactId,
        );

        $objContact = CCrmContact::GetList(false, $arFilter, Array(), 1);
        if ($arrContact = $objContact->Fetch())
        {
            $arFields["CONTACT"] = $arrContact;
        }

        $dbResMultiFields = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $contactId)
        );

        $arFields['FM'] = array();
        while ($arMultiFields = $dbResMultiFields->Fetch())
        {
            $arFields['FM'][$arMultiFields["TYPE_ID"]][$arMultiFields["ID"]] = $arMultiFields;
        }

        $arResult["CONTACT"] = array_merge($arFields["CONTACT"], $arFields['FM']);

        return $arResult["CONTACT"];
    }

    /* Поиск контакта по email, в случае нахождения/не нахождения совпадения, у лида изменяется свойство "Старый/новый" */
    public function findContactByEmail($email, $leadId, $source)
    {

        $objLead = new CCrmLead;

        if ($email)
        {
            $email = str_replace(" ", "", $email);
            $email = mb_strtolower(trim($email));

            $arContactFilter = array(
                "!ASSIGNED_BY_ID" => USER_ID_FREE_CONTACT,
                "FM" => array("EMAIL" => array("VALUE" => $email))
            );

            if ($source)
            {
                $arContactFilter[PROP_CONTACT_SOURCE_DESCRIPTION] = $source;
            }

            $contObjEmail = CCrmContact::GetList(
                array("DATE_CREATE" => "ASC"),
                $arContactFilter
            );

            while ($arContEmail = $contObjEmail->Fetch())
            {
                $arContact[] = $arContEmail;
            }


            if (count($arContact) > 0)
            {

                if ($leadId)
                {
                    $arUpdateLeadParams[PROP_LEAD_NEWOLD] = "old";
                    $objLead->Update($leadId, $arUpdateLeadParams, true, true);
                }

                return $arContact;

            }
            else
            {

                if ($leadId)
                {
                    $arUpdateLeadParams[PROP_LEAD_NEWOLD] = "new";
                    $objLead->Update($leadId, $arUpdateLeadParams, true, true);
                }

                return "error";
            }
        }
        else
        {
            return "error";
        }

    }

    /* Поиск контактов по телефону */
    public function findContactByPhone($phone, $leadId, $source)
    {

        $objLead = new CCrmLead;

        $phone = preg_replace("/\D/", "", $phone);

        if ($phone)
        {

            $arContactFilter = array(
                "!ASSIGNED_BY_ID" => USER_ID_FREE_CONTACT,
                "FM" => array("PHONE" => array("VALUE" => $phone))
            );

            if ($source)
            {
                $arContactFilter[PROP_CONTACT_SOURCE_DESCRIPTION] = $source;
            }

            $contactObjPhone = CCrmContact::GetList(
                array("DATE_CREATE" => "ASC"),
                $arContactFilter
            );

            while ($arContactPhone = $contactObjPhone->Fetch())
            {
                $arContact[] = $arContactPhone;
            }

            if (count($arContact) == 0)
            {
                $arContact = "error";
            }

            if (substr($phone, 0, 1) == 7)
            {
                $phone8 = "8" . substr($phone, 1);

                $contactObjPhone8 = CCrmContact::GetList(
                    array("DATE_CREATE" => "ASC"),
                    array(
                        "!ASSIGNED_BY_ID" => USER_ID_FREE_CONTACT,
                        "STATUS_ID" => "CONVERTED",
                        "FM" => array("PHONE" => array("VALUE" => $phone8))
                    )
                );

                while ($arContactPhone8 = $contactObjPhone8->Fetch())
                {
                    $arContact8[] = $arContactPhone8;
                }

                if (count($arContact8) == 0)
                {
                    $arContact8 = "error";
                }

                if ($arContact != "error" and $arContact8 != "error")
                {
                    $arContact = array_merge($arContact, $arContact8);
                }
                elseif ($arContact == "error" and $arContact8 != "error")
                {
                    $arContact = $arContact8;
                }
                elseif ($arContact == "error" and $arContact8 == "error")
                {
                    $arContact = "error";
                }

            }
            elseif (substr($phone, 0, 1) == 8)
            {
                $phone7 = "7" . substr($phone, 1);

                $contactObjPhone7 = CCrmContact::GetList(
                    array("DATE_CREATE" => "ASC"),
                    array(
                        "!ASSIGNED_BY_ID" => USER_ID_FREE_CONTACT,
                        "STATUS_ID" => "CONVERTED",
                        "FM" => array("PHONE" => array("VALUE" => $phone7))
                    )
                );

                while ($arContactPhone7 = $contactObjPhone7->Fetch())
                {
                    $arContact7[] = $arContactPhone7;
                }

                if (count($arContact7) == 0)
                {
                    $arContact7 = "error";
                }

                if ($arContact != "error" and $arContact7 != "error")
                {
                    $arContact = array_merge($arContact, $arContact7);
                }
                elseif ($arContact == "error" and $arContact7 != "error")
                {
                    $arContact = $arContact7;
                }
                elseif ($arContact == "error" and $arContact7 == "error")
                {
                    $arContact = "error";
                }
            }

            if ($arContact != "error")
            {

                if ($leadId)
                {
                    $arUpdateLeadParams[PROP_LEAD_NEWOLD] = "old";
                    $objLead->Update($leadId, $arUpdateLeadParams, true, true);
                }

                return $arContact;
            }
            else
            {

                if ($leadId)
                {
                    $arUpdateLeadParams[PROP_LEAD_NEWOLD] = "new";
                    $objLead->Update($leadId, $arUpdateLeadParams, true, true);
                }

                return "error";
            }

        }
        else
        {
            return "error";
        }

    }


    /* смена подразделения контакта */
    public function changeContactDepartment($contactId, $depId)
    {

        CModule::IncludeModule("iblock");

        $contact = new CCrmContact;

        $arSelect = Array("ID", "NAME");

        $nav = CIBlockSection::GetNavChain(false, $depId, $arSelect);

        while ($arNav = $nav->Fetch())
        {
            $arContactParams[PROP_CONTACT_DEPARTMENT_STRUCTURE][] = $arNav["ID"];
        }

        $contact->Update($contactId, $arContactParams, false, false);
    }


    /* смена дополнительно об источнике контакта */
    public function changeContactSource($contactId)
    {

        $contact = new CCrmContact;

        $arContactParams = Array(
            PROP_CONTACT_SOURCE_DESCRIPTION => "Школа Бизнеса"
        );

        $contact->Update($contactId, $arContactParams, false, false);
    }


    public function getSynergyContactByLead($leadId)
    {
        $arFilter = Array(
            "LEAD_ID" => $leadId,
        );

        $objLead = CCrmContact::GetList(false, $arFilter, Array(), 1);
        if ($arrLead = $objLead->Fetch())
        {
            $arFields["CONTACT"] = $arrLead;
        }

        $dbResMultiFields = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arFields["CONTACT"]["ID"])
        );

        $arFields['FM'] = array();
        while ($arMultiFields = $dbResMultiFields->Fetch())
        {
            $arFields['FM'][$arMultiFields["TYPE_ID"]][$arMultiFields["ID"]] = $arMultiFields;
        }

        $arResult["CONTACT"] = array_merge($arFields["CONTACT"], $arFields['FM']);

        return $arResult["CONTACT"];
    }

    public function startContactBP($contactId, $workflowId)
    {
        CModule::IncludeModule("bizproc");
        CBPDocument::StartWorkflow($workflowId, array("crm", "CCrmDocumentContact", "CONTACT_" . $contactId));
    }

}


class SynergyDeal
{

    function __construct()
    {
        CModule::IncludeModule('crm');
    }

    /* Возвращает параметры сделки по Id */
    public function getSynergyDealById($dealId)
    {

        $arFilter = Array(
            "ID" => $dealId,
        );

        $objLead = CCrmDeal::GetList(false, $arFilter, Array(), 1);
        if ($arrLead = $objLead->Fetch())
        {
            $arFields["DEAL"] = $arrLead;
        }

        $dbResMultiFields = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array('ENTITY_ID' => 'DEAL', 'ELEMENT_ID' => $dealId)
        );

        $arFields['FM'] = array();
        while ($arMultiFields = $dbResMultiFields->Fetch())
        {
            $arFields['FM'][$arMultiFields["TYPE_ID"]][$arMultiFields["ID"]] = $arMultiFields;
        }

        $arResult["DEAL"] = array_merge($arFields["DEAL"], $arFields['FM']);

        return $arResult["DEAL"];
    }

    public function getSynergyDealByLead($leadId)
    {
        $arFilter = Array(
            "LEAD_ID" => $leadId,
        );

        $objLead = CCrmDeal::GetList(false, $arFilter, Array(), 1);
        if ($arrLead = $objLead->Fetch())
        {
            $arFields["DEAL"] = $arrLead;
        }

        $dbResMultiFields = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array('ENTITY_ID' => 'DEAL', 'ELEMENT_ID' => $arFields["DEAL"]["ID"])
        );

        $arFields['FM'] = array();
        while ($arMultiFields = $dbResMultiFields->Fetch())
        {
            $arFields['FM'][$arMultiFields["TYPE_ID"]][$arMultiFields["ID"]] = $arMultiFields;
        }

        $arResult["DEAL"] = array_merge($arFields["DEAL"], $arFields['FM']);

        return $arResult["DEAL"];
    }

}


$newSynergyLead = new SynergyLead;
$newSynergyContact = new SynergyContact;
$newSynergyDeal = new SynergyDeal;
?>