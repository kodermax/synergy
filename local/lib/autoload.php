<?php
/**
 * Created by PhpStorm.
 * User: MKarpychev
 * Date: 23.09.2015
 * Time: 15:15
 */
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/crm/Entity.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/crm/Contact.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/crm/Lead.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/crm/Deal.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/crm/Invoice.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/iblock/Constants.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/log/FileLogger.php");

require_once($_SERVER["DOCUMENT_ROOT"]."/local/lib/composer/vendor/autoload.php");

if (file_exists($_SERVER["DOCUMENT_ROOT"].'/.env')) {
    $dotenv = new Dotenv\Dotenv($_SERVER["DOCUMENT_ROOT"]);
    $dotenv->load();
}