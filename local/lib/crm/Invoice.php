<?php
namespace Synergy\Crm;
class Invoice extends Entity
{
    const PROP_DEPARTMENT = 'UF_CRM_561297E2C42FF';
    /**
     * Функция возвращает тип сущности CRM
     * @return string
     */
    public static function getType()
    {
        return 'INVOICE';
    }
}