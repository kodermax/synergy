<?php
/**
 * Created by PhpStorm.
 * User: MKarpychev
 * Date: 23.09.2015
 * Time: 11:25
 */

namespace Synergy\Crm;

/**
 * Class CrmContact - Класс для работы с контактами CRM
 * @package Synergy\Crm
 */
class Contact extends Entity
{
    #region constants
    const PROP_CAMPAIGN_CODE = 'UF_CRM_5603AEEB88C85';
    const PROP_CITY = 'UF_CRM_1421312595';
    const PROP_CLASS = 'UF_CRM_1421764447';
    const PROP_COUNTRY = 'UF_CRM_5603AEFF423AA';
    const PROP_DEPARTMENT = 'UF_CRM_1435222549';
    const PROP_LAND_CODE = 'UF_CRM_1426689857';
    const PROP_SCHOOL = 'UF_CRM_1421763800';
    const PROP_SOURCE_CODE = 'UF_CRM_1434363760';
    #endregion

    /**
     * Поиск контакта по email
     * @param $email - Email контакта
     * @return array - массив данных о найденом контакте
     */
    public static function findByEmailForAllocate($email)
    {
        $contactId = Entity::findEntityByFM('CONTACT', 'EMAIL', $email);
        return $contactId > 0 ? self::getInfoContact($contactId) : array();
    }

    /**
     * Поиск контакта по телефону
     * @param $phone - Телефон
     * @return array - Возвращает данные о найденном контакте или пустой массив
     */
    public static function findByPhoneForAllocate($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        $firstLetter = substr($phone, 0, 1);

        if (strlen($phone) == 11 && ($firstLetter == 7 || $firstLetter == 8)) {
            $phone = substr($phone, 1);
            $contactId = Entity::findEntityByFM('CONTACT', 'PHONE', '7' . $phone);
            if ($contactId <= 0)
                $contactId = Entity::findEntityByFM('CONTACT', 'PHONE', '8' . $phone);
        } else {
            $contactId = Entity::findEntityByFM('CONTACT', 'PHONE', $phone);
        }
        return $contactId > 0 ? self::getInfoContact($contactId) : array();
    }

    public static function getDataForEsByArray($arFields)
    {
        $fullName = $arFields['LAST_NAME'];
        if (!empty($arFields['NAME']))
            $fullName .= ' '.$arFields['NAME'];
        if (!empty($arFields['SECOND_NAME']))
            $fullName .= ' '.$arFields['SECOND_NAME'];
        $arPerms = \CCrmPerms::GetEntityAttr('CONTACT', array($arFields['ID']));
        $arResult = [
            'address' => $arFields['ADDRESS'],
            'assigned_by_id' => $arFields['ASSIGNED_BY_ID'],
            'assigned_by' => [
                'department' => implode(' / ', self::getDepartment($arFields['ASSIGNED_BY_ID'], 'name')),
                'id' => $arFields['ASSIGNED_BY_ID'],
                'first_name' => $arFields['ASSIGNED_BY_NAME'],
                'last_name' => $arFields['ASSIGNED_BY_LAST_NAME'],
                'second_name' => $arFields['ASSIGNED_BY_SECOND_NAME'],
                'url' => \CComponentEngine::MakePathFromTemplate(
                    '/company/personal/user/#user_id#/',
                    array('user_id' => $arFields['ASSIGNED_BY_ID'])
                )
            ],
            'birth_date' => $arFields['BIRTHDATE'],
            'campaign_code' => $arFields[self::PROP_CAMPAIGN_CODE],
            'city' => $arFields[self::PROP_CITY],
            'class' => '',
            'comments' => $arFields['COMMENTS'],
            'company' => [],
            'country' => $arFields[self::PROP_COUNTRY],
            'date_create' => $arFields['DATE_CREATE'],
            'date_change' => $arFields['DATE_MODIFY'],
            'deals' => [],
            'desc' => $arFields['SOURCE_DESCRIPTION'],
            'first_name' => $arFields['NAME'],
            'full_name' => $fullName,
            'emails' => [],
            'id' => $arFields['ID'],
            'land_code' => $arFields[self::PROP_LAND_CODE],
            'last_name' => $arFields['LAST_NAME'],
            'perms' => array_key_exists($arFields['ID'], $arPerms) ? $arPerms[$arFields['ID']] : array(),
            'phones' => [],
            'post' => $arFields['POST'],
            'second_name' => $arFields['SECOND_NAME'],
            'school' => [],
            'source_code' => !empty(trim($arFields[self::PROP_SOURCE_CODE])) ? $arFields[self::PROP_SOURCE_CODE] : $arFields['SOURCE_DESCRIPTION'],
            'type_id' => $arFields['TYPE_ID'],
            'url' => \CComponentEngine::MakePathFromTemplate(\COption::GetOptionString('crm', 'path_to_contact_show'),
                array(
                    'contact_id' => $arFields['ID']
                )
            )
        ];
        if (!empty($arFields['COMPANY_ID'])) {
            $arResult['company'] = [
                'id' => $arFields['COMPANY_ID'],
                'title' => $arFields['~COMPANY_TITLE']
            ];
        }
        if (!empty($arFields[self::PROP_SCHOOL])) {
            $res = \CCrmCompany::GetList([], ['ID' => $arFields[self::PROP_SCHOOL][0], 'CHECK_PERMISSIONS' => 'N'], ['TITLE']);
            if ($arSchool = $res->GetNext()) {
                $arResult['school'] = [
                    'id' => $arFields[self::PROP_SCHOOL],
                    'title' => $arSchool['TITLE'],
                    'url' => \CComponentEngine::MakePathFromTemplate(
                        '/crm/company/show/#company_id#/',
                        array('company_id' => $arFields[self::PROP_SCHOOL][0])
                    )
                ];
            }

        }
        if (!empty($arFields[self::PROP_CLASS])) {
            $rsClass = \CUserFieldEnum::GetList(array(), array("ID" => $arFields[self::PROP_CLASS]));
            if ($arClass = $rsClass->GetNext()) {
                $arResult['class'] = $arClass["VALUE"];
            }
        }
        $arMultiFields = array();
        $obRes = \CCrmFieldMulti::GetList(array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arFields['ID']));
        while ($arRes = $obRes->Fetch()) {
            $arMultiFields[] = array(
                'ID' => $arRes['ID'],
                'TYPE_ID' => $arRes['TYPE_ID'],
                'VALUE_TYPE' => $arRes['VALUE_TYPE'],
                'VALUE' => $arRes['VALUE']
            );
            if ($arRes['TYPE_ID'] == 'PHONE')
                $arResult['phones'][] = $arRes['VALUE'];
            if ($arRes['TYPE_ID'] == 'EMAIL')
                $arResult['emails'][] = $arRes['VALUE'];
        }
        $arResult['multi_fields'] = $arMultiFields;
        //Сделки
        $list = \CCrmDeal::GetListEx([], ['CONTACT_ID' => $arFields['ID'], 'CHECK_PERMISSIONS' => 'N'], false, false, ['ID', 'TITLE']);
        while ($row = $list->GetNext()) {
            $arResult['deals'][] = [
                'id' => $row['ID'],
                'title' => $row['TITLE'],
                'url' => \CComponentEngine::MakePathFromTemplate(
                    '/crm/deal/show/#deal_id#/',
                    array('deal_id' => $row['ID']))
            ];
        }
        return $arResult;
    }

    /**
     * Функция возвращает данные для ElasticSearch
     * @param $contactId - Ид Контакта
     * @return array - массив данеых
     */
    public static function getDataForEsById($contactId)
    {
        $list = \CCrmContact::GetList(array(), array('CHECK_PERMISSIONS' => 'N', 'ID' => $contactId));
        if ($row = $list->GetNext()) {
            return self::getDataForEsByArray($row);
        }
        return [];
    }

    /**
     * Возвращает последнюю дату активности по контакту
     * @param $contactId - ID контакта
     * @return mixed - дата
     */
    public static function getDateLastActivity($contactId)
    {
        $eventDate = '';
        $contactDate = '';
        $list = \CCrmContact::GetList(array(), array('ID' => $contactId, 'CHECK_PERMISSIONS' => 'N'), array('DATE_MODIFY'));
        if ($row = $list->Fetch()) {
            $contactDate = ConvertDateTime($row['DATE_MODIFY'], "YYYY-MM-DD", "ru");
        }
        if (!empty($contactDate)) {
            $list = \CCrmEvent::GetList(array('CREATED' => 'DESC'), array('ENTITY_ID' => $contactId), 1);
            if ($row = $list->Fetch()) {
                $eventDate = ConvertDateTime($row['DATE_CREATE'], 'YYYY-MM-DD', 'ru');
            }
        }
        unset($row);
        unset($list);
        return $contactDate > $eventDate ? $contactDate : $eventDate;
    }

    /**
     * Возвращает информацию о контакте
     * @param $contactId - ИД Контакта
     * @return array - массив данных
     */
    private static function getInfoContact($contactId)
    {
        $list = \CCrmContact::GetList(array(), array('ID' => $contactId, 'CHECK_PERMISSIONS' => 'N'), array('ID', 'ASSIGNED_BY_ID', PROP_CONTACT_SOURCE_DESCRIPTION));
        if ($row = $list->Fetch()) {
            return array(
                'ASSIGNED_BY_ID' => $row['ASSIGNED_BY_ID'],
                'ID' => $contactId,
                'SOURCE' => $row[PROP_CONTACT_SOURCE_DESCRIPTION]
            );
        }
        return array();
    }

    /**
     * Функция возвращает тип сущности CRM
     * @return string
     */
    public static function getType()
    {
        return 'CONTACT';
    }

    /**
     * Событие вызываентся после добавления контакта
     * @param $arFields - Данные контакта
     */
    public static function onAfterCrmContactAdd(&$arFields)
    {
        self::addJobToEs($arFields['ID'], 'INSERT');

        if ($arFields["ASSIGNED_BY_ID"] > 0 && $arFields["ASSIGNED_BY_ID"] != USER_ID_LANDER) {
            self::changeDepartment($arFields["ID"], $arFields["ASSIGNED_BY_ID"]);
        }
    }

    /**
     * Событие возникате после удаления контакта
     * @param $id - Ид контакта
     */
    public static function onAfterCrmContactDelete($id)
    {
        self::addJobToEs($id, 'REMOVE');
    }

    /**
     * Событие возникает после обновления контакта
     * @param $arFields - данные контакта
     */
    public static function onAfterCrmContactUpdate(&$arFields)
    {
        self::addJobToEs($arFields['ID'], 'UPDATE');

        if ($arFields["ASSIGNED_BY_ID"] > 0 && $arFields["ASSIGNED_BY_ID"] != USER_ID_LANDER) {
            global $USER;
            $objUser = $USER->GetById($arFields["ASSIGNED_BY_ID"]);
            if ($userParams = $objUser->Fetch()) {
                if (in_array(5, $userParams["UF_DEPARTMENT"])) {
                    self::updateUserField($arFields['ID'], PROP_CONTACT_SOURCE_DESCRIPTION, "Школа Бизнеса");
                }
                self::changeDepartment($arFields['ID'], $arFields["ASSIGNED_BY_ID"]);
            }
        }
    }
}