<?php
/**
 * Created by PhpStorm.
 * User: MKarpychev
 * Date: 19.10.2015
 * Time: 11:17
 */

namespace Synergy\Crm;


use Bitrix\Main\Web\HttpClient;

class Deal extends Entity
{
    const PROP_DEPARTMENT = 'UF_CRM_5602E8F15CCD1';

    /**
     * Добавляем в очередь PAP задание
     * @param $arFields - массив данных сделки
     */
    private function addJobToPap($arFields, $type){
        $client = new HttpClient();
        $arData = [
            'entityId' => $arFields['ID'],
            'entityType' => static::getType(),
            'jobType' => $type,
            'visitorId' => $arFields['UF_PAP_VISITOR_ID'],
            'cost' => $arFields['OPPORTUNITY']
        ];
        $data = json_encode($arData);
        $client->post('https://127.0.0.1/api/pap/jobs', $data);
    }

    /**
     * Функция возвращает данные для индексации Es
     * @param $arFields - массив данных Сделки
     * @return array - данные для Es
     */
    public static function getDataForEsByArray($arFields){
        $arStage = \CCrmStatus::GetStatusList('DEAL_STAGE');
        $arResult = [
            'assigned_by' => [
                'department' => implode('/', self::getDepartment($arFields['ASSIGNED_BY_ID'], 'name')),
                'id' => $arFields['ASSIGNED_BY_ID'],
                'first_name' => $arFields['ASSIGNED_BY_NAME'],
                'last_name' => $arFields['ASSIGNED_BY_LAST_NAME'],
                'second_name' => $arFields['ASSIGNED_BY_SECOND_NAME'],
                'url' => \CComponentEngine::MakePathFromTemplate(
                    '/company/personal/user/#user_id#/',
                    array('user_id' => $arFields['ASSIGNED_BY_ID'])
                )
            ],
            'begin_date' => $arFields['BEGINDATE'],
            'close_date' => $arFields['CLOSEDATE'],
            'contact' => [],
            'id' => $arFields['ID'],
            'stage' => [
                'id' => $arFields['STAGE_ID'],
                'title' => $arStage[$arFields['STAGE_ID']]
            ],
            'title' => $arFields['TITLE'],
            'url' => \CComponentEngine::MakePathFromTemplate(\COption::GetOptionString('crm', 'path_to_deal_show'),
                array(
                    'deal_id' => $arFields['ID']
                )
            )
        ];
        $arPerms = \CCrmPerms::GetEntityAttr('DEAL', array($arFields['ID']));
        $arResult['perms'] = $arPerms[$arFields['ID']];

        if ($arFields['CONTACT_ID'] > 0){
            $arResult['contact'] = [
                'id' => $arFields['CONTACT_ID'],
                'title' => $arFields['CONTACT_FULL_NAME'],
                'url' => \CComponentEngine::MakePathFromTemplate(\COption::GetOptionString('crm', 'path_to_contact_show'),
                    array(
                        'contact_id' => $arFields['CONTACT_ID']
                    ))
            ];
            $obRes = \CCrmFieldMulti::GetList(array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arFields['CONTACT_ID']));
            while ($arRes = $obRes->Fetch()) {
                if ($arRes['TYPE_ID'] == 'PHONE')
                    $arResult['contact']['phones'][] = $arRes['VALUE'];
                if ($arRes['TYPE_ID'] == 'EMAIL')
                    $arResult['contact']['emails'][] = $arRes['VALUE'];
            }
        }
        return $arResult;
    }

    /**
     * Функция возвращает данные для ElasticSearch
     * @param $Id - Ид Контакта
     * @return array - массив данеых
     */
    public static function getDataForEsById($Id)
    {

        $list = \CCrmDeal::GetListEx(array(), array('CHECK_PERMISSIONS' => 'N', 'ID' => $Id));
        if ($row = $list->GetNext()) {
            return self::getDataForEsByArray($row);
        }
        return [];
    }

    /**
     * Функция возвращает тип сущности CRM
     * @return string
     */
    public static function getType()
    {
        return 'DEAL';
    }

    /**
     * Событие вызываентся после добавления сделки
     * @param $arFields - Данные контакта
     */
    public static function onAfterCrmDealAdd(&$arFields)
    {
        self::addJobToEs($arFields['ID'], 'INSERT');
        if (!empty($arFields['CONTACT_ID'])) {
            Contact::addJobToEs($arFields['CONTACT_ID'], 'UPDATE');
        }
        if (!empty($arFields['UF_PAP_VISITOR_ID'])) {
            self::addJobToPap($arFields, 'INSERT');
        }
    }

    /**
     * Событие возникате после удаления сделки
     * @param $id - Ид сделки
     */
    public static function onAfterCrmDealDelete($id)
    {
        self::addJobToEs($id, 'REMOVE');
    }

    /**
     * Событие возникает после обновления сделки
     * @param $arFields - данные седкли
     */
    public static function onAfterCrmDealUpdate(&$arFields)
    {
        self::addJobToEs($arFields['ID'], 'UPDATE');
        if (!empty($arFields['CONTACT_ID'])) {
            Contact::addJobToEs($arFields['CONTACT_ID'], 'UPDATE');
        }
        if (!empty($arFields['UF_PAP_VISITOR_ID'])) {
            self::addJobToPap($arFields, 'UPDATE');
        }
    }
}