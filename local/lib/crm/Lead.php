<?php
/**
 * Created by PhpStorm.
 * User: MKarpychev
 * Date: 23.09.2015
 * Time: 11:26
 */

namespace Synergy\Crm;

use Bitrix\Main\Loader;
use Bitrix\Main\Web\HttpClient;
use Synergy\FileLogger;
use Synergy\IBlock\Constants as IBlockConstants;
use Synergy\IBlock\Constants;

/**
 * Class CrmLead - Работа с лидами CRM
 * @package Synergy\Crm
 */
class Lead extends Entity
{
    #region constants
    const PROP_CAMPAIGN_CODE = 'UF_CRM_1417763938';
    const PROP_CITY = 'UF_CRM_1417767839';
    const PROP_CLASS = 'UF_CRM_1423056666';
    const PROP_COUNTRY = 'UF_CRM_1417767787';
    const PROP_DISPATCHER = 'UF_CRM_1427878845';
    const PROP_LAND_CODE = 'UF_CRM_1417763397';
    const PROP_SCHOOL = 'UF_CRM_1423056426';
    const PROP_SOURCE_CODE = 'UF_CRM_1417763889';
    const PROP_NEW_OLD = 'UF_CRM_1428579860';
    const PROP_GETRESPONSE = 'UF_GETRESPONSE';
    const PROP_EDUCATION_LEVEL = 'UF_EDUCATION_LEVEL';
    const PROP_PRODUCT = 'UF_CRM_1424241721';
    const PROP_IP = 'UF_IP';
    const PROP_DEPARTMENT = 'UF_CRM_1434961567';

    #endregion

    private function addJobToPap($arData, $visitorId, $type = 'INSERT')
    {

        $arStatuses = \CCrmStatus::GetStatusList('STATUS');
        $client = new HttpClient();
        if ($type === 'INSERT') {
            $arResult = [
                'dateCreate' => date('d.m.Y H:i:s'),
                'educationLevel' => $arData[self::PROP_EDUCATION_LEVEL],
                'entityId' => $arData['ID'],
                'entityType' => static::getType(),
                'ip' => $arData[self::PROP_IP],
                'jobType' => 'INSERT',
                'sourceCode' => $arData[self::PROP_SOURCE_CODE],
                'status' => $arStatuses[$arData['STATUS_ID']],
                'title' => $arData['TITLE'],
                'visitorId' => $visitorId
            ];
        } else if ($type === 'UPDATE') {
            $arResult = [
                'entityId' => $arData['ID'],
                'entityType' => static::getType(),
                'jobType' => 'UPDATE',
                'status' => $arStatuses[$arData['STATUS_ID']]
            ];
        }
        $data = json_encode($arResult);
        $client->post('https://127.0.0.1/api/pap/jobs', $data);
    }

    private static function addToInfinity($arData)
    {
        $log = new FileLogger($_SERVER['DOCUMENT_ROOT'] . "/leads.txt");
        $phone = $arData['phone'];
        $date = new \DateTime();
        $hour = $date->format('G');
        $dayOfWeek = $date->format('w');
        //working days
        if (($dayOfWeek > 0 && $dayOfWeek < 6)) {
            if ($hour >= 9 && $hour <= 18) {
                $log->log('Отправляем лида в инфинити на дозвон ' . $arData['tableName']);
                $client = new HttpClient();
                $firstLetter = (int)$phone[0];
                $twoLetter = (int)substr($phone, 0, 2);
                //Казахстан
                if ($twoLetter === 77 || $twoLetter === 76) {
                    $phone = '810' . $phone;
                } else if ($firstLetter === 7 || $firstLetter === 8) {
                    $phone[0] = 8;
                } else {
                    $phone = '810' . $phone;
                }
                $arData = [
                    'landCode' => $arData['landCode'],
                    'leadId' => $arData['id'],
                    'phone' => $phone,
                    'tableName' => $arData['tableName'],
                    'title' => $arData['title']
                ];
                $client->post('https://127.0.0.1/api/infinity/leads', $arData);
            }
        }
    }

    /**
     * Распределение лида между указанными сотрудниками
     * @param $landCode - код Ленжа
     * @param $arUsers - массив сотрудников
     */
    private static function allocationLandsForInfinity($landCode, $arUsers)
    {
        $fileName = $_SERVER['DOCUMENT_ROOT'] . "/api/storage/app/infinity_" . $landCode . ".json";
        if (!file_exists($fileName)) {
            foreach ($arUsers as $item) {
                $arDispatch[$item] = 0;
            }
            file_put_contents($fileName, json_encode($arDispatch));
        } else {
            $fileContent = file_get_contents($fileName);
            $arDispatch = json_decode($fileContent, true);
        }
        //check users online
        foreach ($arDispatch as $key => $user) {
            if (\CUser::IsOnLine($key)) {
                $arOnlineUsers[$key] = $arDispatch[$key];
            }
        }
        //select dispatcher
        $userId = self::selectDispatcher($arDispatch, $arOnlineUsers);
        file_put_contents($fileName, json_encode($arDispatch));
        return $userId;
    }

    /**
     * Функция распределения лида
     * @param $arFields - данные лида
     * @return bool|int
     */
    public static function allocationLead($arFields)
    {
        $arInfinityLands = self::getLandsForInfinity();
        $arPhones = [];
        $arEmails = [];
        $isNew = false;
        $responsibleId = USER_ID_LANDER;
        $land = $arFields[self::PROP_LAND_CODE];
        $log = new FileLogger($_SERVER['DOCUMENT_ROOT'] . "/leads.txt");
        $log->log('=====================================================');
        $log->log('Проверка лида ' . $arFields['ID'], FileLogger::NOTICE);

        #region Берем данные из Elastic
        $client = new HttpClient();
        $response = $client->get('https://127.0.0.1/api/es/crm/lead/' . $arFields['ID']);
        if ($client->getStatus() === 404) {
            $obRes = \CCrmFieldMulti::GetList(array('ID' => 'asc'), array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $arFields['ID']));
            while ($arRes = $obRes->Fetch()) {
                if ($arRes['TYPE_ID'] === 'PHONE') {
                    $arPhones[] = preg_replace("/\D/", "", $arRes['VALUE']);
                }
                if ($arRes['TYPE_ID'] === 'EMAIL') {
                    $arEmails[] = trim($arRes['VALUE']);
                }
            }
            unset($arRes, $obRes);
        } else {
            if ($client->getStatus() === 200) {
                $arResponse = json_decode($response, true);
                $arPhones = $arResponse['phones'];
                $arEmails = $arResponse['emails'];
            }
        }
        unset($client);
        #endregion
        $phone = count($arPhones) > 0 ? $arPhones[0] : '';
        $email = count($arEmails) > 0 ? $arEmails[0] : '';
        #region Обработка new/old
        $isNew = true;
        if (strlen($phone) > 0) {
            $log->log('Обработка new/old');
            $client = new HttpClient();
            $arData = json_encode(['id' => $arFields['ID'], 'phone' => $phone]);
            $client->post('https://127.0.0.1/api/es/crm/searchByPhone', $arData);
            if ($client->getStatus() === 200) {
                $log->log('Телефон в базе есть. Помечаем old');
                $isNew = false;
            } else {
                $isNew = true;
            }
            unset($client);
        }
        //Ищем по почте
        if ($isNew && strlen($email) > 0) {
            $arData = json_encode(['id' => $arFields['ID'], 'mail' => $email]);
            $client = new HttpClient();
            $result = $client->post('https://127.0.0.1/api/es/crm/searchByMail', $arData);
            if ($client->getStatus() === 200) {
                $log->log('Почта в базе есть. Помечаем old');
                $isNew = false;
            } else {
                $isNew = true;
            }
            unset($client);
        }
        if (!$isNew) {
            self::updateUserField($arFields['ID'], self::PROP_NEW_OLD, 'old');

        } else {
            self::updateUserField($arFields['ID'], self::PROP_NEW_OLD, 'new');
            $log->log('Почты в базе нет. Помечаем new');
            $log->log('Телефона в базе нет. Помечаем new');
        }
        #endregion
        #region Обработка дубликата
        if (strlen($phone) > 0) {
            $log->log('Проверка на дубли по телефону', FileLogger::NOTICE);
            $arData = json_encode(['id' => $arFields['ID'], 'phone' => $phone, 'landCode' => $land]);
            $client = new HttpClient();
            $client->post('https://127.0.0.1/api/es/crm/lead/searchDuplicate', $arData);
            if ($client->getStatus() === 200) {
                $log->log('Дубликат найден!', FileLogger::NOTICE);
                self::changeStatus($arFields['ID'], 5);
                $log->log('Статус изменен на дубль!', FileLogger::NOTICE);
            } else {
                $log->log('Проверка на повторные заявки');
                $arData = json_encode(['id' => $arFields['ID'], 'phone' => $phone]);
                $client = new HttpClient();
                $response = $client->post('https://127.0.0.1/api/es/crm/lead/searchRepeat', $arData);
                if ($client->getStatus() === 200) {
                    $arResponse = json_decode($response, true);
                    if ($arResponse['assigned_by_id'] > 0) {
                        $log->log('Найден повтор по телефону', FileLogger::NOTICE);
                        self::changeStatus($arFields['ID'], 'DETAILS');
                        $log->log('Статус изменен на повтор!', FileLogger::NOTICE);
                    }
                }
            }
            unset($client);
        }
#endregion

        if (array_key_exists($land, $arInfinityLands)) {
            self::addToInfinity([
                'id' => $arFields['ID'],
                'landCode' => $land,
                'phone' => $phone,
                'tableName' => $land,
                'title' => $arFields['NAME'] . ' ' . $arFields['LAST_NAME']
            ]);
            $responsibleId = self::allocationLandsForInfinity($land, $arInfinityLands[$land]);
            $log->log('Распределяем на:' . $responsibleId);
        } else {
            #region Выбор ответственного
            // Проверка если лид новый
            /*   if ($isNew) {
                   self::addToInfinity([
                       'id' => $arFields['ID'],
                       'landCode' => $land,
                       'phone' => $phone,
                       'tableName' => 'leads',
                       'title' => $arFields['NAME'] . ' ' . $arFields['LAST_NAME']
                   ]);
                   $log->log('=====================================================');
                   unset($log);
                   return getenv('USER_AUTOREDIAL');
               }*/
            // Поиск контакта по телефону  и email
            $arContact = [];
            if (strlen($phone) > 0) {
                $log->log('Поиск контакта по телефону', FileLogger::NOTICE);
                $client = new HttpClient();
                $response = $client->get('https://127.0.0.1/api/es/crm/contact/phone/' . $phone);
                if ($client->getStatus() === 200) {
                    $arContact = json_decode($response, true);
                }
                unset($client);
            }
            if (empty($arContact) && strlen($email) > 0) {
                $log->log('Поиск контакта по email', FileLogger::NOTICE);
                $client = new HttpClient();
                $response = $client->get('https://127.0.0.1/api/es/crm/contact/mail/' . $email);
                if ($client->getStatus() === 200) {
                    $arContact = json_decode($response, true);
                }
                unset($client);
            }
            //Контакт найден
            if (!empty($arContact)) {
                $log->log('Контакт найден: ' . $arContact['_id'], FileLogger::NOTICE);
                $log->log('SourceCodeTarget: ' . $arContact['source_code'], FileLogger::NOTICE);
                $log->log('SourceCodeSource: ' . $arFields['SOURCE_DESCRIPTION'], FileLogger::NOTICE);
                $arFields['CONTACT_ID'] = $arContact['_id'];
                $responsibleId = $arContact['assigned_by_id'];
                if ($arContact['source_code'] === $arFields['SOURCE_DESCRIPTION']) {
                    $contactDate = Contact::getDateLastActivity($arContact['_id']);
                    $lastDate = new \DateTime();
                    $cntMonth = $lastDate->diff(new \DateTime($contactDate))->format('%m');
                    if ($cntMonth < 4) {
                        $log->log('Ответственный установлен из контакта: ' . $responsibleId);
                        if (!self::userIsActive($responsibleId)) {
                            $responsibleId = self::startBPAllocation($arFields['ID']);
                        } else {
                            self::changeResponsible($arFields['ID'], $responsibleId);
                        }

                    } else {
                        $responsibleId = self::startBPAllocation($arFields['ID']);
                    }
                } else {
                    $log->log('Поиск ответственных через БП');
                    $responsibleId = self::startBPAllocation($arFields['ID']);
                    $log->log('Ответственный найден: ' . $responsibleId);
                }
            } else {
                //Если контакт не найден
                $log->log('Контакт не найден!');
                $log->log('Ищем ответственного через БП');
                $responsibleId = self::startBPAllocation($arFields['ID']);
                $log->log('Ответственный найден: ' . $responsibleId);
            }
            #endregion
        }
        if ($phone === '71000000000') {
            self::changeStatus($arFields['ID'], 11);
        }
        $log->log('=====================================================');
        unset($log);
        return $responsibleId;
    }

    /**
     * Функция распредляет лиды с ответственным Lender
     */
    public
    static function allocationLeads()
    {
        $_SESSION["SESS_AUTH"]["USER_ID"] = USER_ID_LANDER;
        $list = \CCrmLead::GetList(array(),
            array(
                'CHECK_PERMISSIONS' => 'N',
                'ASSIGNED_BY_ID' => USER_ID_LANDER,
                'STATUS_ID' => ['NEW', '5'],
                'UF_NOT_ALLOCATION' => false
            ),
            array('ID', self::PROP_LAND_CODE, 'SOURCE_DESCRIPTION', 'UF_NOT_ALLOCATION'),
            false);
        while ($row = $list->GetNext()) {
            //Телефоны и email
            $responsibleId = self::allocationLead($row);
            if ($responsibleId === USER_ID_LANDER) {
                self::updateUserField($row['ID'], 'UF_NOT_ALLOCATION', 1);
            } else {
                if (self::userIsActive($responsibleId)) {
                    self::changeResponsible($row['ID'], $responsibleId);
                    //Актуализируем структурное подразделение
                    self::changeDepartment($row["ID"], $responsibleId);
                }
            }
        }
    }

    /**
     * Функция распределения лида на диспетчерскую
     * @param $id - ид лида
     * @param $sectionId - ид секции инфоблока, где указаны диспетчеры
     * @throws \Bitrix\Main\LoaderException
     */
    public
    static function allocationToDispatch($id, $sectionId)
    {
        Loader::includeModule('iblock');
        $fileName = $_SERVER['DOCUMENT_ROOT'] . "/api/storage/app/dispatch.json";
        $arDispatch = [];
        if (!file_exists($fileName)) {
            $rsSection = \CIBlockSection::GetList([], [
                'IBLOCK_ID' => IBlockConstants::ALLOCATION,
                'CHECK_PERMISSION' => 'N',
                'ID' => $sectionId,
            ], false, ['UF_DISPATCHER']);
            if ($row = $rsSection->Fetch()) {
                $arUsers = $row['UF_DISPATCHER'];
                foreach ($arUsers as $item) {
                    $arDispatch[$item] = 0;
                }
                file_put_contents($fileName, json_encode($arDispatch));
            }
        } else {
            $fileContent = file_get_contents($fileName);
            $arDispatch = json_decode($fileContent, true);
        }

        $date = new \DateTime();
        $hour = $date->format('G');
        $dayOfWeek = $date->format('w');
        $arOnlineUsers = [];
        $isHoliday = false;
        if (!$isHoliday) {
            //будни
            if (($dayOfWeek > 0 && $dayOfWeek < 6)) {
                //с 5 до 9 утра
                if (($hour >= 5 && $hour < 9)) {
                    $responsibleId = 3225;
                } else if ($hour >= 9 && $hour <= 21) {
                    //проверка юзеров на онлайн
                    foreach ($arDispatch as $key => $user) {
                        if (\CUser::IsOnLine($key)) {
                            $arOnlineUsers[$key] = $arDispatch[$key];
                        }
                    }
                    //Выбираем диспетчера
                    $responsibleId = self::selectDispatcher($arDispatch, $arOnlineUsers);
                } else {
                    $responsibleId = self::selectDispatcher($arDispatch);
                }
            } else {
                //Остальное время
                $responsibleId = self::selectDispatcher($arDispatch);
            }
        } else {
            $responsibleId = self::selectDispatcher($arDispatch);
        }
        if ($responsibleId !== USER_ID_LANDER) {
            file_put_contents($fileName, json_encode($arDispatch));
            self::changeResponsible($id, $responsibleId);
        }

    }

    /**
     * Функция изменения ответственного у лида
     * @param $leadId - Ид Лида
     * @param $responsibleId - Ид Ответственного
     */
    public
    static function changeResponsible($leadId, $responsibleId)
    {
        $crmLead = new \CCrmLead();
        $arFields = [
            "ASSIGNED_BY_ID" => $responsibleId,
            self::PROP_DISPATCHER => $responsibleId,
            "MODIFY_BY_ID" => USER_ID_LANDER
        ];
        $crmLead->Update($leadId, $arFields, true, false, ['REGISTER_SONET_EVENT' => true]);
    }

    /**
     * Функция изменяет статус лида
     * @param $leadId - Ид Лида
     * @param $statusId - Ид Статуса
     * @return bool - Результат изменения
     */
    public
    static function changeStatus($leadId, $statusId)
    {
        $crmLead = new \CCrmLead(false);
        $arFields = ['STATUS_ID' => $statusId];
        $bSuccess = $crmLead->Update($leadId, $arFields, false, false);
        unset($crmLead);
        return $bSuccess;
    }

    /**
     * Ковертация лида в сделку
     * @param $arFields - массив данных Лида
     * @param $assignedId - ответственный за сделку
     * @param string $stageId - стадия сделки
     * @return bool - результат конвертации
     */
    public
    static function convertToDeal($arFields, $assignedId, $stageId = 'NEW')
    {
        $beginDate = time() + \CTimeZone::GetOffset();
        $time = localtime($beginDate, true);
        $beginDate -= $time['tm_sec'];

        $arDealFields = array(
            'TITLE' => strlen($arFields['FULL_NAME']) > 0 ? $arFields['FULL_NAME'] : $arFields['TITLE'],
            'BEGINDATE' => ConvertTimeStamp($beginDate, 'FULL', SITE_ID),
            'CONTACT_ID' => $arFields['CONTACT_ID'],
            'LEAD_ID' => $arFields['ID'],
            'ASSIGNED_BY_ID' => $assignedId,
            'STAGE_ID' => $stageId
        );
        $CCrmDeal = new \CCrmDeal(false);
        $dealId = $CCrmDeal->Add($arDealFields, true, array('REGISTER_SONET_EVENT' => true));
        if ($dealId > 0) {
            $CCrmLead = new \CCrmLead();
            $arLeadFields = array('STATUS_ID' => 'CONVERTED', 'CONTACT_ID' => $arFields['CONTACT_ID']);
            if ($CCrmLead->Update($arFields['ID'], $arLeadFields, true, true, array('REGISTER_SONET_EVENT' => true))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Функция по создании лида
     * @param $arData - массив данных
     * @return int|string - Ид созданного лида
     * @throws \Bitrix\Main\LoaderException
     */
    public
    static function createLead($arData)
    {
        Loader::includeModule('crm');
        $arFields = [];
        $arFieldsMaps = [
            'birthDate' => 'BIRTHDATE',
            'campaign' => 'UF_CRM_1417763938',
            'city' => 'UF_CRM_1417767839',
            'comments' => 'COMMENTS',
            'country' => 'UF_CRM_1417767787',
            'education' => 'UF_EDUCATION_LEVEL',
            'ip' => self::PROP_IP,
            'landerCode' => 'UF_LANDER_CODE',
            'landCode' => 'UF_CRM_1417763397',
            'landName' => 'UF_CRM_1433230500',
            'medium' => 'UF_CRM_1417764328',
            'form' => 'UF_CRM_1433230438',
            'NAME' => 'TITLE',
            'PAPVisitorId' => 'UF_PAP_VISITOR_ID',
            'partnerName' => 'UF_CRM_1419509253',
            'phoneIsValid' => 'UF_CRM_1417764508',
            'productName' => 'UF_CRM_1424241721',
            'sourceName' => 'SOURCE_ID',
            'sourceDesc' => 'SOURCE_DESCRIPTION',
            'sourceCode' => 'UF_CRM_1417763889',
            'term' => 'UF_CRM_1417764366',
            'timeCall' => 'UF_CRM_1425297220',
            'url' => 'UF_CRM_1417448225',
            'version' => 'UF_CRM_1422017692',
            'visitType' => 'UF_CRM_1436793982'
        ];
        foreach ($arFieldsMaps as $key => $mapKey) {
            if (isset($arData[$key])) {
                $arFields[$mapKey] = trim($arData[$key]);
            }
        }
        //Web Mail Version
        if ($arData['sourceCode'] === 'maillist') {
            $arFields['UF_GETRESPONSE'] = $arData['campaign'];
        }
        if (isset($arData['email'])) {
            $arFields['FM']['EMAIL'] = array(
                'n0' => array('VALUE' => strtolower($arData['email']), 'VALUE_TYPE' => 'HOME'));
        }
        if (isset($arData['phone'])) {
            $arFields['FM']['PHONE'] = array(
                'n0' => array('VALUE' => $arData['phone'], 'VALUE_TYPE' => 'MOBILE'));
        }
        $arFields['ASSIGNED_BY_ID'] = USER_ID_LANDER;
        $arFields['CREATED_BY_ID'] = USER_ID_LANDER;
        $crmLead = new \CCrmLead(false);
        $Id = $crmLead->Add($arFields, true);
        return $Id > 0 ? $Id : $crmLead->LAST_ERROR;

    }

    /**
     * Поиск лидов по email, исключая текущий
     * @param $leadId - Ид Лида
     * @param $email - Email
     * @param $sourceCode - источник
     * @return array - массив найденных лидов
     */
    private
    static function findByEmail($leadId, $email, $sourceCode)
    {
        $arLeads = array();
        $arSelect = array('ASSIGNED_BY_ID', 'DATE_MODIFY', 'ID', 'TITLE');
        $arFilter = array(
            'CHECK_PERMISSIONS' => 'N',
            '!ID' => $leadId,
            '!STATUS' => array('JUNK', 2, 12, 13, 14, 15, 16, 8, 9, 5, 10, 11),
            PROP_LEAD_SOURCE => $sourceCode,
            'FM' => array('EMAIL' => array('VALUE' => $email))
        );
        $list = \CCrmLead::GetList(array('DATE_MODIFY' => 'DESC'), $arFilter, $arSelect, false);
        while ($row = $list->Fetch()) {
            $arLeads[$row['ID']] = array(
                'ASSIGNED_BY_ID' => $row['ASSIGNED_BY_ID'],
                'ID' => $row['ID'],
                'LAST_DATE' => $row['DATE_MODIFY']
            );
        }
        return $arLeads;
    }

    /**
     * Поиск лидов по email для распределения
     * @param $leadId - Ид Лида
     * @param $email - EMail
     * @param $sourceCode - Структурное подразделение
     * @return array - данные о найденном лиде
     */
    public
    static function findByEmailForAllocation($leadId, $email, $sourceCode)
    {
        $arLeads = self::findByEmail($leadId, $email, $sourceCode);
        $date = '';
        $lastLeadId = 0;
        if (count($arLeads) > 0) {
            foreach ($arLeads as $key => $item) {
                if ($item['LAST_DATE'] > $date) {
                    $date = $item['LAST_DATE'];
                    $lastLeadId = $key;
                }
            }
            return $arLeads[$lastLeadId];
        }
        return [];
    }

    /**
     * Поиск телефона с учетом статуса лида, исключая заданного
     * @param $leadId - Ид Лида
     * @param $phone - телефон
     * @param $sourceCode - источник
     * @return array - массив данных о лиде
     */
    private
    static function findByPhone($leadId, $phone, $sourceCode)
    {
        $arLeads = array();
        $arSelect = array('ASSIGNED_BY_ID', 'DATE_MODIFY', 'ID', 'TITLE');
        $arFilter = array(
            'CHECK_PERMISSIONS' => 'N',
            '!ID' => $leadId,
            '!STATUS' => array('JUNK', 2, 12, 13, 14, 15, 16, 8, 9, 5, 10, 11),
            PROP_LEAD_SOURCE => $sourceCode,
            'FM' => array('PHONE' => array('VALUE' => $phone))
        );
        $list = \CCrmLead::GetList(array('DATE_MODIFY' => 'DESC'), $arFilter, $arSelect, false);
        while ($row = $list->Fetch()) {
            $arLeads[$row['ID']] = array(
                'ASSIGNED_BY_ID' => $row['ASSIGNED_BY_ID'],
                'ID' => $row['ID'],
                'LAST_DATE' => $row['DATE_MODIFY']
            );
        }
        return $arLeads;
    }

    /**
     * Поиск лида по телефону, исключая заданного
     * @param $leadId - Ид Лида
     * @param $phone - Телефон
     * @param $sourceCode - структурное подразделение
     * @return array - Возвращает данные о найденном контакте или пустой массив
     */
    public
    static function findByPhoneForAllocate($leadId, $phone, $sourceCode)
    {
        $phone = preg_replace('/\D/', '', $phone);
        $firstLetter = substr($phone, 0, 1);
        if (($firstLetter === 7 || $firstLetter === 8) && strlen($phone) === 11) {
            $phone = substr($phone, 1);
            $arLeads = self::findByPhone($leadId, '7' . $phone, $sourceCode);
            $arLeads = array_merge($arLeads, self::findByPhone($leadId, '8' . $phone, $sourceCode));
        } else {
            $arLeads = self::findByPhone($leadId, $phone, $sourceCode);
        }
        $date = '';
        $lastLeadId = 0;
        if (count($arLeads) > 0) {
            foreach ($arLeads as $key => $item) {
                if ($item['LAST_DATE'] > $date) {
                    $date = $item['LAST_DATE'];
                    $lastLeadId = $key;
                }
            }
            return $arLeads[$lastLeadId];
        }
        return array();
    }

    /**
     * Функция возвращает подготовленные данные для ElasticSearch
     * @param $arFields - массив данных Лида
     * @return array - массив данных для Es
     */
    public
    static function getDataForEsByArray($arFields)
    {
        $arStatus = \CCrmStatus::GetStatusList('STATUS');
        $fullName = $arFields['LAST_NAME'];
        if (!empty($arFields['NAME']))
            $fullName .= ' ' . $arFields['NAME'];
        if (!empty($arFields['SECOND_NAME']))
            $fullName .= ' ' . $arFields['SECOND_NAME'];

        $arResult = [
            'assigned_by_id' => $arFields['ASSIGNED_BY_ID'],
            'assigned_by' => [
                'department' => implode(' / ', self::getDepartment($arFields['ASSIGNED_BY_ID'], 'name')),
                'id' => $arFields['ASSIGNED_BY_ID'],
                'first_name' => $arFields['ASSIGNED_BY_NAME'],
                'last_name' => $arFields['ASSIGNED_BY_LAST_NAME'],
                'second_name' => $arFields['ASSIGNED_BY_SECOND_NAME'],
                'url' => \CComponentEngine::MakePathFromTemplate(
                    '/company/personal/user/#user_id#/',
                    array('user_id' => $arFields['ASSIGNED_BY_ID'])
                )
            ],
            'birth_date' => $arFields['BIRTHDATE'],
            'campaign_code' => $arFields[self::PROP_CAMPAIGN_CODE],
            'city' => $arFields[self::PROP_CITY],
            'class' => $arFields[self::PROP_CLASS],
            'company' => $arFields['COMPANY_TITLE'],
            'country' => $arFields[self::PROP_COUNTRY],
            'date_create' => $arFields['DATE_CREATE'],
            'date_change' => $arFields['DATE_MODIFY'],
            'desc' => $arFields['SOURCE_DESCRIPTION'],
            'first_name' => $arFields['NAME'],
            'full_name' => $fullName,
            'emails' => [],
            'id' => $arFields['ID'],
            'land_code' => $arFields[self::PROP_LAND_CODE],
            'last_name' => $arFields['LAST_NAME'],
            'phones' => [],
            'school' => [],
            'source_code' => !empty($arFields[self::PROP_SOURCE_CODE]) ? $arFields[self::PROP_SOURCE_CODE] : $arFields['SOURCE_DESCRIPTION'],
            'second_name' => $arFields['SECOND_NAME'],
            'status' => [
                'id' => $arFields['STATUS_ID'],
                'title' => $arStatus[$arFields['STATUS_ID']]
            ],
            'title' => $arFields['TITLE'],
            'url' => \CComponentEngine::MakePathFromTemplate(\COption::GetOptionString('crm', 'path_to_lead_show'),
                array(
                    'lead_id' => $arFields['ID']
                )
            ),
        ];
        $arPerms = \CCrmPerms::GetEntityAttr('LEAD', array($arFields['ID']));
        $arResult['perms'] = $arPerms[$arFields['ID']];

        if (!empty($arFields[self::PROP_SCHOOL])) {
            $res = \CCrmCompany::GetList([], ['ID' => $arFields[self::PROP_SCHOOL], 'CHECK_PERMISSIONS' => 'N'], ['ID', 'TITLE']);
            if ($arSchool = $res->GetNext()) {
                $arResult['school'] = [
                    'id' => $arSchool['ID'],
                    'title' => $arSchool['TITLE'],
                    'url' => \CComponentEngine::MakePathFromTemplate(
                        '/crm/company/show/#company_id#/',
                        array('company_id' => $arSchool['ID'])
                    )
                ];
            }

        }
        if (!empty($arFields[self::PROP_CLASS])) {
            $rsClass = \CUserFieldEnum::GetList(array(), array("ID" => $arFields[self::PROP_CLASS]));
            if ($arClass = $rsClass->GetNext()) {
                $arResult['class'] = $arClass["VALUE"];
            }
        }
        $arMultiFields = array();
        $obRes = \CCrmFieldMulti::GetList(array('ID' => 'asc'), array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $arFields['ID']));
        while ($arRes = $obRes->Fetch()) {
            $arMultiFields[] = array(
                'ID' => $arRes['ID'],
                'TYPE_ID' => $arRes['TYPE_ID'],
                'VALUE_TYPE' => $arRes['VALUE_TYPE'],
                'VALUE' => $arRes['VALUE']
            );
            if ($arRes['TYPE_ID'] === 'PHONE') {
                $arResult['phones'][] = $arRes['VALUE'];
            }

            if ($arRes['TYPE_ID'] === 'EMAIL') {
                $arResult['emails'][] = $arRes['VALUE'];
            }
        }
        $arResult['multi_fields'] = $arMultiFields;
        return $arResult;
    }

    /**
     * Функция возвращает данные для ElasticSearch
     * @param $leadId - Ид Лида
     * @return array - массив данных
     */
    public
    static function getDataForEsById($leadId)
    {
        $list = \CCrmLead::GetList(array(), array('CHECK_PERMISSIONS' => 'N', 'ID' => $leadId));
        if ($row = $list->GetNext()) {
            return self::getDataForEsByArray($row);
        }
        return [];
    }

    /**
     * Возвращает последнюю дату активности по лиду
     * @param $leadId - ID лида
     * @return mixed - дата
     */
    public
    static function getDateLastActivity($leadId)
    {
        $list = \CCrmEvent::GetListEx(array('CREATED' => 'DESC'), array('ENTITY_ID' => $leadId), false, array('nTopCount' => 1), array('DATE_CREATE'));
        if ($row = $list->Fetch()) {
            return $row['DATE_CREATE'];
        }
        return false;

    }

    /**
     * Возвращает список лендов и сотрудников для автодозвона
     * @return array
     * @throws \Bitrix\Main\LoaderException
     */
    private
    static function getLandsForInfinity()
    {
        $obCache = new \CPHPCache();
        $cacheTime = 1440 * 60;
        $cacheId = 'infinity_lands';
        if ($obCache->InitCache($cacheTime, $cacheId, "/")) {
            $vars = $obCache->GetVars();
            $arLands = $vars['LANDS'];
        } else {
            Loader::includeModule('iblock');
            $arGroups = [];
            $arLands = [];
            $list = \CIBlockElement::GetList(
                [],
                ['IBLOCK_ID' => getenv('IBLOCK_AUTOREDIAL'), 'ACTIVE' => 'Y', 'CHECK_PERMISSIONS' => 'N'],
                false,
                false,
                ['ID', 'IBLOCK_ID', 'NAME', 'CODE', 'PROPERTY_*']);
            while ($row = $list->GetNextElement()) {
                $arProps = $row->GetProperties();
                if (!empty($arProps['USERS']['VALUE']) && !empty($arProps['LANDS']['VALUE'])) {
                    $arUsers = [];
                    foreach ($arProps['USERS']['VALUE'] as $user) {
                        $arUsers[] = $user;
                    }
                    foreach ($arProps['LANDS']['VALUE'] as $land) {
                        $arLands[$land] = $arUsers;
                    }
                }
            }
            $obCache->StartDataCache($cacheTime, $cacheId, "/");
            $obCache->EndDataCache(['LANDS' => $arLands]);
        }
        return $arLands;
    }

    /**
     * Функция возвращает Ид Ответственного за лид
     * @param $leadId - Ид Лида
     * @return int - Ид Ответственного
     */
    public
    static function getResponsibleId($leadId)
    {
        $list = \CCrmLead::GetList([], ['ID' => $leadId, 'CHECK_PERMISSIONS' => 'N'], ['ASSIGNED_BY_ID'], false);
        if ($row = $list->Fetch()) {
            return $row['ASSIGNED_BY_ID'];
        }
        return 0;
    }

    /**
     * Функция возвращает тип сущности CRM
     * @return string
     */
    public
    static function getType()
    {
        return 'LEAD';
    }

    /**
     * Событие вызывается после добавления лида.
     * @param $arFields - Массив данных лида
     */
    public
    static function onAfterCrmLeadAdd($arFields)
    {
        //Индексация ES
        self::addJobToEs($arFields['ID'], 'INSERT');
        if (!empty($arFields['UF_PAP_VISITOR_ID'])) {
            self::addJobToPap($arFields, $arFields['UF_PAP_VISITOR_ID']);
        }
    }

    /**
     * Событие вызывается после удаления лида
     * @param $id - Ид Лида
     */
    public
    static function onAfterCrmLeadDelete($id)
    {
        self::addJobToEs($id, 'REMOVE');
    }

    /**
     * Событие вызывается после обновление лида
     * @param $arFields
     */
    public
    static function onAfterCrmLeadUpdate($arFields)
    {
        self::addJobToEs($arFields['ID'], 'UPDATE');
        self::addJobToPap($arFields, $arFields['UF_PAP_VISITOR_ID'], 'UPDATE');
        // Актуализируем Структурное подразделение
        if ($arFields["ASSIGNED_BY_ID"] > 0 && $arFields["ASSIGNED_BY_ID"] !== USER_ID_LANDER) {
            self::changeDepartment($arFields["ID"], $arFields["ASSIGNED_BY_ID"]);
        }
    }

    /**
     * @param $arDispatch
     * @param $arOnlineUsers
     */
    private
    function selectDispatcher(&$arDispatch, $arOnlineUsers = [])
    {
        if (count($arOnlineUsers) !== 0) {
            asort($arOnlineUsers);
            $responsibleId = array_keys($arOnlineUsers)[0];
            $maxValue = max($arDispatch);
            if ($maxValue - $arOnlineUsers[$responsibleId] > 2) {
                $arDispatch[$responsibleId] = $maxValue;
            } else {
                $arDispatch[$responsibleId]++;
            }
        } else {
            asort($arDispatch);
            $responsibleId = array_keys($arDispatch)[0];
            $maxValue = max($arDispatch);
            if ($maxValue - $arDispatch[$responsibleId] > 2) {
                $arDispatch[$responsibleId] = $maxValue;
            } else {
                $arDispatch[$responsibleId]++;
            }
        }
        return $responsibleId;
    }

    /**
     * Запуск БП лида
     * @param $leadId - Ид Лида
     * @param $workflowId - Шаблон БП
     * @return array|bool - результат выполенния БП
     * @throws \Bitrix\Main\LoaderException
     */
    public
    static function startBP($leadId, $workflowId)
    {
        loader::includeModule('bizproc');
        $arErrors = [];
        \CBPDocument::StartWorkflow(
            $workflowId,
            array("crm", "CCrmDocumentLead", "LEAD_" . $leadId),
            array(),
            $arErrors
        );
        return count($arErrors) > 0 ? $arErrors : true;

    }

    /**
     * Функция запускает бизнес-процесс распределения и возвращает найденного ответственного за лид
     * @param $leadId - Ид Лида
     * @return int - Ид Ответственного
     */
    public
    static function startBPAllocation($leadId)
    {
        $log = new FileLogger($_SERVER['DOCUMENT_ROOT'] . "/leads.txt");
        if (self::startBP($leadId, 19)) {
            $responsibleId = self::getResponsibleId($leadId);
            $log->log('Поиск отв. по менеджерам: ' . $responsibleId);
            if ($responsibleId != USER_ID_LANDER) {
                return $responsibleId;
            }
        }
        if (self::startBP($leadId, 40)) {
            $responsibleId = self::getResponsibleId($leadId);
            $log->log('Поиск отв. по диспетчерской: ' . $responsibleId);
            if ($responsibleId != USER_ID_LANDER) {
                return $responsibleId;
            }
        }
        return USER_ID_LANDER;

    }
}