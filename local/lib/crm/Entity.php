<?php
/**
 * Created by PhpStorm.
 * User: MKarpychev
 * Date: 23.09.2015
 * Time: 12:16
 */

namespace Synergy\Crm;

use Bitrix\Main\Loader;
use Bitrix\Main\Web\HttpClient;

/**
 * Class CrmEntity - Класс для работы с разными сущностями CRM
 * @package Synergy\Crm
 */
class Entity
{
    /**
     * Добавляет пользователям доступ к сущностям crm других подразделений
     * @param $arData
     * @return array
     */
    public static function addPermsToUserAttr()
    {
        global $USER;
        $arData = [];
        $arUserDeps = [];
        $by = 'ID';
        $order = 'ASC';
        $list = \CUser::GetList($by, $order,
            [
                'ID' => $USER->GetID()
            ],
            [
                'SELECT' => ['UF_DEPARTMENT'],
                'FIELDS' => ['ID']
            ]
        );
        if ($row = $list->GetNext()) {
            $arUserDeps = $row['UF_DEPARTMENT'];
        }
        $obCache = new \CPHPCache();
        $cacheTime = 1440 * 60;
        $cacheId = 'crm_access_departments';
        if ($obCache->InitCache($cacheTime, $cacheId, "/")) {
            $vars = $obCache->GetVars();
            $arDeps = $vars['DEPS'];
        } else {
            Loader::includeModule('iblock');
            $arDeps = [];
            $list = \CIBlockElement::GetList(
                [],
                ['IBLOCK_ID' => getenv('IBLOCK_CRM_ACCESS'), 'ACTIVE' => 'Y', 'CHECK_PERMISSIONS' => 'N'],
                false,
                false,
                ['ID', 'IBLOCK_ID', 'NAME', 'CODE', 'PROPERTY_*']);
            while ($row = $list->GetNextElement()) {
                $arProps = $row->GetProperties();
                if (!empty($arProps['DEP_ID']['VALUE']) && !empty($arProps['DEPARTMENTS']['VALUE'])){
                    foreach($arProps['DEP_ID']['VALUE'] as $item){
                        $arDeps[$item] = $arProps['DEPARTMENTS']['VALUE'];
                    }
                }

            }
            $obCache->StartDataCache($cacheTime, $cacheId, "/");
            $obCache->EndDataCache(['DEPS' => $arDeps]);
        }
        //Диспетчеры
        $arDepIds = array_keys($arDeps);
        foreach ($arUserDeps as $groupId) {
            if (in_array($groupId, $arDepIds)) {
                foreach ($arDeps[$groupId] as $item)
                    $arData[] = 'D' . $item;
            }
        }
        return $arData;
    }

    public static function addDepartmentToUserAttr(&$arData)
    {
        global $USER;
        $arGroups = $USER->GetUserGroupArray();
        //Диспетчеры
        if (in_array(19, $arGroups)) {
            $arData[] = 'D4279';
        }
    }

    /**
     * Функция добавляет задание для ES Job
     * @param $id - Ид сущности
     * @param $jobType
     */
    public static function addJobToEs($id, $jobType)
    {
        $client = new HttpClient();
        $arData = [
            'entity_id' => $id,
            'entity_type' => static::getType(),
            'job_type' => $jobType
        ];
        $data = json_encode($arData);
        $client->post('https://127.0.0.1/api/es/jobs', $data);
    }

    public static function calculateSourceCode($userId)
    {
        $sbs = ['ООО "Школа Бизнеса Синергия"','ООО Компания "Частный репетитор"'];
        $list = \CUser::GetList($by, $order, ['ID' => $userId],['FIELDS' => ['WORK_COMPANY']]);
        if ($row = $list->GetNext()){

        }
    }

    /**
     * Функция меняет структурное подразделение
     * @param $id - ид сущности
     * @param $responsibleId - Ид юзера
     */
    public static function changeDepartment($id, $responsibleId)
    {
        $type = static::getType();
        $field = '';
        switch ($type) {
            case 'CONTACT':
                $field = Contact::PROP_DEPARTMENT;
                break;
            case 'LEAD':
                $field = Lead::PROP_DEPARTMENT;
                break;
            case 'INVOICE':
                $field = Invoice::PROP_DEPARTMENT;
                break;
        }
        $arData = self::getDepartment($responsibleId);
        if (!empty($arData) && !empty($field)) {
            self::updateUserField($id, $field, $arData);
        }
        unset($arData);
    }

    /**
     * Поиск crm сущности по телефону, email
     * @param $entityType - сущность CRM (CONTACT, LEAD, DEAL)
     * @param $fmType - тип поля (EMAIL, PHONE)
     * @param $phone - телефон
     * @return int - ID сущности
     */
    public static function findEntityByFM($entityType, $fmType, $phone)
    {
        $list = \CCrmFieldMulti::GetList(array('ELEMENT_ID' => 'asc'),
            array(
                'ENTITY_ID' => $entityType,
                'TYPE_ID' => $fmType,
                'VALUE' => $phone
            ));

        if ($row = $list->Fetch()) {
            return $row['ELEMENT_ID'];
        }
        return 0;
    }

    /**
     * Получить подразделение
     * @param $userId - Ид пользователя
     * @param string $type - тип возвращаемых данных
     * @return array
     */
    public static function getDepartment($userId, $type = 'id')
    {
        $arIds = [];
        $arNames = [];
        $list = \CUser::GetList($by, $order, array('ID' => $userId), array('SELECT' => array('UF_DEPARTMENT'), 'FIELDS' => array('ID')));
        if ($row = $list->GetNext()) {
            if ($row["UF_DEPARTMENT"][0] > 0) {
                $arSelect = Array("ID", "NAME");
                $nav = \CIBlockSection::GetNavChain(false, $row["UF_DEPARTMENT"][0], $arSelect);
                while ($arNav = $nav->Fetch()) {
                    $arIds[] = $arNav["ID"];
                    $arNames[] = $arNav["NAME"];
                }
                unset($arNav);
                unset($nav);
            }
        }
        unset($row);
        unset($list);
        if ($type == 'id')
            return $arIds;
        else if ($type == 'name')
            return $arNames;
        return [];
    }

    /**
     * Тип сущности
     * @return string
     */
    public static function getType()
    {
        return '';
    }

    /**
     * Функция обновляет пользовательское поле Crm сущности
     * @param $leadId - Ид Контакта
     * @param $fieldName - Код пользовательского поля
     * @param $value - значение
     */
    public static function updateUserField($leadId, $fieldName, $value)
    {
        global $USER_FIELD_MANAGER;
        $type = static::getType();
        if ($type !== 'INVOICE')
            $object = 'CRM_' . $type;
        else
            $object = 'ORDER';
        $USER_FIELD_MANAGER->Update($object, $leadId, array($fieldName => $value));
    }

    /**
     * Проверка пользователя на активность
     * @param $userId - Ид Юзера
     * @return bool - результат проверки
     */
    public static function userIsActive($userId)
    {
        $list = \CUser::GetList($by, $order, ['ID' => $userId], ['FIELDS' => ['ACTIVE']]);
        if ($row = $list->GetNext()) {
            return $row['ACTIVE'] === 'Y' ? true : false;
        }
        return false;
    }
}