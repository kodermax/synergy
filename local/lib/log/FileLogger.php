<?php
/**
 * Created by PhpStorm.
 * User: MKarpychev
 * Date: 29.09.2015
 * Time: 14:59
 */

namespace Synergy;



class FileLogger
{
    protected $fileHandle = NULL;
    protected $timeFormat = 'd.m.Y - H:i:s';

    const FILE_CHMOD = 756;

    const NOTICE = '[NOTICE]';
    const WARNING = '[WARNING]';
    const ERROR = '[ERROR]';
    const FATAL = '[FATAL]';

    public function __construct($logfile) {
        if($this->fileHandle == NULL){
            $this->openLogFile($logfile);
        }
    }

    public function __destruct() {
        $this->closeLogFile();
    }

    protected function closeLogFile() {
        if($this->fileHandle != NULL) {
            fclose($this->fileHandle);
            $this->fileHandle = NULL;
        }
    }

    public function log($message, $messageType = FileLogger::NOTICE) {
        if($this->fileHandle == NULL){
            throw new \Exception('Лог файл не открыт.');
        }

        if(!is_string($message)){
            throw new \Exception('$message не строка');
        }

        if($messageType != FileLogger::NOTICE &&
            $messageType != FileLogger::WARNING &&
            $messageType != FileLogger::ERROR &&
            $messageType != FileLogger::FATAL
        ){
            throw new \Exception('Не верный $messageType.');
        }

        $this->writeToLogFile("[".$this->getTime()."]".$messageType." - ".$message);
    }

    private function writeToLogFile($message) {
        flock($this->fileHandle, LOCK_EX);
        fwrite($this->fileHandle, $message.PHP_EOL);
        flock($this->fileHandle, LOCK_UN);
    }

    private function getTime() {
        return date($this->timeFormat);
    }

    public function openLogFile($logFile) {
        $this->closeLogFile();

        if(!is_dir(dirname($logFile))){
            if(!mkdir(dirname($logFile), FileLogger::FILE_CHMOD, true)){
                throw new \Exception('Не удалось создать директорию для файла.');
            }
        }

        if(!$this->fileHandle = fopen($logFile, 'a+')){
            throw new \Exception('Не удалось открыть файл.');
        }
    }
}