<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 26.10.15
 * Time: 15:38
 */
function duration2sec($duration)
{
    if(preg_match("/^(\d\d):(\d\d):(\d\d):(\d\d)/",$duration,$matches))
    {
        return intval($matches[4])+intval($matches[3])*60+intval($matches[2])*60*60+intval($matches[1])*60*60*24;
    }
    return false;
}

function day2duration($value)
{
    if($value)
    {
        $value=floor($value*60*60*24);
        $day=floor($value/(24*60*60));
        $value=$value%(24*60*60);
        $hour=floor($value/(60*60));
        $value=$value%(60*60);
        $min=floor($value/60);
        $value=$value%60;
        $sec=$value;

        return sprintf("%02d:%02d:%02d:%02d",$day,$hour,$min,$sec);
    }
    return false;
}
