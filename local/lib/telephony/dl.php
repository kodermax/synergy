<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 13.11.15
 * Time: 13:30
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/local/lib/telephony/mssql_connect.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/lib/telephony/defines.php");

define("SOX", "/usr/bin/sox");


if (intval($_REQUEST["id"]) != 0)
{

    $sql = "SELECT ID, TimeStart, ANumber, BNumber, AWavFile, BWavFile FROM [Infinity].[dbo].[tConnections] WHERE ID=" . $_REQUEST["id"];

    $res = mssql_query($sql);
    $ff = $sf = "";
    if ($row = mssql_fetch_assoc($res))
    {
        $tempfile = "call";

        if(trim($row["ANumber"])!="")
            $tempfile.="-".trim($row["ANumber"]);
        if(trim($row["BNumber"])!="")
            $tempfile.="-".trim($row["BNumber"]);

        $row["TimeStart"]=str_replace([":"," "],"-",$row["TimeStart"]);
        $tempfile.="-".substr(trim($row["TimeStart"]),0,19).".wav";

        $tempfile_full = $_SERVER["DOCUMENT_ROOT"].TELEPHONY_PATH_TO_CACHE.$tempfile;

        if (trim($row["AWavFile"]) != "" && file_exists($_SERVER["DOCUMENT_ROOT"] . TELEPHONY_PATH_TO_WAV . (str_replace("\\", "/", $row["AWavFile"]))))
        {
            $ff = $_SERVER["DOCUMENT_ROOT"] . TELEPHONY_PATH_TO_WAV . (str_replace("\\", "/", $row["AWavFile"]));
        }
        if (trim($row["BWavFile"]) != "" && file_exists($_SERVER["DOCUMENT_ROOT"] . TELEPHONY_PATH_TO_WAV . (str_replace("\\", "/", $row["BWavFile"]))))
        {
            $sf = $_SERVER["DOCUMENT_ROOT"] . TELEPHONY_PATH_TO_WAV . (str_replace("\\", "/", $row["BWavFile"]));
        }
    }
    if ($ff != "" || $sf != "")
    {
        system(SOX . " -m " . $ff . " " . $sf . " " .  $tempfile_full, $retval);
        if ($retval == 0)
        {
            header('Content-Type: audio/x-wav');
            header('Content-Disposition: attachment;filename="'.$tempfile.'"');
            header("Content-Transfer-Encoding: binary ");
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($tempfile_full));
            readfile($tempfile_full);
            unlink($tempfile_full);
            exit;
        }
    }
}
