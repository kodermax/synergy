<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

    <link href="/services/meeting/print.css" type="text/css"  rel="stylesheet" />
<?
$APPLICATION->SetTitle("Протокол совещания");?>
<?
if (!CModule::IncludeModule("meeting"))
{
    ShowError(GetMessage("M_MODULE_NOT_INSTALLED"));
    return;
}
// Выбираем требуемое собрание
// Предполагаем, что оно существует
$tmpMeetings = CMeeting::GetList(array(),
    array('ID' => intval($_GET[item])), false, false,
    array()
);
$Meeting=$tmpMeetings->fetch();

// Выбираем все пункты повестки дня в массив $tmpMeetingItem
$MeetingItems=CMeeting::GetItems(intval($_GET[item]));
$i = 0;
while ($tmpRes = $MeetingItems->Fetch())
{
    $tmpMeetingItem[$i] = $tmpRes;
//		print_r($tmpMeetingItem[$i]);
    $i++;
}
?>

<!--    <p class="proto-caption">Протокол совещания</p>-->
    <p class="proto-text"><span>Дата:</span> <?=date_create($Meeting['DATE_START'])->Format('d/m/Y');?></p>
    <p class="proto-text"><span>Тема:</span> <?=$Meeting['TITLE']?></p>
    <p class="proto-text"><span>Повестка дня:</span></p>
    <ul>

        <?
        $tmpMeetingItem2 = $tmpMeetingItem;
// Обходим все пункты повестки дня
        foreach ($tmpMeetingItem as $MeetingItem)
        {
// Если INSTANCE_PARENT_ID - значит это основной пункт - выводим его в список
            if ($MeetingItem['INSTANCE_PARENT_ID'] == 0)
            {?>
                <li><?= $MeetingItem['TITLE'] ?></li>

            <?
            }
// Ищем все подпункты (INSTANCE_PARENT_ID=ID)
            $tmpID = $MeetingItem['ID'];
            $tmpFlag = 0;
            foreach ($tmpMeetingItem2 as $MeetingItem2level)
            {
//Если INSTANCE_PARENT_ID=ID значит это подпункт
                if ($MeetingItem2level['INSTANCE_PARENT_ID']===$tmpID)
                {
//Если tmpFlag==0 - значит это первый подпункт
                    if ($tmpFlag == 0)
                    {
                        $tmpFlag = 1;?>
                        <ul>
                        <li><?=$MeetingItem2level['TITLE']?></li>
                    <?}
                    else
//Если tmpFlag!=0 - значит это один мз следующих пунктов
                    {?>
                        <li><?=$MeetingItem2level['TITLE']?></li>
                    <?}
                }
            }
            if ($tmpFlag === 1)
            {?>
                </ul>
            <?}
            ?>
        <?}?>

    </ul>
    <p class="proto-text"><span>Протокол:</span></p>
    <p class="proto-text-protocol"><?=$Meeting['PROTOCOL_TEXT']?></p>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>