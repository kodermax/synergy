<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
$APPLICATION->SetTitle("Настройка модуля \"Конструктор документов\"");
?>
<?$APPLICATION->IncludeComponent("bitrix:menu", "htmls.docdesigner", array(
	"ROOT_MENU_TYPE" => "left",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
<br />
<link rel="stylesheet" type="text/css" href="/bitrix/themes/.default/htmls.docdesigner.css?"<?=time()?>>
<style type="text/css">

td.field-name1{
	font-size:9pt !important;
	color:rgb(96,96,96) !important;
	font-family:Tahoma !important;
}


</style>
<?
//v4.5.2 15.09.2012
$module_id = "htmls.docdesigner";
$DocDesigner_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($DocDesigner_RIGHT>="R") :

	global $MESS, $DB;

	include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/main/lang/", "/options.php"));
	include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/lang/", "/options.php"));

	$arAllOptionsP = array(
		array("DOCDESIGNER_COMPANY", GetMessage("DOCDESIGNER_COMPANY"), "", Array("selectbox"), $arSelectIBlock),
		array("DOCDESIGNER_COMPANY_FAKSIMILE", GetMessage("DOCDESIGNER_COMPANY_FAKSIMILE"), "", Array("selectbox"), $arProp),
	);

	if ($REQUEST_METHOD=="POST" && $DocDesigner_RIGHT=="W" && check_bitrix_sessid() && $_REQUEST["SUpdate"]){
		for ($i = 0; $i < count($arAllOptionsP); $i++){
			$name = $arAllOptionsP[$i][0];
			$val = $$name;
			if ($arAllOptionsP[$i][3][0] == "checkbox" && $val != "Y")
				$val = "N";
			COption::SetOptionString($module_id, $name, $val, $arAllOptionsP[$i][1]);
		}
	}
	?>
	<script type="text/javascript">
	var stop;

	function EndScan(){
		stop = true;
	}
	function StartScan(val){
		stop = false;
		DoNext(true, val);
	}

	function DoNext(start, val){
		queryString='lang=ru&mode=scan';

		if(start)
			queryString+='&listID='+val;
		else
			queryString+='&step=DoNext';

		CHttpRequest.Action = function(result)
		{
			CloseWaitWindow();
			arr = (eval('('+result+')'));
			innerHtml = '';
			for(var i in arr){
				innerHtml = innerHtml + '<option value="' + i + '">' + arr[i] + '</option>';
			}

			strlist = 'DOCDESIGNER_COMPANY_FAKSIMILE,DOCDESIGNER_COMPANY_OGRN,DOCDESIGNER_COMPANY_INN,DOCDESIGNER_COMPANY_KPP,DOCDESIGNER_COMPANY_ADDRESS_LEGAL,DOCDESIGNER_COMPANY_ADDRESS,DOCDESIGNER_COMPANY_POST_ADDRESS,DOCDESIGNER_COMPANY_PHONE,DOCDESIGNER_COMPANY_PERSON,DOCDESIGNER_COMPANY_RS,DOCDESIGNER_COMPANY_BANK_NAME,DOCDESIGNER_COMPANY_INVOICE_FACSIMILE';
			arr = strlist.split(',');
			for(var i in arr){
				elem = document.getElementById(arr[i]);
				elem.innerHTML = innerHtml;
			}
			if (result.search('DoNext') == -1)
				EndScan();
			else if(!stop)
				setTimeout('DoNext()', 1000);
		}
		ShowWaitWindow();

		CHttpRequest.Send('/bitrix/admin/docdesigner_options.php?'+queryString);
	}

	function StartScan2(val){
		stop = false;
		DoNext2(true, val);
	}

	function DoNext2(start, val){
		queryString='lang=ru&mode=scan';

		if(start)
			queryString+='&listID='+val;
		else
			queryString+='&step=DoNext';

		CHttpRequest.Action = function(result)
		{
			CloseWaitWindow();
			arr = (eval('('+result+')'));
			innerHtml = '';
			for(var i in arr){
				innerHtml = innerHtml + '<option value="' + i + '">' + arr[i] + '</option>';
			}

			strlist = 'DOCDESIGNER_OFFER_PRICE';
			arr = strlist.split(',');
			for(var i in arr){
				elem = document.getElementById(arr[i]);
				elem.innerHTML = innerHtml;
			}
			if (result.search('DoNext') == -1)
				EndScan();
			else if(!stop)
				setTimeout('DoNext2()', 1000);
		}
		ShowWaitWindow();

		CHttpRequest.Send('/bitrix/admin/docdesigner_options.php?'+queryString);
	}

	function StartImport(){
		CHttpRequest.Action = function(result)
		{
			CloseWaitWindow();
			elem = document.getElementById('import_answer');
			elem.innerHTML = result;
		}
		ShowWaitWindow();

		CHttpRequest.Send('/bitrix/admin/docdesigner_1c_exchange.php?action=import');
	}

	function StartExport(){
		CHttpRequest.Action = function(result)
		{
			CloseWaitWindow();
			elem = document.getElementById('export_answer');
			elem.innerHTML = result;
		}
		ShowWaitWindow();
		CHttpRequest.Send('/bitrix/admin/docdesigner_1c_exchange.php?action=export');
	}
	</script>

	<?
	$IsCrm = IsModuleInstalled("crm");
	$aTabs = array(
	//	array("DIV" => "edit3", "TAB" => GetMessage("DOCDESIGNER_TAB_CONTRACTS"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_CONTRACTS_ALT")),
		array("DIV" => "edit1", "TAB" => GetMessage("DOCDESIGNER_TAB_SETTINGS"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_SETTINGS_ALT")),

		//array("DIV" => "edit5", "TAB" => GetMessage("DOCDESIGNER_TAB_INVOICES"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_INVOICES_ALT"))
	);
	//if($IsCrm){
		$aTabs[] = array("DIV" => "edit4", "TAB" => GetMessage("DOCDESIGNER_TAB_COMPANY"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_COMPANY_ALT"));//}
		//$aTabs[] = array("DIV" => "edit6", "TAB" => GetMessage("DOCDESIGNER_TAB_FNAMES"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_FNAMES_ALT"));
		$aTabs[] = array("DIV" => "edit7", "TAB" => GetMessage("DOCDESIGNER_TAB_OTHER"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_OTHER_ALT"));
		//$aTabs[] = array("DIV" => "edit2", "TAB" => GetMessage("DOCDESIGNER_TAB_RIGHTS"), "ICON" => "docdesigner_settings_icon", "TITLE" => GetMessage("DOCDESIGNER_TAB_RIGHTS_ALT"));

    CModule::IncludeModule('iblock');
	$arProp['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
	$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>COption::GetOptionString($module_id, "DOCDESIGNER_COMPANY", 0)));
	while ($prop_fields = $properties->GetNext()){
		$arProp[$prop_fields["ID"]] = $prop_fields['NAME'];
	}

	CModule::IncludeModule("iblock");
	$res = CIBlock::GetList(Array(), Array('TYPE'=>"lists",'ACTIVE'=>'Y'), true);
	if($res->SelectedRowsCount() > 0){
		$arSelectIBlock['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
		while($ar_res = $res->Fetch()){
			$arSelectIBlock[$ar_res["ID"]] = $ar_res['NAME'];
		}
	}

	$arSelectCatalog = $arSelectIBlock;
	$res = CIBlock::GetList(Array(), Array('TYPE'=>"library",'ACTIVE'=>'Y'), true);
	if($res->SelectedRowsCount() > 0){
		$arSelectLibrary['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
		while($ar_res = $res->Fetch()){
			$arSelectLibrary[$ar_res["ID"]] = $ar_res['NAME'];
		}
	}

	//get user fields for company
	$arUFCompany['no'] = $MESS['DOCDESIGNER_NO_SELECT'];
	CModule::IncludeModule("crm");
	$crm = new CCrmFields($USER_FIELD_MANAGER, 'CRM_COMPANY');
	$arFields = $crm->GetFields();
	foreach($arFields as $CODE => $v){
		$arUFCompany[$CODE] = $v['EDIT_FORM_LABEL'];
	}

	//check invoices list
	$property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>COption::GetOptionInt($module_id, 'DOCDESIGNER_INVOICES_REGISTER'), "CODE"=>"TYPE"));
	while($enum_fields = $property_enums->GetNext()){
		$arType[$enum_fields["ID"]] = $enum_fields["VALUE"];
	}

	$arAllOptions = array(
		'main' => array(
			array("DOCDESIGNER_COMPANY", GetMessage("DOCDESIGNER_COMPANY"), "", Array("selectbox", $arSelectIBlock)),
			array("DOCDESIGNER_COMPANY_FAKSIMILE", GetMessage("DOCDESIGNER_COMPANY_FAKSIMILE"), "", Array("selectbox", $arProp)),
			//GetMessage("DOCDESIGNER_CRM_COMPANY_ID_DATA"),
			array('DOCDESIGNER_COMPANY_OGRN', GetMessage('DOCDESIGNER_CRM_COMPANY_OGRN'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_INN', GetMessage('DOCDESIGNER_CRM_COMPANY_INN'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_KPP', GetMessage('DOCDESIGNER_CRM_COMPANY_KPP'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_ADDRESS_LEGAL', GetMessage('DOCDESIGNER_COMPANY_ADDRESS_LEGAL'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_ADDRESS', GetMessage('DOCDESIGNER_COMPANY_ADDRESS'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_POST_ADDRESS', GetMessage('DOCDESIGNER_CRM_COMPANY_POST_ADDRESS'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_PHONE', GetMessage('DOCDESIGNER_CRM_COMPANY_PHONE'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_PERSON', GetMessage('DOCDESIGNER_COMPANY_PERSON'), '', Array("selectbox", $arProp)),
			//GetMessage("DOCDESIGNER_CRM_COMPANY_BANK_DETAILS"),
			array('DOCDESIGNER_COMPANY_RS', GetMessage('DOCDESIGNER_CRM_COMPANY_RS'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_BANK_NAME', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_NAME'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_BANK_BIK', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_BIK'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_BANK_KS', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_KS'), '', Array("selectbox", $arProp)),
			array('DOCDESIGNER_COMPANY_BANK_CITY', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_CITY'), '', Array("selectbox", $arProp)),
			//GetMessage("DOCDESIGNER_CRM_COMPANY_OTHER"),
			array("DOCDESIGNER_COMPANY_INVOICE_FACSIMILE", GetMessage("DOCDESIGNER_COMPANY_INVOICE_FACSIMILE"), "", Array("selectbox", $arProp)),
			//array("DOCDESIGNER_COMPANY_CODE1C", GetMessage("DOCDESIGNER_COMPANY_CODE1C"), "", Array("selectbox", $arProp))
			),
		'company' => array(
			//GetMessage("DOCDESIGNER_CRM_COMPANY_ID_DATA"),
			array('DOCDESIGNER_CRM_COMPANY_OGRN', GetMessage('DOCDESIGNER_CRM_COMPANY_OGRN'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_INN', GetMessage('DOCDESIGNER_CRM_COMPANY_INN'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_KPP', GetMessage('DOCDESIGNER_CRM_COMPANY_KPP'), '', Array("selectbox", $arUFCompany)),
			//GetMessage("DOCDESIGNER_CRM_COMPANY_BANK_DETAILS"),
			array('DOCDESIGNER_CRM_COMPANY_RS', GetMessage('DOCDESIGNER_CRM_COMPANY_RS'), '', Array("selectbox", $arUFCompany)),
			//array('DOCDESIGNER_CRM_COMPANY_BANK', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_BANK_NAME', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_NAME'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_BANK_BIK', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_BIK'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_BANK_KS', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_KS'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_BANK_CITY', GetMessage('DOCDESIGNER_CRM_COMPANY_BANK_CITY'), '', Array("selectbox", $arUFCompany)),
			//GetMessage("DOCDESIGNER_CRM_COMPANY_OTHER"),
			array('DOCDESIGNER_CRM_COMPANY_POST_ADDRESS', GetMessage('DOCDESIGNER_CRM_COMPANY_POST_ADDRESS'), '', Array("selectbox", $arUFCompany)),
			array('DOCDESIGNER_CRM_COMPANY_PHONE', GetMessage('DOCDESIGNER_CRM_COMPANY_PHONE'), '', Array("selectbox", $arUFCompany)))
	);
    //numerators - start
    $arSelect = Array("ID", "NAME");
	$arFilter = Array("IBLOCK_CODE"=>"htmls_numerators", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$arSelectNums['no'] = GetMessage("DOCDESIGNER_NO_SELECT");
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arSelectNums[$arFields['ID']] = $arFields['NAME'];
	}
	$arAllOptions["v8exchange"][] = array('DOCDESIGNER_NUMERATOR_1C', GetMessage('DOCDESIGNER_NUMERATOR_1C'), '', Array("selectbox", $arSelectNums));

	//numerators - end

	//v8exchange - start
	$path2epf = "/bitrix/modules/".$module_id."/v8exchange/1CV8-BXCRM.epf";
	$v8epf = "<a href='".$path2epf."' onclick=\"jsUtils.Redirect([], 'fileman_file_download.php?path=".$path2epf."&site=s1&lang=ru'); return false;\">".GetMessage("V8EXCHANGE_DOWNLOAD")."</a>";
	//$arAllOptions["v8exchange"][] = GetMessage("DOCDESIGNER_V8EXCHANGE_CONFIGURATIONS");
	$arAllOptions["v8exchange"][] = Array("", GetMessage("DOCDESIGNER_V8EXCHANGE_EPF"), $v8epf, Array("statichtml"));
	//$arAllOptions["v8exchange"][] = GetMessage("DOCDESIGNER_ADDMESSAGE2LOG_HEADER");
	//$arAllOptions["v8exchange"][] = Array("DOCDESIGNER_ADDMESSAGE2LOG", GetMessage("DOCDESIGNER_ADDMESSAGE2LOG"), '', Array("checkbox"));
	//v8exchange - end

	//service - end
	if ($REQUEST_METHOD=="POST" && $DocDesigner_RIGHT=="W" && $_REQUEST["SUpdate"]){
		foreach($arAllOptions as $aOptGroup){
			if(!is_array($aOptGroup)) continue;
			for ($i = 0; $i < count($aOptGroup); $i++){
				$name = $aOptGroup[$i][0];
				$val = $$name;
				if ($aOptGroup[$i][3][0] == "checkbox" && $val != "Y")
					$val = "N";
				COption::SetOptionString($module_id, $name, $val, $aOptGroup[$i][1]);
			}
		}
	}

	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	$tabControl->Begin();
	?><form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&lang=<?=LANGUAGE_ID?>"><?
	//bitrix_sessid_post();

	$tabControl->BeginNextTab();//contracts

	for ($i = 0; $i < count($arAllOptions['main']); $i++):
		$Option = $arAllOptions['main'][$i];
		$val = COption::GetOptionString($module_id, $Option[0], $Option[2]);

		$type = $Option[3];
		?>
		<tr>
			<td valign="top" width="50%"><?
				if ($type[0]=="checkbox")
					echo "<label for=\"".htmlspecialchars($Option[0])."\">".$Option[1]."</label>";
				else
					echo $Option[1];
			?></td>
			<td valign="middle" width="50%">
				<?if($type[0]=="checkbox"):?>
					<input type="checkbox" name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" value="Y"<?if($val=="Y")echo" checked";?>>
				<?elseif($type[0]=="text"):?>
					<input type="text" size="<?echo $type[1]?>" value="<?echo htmlspecialchars($val)?>" name="<?echo htmlspecialchars($Option[0])?>">
				<?elseif($type[0]=="textarea"):?>
					<textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialchars($Option[0])?>"><?echo htmlspecialchars($val)?></textarea>
				<?elseif($type[0]=="selectbox"):?>
					<?if($Option[0] == "DOCDESIGNER_COMPANY"):?>
						<select name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" onchange="StartScan(this.value);">
					<?else:?>
						<select name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>">
					<?endif;?>
						<?foreach($Option[3][1] as $v => $k)
						{
							?><option value="<?=$v?>"<?if($val==$v)echo" selected";?>><?=$k?></option><?
						}
						?>
					</select>
				<?endif?>
			</td>
		</tr>
	<?endfor;?>
	<?

	$tabControl->BeginNextTab();//company
	for ($i = 0; $i < count($arAllOptions['company']); $i++):
		$Option = $arAllOptions['company'][$i];
		$val = COption::GetOptionString($module_id, $Option[0], $Option[2]);

		$type = $Option[3];
		?>
		<tr>
			<td valign="top" width="50%"><?
				if ($type[0]=="checkbox")
					echo "<label for=\"".htmlspecialchars($Option[0])."\">".$Option[1]."</label>";
				else
					echo $Option[1];
			?></td>
			<td valign="middle" width="50%">
				<?if($type[0]=="checkbox"):?>
					<input type="checkbox" name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" value="Y"<?if($val=="Y")echo" checked";?>>
				<?elseif($type[0]=="text"):?>
					<input type="text" size="<?echo $type[1]?>" value="<?echo htmlspecialchars($val)?>" name="<?echo htmlspecialchars($Option[0])?>">
				<?elseif($type[0]=="textarea"):?>
					<textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialchars($Option[0])?>"><?echo htmlspecialchars($val)?></textarea>
				<?elseif($type[0]=="selectbox"):?>
					<?if($Option[0] == "DOCDESIGNER_COMPANY"):?>
						<select name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" onchange="StartScan(this.value);">
					<?else:?>
						<select name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>">
					<?endif;?>
						<?foreach($Option[3][1] as $v => $k)
						{
							?><option value="<?=$v?>"<?if($val==$v)echo" selected";?>><?=$k?></option><?
						}
						?>
					</select>
				<?endif?>
			</td>
		</tr>
	<?endfor;
	$tabControl->BeginNextTab();//other
	for ($i = 0; $i < count($arAllOptions['v8exchange']); $i++):
		$Option = $arAllOptions['v8exchange'][$i];
		$val = COption::GetOptionString($module_id, $Option[0], $Option[2]);
		$type = $Option[3];
		?>
		<tr>
			<td valign="top" width="50%"><?
				if ($type[0]=="checkbox")
					echo "<label for=\"".htmlspecialchars($Option[0])."\">".$Option[1]."</label>";
				else
					echo $Option[1];
			?></td>
			<td valign="middle" width="50%">
				<?if($type[0]=="checkbox"):?>
					<input type="checkbox" name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" value="Y"<?if($val=="Y")echo" checked";?>>
				<?elseif($type[0]=="text"):?>
					<input type="text" size="<?echo $type[1]?>" value="<?echo htmlspecialchars($val)?>" name="<?echo htmlspecialchars($Option[0])?>">
				<?elseif($type[0]=="statichtml"):?>
					<?echo ($Option[2])?>
				<?elseif($type[0]=="textarea"):?>
					<textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialchars($Option[0])?>"><?echo htmlspecialchars($val)?></textarea>
				<?elseif($type[0]=="selectbox"):?>
					<?if($Option[0] == "DOCDESIGNER_COMPANY"):?>
						<select name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" onchange="StartScan(this.value);">
					<?else:?>
						<select name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>">
					<?endif;?>
						<?foreach($Option[3][1] as $v => $k)
						{
							?><option value="<?=$v?>"<?if($val==$v)echo" selected";?>><?=$k?></option><?
						}
						?>
					</select>
				<?endif?>
			</td>
		</tr>
	<?endfor;
	$tabControl->Buttons();
	?>
	<script language="JavaScript">
	function RestoreDefaults()
	{
		if (confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>'))
			window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)."&".bitrix_sessid_get();?>";
	}
	</script>

	<input type="submit" <?if ($DocDesigner_RIGHT<"W") echo "disabled" ?> name="SUpdate" value="<?echo GetMessage("MAIN_SAVE")?>">
	<input type="hidden" name="Update" value="Y">
	<input type="reset" name="reset" value="<?echo GetMessage("MAIN_RESET")?>">
	<?
	$tabControl->End();
	?>
	</form>
<?
else:?>
<p><font size="2" color="red">Доступ запрещен</font></p>
<?
endif;
?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>