<?php
/**
 * Created by PhpStorm.
 * User: dh
 * Date: 18.09.15
 * Time: 10:09
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/csv_data.php");

CModule::IncludeModule("crm");
CJSCore::Init('jquery');


$writeContacts = false;


$el = new CIBlockElement;

$arGroups = CUser::GetUserGroup($USER->GetId());


if (in_array(27, $arGroups) or $USER->IsAdmin())
{

    $dateFrom=(strtotime($_REQUEST["datefrom"]))?$_REQUEST["datefrom"]:"";
    $dateTo=(strtotime($_REQUEST["dateto"]))?$_REQUEST["dateto"]:"";
    $sent=(intval($_REQUEST["sent"])==1)?1:0;

    $csvFile = new CCSVData();
    $fields_type = 'R';
    $delimiter = ";";
    $csvFile->SetFieldsType($fields_type);
    $csvFile->SetDelimiter($delimiter);
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery-ui-1.11.4.custom/jquery-ui.min.js", true);
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/jquery-ui-1.11.4.custom/jquery-ui.min.css", true);
    $APPLICATION->ShowHead();


    if (strlen($dateFrom) == 0 and strlen($dateTo) > 0 and $sent == 1)
    {
        echo "<font color='red'>Введите начальную дату</font>";
    }
    elseif (strlen($dateTo) == 0 and strlen($dateFrom) > 0 and $sent == 1)
    {
        echo "<font color='red'>Введите конечную дату</font>";
    }
    elseif (strlen($dateFrom) == 0 and strlen($dateTo) == 0 and $sent == 1)
    {
        echo "<font color='red'>Введите начальную и конечную даты</font>";
    }
    elseif (strlen($dateFrom) > 0 and strlen($dateTo) > 0 and $sent == 1)
    {

        $arSort = Array(
            "ID" => "DESC"
        );

        $arFilter = Array(
            "CREATED_BY_ID" => USER_ID_LANDER
        );

        if ($dateFrom)
        {
            $arFilter[">=DATE_CREATE"] = $dateFrom;
        }

        if ($dateTo)
        {
            $arFilter["<DATE_CREATE"] = ConvertTimeStamp(makeTimeStamp($dateTo) + 86400, "SHORT");
        }

        $arSelect = Array("DATE_CREATE", "CREATED_BY", "TITLE", "LAST_NAME", "FULL_NAME", "OPPORTUNITY", PROP_LEAD_LAND, PROP_LEAD_CAMPANY, PROP_LEAD_CITY);
        $nPageTop = false;

        $object = CCrmLead::GetList($arSort, $arFilter, $arSelect, $nPageTop);
        while ($array = $object->Fetch())
        {

            $dbResMultiFields = CCrmFieldMulti::GetList(
                array('ID' => 'asc'),
                array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $array["ID"])
            );

            $arFields['FM'] = array();
            while ($arMultiFields = $dbResMultiFields->Fetch())
            {
                $arFields['FM'][$arMultiFields["TYPE_ID"]][$arMultiFields["ID"]] = $arMultiFields;
            }

            $arResult["LEAD"][$array["ID"]] = $array;

        }


        if ($writeContacts == true)
        {
            $arrHeaderCSV = array(
                "Дата создания",
                "Заголовок",
                "Имя",
                "E-mail",
                "телефон",
                "Возм. сумма сделки",
                "Ленд",
                "Кампания",
                "Город"
            );
        }
        else
        {
            $arrHeaderCSV = array(
                "Дата создания",
                "Заголовок",
                "Имя",
                "Возм. сумма сделки",
                "Ленд",
                "Кампания",
                "Город"
            );
        }

        foreach ($arrHeaderCSV as $k => $v)
        {
            $arrHeaderCSV[$k] = iconv('utf-8', 'windows-1251', $v);
        }


        unlink($_SERVER["DOCUMENT_ROOT"] . "/crm/lead/leads.csv");

        $csvFile->SaveFile("leads.csv", $arrHeaderCSV);


        foreach ($arResult["LEAD"] as $arLead):

            foreach ($arLead["EMAIL"] as $arEmail):
                $email = $arEmail["VALUE"];
            endforeach;

            foreach ($arLead["PHONE"] as $arPhone):
                $phone = (string)$arPhone["VALUE"];
            endforeach;


            if ($writeContacts == true)
            {
                $arrHeaderCSV = Array(
                    ConvertDateTime($arLead["DATE_CREATE"], "DD.MM.YYYY"),
                    iconv('utf-8', 'windows-1251', $arLead["TITLE"]),
                    iconv('utf-8', 'windows-1251', $arLead["FULL_NAME"]),
                    $email,
                    strval($phone),
                    iconv('utf-8', 'windows-1251', $arLead["OPPORTUNITY"]),
                    iconv('utf-8', 'windows-1251', $arLead[PROP_LEAD_CAMPANY]),
                    iconv('utf-8', 'windows-1251', $arLead[PROP_LEAD_LAND]),
                    iconv('utf-8', 'windows-1251', $arLead[PROP_LEAD_CITY])
                );
            }
            else
            {
                $arrHeaderCSV = Array(
                    ConvertDateTime($arLead["DATE_CREATE"], "DD.MM.YYYY"),
                    iconv('utf-8', 'windows-1251', $arLead["TITLE"]),
                    iconv('utf-8', 'windows-1251', $arLead["FULL_NAME"]),
                    iconv('utf-8', 'windows-1251', $arLead["OPPORTUNITY"]),
                    iconv('utf-8', 'windows-1251', $arLead[PROP_LEAD_CAMPANY]),
                    iconv('utf-8', 'windows-1251', $arLead[PROP_LEAD_LAND]),
                    iconv('utf-8', 'windows-1251', $arLead[PROP_LEAD_CITY])
                );
            }

            $csvFile->SaveFile("leads.csv", $arrHeaderCSV);


        endforeach;

        $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . "/crm/lead/leads.csv");

        $fileId = CFile::SaveFile($arFile);


        if ($fileId > 0)
        {
            unlink($_SERVER["DOCUMENT_ROOT"] . "/crm/lead/leads.csv");

            $arElement = Array(
                "NAME" => "Выгрузка лидов от " . ConvertTimeStamp(time()),
                "IBLOCK_ID" => 80,
                "DATE_ACTIVE_FROM" => $dateFrom,
                "DATE_ACTIVE_TO" => $dateTo,
                "PROPERTY_VALUES" => Array(
                    "FILE" => $fileId,
                    "USER" => $USER->GetId()
                )
            );


            $el->Add($arElement, false, false);

            echo "<a href='" . CFile::GetPath($fileId) . "'>Скачать в формате csv (Окрывается с помощью EXCEL)</a>";
        }
        else
            echo "<font color='red'>Ошибка сохранения файла</font>";


    }


    ?>


    <form name="leads" method="post">

        <p>Вывести лиды</p>

        <p>С <input name="datefrom" type="text" placeholder="Дата" value="<?= $dateFrom ?>">
            по <input name="dateto" type="text" placeholder="Дата" value="<?= $dateTo ?>"></p>
        <input type="hidden" value="1" name="sent">
        <input type="submit" value="Искать"><br/><br/>

    </form>


    <style>
        table tr.header td {
            font-weight: bold;
        }

        body {
            padding: 30px;
        }
    </style>

    <script>
        $(document).ready(function () {
            $("input:text").datepicker({
                dateFormat: "dd.mm.yy",
                firstDay: 1
            });
        });
    </script>


    <?

}
else
{
    echo "<font color='red'>Недостаточно прав</font>";
}

?>