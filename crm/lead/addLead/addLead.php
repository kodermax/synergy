<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$log = new Synergy\FileLogger($_SERVER['DOCUMENT_ROOT'] . "/lander.txt");
$log->log("=====================================");
$log->log('Запрос: '.json_encode($_REQUEST));
if(CModule::IncludeModule("crm")){

	$arLeadParams = $_REQUEST;

	$arLeadParams["FM"] = Array
	(
		"EMAIL" => Array
		(
			"n1" => Array
			(
				"VALUE" => trim($arLeadParams["EMAIL_HOME"]),
				"VALUE_TYPE" => "WORK"
			)
		),
		"PHONE" => Array
		(
			"n1" => Array
			(
				"VALUE" => preg_replace("/\D/","",$arLeadParams["PHONE_MOBILE"]),
				"VALUE_TYPE" => "WORK"
			)
		)
	);

	$arLeadParams["ASSIGNED_BY_ID"] = USER_ID_LANDER;
	$arLeadParams[PROP_LEAD_DISPATCHER] = USER_ID_LANDER;




	if(isset($arLeadParams["TITLE"])){

		$objLead = new CCrmLead(false);

		$responseLeadID = $objLead->Add($arLeadParams, true, true);

		if($responseLeadID > 0){

			/*CModule::IncludeModule("bizproc");

			$CCrmBizProc = new CCrmBizProc('LEAD');

			$arBizProcParametersValues19 = $CCrmBizProc->CheckFields(19);

			$CCrmBizProc->StartWorkflow($responseLeadID, $arBizProcParametersValues19);*/

			//SynergyLead::startLeadBP($responseLeadID, 19);
$log->log('Результат: '.$responseLeadID);
			echo json_encode(Array("response" => 1, "leadBxId" => $responseLeadID));

		}else{
			/**
			Метод CCrmLead::Add() изменяет переданный ему массив, дописывая в него некоторые поля.
			В том числе и данные с результатом работы метода: $arLeadParams['RESULT_MESSAGE']
			 */
			$log->log('Результат: '.$objLead->LAST_ERROR);
			$response = Array("response" => 0,'result' => $objLead->LAST_ERROR);
			echo json_encode($response);

		}

	}else{

		echo json_encode(Array("response" => 0, "result"=>"No TITLE field exists"));

	}
}
$log->log("====================================");
?>