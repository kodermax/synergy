<?

/*Делаем невозможным редактирование номера договора*/
$arUnsetUF = array(
    'UF_CRM_1433333523',//филиал
    'UF_CRM_1435304800',//Номер договора
    'UF_CRM_1435567800',//Номер договора
    'UF_CRM_1433333585',//Учебный год
    'UF_CRM_1433333598',//Семестр учебного года
    'UF_CRM_1433333608',//Уровень образования
    'UF_CRM_1433333639',//Подуровень образования
    'UF_CRM_1433333651',//Факультет
    'UF_CRM_1433333663',//Направление
    'UF_CRM_1433333673',//Профессиональная программа
    'UF_CRM_1433333681',//Программа обучения
    'UF_CRM_1433333692',//День обучения
    'UF_CRM_1433333701',//Форма обучения
    'UF_CRM_1433333709',//Срок
    'UF_CRM_1433333719',//Дополнительная категория
    
);

foreach($arUnsetUF as $uf){
    unset($_REQUEST[$uf]);
    unset($_POST[$uf]);
}
/**/

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/deal/index.php");
$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));

?><?$APPLICATION->IncludeComponent(
	"bitrix:crm.deal", 
	".default", 
	array(
		"SEF_MODE" => "Y",
		"PATH_TO_CONTACT_SHOW" => "/crm/contact/show/#contact_id#/",
		"PATH_TO_CONTACT_EDIT" => "/crm/contact/edit/#contact_id#/",
		"PATH_TO_COMPANY_SHOW" => "/crm/company/show/#company_id#/",
		"PATH_TO_COMPANY_EDIT" => "/crm/company/edit/#company_id#/",
		"PATH_TO_INVOICE_SHOW" => "/crm/invoice/show/#invoice_id#/",
		"PATH_TO_INVOICE_EDIT" => "/crm/invoice/edit/#invoice_id#/",
		"PATH_TO_LEAD_SHOW" => "/crm/lead/show/#lead_id#/",
		"PATH_TO_LEAD_EDIT" => "/crm/lead/edit/#lead_id#/",
		"PATH_TO_LEAD_CONVERT" => "/crm/lead/convert/#lead_id#/",
		"PATH_TO_USER_PROFILE" => "/company/personal/user/#user_id#/",
		"PATH_TO_PRODUCT_EDIT" => "/crm/product/edit/#product_id#/",
		"PATH_TO_PRODUCT_SHOW" => "/crm/product/show/#product_id#/",
		"ELEMENT_ID" => $_REQUEST["deal_id"],
		"SEF_FOLDER" => "/crm/deal/",
		"NAME_TEMPLATE" => "",
		"SEF_URL_TEMPLATES" => array(
			"index" => "index.php",
			"list" => "list/",
			"funnel" => "funnel/",
			"edit" => "edit/#deal_id#/",
			"show" => "show/#deal_id#/",
			"import" => "import/",
		)
	),
	false
);?>
<script  type="text/javascript" >

    function SetUfDsbl(){
        if($('input[name="<?=$uf?>"]').attr("class") != ''){

        <?
            foreach($arUnsetUF as $uf){
                ?>
                    $('input[name="<?=$uf?>"]').attr("disabled", "disabled"); 
                <?
            }
        ?>
        }
        else{
            setTimeout('SetUfDsbl();',1500);
        }
    }    
    $(document).ready(function(){
        setTimeout('SetUfDsbl();',1500);
    });
</script>    
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>