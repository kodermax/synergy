<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/crm/product/index.php");
global $APPLICATION;
$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));
$APPLICATION->IncludeComponent(
	"bitrix:crm.product", 
	".default", 
	array(
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/crm/product/",
		"CATALOG_ID" => "21",
		"SEF_URL_TEMPLATES" => array(
			"index" => "index.php",
			"product_list" => "product_list/#section_id#/",
			"product_edit" => "product_edit/#product_id#/",
			"product_show" => "product_show/#product_id#/",
			"section_list" => "section_list/#section_id#/",
			"product_file" => "file/#product_id#/#field_id#/#file_id#/",
		)
	),
	false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
