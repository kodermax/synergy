<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/api/bootstrap/app.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Поиск по CRM");
use Elasticsearch\Client;

?>
    <style>
        .result_table {
            width: 100%;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }

        .result_table td, .result_table th {
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;
            padding: 3px 5px;
        }


    </style>
<?
$APPLICATION->IncludeComponent(
    'bitrix:crm.control_panel',
    '',
    array(
        'ID' => 'LEAD_LIST',
        'ACTIVE_ITEM_ID' => 'LEAD',
    )
);
?>
<?
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $client = \Elasticsearch\ClientBuilder::create()->build();
    $q = trim($_REQUEST['q']);
    if (strlen($q) >= 0) {
        $qType = 'string';
        if (is_numeric($q))
            $qType = 'phone';
        else if (filter_var($q, FILTER_VALIDATE_EMAIL)) {
            $qType = 'mail';
            $q = strtolower($q);
        } else {
            $qType = 'title';
        }
        //perms
        $userId = $GLOBALS['USER']->GetID();
        $params = [
            'index' => 'portal',
            'type' => 'perms',
            'id' => $userId
        ];

        try {
            $response = $client->get($params);
        } catch (\Exception $e) {

        }
        if (isset($response)) {
            $arResult = $response['_source']['perms']['CONTACT'];
            $arContactPerms = [];
            foreach ($arResult[0] as $item) {
                if (!empty($item))
                    $arContactPerms[] = $item;
            }
            $arResult = $response['_source']['perms']['DEAL'];
            $arDealPerms = [];
            foreach ($arResult[0] as $item) {
                if (!empty($item))
                    $arDealPerms[] = $item;
            }
            $arResult = $response['_source']['perms']['LEAD'];
            $arLeadPerms = [];
            foreach ($arResult[0] as $item) {
                if (!empty($item))
                    $arLeadPerms[] = $item;
            }
        }

        //Лиды
        $params = [
            'index' => 'portal',
            'type' => 'lead',
            'size' => 200
        ];

        switch ($qType) {
            case "phone":
                if (preg_match("/^[78]\d{10}/", $q)) {
                    $phone = substr($q, 1);
                    $params['body']['query']['filtered']['query']['bool']['should'][] = ['terms' => ['phones' => ['7' . $phone]]];
                    $params['body']['query']['filtered']['query']['bool']['should'][] = ['terms' => ['phones' => ['8' . $phone]]];
                }
                else {
                    $params['body']['query']['filtered']['query']['bool']['must']['terms']['phones'] = [$q];
                }
                break;
            case "mail":
                $params['body']['query']['filtered']['query']['bool']['must']['terms']['emails'] = [$q];
                break;
            case "title":
                $params['body']['query']['filtered']['query']['match']['full_name'] = [
                    'query' => $q,
                    'operator' => 'and',
                    'fuzziness' => 1
                ];
                break;
        }
        if (!empty($arLeadPerms)) {
            $params['body']['query']['filtered']['filter'] = [
                'terms' =>
                    [
                        'perms' => $arLeadPerms
                    ]
            ];
        }
        $params['body']['sort']['date_create'] = ['order'=>'asc'];
        $arLeads = $client->search($params);
        //Контакты
        $params = [
            'index' => 'portal',
            'type' => 'contact',
            'size' => 200
        ];
        switch ($qType) {
            case "phone":
                if (preg_match("/^[78]\d{10}/", $q)) {
                    $phone = substr($q, 1);
                    $params['body']['query']['filtered']['query']['bool']['should'][] = ['terms' => ['phones' => ['7' . $phone]]];
                    $params['body']['query']['filtered']['query']['bool']['should'][] = ['terms' => ['phones' => ['8' . $phone]]];
                }
                else {
                    $params['body']['query']['filtered']['query']['bool']['must']['terms']['phones'] = [$q];
                }
                break;
            case "mail":
                $params['body']['query']['filtered']['query']['bool']['must']['terms']['emails'] = [$q];
                break;
            case "title":
                $params['body']['query']['filtered']['query']['match']['full_name'] = [
                    'query' => $q,
                    'operator' => 'and',
                    'fuzziness' => 1
                ];
                break;
        }
        if (!empty($arContactPerms)) {
            $params['body']['query']['filtered']['filter'] = [
                'terms' =>
                    [
                        'perms' => $arContactPerms
                    ]
            ];
        }

        $params['body']['sort']['date_create'] = ['order'=>'asc'];
        $arContacts = $client->search($params);
        //Сделки
        $params = [
            'index' => 'portal',
            'type' => 'deal',
            'size' => 200
        ];
        switch ($qType) {
            case "phone":
                if (preg_match("/^[78]\d{10}/", $q)) {
                    $phone = substr($q, 1);
                    $params['body']['query']['filtered']['query']['bool']['should'][] = ['terms' => ['contact.phones' => ['7' . $phone]]];
                    $params['body']['query']['filtered']['query']['bool']['should'][] = ['terms' => ['contact.phones' => ['8' . $phone]]];
                }
                else {
                    $params['body']['query']['filtered']['query']['bool']['must']['terms']['contact.phones'] = [$q];
                }
                break;
            case "mail":
                $params['body']['query']['filtered']['query']['bool']['must']['terms']['contact.emails'] = [$q];
                break;
            case "title":
                $params['body'] = [
                    'query' => [
                        'filtered' => [
                            'query' => [
                                'bool' => [
                                    'should' => [
                                        [
                                            'match' => [
                                                'contact.title' => [
                                                    'query' => $q,
                                                    'operator' => 'and',
                                                    'fuzziness' => 1
                                                ]
                                            ],
                                        ],
                                        [
                                            'match' => [
                                                'title' => [
                                                    'query' => $q,
                                                    'operator' => 'and',
                                                    'fuzziness' => 1
                                                ]
                                            ],
                                        ]
                                    ]

                                ]
                            ]
                        ]
                    ]
                ];
                break;
        }
        if (!empty($arDealPerms)) {
            $params['body']['query']['filtered']['filter'] = [
                'terms' =>
                    [
                        'perms' => $arDealPerms
                    ]
            ];
        }
        $params['body']['sort']['begin_date'] = ['order'=>'asc'];
        $arDeals = $client->search($params);
    }
}

?>
    <h3 style="margin:0">Результат поиска <sup>*</sup></h3>
    <span style="font-size: 12px"><sup>*</sup>Каждая таблица ограничена 200 записями.</span>
    <br/>
    <br/>
    <b>Контакты:</b>
<? if (!empty($arContacts['hits']['hits'])): ?>
    <table class="result_table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Контакт</th>
            <th>Дата создания</th>
            <th>Ответственный</th>
            <th>Подразделение ответственного</th>
            <th>Компания</th>
            <th>Учебное заведение</th>
            <th>Класс</th>
            <th>Телефон</th>
            <th>Email</th>
            <th>Сделки</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($arContacts['hits']['hits'] as $hit): ?>
            <tr>
                <td><?= $hit['_id'] ?></td>
                <td><a href="<?= $hit['_source']['url'] ?>" target="_blank"><?= $hit['_source']['full_name'] ?></a></td>
                <td><?= $hit['_source']['date_create'] ?></td>
                <td><a href="<?= $hit['_source']['assigned_by']['url'] ?>"
                       target="_blank"><?= $hit['_source']['assigned_by']['first_name'] . ' ' . $hit['_source']['assigned_by']['last_name'] ?></a>
                </td>
                <td><?= $hit['_source']['assigned_by']['department'] ?></td>
                <td><?= $hit['_source']['company']['title'] ?></td>
                <td><?= $hit['_source']['school']['title'] ?></td>
                <td><?= $hit['_source']['class'] ?></td>
                <td><?= implode(' / ', $hit['_source']['phones']) ?></td>
                <td><?= implode(' / ', $hit['_source']['emails']) ?></td>
                <td>
                    <ul style="padding-left: 10px;">
                        <? foreach ($hit['_source']['deals'] as $deal): ?>
                            <li style="font-size: 11px"><a target="_blank"
                                                           href="<?= $deal['url'] ?>"><?= $deal['title'] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
<? else: ?>
    <br/>Ничего не найдено<br/>
<? endif; ?>
    <br/>
    <b>Лиды:</b>
<? if (!empty($arLeads['hits']['hits'])): ?>
    <table class="result_table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Лид</th>
            <th>Ленд</th>
            <th>Дата создания</th>
            <th>Статус</th>
            <th>Ответственный</th>
            <th>Подразделение ответственного</th>
            <th>Компания</th>
            <th>Учебное заведение</th>
            <th>Класс</th>
            <th>Телефон</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($arLeads['hits']['hits'] as $hit): ?>
            <tr>
                <td><?= $hit['_id'] ?></td>
                <td><a href="<?= $hit['_source']['url'] ?>" target="_blank"><?= $hit['_source']['title'] ?></a></td>
                <td><a href="<?= $hit['_source']['url'] ?>" target="_blank"><?= $hit['_source']['land_code'] ?></a></td>
                <td><?= $hit['_source']['date_create'] ?></td>
                <td><?= $hit['_source']['status']['title'] ?></td>
                <td><a href="<?= $hit['_source']['assigned_by']['url'] ?>"
                       target="_blank"><?= $hit['_source']['assigned_by']['first_name'] . ' ' . $hit['_source']['assigned_by']['last_name'] ?></a>
                </td>
                <td><?= $hit['_source']['assigned_by']['department'] ?></td>
                <td><?= $hit['_source']['company'] ?></td>
                <td><?= $hit['_source']['school']['title'] ?></td>
                <td><?= $hit['_source']['class'] ?></td>
                <td><?= implode(' / ', $hit['_source']['phones']) ?></td>
                <td><?= implode(' / ', $hit['_source']['emails']) ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
<? else: ?>
    <br/>Ничего не найдено<br/>
<? endif; ?>
    <br/>
    <b>Сделки:</b>
<? if (!empty($arDeals['hits']['hits'])): ?>
    <table class="result_table">
        <thead>
        <tr>
            <th>ID</th>
            <th>ФИО контакта</th>
            <th>Дата создания</th>
            <th>Стадия</th>
            <th>Ответственный</th>
            <th>Подразделение ответственного</th>
            <th>Название сделки</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($arDeals['hits']['hits'] as $hit): ?>
            <tr>
                <td><?= $hit['_id'] ?></td>
                <td><a href="<?= $hit['_source']['contact']['url'] ?>"
                       target="_blank"><?= $hit['_source']['contact']['title'] ?></a></td>
                <td><?= $hit['_source']['begin_date'] ?></td>
                <td><?= $hit['_source']['stage']['title'] ?></td>
                <td><a href="<?= $hit['_source']['assigned_by']['url'] ?>"
                       target="_blank"><?= $hit['_source']['assigned_by']['first_name'] . ' ' . $hit['_source']['assigned_by']['last_name'] ?></a>
                </td>
                <td><?= $hit['_source']['assigned_by']['department'] ?></td>
                <td><a target="_blank" href="<?= $hit['_source']['url'] ?>"><?= $hit['_source']['title'] ?></a></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
<? else: ?>
    <br/>Ничего не найдено<br/>
<? endif; ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>