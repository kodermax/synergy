<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отчеты Синергия");
$APPLICATION->SetAdditionalCSS("/bitrix/js/report/css/report.min.css");
?>
<h3>Отчёты по лидам</h3>
    <div class="reports-list-wrap">
        <div class="reports-list">
            <div class="reports-list-left-corner"></div>
            <div class="reports-list-right-corner"></div>
            <style>
                .reports-list-table th:hover {
                    cursor: default;
                }
            </style>
            <table cellspacing="0" id="reports_list_table_crm" class="reports-list-table">
                <tbody><tr>
                    <th class="reports-first-column reports-head-cell-top">
                        <div class="reports-head-cell"><!--<span class="reports-table-arrow"></span>--><span class="reports-head-cell-title">Название отчета</span></div>
                    </th>
                    <th class="reports-last-column">
                        <div class="reports-head-cell"><!--<span class="reports-table-arrow"></span>--><span class="reports-head-cell-title">Описание</span></div>
                    </th>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/missed_calls/"
                                                        title="Отчет по недозвонам.">
                            Отчет по недозвонам
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/task_statuses/"
                                                        title="Отчет по делам.">
                            Отчет по делам
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/sources_lend/"
                                                    title="Отчет по источникам.">
                        Отчет по источникам
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/daily_leads/"
                                                        title="Ежедневный отчет по заявкам.">
                            Ежедневный отчет по заявкам
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/leads_funnel/"
                                                        title="Воронка эффективности лидов.">
                            Воронка эффективности лидов
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/assigned.bysource/"
                                                        title="Отчет по соединенным.">
                            Отчет по соединенным
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/assigned.bysource.table/"
                                                        title="Отчет по соединенным (таблица).">
                            Отчет по соединенным (таблица)
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/lead_statuses/"
                                                        title="Отчет по статусам лидов.">
                            Отчет по статусам лидов
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/lead_statuses_by_source/"
                                                        title="Отчет по статусам лидов по источникам.">
                            Отчет по статусам лидов по источникам
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/leads_sb/"
                                                        title="Список лидов ШБ.">
                            Список лидов ШБ
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
            </tbody></table>
        </div>
    </div>
    <h3>Отчёты по сделкам</h3>
    <div class="reports-list-wrap">
        <div class="reports-list">
            <div class="reports-list-left-corner"></div>
            <div class="reports-list-right-corner"></div>
            <style>
                .reports-list-table th:hover {
                    cursor: default;
                }
            </style>
            <table cellspacing="0" id="reports_list_table_crm" class="reports-list-table">
                <tbody><tr>
                    <th class="reports-first-column reports-head-cell-top">
                        <div class="reports-head-cell"><!--<span class="reports-table-arrow"></span>--><span class="reports-head-cell-title">Название отчета</span></div>
                    </th>
                    <th class="reports-last-column">
                        <div class="reports-head-cell"><!--<span class="reports-table-arrow"></span>--><span class="reports-head-cell-title">Описание</span></div>
                    </th>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/deal_statuses/"
                                                        title="Отчет по статусам сделок.">
                            Отчет по статусам сделок
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/sale_funnel/"
                                                        title="Воронка продаж.">
                            Воронка продаж
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/deal_contacts/"
                                                        title="Отчет с выгрузкой контактов по сделкам.">
                            Отчет с выгрузкой контактов по сделкам
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                </tbody></table>
        </div>
    </div>
    <h3>Отчёты по телефонии</h3>
    <div class="reports-list-wrap">
        <div class="reports-list">
            <div class="reports-list-left-corner"></div>
            <div class="reports-list-right-corner"></div>
            <style>
                .reports-list-table th:hover {
                    cursor: default;
                }
            </style>
            <table cellspacing="0" id="reports_list_table_crm" class="reports-list-table">
                <tbody><tr>
                    <th class="reports-first-column reports-head-cell-top">
                        <div class="reports-head-cell"><!--<span class="reports-table-arrow"></span>--><span class="reports-head-cell-title">Название отчета</span></div>
                    </th>
                    <th class="reports-last-column">
                        <div class="reports-head-cell"><!--<span class="reports-table-arrow"></span>--><span class="reports-head-cell-title">Описание</span></div>
                    </th>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/telephony01/"
                                                        title="Полная таблица звонков.">
                            Полная таблица звонков
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/telephony02/"
                                                        title="Сводный отчет.">
                            Сводный отчет
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                <tr class="reports-list-item">
                    <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports.synergy/telephony03/"
                                                        title="Полная таблица разговоров.">
                            Полная таблица разговоров
                        </a></td>
                    <td class="reports-last-column">&nbsp;</td>
                </tr>
                </tbody></table>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>