<?php
/** ============================================================================================ */
/** Данные фильтров для отчета
 * ============================================================================================ */
if(!($from = filter_input(INPUT_POST, 'from', FILTER_SANITIZE_STRING))) { $from = date("Y-m-d H:i:s", strtotime("yesterday")); }
if(!($to = filter_input(INPUT_POST, 'to', FILTER_SANITIZE_STRING))) { $to = date("Y-m-d")." 00:00:00"; }
if(!($department = filter_input(INPUT_POST, 'department', FILTER_SANITIZE_NUMBER_INT))) { $department = 1; }
/** ============================================================================================ */
// подключение к БД
$conn = array(
	'host' => '10.10.60.94',
	'port' => '3306',
	'db' => 'sitemanager0',
	'user' => 'root',
	'pass' => 'Frtyfg56&fg5R6Rs',
);
$pdo = new PDO(
	"mysql:host={$conn['host']};dbname={$conn['db']};", 
	$conn['user'], 
	$conn['pass'], 
	array(
		PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
	)
);
/** ============================================================================================ */
/** Достаем данные из БД
 * ============================================================================================ */
// данные для отчета
$sql = "
	SELECT 
		`crm_lead`.`STATUS_ID`,
	   (SELECT 
			`b_crm_status`.`NAME` 
		FROM
			 `b_crm_status` 
		WHERE
			 `b_crm_status`.`ENTITY_ID` = 'STATUS' AND
			 `b_crm_status`.`STATUS_ID` = `crm_lead`.`STATUS_ID`
		) AS `STATUS`,
		COUNT(
			`crm_lead`.`STATUS_ID`
		) AS `CNT`,
		(SELECT
			NAME
		FROM
			b_iblock_section
		WHERE
			b_iblock_section.IBLOCK_ID = '1'
			AND b_iblock_section.ID = :dep_id
		LIMIT 1
		) AS `DEPARTMENT`
	FROM
		`b_crm_lead` `crm_lead` 
		LEFT JOIN `b_user` `crm_lead_assigned_by` ON `crm_lead`.`ASSIGNED_BY_ID` = `crm_lead_assigned_by`.`ID` /* связь лидов с менеджерами */
		LEFT JOIN `b_uts_user` ON `crm_lead_assigned_by`.`ID` = `b_uts_user`.`VALUE_ID` /* чтобы получить доступ к ID отдела менеджера */
	WHERE
		(
			(
				(
					DATE(`crm_lead`.`DATE_CREATE`) < :to 
				AND 
					DATE(`crm_lead`.`DATE_CREATE`) >= :from 
				)
			OR 
				DATE(`crm_lead`.`DATE_CREATE`) IS NULL
			) AND (
				`b_uts_user`.`UF_DEPARTMENT` = :department
			)
		)
	GROUP BY 
		`STATUS`
";
$param = array(
	':from' => $from,
	':to' => $to,
	':department' => 'a:1:{i:0;i:' . $department . ';}',
	':dep_id' => $department,
);
$sth = $pdo->prepare($sql);
$sth->execute($param);
$resultLeadsList = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<?php
header("Pragma: public"); 
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=report.xls");
header("Content-Transfer-Encoding: binary");
//header("Content-Length: ".filesize('report.xls'));
?>
<table class="table-result">
	<thead>
		<tr>
			<th>Статус заявки</th>
			<th>Количество</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($resultLeadsList as $v) {
		$report = "
		<tr>
			<td>{$v['STATUS']}</td>
			<td>{$v['CNT']}</td>
		</tr>
		";
		echo $report;
	} ?>
	</tbody>
</table>
<?php 
exit();
?>