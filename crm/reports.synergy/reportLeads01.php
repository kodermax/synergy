<?php
// шапка
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявки за период по статусам и ответственным");
/** ============================================================================================ */
/** Данные фильтров для отчета
 * ============================================================================================ */
if(!($from = filter_input(INPUT_POST, 'from', FILTER_SANITIZE_STRING))) { $from = date("Y-m-d H:i:s", strtotime("yesterday")); }
if(!($to = filter_input(INPUT_POST, 'to', FILTER_SANITIZE_STRING))) { $to = date("Y-m-d")." 00:00:00"; }
if(!($department = filter_input(INPUT_POST, 'department', FILTER_SANITIZE_NUMBER_INT))) { $department = 1; }
$departmentName = "Неизвестный отдел";
/** ============================================================================================ */
// подключение к БД
$conn = array(
	'host' => '10.10.60.94',
	'port' => '3306',
	'db' => 'sitemanager0',
	'user' => 'root',
	'pass' => 'Frtyfg56&fg5R6Rs',

);
$pdo = new PDO(
	"mysql:host={$conn['host']};dbname={$conn['db']};", 
	$conn['user'], 
	$conn['pass'], 
	array(
		PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
	)
);
/** ============================================================================================ */
/** Достаем данные из БД
 * ============================================================================================ */
// все подразделения
$sql = "
	SELECT DISTINCT
	   ID,
	   DEPTH_LEVEL,
	   NAME
	FROM
	   b_iblock_section
	WHERE
	   IBLOCK_ID = '1'
	ORDER BY
	   LEFT_MARGIN ASC
";
$sth = $pdo->prepare($sql);
$sth->execute();
$departmentList = $sth->fetchAll(PDO::FETCH_ASSOC);
$resultLeadsList = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	// данные для отчета
	$sql = "
		SELECT 
			`crm_lead_assigned_by`.`ID` AS `CRM_LEAD_ASSIGNED_BY_ID`,
			CASE WHEN length(
				  LTRIM(
					 RTRIM(
						concat(
						   ifnull(
							  `crm_lead_assigned_by`.`NAME`,
							  ' '
						   ),
						   ifnull(
							  `crm_lead_assigned_by`.`LAST_NAME`,
							  ' '
						   )
						)
					 )
				  )
			   )>0 THEN concat(
				  ifnull(
					 `crm_lead_assigned_by`.`NAME`,
					 ' '
				  ),
				  ' ',
				  ifnull(
					 `crm_lead_assigned_by`.`LAST_NAME`,
					 ' '
				  )
			   ) ELSE `crm_lead_assigned_by`.`LOGIN` END AS `CRM_LEAD_ASSIGNED_BY_SHORT_NAME`,
			   (SELECT 
					`b_crm_status`.`NAME` 
				FROM
					 `b_crm_status` 
				WHERE
					 `b_crm_status`.`ENTITY_ID` = 'STATUS' AND
					 `b_crm_status`.`STATUS_ID` = `crm_lead`.`STATUS_ID`
				) AS `STATUS`,
				COUNT(`crm_lead_assigned_by`.`ID`) AS `CNT`,
				(
					SELECT
						NAME
					FROM
						b_iblock_section
					WHERE
						b_iblock_section.IBLOCK_ID = '1'
						AND b_iblock_section.ID = :dep_id
					LIMIT 1
				) AS `DEPARTMENT`
		FROM

			`b_user` `crm_lead_assigned_by`
			LEFT JOIN `b_crm_lead` `crm_lead` ON `crm_lead`.`ASSIGNED_BY_ID` = `crm_lead_assigned_by`.`ID` /* связь лидов с менеджерами */

			LEFT JOIN `b_uts_user` ON `crm_lead_assigned_by`.`ID` = `b_uts_user`.`VALUE_ID` /* чтобы получить доступ к ID отдела менеджера */
		WHERE
			(
				(
					(
						DATE(`crm_lead`.`DATE_CREATE`) < :to 
					AND 
						DATE(`crm_lead`.`DATE_CREATE`) >= :from 
					)
				OR 
					DATE(`crm_lead`.`DATE_CREATE`) IS NULL
				) AND (
					`b_uts_user`.`UF_DEPARTMENT` = :department
				)
			)
		GROUP BY 
			`CRM_LEAD_ASSIGNED_BY_ID`, 
			`STATUS`

	";
	$param = array(
		':from' => $from,
		':to' => $to,
		':department' => 'a:1:{i:0;i:' . $department . ';}',
		':dep_id' => $department,
	);
	$sth = $pdo->prepare($sql);
	$sth->execute($param);
	$resultLeadsList = $sth->fetchAll(PDO::FETCH_ASSOC);
}
/** ============================================================================================ */
/** формирование элементов представления 
 * ============================================================================================ */
// Стили
?>
<style>
.form-element { display: inline-block; }
.filter { background-color: #F5F5F5; border: 1px ridge #000000; padding: 10px 5px; }
.result { padding: 10px 0; }
table { border-collapse: collapse; }
.table-result td { border: 1px solid #AAAAAA; padding: 1px 5px; }
.table-result tr:hover { background-color: #C5C5C5; }
</style>
<?php
// SELECT для подразделений
$departmentSelectLabel = "<label for='department'>Отдел</label>";
$departmentSelectTag = "<SELECT id='department' name='department'>\n";
foreach($departmentList as $v) {
	$level = "";
	for($i=1;$i<(int)$v['DEPTH_LEVEL'];$i++) {$level .= " . "; }
	
	$departmentSelectTag .= "<OPTION value='{$v['ID']}'";
	$departmentSelectTag .= ($department==$v['ID']) ? "selected='selected'" : "";
	$departmentSelectTag .= ">{$level}{$v['NAME']}</OPTION>\n";
	
	if($department == $v['ID']) { $departmentName = $v['NAME']; };
}
$departmentSelectTag .= "</SELECT>";
// данные отчета
$report = "";
foreach($resultLeadsList as $v) {
	$report .= "
		<tr>
			<td>{$v['CRM_LEAD_ASSIGNED_BY_ID']}</td>
			<td>{$v['CRM_LEAD_ASSIGNED_BY_SHORT_NAME']}</td>
			<td>{$v['STATUS']}</td>
			<td>{$v['CNT']}</td>
		</tr>
	";
}

/** ============================================================================================ */
/** Отображение представления <td>{$v['']}</td>	
 * ============================================================================================ */
// Блок фильтров
?>
	<div style="margin-top: 10px; margin-bottom: 10px;"><a href="/crm/reports.synergy/">&lt;&lt; Назад к списку отчетов</a></div>
<div class="filter">
	<form method="POST">
		<div class="form-element">
			<label for="from">Дата от</label>
			<input id="from" name="from" type="text" placeholder="2015-01-01 00:00:00" value="<?php echo $from ?>">
		</div>
		<div class="form-element">
			<label for="to">до</label>
			<input id="to" name="to" type="text" placeholder="2015-01-02 00:00:00" value="<?php echo $to ?>">
		</div>
		<div class="form-element">
			<?php echo $departmentSelectLabel; ?>
			<?php echo $departmentSelectTag; ?>
		</div>
		<div class="form-element">
			<input type="submit" value="Найти">
		</div>
	</form>
</div>
<?php if ($_SERVER['REQUEST_METHOD'] == 'POST') { ?>
<div class="result">
	<h2><?php echo $departmentName; ?></h2>
		<form method="POST" target="_blank" action="reportLeads01xls.php">

			<input name="from" type="hidden" value="<?php echo $from; ?>">
			<input name="to" type="hidden"value="<?php echo $to; ?>">
			<input name="department" type="hidden"value="<?php echo $department; ?>">
			<input name="export" type="hidden"value="excel">
			<button type="submit">Выгрузить в Excel</button>
		</form>
	<table class="table-result">
		<tr>
			<th>№</th>
			<th>Ответственный</th>
			<th>Статус заявки</th>
			<th>Количество</th>
		</tr>
		<?php echo $report; ?>
	</table>
</div>
<? } ?>
<?php
// раздел для отображения отладочной информации
if(stripos($_SERVER['HTTP_HOST'], 'msk1-portaldev01') !== false) {
?>
<div class="debug"><hr><pre><?php
	// ...
?></pre><hr></div>
<?php
}
// подвал
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");