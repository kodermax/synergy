<?
$MESS["SEARCH_FONT_MAX"] = "Didžiausias šrifto dydis (pks)";
$MESS["SEARCH_FONT_MIN"] = "Mažiausias šrifto dydis (pks)";
$MESS["SEARCH_COLOR_OLD"] = "Paskutinės žymės spalva (pvz. \"FEFEFE\")";
$MESS["SEARCH_COLOR_NEW"] = "Ankstesnės žymės spalva (pvz. \"C0C0C0\")";
$MESS["SEARCH_PERIOD_NEW_TAGS"] = "Laikyti žymę kaip maują per (dienas)";
$MESS["SEARCH_SHOW_CHAIN"] = "Rodyti džiūvėsėlių navigaciją.";
$MESS["SEARCH_COLOR_TYPE"] = "Naudoti gradiento spalvas";
$MESS["SEARCH_WIDTH"] = "Žymių debesies plotis (pvz. \"100%\", \"100px\", \"100pt\" or \"100in\")";
?>