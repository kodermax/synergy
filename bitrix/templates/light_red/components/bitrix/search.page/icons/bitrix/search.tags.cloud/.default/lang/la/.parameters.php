<?
$MESS["SEARCH_FONT_MAX"] = "Mayor tamaño de fuente (píxeles)";
$MESS["SEARCH_FONT_MIN"] = "Menor tamaño de fuente (píxeles)";
$MESS["SEARCH_COLOR_OLD"] = "Últimos color de la etiqueta (ex. \"FEFEFE\")";
$MESS["SEARCH_COLOR_NEW"] = "Primeros colores de la etiqueta (ex. \"C0C0C0\")";
$MESS["SEARCH_PERIOD_NEW_TAGS"] = "Considerar etiqueta nueva durante (días)";
$MESS["SEARCH_SHOW_CHAIN"] = "Mostrar ruta de navegación.";
$MESS["SEARCH_COLOR_TYPE"] = "Utilice los colores de degradado";
$MESS["SEARCH_WIDTH"] = "Ancho de nube de la etiquetas (ex. \"100%\", \"100px\", \"100pt\" or \"100in\")";
?>