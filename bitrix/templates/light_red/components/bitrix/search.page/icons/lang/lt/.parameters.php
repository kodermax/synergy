<?
$MESS["TP_BSP_USE_SUGGEST"] = "Rodyti paieškos frazės instrukcijas";
$MESS["TP_BSP_SHOW_ITEM_TAGS"] = "Rodyti dokumento žymes";
$MESS["TP_BSP_TAGS_INHERIT"] = "Siaura paieškos sritis";
$MESS["TP_BSP_SHOW_ITEM_DATE_CHANGE"] = "Rodyti pakeitimo datą";
$MESS["TP_BSP_SHOW_ORDER_BY"] = "Rodyti r86iavimo tvarką";
$MESS["TP_BSP_SHOW_TAGS_CLOUD"] = "Rodyti žymių debesį";
$MESS["TP_BSP_FONT_MAX"] = "Didžiausias šrifto dydis (pks)";
$MESS["TP_BSP_FONT_MIN"] = "Mažiausias šrifto dydis (pks)";
$MESS["TP_BSP_COLOR_OLD"] = "Paskutinės žymės spalva  (pvz. \"FEFEFE\")";
$MESS["TP_BSP_COLOR_NEW"] = "Ankstesnės žymės spalva  (pvz. \"C0C0C0\")";
$MESS["TP_BSP_CNT"] = "Pagal dažnumą";
$MESS["TP_BSP_COLOR_TYPE"] = "Naudoti gradiento spalvas";
$MESS["TP_BSP_NAME"] = "Pagal pavadinimą";
$MESS["TP_BSP_PAGE_ELEMENTS"] = "Žymių skaičius";
$MESS["TP_BSP_PERIOD"] = "Ieškoti žymes per (dienų)";
$MESS["TP_BSP_PERIOD_NEW_TAGS"] = "Laikyti žymę kaip naują per (dienų)";
$MESS["TP_BSP_WIDTH"] = "Žymių debesio plotis (pvz. \"100%\", \"100px\", \"100pt\" or \"100in\")";
$MESS["TP_BSP_URL_SEARCH"] = "Kelias ieškoti puslapį  (susijusį su svetainės šaknimi)";
$MESS["TP_BSP_SHOW_CHAIN"] = "Rodyti džiūvėsėlių navigaciją";
$MESS["TP_BSP_SORT"] = "Reitingo žymės";
?>