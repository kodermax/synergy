<?
$MESS["SONET_UM_GENERAL"] = "General";
$MESS["SONET_UM_LOG"] = "Flujo de actividad";
$MESS["SONET_UM_FRIENDS"] = "Amigos";
$MESS["SONET_UM_GROUPS"] = "Grupos";
$MESS["SONET_UM_PHOTO"] = "Foto";
$MESS["SONET_UM_FORUM"] = "Foro";
$MESS["SONET_UM_CALENDAR"] = "Calendario";
$MESS["SONET_UM_FILES"] = "Archivos";
$MESS["SONET_UM_BLOG"] = "Conversaciones";
$MESS["SONET_UM_TASKS"] = "Tareas";
$MESS["SONET_UM_SEND_MESSAGE"] = "Enviar de mensajes";
$MESS["SONET_UM_VIDEO_CALL"] = "Vídeo llamada";
$MESS["SONET_UM_ONLINE"] = "En línea";
$MESS["SONET_UM_ABSENT"] = "(fuera de la oficina)";
$MESS["SONET_UM_MESSAGES"] = "Mensajes";
$MESS["SONET_UM_BIRTHDAY"] = "Hoy es el cumpleaños de un usuario";
$MESS["SONET_UM_HONOUR"] = "El usuario esta en el cuadro de Honor";
$MESS["SONET_UM_EDIT_PROFILE"] = "Editar perfil";
$MESS["SONET_UM_EDIT_SETTINGS"] = "Editar configuración de privacidad";
$MESS["SONET_UM_EDIT_FEATURES"] = "Editar configuración";
$MESS["SONET_UM_SUBSCRIBE"] = "Subscripción";
$MESS["SONET_UM_REQUESTS"] = "Invitaciones y solicitudes";
?>