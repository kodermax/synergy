<?
$MESS["SONET_UM_GENERAL"] = "Bendras";
$MESS["SONET_UM_LOG"] = "Veiklos juosta";
$MESS["SONET_UM_FRIENDS"] = "Draugai";
$MESS["SONET_UM_GROUPS"] = "Grupės";
$MESS["SONET_UM_PHOTO"] = "Nuotrauka";
$MESS["SONET_UM_FORUM"] = "Forumas";
$MESS["SONET_UM_CALENDAR"] = "Kalendorius";
$MESS["SONET_UM_FILES"] = "Failai";
$MESS["SONET_UM_BLOG"] = "Pokalbiai";
$MESS["SONET_UM_TASKS"] = "Užduotys";
$MESS["SONET_UM_SEND_MESSAGE"] = "Siųsti žinutę";
$MESS["SONET_UM_VIDEO_CALL"] = "Vaizdo skambutis";
$MESS["SONET_UM_ONLINE"] = "Online";
$MESS["SONET_UM_ABSENT"] = "(nėra biure)";
$MESS["SONET_UM_MESSAGES"] = "Pranešimai";
$MESS["SONET_UM_BIRTHDAY"] = "Šiandien yra naudotjo gimtadienis";
$MESS["SONET_UM_HONOUR"] = "Naudotojas yra garbės lentoje";
$MESS["SONET_UM_EDIT_PROFILE"] = "Readaguoti profilį";
$MESS["SONET_UM_EDIT_SETTINGS"] = "Redaguoti privatumo nustatymus";
$MESS["SONET_UM_EDIT_FEATURES"] = "Redaguoti nustatymus";
$MESS["SONET_UM_SUBSCRIBE"] = "Prenumerata";
$MESS["SONET_UM_REQUESTS"] = "Kvietimai ir prašymai";
?>