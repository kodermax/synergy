<?
$MESS["BLOG_MENU_DRAFT_MESSAGES"] = "Borrador";
$MESS["BLOG_MENU_DRAFT_MESSAGES_TITLE"] = "Ver conversaciones inéditas";
$MESS["BLOG_MENU_MODERATION_MESSAGES"] = "Moderación";
$MESS["BLOG_MENU_MODERATION_MESSAGES_TITLE"] = "Ver los mensajes en espera de moderación";
$MESS["BLOG_MENU_4ME_ALL"] = "Todos";
$MESS["BLOG_MENU_MINE"] = "Mi";
$MESS["BLOG_MENU_4ME"] = "Para mí";
$MESS["BLOG_MENU_MINE_TITLE"] = "Ver mis mensajes";
$MESS["BLOG_MENU_4ME_TITLE"] = "Ver mensajes que me han dirigido";
$MESS["BLOG_MENU_4ME_ALL_TITLE"] = "Ver todas las conversaciones";
$MESS["BLOG_MENU_4ME_DR"] = "Mi departamento";
$MESS["BLOG_MENU_4ME_DR_TITLE"] = "Ver conversaciones en mi departamento";
?>