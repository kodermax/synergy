<?
$MESS["SAF_TP_PATH_TO_MYPORTAL"] = "Naudotojo darbalaukio puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_SONET_PROFILE"] = "Naudotojo profilio puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_SONET_BIZPROC"] = "Verslo proceso puslapio kelias";
$MESS["SAF_TP_PATH_TO_SONET_GROUP"] = " Darbo grupės puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES"] = "Naudotojo pranešimo puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM"] = "Pranešimo formos kelio  šablonas";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM_MESS"] = "Atsakymo formos kelio  šablonas";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES_CHAT"] = "Pokalbio puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_SONET_GROUPS"] = "Naudotojo grupės puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_SONET_LOG"] = "Naudotojo atnaujinimų puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_BLOG"] = "Naudotojo blogo puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_PHOTO"] = "Naudotojo nuotraukų galerijos puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_CALENDAR"] = "Naudotojo kalendoriaus puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_TASKS"] = "Naudotojo užduočių puslapio kelio  šablonas";
$MESS["SAF_TP_PATH_TO_FILES"] = "Naudotojo failų puslapio kelio  šablonas";
?>