<?
$MESS["SAF_TP_PATH_TO_MYPORTAL"] = "Plantilla de la ruta a la página de escritorio del usuario";
$MESS["SAF_TP_PATH_TO_SONET_PROFILE"] = "Plantilla de la ruta a la página del perfil del usuario";
$MESS["SAF_TP_PATH_TO_SONET_BIZPROC"] = "Ruta a la página de Business Processes";
$MESS["SAF_TP_PATH_TO_SONET_GROUP"] = "Plantilla de la ruta a la página del grupo de trabajo";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES"] = "Plantilla de la ruta a la página de mensajes del usuario";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM"] = "Plantilla de la ruta a la página del formulario de mensajes";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM_MESS"] = "Ruta a la página al formulario de respuestas";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES_CHAT"] = "Plantilla de la ruta a la página del Chat";
$MESS["SAF_TP_PATH_TO_SONET_GROUPS"] = "Plantilla de la ruta a la página del grupo de usuarios";
$MESS["SAF_TP_PATH_TO_SONET_LOG"] = "Plantilla de la ruta a la página de actualización de usuarios";
$MESS["SAF_TP_PATH_TO_BLOG"] = "Plantilla de la ruta a la página del usuario del blog";
$MESS["SAF_TP_PATH_TO_PHOTO"] = "Plantilla de la ruta a la página de Galería de fotos del usuario";
$MESS["SAF_TP_PATH_TO_CALENDAR"] = "Plantilla de la ruta a la página de calendarios de usuario";
$MESS["SAF_TP_PATH_TO_TASKS"] = "Plantilla de la URL a la página de tareas del usuario";
$MESS["SAF_TP_PATH_TO_FILES"] = "Plantilla de la ruta a la página de archivos del usuario";
?>