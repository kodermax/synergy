<?
$MESS["SONET_UM_ONLINE"] = "Online";
$MESS["SONET_UM_ABSENT"] = "(nėra biure)";
$MESS["SONET_UM_GENERAL"] = "Bendras";
$MESS["SONET_UM_FRIENDS"] = "Draugai";
$MESS["SONET_UM_GROUPS"] = "Grupės";
$MESS["SONET_UM_PHOTO"] = "Nuotrauka";
$MESS["SONET_UM_FORUM"] = "Forumas";
$MESS["SONET_UM_CALENDAR"] = "Kalendorius";
$MESS["SONET_UM_FILES"] = "Failai";
$MESS["SONET_UM_BLOG"] = "Blogas";
$MESS["SONET_UM_TASKS"] = "Užduotys";
$MESS["SONET_UM_MICROBLOG"] = "Mikroblogas";
$MESS["SONET_UM_SEARCH"] = "Paieška";
?>