<?
$MESS["SONET_UM_MY_MESSAGES"] = "Mis mensajes";
$MESS["SONET_UM_MESSAGES"] = "Mensajes";
$MESS["SONET_UM_INPUT"] = "Entrante";
$MESS["SONET_UM_OUTPUT"] = "Saliente";
$MESS["SONET_UM_USER"] = "Mi perfil";
$MESS["SONET_UM_USER_BAN"] = "Lista de prohibición";
$MESS["SONET_UM_MUSERS"] = "Mensajes";
$MESS["SONET_UM_LOG"] = "Flujo de actividad";
$MESS["SONET_UM_SUBSCRIBE"] = "Subscripción";
$MESS["SONET_UM_TASKS"] = "Subscripción";
$MESS["SONET_UM_BIZPROC"] = "Business Processes";
$MESS["SONET_UM_ONLINE"] = "En línea";
$MESS["SONET_UM_ABSENT"] = "(fuera de la oficina)";
$MESS["SONET_UM_BIRTHDAY"] = "Hoy es cumpleaños de un usuario";
$MESS["SONET_UM_HONOUR"] = "El usuario se encuentra en el cuadro de Honor";
$MESS["SONET_UM_EDIT_PROFILE"] = "Editar perfil";
$MESS["SONET_UM_EDIT_SETTINGS"] = "Editar configuración de privacidad";
$MESS["SONET_UM_EDIT_FEATURES"] = "Editar configuración";
?>