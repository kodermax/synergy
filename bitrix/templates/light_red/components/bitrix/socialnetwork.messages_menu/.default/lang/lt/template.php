<?
$MESS["SONET_UM_MY_MESSAGES"] = "Mano pranešimai";
$MESS["SONET_UM_MESSAGES"] = "Pranešimai";
$MESS["SONET_UM_INPUT"] = "Gaunami";
$MESS["SONET_UM_OUTPUT"] = "Siunčiami";
$MESS["SONET_UM_USER"] = "Mano profilis";
$MESS["SONET_UM_USER_BAN"] = "Ban sąrašas";
$MESS["SONET_UM_MUSERS"] = "Pranešimai";
$MESS["SONET_UM_LOG"] = "Veiklos juosta";
$MESS["SONET_UM_SUBSCRIBE"] = "Prenumerata";
$MESS["SONET_UM_TASKS"] = "Prenumerata";
$MESS["SONET_UM_BIZPROC"] = "Verrslo procesai";
$MESS["SONET_UM_ONLINE"] = "Online";
$MESS["SONET_UM_ABSENT"] = "(nėra biure)";
$MESS["SONET_UM_BIRTHDAY"] = "Šiandien yra naudotojo gimtadienis";
$MESS["SONET_UM_HONOUR"] = "Naudotojas yra garbės lentoje";
$MESS["SONET_UM_EDIT_PROFILE"] = "Redaguoti profilį";
$MESS["SONET_UM_EDIT_SETTINGS"] = "Redaguoti privatumo nustatymus";
$MESS["SONET_UM_EDIT_FEATURES"] = "Redaguoti nustatymus";
?>