<?
$MESS["P_ADD_ALBUM"] = "Naujas albumas";
$MESS["P_UPLOAD"] = "Įkelti nuotraukas";
$MESS["P_SECTION_EDIT"] = "Redaguoti albumą";
$MESS["P_SECTION_EDIT_ICON"] = "Redaguoti albumo viršelį";
$MESS["P_SECTION_DELETE"] = "Pašalinti albumą";
$MESS["P_SECTION_DELETE_ASK"] = "Ar tikrai norite pašalinti albumą negrįžtamai?";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "Albumas yra paslėptas.";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "Albumas yra paslėptas ir apsaugotas slaptažodiu.";
$MESS["P_ALBUM_IS_PASSWORDED"] = "Albumas yra apsaugotas slaptažodiu.";
?>