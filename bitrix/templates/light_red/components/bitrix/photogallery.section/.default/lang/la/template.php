<?
$MESS["P_ADD_ALBUM"] = "Nuevo álbum";
$MESS["P_UPLOAD"] = "Subir fotos";
$MESS["P_SECTION_EDIT"] = "Editar álbum";
$MESS["P_SECTION_EDIT_ICON"] = "Editar portada del ábum";
$MESS["P_SECTION_DELETE"] = "Eliminar álbum";
$MESS["P_SECTION_DELETE_ASK"] = "Está seguro de que desea eliminar el álbum de forma irreversible?";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "El álbum está oculto.";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "El álbum está oculto y protegido por contraseña.";
$MESS["P_ALBUM_IS_PASSWORDED"] = "El álbum está protegido con contraseña.";
?>