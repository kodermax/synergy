<?
$MESS["INTR_COMP_IS_TPL_FILTER_SIMPLE"] = "Búsqueda";
$MESS["INTR_COMP_IS_TPL_FILTER_ADV"] = "Búsqueda avanzada";
$MESS["INTR_COMP_IS_TPL_FILTER_ALPH"] = "Búsqueda por Orden Alfabética";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_LETTER"] = "Letra";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW"] = "Ver";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_LIST"] = "lista";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_TABLE"] = "detalles";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL"] = "Excel";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL_TITLE"] = "Exportar a MS Excel los Resultados de Búsqueda ";
$MESS["INTR_COMP_IS_TPL_FILTER_AZ"] = "A-Z";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK"] = "Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Exportar Empleados a la lista de contactos del Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Puede exportar los empleados como contactos para Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK_BUTTON"] = "Exportar";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV"] = "Sincronizar Vía CardDAV";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Sincronizar los empleados con el registro del software y hardware de soporte CardDAV (iPhone, iPad etc.)";
?>