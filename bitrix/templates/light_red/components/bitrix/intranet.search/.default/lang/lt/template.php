<?
$MESS["INTR_COMP_IS_TPL_FILTER_SIMPLE"] = "Paieška";
$MESS["INTR_COMP_IS_TPL_FILTER_ADV"] = "Papildoma paieška";
$MESS["INTR_COMP_IS_TPL_FILTER_ALPH"] = "Paieška pagal abėcėlę";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_LETTER"] = "Raidė";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW"] = "Peržiūra";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_LIST"] = "sąrašas";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_TABLE"] = "informacija";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL"] = "Excel";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL_TITLE"] = "Eksportuoti paieškos rezultatus į MS Excel";
$MESS["INTR_COMP_IS_TPL_FILTER_AZ"] = "A-Z";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK"] = "Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Eksportuoti darbuotojų sąrašą kaip Outlook'o kontaktus";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Jūs galite eksportuoti darbuotojus kaip kontaktus į Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK_BUTTON"] = "Eksportuoti";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV"] = "Sinchronizuoti per CardDAV";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Sinchronizuoti darbuotojų registrą su programina ir technina įranga, palaikanča CardDAV (iPhone, iPad ir tt.)";
?>