<?
$MESS["SONET_UM_GENERAL"] = "General";
$MESS["SONET_UM_MODS"] = "Moderadores";
$MESS["SONET_UM_USERS"] = "Miembros";
$MESS["SONET_UM_PHOTO"] = "Foto";
$MESS["SONET_UM_FORUM"] = "Foro";
$MESS["SONET_UM_BLACKLIST"] = "Lista de prohibiciones";
$MESS["SONET_UM_BLOG"] = "Conversaciones";
$MESS["SONET_UM_CALENDAR"] = "Calendario";
$MESS["SONET_UM_FILES"] = "Archivos";
$MESS["SONET_UM_SEARCH"] = "Búsqueda";
$MESS["SONET_UM_ARCHIVE_GROUP"] = "(grupo archivado)";
$MESS["SONET_UM_SEND_MESSAGE"] = "Enviar un mensaje a los participantes";
$MESS["SONET_UM_EDIT"] = "Parámetros del grupo";
$MESS["SONET_UM_FEATURES"] = "Configuración de funciones";
$MESS["SONET_UM_DELETE"] = "Eliminar grupo";
$MESS["SONET_UM_MOD"] = "Editar moderadores";
$MESS["SONET_UM_MOD1"] = "Moderadores";
$MESS["SONET_UM_BAN"] = "Lista de prohibiciones";
$MESS["SONET_UM_INVITE"] = "Invitar al grupo";
$MESS["SONET_UM_REQUESTS_OUT"] = "Invitaciones al grupo";
$MESS["SONET_UM_JOIN"] = "Unirse al Grupo";
$MESS["SONET_UM_LEAVE"] = "Dejar al Grupo";
$MESS["SONET_UM_SUBSCRIBE"] = "Subscripción";
$MESS["SONET_UM_MEMBERS"] = "Editar usuarios";
$MESS["SONET_UM_MEMBERS1"] = "Miembros";
$MESS["SONET_UM_IS_EXTRANET"] = " (extranet)";
$MESS["SONET_UM_REQUESTS"] = "Invitaciones y solicitudes";
$MESS["SONET_SGM_T_SESSION_WRONG"] = "Su sesión ha caducado. Por favor, inténtelo de nuevo.";
$MESS["SONET_SGM_T_NOT_ATHORIZED"] = "Usted no está logueado";
$MESS["SONET_SGM_T_MODULE_NOT_INSTALLED"] = "El módulo Social Network no está instalado";
$MESS["SONET_SGM_T_WAIT"] = "Por favor, espere...";
$MESS["SONET_SGM_T_NOTIFY_HINT_OFF"] = "Usted ya no recibirá más actualizaciones<br>del grupo en su messenger";
$MESS["SONET_SGM_T_NOTIFY_HINT_ON"] = "Usted recibirá actualizaciones<br>del grupo en su messenger";
$MESS["SONET_SGM_T_NOTIFY_TITLE_OFF"] = "Notificaciones desactivadas";
$MESS["SONET_SGM_T_NOTIFY_TITLE_ON"] = "Notificaciones activadas";
?>