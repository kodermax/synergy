<?
$MESS["P_PHOTOS"] = "nuotraukos";
$MESS["P_SECTION_EDIT"] = "Redaguoti";
$MESS["P_SECTION_EDIT_TITLE"] = "Redaguoti albumą";
$MESS["P_SECTION_DELETE"] = "Pašalinti";
$MESS["P_SECTION_DELETE_TITLE"] = "Pašalinti albumą";
$MESS["P_SECTION_DELETE_ASK"] = "Ar tikrai norite pašalinti albumą negrįžtamai?";
$MESS["P_EDIT_ICON"] = "Pasirinkti viršelį";
$MESS["P_EDIT_ICON_TITLE"] = "Pasirinkti albumo viršelį";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "Albumas yra paslėptas.";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "Albumas yra paslėptas ir apsaugotas slaptažodžiu.";
$MESS["P_ALBUM_IS_PASSWORDED"] = "Apsaugotas slaptažodžiu.";
$MESS["P_EMPTY_DATA"] = "Dar nėra pridėtų albumų";
?>