<?
$MESS["P_PHOTOS"] = "fotos";
$MESS["P_SECTION_EDIT"] = "Editar";
$MESS["P_SECTION_EDIT_TITLE"] = "Editar álbum";
$MESS["P_SECTION_DELETE"] = "Eliminar";
$MESS["P_SECTION_DELETE_TITLE"] = "Eliminar álbum";
$MESS["P_SECTION_DELETE_ASK"] = "Es seguro que quieres eliminar el álbum de forma irreversible?";
$MESS["P_EDIT_ICON"] = "Elija portada";
$MESS["P_EDIT_ICON_TITLE"] = "Elija portada del álbum";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "El álbum está oculto.";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "El álbum está oculto y protegido por contraseña.";
$MESS["P_ALBUM_IS_PASSWORDED"] = "Protegido por contraseña.";
$MESS["P_EMPTY_DATA"] = "Todavia no hay álbum agregado";
?>