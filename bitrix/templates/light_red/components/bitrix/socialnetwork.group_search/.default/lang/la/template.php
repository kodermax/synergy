<?
$MESS["SONET_C24_T_SEARCH_TITLE"] = "Búsqueda de grupos";
$MESS["SONET_C24_T_SEARCH"] = "Búsqueda";
$MESS["SONET_C24_T_SUBJECT"] = "El tema";
$MESS["SONET_C24_T_ANY"] = "Año";
$MESS["SONET_C24_T_DO_SEARCH"] = "&nbsp;&nbsp;Search&nbsp;&nbsp;";
$MESS["SONET_C24_T_DO_CANCEL"] = "&nbsp;&nbsp;Cancel&nbsp;&nbsp;";
$MESS["SONET_C24_T_CREATE_GROUP"] = "Crear grupo";
$MESS["SONET_C24_T_SUBJ"] = "Tema";
$MESS["SONET_C24_T_MEMBERS"] = "Miembros";
$MESS["SONET_C24_T_ACTIVITY"] = "Ultimo acceso";
$MESS["SONET_C24_T_ORDER_REL"] = "Organizar por relevancia";
$MESS["SONET_C24_T_ORDER_DATE"] = "Ordenar por Fecha";
$MESS["SONET_C39_ARCHIVE_GROUP"] = "Archivos de grupo";
?>