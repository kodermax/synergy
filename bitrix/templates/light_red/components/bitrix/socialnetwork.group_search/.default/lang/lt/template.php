<?
$MESS["SONET_C24_T_SEARCH_TITLE"] = "Ieškoti grupėse";
$MESS["SONET_C24_T_SEARCH"] = "Ieškoti ";
$MESS["SONET_C24_T_SUBJECT"] = "Temoje";
$MESS["SONET_C24_T_ANY"] = "Bet kas";
$MESS["SONET_C24_T_DO_SEARCH"] = "&nbsp;&nbsp;Search&nbsp;&nbsp;";
$MESS["SONET_C24_T_DO_CANCEL"] = "&nbsp;&nbsp;Cancel&nbsp;&nbsp;";
$MESS["SONET_C24_T_CREATE_GROUP"] = "Sukurti grupę";
$MESS["SONET_C24_T_SUBJ"] = "Tema";
$MESS["SONET_C24_T_MEMBERS"] = "Dalyviai";
$MESS["SONET_C24_T_ACTIVITY"] = "Paskutinis apsilankymas";
$MESS["SONET_C24_T_ORDER_REL"] = "Rūšiuoti pagal svarbumą";
$MESS["SONET_C24_T_ORDER_DATE"] = "Rūšiuoti pagal datą";
$MESS["SONET_C39_ARCHIVE_GROUP"] = "Archyvuoti grupę";
?>