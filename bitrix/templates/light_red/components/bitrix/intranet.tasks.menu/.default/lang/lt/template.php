<?
$MESS["INTMT_BACK2LIST_DESCR"] = "Grįžti prie užduočių sąrašo";
$MESS["INTMT_BACK2LIST"] = "Užduotys";
$MESS["INTMT_EDIT_TASK_DESCR"] = "Redaguoti dabartinę užduotį";
$MESS["INTMT_EDIT_TASK"] = "Redaguoti užduotį";
$MESS["INTMT_VIEW_TASK_DESCR"] = "Peržiūrėti dabartinę užduotį";
$MESS["INTMT_VIEW_TASK"] = "Peržiūrėti užduotį";
$MESS["INTMT_CREATE_TASK_DESCR"] = "Sukurti naują užduotį";
$MESS["INTMT_CREATE_TASK"] = "Sukurti užduotį";
$MESS["INTMT_CREATE_FOLDER_DESCR"] = "Sukurti naują aplanką";
$MESS["INTMT_CREATE_FOLDER"] = "Sukurti aplanką";
$MESS["INTMT_VIEW"] = "Peržiūra";
$MESS["INTMT_DEFAULT"] = "Numatytasis";
$MESS["INTMT_CREATE_VIEW"] = "Sukurti peržiūrą";
$MESS["INTMT_EDIT_VIEW"] = "Redaguoti peržiūrą";
$MESS["INTMT_DELETE_VIEW_CONF"] = "Ar tikrai norite pašalinti šią peržiūrą?";
$MESS["INTMT_DELETE_VIEW"] = "Pašalinti šią peržiūrą";
$MESS["INTMT_OUTLOOK"] = "Prisijungti prie Outlook'o";
$MESS["INTMT_OUTLOOK_TITLE"] = "Outlook'o užduočių sinchronizavimas";
?>