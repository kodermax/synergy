<?
$MESS["SONET_P_USER_SEX"] = "Lytis:";
$MESS["SONET_P_USER_BIRTHDAY"] = "Gimimo diena:";
$MESS["SONET_C38_T_ONLINE"] = "Online";
$MESS["SONET_C38_T_HONOURED"] = "Nusipelnęs
";
$MESS["SONET_C38_T_BIRTHDAY"] = "Gimimo diena";
$MESS["SONET_C39_ABSENT"] = "Nėra biure";
$MESS["SONET_C39_SEND_MESSAGE"] = "Siųsti pranešimą";
$MESS["SONET_C39_SHOW_MESSAGES"] = "Rodyti pranešimų žurnalą";
$MESS["SONET_C39_FR_DEL"] = "Nebedraugauti";
$MESS["SONET_C39_FR_ADD"] = "Pridėti prie draugų";
$MESS["SONET_C39_INV_GROUP"] = "Pakviesti į grupę";
$MESS["SONET_C39_EDIT_PROFILE"] = "Redaguoti profilį";
$MESS["SONET_C39_EDIT_SETTINGS"] = "Redaguoti privatumo nustatymus";
$MESS["SONET_C39_EDIT_FEATURES"] = "Redaguoti nustatymus";
$MESS["SONET_C39_CONTACT_TITLE"] = "Kontaktinė informacija";
$MESS["SONET_C39_CONTACT_UNAVAIL"] = "Kontaktinė informacija prieinama.";
$MESS["SONET_C39_PERSONAL_TITLE"] = "Asmeninė informacija";
$MESS["SONET_C39_PERSONAL_UNAVAIL"] = "Asmeninė informacija prieinama.";
$MESS["SONET_C38_TP_NO_PERMS"] = "Jūs neturite leidimo peržiūrėti šio naudotojo profilį.";
$MESS["SONET_C39_VIDEO_CALL"] = "Vaizdo skambutis";
?>