<?
$MESS["CMDESKTOP_TDEF_ERR1"] = "Klaida išsaugant  įtaiso poziciją serveryje.";
$MESS["CMDESKTOP_TDEF_ERR2"] = "Klaida pridedant įtaiso poziciją prie serverio.";
$MESS["CMDESKTOP_TDEF_CONF"] = "Jūsų asmeniniai prietaisų skydo nustatymai bus taikomi kaip numatytieji visiems naujiems arba neautorizuotiems naudotojams. Tęsti?";
$MESS["CMDESKTOP_TDEF_CONF_USER"] = "Jūsų darbalaukio nustatymai bus taikomi kaip numatytieji visiems naujų naudotojų profiliams. Ar norite tęsti?";
$MESS["CMDESKTOP_TDEF_CONF_GROUP"] = "Jūsų darbalaukio nustatymai bus taikomi kaip numatytieji visioms naujoms darbo grupėms. Ar norite tęsti?";
$MESS["CMDESKTOP_TDEF_ADD"] = "Pridėti prietaisą";
$MESS["CMDESKTOP_TDEF_SET"] = "Išsaugoti kaip numatytuosius nustatymus";
$MESS["CMDESKTOP_TDEF_CLEAR"] = "Atstatyti dabartinius nustatymus";
$MESS["CMDESKTOP_TDEF_CANCEL"] = "Atšaukti";
$MESS["CMDESKTOP_DESC_NAME"] = "Asmeninis įrankių skydas";
$MESS["CMDESKTOP_TDEF_DELETE"] = "Pašalinti";
$MESS["CMDESKTOP_TDEF_SETTINGS"] = "Nustatymai";
$MESS["CMDESKTOP_DEMO_DATA_BLOCK_TITLE"] = "Valomos demo duomenys";
$MESS["CMDESKTOP_DEMO_DATA_BLOCK_DESC"] = "Norėdami pašalinti demo duomenis iš savo portalo, naudokite  <b>Valymo vedlį</b>. Jūs galite paleisti vedlį, paspausdami \"Valymo vedlys\" valdymo skydo juostoje naršymo režime arba naudojant <a href='#LINK_TO_WIZARD#'>šią nuorodą</a>.";
$MESS["CMDESKTOP_TDEF_CLEAR_CONF"] = "Numatytieji parametrai bus pritaikyti jūsų darbalaukiui. Tęsti?";
$MESS["CMDESKTOP_TDEF_HIDE"] = "Slėpti/Rodyti";
?>