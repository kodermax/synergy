<?
$MESS["CMDESKTOP_TDEF_ERR1"] = "Error al guardar la posición gadget para servidor.";
$MESS["CMDESKTOP_TDEF_ERR2"] = "Error al agregar gadget para servidor.";
$MESS["CMDESKTOP_TDEF_CONF"] = "La configuración de escritorio personal se aplicarán de forma predeterminada para todos los usuarios nuevos o no autorizados. ¿Desea continuar?";
$MESS["CMDESKTOP_TDEF_CONF_USER"] = "La configuración de escritorio se aplicarán a todos los nuevos perfiles de usuario por defecto. ¿Quieres continuar?";
$MESS["CMDESKTOP_TDEF_CONF_GROUP"] = "La configuración de escritorio se aplicarán a todos los nuevos grupos de trabajo por defecto. ¿Quieres continuar?";
$MESS["CMDESKTOP_TDEF_ADD"] = "Agregar Gadget";
$MESS["CMDESKTOP_TDEF_SET"] = "Guardar como configuración predeterminada";
$MESS["CMDESKTOP_TDEF_CLEAR"] = "Restablecer configuración actual";
$MESS["CMDESKTOP_TDEF_CANCEL"] = "Cancelar";
$MESS["CMDESKTOP_DESC_NAME"] = "Escritorio personal";
$MESS["CMDESKTOP_TDEF_DELETE"] = "Eliminar";
$MESS["CMDESKTOP_TDEF_SETTINGS"] = "Configuración";
$MESS["CMDESKTOP_DEMO_DATA_BLOCK_TITLE"] = "Borrar datos de demostración";
$MESS["CMDESKTOP_DEMO_DATA_BLOCK_DESC"] = "Para borrar datos de demo de su portal, utilice <b> Asistente de limpieza</b>. Puede ejecutar el asistente haciendo clic en \"Asistente para limpieza\" en la barra de herramientas del Panel de control en el modo Examinar o utilizando <a href='#LINK_TO_WIZARD#'>this link</a>.";
$MESS["CMDESKTOP_TDEF_CLEAR_CONF"] = "Los parámetros por defecto se aplicarán a su escritorio. Desea continuar?";
$MESS["CMDESKTOP_TDEF_HIDE"] = "Ocultar/Mostrar";
?>