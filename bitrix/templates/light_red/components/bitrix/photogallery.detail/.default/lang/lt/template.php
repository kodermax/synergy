<?
$MESS["NO_OF_COUNT"] = "#NO# iš #TOTAL#";
$MESS["P_EDIT"] = "Redaguoti";
$MESS["P_EDIT_TITLE"] = "Redaguoti nuotraukos savybes";
$MESS["P_DROP"] = "Pašalinti";
$MESS["P_DROP_TITLE"] = "Pašalinti nuotrauką";
$MESS["P_ORIGINAL"] = "Originalas";
$MESS["P_ORIGINAL_TITLE"] = "Nuotraukos originalas";
$MESS["P_SLIDE_SHOW"] = "Pristatymas";
$MESS["P_SLIDE_SHOW_TITLE"] = "Pradėti pristatymą nuo šios nuotraukos";
$MESS["P_DROP_CONFIM"] = "Ar tikrai norite pašalinti šią nuotrauką negrįžtamai?";
$MESS["P_PREV"] = "grįžti";
$MESS["P_GO_TO_PREV"] = "Ankstesnė nuotrauka";
$MESS["P_GO_TO_NEXT"] = "Kita nuotrauka";
$MESS["P_NEXT"] = "kita";
$MESS["P_TAGS"] = "Žymės";
$MESS["P_UNKNOWN_ERROR"] = "Duomėnų išsaugojimo klaida";
?>