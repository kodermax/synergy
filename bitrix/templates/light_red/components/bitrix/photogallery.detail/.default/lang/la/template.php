<?
$MESS["NO_OF_COUNT"] = "#NO# of #TOTAL#";
$MESS["P_EDIT"] = "Editar";
$MESS["P_EDIT_TITLE"] = "Editar las propiedades de la imagen";
$MESS["P_DROP"] = "Eliminar";
$MESS["P_DROP_TITLE"] = "Eliminar imagen";
$MESS["P_ORIGINAL"] = "Original";
$MESS["P_ORIGINAL_TITLE"] = "Imagen original";
$MESS["P_SLIDE_SHOW"] = "Mostrar diapositivas";
$MESS["P_SLIDE_SHOW_TITLE"] = "Iniciar la secuencia de diapositivas desde la imagen";
$MESS["P_DROP_CONFIM"] = "Está seguro de que desea eliminar la foto de forma irreversible?";
$MESS["P_PREV"] = "atrás";
$MESS["P_GO_TO_PREV"] = "Foto anterior";
$MESS["P_GO_TO_NEXT"] = "Siguiente foto";
$MESS["P_NEXT"] = "siguiente";
$MESS["P_TAGS"] = "Etiquetas";
$MESS["P_UNKNOWN_ERROR"] = "Error al guardar datos";
?>