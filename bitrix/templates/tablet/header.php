<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init('jquery');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-ui-1.11.4.custom/jquery-ui.min.js", true);
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/jquery-ui-1.11.4.custom/jquery-ui.min.css", true);

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.maskedinput.min.js", true);

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jscrollpane/jquery.jscrollpane.min.js", true);
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/js/jscrollpane/jquery.jscrollpane.css", true);

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/scripts.js", true);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?

$APPLICATION->ShowHead();
?><title><?$APPLICATION->ShowTitle()?></title>
</head>

<body class="template-bitrix24">

<?$APPLICATION->ShowPanel();?>
<table class="bx-layout-table">
	<tr>
		<td class="bx-layout-cont">
			<table class="bx-layout-inner-table">
				<tr class="bx-layout-inner-top-row">

					<td class="bx-layout-inner-center" id="content-table">

						<table class="bx-layout-inner-inner-table">
							<tr>
								<td class="bx-layout-inner-inner-cont">
									<div id="workarea">
										<div id="workarea-content">