function isValidEmail(email, strict)
{
	if ( !strict ) email = email.replace(/^\s+|\s+$/g, '');
	return (/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i).test(email);
}

function numericParseInt(string){
	var thisValue = parseInt(string);
	if(isNaN(thisValue)){
		var newValue = "";
	}else{
		var newValue = thisValue.toString();
		if(newValue.length >= 15){
            newValue = newValue.substr(0,15);
		}
	}
	return newValue;
}

function callInfinity(callTo){
	var dataCall = {};

	dataCall["callTo"] = callTo;

	var urlPhone = "/bitrix/templates/bitrix24/ajax/phone.php";

	$.ajax({
		type:'POST',
		url:urlPhone,
		success:function(data){
			if(data.response == false){
				alert('Вы не можете позвонить. Нет внутреннего номера.');
			}
		},
		data:dataCall,
		dataType:'json'
	});
}

$("document").ready(function(){

	$(document).on("keyup","input[name*='PHONE']",function(){
		$(this).val(numericParseInt($(this).val()));
	});

});