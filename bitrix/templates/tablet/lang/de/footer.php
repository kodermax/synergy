<?
$MESS["BITRIX24_COPYRIGHT1"] = "Spaß an der Arbeit mit";
$MESS["BITRIX24_COPYRIGHT2"] = "&copy; 2001-#CURRENT_YEAR# Bitrix, Inc.";
$MESS["BITRIX24_REVIEW"] = "Geben Sie Ihr Feedback";
$MESS["BITRIX24_URL"] = "http://www.bitrix24.de";
$MESS["BITRIX24_FEEDBACK_URL"] = "http://www.bitrix.de/bitrix24de/";
$MESS["BITRIX24_MENU_POPUP"] = "Menüpunkte können angepasst werden";
$MESS["BITRIX24_SSL_URL"] = "https://www.bitrix24.de/support/";
$MESS["BX24_SITE_TITLE"] = "Bitrix24.de öffnen";
$MESS["BX24_SITE_BUTTON"] = "Senden";
$MESS["BX24_CLOSE_BUTTON"] = "Schließen";
$MESS["BX24_LOADING"] = "Wird geladen";
$MESS["BX24_SITE_PASS_INPUT"] = "Bestätigen Sie bitte Ihre Passwort für #CPNAME#";
$MESS["BITRIX24_MENU_HELP"] = "Hilfe";
$MESS["BITRIX24_PARTNER_CONNECT"] = "Partner kontaktieren";
$MESS["BX24_SITE_PARTNER"] = "Persönlicher Support";
$MESS["BITRIX24_LANG_INTERFACE"] = "Sprache";
$MESS["BITRIX24_LANG_RU"] = "Russisch";
$MESS["BITRIX24_LANG_EN"] = "English";
$MESS["BITRIX24_LANG_DE"] = "Deutsch";
$MESS["BITRIX24_LANG_UA"] = "Ukrainisch";
$MESS["BITRIX24_LANG_LA"] = "Spanisch";
?>