<?
$MESS["B_B_MS_LINK"] = "Nuoroda";
$MESS["BPC_MES_EDIT"] = "Redaguoti";
$MESS["BPC_MES_SHOW"] = "Rodyti";
$MESS["BPC_MES_HIDE"] = "Slėpti";
$MESS["BPC_MES_DELETE_POST_CONFIRM"] = "Ar tikrai norite pašalinti šį komentarą?";
$MESS["BPC_MES_DELETE"] = "Pašalinti";
$MESS["B_B_MS_ADD_COMMENT"] = "Pridėti komentarą";
$MESS["B_B_MS_CAPTCHA_SYM"] = "CAPTCHA kodas";
$MESS["B_B_MS_NAME"] = "Vardas";
$MESS["BLOG_MONTH_01"] = "Sausis";
$MESS["BLOG_MONTH_02"] = "Vasaris";
$MESS["BLOG_MONTH_03"] = "Kovas ";
$MESS["BLOG_MONTH_04"] = "Balandis";
$MESS["BLOG_MONTH_05"] = "Gegužė";
$MESS["BLOG_MONTH_06"] = "Birželis";
$MESS["BLOG_MONTH_07"] = "Liepa";
$MESS["BLOG_MONTH_08"] = "Rugpjūtis";
$MESS["BLOG_MONTH_09"] = "Rugsėjis";
$MESS["BLOG_MONTH_10"] = "Spalis";
$MESS["BLOG_MONTH_11"] = "Lapkritis";
$MESS["BLOG_MONTH_12"] = "Gruodis";
$MESS["BLOG_C_VIEW_ALL"] = "Peržiūrėti visus komentarus";
$MESS["BLOG_C_HIDE"] = "Slėpti komentarus";
$MESS["BLOG_C_TEXT_TITLE"] = "Siųsti pranešimą...";
$MESS["BLOG_C_BUTTON_ALL"] = "Visi komentarai (#COMMENTS#)";
$MESS["BLOG_C_BUTTON_OLD"] = "Ankstesni komentarai (#COMMENTS#)";
$MESS["BLOG_COMMENT_MOBILE_FORMAT_DATE"] = "d F, g:i a";
$MESS["BLOG_COMMENT_MOBILE_FORMAT_DATE_YEAR"] = "d F Y, g:i a";
$MESS["BLOG_COMMENT_MOBILE_FORMAT_TIME"] = "g:i a";
?>