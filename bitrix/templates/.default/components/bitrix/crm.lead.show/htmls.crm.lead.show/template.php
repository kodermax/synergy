<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$arTabs = array();
$arTabs[] = array(
	'id' => 'tab_1',
	'name' => GetMessage('CRM_TAB_1'),
	'title' => GetMessage('CRM_TAB_1_TITLE'),
	'icon' => '',
	'fields'=> $arResult['FIELDS']['tab_1']
);
if ($arResult['BIZPROC'])
	$arTabs[] = array(
		'id' => 'tab_bizproc',
		'name' => GetMessage('CRM_TAB_7'),
		'title' => GetMessage('CRM_TAB_7_TITLE'),
		'icon' => '',
		'fields' => $arResult['FIELDS']['tab_bizproc']
	);

if ($arResult['ELEMENT']['STATUS_ID'] == 'CONVERTED'):
	if (!empty($arResult['FIELDS']['tab_contact']))
		$arTabs[] = array(
			'id' => 'tab_contact',
			'name' => GetMessage('CRM_TAB_2')." ($arResult[CONTACT_COUNT])",
			'title' => GetMessage('CRM_TAB_2_TITLE'),
			'icon' => '',
			'fields'=> $arResult['FIELDS']['tab_contact']
		);
	if (!empty($arResult['FIELDS']['tab_company']))
		$arTabs[] = array(
			'id' => 'tab_company',
			'name' => GetMessage('CRM_TAB_3')." ($arResult[COMPANY_COUNT])",
			'title' => GetMessage('CRM_TAB_3_TITLE'),
			'icon' => '',
			'fields'=> $arResult['FIELDS']['tab_company']
		);
	if (!empty($arResult['FIELDS']['tab_deal']))
		$arTabs[] = array(
			'id' => 'tab_deal',
			'name' => GetMessage('CRM_TAB_4')." ($arResult[DEAL_COUNT])",
			'title' => GetMessage('CRM_TAB_4_TITLE'),
			'icon' => '',
			'fields'=> $arResult['FIELDS']['tab_deal']
		);
endif;
$arTabs[] = array(
	'id' => 'tab_event',
	'name' => GetMessage('CRM_TAB_5')." ($arResult[EVENT_COUNT])",
	'title' => GetMessage('CRM_TAB_5_TITLE'),
	'icon' => '',
	'fields' => $arResult['FIELDS']['tab_event']
);
if (!empty($arResult['FIELDS']['tab_activity']))
{
	$arTabs[] = array(
		'id' => 'tab_activity',
		'name' => GetMessage('CRM_TAB_6')." (".($arResult['ACTIVITY_COUNT']+$arResult['CALENDAR_COUNT']).")",
		'title' => GetMessage('CRM_TAB_6_TITLE'),
		'icon' => '',
		'fields' => $arResult['FIELDS']['tab_activity']
	);
}
CCrmGridOptions::SetTabNames($arResult['FORM_ID'], $arTabs);

$APPLICATION->IncludeComponent(
	'bitrix:main.interface.form',
	'',
	array(
		'FORM_ID' => $arResult['FORM_ID'],
		'TABS' => $arTabs,
		'BUTTONS' => array(
			'standard_buttons' =>  false
		),
		'DATA' => $arResult['ELEMENT'],
		'SHOW_SETTINGS' => 'Y',
		'THEME_GRID_ID' => $arResult['GRID_ID'],
		'SHOW_FORM_TAG' => 'N'
	),
	$component, array('HIDE_ICONS' => 'Y')
);
?>