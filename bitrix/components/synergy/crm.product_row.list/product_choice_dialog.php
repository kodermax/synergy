<?
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

/** @global $APPLICATION CMain */
global $USER, $APPLICATION;

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

if (!$USER->IsAuthorized() || !check_bitrix_sessid() || $_SERVER['REQUEST_METHOD'] != 'GET')
{
	ShowError(GetMessage('CRM_LOAD_DIALOG_ERROR'));
	return;
}

$jsEventsManagerId = isset($_GET['JS_EVENTS_MANAGER_ID'])? strval($_GET['JS_EVENTS_MANAGER_ID']) : '';
if ($jsEventsManagerId === '')
	return;

$APPLICATION->ShowAjaxHead();


$arParamsFilter = array('JS_EVENTS_MANAGER_ID' => $jsEventsManagerId);


if($_GET['PROPERTY_FILIAL']){
	$arParamsFilter['PROPERTY_FILIAL'] = intval($_GET['PROPERTY_FILIAL']);
}

if($_GET['PROPERTY_ACADEMIC_YEAR']){
	$arParamsFilter['PROPERTY_ACADEMIC_YEAR'] = intval($_GET['PROPERTY_ACADEMIC_YEAR']);
}

if($_GET['PROPERTY_SEMESTR']){
	$arParamsFilter['PROPERTY_SEMESTR'] = intval($_GET['PROPERTY_SEMESTR']);
}

if($_GET['PROPERTY_CODELEVEL']){
	$arParamsFilter['PROPERTY_CODELEVEL'] = intval($_GET['PROPERTY_CODELEVEL']);
}

if($_GET['PROPERTY_CODESUBLEVEL']){
	$arParamsFilter['PROPERTY_CODESUBLEVEL'] = intval($_GET['PROPERTY_CODESUBLEVEL']);
}

if($_GET['PROPERTY_UNIT']){
	$arParamsFilter['PROPERTY_UNIT'] = intval($_GET['PROPERTY_UNIT']);
}

if($_GET['PROPERTY_TRAININGPROGRAM']){
	$arParamsFilter['PROPERTY_TRAININGPROGRAM'] = intval($_GET['PROPERTY_TRAININGPROGRAM']);
}

if($_GET['PROPERTY_TYPESOFPROGRAMS']){
	$arParamsFilter['PROPERTY_TYPESOFPROGRAMS'] = intval($_GET['PROPERTY_TYPESOFPROGRAMS']);
}

if($_GET['PROPERTY_SPECIALITY']){
	$arParamsFilter['PROPERTY_SPECIALITY'] = intval($_GET['PROPERTY_SPECIALITY']);
}

if($_GET['PROPERTY_SPECIALIZATION']){
	$arParamsFilter['PROPERTY_SPECIALIZATION'] = intval($_GET['PROPERTY_SPECIALIZATION']);
}

if($_GET['PROPERTY_CODEFORMOFLEARNING']){
	$arParamsFilter['PROPERTY_CODEFORMOFLEARNING'] = intval($_GET['PROPERTY_CODEFORMOFLEARNING']);
}

if($_GET['PROPERTY_DURATION']){
	$arParamsFilter['PROPERTY_DURATION'] = intval($_GET['PROPERTY_DURATION']);
}


$APPLICATION->IncludeComponent(
	'synergy:crm.product.search.dialog',
	'',
	$arParamsFilter,
	false
);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>