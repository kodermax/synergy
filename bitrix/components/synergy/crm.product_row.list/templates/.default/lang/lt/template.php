<?
$MESS["CRM_PRODUCT_ROW_COL_TTL_NAME"] = "Produktas";
$MESS["CRM_PRODUCT_ROW_COL_TTL_QUANTITY"] = "Kiekis";
$MESS["CRM_PRODUCT_ROW_COL_TTL_PRICE"] = "Kaina";
$MESS["CRM_PRODUCT_SUM_TOTAL"] = "Bendra suma";
$MESS["CRM_FF_OK"] = "Pasirinkti";
$MESS["CRM_FF_CANCEL"] = "Atšaukti";
$MESS["CRM_FF_CLOSE"] = "Uždaryti";
$MESS["CRM_FF_NO_RESULT"] = "Deja, pagal jūsų paieškos užklausą rezultatų nerasta.";
$MESS["CRM_FF_CHOISE"] = "Pridėti produktą";
$MESS["CRM_FF_CHANGE"] = "Redaguoti";
$MESS["CRM_FF_LAST"] = "Paskutinis";
$MESS["CRM_FF_SEARCH"] = "Ieškoti";
$MESS["CRM_PRODUCT_ROW_DELETION_CONFIRM"] = "Ar tikrai norite pašalinti šį produktą?";
$MESS["CRM_PRODUCT_ROW_DELIVERY"] = "Pristatymas";
$MESS["CRM_PRODUCT_ROW_DISCOUNT"] = "Nuolaida";
$MESS["CRM_EDIT_BTN_TTL"] = "Spustelėkite, norėdami redaguoti";
$MESS["CRM_DEL_BTN_TTL"] = "Spustelėkite, norėdami pašalinti";
$MESS["CRM_PERMISSION_DENIED_ERROR"] = "Negalima įeiti";
$MESS["CRM_INVALID_REQUEST_ERROR"] = "Užklausos duomenys yra neteisingi arba sugadinti. Prašome bandyti dar kartą.";
$MESS["CRM_FF_CHOISE_2"] = "Pasirinkti";
$MESS["CRM_FF_CHOISE_3"] = "Pasirinkti produktą";
$MESS["CRM_FF_ADD_CUSTOM"] = "Pridėti";
$MESS["CRM_FF_ADD_CUSTOM_1"] = "Pridėti naują produktą";
$MESS["CRM_ADD_CUSTOM_PRODUCT_DLG_TTL"] = "Naujas produktas";
$MESS["CRM_CUSTOM_PRODUCT_NAME_NOT_ASSIGNED_ERROR"] = "Nenurodytas užsakomo produkto pavadinimas.";
$MESS["CRM_PRODUCT_ROW_COL_TTL_DISCOUNT_RATE"] = "Nuolaida";
$MESS["CRM_PRODUCT_ROW_COL_TTL_DISCOUNT"] = "Nuolaidos suma";
$MESS["CRM_PRODUCT_ROW_COL_TTL_SUM"] = "Iš viso";
$MESS["CRM_PRODUCT_ROW_COL_TTL_MEASURE"] = "Matavimo vienetas";
$MESS["CRM_PRODUCT_ROW_COL_TTL_TAX"] = "Mokestis";
$MESS["CRM_PRODUCT_TOTAL_BEFORE_DISCOUNT"] = "Iš viso be nuolaidų ir mokesčių";
$MESS["CRM_PRODUCT_TOTAL_DISCOUNT"] = "Nuolaidos suma";
$MESS["CRM_PRODUCT_TOTAL_BEFORE_TAX"] = "Iš viso prieš apmokestinimą";
$MESS["CRM_PRODUCT_TOTAL_TAX"] = "Mokesčių iš viso";
$MESS["CRM_PRODUCT_SHOW_DISCOUNT"] = "Rodyti nuolaidą";
$MESS["CRM_PRODUCT_SHOW_TAX"] = "Rodyti mokestį";
$MESS["CRM_PRODUCT_ROW_COL_TTL_TAX_INCLUDED"] = "Įtraukta
";
$MESS["CRM_PRODUCT_ROW_COL_TTL_TAX_SUM"] = "Mokesčių iš viso";
$MESS["CRM_PRODUCT_ROW_BTN_EDIT"] = "Redaguoti";
$MESS["CRM_PRODUCT_ROW_BTN_EDIT_F"] = "Išsaugoti";
$MESS["CRM_PRODUCT_ROW_SAVING"] = "Išsaugoma...";
$MESS["CRM_PRODUCT_ROW_ADD_ROW"] = "Pridėti eilutę";
$MESS["CRM_PRODUCT_TAX_INCLUDING"] = "įtraukta";
$MESS["CRM_PRODUCT_ROW_OPEN_PRODUCT_CARD"] = "Atidaryti produkto savybes";
$MESS["CRM_PRODUCT_ROW_CREATE_PRODUCT"] = "Pridėti prie katalogo";
?>