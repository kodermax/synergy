<?
$MESS["CRM_COLUMN_STAGE_ID"] = "Etapa de la negociación";
$MESS["CRM_COLUMN_OPPORTUNITY"] = "Monto";
$MESS["CRM_COLUMN_PROBABILITY"] = "Probabilidad";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Modificado el";
$MESS["CRM_COLUMN_COMPANY_TITLE"] = "Compañía";
$MESS["CRM_COLUMN_CONTACT_FULL_NAME"] = "Contacto";
$MESS["CRM_OPER_SHOW"] = "Ver";
$MESS["CRM_OPER_EDIT"] = "Editar";
$MESS["CRM_COLUMN_PRODUCTS"] = "Producto";
?>