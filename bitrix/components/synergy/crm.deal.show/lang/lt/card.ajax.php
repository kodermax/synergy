<?
$MESS["CRM_COLUMN_STAGE_ID"] = "Sandorio stadija";
$MESS["CRM_COLUMN_PRODUCTS"] = "Produktas";
$MESS["CRM_COLUMN_OPPORTUNITY"] = "Suma";
$MESS["CRM_COLUMN_PROBABILITY"] = "Tikimybė";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Paakeista";
$MESS["CRM_COLUMN_COMPANY_TITLE"] = "Įmonė";
$MESS["CRM_COLUMN_CONTACT_FULL_NAME"] = "Kontaktas";
$MESS["CRM_OPER_SHOW"] = "Peržiūrėti";
$MESS["CRM_OPER_EDIT"] = "Redaguoti";
$MESS["CRM_COLUMN_PRODUCT_ID"] = "Produktas";
?>