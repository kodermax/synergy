<?
$MESS["CRM_TAB_1"] = "Negociación";
$MESS["CRM_TAB_1_TITLE"] = "Propiedades de la negociación";
$MESS["CRM_TAB_2"] = "Eventos";
$MESS["CRM_TAB_2_TITLE"] = "Eventos de la negociación";
$MESS["CRM_TAB_3"] = "Business Process";
$MESS["CRM_TAB_3_TITLE"] = "Business Process de la negociación";
$MESS["CRM_DEAL_EDIT_TITLE"] = "Negociación ##ID# &mdash; #TITLE#";
$MESS["CRM_DEAL_CREATE_TITLE"] = "Nueva negociación";
?>