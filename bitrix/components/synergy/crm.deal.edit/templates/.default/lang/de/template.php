<?
$MESS["CRM_TAB_1"] = "Auftrag";
$MESS["CRM_TAB_1_TITLE"] = "Auftragseigenschaften";
$MESS["CRM_TAB_2"] = "Protokoll";
$MESS["CRM_TAB_2_TITLE"] = "Protokoll des Auftrags";
$MESS["CRM_TAB_3"] = "Geschäftsprozesse";
$MESS["CRM_TAB_3_TITLE"] = "Geschäftsprozesse mit dem Auftrag";
$MESS["CRM_DEAL_EDIT_TITLE"] = "Auftrag Nr.#ID# &mdash; #TITLE#";
$MESS["CRM_DEAL_CREATE_TITLE"] = "Neuer Auftrag";
?>