<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

class CCrmProductSearchDialogComponent extends \CBitrixComponent
{
	public function executeComponent()
	{
		if (!CModule::IncludeModule('crm'))
		{
			ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
			return;
		}

		if($this->arParams['PROPERTY_FILIAL']){
			$this->arResult['PROPERTY_FILIAL'] = $this->arParams['PROPERTY_FILIAL'];
		}

		if($this->arParams['PROPERTY_ACADEMIC_YEAR']){
			$this->arResult['PROPERTY_ACADEMIC_YEAR'] = $this->arParams['PROPERTY_ACADEMIC_YEAR'];
		}

		if($this->arParams['PROPERTY_SEMESTR']){
			$this->arResult['PROPERTY_SEMESTR'] = $this->arParams['PROPERTY_SEMESTR'];
		}

		if($this->arParams['PROPERTY_CODELEVEL']){
			$this->arResult['PROPERTY_CODELEVEL'] = $this->arParams['PROPERTY_CODELEVEL'];
		}

		if($this->arParams['PROPERTY_CODESUBLEVEL']){
			$this->arResult['PROPERTY_CODESUBLEVEL'] = $this->arParams['PROPERTY_CODESUBLEVEL'];
		}

		if($this->arParams['PROPERTY_UNIT']){
			$this->arResult['PROPERTY_UNIT'] = $this->arParams['PROPERTY_UNIT'];
		}

		if($this->arParams['PROPERTY_TRAININGPROGRAM']){
			$this->arResult['PROPERTY_TRAININGPROGRAM'] = $this->arParams['PROPERTY_TRAININGPROGRAM'];
		}

		if($this->arParams['PROPERTY_TYPESOFPROGRAMS']){
			$this->arResult['PROPERTY_TYPESOFPROGRAMS'] = $this->arParams['PROPERTY_TYPESOFPROGRAMS'];
		}

		if($this->arParams['PROPERTY_SPECIALITY']){
			$this->arResult['PROPERTY_SPECIALITY'] = $this->arParams['PROPERTY_SPECIALITY'];
		}

		if($this->arParams['PROPERTY_SPECIALIZATION']){
			$this->arResult['PROPERTY_SPECIALIZATION'] = $this->arParams['PROPERTY_SPECIALIZATION'];
		}

		if($this->arParams['PROPERTY_CODEFORMOFLEARNING']){
			$this->arResult['PROPERTY_CODEFORMOFLEARNING'] = $this->arParams['PROPERTY_CODEFORMOFLEARNING'];
		}

		if($this->arParams['PROPERTY_DURATION']){
			$this->arResult['PROPERTY_DURATION'] = $this->arParams['PROPERTY_DURATION'];
		}




		$catalogID = isset($this->arParams['CATALOG_ID']) ? intval($this->arParams['CATALOG_ID']) : 0;
		if ($catalogID <= 0)
			$catalogID = CCrmCatalog::EnsureDefaultExists();
		$this->arResult['CATALOG_ID'] = $catalogID;

		$this->arResult['JS_EVENTS_MANAGER_ID'] = isset($this->arParams['JS_EVENTS_MANAGER_ID'])? $this->arParams['JS_EVENTS_MANAGER_ID'] : '';
		if (!is_string($this->arResult['JS_EVENTS_MANAGER_ID']) || $this->arResult['JS_EVENTS_MANAGER_ID'] === '')
			return;

		$this->includeComponentTemplate();
	}
}
