<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arResult */
/** @var \CCrmProductSearchDialogComponent $component */

global $APPLICATION;


$arParams = array(
	'IBLOCK_ID' => $arResult['CATALOG_ID'],
	'CHECK_PERMISSIONS' => 'N'
);


if($arResult["PROPERTY_FILIAL"]){
	$arParams["PROPERTY_FILIAL"] = $arResult["PROPERTY_FILIAL"];
}

if($arResult["PROPERTY_ACADEMIC_YEAR"]){
	$arParams["PROPERTY_ACADEMIC_YEAR"] = $arResult["PROPERTY_ACADEMIC_YEAR"];
}

if($arResult["PROPERTY_SEMESTR"]){
	$arParams["PROPERTY_SEMESTR"] = $arResult["PROPERTY_SEMESTR"];
}

if($arResult["PROPERTY_CODELEVEL"]){
	$arParams["PROPERTY_CODELEVEL"] = $arResult["PROPERTY_CODELEVEL"];
}

if($arResult["PROPERTY_CODESUBLEVEL"]){
	$arParams["PROPERTY_CODESUBLEVEL"] = $arResult["PROPERTY_CODESUBLEVEL"];
}

if($arResult["PROPERTY_UNIT"]){
	$arParams["PROPERTY_UNIT"] = $arResult["PROPERTY_UNIT"];
}

if($arResult["PROPERTY_TRAININGPROGRAM"]){
	$arParams["PROPERTY_TRAININGPROGRAM"] = $arResult["PROPERTY_TRAININGPROGRAM"];
}

if($arResult["PROPERTY_TYPESOFPROGRAMS"]){
	$arParams["PROPERTY_TYPESOFPROGRAMS"] = $arResult["PROPERTY_TYPESOFPROGRAMS"];
}

if($arResult["PROPERTY_SPECIALITY"]){
	$arParams["PROPERTY_SPECIALITY"] = $arResult["PROPERTY_SPECIALITY"];
}

if($arResult["PROPERTY_SPECIALIZATION"]){
	$arParams["PROPERTY_SPECIALIZATION"] = $arResult["PROPERTY_SPECIALIZATION"];
}

if($arResult["PROPERTY_CODEFORMOFLEARNING"]){
	$arParams["PROPERTY_CODEFORMOFLEARNING"] = $arResult["PROPERTY_CODEFORMOFLEARNING"];
}

if($arResult["PROPERTY_DURATION"]){
	$arParams["PROPERTY_DURATION"] = $arResult["PROPERTY_DURATION"];
}

$APPLICATION->IncludeComponent(
	'synergy:catalog.product.search',
	'',
	$arParams,
	$component,
	array('HIDE_ICONS'=>true)
);
