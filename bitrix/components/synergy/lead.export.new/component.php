<?
if($_POST){

	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");


	$csvFile = new CCSVData();
	$fields_type = 'R';
	$delimiter = ";";
	$csvFile->SetFieldsType($fields_type);
	$csvFile->SetDelimiter($delimiter);


    unlink("astra.csv");


	CModule::IncludeModule("crm");

	$arSort = Array("ID" => "DESC");

	$arFilter = Array();

	if($_POST["dateFrom"]){
		$arFilter[">=DATE_CREATE"] = $_POST["dateFrom"];
	}

	if($_POST["dateTo"]){
		$arFilter["<=DATE_CREATE"] = $_POST["dateTo"];
	}

	$arSelect = Array("ID", "TITLE", "DATE_CREATE", "ASSIGNED_BY_ID", "ASSIGNED_BY_LOGIN", "STATUS_ID");

    $arHeaders = Array("ID", "Лид", "Дата создания", "Статус", "Имя", "Фамилия", "Подразделение");

    foreach($arHeaders as $key => $arHead){
		//$arHeaders[$key] = iconv("UTF-8", "windows-1251", $arHead);
    }

    o($arHeaders);

	$csvFile->SaveFile("astra.csv", $arSelect);

	$objLead = CCrmLead::GetList($arSort, $arFilter, $arSelect, 20);

	while($arrLead = $objLead->Fetch()){

	    foreach($arrLead as $key => $arLead){
			$arrLead[$key] = iconv("UTF-8", "windows-1251", $arLead);
	    }

		$objUserParams = $USER->GetById($arrLead["ASSIGNED_BY_ID"]);

		if($userParams = $objUserParams->Fetch()){
			$objDepartments = CIBlockSection::GetNavChain(1, $userParams["UF_DEPARTMENT"][0]);
			$arAssigned[$arLead["ID"]] = $userParams;
		}

		$leadStatus = $arrLead["STATUS_ID"];

		$strSql = "SELECT CS.ID, CS.NAME
            FROM
                b_crm_status CS
            WHERE STATUS_ID = '".$leadStatus."'	and ENTITY_ID = 'STATUS'";

		$res = $DB->Query($strSql);

		if($status = $res->Fetch()){
			$statusName = $status["NAME"];
		}


		$department = "";

		while($arDepartment = $objDepartments->ExtractFields()):
			$department .= $arDepartment["NAME"];
			if($arDepartment["ID"] != $userParams["UF_DEPARTMENT"][0]):
				$department .= " / ";
			endif;
		endwhile;

		$string = Array($arrLead["ID"], $arrLead["TITLE"], $arrLead["DATE_CREATE"], $statusName, $arrLead["ASSIGNED_BY_NAME"], $arrLead["ASSIGNED_BY_LAST_NAME"], $department);

		//$csvFile->SaveFile("astra.csv", $string);

	}

}

$this->IncludeComponentTemplate()?>