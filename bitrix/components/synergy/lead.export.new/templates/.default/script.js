$(document).ready(function(){
	$("form input.datepicker").datepicker({		dateFormat: "dd.mm.yy",
		firstDay: 1,
		changeYear:1,
		monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
		dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ]	});
});