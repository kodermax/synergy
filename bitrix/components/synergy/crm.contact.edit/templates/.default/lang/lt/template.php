<?
$MESS["CRM_TAB_1"] = "Kontaktas";
$MESS["CRM_TAB_1_TITLE"] = "Kontakto savybės";
$MESS["CRM_TAB_2"] = "Žurnalas";
$MESS["CRM_TAB_2_TITLE"] = "Kontakto žurnalas";
$MESS["CRM_IMPORT_SNS"] = "Taip pat jūs galite naudoti \"vCard/ norėdami pridėti kontaktus. <br> Atidarykite \"Kontaktai \"<b>MS Outlook</ b> ir pasirinkite reikiamus kontaktus; po to pasirinkite \"<b>Veiksmai%ARROW% Siųsti kaip vizitinę kortelę </b>\" viršutinėje juostoje ir siųsti <b>%EMAIL%</b>";
$MESS["CRM_TAB_3"] = "Verslo procesas";
$MESS["CRM_TAB_3_TITLE"] = "Sandorio verslo procesai";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Rasti sutapimai";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Galimi klonai";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignoruoti ir išsaugoti";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Atšaukti";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_FULL_NAME_SUMMARY_TITLE"] = "pagal pilną pavadinimą";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "pagal telefoną";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "pagal el.paštą";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "rasta";
$MESS["CRM_CONTACT_EDIT_TITLE"] = "Kontaktas ##ID# &mdash; #NAME#";
$MESS["CRM_CONTACT_CREATE_TITLE"] = "Naujas kontaktas";
?>