<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "CRM modulis nėra įdiegtas.";
$MESS["CRM_PERMISSION_DENIED"] = "Negalima įeiti";
$MESS["UNKNOWN_ERROR"] = "Nežynoma klaida.";
$MESS["CRM_DELETE_ERROR"] = "Įvyko klaida šalinant objektą. ";
$MESS["CRM_FIELD_FIND"] = "Ieškoti";
$MESS["CRM_FIELD_ID"] = "ID";
$MESS["CRM_FIELD_OPENED"] = "Prieinama visiems";
$MESS["CRM_FIELD_NAME"] = "Vardas";
$MESS["CRM_FIELD_LAST_NAME"] = "Pavardė";
$MESS["CRM_FIELD_SECOND_NAME"] = "Tėvo vardas";
$MESS["CRM_FIELD_PHOTO"] = "Nuotrauka";
$MESS["CRM_FIELD_BIRTHDATE"] = "Gimimo data";
$MESS["CRM_FIELD_PHONE"] = "Telefonas";
$MESS["CRM_FIELD_EMAIL"] = "El.paštas";
$MESS["CRM_FIELD_WEB"] = "Svetainė";
$MESS["CRM_FIELD_MESSENGER"] = "Messenger";
$MESS["CRM_FIELD_POST"] = "Pareigos";
$MESS["CRM_FIELD_ADDRESS"] = "Adresas";
$MESS["CRM_FIELD_COMMENTS"] = "Komentaras";
$MESS["CRM_FIELD_COMPANY_ID"] = "Įmonė";
$MESS["CRM_FIELD_COMPANY_ID_START_TEXT"] = "(Įvesti įmonės pavadinimą)";
$MESS["CRM_FIELD_SOURCE_ID"] = "Šaltinis";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "Aprašymas";
$MESS["CRM_FIELD_ASSIGNED_BY_ID"] = "Atsakingas";
$MESS["CRM_FIELD_EXPORT"] = "Naudoti kontaktų eksportui";
$MESS["CRM_FIELD_TYPE_ID"] = "Kontakto tipas";
$MESS["CRM_FIELD_CONTACT_EVENT"] = "Kontaktų žurnalas";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Kontakto informacija";
$MESS["CRM_SECTION_ADDITIONAL"] = "Daugiau";
$MESS["CRM_CONTACT_NAV_TITLE_EDIT"] = "Kontaktas: #NAME#";
$MESS["CRM_CONTACT_NAV_TITLE_ADD"] = "Pridėti kontaktą";
$MESS["CRM_CONTACT_NAV_TITLE_LIST"] = "Kontaktai";
$MESS["CRM_ALL"] = "Iš viso";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Verslo procesų modulis nėra įdiegtas.";
$MESS["CRM_FIELD_BP_PARAMETERS"] = "Verslo proceso parametrai";
$MESS["CRM_FIELD_BP_TEMPLATE_DESC"] = "Aprašymas";
$MESS["CRM_FIELD_BP_STATE_MODIFIED"] = "Dabartinio statuso data";
$MESS["CRM_FIELD_BP_STATE_NAME"] = "Dabartinis statusas";
$MESS["CRM_FIELD_BP_EVENTS"] = "Komanda";
$MESS["CRM_FIELD_BP_EMPTY_EVENT"] = "nepaleisti";
$MESS["CRM_FIELD_BP_TEXT"] = "tekstas";
$MESS["CRM_FIELD_LEAD_ID"] = "Lead'as";
$MESS["CRM_FIELD_ORIGIN_ID"] = "Interneto parduotuvės naudotojo ID";
$MESS["CRM_FIELD_ORIGINATOR_ID"] = "Interneto parduotuvė";
$MESS["CRM_FIELD_OPENED_TITLE"] = "Kontaktas gali būti matomas visiems.";
$MESS["CRM_SECTION_CONTACT_INFO2"] = "Apie kontaktą";
$MESS["CRM_CONTACT_EDIT_FIELD_BIRTHDATE"] = "Gimimo data";
?>