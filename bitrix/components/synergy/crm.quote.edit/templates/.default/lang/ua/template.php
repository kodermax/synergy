<?
$MESS["CRM_TAB_1"] = "Пропозиція";
$MESS["CRM_TAB_1_TITLE"] = "Властивості пропозиції";
$MESS["CRM_QUOTE_SHOW_TITLE"] = "Пропозиція № #QUOTE_NUMBER# від #BEGINDATE#";
$MESS["CRM_QUOTE_SHOW_NEW_TITLE"] = "Нова пропозиція";
$MESS["CRM_QUOTE_WEBDAV_FILE_LOADING"] = "Завантаження";
$MESS["CRM_QUOTE_WEBDAV_FILE_ALREADY_EXISTS"] = "Файл з такою назвою вже існує. Ви можете вибрати поточну папку, в цьому випадку стара версія файлу буде збережена в історії документа.";
$MESS["CRM_QUOTE_WEBDAV_FILE_ACCESS_DENIED"] = "Доступ заборонений.";
$MESS["CRM_QUOTE_WEBDAV_ATTACH_FILE"] = "Прикріпити файл";
$MESS["CRM_QUOTE_WEBDAV_TITLE"] = "Файли";
$MESS["CRM_QUOTE_WEBDAV_DRAG_FILE"] = "Перетягніть один або декілька файлів в цю область";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FILE"] = "або виберіть файл на комп'ютері";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FROM_LIB"] = "Вибрати з бібліотеки";
$MESS["CRM_QUOTE_WEBDAV_LOAD_FILES"] = "Завантажити файли";
?>