<?
$MESS["CRM_TAB_1"] = "Cotización";
$MESS["CRM_TAB_1_TITLE"] = "Propiedades de cotización";
$MESS["CRM_QUOTE_SHOW_TITLE"] = "Cotización ##QUOTE_NUMBER# desde #BEGINDATE#";
$MESS["CRM_QUOTE_SHOW_NEW_TITLE"] = "Cotización nueva";
$MESS["CRM_QUOTE_WEBDAV_FILE_LOADING"] = "Cargando";
$MESS["CRM_QUOTE_WEBDAV_FILE_ALREADY_EXISTS"] = "Un archivo con este nombre ya existe. Puede seguir utilizando la carpeta actual, en cuyo caso la versión existente del documento se guardará en el historial.";
$MESS["CRM_QUOTE_WEBDAV_FILE_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_QUOTE_WEBDAV_ATTACH_FILE"] = "Adjuntar archivo";
$MESS["CRM_QUOTE_WEBDAV_TITLE"] = "Archivos";
$MESS["CRM_QUOTE_WEBDAV_DRAG_FILE"] = "Arrastrar y soltar uno o más archivos aqui";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FILE"] = "o seleccionar un archivo de tu computadora";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FROM_LIB"] = "Seleccionar desde la Librería";
$MESS["CRM_QUOTE_WEBDAV_LOAD_FILES"] = "Cargar archivos";
$MESS["CRM_QUOTE_DISK_ATTACH_FILE"] = "Adjuntar archivos";
$MESS["CRM_QUOTE_DISK_ATTACHED_FILES"] = "Adjuntos";
$MESS["CRM_QUOTE_DISK_SELECT_FILE"] = "Buscar Bitrix24";
$MESS["CRM_QUOTE_DISK_SELECT_FILE_LEGEND"] = "Abrir la ventana Bitrix24.Drive";
$MESS["CRM_QUOTE_DISK_UPLOAD_FILE"] = "Cargar archivo";
$MESS["CRM_QUOTE_DISK_UPLOAD_FILE_LEGEND"] = "Arrastre aquí archivos adjuntos";
?>