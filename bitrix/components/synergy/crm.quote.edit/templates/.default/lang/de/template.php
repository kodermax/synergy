<?
$MESS["CRM_TAB_1"] = "Angebot";
$MESS["CRM_TAB_1_TITLE"] = "Angebotseigenschaften";
$MESS["CRM_QUOTE_SHOW_TITLE"] = "Angebot Nr. #QUOTE_NUMBER# vom #BEGINDATE#";
$MESS["CRM_QUOTE_SHOW_NEW_TITLE"] = "Neues Angebot";
$MESS["CRM_QUOTE_WEBDAV_FILE_LOADING"] = "Wird geladen";
$MESS["CRM_QUOTE_WEBDAV_FILE_ALREADY_EXISTS"] = "Eine Datei mit diesem Namen existiert bereits. Sie können den aktuellen Ordner auswählen, in diesem Fall wird die alte Dateiversion in der Dokument-History gespeichert.";
$MESS["CRM_QUOTE_WEBDAV_FILE_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_QUOTE_WEBDAV_ATTACH_FILE"] = "Datei anhängen";
$MESS["CRM_QUOTE_WEBDAV_TITLE"] = "Dateien";
$MESS["CRM_QUOTE_WEBDAV_DRAG_FILE"] = "Verschieben Sie eine oder mehrere Dateien hierher per Drag-and-Drop";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FILE"] = "oder wählen Sie eine Datei von Ihrem Computer aus";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FROM_LIB"] = "Aus der Bibliothek auswählen";
$MESS["CRM_QUOTE_WEBDAV_LOAD_FILES"] = "Dateien hochladen";
$MESS["CRM_QUOTE_DISK_ATTACH_FILE"] = "Dateien anhängen";
$MESS["CRM_QUOTE_DISK_ATTACHED_FILES"] = "Anhänge";
$MESS["CRM_QUOTE_DISK_SELECT_FILE"] = "In Bitrix24 suchen";
$MESS["CRM_QUOTE_DISK_SELECT_FILE_LEGEND"] = "Bitrix24.Drive-Fenster öffnen";
$MESS["CRM_QUOTE_DISK_UPLOAD_FILE"] = "Datei hochladen";
$MESS["CRM_QUOTE_DISK_UPLOAD_FILE_LEGEND"] = "Anhänge hierher verschieben";
?>