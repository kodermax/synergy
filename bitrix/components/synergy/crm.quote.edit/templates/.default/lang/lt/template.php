<?
$MESS["CRM_TAB_1"] = "Citata";
$MESS["CRM_TAB_1_TITLE"] = "Citatos savybės";
$MESS["CRM_QUOTE_SHOW_TITLE"] = "Citata ##QUOTE_NUMBER# nuo #BEGINDATE#";
$MESS["CRM_QUOTE_SHOW_NEW_TITLE"] = "Nauja citata";
$MESS["CRM_QUOTE_WEBDAV_FILE_LOADING"] = "Įkeliama";
$MESS["CRM_QUOTE_WEBDAV_FILE_ALREADY_EXISTS"] = "Failas tokiu pavadinimu jau egzistuoja. Jūs galite naudoti esamą aplanką, tokiu atveju esama dokumento versija bus išsaugotą istorijoje.";
$MESS["CRM_QUOTE_WEBDAV_FILE_ACCESS_DENIED"] = "Negalima įeiti";
$MESS["CRM_QUOTE_WEBDAV_ATTACH_FILE"] = "Prisegti failą";
$MESS["CRM_QUOTE_WEBDAV_TITLE"] = "Failai";
$MESS["CRM_QUOTE_WEBDAV_DRAG_FILE"] = "Pertempti vieną arba daugiau failų čia";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FILE"] = "arba pasirinkti savo kompiuteryje";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FROM_LIB"] = "Pasirinkti iš bibiotekos";
$MESS["CRM_QUOTE_WEBDAV_LOAD_FILES"] = "Atnaujinti failus";
?>