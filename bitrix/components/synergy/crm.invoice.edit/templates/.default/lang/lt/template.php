<?
$MESS["CRM_TAB_1"] = "Sąskaita-faktūra";
$MESS["CRM_TAB_1_TITLE"] = "Sąskaitos-faktūros parametrai";
$MESS["CRM_INVOICE_SHOW_TITLE"] = "Sąskaita-faktūra ##ACCOUNT_NUMBER# &mdash; #ORDER_TOPIC#";
$MESS["CRM_INVOICE_SHOW_NEW_TITLE"] = "Nauja sąskaita-faktūra";
$MESS["CRM_INVOICE_SHOW_LEGEND"] = "Sąskaita-faktūra";
$MESS["CRM_INVOICE_PS_PROPS_TITLE"] = "Įmonės informacija";
$MESS["CRM_INVOICE_PS_PROPS_CONTENT"] = "Norėdami pradėti darbą su sąskaitomis, pateikite informaciją apie įmonę, kuri bus naudojama sąskaitose.";
$MESS["CRM_INVOICE_PS_PROPS_GOTO"] = "Pridėti įmonės informaciją";
?>