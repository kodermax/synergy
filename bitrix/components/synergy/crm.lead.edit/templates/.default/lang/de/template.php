<?
$MESS["CRM_TAB_1"] = "Lead";
$MESS["CRM_TAB_1_TITLE"] = "Leadeigenschaften";
$MESS["CRM_TAB_2"] = "Protokoll";
$MESS["CRM_TAB_2_TITLE"] = "Protokoll des Leads";
$MESS["CRM_TAB_3"] = "Geschäftsprozesse";
$MESS["CRM_TAB_3_TITLE"] = "Geschäftsprozesse mit dem Lead";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Übereinstimmungen gefunden";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Eventuelle Dubletten";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignorieren und speichern";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Abbrechen";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_FULL_NAME_SUMMARY_TITLE"] = "nach vollständigem Namen";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_COMPANY_TTL_SUMMARY_TITLE"] = "nach Unternehmensnamen";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "nach Telefon";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "nach E-Mail";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "gefunden";
$MESS["CRM_LEAD_EDIT_TITLE"] = "Lead Nr.#ID# &mdash; #TITLE#";
$MESS["CRM_LEAD_CREATE_TITLE"] = "Neuer Lead";
?>