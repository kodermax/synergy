<?
$MESS["CRM_TAB_1"] = "Prospecto";
$MESS["CRM_TAB_1_TITLE"] = "Propiedades del Prospecto";
$MESS["CRM_TAB_2"] = "Eventos";
$MESS["CRM_TAB_2_TITLE"] = "Eventos del Prospecto";
$MESS["CRM_TAB_3"] = "Business Processes";
$MESS["CRM_TAB_3_TITLE"] = "Business Processes del Prospecto";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Coincidencias encontradas";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Clones posibles";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignorar y guardar";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Cancelar";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_FULL_NAME_SUMMARY_TITLE"] = "por nombre completo";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_COMPANY_TTL_SUMMARY_TITLE"] = "por nombre de la compañía";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "por teléfono";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "por correo electrónico";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "encontrado";
$MESS["CRM_LEAD_EDIT_TITLE"] = "Prospecto ##ID# &mdash; #TITLE#";
$MESS["CRM_LEAD_CREATE_TITLE"] = "Nuevo prospecto";
?>