<?
$MESS["CRM_TAB_1"] = "Лід";
$MESS["CRM_TAB_1_TITLE"] = "Властивості ліда";
$MESS["CRM_TAB_2"] = "Події";
$MESS["CRM_TAB_2_TITLE"] = "Події ліду";
$MESS["CRM_TAB_3"] = "Бізнес-процеси";
$MESS["CRM_TAB_3_TITLE"] = "Бізнес-процеси ліда";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Знайдені збіги";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Підозра на дублікати";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ігнорувати і зберегти";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Скасувати";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_FULL_NAME_SUMMARY_TITLE"] = "по ПІБ";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_COMPANY_TTL_SUMMARY_TITLE"] = "за назвою компанії";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "по телефону";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "по e-mail";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "знайдено";
?>