<?
$MESS["CRM_TAB_1"] = "Lead'as";
$MESS["CRM_TAB_1_TITLE"] = "Lead'o savybės";
$MESS["CRM_TAB_2"] = "Žurnalas";
$MESS["CRM_TAB_2_TITLE"] = "Lead'o žurnalas";
$MESS["CRM_TAB_3"] = "Verslo procesai";
$MESS["CRM_TAB_3_TITLE"] = "Lead'o verslo procesai";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Rasti sutapimai";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Galimi klonai";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignoruoti ir išsaugoti";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Atšaukti";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_FULL_NAME_SUMMARY_TITLE"] = "pagal pilną vardą";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_COMPANY_TTL_SUMMARY_TITLE"] = "pagal įmonės pavadinimą";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "pagal telefoną";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "pagal el.paštą";
$MESS["CRM_LEAD_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "rasta";
$MESS["CRM_LEAD_EDIT_TITLE"] = "Lead'as ##ID# &mdash; #TITLE#";
$MESS["CRM_LEAD_CREATE_TITLE"] = "Naujas lead'as";
?>