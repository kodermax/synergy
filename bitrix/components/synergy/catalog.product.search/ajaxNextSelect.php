<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");

$arSort = Array("ID" => "ASC");

$arFilter = Array(
	"IBLOCK_ID" => 21,
	$_POST["code"] => intval($_POST["value"])
);


foreach($_POST["prevSelects"] as $arPrevSelect){	$arFilter[$arPrevSelect["name"]] = $arPrevSelect["value"];}

$nextCode = $_POST["nextCode"];

$arSelect = Array("ID", "NAME", $nextCode, $nextCode.".NAME");

$obj = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

while($array = $obj->Fetch()){
	$arCode[$array[$nextCode."_VALUE"]]["NAME"] = $array[$nextCode."_NAME"];
	$arCode[$array[$nextCode."_VALUE"]]["VALUE"] = $array[$nextCode."_VALUE"];
}

$count = $obj->SelectedRowsCount();

sort($arCode);

if(strlen($arCode[0]["NAME"]) == 0){	$count = 0;}
?>

<script>
var count = parseInt("<?=$count?>");
</script>

<option value="0" disabled="" selected=""><?=$_POST["nameNextSelect"]?></option>
<?foreach($arCode as $arOption):?>
<option value="<?=$arOption["VALUE"]?>"><?=$arOption["NAME"]?></option>
<?endforeach;?>