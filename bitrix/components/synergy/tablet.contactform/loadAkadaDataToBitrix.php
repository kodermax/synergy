<?require($_SERVER["DOCUMENT_ROOT"]."/crm/deal/def.ini.php");?><?require($_SERVER["DOCUMENT_ROOT"]."/crm/deal/def.ini.php");?><?
$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www";
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
@set_time_limit(0);
ignore_user_abort(true);

require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('highloadblock');

global $APPLICATION;

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hlblock   = HL\HighloadBlockTable::getById( 4 )->fetch();
$entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
$entity_data_class = $entity->getDataClass();

$arAkadaData = array();

if (($handle = fopen("Akada2.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 255, ";")) !== FALSE) {

        $arAkadaData = array(
                'UF_GUID_CONTACT'=> $data[0],
                'UF_FIO'=> $data[1],
                'UF_BIRTHDAY'=> $data[2],
        );

        $result = $entity_data_class::add($arAkadaData);
        if (!$result->isSuccess()){
            $APPLICATION->ThrowException(var_dump($result->getErrors()).var_dump($arAkadaData));
            return false;
        }

    }
    fclose($handle);
}
?>