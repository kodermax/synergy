var
now = new Date(),
IS_MANUAL_EDITING = false
;

 
function FirstCharUp(frmObj) {
var index;
var tmpStr;
var tmpChar;
var preString;
var postString;
var strlen;
tmpStr = frmObj.value.toLowerCase();
strLen = tmpStr.length;
if (strLen > 0)  {
for (index = 0; index < strLen; index++)  {
if (index == 0)  {
tmpChar = tmpStr.substring(0,1).toUpperCase();
postString = tmpStr.substring(1,strLen);
tmpStr = tmpChar + postString;
}
else {
tmpChar = tmpStr.substring(index, index+1);
if (tmpChar == " " && index < (strLen-1))  {
tmpChar = tmpStr.substring(index+1, index+2).toUpperCase();
preString = tmpStr.substring(0, index+1);
postString = tmpStr.substring(index+2,strLen);
tmpStr = preString + tmpChar + postString;
         }
      }
   }
}
frmObj.value = tmpStr;
}

function goodMessage(text, to){
	if(!text) text = "Изменения сохранены успешно";
	if(!to) to = 2000;

	$(".fastMessage").html(text).fadeIn("middle", function(){
		setTimeout(function(){
			$(".fastMessage").fadeOut("middle");
	}, to);
	});
}

function badMessage(text, to){
	if(!text) text = "Ошибка!";
	if(!to) to = 5000;

	$(".fastMessage").html(text).addClass("bad").fadeIn("middle", function(){
		setTimeout(function(){
			$(".fastMessage").fadeOut("middle",function(){
				$(this).removeClass("bad")
			});
	}, to);
	});
}

/* function for change deal params */
function changeInput(input){
	var doAjax = true;
	if(input.context.defaultValue == input.val()){
		doAjax = false;
	}
	var dataInput = {};
	dataInput["value"] = input.val();
	dataInput["codeField"] = input.attr("codeField");
	dataInput["typeField"] = input.attr("crm");
	dataInput["dealId"] = parseInt(dealId);
	dataInput["contactId"] = parseInt(contactId);
	if(input.attr("name") == "EMAIL" && !isValidEmail(input.val())) {
		badMessage("Некорректный e-mail!");
		doAjax = false;
	}
	if(input.attr("valueId")){
		dataInput["valueId"] = input.attr("valueId");
		dataInput["valueType"] = input.attr("valueType");
	}

	if(doAjax == true){
		$.ajax({
			type:'POST',
			url:componentPath+"/changeInput.php",
			success:function(data){
				if(data.response == true){
					goodMessage();
				}else{
					badMessage();
				}
			},
			data:dataInput,
			dataType:'json'
	});
	}
}

function matrixSelect(inputName, isInput){
	if(isInput == true){
		var thisInput = $("input[name="+inputName+"]");
		var code = thisInput.attr("code");
		var indexThisInput = thisInput.parents("tr").index();
		indexThisInput = indexThisInput+1;
		var nextCode = $("#matrix tr").eq(indexThisInput).find("select").attr("code");
		$("input[name="+inputName+"]").autocomplete({
			source: arrayAutocomplete[inputName],
			select:function(i, val){
				filialId = val.item.paramId;
				matrixData = {};
				matrixData["code"] = code;
				matrixData["paramId"] = filialId;
				matrixData["nextCode"] = nextCode;

				$("#matrix .iconChange").eq(0).show();

				$.ajax({
					type:'POST',
					url:componentPath+"/matrixSelect.php",
					success:function(data){
						if(data.count > 0){
							var allSelects = $("#matrix select");
							$.each(allSelects, function(i, val){
								$("#matrix select").eq(i).html("").attr("disabled","disabled");
								$("#matrix select").eq(i).removeClass("focus");
								$("#matrix select").eq(i).parent().parent("tr").find(".iconGood").hide();
							});
							var strSelect = "<option value='0' disabled selected>Выберите значение</option>";
							$.each(data.nextAutocompleteArray, function(i, val){
								if(val.VALUE == null) {
									strSelect += "<option value=0>Нет значения</option>";
								}
								else {
									strSelect += "<option value="+val.VALUE+">"+val.NAME+"</option>";
								}
							});
							$("#matrix select[code="+data.nextCode+"]").removeAttr("disabled").html(strSelect).addClass("focus").trigger('loaded');
							$("#matrix .iconGood").eq(0).show();
							$("#matrix .iconChange").eq(0).hide();
						}else{
							badMessage("Программ не найдено!");
						}
					},
					data:matrixData,
					dataType:'json'
				});
			},
		});
	}
	else {
		var thisSelect = $("#matrix select[name="+inputName+"]");
		var indexThisSelect = thisSelect.parents("tr").index();
		indexThisSelect = indexThisSelect+1;
		var nextCode = $("#matrix tr").eq(indexThisSelect).find("select").attr("code");
		var allSelects = $("#matrix select");
		var matrixData = {};
		matrixData["prevParams"] = {};
		$.each(allSelects, function(i, val){
			var value = parseInt($("#matrix select").eq(i).val());
			var code = $("#matrix select").eq(i).attr("code");
			if(value > 0 && code != thisSelect.attr("code") && i < thisSelect.parents("tr").index()){
				matrixData["prevParams"][code] = value;
			}
			if(i >= thisSelect.parents("tr").index()){
				$("#matrix select").eq(i).html("").attr("disabled","disabled");
				$("#matrix select").eq(i).removeClass("focus");
				$("#matrix select").eq(i).parent().parent("tr").find(".iconGood").hide();
			}
		});
		matrixData["code"] = thisSelect.attr("code");

		if(thisSelect.val() > 0){
			matrixData["paramId"] = thisSelect.val();
		}
		matrixData["nextCode"] = nextCode;
		matrixData["prevParams"]["PROPERTY_FILIAL"] = filialId;

		/*thisSelect.parents("tr").find(".iconChange").show();*/

		$.ajax({
			type:'POST',
			url:componentPath+"/matrixSelect.php",
			success:function(data){

				// if(data.nextAutocompleteArray && data.nextAutocompleteArray[0]["NAME"]){
				if(data.nextAutocompleteArray && data.nextAutocompleteArray.length > 1){

					var strSelect = "<option value='0' disabled selected>Выберите значение</option>";
					$.each(data.nextAutocompleteArray, function(i, val){
						if(val.VALUE == null) {
							strSelect += "<option value=0>Нет значения</option>";
						}
						else {
							strSelect += "<option value="+val.VALUE+">"+val.NAME+"</option>";
						}
					});
					$("#matrix select[code="+data.nextCode+"]").removeAttr("disabled").html(strSelect).addClass("focus").trigger('loaded');
				}else{

					$("#matrix select[code="+data.nextCode+"]").removeAttr("disabled").html("<option value='0' selected>Нет значения</option>");
					$("#matrix select[code="+data.nextCode+"]").parent().parent()
					.find(".iconGood").show().end()
					.find(".iconChange").hide();

					var nextSelect = $("#matrix select[code="+data.nextCode+"]").attr("name");
					var price;
					var id;
					if(data.nextCode){
						matrixSelect(nextSelect);
					} else {
						$('#matrix select').off('loaded');

						var i = 0;
						$.each(data.programs, function(j, value){
							if(i == 0){
								id = value.ID;
								price = value.PROPERTY_PRICE_VALUE;
							}
							i++;
						});
						$("#matrix tr.productInfo").remove();
						$(".matrix table").append("<tr class='productInfo'><td class='priceProduct'>Ставка:</td><td class='priceProduct' productId='"+id+"'><input type='hidden' id='productId' value='"+id+"'><b><big>"+price+"</big></b></td><td class='priceProduct SaveAkada'><button class='webform-button-text webform-button-create' onclick='saveAkada();' id='button-saveAkada'>Отправить в Акада</button></td></tr>");
						if (AKADA_SYNC) { /* Если у сделки указан AKADA_SYNC, нужно дизаблить редактирование товара */
							$('#matrix :input').prop('disabled', true).removeAttr('placeholder').off().attr('class', 'disabled');
						}
						if(id && IS_MANUAL_EDITING){
							var dataProduct = {};
							dataProduct["productId"] = id;
							dataProduct["dealId"] = dealId;
							dataProduct["contactId"] = contactId;
							dataProduct["price"] = price;
							$.ajax({
								type:'POST',
								url:componentPath+"/saveProduct.php",
								success:function(data){
									if(data.response == true){
										goodMessage("Продукт сохранен в сделку!");
									}
								},
								data:dataProduct,
								dataType:'json'
							});
						}
					}
				}
				thisSelect.removeClass("focus");
				$("#matrix tr").eq(indexThisSelect-1).find(".iconGood").show();
			},
			data:matrixData,
			dataType:'json'
		});
	}
}

$(document).ready(function(){
    var iii=0;
        $('.auto_scroll_focus').each(function(index ){
            if($(this).css('dispay') != 'none'){
                iii++;
                $(this).attr('unic_id', 'uid_' + iii);
            }
        })    
    
	$("a.linkPrint").click(function(){
		var dataPrint = {};
		dataPrint["contactId"] = contactId;
		dataPrint["dealId"] = dealId;
		dataPrint["bpId"] = $(this).attr("bpId");
		dataPrint["productid"] = $('#productId').val();

//                dataPrint["BL_LVL"] = $('#PROPERTY_LEVEL').val();
//		dataPrint["BL_SUBLVL"] = $('#PROPERTY_SUBLEVEL').val();
//                dataPrint["BL_NUM_SMSTR"] = $('#PROPERTY_SEMESTR').val();
//		dataPrint["BL_CNT_SMSTR"] = $('#qqq').val();
//                dataPrint["BL_CLSS"] = $('#PROPERTY_CLASS').val();
//		dataPrint["BL_CTGR"] = $('#PROPERTY_ADDITIONAL_CATEGORY').val();
//                dataPrint["BL_FLL"] = $('#filials').val();
                
                
                //console.log(dataPrint);
		$.ajax({
			type:'POST',
			url:componentPath+"/print_avail.php",
			success:function(data){
                            $(".selectBP").html(data);
                            $(".selectBP").show();
				
			},
			data:dataPrint
		});            
		
	});

	$(document).on("click",".selectBP a",function(){
		var dataPrint = {};
		dataPrint["contactId"] = contactId;
		dataPrint["dealId"] = dealId;
		dataPrint["bpId"] = $(this).attr("bpId");
		dataPrint["printId"] = $('#pr option:selected').attr('id');
                //console.log(dataPrint);
		$.ajax({
			type:'POST',
			url:componentPath+"/printAjax.php",
			success:function(data){
				alert("Документ отправлен на печать");
			},
			data:dataPrint
		});
	});

	$.ui.autocomplete.filter = function (array, term) {
		var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
		return $.grep(array, function (value) {
			return matcher.test(value.label || value.value || value);
		});
	};
        $(".tablet-form .form-fields input[type='number']")
 	.keyup(function(e){
		if(e.keyCode == 13){
               
                        var ob = $(this);
                        $('.auto_scroll_focus').each(function(index ){
                            if($(this).attr('unic_id') == $(ob).attr('unic_id')){
                                $('.auto_scroll_focus').eq(index+1).focus();
                            }
                        })

		}
	})       
        
        
	$(".tablet-form .form-fields input:text")
	.focus(function(){
		$(this).addClass("focus");
	})
	.keyup(function(e){
		if(e.keyCode == 13){
			$(this).blur();
                
                
                        var ob = $(this);
                        $('.auto_scroll_focus').each(function(index ){
                            if($(this).attr('unic_id') == $(ob).attr('unic_id')){
                                $('.auto_scroll_focus').eq(index+1).focus();
                            }
                        })

		}
	})
	.blur(function(){
		$(this).removeClass("focus");
		if( $(this).hasClass("datepicker") || $(this).hasClass("autocomplete") || $(this).hasClass("group-validate") ) return;
                               
		changeInput($(this));
	});

	$(".tablet-form .form-fields select")
	.focus(function(){
		$(this).addClass("focus"); 
	})
	.blur(function(){
		$(this).removeClass("focus");
	})
	.change(function(){
		$(this).removeClass("focus");
		if( $(this).hasClass("group-validate") ) return;

                        var ob = $(this);
                        $('.auto_scroll_focus').each(function(index ){
                            if($(this).attr('unic_id') == $(ob).attr('unic_id')){
                                $('.auto_scroll_focus').eq(index+1).focus();
                            }
                        })            
                
		changeInput($(this));
	})
	.keyup(function(e){
		if(e.keyCode == 13){
                        var ob = $(this);
                        $('.auto_scroll_focus').each(function(index ){
                            if($(this).attr('unic_id') == $(ob).attr('unic_id')){
                                $('.auto_scroll_focus').eq(index+1).focus();
                            }
                        })
		}
	})        
	;

	$("input[name*=EMAIL]")
	.keyup(function(){
		if(isValidEmail($(this).val())) {
			$(this).css({"background":"#06BD1E","color":"#000"});
		} else {
			$(this).css({"background":"red","color":"#fff"});
		}
	})
	.blur(function(){
		$(this).css({"background":"#fff","color":"#000"});
	})
	;

	$('.tablet-form .form-fields input:text.datepicker').on('init', function() {
		$(this).datepicker({
			dateFormat: "dd.mm.yy",
			firstDay: 1,
			changeYear:1,
			yearRange: "1940:"+now.getFullYear(),
			monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
			dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
			onSelect:function(){
				$(this).trigger('change');

				if( !$(this).hasClass("group-validate") ) {
					changeInput($(this));
				}
			}
		});
	}).trigger('init');

	$("#matrix select")
	.change(function(){
		matrixSelect($(this).attr("name"));
	})
	.click(function(){
		$('#matrix select').off('loaded');
		IS_MANUAL_EDITING = true;
	})
	.blur(function(){
		$(this).removeClass("focus");
	})
	;

	$(".tablet-form .form-fields input:text.autocomplete").keyup(function(e){
			var dataAjax = {};
			var urlAjax;
			dataAjax["query"] = $(this).val();
			dataAjax["codeField"] = $(this).attr("codefield");
			if(dataAjax["codeField"] == PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID){
				urlAjax = componentPath + "/EducationalInstitutions.php";
			}else if(dataAjax["codeField"] == PROP_CONTACT_SPECIALITYSBEFOREENTERING){
				urlAjax = componentPath + "/SpecialitysBeforeEntering.php";
			}
			if(dataAjax["query"].length == 2){
				$.ajax({
					type:'POST',
					url:urlAjax,
					success:function(data){
					var tags = [];
					var i = 0;
					$.each(data.response, function(j, val){
							tags[i] = {label: val.NAME, paramId: val.ID};
							i++;
						});
						$(".tablet-form .form-fields input:text.autocomplete[codeField="+dataAjax["codeField"]+"]").autocomplete({
							source:tags,
							select:function(data, value){
								var dataInput = {};
								dataInput["value"] = value.item.paramId;
								dataInput["codeField"] = dataAjax["codeField"];
								dataInput["typeField"] = "CONTACT";
								dataInput["dealId"] = parseInt(dealId);
								dataInput["contactId"] = parseInt(contactId);
								$.ajax({
									type:'POST',
									url:componentPath+"/changeInput.php",
									success:function(data){
										if(data.response == true){
											goodMessage();
										}else{
											badMessage();
										}
									},
									data:dataInput,
									dataType:'json'
								});
							}
						});
					},
					data:dataAjax,
					dataType:'json'
				});
			}
		});
		/*$("#theForm input:button").click(function(){
			$("#theForm").ajaxSubmit({
				url: componentPath+'/saveFile.php',
	type: 'post',
	success: function(data){
			},
			dataType:'json'
	});
		});*/
		$("#citizenship").autocomplete({
			source:arrayAutocomplete["citizenship"],
			select:function(i, val){
				var dataInput = {};
				dataInput["value"] = val.item.paramId;
				dataInput["codeField"] = PROP_CONTACT_CITIZENSHIP;
				dataInput["typeField"] = "CONTACT";
				dataInput["dealId"] = parseInt(dealId);
				dataInput["contactId"] = parseInt(contactId);
				$.ajax({
					type:'POST',
					url:componentPath+"/changeInput.php",
					success:function(data){
						if(data.response == true){
							goodMessage();
						}else{
							badMessage();
						}
					},
					data:dataInput,
					dataType:'json'
				});
			}
		});
		$("input.foreignlanguage").autocomplete({
			source:arrayAutocomplete["foreignlanguage"],
			select:function(i, val){
				var dataInput = {};
				dataInput["value"] = val.item.paramId;
				dataInput["codeField"] = PROP_CONTACT_FOREIGNLANGUAGE;
				dataInput["typeField"] = "CONTACT";
				dataInput["dealId"] = parseInt(dealId);
				dataInput["contactId"] = parseInt(contactId);
				$.ajax({
					type:'POST',
					url:componentPath+"/changeInput.php",
					success:function(data){
						if(data.response == true){
							goodMessage();
						}else{
							badMessage();
						}
					},
					data:dataInput,
					dataType:'json'
				});
			}
		});
		matrixSelect("filials", true);
		$(".inputPhone").mask("79999999999");
		$(".inputZip").mask("999999");
                
                $.mask.definitions['А'] = "[А-Яа-я0-9]*";
                $(".inputAddress").mask("индеск:999999, адрес:ААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААА");
	}
);

function saveAkada(){
	var
	dataInput = {},
	error = false
	;

	dataInput["dealId"] = parseInt(dealId);
	$('#button-saveAkada').removeProp('disabled').addClass('webform-button-create');

	for(var k in arrayRequiredFilds) {
		var
		$input = $('#' + arrayRequiredFilds[k]),
		value = $.trim( $input.val() )
		;

		if(value == '') {
			error = true;
		}
		$input.toggleClass('has-error', value == '');
	}

	if(error)	{
		$('.tablet-form :input.has-error:first').focus();
		return;
	}

	goodMessage('<div class="iconChange"></div> Ждите, анкета отправляется в АКАДА.', 60000);
	$('#button-saveAkada').prop('disabled', true).removeClass('webform-button-create');

	$.ajax({
		type: 'GET',
		url: componentPath+"/saveAkada.php",
		data:dataInput,
		dataType:'json',
		success: function(msg) {
			if(msg.response == true){
				goodMessage(msg.message, 6000);
			}else{
				badMessage();
				$('#button-saveAkada').removeProp('disabled').addClass('webform-button-create');
			}
		}

	});
}
