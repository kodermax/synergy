<?$APPLICATION->SetTitle("Сделка ".$arParams["DEAL_ID"].". Форма дял планшета");?>
<div class="dialogChangeInput"></div>
<div class="fastMessage"></div>
<div class="tablet-form">
    <div style="position:relative;padding-right:60px;">
        <a style="float:left" href="/crm/deal/show/<?=$arParams["DEAL_ID"]?>/">Вернуться на страницу сделки</a>
    </div>
    <br /><br />
    <table class="form-fields">
        <tr class="caption">
            <td colspan="2">Информация о контакте:</td>
        </tr>
        <tr class="name-contact">
            <td>
                <label>Имя: </label>
            </td>
            <td>
                <input codefield="NAME" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"]["NAME"]?>">
            </td>
        </tr>
        <tr class="last-name-contact">
            <td>
                <label>Фамилия: </label>
            </td>
            <td>
                <input codefield="LAST_NAME" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"]["LAST_NAME"]?>">
            </td>
        </tr>
        <tr class="second-name-contact">
            <td>
                <label>Отчество: </label>
            </td>
            <td>
                <input codefield="SECOND_NAME" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"]["SECOND_NAME"]?>">
            </td>
	</tr>
	<tr class="birthday">
            <td>
                <label>День рождения: </label>
            </td>
            <td>
                <input codefield="BIRTHDATE" crm="CONTACT" class="datepicker" type="text" value="<?=ConvertDateTime($arResult["CONTACT_FIELDS"]["BIRTHDATE"],"DD.MM.YYYY")?>">
            </td>
        </tr>
        <tr class="gender">
            <td>
                <label>Пол: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_GENDER?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["GENDER"] as $arGender):?>
                        <option <?if($arGender["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_GENDER]):?>selected<?endif;?> value="<?=$arGender["ID"]?>"><?=$arGender["VALUE"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
	<?if(count($arResult["CONTACT_FIELDS"]["PHONE"] > 0)):?>
            <?foreach($arResult["CONTACT_FIELDS"]["PHONE"] as $arPhone):?>
                <tr class="phone">
                    <td>
                        <label>Телефон: </label>
                    </td>
                    <td>
                        <input class="inputPhone" codefield="PHONE" crm="CONTACT" type="text" valueId="<?=$arPhone["ID"]?>" valueType="<?=$arPhone["VALUE_TYPE"]?>" value="<?=$arPhone["VALUE"]?>">
                    </td>
		</tr>
            <?endforeach;?>
	<?endif;?>
	<?if(count($arResult["CONTACT_FIELDS"]["EMAIL"] > 0)):?>
            <?foreach($arResult["CONTACT_FIELDS"]["EMAIL"] as $arEmail):?>
                <tr class="email">
                    <td>
                        <label>E-mail: </label>
                    </td>
                    <td>
                        <input codefield="EMAIL" crm="CONTACT" name="EMAIL" type="text" valueId="<?=$arEmail["ID"]?>" valueType="<?=$arEmail["VALUE_TYPE"]?>" value="<?=$arEmail["VALUE"]?>">
                    </td>
		</tr>
            <?endforeach;?>
	<?endif;?>
	<tr class="fio_mother">
            <td>
                <label>ФИО матери: </label>
            </td>
            <td>
                <input codefield="<?=PROP_CONTACT_FIOMOTHER?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOMOTHER]?>">
            </td>
	</tr>
	<tr class="phone_mother">
            <td>
                <label>Телефон матери: </label>
            </td>
            <td>
                <input class="inputPhone" codefield="<?=PROP_CONTACT_PHONEMOTHER?>" crm="CONTACT" type="text" name="PHONE" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEMOTHER]?>">
            </td>
	</tr>
	<tr class="fio_father">
            <td>
                <label>ФИО отца: </label>
            </td>
            <td>
                <input codefield="<?=PROP_CONTACT_FIOFATHER?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOFATHER]?>">
            </td>
	</tr>
	<tr class="phone_father">
            <td>
		<label>Телефон отца: </label>
            </td>
            <td>
		<input class="inputPhone" codefield="<?=PROP_CONTACT_PHONEFATHER?>" crm="CONTACT" type="text" name="PHONE" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEFATHER]?>">
            </td>
	</tr>
	<tr class="training_period">
            <td>
                <label>Срок обучения: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_TRAINING_PERIOD?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["TRAINING_PERIOD"] as $arTrainingPeriod):?>
                        <option <?if($arTrainingPeriod["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TRAINING_PERIOD]):?>selected<?endif;?> value="<?=$arTrainingPeriod["ID"]?>"><?=$arTrainingPeriod["VALUE"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
	<tr class="hostel">
            <td>
		<label>Нуждаюсь в общежитии: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_HOSTEL?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["HOSTEL"] as $arHostel):?>
			<option <?if($arHostel["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_HOSTEL]):?>selected<?endif;?> value="<?=$arHostel["ID"]?>"><?=$arHostel["VALUE"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
        <tr>
            <td>
		<label>Место рождения: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PLACE_BIRTH?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PLACE_BIRTH]?>">
            </td>
	</tr>
    	<tr>
            <td>
		<label>Являюсь победителем и призером заключительного этапа Всероссийской олимпиады школьников и члены сборных команд РФ международный олимпиад по: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_WINNER?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_WINNER]?>">
            </td>
	</tr>
	<tr>
            <td>
		<label>Реквизиты документа: </label>
            </td>
            <td>
                <input codefield="<?=PROP_CONTACT_REQUISITES?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_REQUISITES]?>">
            </td>
	</tr>
	<tr>
            <td>
                <label>Перечень необходимых специальных условий при проведении вступительных испытаний в связи с ограниченными возможностями здоровья или инвалидностью: </label>
            </td>
            <td>
                <input codefield="<?=PROP_CONTACT_LIST_DOCS?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_LIST_DOCS]?>">
            </td>
	</tr>
	<tr>
            <td>
                <label>Место сдачи вступительных испытаний с использованием дистанционных технологий: </label>
            </td>
            <td>
                <input codefield="<?=PROP_CONTACT_PLACE_EXAMS?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PLACE_EXAMS]?>">
            </td>
	</tr>
	<tr>
            <td>
                <label>Сведения об индивидуальных достижениях: </label>
            </td>
            <td>
                <input codefield="<?=PROP_CONTACT_PROGRESS?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PROGRESS]?>">
            </td>
	</tr>
        <tr>
            <td>
                <label>Гражданство: </label>
            </td>
            <td>
                <input value="<?=$arResult["PROP_CONTACT_CITIZENSHIP"]["NAME"]?>" type="text" class="autocomplete" id="citizenship">
                    <?/*
                        <select codefield="<?=PROP_CONTACT_CITIZENSHIP?>" crm="CONTACT">
                            <option value="0" disabled selected>Выберите значение</option>
                            <?foreach($arResult["DOP_PARAMS"]["Citizenship"] as $arCitizenship):?>
                                <option <?if($arCitizenship["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_CITIZENSHIP]):?>selected<?endif;?> value="<?=$arCitizenship["ID"]?>"><?=$arCitizenship["NAME"]?></option>
                            <?endforeach;?>
			</select>
                     */?>
            </td>
	</tr>
	<tr>
            <td>
                <label>Иностранный язык: </label>
            </td>
            <td>
                <input value="<?=$arResult["PROP_CONTACT_FOREIGNLANGUAGE"]["NAME"]?>" type="text" class="autocomplete" id="foreignlanguage">
                    <?/*
                        <select codefield="<?=PROP_CONTACT_FOREIGNLANGUAGE?>" crm="CONTACT">
                            <option value="0" disabled selected>Выберите значение</option>
                            <?foreach($arResult["DOP_PARAMS"]["ForeignLanguage"] as $arForeignLanguage):?>
                                <option <?if($arForeignLanguage["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_FOREIGNLANGUAGE]):?>selected<?endif;?> value="<?=$arForeignLanguage["ID"]?>"><?=$arForeignLanguage["NAME"]?></option>
                            <?endforeach;?>
			</select>
                    */?>
            </td>
	</tr>
        <?/*<tr class="caption">
            <td colspan="2">Баллы вступительных испытаний: </td>
	</tr>*/?>
        <tr class="class">
            <td>
                <label>Категория вступительных испытаний: </label>
            </td>
            <td>
            	<select codefield="<?=PROP_CONTACT_CATEGOREXUM?>" crm="CONTACT">
                    <option value="0">Нет</option>
                    <?foreach($arResult["SELECT_FIELDS"]["CATEGOR_EXUM"] as $arCatExumDO):?>
                        <option <?if($arResult["CONTACT_FIELDS"][PROP_CONTACT_CATEGOREXUM] == $arCatExumDO["ID"]):?>selected<?endif;?> value="<?=$arCatExumDO["ID"]?>"><?=$arCatExumDO["VALUE"]?></option>
                    <?endforeach;?>
                </select>
            </td>
	</tr>
	<tr>
            <td>
		<label>Дисциплины вступительных испытаний:</label>
            </td>
            <td>
                <select multiple size="5" codefield="<?=PROP_CONTACT_DISCIPLINESOFADMISSIONTESTS?>" crm="CONTACT">
                    <?foreach($arResult["DOP_PARAMS"]["DisciplinesOfAdmissionTests"] as $arDisciplinesOfAdmissionTests):?>
                        <option <?if(in_array($arDisciplinesOfAdmissionTests["ID"], $arResult["CONTACT_FIELDS"][PROP_CONTACT_DISCIPLINESOFADMISSIONTESTS])):?>selected<?endif;?> value="<?=$arDisciplinesOfAdmissionTests["ID"]?>"><?=$arDisciplinesOfAdmissionTests["NAME"]?></option>
                    <?endforeach;?>
                    <option value="0">Нет</option>
		</select>
            </td>
	</tr>
        <tr>
            <td>
                <label>Результаты вступительных испытаний:</label>
            </td>
            <td>
                <input placeholder="Вводите через запятую" codefield="<?=PROP_CONTACT_RESULTSOFADMISSIONTESTS?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_RESULTSOFADMISSIONTESTS]?>">
            </td>
	</tr>
    	<tr>
            <td>
                <label>Виды вступительных испытаний: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_TYPESOFADMISSIONTESTS?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["DOP_PARAMS"]["TypesOfAdmissionTests"] as $arTypesOfAdmissionTests):?>
                        <option <?if($arTypesOfAdmissionTests["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPESOFADMISSIONTESTS]):?>selected<?endif;?> value="<?=$arTypesOfAdmissionTests["ID"]?>"><?=$arTypesOfAdmissionTests["NAME"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
        <tr>
            <td>
                <label>Образовательное учреждение: </label>
            </td>
            <td>
                <input class="autocomplete" placeholder="Начните ввод" codefield="<?=PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID?>" crm="CONTACT" type="text" value="<?=$arResult["PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID"]["NAME"]?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Тип документа об образовании: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_TYPSDOCUMENTOFEDUCATION?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["DOP_PARAMS"]["TypsDocumentOfEducation"] as $arTypsDocumentOfEducation):?>
                        <option <?if($arTypsDocumentOfEducation["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPSDOCUMENTOFEDUCATION]):?>selected<?endif;?> value="<?=$arTypsDocumentOfEducation["ID"]?>"><?=$arTypsDocumentOfEducation["NAME"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
	<tr>
            <td>
                <label>Специальность до поступления: </label>
            </td>
            <td>
                <input class="autocomplete" placeholder="Начните ввод" codefield="<?=PROP_CONTACT_SPECIALITYSBEFOREENTERING?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_SPECIALITYSBEFOREENTERING"]["NAME"]?>">
            </td>
	</tr>
	<tr>
            <td>
		<label>Тип документа удостоверяющего личность: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_TYPEIDENTITYDOCUMENTS?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["DOP_PARAMS"]["TypeIdentityDocuments"] as $arTypeIdentityDocuments):?>
                        <option <?if($arTypeIdentityDocuments["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPEIDENTITYDOCUMENTS]):?>selected<?endif;?> value="<?=$arTypeIdentityDocuments["ID"]?>"><?=$arTypeIdentityDocuments["NAME"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
	<?/*
            <tr>
                <td>
                    <label>Тип контактной информации: </label>
		</td>
		<td>
                    <select codefield="<?=PROP_CONTACT_TYPESOFCONTACTINFORMATION?>" crm="CONTACT">
                        <option value="0" disabled selected>Выберите значение</option>
			<?foreach($arResult["DOP_PARAMS"]["TypesOfContactInformation"] as $arTypesOfContactInformation):?>
                            <option <?if($arTypesOfContactInformation["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPESOFCONTACTINFORMATION]):?>selected<?endif;?> value="<?=$arTypesOfContactInformation["ID"]?>"><?=$arTypesOfContactInformation["NAME"]?></option>
			<?endforeach;?>
                    </select>
		</td>
            </tr>
        */?>
	<tr class="caption">
            <td colspan="2">Паспортные данные: </td>
	</tr>
	<tr class="passport_seria">
            <td>
		<label>Серия: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PASSPORT_SERIA?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_SERIA]?>">
            </td>
	</tr>
	<tr class="passport_number">
            <td>
                <label>Номер: </label>
            </td>
            <td>
                <input codefield="<?=PROP_CONTACT_PASSPORT_NUMBER?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_NUMBER]?>">
            </td>
	</tr>
	<tr class="passport_number">
            <td>
                <label>Кем выдан: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PASSPORT_KEMVIDAN?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_KEMVIDAN]?>">
            </td>
	</tr>
	<tr class="passport_dateissue">
            <td>
		<label>Дата выдачи: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PASSPORT_DATEISSUE?>" crm="CONTACT" class="datepicker" type="text" value="<?=ConvertDateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_DATEISSUE], "DD.MM.YYYY")?>">
            </td>
	</tr>
	<tr class="passport_codepodrazdel">
            <td>
                <label>Код подразделения: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PASSPORT_CODEPODRAZDEL?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_CODEPODRAZDEL]?>">
            </td>
	</tr>
	<tr class="caption">
            <td colspan="2">Информация о абитуриенте:</td>
	</tr>
	<tr class="vid_abiturient">
            <td>
		<label>Вид абитуриента: </label>
            </td>
            <td>
                <select codefield="<?=PROP_DEAL_VID_ABITURA?>" crm="DEAL">
                    <option value="0">Нет</option>
                    <?foreach($arResult["SELECT_FIELDS"]["VID_ABITURA"] as $arVidAbitura):?>
                	<option <?if($arResult["DEAL_FIELDS"][PROP_DEAL_VID_ABITURA] == $arVidAbitura["ID"]):?>selected<?endif;?> value="<?=$arVidAbitura["ID"]?>"><?=$arVidAbitura["VALUE"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
	<tr class="project">
            <td>
                <label>Проект: </label>
            </td>
            <td>
                <select codefield="<?=PROP_DEAL_PROJECT?>" crm="DEAL">
                    <option></option>
		</select>
            </td>
	</tr>
	<tr class="project">
            <td>
                <label>Название учебного заведения: </label>
            </td>
            <td>
                <input codefield="<?=PROP_DEAL_NAME_UCHEBZAVED?>" crm="DEAL" type="text" value="<?=$arResult["DEAL_FIELDS"][PROP_DEAL_NAME_UCHEBZAVED]?>">
            </td>
	</tr>
        <tr class="project">
            <td>
                <label>Тип учебного заведения: </label>
            </td>
            <td>
                <input codefield="<?=PROP_DEAL_TYPE_UCHEBZAVED?>" crm="DEAL" type="text" value="<?=$arResult["DEAL_FIELDS"][PROP_DEAL_TYPE_UCHEBZAVED]?>">
            </td>
	</tr>
	<tr class="class">
            <td>
		<label>Класс: </label>
            </td>
            <td>
		<select codefield="<?=PROP_DEAL_CLASS?>" crm="DEAL">
                    <option value="0">Нет</option>
                    <?foreach($arResult["SELECT_FIELDS"]["CLASS"] as $arClass):?>
                	<option <?if($arResult["DEAL_FIELDS"][PROP_DEAL_CLASS] == $arClass["ID"]):?>selected<?endif;?> value="<?=$arClass["ID"]?>"><?=$arClass["VALUE"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
	<tr class="caption">
            <td colspan="2">Документ об образовании:</td>
	</tr>
	<tr class="class">
            <td>
		<label>Вид документа об образовании: </label>
            </td>
            <td>
		<select codefield="<?=PROP_DEAL_VYD_DO?>" crm="DEAL">
                    <option value="0">Нет</option>
                    <?foreach($arResult["SELECT_FIELDS"]["VYD_DO"] as $arVydDO):?>
                        <option <?if($arResult["DEAL_FIELDS"][PROP_DEAL_VYD_DO] == $arVydDO["ID"]):?>selected<?endif;?> value="<?=$arVydDO["ID"]?>"><?=$arVydDO["VALUE"]?></option>
                    <?endforeach;?>
		</select>
            </td>
	</tr>
	<tr class="project">
            <td>
		<label>Серия ДО: </label>
            </td>
            <td>
                <input codefield="<?=PROP_DEAL_SERIA_DO?>" crm="DEAL" type="text" value="<?=$arResult["DEAL_FIELDS"][PROP_DEAL_SERIA_DO]?>">
            </td>
	</tr>
	<tr class="project">
            <td>
            	<label>Номер ДО: </label>
            </td>
            <td>
		<input codefield="<?=PROP_DEAL_NUMBER_DO?>" crm="DEAL" type="text" value="<?=$arResult["DEAL_FIELDS"][PROP_DEAL_NUMBER_DO]?>">
            </td>
	</tr>
	<tr class="project">
            <td>
		<label>Дата выдачи ДО: </label>
            </td>
            <td>
		<input codefield="<?=PROP_DEAL_DATE_DO?>" crm="DEAL" class="datepicker" type="text" value="<?=ConvertDateTime($arResult["DEAL_FIELDS"][PROP_DEAL_DATE_DO], "DD.MM.YYYY")?>">
            </td>
	</tr>
	<tr class="caption">
            <td colspan="2">Плательщик: </td>
	</tr>
        <tr class="project">
            <td>
		<label>Фамилия: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_LAST_NAME?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_LAST_NAME]?>">
            </td>
	</tr>
    	<tr class="project">
            <td>
		<label>Имя: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_NAME?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_NAME]?>">
            </td>
	</tr>
	<tr class="project">
            <td>
		<label>Отчество: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_SECOND_NAME?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_SECOND_NAME]?>">
            </td>
	</tr>
	<tr class="project">
            <td>
		<label>Дата рождения: </label>
            </td>
            <td>
            	<input codefield="<?=PROP_CONTACT_PAYER_BIRTHDAY?>" crm="CONTACT" class="datepicker" type="text" value="<?=ConvertDateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_BIRTHDAY],"DD.MM.YYYY")?>">
            </td>
	</tr>
	<tr class="project">
            <td>
		<label>Телефон: </label>
            </td>
            <td>
		<input class="inputPhone" codefield="<?=PROP_CONTACT_PAYER_PHONE?>" crm="CONTACT" name="PHONE" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PHONE]?>">
            </td>
	</tr>
	<tr class="project">
            <td>
		<label>E-mail: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_EMAIL?>" crm="CONTACT" name="EMAIL" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL]?>">
            </td>
	</tr>
	<tr class="caption">
            <td colspan="2">Паспортные данные плательщика: </td>
	</tr>
	<tr class="passport_seria">
            <td>
		<label>Серия: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_SERIA?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_SERIA]?>">
            </td>
	</tr>
        <tr class="passport_number">
            <td>
		<label>Номер: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_NUMBER?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_NUMBER]?>">
            </td>
	</tr>
    	<tr class="passport_number">
            <td>
		<label>Кем выдан: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN]?>">
            </td>
	</tr>
	<tr class="passport_dateissue">
            <td>
		<label>Дата выдачи: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_DATEISSUE?>" crm="CONTACT" class="datepicker" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_DATEISSUE]?>">
            </td>
	</tr>
	<tr class="passport_codepodrazdel">
            <td>
		<label>Код подразделения: </label>
            </td>
            <td>
		<input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL?>" crm="CONTACT" type="text" value="<?=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL]?>">
            </td>
	</tr>
    </table>
    <div class="matrix">
    	<table>
            <tr class="caption">
		<td colspan="3">Выбор продукта:</td>
            </tr>
            <tr>
		<td>Филиал: </td>
		<td>
                    <input name="filials" type="text" code="PROPERTY_FILIAL" placeholder="Начните ввод" value="Москва">
		</td>
		<td class="icon">
                    <div class="iconChange"></div>
                    <div class="iconGood" style="display:block"></div>
		</td>
            </tr>
            
            <tr>
		<td>Учебный год: </td>
		<td>
                    <select name="academic_year" code="PROPERTY_ACADEMIC_YEAR">
			<option value="0" disabled="" selected="">Выберите значение</option>
			<?/*<option value="<?=IBLOCK_ELEMENT_ID_YEAR_13_14?>">13/14</option> 
			<option value="<?=IBLOCK_ELEMENT_ID_YEAR_14_15?>">14/15</option>*/?>
			<option value="<?=IBLOCK_ELEMENT_ID_YEAR_15_16?>" selected>15/16</option>
                    </select>
		</td>
		<td class="icon">
                    <div class="iconGood" style="display:block"></div>
		</td>
            </tr>
            <tr>
		<td>Семестр учебного года: </td>
                <td>
                    <select name="semestr" code="PROPERTY_SEMESTR">
			<option value="0" disabled="" selected="">Выберите значение</option>
			<option value="<?=IBLOCK_ELEMENT_ID_SEMESTR_1?>">1</option>
                    </select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>Уровень образования: </td>
		<td>
                    <select name="codelevel" code="PROPERTY_LEVEL" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>Подуровень образования: </td>
		<td>
                    <select name="codesublevel" code="PROPERTY_SUBLEVEL" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>Факультет: </td>
		<td>
                    <select name="fuculty" code="PROPERTY_FACULTY" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>Направление: </td>
		<td>
                    <select name="speciality" code="PROPERTY_SPECIALITY" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
                <td>Профессиональная программа: </td>
		<td>
                    <select name="specialization" code="PROPERTY_SPECIALIZATION" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>Программа обучения: </td>
		<td>
                    <select name="trainingprogram" code="PROPERTY_TRAININGPROGRAM" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>День обучения: </td>
		<td>
                    <select name="typesofprograms" code="PROPERTY_TYPESOFPROGRAMS" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>Форма обучения: </td>
                <td>
                    <select name="codeformoflearning" code="PROPERTY_FORMOFLEARNING" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>Срок: </td>
                <td>
                    <select name="duration" code="PROPERTY_DURATION" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
            <tr>
		<td>Дополнительная категория: </td>
		<td>
                    <select name="additional_category" code="PROPERTY_ADDITIONAL_CATEGORY" disabled></select>
		</td>
		<td class="icon">
                    <div class="iconGood"></div>
		</td>
            </tr>
    	</table>
    </div>
    <br /><br />
    <div class="printBP">        
        <?
            CModule::IncludeModule("iblock"); // подключаем модуль инфблок
            $zapros = CIBlockElement::GetList(
                array(), // сортировка нам тут не важна
                array(
                    "IBLOCK_ID" => 94, // выбираем инфоблок с принтерами
                    "ACTIVE" => "Y" // выбираем все активные элементы
                ),
                false,
                false,
                array("ID", "IBLOCK_ID", "PROPERTY_*")
            );
            while ($object = $zapros->GetNextElement()){
                $property = $object->GetProperties();
                $temp = $object->GetFields();
                $result[$temp["ID"]] = $temp;
                $result[$temp["ID"]]["settings"] = $property;
            }
            foreach ($result as $result_tmp){
                foreach ($result_tmp["settings"]["user"]["VALUE"] as $user){
                    if($user == $USER->GetID()){
                        $printer[] = $result_tmp["settings"]["printer"]["VALUE"];
                    }
                }
            }
        ?>
        <script type="text/javascript">
            function print_auto(print){
                $("#printer").load("<?=$templateFolder?>/ajax_print.php",{pr:print});
            }
       </script>
       Принтер
        <select name="printer" id="pr" onchange="javascript:print_auto(this.value);return false;">
            <?$n=1?>
            <?foreach ($printer as $print):?>
                <?if($print == $_SESSION["printer"]):?>
                    <option disabled selected><?=$print?></option> 
                <?else:?>
                    <option id="<?=$n?>" value="<?=$print?>"><?=$print?></option> 
                <?endif;?>
                <?$n++?>
            <?endforeach;?>
        </select>
        <div id="printer"> </div>
        <br /><br />
        <a class="linkPrint" href="javascript:void(0);">Печать</a>
        <div class="selectBP">
            <p>Выберите категорию абитуриента:</p>
            <div>
                <a href="javascript:void(0)" bpId="69">После 9 класса СПО</a><br />
                <a href="javascript:void(0)" bpId="61">После 11 класса ВПО</a><br />
            </div>
        </div>
    </div>
    <script>
        var dealId = "<?=$arResult["DEAL_FIELDS"]["ID"]?>";
        var contactId = "<?=$arResult["CONTACT_FIELDS"]["ID"]?>";
        var componentPath = "<?=$componentPath?>";
        var filialId = "<?=IBLOCK_ELEMENT_ID_CITY_MOSCOW?>";
        var PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID = "<?=PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID?>";
        var PROP_CONTACT_SPECIALITYSBEFOREENTERING = "<?=PROP_CONTACT_SPECIALITYSBEFOREENTERING?>";
        var PROP_CONTACT_CITIZENSHIP = "<?=PROP_CONTACT_CITIZENSHIP?>";
        var PROP_CONTACT_FOREIGNLANGUAGE = "<?=PROP_CONTACT_FOREIGNLANGUAGE?>";
        var arrayAutocomplete = {};
        arrayAutocomplete["filials"] = [];
        arrayAutocomplete["citizenship"] = [];
        arrayAutocomplete["foreignlanguage"] = [];
        <?$i=0;foreach($arResult["FILIAL"] as $arFilial):?>
            arrayAutocomplete["filials"][<?=$i?>] = {label: "<?=$arFilial["NAME"]?>", paramId: <?=$arFilial["ID"]?>};
        <?$i++;endforeach;?>
        <?$i=0;foreach($arResult["CITIZENSHIP"] as $arCitizenShip):?>
            arrayAutocomplete["citizenship"][<?=$i?>] = {label: "<?=$arCitizenShip["NAME"]?>", paramId: <?=$arCitizenShip["ID"]?>};
        <?$i++;endforeach;?>
        <?$i=0;foreach($arResult["FOREIGNLANGUAGE"] as $arForeignLanguage):?>
            arrayAutocomplete["foreignlanguage"][<?=$i?>] = {label: "<?=$arForeignLanguage["NAME"]?>", paramId: <?=$arForeignLanguage["ID"]?>};
        <?$i++;endforeach;?>
    </script>