var now = new Date();
function goodMessage(text){
    if(!text){
        var text = "Изменения сохранены успешно"
    }
    $(".fastMessage").html(text).fadeIn("middle", function(){
        setTimeout(function(){
            $(".fastMessage").fadeOut("middle");
   	}, 2000);
    });
}
function badMessage(text){
    if(!text){
        var text = "Ошибка!"
    }
    $(".fastMessage").html(text).addClass("bad").fadeIn("middle", function(){
        setTimeout(function(){
            $(".fastMessage").fadeOut("middle",function(){
                $(this).removeClass("bad")
            });
   	}, 2000);
    });
}
/* function for change deal params */
function changeInput(input){
    var doAjax = true;
    if(input.context.defaultValue == input.val()){
      	doAjax = false;
    }
    var dataInput = {};
    dataInput["value"] = input.val();
    dataInput["codeField"] = input.attr("codeField");
    dataInput["typeField"] = input.attr("crm");
    dataInput["dealId"] = parseInt(dealId);
    dataInput["contactId"] = parseInt(contactId);
    if(input.attr("name") == "EMAIL"){
	if(!isValidEmail(input.val())){
            badMessage("Некорректный e-mail!");
            doAjax = false;
	}
    }
    if(input.attr("valueId")){
        dataInput["valueId"] = input.attr("valueId");
	dataInput["valueType"] = input.attr("valueType");
    }
    if(doAjax == true){
        $.ajax({
            type:'POST',
            url:componentPath+"/changeInput.php",
            success:function(data){
                if(data.response == true){
                    goodMessage();
                }else{
                    badMessage();
                }
            },
            data:dataInput,
            dataType:'json'
	});
    }
}
function matrixSelect(inputName, isInput){
    if(isInput == true){
        var thisInput = $("input[name="+inputName+"]");
	var code = thisInput.attr("code");
        var indexThisInput = thisInput.parents("tr").index();
	indexThisInput = indexThisInput+1;
	var nextCode = $(".matrix table tr").eq(indexThisInput).find("select").attr("code");
        $("input[name="+inputName+"]").autocomplete({
            source: arrayAutocomplete[inputName],
            select:function(i, val){
		filialId = val.item.paramId;
		matrixData = {};
		matrixData["code"] = code;
		matrixData["paramId"] = filialId;
		matrixData["nextCode"] = nextCode;
		$.ajax({
                    type:'POST',
                    url:componentPath+"/matrixSelect.php",
                    success:function(data){
                        if(data.count > 0){
                            var allSelects = $(".matrix table tr select");
                            $.each(allSelects, function(i, val){
                                $(".matrix table tr select").eq(i).html("").attr("disabled","disabled");
                                $(".matrix table tr select").eq(i).removeClass("focus");
                                $(".matrix table tr select").eq(i).parent().parent("tr").find(".iconGood").hide();
                            });
                            var strSelect = "<option value='0' disabled selected>Выберите значение</option>";
                            $.each(data.nextAutocompleteArray, function(i, val){
                                strSelect += "<option value="+val.VALUE+">"+val.NAME+"</option>";
                            });
                            $(".matrix table tr select[code="+data.nextCode+"]").removeAttr("disabled").html(strSelect).addClass("focus");
                            $(".matrix table tr .iconGood").eq(0).show();
                            $(".matrix table tr .iconChange").eq(0).hide();
                        }else{
                            badMessage("Программ не найдено!");
                        }
                    },
                    data:matrixData,
                    dataType:'json'
                });
            },
        });
    }else{
	var thisSelect = $(".matrix table tr select[name="+inputName+"]");
	var indexThisSelect = thisSelect.parents("tr").index();
	indexThisSelect = indexThisSelect+1;
	var nextCode = $(".matrix table tr").eq(indexThisSelect).find("select").attr("code");
	var allSelects = $(".matrix table tr select");
	var matrixData = {};
	matrixData["prevParams"] = {};
	$.each(allSelects, function(i, val){
            var value = parseInt($(".matrix table tr select").eq(i).val());
            var code = $(".matrix table tr select").eq(i).attr("code");
            if(value > 0 && code != thisSelect.attr("code") && i < thisSelect.parents("tr").index()){
                matrixData["prevParams"][code] = value;
            }
            if(i >= thisSelect.parents("tr").index()){
                $(".matrix table tr select").eq(i).html("").attr("disabled","disabled");
		$(".matrix table tr select").eq(i).removeClass("focus");
		$(".matrix table tr select").eq(i).parent().parent("tr").find(".iconGood").hide();
            }
    	});
	matrixData["code"] = thisSelect.attr("code");
	if(thisSelect.val() > 0){
            matrixData["paramId"] = thisSelect.val();
	}
	matrixData["nextCode"] = nextCode;
	matrixData["prevParams"]["PROPERTY_FILIAL"] = filialId;
	$.ajax({
            type:'POST',
            url:componentPath+"/matrixSelect.php",
            success:function(data){
                if(data.nextAutocompleteArray && data.nextAutocompleteArray[0]["NAME"]){
                    var strSelect = "<option value='0' disabled selected>Выберите значение</option>";
                    $.each(data.nextAutocompleteArray, function(i, val){
                        strSelect += "<option value="+val.VALUE+">"+val.NAME+"</option>";
                    });
                    $(".matrix table tr select[code="+data.nextCode+"]").removeAttr("disabled").html(strSelect).addClass("focus");
                }else{
                    $(".matrix table tr select[code="+data.nextCode+"]").removeAttr("disabled").html("<option value='0' selected>Нет значения</option>");
                    $(".matrix table tr select[code="+data.nextCode+"]").parent().parent().find(".iconGood").show();
                    var nextSelect = $(".matrix table tr select[code="+data.nextCode+"]").attr("name");
                    var price;
                    var id;
                    if(data.nextCode){
                        matrixSelect(nextSelect);
                    }else{
                        var i = 0;
                        $.each(data.programs, function(j, value){
                            if(i == 0){
                                id = value.ID;
                                price = value.PROPERTY_PRICE_VALUE;
                            }
                            i++;
                        });
                        $("tr.productInfo").remove();
                        $(".matrix table").append("<tr class='productInfo'><td class='priceProduct'>Ставка:</td><td class='priceProduct' productId='"+id+"'><b><big>"+price+"</big></b></td><td>&nbsp;</td></tr>");
                        if(id){
                            var dataProduct = {};
                            dataProduct["productId"] = id;
                            dataProduct["dealId"] = dealId;
                            dataProduct["contactId"] = contactId;
                            dataProduct["price"] = price;
                            $.ajax({
                                type:'POST',
                                url:componentPath+"/saveProduct.php",
                                success:function(data){
                                    if(data.response == true){
                                        goodMessage("Продукт сохранен в сделку!");
                                    }
                                },
                                data:dataProduct,
                                dataType:'json'
                            });
                        }
                    }
                }
                thisSelect.removeClass("focus");
                $(".matrix table tr").eq(indexThisSelect-1).find(".iconGood").show();
            },
            data:matrixData,
            dataType:'json'
	});
    }
}
$(document).ready(function(){
    $("a.linkPrint").click(function(){
	$(".selectBP").show();
    });
    $(document).on("click",".selectBP a",function(){
        var dataPrint = {};
	dataPrint["contactId"] = contactId;
	dataPrint["dealId"] = dealId;
	dataPrint["bpId"] = $(this).attr("bpId");
    	$.ajax({
            type:'POST',
            url:componentPath+"/printAjax.php",
            success:function(data){
                alert("Документ отправлен на печать");
            },
            data:dataPrint
	});
    });
    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
	return $.grep(array, function (value) {
	        return matcher.test(value.label || value.value || value);
	    });
	};
	$(".tablet-form .form-fields input:text").focus(function(){
            $(this).addClass("focus");
	});
	$(".tablet-form .form-fields input:text").keyup(function(e){
            if(e.keyCode == 13){
                $(this).blur();
            }
	});
	$(".tablet-form .form-fields input:text").blur(function(){
            $(this).removeClass("focus");
            if(!$(this).hasClass("datepicker") && !$(this).hasClass("autocomplete")){
                changeInput($(this));
            }
	});
	$(".tablet-form .form-fields select").focus(function(){
            $(this).addClass("focus");
	});
	$(".tablet-form .form-fields select").change(function(){
            $(this).removeClass("focus");
            changeInput($(this));
	});
	$(".tablet-form .form-fields select").blur(function(){
            $(this).removeClass("focus");
	});
	$("input[name*=EMAIL]").keyup(function(){
            if(isValidEmail($(this).val())){
                $(this).css({"background":"#06BD1E","color":"#000"});
            }else{
                $(this).css({"background":"red","color":"#fff"});
            }
	});
	$("input[name*=EMAIL]").blur(function(){
            $(this).css({"background":"#fff","color":"#000"});
	});
        $(".tablet-form .form-fields input:text.datepicker").datepicker({
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            changeYear:1,
            yearRange: "1940:"+now.getFullYear(),
            monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
            dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
            onSelect:function(){
            	changeInput($(this));
            }
        });
        $(".matrix table tr select").change(function(){
            matrixSelect($(this).attr("name"));
        });
	$(".matrix table tr select").blur(function(){
            $(this).removeClass("focus");
	});
	$(".tablet-form .form-fields input:text.autocomplete").keyup(function(e){
            var dataAjax = {};
            var urlAjax;
            dataAjax["query"] = $(this).val();
            dataAjax["codeField"] = $(this).attr("codefield");
            if(dataAjax["codeField"] == PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID){
                urlAjax = componentPath + "/EducationalInstitutions.php";
            }else if(dataAjax["codeField"] == PROP_CONTACT_SPECIALITYSBEFOREENTERING){
		urlAjax = componentPath + "/SpecialitysBeforeEntering.php";
            }
            if(dataAjax["query"].length == 2){
		$.ajax({
                    type:'POST',
                    url:urlAjax,
                    success:function(data){
			var tags = [];
			var i = 0;
    			$.each(data.response, function(j, val){
                            tags[i] = {label: val.NAME, paramId: val.ID};
                            i++;
                        });
                        $(".tablet-form .form-fields input:text.autocomplete[codeField="+dataAjax["codeField"]+"]").autocomplete({
                            source:tags,
                            select:function(data, value){
                                var dataInput = {};
                                dataInput["value"] = value.item.paramId;
                                dataInput["codeField"] = dataAjax["codeField"];
                                dataInput["typeField"] = "CONTACT";
                                dataInput["dealId"] = parseInt(dealId);
                                dataInput["contactId"] = parseInt(contactId);
                                $.ajax({
                                    type:'POST',
                                    url:componentPath+"/changeInput.php",
                                    success:function(data){
                                        if(data.response == true){
                                            goodMessage();
                                        }else{
                                            badMessage();
                                        }
                                    },
                                    data:dataInput,
                                    dataType:'json'
                                });
                            }
                        });
                    },
                    data:dataAjax,
                    dataType:'json'
                });
            }
        });
        /*$("#theForm input:button").click(function(){
            $("#theForm").ajaxSubmit({
                url: componentPath+'/saveFile.php',
		type: 'post',
		success: function(data){
            },
            dataType:'json'
		});
        });*/
        $("#citizenship").autocomplete({
            source:arrayAutocomplete["citizenship"],
            select:function(i, val){
    		var dataInput = {};
		dataInput["value"] = val.item.paramId;
		dataInput["codeField"] = PROP_CONTACT_CITIZENSHIP;
		dataInput["typeField"] = "CONTACT";
		dataInput["dealId"] = parseInt(dealId);
		dataInput["contactId"] = parseInt(contactId);
		$.ajax({
                    type:'POST',
                    url:componentPath+"/changeInput.php",
                    success:function(data){
                        if(data.response == true){
                            goodMessage();
                        }else{
                            badMessage();
		        }
                    },
                    data:dataInput,
                    dataType:'json'
		});
            }
        });
        $("#foreignlanguage").autocomplete({
            source:arrayAutocomplete["foreignlanguage"],
            select:function(i, val){
            	var dataInput = {};
		dataInput["value"] = val.item.paramId;
		dataInput["codeField"] = PROP_CONTACT_FOREIGNLANGUAGE;
		dataInput["typeField"] = "CONTACT";
		dataInput["dealId"] = parseInt(dealId);
		dataInput["contactId"] = parseInt(contactId);
		$.ajax({
                    type:'POST',
                    url:componentPath+"/changeInput.php",
                    success:function(data){
                        if(data.response == true){
                            goodMessage();
                        }else{
                            badMessage();
                        }
                    },
                    data:dataInput,
                    dataType:'json'
                });
            }
        });
        matrixSelect("filials", true);
        $(".inputPhone").mask("79999999999");
    }
);