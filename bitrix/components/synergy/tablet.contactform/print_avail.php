<?require($_SERVER["DOCUMENT_ROOT"]."/crm/deal/def.ini.php");?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$id = intval($_POST['productid']);
if($id > 0){
    
    $res = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>MAIN_CATALOG_ID, 'ID' => $id));
    if($ob = $res->GetNextElement()){
        $props = $ob->GetProperties();
        //o($props);
        $arSelect = Array("ID", "NAME", "CODE");
        $arFilter = Array(
            "IBLOCK_CODE"=>'tablet_deal_bp', 
            "ACTIVE_DATE"=>"Y", 
            "ACTIVE"=>"Y",
            "PROPERTY_BL_LVL" => $props['LEVEL']['VALUE'] ? $props['LEVEL']['VALUE'] : false,
            "PROPERTY_BL_SUBLVL" => $props['SUBLEVEL']['VALUE'] ? $props['SUBLEVEL']['VALUE'] : false,
            "PROPERTY_BL_NUM_SMSTR" => $props['SEMESTR']['VALUE'] ? $props['SEMESTR']['VALUE'] : false,
            "PROPERTY_BL_CNT_SMSTR" => $props['DURATION']['VALUE'] ? $props['DURATION']['VALUE'] : false,
            "PROPERTY_BL_CLSS" => $props['CLASS']['VALUE'] ? $props['CLASS']['VALUE'] : false,
            //"PROPERTY_BL_CTGR" => $props['ADDITIONAL_CATEGORY']['VALUE'] ? $props['ADDITIONAL_CATEGORY']['VALUE'] : false,
            "PROPERTY_BL_PRGM" => $props['TRAININGPROGRAM']['VALUE'] ? $props['TRAININGPROGRAM']['VALUE'] : false,
            "PROPERTY_BL_FLL" => $props['FILIAL']['VALUE'] ? $props['FILIAL']['VALUE'] : false,
            );
        //echo '<!--'.var_export($arFilter,true).'-->'; 
                
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob2 = $res->Fetch()){
            $arResult['WF_DOC'][$ob2['ID']] = $ob2;
        }
        
        if(count($arResult['WF_DOC'])){
            
?>
			<p>Выберите категорию договора:</p>
			<div>
                            <?foreach($arResult['WF_DOC'] as $wf_doc){?>
				<a href="javascript:void(0)" bpId="<?=$wf_doc['CODE']?>"><?=$wf_doc['NAME']?></a><br />
                            <?}?>
			</div>    
<?            
        }
        else{
            echo '<div style="color:#ff0000;">Не найдено соответствующего договора</div>';
        }
    }
    else{
        echo '<div style="color:#ff0000;">Не найден продукт</div>'; 
    }
}
else{
    echo '<div style="color:#ff0000;">Не выбран продукт</div>';
}
/*

//                dataPrint[""] = $('#PROPERTY_LEVEL').val();
//		dataPrint["B"] = $('#').val();
//                dataPrint[""] = $('#PROPERTY_SEMESTR').val();
//		dataPrint[""] = $('#qqq').val();
//                dataPrint[""] = $('#PROPERTY_CLASS').val();
//		dataPrint[""] = $('#PROPERTY_ADDITIONAL_CATEGORY').val();
//                dataPrint[""] = $('#filials').val();

 [Budget] => Array
        (
            [ID] => 339
            [TIMESTAMP_X] => 2015-05-21 16:47:11
            [IBLOCK_ID] => 21
            [NAME] => Budget (Бюджет)
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => Budget
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => S
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 325
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 0
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => N
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216268
            [VALUE] => 2
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 2
            [~DESCRIPTION] => 
            [~NAME] => Budget (Бюджет)
            [~DEFAULT_VALUE] => 
        )

    [Price] => Array
        (
            [ID] => 343
            [TIMESTAMP_X] => 2015-05-21 16:47:11
            [IBLOCK_ID] => 21
            [NAME] => Price (Ставка)
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => Price
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => S
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 329
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 0
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => N
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216269
            [VALUE] => 26000
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 26000
            [~DESCRIPTION] => 
            [~NAME] => Price (Ставка)
            [~DEFAULT_VALUE] => 
        )

    [Plan] => Array
        (
            [ID] => 344
            [TIMESTAMP_X] => 2015-05-21 16:47:11
            [IBLOCK_ID] => 21
            [NAME] => Plan (План)
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => Plan
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => S
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 330
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 0
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => N
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216270
            [VALUE] => 10
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 10
            [~DESCRIPTION] => 
            [~NAME] => Plan (План)
            [~DEFAULT_VALUE] => 
        )

    [FACULTY] => Array
        (
            [ID] => 345
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Факультет
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => FACULTY
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 331
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 65
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216271
            [VALUE] => 41439
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 41439
            [~DESCRIPTION] => 
            [~NAME] => Факультет
            [~DEFAULT_VALUE] => 
        )

    [ACADEMIC_YEAR] => Array
        (
            [ID] => 346
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Учебный год
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => ACADEMIC_YEAR
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 332
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 68
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216272
            [VALUE] => 41548
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 41548
            [~DESCRIPTION] => 
            [~NAME] => Учебный год
            [~DEFAULT_VALUE] => 
        )

    [ADDITIONAL_CATEGORY] => Array
        (
            [ID] => 347
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Дополнительная категория
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => ADDITIONAL_CATEGORY
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 333
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 64
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216273
            [VALUE] => 40429
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 40429
            [~DESCRIPTION] => 
            [~NAME] => Дополнительная категория
            [~DEFAULT_VALUE] => 
        )

    [COURSES] => Array
        (
            [ID] => 348
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Курсы
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => COURSES
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 334
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 69
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216274
            [VALUE] => 39863
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 39863
            [~DESCRIPTION] => 
            [~NAME] => Курсы
            [~DEFAULT_VALUE] => 
        )

    [DURATION] => Array
        (
            [ID] => 349
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Продолж.
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => DURATION
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 335
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 66
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216275
            [VALUE] => 41213
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 41213
            [~DESCRIPTION] => 
            [~NAME] => Продолж.
            [~DEFAULT_VALUE] => 
        )

    [FILIAL] => Array
        (
            [ID] => 350
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Филиал
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => FILIAL
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 336
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 60
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216276
            [VALUE] => 41282
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 41282
            [~DESCRIPTION] => 
            [~NAME] => Филиал
            [~DEFAULT_VALUE] => 
        )

    [SPECIALITY] => Array
        (
            [ID] => 351
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Специальность
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => SPECIALITY
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 337
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 62
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216277
            [VALUE] => 40704
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 40704
            [~DESCRIPTION] => 
            [~NAME] => Специальность
            [~DEFAULT_VALUE] => 
        )

    [SPECIALIZATION] => Array
        (
            [ID] => 352
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Специализация
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => SPECIALIZATION
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 338
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 63
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 
            [VALUE] => 
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 
            [~DESCRIPTION] => 
            [~NAME] => Специализация
            [~DEFAULT_VALUE] => 
        )

    [TRAININGPROGRAM] => Array
        (
            [ID] => 353
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Учебная программа
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => TRAININGPROGRAM
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 339
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 61
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216278
            [VALUE] => 41525
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 41525
            [~DESCRIPTION] => 
            [~NAME] => Учебная программа
            [~DEFAULT_VALUE] => 
        )

    [TYPESOFPROGRAMS] => Array
        (
            [ID] => 354
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Тип программы
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => TYPESOFPROGRAMS
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 340
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 67
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 
            [VALUE] => 
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 
            [~DESCRIPTION] => 
            [~NAME] => Тип программы
            [~DEFAULT_VALUE] => 
        )

    [UNIT] => Array
        (
            [ID] => 355
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Единица
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => UNIT
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 341
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 72
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216279
            [VALUE] => 40236
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 40236
            [~DESCRIPTION] => 
            [~NAME] => Единица
            [~DEFAULT_VALUE] => 
        )

    [LEVEL] => Array
        (
            [ID] => 356
            [TIMESTAMP_X] => 2015-06-03 15:54:59
            [IBLOCK_ID] => 21
            [NAME] => Уровень образования
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => LEVEL
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 345
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 71
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216280
            [VALUE] => 40454
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 40454
            [~DESCRIPTION] => 
            [~NAME] => Уровень образования
            [~DEFAULT_VALUE] => 
        )

    [SUBLEVEL] => Array
        (
            [ID] => 357
            [TIMESTAMP_X] => 2015-06-03 15:54:59
            [IBLOCK_ID] => 21
            [NAME] => Подуровень образования
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => SUBLEVEL
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 346
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 74
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216281
            [VALUE] => 40460
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 40460
            [~DESCRIPTION] => 
            [~NAME] => Подуровень образования
            [~DEFAULT_VALUE] => 
        )

    [SEMESTR] => Array
        (
            [ID] => 358
            [TIMESTAMP_X] => 2015-05-29 15:34:13
            [IBLOCK_ID] => 21
            [NAME] => Семестр
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => SEMESTR
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 347
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 73
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216282
            [VALUE] => 10961
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 10961
            [~DESCRIPTION] => 
            [~NAME] => Семестр
            [~DEFAULT_VALUE] => 
        )

    [FORMOFLEARNING] => Array
        (
            [ID] => 359
            [TIMESTAMP_X] => 2015-06-03 15:41:35
            [IBLOCK_ID] => 21
            [NAME] => Форма обучения
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => FORMOFLEARNING
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 348
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 70
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => Y
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216283
            [VALUE] => 40450
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 40450
            [~DESCRIPTION] => 
            [~NAME] => Форма обучения
            [~DEFAULT_VALUE] => 
        )

    [CLASS] => Array
        (
            [ID] => 399
            [TIMESTAMP_X] => 2015-06-03 18:06:38
            [IBLOCK_ID] => 21
            [NAME] => Класс
            [ACTIVE] => Y
            [SORT] => 500
            [CODE] => CLASS
            [DEFAULT_VALUE] => 
            [PROPERTY_TYPE] => E
            [ROW_COUNT] => 1
            [COL_COUNT] => 30
            [LIST_TYPE] => L
            [MULTIPLE] => N
            [XML_ID] => 
            [FILE_TYPE] => 
            [MULTIPLE_CNT] => 5
            [TMP_ID] => 
            [LINK_IBLOCK_ID] => 82
            [WITH_DESCRIPTION] => N
            [SEARCHABLE] => N
            [FILTRABLE] => N
            [IS_REQUIRED] => N
            [VERSION] => 1
            [USER_TYPE] => 
            [USER_TYPE_SETTINGS] => 
            [HINT] => 
            [PROPERTY_VALUE_ID] => 5216284
            [VALUE] => 39854
            [DESCRIPTION] => 
            [VALUE_ENUM] => 
            [VALUE_XML_ID] => 
            [VALUE_SORT] => 
            [~VALUE] => 39854
            [~DESCRIPTION] => 
            [~NAME] => Класс
            [~DEFAULT_VALUE] => 
        ) 

 */