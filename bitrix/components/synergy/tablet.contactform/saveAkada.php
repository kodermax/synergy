<?require($_SERVER["DOCUMENT_ROOT"]."/crm/deal/def.ini.php");?><?
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("crm");

ini_set('default_socket_timeout', 600);

// PROD $wsdl='http://sok-1c-app01/ws/sugarcrm.1cws?wsdl';
//$wsdl='http://sok-devapp/ws/sugarcrm.1cws?wsdl';
//@$client=new SoapClient($wsdl,array('trace'=>1,'cache_wsdl'=>1,'encoding'=>'UTF-8','check_int_arguments'=>1,'login'=>'iis1','password'=>'iis'));
//var_dump($client->GetTypesOfAdmissionTests());
//var_dump($client->GetDisciplinesOfAdmissionTests());
//die;
//
//var_dump($client->GetTypeIdentityDocuments());
//die;
//var_dump($client->GetForeignLanguage());
//die;
# var_dump($client->GetSubLevels());
# die;
//var_dump($client->GetEducationalInstitutions());
//die;
//var_dump($client->GetTypsDocumentOfEducation());
//die;

//var_dump($client->GetClasses());
//var_dump($client->GetFormsOfLearning());
//die;
//var_dump($client->GetLevels());
//die;
//var_dump($client->GetUnits());
// die;
 //var_dump($client->GetCNP());
 //die;
//var_dump($client->GetCitizenship());
// die();
// var_dump($client->GetTypesOfContactInformation());
// die;
$dealId = isset($_REQUEST['dealId']) ? intval($_REQUEST['dealId']) : 0;
// Deal
//$dealId = 44068;
//$dealId = 44070;


$arResult["DEAL_FIELDS"] = SynergyDeal::getSynergyDealById($dealId);

$arResult["CONTACT_FIELDS"] = SynergyContact::getSynergyContactById($arResult["DEAL_FIELDS"]["CONTACT_ID"]);

//echo $arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOMOTHER];
//var_dump($arResult);
//die;

$arFilter = Array(
    "ID" => $dealId
);
$objLead = CCrmDeal::GetList(false, $arFilter, Array(), 1);
if ($arrLead = $objLead->Fetch()) {
    $arFields["DEAL"] = $arrLead;
}
$dbResMultiFields = CCrmFieldMulti::GetList(array(
    'ID' => 'asc'
), array(
    'ENTITY_ID' => 'DEAL',
    'ELEMENT_ID' => $dealId
));
$arFields['FM'] = array();
while ($arMultiFields = $dbResMultiFields->Fetch()) {
    $arFields['FM'][$arMultiFields["TYPE_ID"]][$arMultiFields["ID"]] = $arMultiFields;
}
$arResult["DEAL"] = array_merge($arFields["DEAL"], $arFields['FM']);
$deal = $arResult["DEAL"];

//var_dump($deal);
//die();
$arFilter = Array(
    "ID" => $deal["CONTACT_ID"]
);
$arrContacte = array();
$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_FOREIGNLANGUAGE])."' LIMIT 50";
//var_dump($sql);
$res= $DB->Query($sql);
if ($arr = $res->Fetch()) {
    $arrContacte['GUID']=$arr['VALUE'];
}
//var_dump($arrContacte);
//die;
// Contact
$objContact = CCrmContact::GetList(false, $arFilter, Array(), 1);
if ($arrContact = $objContact->Fetch()) {
    $arFields["CONTACT"] = $arrContact;
}
//var_dump($arFields['CONTACT']);
//die;


$dbResMultiFields = CCrmFieldMulti::GetList(array(
    'ID' => 'asc'
), array(
    'ENTITY_ID' => 'CONTACT',
    'ELEMENT_ID' => $deal["CONTACT_ID"]
));
//$arFields['CONTACT_FM'] = array();
// TODO получать GUID контакты из АКАДА пока хардкод основных
/*
 * ["GUID"]=>
 * string(36) "650b2aa6-407d-11df-8bc1-00304863c48b"
 * ["Name"]=>
 * string(33) "Телефон мобильный"
 * ["CodeType"]=>
 * int(2)
 * ["GUID"]=>
 * string(36) "0f22a2c9-0baa-11e1-a253-00304863c48b"
 * ["Name"]=>
 * string(63) "Адрес электронной почты слушателя"
 * ["CodeType"]=>
 * int(3)
 */
$arrAkada_Contacts = array();
while ($arMultiFields = $dbResMultiFields->Fetch()) {
    // $arFields['CONTACT_FM'][$arMultiFields["TYPE_ID"]][$arMultiFields["ID"]] = $arMultiFields;
   // var_dump($arMultiFields);
    switch ($arMultiFields['COMPLEX_ID']) {
        case 'EMAIL_WORK':
            $arrAkada_Contacts[] =  (object)  array(
           'GUIDTypeOfContactInformation'=>'0f22a2c9-0baa-11e1-a253-00304863c48b',
           'Value'=>$arMultiFields['VALUE'],
           'CodeType'=>3
            );
            break;

        case 'EMAIL_HOME':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'bc04c22e-7b1d-11dc-8981-003048347044',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>3
            );
            break;

        case 'EMAIL_OTHER':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'bc04c22e-7b1d-11dc-8981-003048347044',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>3
            );
            break;

        case 'PHONE_MOBILE':
            $arrAkada_Contacts[] = (object) array(
                'GUIDTypeOfContactInformation'=>'650b2aa6-407d-11df-8bc1-00304863c48b',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>2
            );

            break;

        case 'PHONE_HOME':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'772b1676-12f9-11de-b67d-00304863c48b',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>2
            );
            break;

        case 'PHONE_OTHER':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'bbd6115c-4bf8-11dc-ae47-000423c8668c',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>2
            );
            break;

        case 'PHONE_WORK':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'7b331d53-84dd-11df-8409-00304863c476',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>2
            );
            break;
    }

}
//die();
$arrAkada = array();
$arrAkada['LastName'] = $arFields['CONTACT']['LAST_NAME'];
$arrAkada['FirstName'] = $arFields['CONTACT']['NAME'];
$arrAkada['MiddleName'] = $arFields['CONTACT']['SECOND_NAME'];
$arrAkada['Sex'] = $arFields['CONTACT'][PROP_CONTACT_GENDER]; // 'f';
switch ($arFields['CONTACT'][PROP_CONTACT_GENDER]) {
    case 201:
        $arrAkada['Sex'] = 'm';
        break;
    case 202:
        $arrAkada['Sex'] = 'f';
        break;
}
// Birthdate
$date = new DateTime($arFields['CONTACT']['BIRTHDATE']);
$arrAkada['DateOfBirth'] = $date->format('Y-m-d'); // '1980-12-13';
$arrAkada['Contacts'] = $arrAkada_Contacts;


$arrAkada['FIOMother'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOMOTHER];
$arrAkada['PhoneMother'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEMOTHER];
$arrAkada['FIOFather'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOFATHER];
$arrAkada['PhoneFather'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEFATHER];


$arrPasspot = array();
$arrPasspot['Series']=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_SERIA];
$arrPasspot['Number']=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_NUMBER];
$arrPasspot['Issued']=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_KEMVIDAN];
$arrPasspot['CodeDivision']=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_CODEPODRAZDEL];
$date = new DateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_DATEISSUE]);
$arrPasspot['DateIssued'] = $date->format('Y-m-d');

$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPEIDENTITYDOCUMENTS])."' LIMIT 1";
$res= $DB->Query($sql);
if ($arr = $res->Fetch()) {
    $arrPasspot['GUIDTypeIdentityDocument']=$arr['VALUE'];

} else {
    $arrPasspot['GUIDTypeIdentityDocument']='';
}
if (strlen($arrPasspot['Number'])>0) {
    $arrAkada['Passport'] = (object)$arrPasspot;
}

//var_dump($arrPasspot);

$arrForeignLanguages = array();
$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_FOREIGNLANGUAGE])."' LIMIT 1";
$res= $DB->Query($sql);
if ($arr = $res->Fetch()) {
    $arrForeignLanguages['GUID']=$arr['VALUE'];
}


$arrAkada['ForeignLanguages'] = (object) $arrForeignLanguages;
//var_dump($arrAkada['ForeignLanguages']);
//die;

//var_dump($arrForeignLanguages);
//die;
//var_dump($arResult["CONTACT_FIELDS"][PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID]);
//die;
$arrEducationalInstitution = array();
$sql = "SELECT VALUE, IBLOCK_PROPERTY_ID FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID])."' LIMIT 2";
$res= $DB->Query($sql);
//var_dump($sql);

while ($arr = $res->Fetch()) {
   // var_dump($arr);
    switch ($arr['IBLOCK_PROPERTY_ID']){
        case 404:
            $arrEducationalInstitution['GUID']=$arr['VALUE'];
            break;
        case 405:
            $arrEducationalInstitution['Type']=$arr['VALUE'];
            break;
    }

}
//var_dump($arrEducationalInstitution);
//die;

$arrAkada['EducationalInstitution'] = (object) $arrEducationalInstitution;


$arrDocumentOfEducation = array();
$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPSDOCUMENTOFEDUCATION])."' LIMIT 1";
$res= $DB->Query($sql);
//var_dump($sql);
//die;
while ($arr = $res->Fetch()) {
    $arrDocumentOfEducation['GUIDTypeDocumentOfEducation']=$arr['VALUE'];
}


$arrDocumentOfEducation["Series"]=$arResult["DEAL_FIELDS"][PROP_DEAL_SERIA_DO];
$arrDocumentOfEducation["Number"]=$arResult["DEAL_FIELDS"][PROP_DEAL_NUMBER_DO];
if (strlen($arResult["DEAL_FIELDS"][PROP_DEAL_DATE_DO])>0) {
    $date = new DateTime($arResult["DEAL_FIELDS"][PROP_DEAL_DATE_DO]);
    $arrDocumentOfEducation["DateIssued"] = $date->format('Y-m-d');
}
$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_SPECIALITYSBEFOREENTERING])."' LIMIT 1";
$res= $DB->Query($sql);
while ($arr = $res->Fetch()) {
    $arrDocumentOfEducation['GUIDSpecialityBeforeEntering']=$arr['VALUE'];
}

//var_dump($arrDocumentOfEducation);
//die;
$arrAkada['DocumentOfEducation'] = (object) $arrDocumentOfEducation;


//Плательщик

if (strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_LAST_NAME])>0) {
    $arrPayerFL = array();
    $arrPayerFL['LastName'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_LAST_NAME];
    $arrPayerFL['FirstName'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_NAME];
    $arrPayerFL['MiddleName'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_SECOND_NAME];
    // дефолт F, нет строки пола для нового плательщика
    //TODO добавить поле Sex и GUIDTypeIdentityDocument
    $arrPayerFL['Sex'] = 'f';
    if (strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_BIRTHDAY]) > 0) {
        $date = new DateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_BIRTHDAY]);
        $arrPayerFL["DateOfBirth"] = $date->format('Y-m-d');
    }
//var_dump($arrPayerFL);
//die;
    $arrPayerFL_passport = array();
    $arrPayerFL_passport['Series'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_SERIA];
    $arrPayerFL_passport['Number'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_NUMBER];
    $arrPayerFL_passport['Issued'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN];
    $arrPayerFL_passport['CodeDivision'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL];
   if (strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_DATEISSUE]) > 0) {
        $date = new DateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_DATEISSUE]);
        $arrPayerFL_passport['DateIssued'] = $date->format('Y-m-d');
    }
    //TODO надо добавить пункт выбора "Тип документа удостоверяющего личность" для плательщика.
    //Установлен гуид от выбора документа поступающего
    $arrPayerFL_passport['GUIDTypeIdentityDocument'] = $arrPasspot['GUIDTypeIdentityDocument'];

    $arrPayerFL['Passport'] = (object) $arrPayerFL_passport;

//var_dump($arrPayerFL_passport);
//die;

//БРЕД, но работает. Огромная вложенность, не катит.
//ДА, знаю, что не красиво и не рационально.
   $arrPayerFL_contact = array();

    if( strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PHONE])>0 ) {
        $arrPayerFL_contact[] = (object)array(
            'GUIDTypeOfContactInformation' => '650b2aa6-407d-11df-8bc1-00304863c48b',
            'Value' => $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PHONE],
            'CodeType' => 3
         );
        if( strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL])>0 ) {
            $arrPayerFL_contact[] = (object)array(
                'GUIDTypeOfContactInformation' => '0f22a2c9-0baa-11e1-a253-00304863c48b',
                'Value' => $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL],
                'CodeType' => 2
            );
        }
    }
    elseif ( strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL])>0 ){
            $arrPayerFL_contact[] = (object)array(
                'GUIDTypeOfContactInformation' => '0f22a2c9-0baa-11e1-a253-00304863c48b',
                'Value' => $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL],
                'CodeType' => 2
            );
    }
    else {
        $arrPayerFL_contact[] = (object)array(
            'GUIDTypeOfContactInformation' => '650b2aa6-407d-11df-8bc1-00304863c48b',
            'Value' => " ",
            'CodeType' => 3
        );
    }

//var_dump($arrPayerFL_contact);
//    die;


    //var_dump($arrPayerFL_contact);

    $arrPayerFL['Contacts'] = $arrPayerFL_contact;
    //$arrPayerFL['Contacts'] = $arrAkada_Contacts;
    //var_dump($arrPayerFL);
   // die;

    if (strlen($arrPayerFL['LastName'])>0) {
           $arrAkada['PayerFL'] = (object) $arrPayerFL;
    }


}

//var_dump($arrPayerFL);
// die;

//Совсем грустно:
/*if ($arrAkada['DateOfBirth']===$arrPayerFL["DateOfBirth"]){
    $arrPlayerFL = array_merge($arrPayerFL,$arrPasspot,$arrAkada_Contacts);

    var_dump($arrPlayerFL);
}
*/


$arrAkada['GUIDCitizenship'] = '';
$arrAkada['ContractNumber'] = $arFields['CONTACT']['ID'];
$arrAkada['ContractDate'] = date('Y-m-d',strtotime($arFields['CONTACT']['DATE_CREATE']));
$CitizenshipId=$arFields['CONTACT'][PROP_CONTACT_CITIZENSHIP];

//var_dump($arResult["CONTACT_FIELDS"][PROP_CONTACT_RESULTSOFADMISSIONTESTS]);
//var_dump($arResult["CONTACT_FIELDS"]);

//die;

// Манипуляции с элементами Highload
CModule::IncludeModule('highloadblock');
$rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('ID'=>'3')));
//var_dump($rsData);
if ( !($arData = $rsData->fetch()) ){
    echo 'Инфоблок не найден';
}
$Entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arData);
//var_dump($Entity);
$Query = new \Bitrix\Main\Entity\Query($Entity);
$Query->setSelect(array('*'));
$Query->setFilter(array('UF_CONTACT_ID'=> $arResult["DEAL_FIELDS"]["CONTACT_ID"]));

$result = $Query->exec();

$result = new CDBResult($result);
$arLang = array();
while ($row = $result->Fetch()){
    $arLang[$row['ID']] = $row;
    //var_dump($arLang);
}
//die;
//Собираем нужный
//var_dump($arLang);
$arAdmissionTest = array();

foreach($arLang as $key1 => $value1) {

    //CodeType
    $CodeTypeArr = array (
    "ЕГЭ"=>1,
    "Внутренее вступительное испытание"=>2,
    "Собеседование"=>3,
    "Олимпиада"=>4,
    "Стартовое"=>5,
    "Промежуточное"=>6,
    "Итоговое"=>7
);
    $sql = "SELECT NAME FROM `b_iblock_element` WHERE `ID` = '".(int)$value1['UF_CODE_TYPE']."' LIMIT 1";
    $res= $DB->Query($sql);
    $CodeType='';
    //var_dump($sql);
    //die;
    if ($arr = $res->Fetch()) {
        if (isset($CodeTypeArr[$arr['NAME']])) {
        $CodeType=$CodeTypeArr[$arr['NAME']];
        }
    }

    //GUIDDiscipline
    $sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".$value1['UF_GUID_DISCIPLINE']."' LIMIT 1";
    $res= $DB->Query($sql);
    if ($arr = $res->Fetch()) {
        $GUIDDiscipline=$arr['VALUE'];
    } else {
        $GUIDDiscipline='';
    }
    if($GUIDDiscipline){
        $arAdmissionTest[]= (object) array(
            "CodeType"       => $CodeType,
            "GUIDDiscipline" => $GUIDDiscipline,
            "DeliveryDate"   => date("Y-m-d",strtotime($value1['UF_DELIVERY_DATE'])),
            "CountOfPoints"  => (int)$value1['UF_COUNT_POINTS']
        );
    }

}
//var_dump($arAdmissionTest);
//die;
$arrAkada['AdmissionTests'] =  $arAdmissionTest;

// Hardcore & Sodomia
$sql = "
SELECT ip.code,iep.*
FROM `b_iblock_element_property` iep
JOIN b_iblock_property ip on (ip.ID=iep.IBLOCK_PROPERTY_ID)
WHERE `IBLOCK_ELEMENT_ID` = '".intval($CitizenshipId)."' ";
$res= $DB->Query($sql);
if ($arrCitizenship = $res->Fetch()) {
    $arrAkada['GUIDCitizenship']=$arrCitizenship['VALUE'];
}



//var_dump($arrAkada);

// Product
$totalInfo = CCrmProductRow::LoadRows('D', $dealId);
$productId=$totalInfo[0]['PRODUCT_ID'];
$price=$totalInfo[0]['PRICE'];
//var_dump($totalInfo);
//die;

$arSort = Array(
    "ID" => "ASC"
);

$arFilter = Array(
    "IBLOCK_ID" => MAIN_CATALOG_ID,
    "ID" => $productId
); // intval($_POST["productId"])

$arNav = Array(
    "nTopCount" => 1
);

$arSelect = Array(
    "ID",
    "NAME",
    "PROPERTY_FILIAL",
    "PROPERTY_ACADEMIC_YEAR",
    "PROPERTY_SEMESTR",
    "PROPERTY_LEVEL",
    "PROPERTY_SUBLEVEL",
    "PROPERTY_FACULTY",
    "PROPERTY_SPECIALITY",
    "PROPERTY_SPECIALIZATION",
    "PROPERTY_TRAININGPROGRAM",
    "PROPERTY_TYPESOFPROGRAMS",
    "PROPERTY_FORMOFLEARNING",
    "PROPERTY_DURATION",
    "PROPERTY_ADDITIONAL_CATEGORY",
    "PROPERTY_UNIT",
    "PROPERTY_COURSES",
    "PROPERTY_Budget",
    "PROPERTY_Plan",
    "PROPERTY_CLASS",
);

$objProduct = CIBlockElement::GetList($arSort, $arFilter, false, $arNav, $arSelect);
$arrProduct = $objProduct->Fetch();
//var_dump($arrProduct);
//die;

$CNP = array();

$CNP['GUIDFilial']='';

/*
<xsd:complexType name="AdmissionTest">
<xsd:sequence>
<xsd:element name="CodeType" type="xsd:byte"/>
<xsd:element name="GUIDDiscipline" type="xsd:string"/>
<xsd:element name="DeliveryDate" type="xsd:date"/>
<xsd:element name="CountOfPoints" type="xsd:int"/>
</xsd:sequence>
</xsd:complexType>
*/

/*
*'GUIDSpeciality'=>'4b3b7668-330a-11e2-812e-9c8e99fb2ee6', //'5f3c9765-5d3b-11dd-ade7-00304863c48b',
* 'GUIDTrainingProgram'=>'1ec2e40b-093a-11dc-842f-000423c8668c',
* 'GUIDFilial'=>'',
* 'CodeClass'=>'10',
* 'GUIDSpecialization'=>'',
* 'CodeLevel'=>'1',
* 'CodeSubLevel'=>'2',
* 'GUIDDuration'=>'93be5a9b-f0a0-11dd-a34c-00304863c477',
* 'GUIDAdditionalCategory'=>'',
* 'GUIDAcademicYear'=>'ce614358-b68f-11db-abaf-000423c8668c',
* 'Semester'=>'1',
* 'GUIDFaculty'=>'',
* 'GUIDTypesOfPrograms'=>'',
* 'Budget'=>'',
* 'GUIDUnit'=>'',
* 'GUIDCourses'=>'',
* 'CodeFormOfLearning'=>'',
* 'Price'=>'',
* 'Plan'=>'',
*/
$keyCNP = array (
'GUIDFilial'=>'PROPERTY_FILIAL_VALUE',
'GUIDSpeciality'=>'PROPERTY_SPECIALITY_VALUE',
'GUIDTrainingProgram'=>'PROPERTY_TRAININGPROGRAM_VALUE',
 'CodeClass'=>'PROPERTY_CLASS_VALUE',
 'GUIDSpecialization'=>'PROPERTY_SPECIALIZATION_VALUE',
 'CodeLevel'=>'PROPERTY_LEVEL_VALUE',
 'CodeSubLevel'=>'PROPERTY_SUBLEVEL_VALUE',
 'GUIDDuration'=>'PROPERTY_DURATION_VALUE',
 'GUIDAdditionalCategory'=>'PROPERTY_ADDITIONAL_CATEGORY_VALUE',
 'GUIDAcademicYear'=>'PROPERTY_ACADEMIC_YEAR_VALUE',
 'Semester' => 'PROPERTY_SEMESTR_VALUE',
 'GUIDFaculty'=>'PROPERTY_FACULTY_VALUE',
 'GUIDTypesOfPrograms'=>'PROPERTY_TYPESOFPROGRAMS_VALUE',
 'Budget' => '',
 'GUIDUnit'=> 'PROPERTY_UNIT_VALUE',
 'GUIDCourses'=> 'PROPERTY_COURSES_VALUE',
 'CodeFormOfLearning'=>'PROPERTY_FORMOFLEARNING_VALUE',
 'Price'=>'',
 'Plan'=>'',

);

// Hardcore & Sodomia

foreach ($keyCNP as $key=>$val) {
$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arrProduct[$val])."' LIMIT 1";
$res= $DB->Query($sql);
if ($arr = $res->Fetch()) {
    $CNP[$key]=$arr['VALUE'];
} else {
    $CNP[$key]='';
}
}

// Не работает выборка класса
//$CNP['CodeClass']='10';
//$ClassId=$arFields['DEAL'][PROP_DEAL_CLASS];
//echo '==>'.$ClassId.'<==';
$CNP['Price']=$price;
$CNP['Budget']=$arrProduct['PROPERTY_BUDGET_VALUE'];
$CNP['Plan']=$arrProduct['PROPERTY_PLAN_VALUE'];


//$ClassId=$arFields['DEAL'][PROP_DEAL_CLASS];
// Hardcore & Sodomia
/*
$sql = "
SELECT ip.code,iep.*
FROM `b_iblock_element_property` iep
JOIN b_iblock_property ip on (ip.ID=iep.IBLOCK_PROPERTY_ID)
WHERE `IBLOCK_ELEMENT_ID` = '".intval($ClassId)."' ";
//echo $sql;
$res= $DB->Query($sql);
if ($arrClass = $res->Fetch()) {
    //var_dump($res);
    $CNP['CodeClass']=$arrClass['VALUE'];
}
*/


 //die;

//var_dump($arrAkada);
//die;
$arrAkada['CNP'] = (object) $CNP;
//var_dump($arrAkada);
//die;
  // PROD $wsdl='http://sok-1c-app01/ws/sugarcrm.1cws?wsdl';
//o($arrAkada);

try {
  //$wsdl='http://sok-devapp/ws/sugarcrm.1cws?wsdl';
    $wsdl='http://sok-devapp/ws/bitrixcrm.1cws?wsdl';
    @$client=new SoapClient($wsdl,array('trace'=>1,'cache_wsdl'=>0,'encoding'=>'UTF-8','check_int_arguments'=>1,'login'=>'iis1','password'=>'iis'));


    
    $lastInfoAkada = $client->ImportClientWithPreliminaryAgreement(
         array(
           'InputData'=>(object) $arrAkada

         )
         );
    
    @mkdir($_SERVER["DOCUMENT_ROOT"].'/local/logs');
    @mkdir($_SERVER["DOCUMENT_ROOT"].'/local/logs/akada');
    $text = "================================================\r\n";
    $text .= date('Y.m.d_H:i:s')."Отправка данных в акаду\r\n";
    $text .= "==Отправили\r\n";
    $text .= var_export($arrAkada,true);
    $text .= "==Получили\r\n";
    $text .= var_export($lastInfoAkada,true);
    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/local/logs/akada/akada.log', $text, FILE_APPEND);
    
    foreach ($lastInfoAkada as $values) {
        $deal = new CCrmDeal;
        $contact = new CCrmContact;

        $arDealParams = Array(
            AKADA_DATA_SYNC => date("d.m.Y"),
            AKADA_SYNC => 1,
            AKADA_GUID => $values->GUIDEntrant,
            STAGE_ID => AKADA_DEAL_STAGE_ID
        );

        $deal->Update($dealId , $arDealParams, false, false);

        $contactId = $arResult["DEAL_FIELDS"]["CONTACT_ID"];
        $arContactParams = Array(
            AKADA_CONTACT_GUID => $values->GUIDFL,
        );
        $contact->Update($contactId , $arContactParams, false, false);

        $error = 0;
        $akadaMessage = $values->Message;
        if(strlen($akadaMessage) > 0){
            $response = true;
        }else{
            $response = false;
        }

        $arResponse = Array(
            "response" => $response,
            "message" => $akadaMessage,
            "error" => $error
        );

        echo json_encode($arResponse);
   }

    } catch ( SoapFault $e ) { // Do NOT try and catch "Exception" here
        //o($lastInfoAkada);
        
         echo 'sorry... our service is down';
         //var_dump($e->getCode());
         //o($e->getMessage());
         switch ($e->getMessage()) {
             case 'SOAP-ERROR: Encoding: object has no \'DateIssued\' property':
                  echo 'Не заполнено дата выдачи';
                 break;

                 case 'SOAP-ERROR: Encoding: object has no \'GUIDSpecialityBeforeEntering\' property':
                     echo 'Не заполнено специальность перед поступлением';
                     break;


         }
     }

 //echo "Ответ:\n" . $client->__getLastResponse() . "\n";
/*
 * //var_dump($client->__getFunctions());
 * //var_dump($client->__getTypes());
 * //die;
 * // Специальности
 * //var_dump($client->GetSpecialitys());
 * // Программы
 * // Очная 1ec2e40b-093a-11dc-842f-000423c8668c
 * //var_dump($client->GetTrainingPrograms());
 * // Уровень
 * // ВПО
 * //var_dump($client->GetLevels());
 * // 93be5a9b-f0a0-11dd-a34c-00304863c477
 * //var_dump($client->GetDurations());
 * // ce614358-b68f-11db-abaf-000423c8668c
 * //var_dump($client->GetAcademicYears());
 * // Подуровень
 * //var_dump($client->GetSublevels());
 * //var_dump($client->GetClasses());
 * //var_dump($client->GetTypesOfContactInformation());
 * //var_dump($client->GetCitizenship());
 * //die;
 *
 * var_dump($client->ImportClientWithPreliminaryAgreement(array(
 * 'InputData'=>(object)array(
 * 'LastName'=>'Ивановкин',
 * 'FirstName'=>'Иванко',
 * 'MiddleName'=>'Иванович',
 * 'Sex'=>'f',
 * 'DateOfBirth'=>'1980-12-12',
 * 'Contacts'=>array(
 * (object)array('GUIDTypeOfContactInformation'=>'7da9456a-95f0-11de-82cc-00304863c476','Value'=>'1234567','CodeType'=>2),
 * ),
 * 'CNP'=>array(
 * (object)array(
 * 'GUIDSpeciality'=>'4b3b7668-330a-11e2-812e-9c8e99fb2ee6', //'5f3c9765-5d3b-11dd-ade7-00304863c48b',
 * 'GUIDTrainingProgram'=>'1ec2e40b-093a-11dc-842f-000423c8668c',
 * 'GUIDFilial'=>'',
 * 'CodeClass'=>'10',
 * 'GUIDSpecialization'=>'',
 * 'CodeLevel'=>'1',
 * 'CodeSubLevel'=>'2',
 * 'GUIDDuration'=>'93be5a9b-f0a0-11dd-a34c-00304863c477',
 * 'GUIDAdditionalCategory'=>'',
 * 'GUIDAcademicYear'=>'ce614358-b68f-11db-abaf-000423c8668c',
 * 'Semester'=>'1',
 * 'GUIDFaculty'=>'',
 * 'GUIDTypesOfPrograms'=>'',
 * 'Budget'=>'',
 * 'GUIDUnit'=>'',
 * 'GUIDCourses'=>'',
 * 'CodeFormOfLearning'=>'',
 * 'Price'=>'',
 * 'Plan'=>'',
 * ),
 * ),
 * 'GUIDCitizenship' => 'ee322fb4-36e2-11dd-802f-00304863c48b'
 * )
 * )));
 */
?>