<?require($_SERVER["DOCUMENT_ROOT"]."/crm/deal/def.ini.php");?>
<?
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');
CModule::IncludeModule('highloadblock');

global $APPLICATION;
global $ID;

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$arrAdmissionTest_content = array(
    'UF_CONTACT_ID' => isset($_POST['CONTACT_ID']) ? intval($_POST['CONTACT_ID']) : 0,
    'UF_CODE_TYPE' => '',
    'UF_DELIVERY_DATE' => '',
    'UF_COUNT_POINTS' => '',
    'UF_GUID_DISCIPLINE' => ''
);
$hlblock = HL\HighloadBlockTable::getById(3)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);

$entity_data_class = $entity->getDataClass();
$result = $entity_data_class::add($arrAdmissionTest_content);
$error='';

if($_POST['CODE_TYPE'] == EGE_TYPE){
    switch ($_POST['GUID_DISCIPLINE']) {
        case EGE_RUSSIAN:
            $utm_f = PROP_CONTRACT_BALLRUSSIAN;
        break;
        case EGE_MATH:
            $utm_f = PROP_CONTRACT_BALLMATH;
        break;
        case EGE_OBSHTESTV:
            $utm_f = PROP_CONTRACT_BALLOBSHTESTV;
        break;
        case EGE_HISTORY:
            $utm_f = PROP_CONTRACT_BALLHISTORY;
        break;

    }
    
    $contact = new CCrmContact;
    $arParams = Array(
            $utm_f => intval($_POST['COUNT_POINTS'])
    );
    
    $contact->Update($_POST['CONTACT_ID'], $arParams, true, true);
    $error = $contact->LAST_ERROR;        
}


if (! $result->isSuccess()) {
    //$APPLICATION->ThrowException(var_dump($result->getErrors()));
   $error= $result->getErrors();
    $response = false;
}
$ID = $result->getId();
if ($ID>0) $response=true;
$arResponse = Array(
    "response" => $response,
    "ID"  => $ID,
    "error" => $error
);

echo json_encode($arResponse);
