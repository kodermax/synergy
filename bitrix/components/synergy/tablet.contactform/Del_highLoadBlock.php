<?require($_SERVER["DOCUMENT_ROOT"]."/crm/deal/def.ini.php");?><?
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');
CModule::IncludeModule('highloadblock');

global $APPLICATION;
//global $ID;

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
/*
$arrAdmissionTest_content = array(
    'UF_CONTACT_ID' => isset($_POST['CONTACT_ID']) ? intval($_POST['CONTACT_ID']) : 0,
    'UF_CODE_TYPE' => '',
    'UF_DELIVERY_DATE' => '',
    'UF_COUNT_POINTS' => '',
    'UF_GUID_DISCIPLINE' => ''
);
*/

$hlblock = HL\HighloadBlockTable::getById(3)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
//TODO добавить проверку прав что ID принадлежит к CONTACT_ID
$result = $entity_data_class::Delete($_REQUEST['ID']);

$error='';
if (! $result->isSuccess()) {
    //$APPLICATION->ThrowException(var_dump($result->getErrors()));
    $error= $result->getErrors();
    $response = false;
} else {
    $response = true;
}

$arResponse = Array(
    "response" => $response,
    "error" => $error
);

echo json_encode($arResponse);

