<form method="post">

	<label>Форматы даты для формирования отчета:</label><br />

    <input type="radio" <?if($_POST["typeDate"] == "day"):?>checked<?endif?> name="typeDate" value="day"> День
    <input type="radio" <?if($_POST["typeDate"] == "week"):?>checked<?endif?> name="typeDate" value="week"> Неделя
    <input type="radio" <?if($_POST["typeDate"] == "month"):?>checked<?endif?> name="typeDate" value="month"> Месяц
    <input type="radio" <?if($_POST["typeDate"] == "year"):?>checked<?endif?> name="typeDate" value="year"> Год

    <br /><br />

    <input class="datepicker" type="text" name="dateFrom" placeholder="Начальная дата" value="<?=$_POST["dateFrom"]?>">
    <input class="datepicker" type="text" name="dateTo" placeholder="Конечная дата" value="<?=$_POST["dateTo"]?>">

    <br /><br />

	<label>Выберите подразделение:</label><br />
	<select size="10" name="idDepartment">
		<?foreach($arResult["DEPARTMENTS"]["FIRST_LEVEL"] as $arFirstLevel):?>
			<option <?if($arResult["FILTER"]["idDepartment"] == $arFirstLevel["ID"]):?>selected<?endif;?> value="<?=$arFirstLevel["ID"]?>">. <?=$arFirstLevel["NAME"]?></option>
			<?foreach($arFirstLevel["SECOND_LEVEL"] as $arSecondLevel):?>
				<option <?if($arResult["FILTER"]["idDepartment"] == $arSecondLevel["ID"]):?>selected<?endif;?> value="<?=$arSecondLevel["ID"]?>">. . <?=$arSecondLevel["NAME"]?></option>
				<?foreach($arSecondLevel["THERD_LEVEL"] as $arTherdLevel):?>
					<option <?if($arResult["FILTER"]["idDepartment"] == $arTherdLevel["ID"]):?>selected<?endif;?> value="<?=$arTherdLevel["ID"]?>">. . . <?=$arTherdLevel["NAME"]?></option>
					<?foreach($arTherdLevel["FOURS_LEVEL"] as $arFoursLevel):?>
						<option <?if($arResult["FILTER"]["idDepartment"] == $arFoursLevel["ID"]):?>selected<?endif;?> value="<?=$arFoursLevel["ID"]?>">. . . . <?=$arFoursLevel["NAME"]?></option>
					<?endforeach;?>
				<?endforeach;?>
			<?endforeach;?>
		<?endforeach;?>
	</select>

	<br /><br />

	<label>Выберите пользователя:</label><br />

	<input class="userName" name="userName" type="text" placeholder="Начните ввод" value="<?=$_POST["userName"]?>">

	<input class="userHidden" name="userId" type="hidden" value="<?=$_POST["userId"]?>">

	<br /><br />

	<input type="submit" value="Применить фильтр">

</form>




<script>
var userObj = {};
userObj["users"] = [];
<?$i=0;foreach($arResult["USERS"] as $arUser):?>
	userObj["users"][<?=$i?>] = {label: "<?=$arUser["NAME"]?> <?=$arUser["LAST_NAME"]?>", userId: <?=$arUser["ID"]?>};
<?$i++;endforeach;?>
</script>