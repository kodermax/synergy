<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("crm");
CModule::IncludeModule("iblock");


$arSort = Array("ID" => "ASC");

$arFilter = Array(
	"IBLOCK_ID" => 1,
	"DEPTH_LEVEL" => Array(1,2)
);

$arNav = Array(
	"nTopCount" => 5
);

$arSelect = Array("ID","NAME","IBLOCK_SECTION_ID","DEPTH_LEVEL");

$objDepartments = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
while($arrDepartments = $objDepartments->Fetch()){

	if($arrDepartments["DEPTH_LEVEL"] == 1){		$arResult["DEPARTMENTS"]["FIRST_LEVEL"][$arrDepartments["ID"]] = $arrDepartments;	}

	if($arrDepartments["DEPTH_LEVEL"] == 2){
		$arResult["DEPARTMENTS"]["FIRST_LEVEL"][$arrDepartments["IBLOCK_SECTION_ID"]]["SECOND_LEVEL"][$arrDepartments["ID"]] = $arrDepartments;
	}
}

foreach($arResult["DEPARTMENTS"]["FIRST_LEVEL"] as $arDepFirstLevel){	foreach($arDepFirstLevel["SECOND_LEVEL"] as $arSecondLevel){
		$arFilterTherd = Array(
			"DEPTH_LEVEL" => 3,
			"SECTION_ID" => $arSecondLevel["ID"]
		);

		$objDepartmentsTherd = CIBlockSection::GetList($arSort, $arFilterTherd, false, $arSelect);

		while($arrDepartmentsTherd = $objDepartmentsTherd->Fetch()){        	$arResult["DEPARTMENTS"]["FIRST_LEVEL"][$arDepFirstLevel["ID"]]["SECOND_LEVEL"][$arSecondLevel["ID"]]["THERD_LEVEL"][$arrDepartmentsTherd["ID"]] = $arrDepartmentsTherd;		}
	}}

foreach($arResult["DEPARTMENTS"]["FIRST_LEVEL"] as $arDepFirstLevel){
	foreach($arDepFirstLevel["SECOND_LEVEL"] as $arSecondLevel){
		foreach($arSecondLevel["THERD_LEVEL"] as $arTherdLevel){
			$arFilterTherd = Array(
				"DEPTH_LEVEL" => 4,
				"SECTION_ID" => $arTherdLevel["ID"]
			);

			$objDepartmentsFours = CIBlockSection::GetList($arSort, $arFilterTherd, false, $arSelect);

			while($arrDepartmentsFours = $objDepartmentsFours->Fetch()){
	        	$arResult["DEPARTMENTS"]["FIRST_LEVEL"][$arDepFirstLevel["ID"]]["SECOND_LEVEL"][$arSecondLevel["ID"]]["THERD_LEVEL"][$arTherdLevel["ID"]]["FOURS_LEVEL"][$arrDepartmentsFours["ID"]] = $arrDepartmentsFours;
			}
		}
	}
}


$arFilterUser = Array(
	"ACTIVE" => "Y"
);

$objUsers = CUser::GetList(($by="ID"), ($order="ASC"), $arFilterUser);

while($arrUsers = $objUsers->Fetch()){	$arResult["USERS"][$arrUsers["ID"]] = $arrUsers;}


if($_POST["idDepartment"]){	$arResult["FILTER"]["idDepartment"] = intval($_POST["idDepartment"]);}

if($_POST["typeDate"]){
	$arResult["FILTER"]["typeDate"] = $_POST["typeDate"];
}

if($_POST["dateFrom"]){
	$arResult["FILTER"]["dateFrom"] = $_POST["dateFrom"];
}

if($_POST["dateTo"]){
	$arResult["FILTER"]["dateTo"] = $_POST["dateTo"];
}

if($_POST["userId"]){
	$arResult["FILTER"]["userId"] = $_POST["userId"];
}

if($_POST["userName"]){
	$arResult["FILTER"]["userName"] = $_POST["userName"];
}



$arFilter = Array();

if($arResult["FILTER"]["idDepartment"]){	$arFilter[PROP_LEAD_DEPARTMENT_STRUCTURE] = $arResult["FILTER"]["idDepartment"];}

if($arResult["FILTER"]["userId"]){
	$arFilter["ASSIGNED_BY_ID"] = intval($arResult["FILTER"]["userId"]);
}

if($arResult["FILTER"]["dateFrom"]){
	$arFilter[">=DATE_CREATE"] = $arResult["FILTER"]["dateFrom"];
}

if($arResult["FILTER"]["dateTo"]){
	$arFilter["<=DATE_CREATE"] = $arResult["FILTER"]["dateTo"];
}


//o($arFilter);


$arSelect = Array(
	"ID", "DATE_CREATE", "TITLE", "ASSIGNED_BY_ID", PROP_LEAD_DEPARTMENT_STRUCTURE
);

$objLead = CCrmLead::GetList(false, $arFilter, $arSelect, 20);

while($arrLead = $objLead->Fetch()){//	o($arrLead);}














$this->IncludeComponentTemplate();
?>
