<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("crm");

$deal = new CCrmDeal;


$arSort = Array("ID" => "ASC");

$arFilter = Array(
    "IBLOCK_ID" => MAIN_CATALOG_ID,
    "ID" => intval($_POST["productId"])
);

$arNav = Array(
    "nTopCount" => 1
);

$arSelect = Array(
    "ID",
    "NAME",
    "PROPERTY_FILIAL.NAME",
    "PROPERTY_ACADEMIC_YEAR.NAME",
    "PROPERTY_SEMESTR.NAME",
    "PROPERTY_LEVEL.NAME",
    "PROPERTY_SUBLEVEL.NAME",
    "PROPERTY_FACULTY.NAME",
    "PROPERTY_SPECIALITY.NAME",
    "PROPERTY_SPECIALIZATION.NAME",
    "PROPERTY_TRAININGPROGRAM.NAME",
    "PROPERTY_TYPESOFPROGRAMS.NAME",
    "PROPERTY_FORMOFLEARNING.NAME",
    "PROPERTY_DURATION.NAME",
    "PROPERTY_ADDITIONAL_CATEGORY.NAME",
);

$objProduct = CIBlockElement::GetList($arSort, $arFilter, false, $arNav, $arSelect);

if($arrProduct = $objProduct->Fetch()){

    $arDealParams = Array(
        PROP_DEAL_FILIAL => $arrProduct["PROPERTY_FILIAL_NAME"],
        PROP_DEAL_ACADEMIC_YEARS => $arrProduct["PROPERTY_ACADEMIC_YEAR_NAME"],
        PROP_DEAL_SEMESTR => $arrProduct["PROPERTY_SEMESTR_NAME"],
        PROP_DEAL_LEVEL => $arrProduct["PROPERTY_LEVEL_NAME"],
        PROP_DEAL_SUBLEVEL => $arrProduct["PROPERTY_SUBLEVEL_NAME"],
        PROP_DEAL_FACULTET => $arrProduct["PROPERTY_FACULTY_NAME"],
        PROP_DEAL_SPECIALITY => $arrProduct["PROPERTY_SPECIALITY_NAME"],
        PROP_DEAL_SPECIALIZATION => $arrProduct["PROPERTY_SPECIALIZATION_NAME"],
        PROP_DEAL_TRAININGPROGRAM => $arrProduct["PROPERTY_TRAININGPROGRAM_NAME"],
        PROP_DEAL_TYPESOFPROGRAMS => $arrProduct["PROPERTY_TYPESOFPROGRAMS_NAME"],
        PROP_DEAL_FORMOFLEARNING => $arrProduct["PROPERTY_FORMOFLEARNING_NAME"],
        PROP_DEAL_DURATION => $arrProduct["PROPERTY_DURATION_NAME"],
        PROP_DEAL_ADDITIONAL_CATEGORY => $arrProduct["PROPERTY_ADDITIONAL_CATEGORY_NAME"],
    );

    $deal->Update(intval($_POST["dealId"]), $arDealParams, false, false);

}


$arSort = Array(PROP_DEAL_NUMBER_DOGOVOR_INT => "ASC");

$arFilter = Array(
    PROP_DEAL_NUMBER_DOGOVOR_INT_ISSET => 0,
    ">=DATE_CREATE" => "01.01.".date("Y"),
    "<=DATE_CREATE" => "31.12.".date("Y"),
    "!ID" => intval($_POST["dealId"])
);

$arSelect = Array("ID", "TITLE", PROP_DEAL_NUMBER_DOGOVOR_INT);

$objDeal = CCrmDeal::GetList($arSort, $arFilter, $arSelect, 1);

if($arrDeal = $objDeal->Fetch()){
    $nextNumber = intval($arrDeal[PROP_DEAL_NUMBER_DOGOVOR_INT]) + 1;
}else{
    $nextNumber = 238800;
}


if($nextNumber <= 268799){
    $arDeal = Array(
        PROP_DEAL_NUMBER_DOGOVOR_INT => $nextNumber,
        PROP_DEAL_NUMBER_DOGOVOR => $nextNumber."/".date("y"),
    );

    $deal->Update(intval($_POST["dealId"]), $arDeal);
}


$arRows = array(
    0 => Array(
        "PRODUCT_ID" => intval($_POST["productId"]),
        "PRICE" => intval($_POST["price"])
    )
);

$response = CCrmDeal::SaveProductRows(intval($_POST["dealId"]), $arRows);

$arResponse = Array(
    "PARAMS" => $_POST,
    "response" => $response
);

echo json_encode($arResponse);
?>