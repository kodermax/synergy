<?$APPLICATION->SetTitle("Сделка ".$arParams["DEAL_ID"].". Форма для планшета");?>
<?
// var_dump( $arResult['DEAL_FIELDS'] );
?>
<div class="dialogChangeInput"></div>
<div class="fastMessage"></div>
<div class="tablet-form">
    <div style="position:relative;padding-right:60px;">
        <a style="float:left" href="/crm/deal/show/<?=$arParams["DEAL_ID"]?>/">Вернуться на страницу сделки</a>
    </div>
    <br /><br />

    <table class="form-fields">
        <tr class="caption">
            <td colspan="2">Информация о контакте:</td>
        </tr>
        <tr class="name-contact">
            <td>
                <label>Имя: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"]["NAME"])){
                    $CONTACT_FIELDS_NAME = $arResult["CONTACT_FIELDS"]["NAME"];
                } else {
                    $CONTACT_FIELDS_NAME = '';
                }
                ?>

                <input codefield="NAME" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_NAME?>">
            </td>
        </tr>
        <tr class="last-name-contact">
            <td>
                <label>Фамилия: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"]["LAST_NAME"])){
                    $CONTACT_FIELDS_LAST_NAME = $arResult["CONTACT_FIELDS"]["LAST_NAME"];
                } else {
                    $CONTACT_FIELDS_LAST_NAME = '';
                }
                ?>
                <input codefield="LAST_NAME" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_LAST_NAME?>">
            </td>
        </tr>
        <tr class="second-name-contact">
            <td>
                <label>Отчество: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"]["SECOND_NAME"])){
                    $CONTACT_FIELDS_SECOND_NAME = $arResult["CONTACT_FIELDS"]["SECOND_NAME"];
                } else {
                    $CONTACT_FIELDS_SECOND_NAME = '';
                }
                ?>
                <input codefield="SECOND_NAME" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_SECOND_NAME?>">
            </td>
        </tr>
        <tr class="birthday">
            <td>
                <label>День рождения: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"]["BIRTHDATE"])){
                    $CONTACT_FIELDS_BIRTHDATE = ConvertDateTime($arResult["CONTACT_FIELDS"]["BIRTHDATE"],"DD.MM.YYYY");
                } else {
                    $CONTACT_FIELDS_BIRTHDATE = '';
                }
                ?>
                <input codefield="BIRTHDATE" crm="CONTACT" class="datepicker" type="text" value="<?=$CONTACT_FIELDS_BIRTHDATE?>">
            </td>
        </tr>
        <tr class="gender">
            <td>
                <label>Пол: </label>
            </td>
            <td>
                <select id="<?=PROP_CONTACT_GENDER?>" codefield="<?=PROP_CONTACT_GENDER?>" crm="CONTACT" class="">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["GENDER"] as $arGender):?>
                        <option <?if($arGender["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_GENDER]):?>selected<?endif;?> value="<?=$arGender["ID"]?>"><?=$arGender["VALUE"]?></option>
                    <?endforeach;?>
                </select>
            </td>
        </tr>
        <?if(count($arResult["CONTACT_FIELDS"]["PHONE"] > 0)):?>
            <?foreach($arResult["CONTACT_FIELDS"]["PHONE"] as $arPhone):?>
                <tr class="phone">
                    <td>
                        <label>Телефон: </label>
                    </td>
                    <td>
                        <?
                        if(!empty($arPhone["ID"])){
                            $arPhone_input = $arPhone["VALUE"];
                        } else {
                            $arPhone_input = '';
                        }
                        ?>
                        <input class="inputPhone" codefield="PHONE" crm="CONTACT" type="text" valueId="<?=$arPhone["ID"]?>" valueType="<?=$arPhone["VALUE_TYPE"]?>" value="<?=$arPhone_input?>">
                    </td>
                </tr>
            <?endforeach;?>
        <?endif;?>
        <?if(count($arResult["CONTACT_FIELDS"]["EMAIL"] > 0)):?>
            <?foreach($arResult["CONTACT_FIELDS"]["EMAIL"] as $arEmail):?>
                <tr class="email">
                    <td>
                        <label>E-mail: </label>
                    </td>
                    <td>
                        <?
                        if(!empty($arEmail["ID"])){
                            $arEmail_form = $arEmail["VALUE"];
                        } else {
                            $arEmail_form = '';
                        }
                        ?>
                        <input codefield="EMAIL" crm="CONTACT" name="EMAIL" type="text" valueId="<?=$arEmail["ID"]?>" valueType="<?=$arEmail["VALUE_TYPE"]?>" value="<?=$arEmail_form?>">
                    </td>
                </tr>
            <?endforeach;?>
        <?endif;?>
        <tr class="fio_mother">
            <td>
                <label>ФИО матери: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOMOTHER])){
                    $CONTACT_FIELDS_PROP_CONTACT_FIOMOTHER = $arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOMOTHER];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_FIOMOTHER = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_FIOMOTHER?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_FIOMOTHER?>">
            </td>
        </tr>
        <tr class="phone_mother">
            <td>
                <label>Телефон матери: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEMOTHER])){
                    $CONTACT_FIELDS_PROP_CONTACT_PHONEMOTHER = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEMOTHER];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PHONEMOTHER = '';
                }
                ?>
                <input class="inputPhone" codefield="<?=PROP_CONTACT_PHONEMOTHER?>" crm="CONTACT" type="text" name="PHONE" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PHONEMOTHER?>">
            </td>
        </tr>
        <tr class="fio_father">
            <td>
                <label>ФИО отца: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOFATHER])){
                    $CONTACT_FIELDS_PROP_CONTACT_FIOFATHER = $arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOFATHER];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_FIOFATHER = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_FIOFATHER?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_FIOFATHER?>">
            </td>
        </tr>
        <tr class="phone_father">
            <td>
                <label>Телефон отца: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEFATHER])){
                    $CONTACT_FIELDS_PROP_CONTACT_PHONEFATHER = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEFATHER];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PHONEFATHER = '';
                }
                ?>
                <input class="inputPhone" codefield="<?=PROP_CONTACT_PHONEFATHER?>" crm="CONTACT" type="text" name="PHONE" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PHONEFATHER?>">
            </td>
        </tr>
        <tr class="training_period">
            <td>
                <label>Срок обучения: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_TRAINING_PERIOD?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["TRAINING_PERIOD"] as $arTrainingPeriod):?>
                        <option <?if($arTrainingPeriod["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TRAINING_PERIOD]):?>selected<?endif;?> value="<?=$arTrainingPeriod["ID"]?>"><?=$arTrainingPeriod["VALUE"]?></option>
                    <?endforeach;?>
                </select>
            </td>
        </tr>
        <tr class="hostel">
            <td>
                <label>Нуждаюсь в общежитии: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_HOSTEL?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["HOSTEL"] as $arHostel):?>
                        <option <?if($arHostel["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_HOSTEL]):?>selected<?endif;?> value="<?=$arHostel["ID"]?>"><?=$arHostel["VALUE"]?></option>
                    <?endforeach;?>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Место рождения: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PLACE_BIRTH])){
                    $CONTACT_FIELDS_PROP_CONTACT_PLACE_BIRTH = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PLACE_BIRTH];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PLACE_BIRTH = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PLACE_BIRTH?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PLACE_BIRTH?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Являюсь победителем и призером заключительного этапа Всероссийской олимпиады школьников и члены сборных команд РФ международный олимпиад по: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_WINNER])){
                    $CONTACT_FIELDS_PROP_CONTACT_WINNER = $arResult["CONTACT_FIELDS"][PROP_CONTACT_WINNER];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_WINNER = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_WINNER?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_WINNER?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Реквизиты документа: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_REQUISITES])){
                    $CONTACT_FIELDS_PROP_CONTACT_REQUISITES = $arResult["CONTACT_FIELDS"][PROP_CONTACT_REQUISITES];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_REQUISITES = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_REQUISITES?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_REQUISITES?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Перечень необходимых специальных условий при проведении вступительных испытаний в связи с ограниченными возможностями здоровья или инвалидностью: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_LIST_DOCS])){
                    $CONTACT_FIELDS_PROP_CONTACT_LIST_DOCS = $arResult["CONTACT_FIELDS"][PROP_CONTACT_LIST_DOCS];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_LIST_DOCS = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_LIST_DOCS?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_LIST_DOCS?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Место сдачи вступительных испытаний с использованием дистанционных технологий: </label>
            </td>
            <td>
                <?
                if($arResult["CONTACT_FIELDS"][PROP_CONTACT_PLACE_EXAMS]){
                    $CONTACT_FIELDS_PROP_CONTACT_PLACE_EXAMS = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PLACE_EXAMS];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PLACE_EXAMS = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PLACE_EXAMS?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PLACE_EXAMS?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Сведения об индивидуальных достижениях: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PROGRESS])){
                    $CONTACT_FIELDS_PROP_CONTACT_PROGRESS = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PROGRESS];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PROGRESS = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PROGRESS?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PROGRESS?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Гражданство: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["PROP_CONTACT_CITIZENSHIP"]["NAME"])){
                    $PROP_CONTACT_CITIZENSHIP_NAME = $arResult["PROP_CONTACT_CITIZENSHIP"]["NAME"];
                } else {
                    $PROP_CONTACT_CITIZENSHIP_NAME = '';
                }
                ?>
                <input value="<?=$PROP_CONTACT_CITIZENSHIP_NAME?>" type="text" class="autocomplete" id="citizenship">
                <?/*
				<select codefield="<?=PROP_CONTACT_CITIZENSHIP?>" crm="CONTACT">
					<option value="0" disabled selected>Выберите значение</option>
					<?foreach($arResult["DOP_PARAMS"]["Citizenship"] as $arCitizenship):?>
						<option <?if($arCitizenship["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_CITIZENSHIP]):?>selected<?endif;?> value="<?=$arCitizenship["ID"]?>"><?=$arCitizenship["NAME"]?></option>
					<?endforeach;?>
				</select>
				*/?>
            </td>
        </tr>
        <tr>
            <td>
                <label>Иностранный язык: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["PROP_CONTACT_FOREIGNLANGUAGE"]["NAME"])){
                    $PROP_CONTACT_FOREIGNLANGUAGE_NAME = $arResult["PROP_CONTACT_FOREIGNLANGUAGE"]["NAME"];
                } else {
                    $PROP_CONTACT_FOREIGNLANGUAGE_NAME = '';
                }
                ?>
                <input value="<?=$PROP_CONTACT_FOREIGNLANGUAGE_NAME?>" type="text" class="autocomplete foreignlanguage" id="<?=PROP_CONTACT_FOREIGNLANGUAGE?>">
                <?/*
				<select codefield="<?=PROP_CONTACT_FOREIGNLANGUAGE?>" crm="CONTACT">
					<option value="0" disabled selected>Выберите значение</option>
					<?foreach($arResult["DOP_PARAMS"]["ForeignLanguage"] as $arForeignLanguage):?>
						<option <?if($arForeignLanguage["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_FOREIGNLANGUAGE]):?>selected<?endif;?> value="<?=$arForeignLanguage["ID"]?>"><?=$arForeignLanguage["NAME"]?></option>
					<?endforeach;?>
				</select>
				*/?>
            </td>
        </tr>
        <?/*
		<tr class="caption">
			<td colspan="2">Баллы вступительных испытаний: </td>
		</tr>
		*/?>
        <tr class="class">
            <td>
                <label>Категория вступительных испытаний: </label>
            </td>
            <td>
                <select codefield="<?=PROP_CONTACT_CATEGOREXUM?>" crm="CONTACT">
                    <option value="0">Нет</option>
                    <?foreach($arResult["SELECT_FIELDS"]["CATEGOR_EXUM"] as $arCatExumDO):?>
                        <option <?if($arResult["CONTACT_FIELDS"][PROP_CONTACT_CATEGOREXUM] == $arCatExumDO["ID"]):?>selected<?endif;?> value="<?=$arCatExumDO["ID"]?>"><?=$arCatExumDO["VALUE"]?></option>
                    <?endforeach;?>
                </select>
            </td>
        </tr>
        <tr class="caption">
            <td colspan="2">
                <a name="results"></a>
                Результаты экзаменов:
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="tablet-form-results">
                    <thead id="tablet-form-results-thead">
                    <tr>
                        <td>Дисциплины:</td>
                        <td>Виды:</td>
                        <td>Результаты:</td>
                        <td>Дата: <span class="webform-button webform-button-create button-add">+</span></td>
                    </tr>
                    <tr id="tablet-form-results-template-row">
                        <td>
                            <select name="GUID_DISCIPLINE[]" codefield="<?=PROP_CONTACT_DISCIPLINESOFADMISSIONTESTS?>" crm="CONTACT" class="select-discipline group-validate">
                                <option value="" style="display:none;">Выберите</option>
                                <?foreach($arResult["DOP_PARAMS"]["DisciplinesOfAdmissionTests"] as $arDisciplinesOfAdmissionTests):?>
                                    <option value="<?=$arDisciplinesOfAdmissionTests["ID"]?>"><?=$arDisciplinesOfAdmissionTests["NAME"]?></option>
                                <?endforeach;?>
                            </select>
                        </td>
                        <td>
                            <select name="CODE_TYPE[]" codefield="<?=PROP_CONTACT_TYPESOFADMISSIONTESTS?>" crm="CONTACT" class="select-admission-type group-validate">
                                <option value="" style="display:none;">Выберите</option>
                                <?foreach($arResult["DOP_PARAMS"]["TypesOfAdmissionTests"] as $arTypesOfAdmissionTests):?>
                                    <option value="<?=$arTypesOfAdmissionTests["ID"]?>"><?=$arTypesOfAdmissionTests["NAME"]?></option>
                                <?endforeach;?>
                            </select>
                        </td>
                        <td>
                            <input name="COUNT_POINTS[0]" type="number" value="" placeholder="Введите число" min="0" codefield="<?=PROP_CONTACT_RESULTSOFADMISSIONTESTS?>" crm="CONTACT" class="input-number group-validate">
                        </td>
                        <td>
                            <input name="DELIVERY_DATE[]" type="text" value="" placeholder="дд.мм.гггг" crm="CONTACT" class="datepicker input-date group-validate">
                            <input name="ID[]" type="hidden" value="" crm="CONTACT" class="input-id">
                            <span class="webform-button webform-button-create button-add">+</span>
                            <span class="webform-button webform-button-decline button-delete">×</span>
                        </td>
                    </tr>
                    </thead>
                    <tbody id="tablet-form-results-tbody">
                    <? foreach($arResult['ADMISSIONTEST'] as $arAdmissionTest_item): ?>
                        <tr>
                            <td>
                                <select name="GUID_DISCIPLINE[]" codefield="<?=PROP_CONTACT_DISCIPLINESOFADMISSIONTESTS?>" crm="CONTACT" class="select-discipline group-validate">
                                    <option value="" style="display:none;">Выберите</option>
                                    <?foreach($arResult["DOP_PARAMS"]["DisciplinesOfAdmissionTests"] as $arDisciplinesOfAdmissionTests):?>
                                        <option <?if($arAdmissionTest_item['UF_GUID_DISCIPLINE'] == $arDisciplinesOfAdmissionTests["ID"]):?>selected<?endif;?> value="<?=$arDisciplinesOfAdmissionTests["ID"]?>"><?=$arDisciplinesOfAdmissionTests["NAME"]?></option>
                                    <?endforeach;?>
                                </select>
                            </td>
                            <td>
                                <select name="CODE_TYPE[]" codefield="<?=PROP_CONTACT_TYPESOFADMISSIONTESTS?>" crm="CONTACT" class="select-admission-type group-validate">
                                    <option value="" style="display:none;">Выберите</option>
                                    <?foreach($arResult["DOP_PARAMS"]["TypesOfAdmissionTests"] as $arTypesOfAdmissionTests):?>
                                        <option <?if($arAdmissionTest_item['UF_CODE_TYPE'] == $arTypesOfAdmissionTests["ID"]):?>selected<?endif;?> value="<?=$arTypesOfAdmissionTests["ID"]?>"><?=$arTypesOfAdmissionTests["NAME"]?></option>
                                    <?endforeach;?>
                                </select>
                            </td>
                            <td>
                                <input name="COUNT_POINTS[0]" type="number" value="<?=$arAdmissionTest_item['UF_COUNT_POINTS']?>" placeholder="Введите число" min="0" codefield="<?=PROP_CONTACT_RESULTSOFADMISSIONTESTS?>" crm="CONTACT" class="input-number group-validate">
                            </td>
                            <td>
                                <input name="DELIVERY_DATE[]" type="text" value="<?=$arAdmissionTest_item['UF_DELIVERY_DATE']?>" placeholder="дд.мм.гггг" crm="CONTACT" class="datepicker input-date group-validate">
                                <input name="ID[]" type="hidden" value="<?=$arAdmissionTest_item['ID']?>" crm="CONTACT" class="input-id">
                                <span class="webform-button webform-button-create button-add">+</span>
                                <span class="webform-button webform-button-decline button-delete">×</span>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <?/*
		<tr>
			<td>
				<label>Виды вступительных испытаний: </label>
			</td>
			<td>
				<select codefield="<?=PROP_CONTACT_TYPESOFADMISSIONTESTS?>" crm="CONTACT">
					<option value="0" disabled selected>Выберите значение</option>
					<?foreach($arResult["DOP_PARAMS"]["TypesOfAdmissionTests"] as $arTypesOfAdmissionTests):?>
					<option <?if($arTypesOfAdmissionTests["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPESOFADMISSIONTESTS]):?>selected<?endif;?> value="<?=$arTypesOfAdmissionTests["ID"]?>"><?=$arTypesOfAdmissionTests["NAME"]?></option>
					<?endforeach;?>
				</select>
			</td>
		</tr>
		*/?>
        <?/*
		<tr>
			<td>
				<label>Тип контактной информации: </label>
			</td>
			<td>
				<select codefield="<?=PROP_CONTACT_TYPESOFCONTACTINFORMATION?>" crm="CONTACT">
					<option value="0" disabled selected>Выберите значение</option>
					<?foreach($arResult["DOP_PARAMS"]["TypesOfContactInformation"] as $arTypesOfContactInformation):?>
						<option <?if($arTypesOfContactInformation["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPESOFCONTACTINFORMATION]):?>selected<?endif;?> value="<?=$arTypesOfContactInformation["ID"]?>"><?=$arTypesOfContactInformation["NAME"]?></option>
					<?endforeach;?>
				</select>
			</td>
		</tr>
		*/?>
        <tr class="caption">
            <td colspan="2">Паспортные данные: </td>
        </tr>
        <tr>
            <td>
                <label>Тип документа удостоверяющего личность: </label>
            </td>
            <td>
                <select id="<?=PROP_CONTACT_TYPEIDENTITYDOCUMENTS?>" codefield="<?=PROP_CONTACT_TYPEIDENTITYDOCUMENTS?>" crm="CONTACT" class="">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["DOP_PARAMS"]["TypeIdentityDocuments"] as $arTypeIdentityDocuments):?>
                        <option <?if($arTypeIdentityDocuments["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPEIDENTITYDOCUMENTS]):?>selected<?endif;?> value="<?=$arTypeIdentityDocuments["ID"]?>"><?=$arTypeIdentityDocuments["NAME"]?></option>
                    <?endforeach;?>
                </select>
            </td>
        </tr>
        <tr class="passport_seria">
            <td>
                <label>Серия: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_SERIA])){
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_SERIA = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_SERIA];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_SERIA = '';
                }
                ?>
                <input id="<?=PROP_CONTACT_PASSPORT_SERIA?>" codefield="<?=PROP_CONTACT_PASSPORT_SERIA?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PASSPORT_SERIA?>">
            </td>
        </tr>
        <tr class="passport_number">
            <td>
                <label>Номер: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_NUMBER])){
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_NUMBER = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_NUMBER];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_NUMBER = '';
                }
                ?>
                <input id="<?=PROP_CONTACT_PASSPORT_NUMBER?>" codefield="<?=PROP_CONTACT_PASSPORT_NUMBER?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PASSPORT_NUMBER?>" class="">
            </td>
        </tr>
        <tr class="passport_number">
            <td>
                <label>Кем выдан: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_KEMVIDAN])){
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_KEMVIDAN = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_KEMVIDAN];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_KEMVIDAN = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PASSPORT_KEMVIDAN?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PASSPORT_KEMVIDAN?>">
            </td>
        </tr>
        <tr class="passport_dateissue">
            <td>
                <label>Дата выдачи: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_DATEISSUE])){
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_DATEISSUE = ConvertDateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_DATEISSUE], "DD.MM.YYYY");
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_DATEISSUE = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PASSPORT_DATEISSUE?>" crm="CONTACT" class="datepicker" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PASSPORT_DATEISSUE?>">
            </td>
        </tr>
        <tr class="passport_codepodrazdel">
            <td>
                <label>Код подразделения: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_CODEPODRAZDEL])){
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_CODEPODRAZDEL = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_CODEPODRAZDEL];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PASSPORT_CODEPODRAZDEL = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PASSPORT_CODEPODRAZDEL?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PASSPORT_CODEPODRAZDEL?>">
            </td>
        </tr>
        <tr class="caption">
            <td colspan="2">Информация о абитуриенте:</td>
        </tr>
        <tr class="vid_abiturient">
            <td>
                <label>Вид абитуриента: </label>
            </td>
            <td>
                <select codefield="<?=PROP_DEAL_VID_ABITURA?>" crm="DEAL">
                    <option value="0">Нет</option>
                    <?foreach($arResult["SELECT_FIELDS"]["VID_ABITURA"] as $arVidAbitura):?>
                        <option <?if($arResult["DEAL_FIELDS"][PROP_DEAL_VID_ABITURA] == $arVidAbitura["ID"]):?>selected<?endif;?> value="<?=$arVidAbitura["ID"]?>"><?=$arVidAbitura["VALUE"]?></option>
                    <?endforeach;?>
                </select>
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Проект: </label>
            </td>
            <td>
                <select codefield="<?=PROP_DEAL_PROJECT?>" crm="DEAL">
                    <option></option>
                </select>
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Название учебного заведения: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["DEAL_FIELDS"][PROP_DEAL_NAME_UCHEBZAVED])){
                    $DEAL_FIELDS_PROP_DEAL_NAME_UCHEBZAVED = $arResult["DEAL_FIELDS"][PROP_DEAL_NAME_UCHEBZAVED];
                } else {
                    $DEAL_FIELDS_PROP_DEAL_NAME_UCHEBZAVED = '';
                }
                ?>
                <input codefield="<?=PROP_DEAL_NAME_UCHEBZAVED?>" crm="DEAL" type="text" value="<?=$DEAL_FIELDS_PROP_DEAL_NAME_UCHEBZAVED?>">
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Тип учебного заведения: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["DEAL_FIELDS"][PROP_DEAL_TYPE_UCHEBZAVED])){
                    $DEAL_FIELDS_PROP_DEAL_TYPE_UCHEBZAVED = $arResult["DEAL_FIELDS"][PROP_DEAL_TYPE_UCHEBZAVED];
                } else {
                    $DEAL_FIELDS_PROP_DEAL_TYPE_UCHEBZAVED = '';
                }
                ?>
                <input codefield="<?=PROP_DEAL_TYPE_UCHEBZAVED?>" crm="DEAL" type="text" value="<?=$DEAL_FIELDS_PROP_DEAL_TYPE_UCHEBZAVED?>">
            </td>
        </tr>
        <tr class="class">
            <td>
                <label>Класс: </label>
            </td>
            <td>
                <select codefield="<?=PROP_DEAL_CLASS?>" crm="DEAL">
                    <option value="0">Нет</option>
                    <?foreach($arResult["SELECT_FIELDS"]["CLASS"] as $arClass):?>
                        <option <?if($arResult["DEAL_FIELDS"][PROP_DEAL_CLASS] == $arClass["ID"]):?>selected<?endif;?> value="<?=$arClass["ID"]?>"><?=$arClass["VALUE"]?></option>
                    <?endforeach;?>
                </select>
            </td>
        </tr>
        <tr class="caption">
            <td colspan="2">Документ об образовании:</td>
        </tr>
        <!-- <tr class="class">
			<td>
				<label>Вид документа об образовании: </label>
			</td>
			<td>
				<select codefield="<?=PROP_DEAL_VYD_DO?>" id="<?=PROP_DEAL_VYD_DO?>"  crm="DEAL" class="empty">
					<option value="0">Нет</option>
					<?foreach($arResult["SELECT_FIELDS"]["VYD_DO"] as $arVydDO):?>
						<option <?if($arResult["DEAL_FIELDS"][PROP_DEAL_VYD_DO] == $arVydDO["ID"]):?>selected<?endif;?> value="<?=$arVydDO["ID"]?>"><?=$arVydDO["VALUE"]?></option>
					<?endforeach;?>
				</select>
			</td>
		</tr>	-->
        <tr>
            <td>
                <label>Тип документа об образовании: </label>
            </td>
            <td>
                <select id="<?=PROP_CONTACT_TYPSDOCUMENTOFEDUCATION?>" codefield="<?=PROP_CONTACT_TYPSDOCUMENTOFEDUCATION?>" crm="CONTACT">
                    <option value="0" disabled selected>Выберите значение</option>
                    <?foreach($arResult["DOP_PARAMS"]["TypsDocumentOfEducation"] as $arTypsDocumentOfEducation):?>
                        <option <?if($arTypsDocumentOfEducation["ID"] == $arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPSDOCUMENTOFEDUCATION]):?>selected<?endif;?> value="<?=$arTypsDocumentOfEducation["ID"]?>"><?=$arTypsDocumentOfEducation["NAME"]?></option>
                    <?endforeach;?>
                </select>
            </td>
        </tr>

        <tr class="project">
            <td>
                <label>Серия ДО: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["DEAL_FIELDS"][PROP_DEAL_SERIA_DO])){
                    $DEAL_FIELDS_PROP_DEAL_SERIA_DO = $arResult["DEAL_FIELDS"][PROP_DEAL_SERIA_DO];
                } else {
                    $DEAL_FIELDS_PROP_DEAL_SERIA_DO = '';
                }
                ?>
                <input id="<?=PROP_DEAL_SERIA_DO?>" codefield="<?=PROP_DEAL_SERIA_DO?>" crm="DEAL" type="text" value="<?=$DEAL_FIELDS_PROP_DEAL_SERIA_DO?>">
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Номер ДО: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["DEAL_FIELDS"][PROP_DEAL_NUMBER_DO])){
                    $DEAL_FIELDS_PROP_DEAL_NUMBER_DO = $arResult["DEAL_FIELDS"][PROP_DEAL_NUMBER_DO];
                } else {
                    $DEAL_FIELDS_PROP_DEAL_NUMBER_DO = '';
                }
                ?>
                <input id="<?=PROP_DEAL_NUMBER_DO?>"  codefield="<?=PROP_DEAL_NUMBER_DO?>" crm="DEAL" type="text" value="<?=$DEAL_FIELDS_PROP_DEAL_NUMBER_DO?>">
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Дата выдачи ДО: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["DEAL_FIELDS"][PROP_DEAL_DATE_DO])){
                    $DEAL_FIELDS_PROP_DEAL_DATE_DO = ConvertDateTime($arResult["DEAL_FIELDS"][PROP_DEAL_DATE_DO], "DD.MM.YYYY");
                } else {
                    $DEAL_FIELDS_PROP_DEAL_DATE_DO = '';
                }
                ?>
                <input id="<?=PROP_DEAL_DATE_DO?>" codefield="<?=PROP_DEAL_DATE_DO?>" crm="DEAL" class="datepicker" type="text" value="<?=$DEAL_FIELDS_PROP_DEAL_DATE_DO?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Образовательное учреждение: </label>
            </td>
            <td>
                <?
                if($arResult["PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID"]["NAME"]){
                    $PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID_NAME = $arResult["PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID"]["NAME"];
                } else {
                    $PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID_NAME = '';
                }
                ?>
                <input class="autocomplete" placeholder="Начните ввод" id="<?=PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID?>" codefield="<?=PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID?>" crm="CONTACT" type="text" value="<?=$PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID_NAME?>">
            </td>
        </tr>

        <tr>
            <td>
                <label>Специальность до поступления: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_SPECIALITYSBEFOREENTERING"]["NAME"])) {
                    $CONTACT_SPECIALITYSBEFOREENTERING_NAME = $arResult["CONTACT_SPECIALITYSBEFOREENTERING"]["NAME"];
                } else {
                    $CONTACT_SPECIALITYSBEFOREENTERING_NAME = '';
                }
                ?>
                <input class="autocomplete" placeholder="Начните ввод" id="<?=PROP_CONTACT_SPECIALITYSBEFOREENTERING?>" codefield="<?=PROP_CONTACT_SPECIALITYSBEFOREENTERING?>" crm="CONTACT" type="text" value="<?=$CONTACT_SPECIALITYSBEFOREENTERING_NAME?>">
            </td>
        </tr>

        <tr class="caption">
            <td colspan="2">Плательщик: </td>
        </tr>
        <tr class="project">
            <td>
                <label>Фамилия: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_LAST_NAME])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_LAST_NAME = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_LAST_NAME];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_LAST_NAME = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PAYER_LAST_NAME?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_LAST_NAME?>">
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Имя: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_NAME])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_NAME = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_NAME];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_NAME = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PAYER_NAME?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_NAME?>">
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Отчество: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_SECOND_NAME])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_SECOND_NAME = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_SECOND_NAME];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_SECOND_NAME = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PAYER_SECOND_NAME?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_SECOND_NAME?>">
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Дата рождения: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_BIRTHDAY])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_BIRTHDAY = ConvertDateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_BIRTHDAY],"DD.MM.YYYY");
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_BIRTHDAY = '';
                }
                ?>
                <input id="<?=PROP_CONTACT_PAYER_BIRTHDAY?>" codefield="<?=PROP_CONTACT_PAYER_BIRTHDAY?>" crm="CONTACT" class="datepicker" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_BIRTHDAY?>">
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>Телефон: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PHONE])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PHONE = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PHONE];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PHONE = '';
                }
                ?>
                <input class="inputPhone" id="<?=PROP_CONTACT_PAYER_PHONE?>" codefield="<?=PROP_CONTACT_PAYER_PHONE?>" crm="CONTACT" name="PHONE" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_PHONE?>">
            </td>
        </tr>
        <tr class="project">
            <td>
                <label>E-mail: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_EMAIL = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_EMAIL = '';
                }
                ?>
                <input id="<?=PROP_CONTACT_PAYER_EMAIL?>" codefield="<?=PROP_CONTACT_PAYER_EMAIL?>" crm="CONTACT" name="EMAIL" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_EMAIL?>">
            </td>
        </tr>
        <tr class="caption">
            <td colspan="2">Паспортные данные плательщика: </td>
        </tr>
        <tr class="passport_seria">
            <td>
                <label>Серия: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_SERIA])) {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_SERIA = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_SERIA];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_SERIA = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_SERIA?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_SERIA?>">
            </td>
        </tr>
        <tr class="passport_number">
            <td>
                <label>Номер: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_NUMBER])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_NUMBER = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_NUMBER];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_NUMBER = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_NUMBER?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_NUMBER?>">
            </td>
        </tr>
        <tr class="passport_number">
            <td>
                <label>Кем выдан: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN?>">
            </td>
        </tr>
        <tr class="passport_dateissue">
            <td>
                <label>Дата выдачи: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_DATEISSUE])) {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_DATEISSUE = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_DATEISSUE];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_DATEISSUE = '';
                }
                ?>
                <input id="<?=PROP_CONTACT_PAYER_PASSPORT_DATEISSUE?>" codefield="<?=PROP_CONTACT_PAYER_PASSPORT_DATEISSUE?>" crm="CONTACT" class="datepicker" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_DATEISSUE?>">
            </td>
        </tr>
        <tr class="passport_codepodrazdel">
            <td>
                <label>Код подразделения: </label>
            </td>
            <td>
                <?
                if(!empty($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL])){
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL];
                } else {
                    $CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL = '';
                }
                ?>
                <input codefield="<?=PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL?>" crm="CONTACT" type="text" value="<?=$CONTACT_FIELDS_PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL?>">
            </td>
        </tr>
        <tr>
            <td>
                <label>Пол: </label>
            </td>
            <td>
                <select name="">
                    <option value="" style="display: none;">Выберите</option>
                    <option value="">Мужской</option>
                    <option value="">Женский</option>
                </select>
            </td>
        </tr>
    </table><!-- .form-fields -->

    <div class="matrix" id="matrix">
        <a name="product"></a>
        <table>
            <tr class="caption">
                <td colspan="3">Выбор продукта:</td>
            </tr>
            <tr>
                <td>Филиал: </td>
                <td>
                    <input id="filials" name="filials" type="text"
                           code="PROPERTY_FILIAL" placeholder="Начните ввод"
                           value="<?if($arResult["DEAL_FIELDS"][PROP_DEAL_FILIAL]) {echo $arResult["DEAL_FIELDS"][PROP_DEAL_FILIAL];}?>">
                </td>
                <td class="icon">
                    <div class="iconChange"></div>
                    <div class="iconGood"></div>
                </td>
            </tr>

            <tr>
                <td>Что вы закончили?</td>
                <td>
                    <select name="class" code="PROPERTY_CLASS" disabled="true">
                        <option value="0" style="display: none;" selected="">Выберите значение</option>
                    </select>
                </td>
                <td class="icon">
                    <div class="iconChange"></div>
                    <div class="iconGood"></div>
                </td>
            </tr>

            <tr>
                <td>Учебный год: </td>
                <td>
                    <select name="academic_year" code="PROPERTY_ACADEMIC_YEAR" disabled="true">
                        <option value="0" style="display: none;" selected="">Выберите значение</option>
                        <?/*
						<option value="<?=IBLOCK_ELEMENT_ID_YEAR_13_14?>">13/14</option>
						<option value="<?=IBLOCK_ELEMENT_ID_YEAR_14_15?>">14/15</option>
						*/?>
                        <?foreach($arResult["DEAL_FIELDS"][PROP_DEAL_ACADEMIC_YEARS] as $arPDAY):?>
                            <option <?if($arResult["DEAL_FIELDS"][PROP_DEAL_ACADEMIC_YEARS] == $arPDAY["ID"]):?>selected<?endif;?> value="<?=$arPDAY["ID"]?>"><?=$arPDAY["VALUE"]?></option>
                        <?endforeach;?>
                    </select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Семестр учебного года: </td>
                <td>
                    <select name="semestr" code="PROPERTY_SEMESTR" disabled="true">
                        <option value="0" style="display: none;" selected="">Выберите значение</option>
                        <!-- <option value="<?=IBLOCK_ELEMENT_ID_SEMESTR_1?>">1</option> -->
                    </select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Уровень образования: </td>
                <td>
                    <select name="codelevel" code="PROPERTY_LEVEL" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Подуровень образования: </td>
                <td>
                    <select name="codesublevel" code="PROPERTY_SUBLEVEL" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Факультет: </td>
                <td>
                    <select name="fuculty" code="PROPERTY_FACULTY" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Направление: </td>
                <td>
                    <select name="speciality" code="PROPERTY_SPECIALITY" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Профессиональная программа: </td>
                <td>
                    <select name="specialization" code="PROPERTY_SPECIALIZATION" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Программа обучения: </td>
                <td>
                    <select name="trainingprogram" code="PROPERTY_TRAININGPROGRAM" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>День обучения: </td>
                <td>
                    <select name="typesofprograms" code="PROPERTY_TYPESOFPROGRAMS" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Форма обучения: </td>
                <td>
                    <select name="codeformoflearning" code="PROPERTY_FORMOFLEARNING" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Срок: </td>
                <td>
                    <select name="duration" code="PROPERTY_DURATION" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
            <tr>
                <td>Дополнительная категория: </td>
                <td>
                    <select name="additional_category" code="PROPERTY_ADDITIONAL_CATEGORY" disabled></select>
                </td>
                <td class="icon">
                    <div class="iconGood"></div>
                </td>
            </tr>
        </table>
    </div><!-- .matrix -->
    <br /><br />


    <div class="printBP">
        <?
        CModule::IncludeModule("iblock"); // подключаем модуль инфблок
        $zapros = CIBlockElement::GetList(
            array(), // сортировка нам тут не важна
            array(
                "IBLOCK_ID" => 94, // выбираем инфоблок с принтерами
                "ACTIVE" => "Y" // выбираем все активные элементы
            ),
            false,
            false,
            array("ID", "IBLOCK_ID", "PROPERTY_*")
        );
        while ($object = $zapros->GetNextElement()){
            $property = $object->GetProperties();
            $temp = $object->GetFields();
            $result[$temp["ID"]] = $temp;
            $result[$temp["ID"]]["settings"] = $property;
        }
        foreach ($result as $result_tmp){
            foreach ($result_tmp["settings"]["user"]["VALUE"] as $user){
                if($user == $USER->GetID()){
                    $printer[] = $result_tmp["settings"]["printer"]["VALUE"];
                }
            }
        }
        ?>
        Принтер
        <select name="printer" id="pr" onchange="javascript:print_auto(this.value);return false;">
            <?$n=1?>
            <?foreach ($printer as $print):?>
                <?if($print == $_SESSION["printer"]):?>
                    <option disabled selected><?=$print?></option>
                <?else:?>
                    <option id="<?=$n?>" value="<?=$print?>"><?=$print?></option>
                <?endif;?>
                <?$n++?>
            <?endforeach;?>
        </select>
        <div id="printer"> </div>
        <br /><br />
        <a class="linkPrint" href="javascript:void(0);">Печать</a>
        <div class="selectBP">
            <p>Выберите категорию абитуриента:</p>
            <div>
                <a href="javascript:void(0)" bpId="69">После 9 класса СПО</a><br />
                <a href="javascript:void(0)" bpId="61">После 11 класса ВПО</a><br />
            </div>
        </div>
    </div><!-- .printBP -->

</div><!-- .tablet-form -->

<script>
    /* Global vars */
    var dealId = "<?=$arResult["DEAL_FIELDS"]["ID"]?>";
    var contactId = "<?=$arResult["CONTACT_FIELDS"]["ID"]?>";
    var componentPath = "<?=$componentPath?>";
    // var filialId = "<?=IBLOCK_ELEMENT_ID_CITY_MOSCOW?>";
    var filialId = "<?=($arResult['PRODUCT']['PROPERTY_FILIAL_VALUE'] ? $arResult['PRODUCT']['PROPERTY_FILIAL_VALUE'] : '') ?>";
    var AKADA_SYNC = "<?=($arResult['DEAL_FIELDS']['UF_CRM_1438331678'] ? $arResult['DEAL_FIELDS']['UF_CRM_1438331678'] : '') ?>";
    var filialName;
    var PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID = "<?=PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID?>";
    var PROP_CONTACT_SPECIALITYSBEFOREENTERING = "<?=PROP_CONTACT_SPECIALITYSBEFOREENTERING?>";
    var PROP_CONTACT_CITIZENSHIP = "<?=PROP_CONTACT_CITIZENSHIP?>";
    var PROP_CONTACT_FOREIGNLANGUAGE = "<?=PROP_CONTACT_FOREIGNLANGUAGE?>";
    var arrayAutocomplete = {};
    var arrayRequiredFilds = {};
    arrayAutocomplete["filials"] = [];
    arrayAutocomplete["citizenship"] = [];
    arrayAutocomplete["foreignlanguage"] = [];
    <?$i=0;foreach($arResult["FILIAL"] as $arFilial):?>
    arrayAutocomplete["filials"][<?=$i?>] = {label: "<?=$arFilial["NAME"]?>", paramId: <?=$arFilial["ID"]?>};
    <?$i++;endforeach;?>
    <?$i=0;foreach($arResult["CITIZENSHIP"] as $arCitizenShip):?>
    arrayAutocomplete["citizenship"][<?=$i?>] = {label: "<?=$arCitizenShip["NAME"]?>", paramId: <?=$arCitizenShip["ID"]?>};
    <?$i++;endforeach;?>
    <?$i=0;foreach($arResult["FOREIGNLANGUAGE"] as $arForeignLanguage):?>
    arrayAutocomplete["foreignlanguage"][<?=$i?>] = {label: "<?=$arForeignLanguage["NAME"]?>", paramId: <?=$arForeignLanguage["ID"]?>};
    <?$i++;endforeach;?>
    <?$i=0;foreach($arResult["REQUIRED"] as $arRequiredFild):?>
    arrayRequiredFilds[<?=$i?>] = '<?=$arRequiredFild?>';
    <?$i++;endforeach;?>

    var arrayProduct = {};
    arrayProduct = <?=json_encode($arResult["PRODUCT"])?>;


    /* Inits */
    $(function(){
        initTabletFormResults();
        initTabletFormProduct();
    });


    /* Functions */
    function print_auto(print){
        $("#printer").load("<?=$templateFolder?>/ajaxPrint.php",{pr:print});
    }


    function initTabletFormProduct() {
        if(filialId) { /* Если у товара в сделке указан город, значит товар есть и нужно загрузить его данные  */
            for(var k in arrayAutocomplete["filials"]){
                var item = arrayAutocomplete["filials"][k];
                if (item.paramId == filialId) filialName = item.label; /* Ищем название города у товара */
            }

            $('#filials').val(filialName).data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item:{value:filialName}}); /* Инициируем автозаполнение города */

            $('#matrix select').on('loaded', function() { /* Обработчик ajax-загрузки данных для селектов, срабатывает по триггеру loaded в script.js */
                var code = $(this).attr('code');
                $(this).val( arrayProduct[code + '_VALUE'] ).trigger('change'); /* Устанавливаем исходные данные и инициируем изменение селекта */
            });
        }
        else if (AKADA_SYNC) { /* Если у сделки указан AKADA_SYNC, нужно дизаблить редактирование товара */
            $('#matrix :input').prop('disabled', true).removeAttr('placeholder').off().addClass('disabled');
        }
    }
</script>

<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/.default/footer.php");  ?>
