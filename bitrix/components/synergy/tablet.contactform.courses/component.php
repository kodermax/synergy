<?
CModule::IncludeModule("iblock");
CModule::IncludeModule("crm");



$arFilterVidAbituraUF = Array(
    "FIELD_NAME" => PROP_DEAL_VID_ABITURA
);

$objVidAbUF = CUserTypeEntity::GetList(false, $arFilterVidAbituraUF);

if($arrVidAbUF = $objVidAbUF->Fetch()){
    $arFilterVidAbEnum = Array(
        "USER_FIELD_ID" => $arrVidAbUF["ID"]
    );

    $objVidAbEnum = CUserFieldEnum::GetList(false, $arFilterVidAbEnum);

    while($arrVidAbEnum = $objVidAbEnum->Fetch()){
        $arResult["SELECT_FIELDS"]["VID_ABITURA"][$arrVidAbEnum["ID"]] = $arrVidAbEnum;
    }
}


$arFilterClassUF = Array(
    "FIELD_NAME" => PROP_DEAL_CLASS
);

$objClassUF = CUserTypeEntity::GetList(false, $arFilterClassUF);

if($arrClassUF = $objClassUF->Fetch()){
    $arFilterClassEnum = Array(
        "USER_FIELD_ID" => $arrClassUF["ID"]
    );

    $objClassEnum = CUserFieldEnum::GetList(false, $arFilterClassEnum);

    while($arrClassEnum = $objClassEnum->Fetch()){
        $arResult["SELECT_FIELDS"]["CLASS"][$arrClassEnum["ID"]] = $arrClassEnum;
    }
}


$arFilterVidDOUF = Array(
    "FIELD_NAME" => PROP_DEAL_VYD_DO
);

$objVidDOUF = CUserTypeEntity::GetList(false, $arFilterVidDOUF);

if($arrVidDOUF = $objVidDOUF->Fetch()){
    $arFilterVidDOEnum = Array(
        "USER_FIELD_ID" => $arrVidDOUF["ID"]
    );

    $objVidDOEnum = CUserFieldEnum::GetList(false, $arFilterVidDOEnum);

    while($arrVidDOEnum = $objVidDOEnum->Fetch()){
        $arResult["SELECT_FIELDS"]["VYD_DO"][$arrVidDOEnum["ID"]] = $arrVidDOEnum;
    }
}


$arFilterCatExumUF = Array(
    "FIELD_NAME" => PROP_CONTACT_CATEGOREXUM
);

$objCatExumUF = CUserTypeEntity::GetList(false, $arFilterCatExumUF);

if($arrCatExumUF = $objCatExumUF->Fetch()){
    $arFilterCatExumEnum = Array(
        "USER_FIELD_ID" => $arrCatExumUF["ID"]
    );

    $objCatExumEnum = CUserFieldEnum::GetList(false, $arFilterCatExumEnum);

    while($arrCatExumEnum = $objCatExumEnum->Fetch()){
        $arResult["SELECT_FIELDS"]["CATEGOR_EXUM"][$arrCatExumEnum["ID"]] = $arrCatExumEnum;
    }
}



$arResult["DEAL_FIELDS"] = SynergyDeal::getSynergyDealById($arParams["DEAL_ID"]);

$arResult["CONTACT_FIELDS"] = SynergyContact::getSynergyContactById($arResult["DEAL_FIELDS"]["CONTACT_ID"]);

$arSort = Array("NAME" => "ASC");

$arFilter = Array(
    "IBLOCK_ID" => IBLOCK_ID_FILIAL
);

$arSelect = Array("ID","NAME");

$objElements = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
while($arrElement = $objElements->Fetch()){
    $arResult["FILIAL"][$arrElement["ID"]] = $arrElement;
}



$arFilter = Array(
    "USER_FIELD_ID" => PROP_CONTACT_GENDER_ID
);


$objGender = CUserFieldEnum::GetList(false, $arFilter);

while($arrGender = $objGender->Fetch()){
    $arResult["GENDER"][$arrGender["ID"]] = $arrGender;
}



$arFilter = Array(
    "USER_FIELD_ID" => PROP_CONTACT_TRAINING_PERIOD_ID
);


$objTrainingPeriodId = CUserFieldEnum::GetList(false, $arFilter);

while($arrTrainingPeriodId = $objTrainingPeriodId->Fetch()){
    $arResult["TRAINING_PERIOD"][$arrTrainingPeriodId["ID"]] = $arrTrainingPeriodId;
}



$arFilter = Array(
    "USER_FIELD_ID" => PROP_CONTACT_HOSTEL_ID
);


$objHostel = CUserFieldEnum::GetList(false, $arFilter);

while($arrHostel = $objHostel->Fetch()){
    $arResult["HOSTEL"][$arrHostel["ID"]] = $arrHostel;
}








$arSort = Array("NAME" => "ASC");

$arFilter = Array(
    "IBLOCK_CODE" => Array(
        "Citizenship",
        "TypsDocumentOfEducation",
        "TypeIdentityDocuments",
        "TypesOfContactInformation"
    )
);

$arSelect = Array("ID","NAME","IBLOCK_CODE");

$objElements = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
while($arrElement = $objElements->Fetch()){
    $arResult["DOP_PARAMS"][$arrElement["IBLOCK_CODE"]][$arrElement["ID"]] = $arrElement;
}




if($arResult["CONTACT_FIELDS"][PROP_CONTACT_SPECIALITYSBEFOREENTERING] > 0){

    $arFilter = Array(
        "IBLOCK_CODE" => SpecialitysBeforeEntering,
        "ID" => $arResult["CONTACT_FIELDS"][PROP_CONTACT_SPECIALITYSBEFOREENTERING]
    );

    $arNav = Array("nTopCount" => 1);

    $arSelect = Array("ID","NAME");

    $objSpec = CIBlockElement::GetList($arSort, $arFilter, false, $arNav, $arSelect);
    if($arSpec = $objSpec->Fetch()){
        $arResult["CONTACT_SPECIALITYSBEFOREENTERING"] = $arSpec;
    }

}


if($arResult["CONTACT_FIELDS"][PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID] > 0){

    $arFilter = Array(
        "IBLOCK_CODE" => EducationalInstitutions,
        "ID" => $arResult["CONTACT_FIELDS"][PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID]
    );

    $arNav = Array("nTopCount" => 1);

    $arSelect = Array("ID","NAME");

    $objSpec = CIBlockElement::GetList($arSort, $arFilter, false, $arNav, $arSelect);
    if($arSpec = $objSpec->Fetch()){
        $arResult["PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID"] = $arSpec;
    }

}



$arSort = Array("NAME" => "ASC");

$arFilter = Array(
    "IBLOCK_CODE" => Array("Citizenship")
);

$arSelect = Array("ID","NAME","IBLOCK_CODE");

$objElements = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
while($arrElements = $objElements->Fetch()){

    if($arrElements["IBLOCK_CODE"] == "Citizenship"){
        $arResult["CITIZENSHIP"][$arrElements["ID"]] = $arrElements;
    }

}



if($arResult["CONTACT_FIELDS"][PROP_CONTACT_CITIZENSHIP] > 0){

    $arFilter = Array(
        "IBLOCK_CODE" => Citizenship,
        "ID" => $arResult["CONTACT_FIELDS"][PROP_CONTACT_CITIZENSHIP]
    );

    $arNav = Array("nTopCount" => 1);

    $arSelect = Array("ID","NAME");

    $objSpec = CIBlockElement::GetList($arSort, $arFilter, false, $arNav, $arSelect);
    if($arSpec = $objSpec->Fetch()){
        $arResult["PROP_CONTACT_CITIZENSHIP"] = $arSpec;
    }

}


//��� ������ ��������
$totalInfo = CCrmProductRow::LoadRows('D', $arParams["DEAL_ID"]);
$productId=$totalInfo[0]['PRODUCT_ID'];
$price=$totalInfo[0]['PRICE'];

$arSort = Array(
    "ID" => "ASC"
);

$arFilter = Array(
    "IBLOCK_ID" => MAIN_CATALOG_ID,
    "ID" => $productId
); // intval($_POST["productId"])

$arNav = Array(
    "nTopCount" => 1
);

$arSelect = Array(
    "ID",
    "NAME",
    "PROPERTY_FILIAL",
    "PROPERTY_FILIAL_NAME",
    "PROPERTY_ACADEMIC_YEAR",
    "PROPERTY_SEMESTR",
    "PROPERTY_LEVEL",
    "PROPERTY_SUBLEVEL",
    "PROPERTY_FACULTY",
    "PROPERTY_SPECIALITY",
    "PROPERTY_SPECIALIZATION",
    "PROPERTY_TRAININGPROGRAM",
    "PROPERTY_TYPESOFPROGRAMS",
    "PROPERTY_FORMOFLEARNING",
    "PROPERTY_DURATION",
    "PROPERTY_ADDITIONAL_CATEGORY",
    "PROPERTY_UNIT",
    "PROPERTY_COURSES",
    "PROPERTY_Budget",
    "PROPERTY_Plan",
    "PROPERTY_CLASS",
);

$objProduct = CIBlockElement::GetList($arSort, $arFilter, false, $arNav, $arSelect);
$arrProduct = $objProduct->Fetch();
$arResult["PRODUCT"]= $arrProduct;


$arRequiredFields = array (
    PROP_CONTACT_TYPSDOCUMENTOFEDUCATION,
    PROP_DEAL_SERIA_DO,
    PROP_DEAL_NUMBER_DO,
    PROP_DEAL_DATE_DO,
    PROP_CONTACT_SPECIALITYSBEFOREENTERING,
    PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID,
    PROP_CONTACT_FOREIGNLANGUAGE,
    PROP_CONTACT_GENDER,
    PROP_CONTACT_TYPEIDENTITYDOCUMENTS,
    PROP_CONTACT_PASSPORT_NUMBER

);

$arResult["REQUIRED"]= $arRequiredFields;


$this->IncludeComponentTemplate();
?>