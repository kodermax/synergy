<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");


$arSort = Array("ID" => "ASC");

$arFilter = Array(
    "IBLOCK_CODE" => "SpecialitysBeforeEntering",
    "NAME" => "%".$_POST["query"]."%"
);

$arNav = Array(
    "nTopCount" => 1000
);

$arSelect = Array("ID","NAME");

$objElements = CIBlockElement::GetList($arSort, $arFilter, false, $arNav, $arSelect);

while($arrElements = $objElements->Fetch()){
    $arResult["response"][$arrElements["ID"]] = $arrElements;
}

echo json_encode($arResult);


?>