<?
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("crm");

$dealId = isset($_REQUEST['dealId']) ? intval($_REQUEST['dealId']) : 0;

$arResult["DEAL_FIELDS"] = SynergyDeal::getSynergyDealById($dealId);
$arResult["CONTACT_FIELDS"] = SynergyContact::getSynergyContactById($arResult["DEAL_FIELDS"]["CONTACT_ID"]);

$arFilter = Array(
    "ID" => $dealId
);
$objLead = CCrmDeal::GetList(false, $arFilter, Array(), 1);
if ($arrLead = $objLead->Fetch()) {
    $arFields["DEAL"] = $arrLead;
}
$dbResMultiFields = CCrmFieldMulti::GetList(array(
    'ID' => 'asc'
), array(
    'ENTITY_ID' => 'DEAL',
    'ELEMENT_ID' => $dealId
));
$arFields['FM'] = array();
while ($arMultiFields = $dbResMultiFields->Fetch()) {
    $arFields['FM'][$arMultiFields["TYPE_ID"]][$arMultiFields["ID"]] = $arMultiFields;
}
$arResult["DEAL"] = array_merge($arFields["DEAL"], $arFields['FM']);
$deal = $arResult["DEAL"];

$arFilter = Array(
    "ID" => $deal["CONTACT_ID"]
);
$arrContacte = array();
$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_FOREIGNLANGUAGE])."' LIMIT 50";

$res= $DB->Query($sql);
if ($arr = $res->Fetch()) {
    $arrContacte['GUID']=$arr['VALUE'];
}

// Contact
$objContact = CCrmContact::GetList(false, $arFilter, Array(), 1);
if ($arrContact = $objContact->Fetch()) {
    $arFields["CONTACT"] = $arrContact;
}


$dbResMultiFields = CCrmFieldMulti::GetList(array(
    'ID' => 'asc'
), array(
    'ENTITY_ID' => 'CONTACT',
    'ELEMENT_ID' => $deal["CONTACT_ID"]
));

$arrAkada_Contacts = array();
while ($arMultiFields = $dbResMultiFields->Fetch()) {

    switch ($arMultiFields['COMPLEX_ID']) {
        case 'EMAIL_WORK':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'0f22a2c9-0baa-11e1-a253-00304863c48b',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>3
            );
            break;

        case 'EMAIL_HOME':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'bc04c22e-7b1d-11dc-8981-003048347044',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>3
            );
            break;

        case 'EMAIL_OTHER':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'bc04c22e-7b1d-11dc-8981-003048347044',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>3
            );
            break;

        case 'PHONE_MOBILE':
            $arrAkada_Contacts[] = (object) array(
                'GUIDTypeOfContactInformation'=>'650b2aa6-407d-11df-8bc1-00304863c48b',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>2
            );

            break;

        case 'PHONE_HOME':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'772b1676-12f9-11de-b67d-00304863c48b',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>2
            );
            break;

        case 'PHONE_OTHER':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'bbd6115c-4bf8-11dc-ae47-000423c8668c',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>2
            );
            break;

        case 'PHONE_WORK':
            $arrAkada_Contacts[] =  (object)  array(
                'GUIDTypeOfContactInformation'=>'7b331d53-84dd-11df-8409-00304863c476',
                'Value'=>$arMultiFields['VALUE'],
                'CodeType'=>2
            );
            break;
    }

}

$arrAkada = array();
$arrAkada['LastName'] = $arFields['CONTACT']['LAST_NAME'];
$arrAkada['FirstName'] = $arFields['CONTACT']['NAME'];
$arrAkada['MiddleName'] = $arFields['CONTACT']['SECOND_NAME'];
$arrAkada['Sex'] = $arFields['CONTACT'][PROP_CONTACT_GENDER]; // 'f';
switch ($arFields['CONTACT'][PROP_CONTACT_GENDER]) {
    case 201:
        $arrAkada['Sex'] = 'm';
        break;
    case 202:
        $arrAkada['Sex'] = 'f';
        break;
}

// Birthdate
$date = new DateTime($arFields['CONTACT']['BIRTHDATE']);
$arrAkada['DateOfBirth'] = $date->format('Y-m-d'); // '1980-12-13';
$arrAkada['Contacts'] = $arrAkada_Contacts;


$arrAkada['FIOMother'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOMOTHER];
$arrAkada['PhoneMother'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEMOTHER];
$arrAkada['FIOFather'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_FIOFATHER];
$arrAkada['PhoneFather'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PHONEFATHER];

//Паспорт
$arrPasspot = array();
$arrPasspot['Series']=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_SERIA];
$arrPasspot['Number']=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_NUMBER];
$arrPasspot['Issued']=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_KEMVIDAN];
$arrPasspot['CodeDivision']=$arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_CODEPODRAZDEL];
$date = new DateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PASSPORT_DATEISSUE]);
$arrPasspot['DateIssued'] = $date->format('Y-m-d');

$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPEIDENTITYDOCUMENTS])."' LIMIT 1";
$res= $DB->Query($sql);
if ($arr = $res->Fetch()) {
    $arrPasspot['GUIDTypeIdentityDocument']=$arr['VALUE'];

} else {
    $arrPasspot['GUIDTypeIdentityDocument']='';
}
if (strlen($arrPasspot['Number'])>0) {
    $arrAkada['Passport'] = (object)$arrPasspot;
}


$arrEducationalInstitution = array();
$sql = "SELECT VALUE, IBLOCK_PROPERTY_ID FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_EDUCATIONALINSTITUTIONS_ID])."' LIMIT 2";
$res= $DB->Query($sql);

while ($arr = $res->Fetch()) {
    switch ($arr['IBLOCK_PROPERTY_ID']){
        case 404:
            $arrEducationalInstitution['GUID']=$arr['VALUE'];
            break;
        case 405:
            $arrEducationalInstitution['Type']=$arr['VALUE'];
            break;
    }

}

$arrAkada['EducationalInstitution'] = (object) $arrEducationalInstitution;
//Документ об образовании
$arrDocumentOfEducation = array();
$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_TYPSDOCUMENTOFEDUCATION])."' LIMIT 1";
$res= $DB->Query($sql);

while ($arr = $res->Fetch()) {
    $arrDocumentOfEducation['GUIDTypeDocumentOfEducation']=$arr['VALUE'];
}


$arrDocumentOfEducation["Series"]=$arResult["DEAL_FIELDS"][PROP_DEAL_SERIA_DO];
$arrDocumentOfEducation["Number"]=$arResult["DEAL_FIELDS"][PROP_DEAL_NUMBER_DO];
if (strlen($arResult["DEAL_FIELDS"][PROP_DEAL_DATE_DO])>0) {
    $date = new DateTime($arResult["DEAL_FIELDS"][PROP_DEAL_DATE_DO]);
    $arrDocumentOfEducation["DateIssued"] = $date->format('Y-m-d');
}
$sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arResult["CONTACT_FIELDS"][PROP_CONTACT_SPECIALITYSBEFOREENTERING])."' LIMIT 1";
$res= $DB->Query($sql);
while ($arr = $res->Fetch()) {
    $arrDocumentOfEducation['GUIDSpecialityBeforeEntering']=$arr['VALUE'];
}

$arrAkada['DocumentOfEducation'] = (object) $arrDocumentOfEducation;

//Плательщик

if (strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_LAST_NAME])>0) {
    $arrPayerFL = array();
    $arrPayerFL['LastName'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_LAST_NAME];
    $arrPayerFL['FirstName'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_NAME];
    $arrPayerFL['MiddleName'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_SECOND_NAME];
    // дефолт F, нет строки пола для нового плательщика
    //TODO добавить поле Sex и GUIDTypeIdentityDocument
    $arrPayerFL['Sex'] = 'f';
    if (strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_BIRTHDAY]) > 0) {
        $date = new DateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_BIRTHDAY]);
        $arrPayerFL["DateOfBirth"] = $date->format('Y-m-d');
    }

    $arrPayerFL_passport = array();
    $arrPayerFL_passport['Series'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_SERIA];
    $arrPayerFL_passport['Number'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_NUMBER];
    $arrPayerFL_passport['Issued'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_KEMVIDAN];
    $arrPayerFL_passport['CodeDivision'] = $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_CODEPODRAZDEL];
    if (strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_DATEISSUE]) > 0) {
        $date = new DateTime($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PASSPORT_DATEISSUE]);
        $arrPayerFL_passport['DateIssued'] = $date->format('Y-m-d');
    }
    //TODO надо добавить пункт выбора "Тип документа удостоверяющего личность" для плательщика.
    //Установлен гуид от выбора документа поступающего
    $arrPayerFL_passport['GUIDTypeIdentityDocument'] = $arrPasspot['GUIDTypeIdentityDocument'];

    $arrPayerFL['Passport'] = (object) $arrPayerFL_passport;


//БРЕД, но работает. Огромная вложенность, не катит.
//ДА, знаю, что не красиво и не рационально.
    $arrPayerFL_contact = array();

    if( strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PHONE])>0 ) {
        $arrPayerFL_contact[] = (object)array(
            'GUIDTypeOfContactInformation' => '650b2aa6-407d-11df-8bc1-00304863c48b',
            'Value' => $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_PHONE],
            'CodeType' => 3
        );
        if( strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL])>0 ) {
            $arrPayerFL_contact[] = (object)array(
                'GUIDTypeOfContactInformation' => '0f22a2c9-0baa-11e1-a253-00304863c48b',
                'Value' => $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL],
                'CodeType' => 2
            );
        }
    }
    elseif ( strlen($arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL])>0 ){
        $arrPayerFL_contact[] = (object)array(
            'GUIDTypeOfContactInformation' => '0f22a2c9-0baa-11e1-a253-00304863c48b',
            'Value' => $arResult["CONTACT_FIELDS"][PROP_CONTACT_PAYER_EMAIL],
            'CodeType' => 2
        );
    }
    else {
        $arrPayerFL_contact[] = (object)array(
            'GUIDTypeOfContactInformation' => '650b2aa6-407d-11df-8bc1-00304863c48b',
            'Value' => " ",
            'CodeType' => 3
        );
    }

    $arrPayerFL['Contacts'] = $arrPayerFL_contact;

    if (strlen($arrPayerFL['LastName'])>0) {
        $arrAkada['PayerFL'] = (object) $arrPayerFL;
    }


}

$arrAkada['GUIDCitizenship']='';
$CitizenshipId=$arFields['CONTACT'][PROP_CONTACT_CITIZENSHIP];

// Hardcore & Sodomia
$sql = "
SELECT ip.code,iep.*
FROM `b_iblock_element_property` iep
JOIN b_iblock_property ip on (ip.ID=iep.IBLOCK_PROPERTY_ID)
WHERE `IBLOCK_ELEMENT_ID` = '".intval($CitizenshipId)."' ";
$res= $DB->Query($sql);
if ($arrCitizenship = $res->Fetch()) {
    $arrAkada['GUIDCitizenship']=$arrCitizenship['VALUE'];
}

// Product
$totalInfo = CCrmProductRow::LoadRows('D', $dealId);
$productId=$totalInfo[0]['PRODUCT_ID'];
$price=$totalInfo[0]['PRICE'];

$arSort = Array(
    "ID" => "ASC"
);

$arFilter = Array(
    "IBLOCK_ID" => MAIN_CATALOG_ID,
    "ID" => $productId
); // intval($_POST["productId"])

$arNav = Array(
    "nTopCount" => 1
);

$arSelect = Array(
    "ID",
    "NAME",
    "PROPERTY_FILIAL",
    "PROPERTY_ACADEMIC_YEAR",
    "PROPERTY_SEMESTR",
    "PROPERTY_LEVEL",
    "PROPERTY_SUBLEVEL",
    "PROPERTY_FACULTY",
    "PROPERTY_SPECIALITY",
    "PROPERTY_SPECIALIZATION",
    "PROPERTY_TRAININGPROGRAM",
    "PROPERTY_TYPESOFPROGRAMS",
    "PROPERTY_FORMOFLEARNING",
    "PROPERTY_DURATION",
    "PROPERTY_ADDITIONAL_CATEGORY",
    "PROPERTY_UNIT",
    "PROPERTY_COURSES",
    "PROPERTY_Budget",
    "PROPERTY_Plan",
    "PROPERTY_CLASS",
);

$objProduct = CIBlockElement::GetList($arSort, $arFilter, false, $arNav, $arSelect);
$arrProduct = $objProduct->Fetch();

$CNP = array();

$CNP['GUIDFilial']='';

$keyCNP = array (
    'GUIDFilial'=>'PROPERTY_FILIAL_VALUE',
    'GUIDSpeciality'=>'PROPERTY_SPECIALITY_VALUE',
    'GUIDTrainingProgram'=>'PROPERTY_TRAININGPROGRAM_VALUE',
    'CodeClass'=>'PROPERTY_CLASS_VALUE',
    'GUIDSpecialization'=>'PROPERTY_SPECIALIZATION_VALUE',
    'CodeLevel'=>'PROPERTY_LEVEL_VALUE',
    'CodeSubLevel'=>'PROPERTY_SUBLEVEL_VALUE',
    'GUIDDuration'=>'PROPERTY_DURATION_VALUE',
    'GUIDAdditionalCategory'=>'PROPERTY_ADDITIONAL_CATEGORY_VALUE',
    'GUIDAcademicYear'=>'PROPERTY_ACADEMIC_YEAR_VALUE',
    'Semester' => 'PROPERTY_SEMESTR_VALUE',
    'GUIDFaculty'=>'PROPERTY_FACULTY_VALUE',
    'GUIDTypesOfPrograms'=>'PROPERTY_TYPESOFPROGRAMS_VALUE',
    'Budget' => '',
    'GUIDUnit'=> 'PROPERTY_UNIT_VALUE',
    'GUIDCourses'=> 'PROPERTY_COURSES_VALUE',
    'CodeFormOfLearning'=>'PROPERTY_FORMOFLEARNING_VALUE',
    'Price'=>'',
    'Plan'=>'',

);

// Hardcore & Sodomia

foreach ($keyCNP as $key=>$val) {
    $sql = "SELECT VALUE FROM `b_iblock_element_property` WHERE `IBLOCK_ELEMENT_ID` = '".intval($arrProduct[$val])."' LIMIT 1";
    $res= $DB->Query($sql);
    if ($arr = $res->Fetch()) {
        $CNP[$key]=$arr['VALUE'];
    } else {
        $CNP[$key]='';
    }
}

$CNP['Price']=$price;
$CNP['Budget']=$arrProduct['PROPERTY_BUDGET_VALUE'];
$CNP['Plan']=$arrProduct['PROPERTY_PLAN_VALUE'];


$arrAkada['CNP'] = (object) $CNP;

try {
    $wsdl='http://sok-devapp/ws/BitrixCRM.1cws?wsdl';
    @$client=new SoapClient($wsdl,array('trace'=>1,'cache_wsdl'=>0,'encoding'=>'UTF-8','check_int_arguments'=>1,'login'=>'iis1','password'=>'iis'));


    $lastInfoAkada = $client->ImportClientWithPreliminaryAgreement(
        array(
            'InputData'=>(object) $arrAkada

        )
    );

    foreach ($lastInfoAkada as $values) {
        $deal = new CCrmDeal;
        $contact = new CCrmContact;

        $arDealParams = Array(
            AKADA_DATA_SYNC => date("d.m.Y"),
            AKADA_SYNC => 1,
            AKADA_GUID => $values->GUIDEntrant,
            STAGE_ID => AKADA_DEAL_STAGE_ID
        );

        $deal->Update($dealId , $arDealParams, false, false);

        $contactId = $arResult["DEAL_FIELDS"]["CONTACT_ID"];
        $arContactParams = Array(
            AKADA_CONTACT_GUID => $values->GUIDFL,
        );
        $contact->Update($contactId , $arContactParams, false, false);

        $error = 0;
        $akadaMessage = $values->Message;
        if(strlen($akadaMessage) > 0){
            $response = true;
        }else{
            $response = false;
        }

        $arResponse = Array(
            "response" => $response,
            "message" => $akadaMessage,
            "error" => $error
        );

        echo json_encode($arResponse);
    }

} catch ( SoapFault $e ) {
    echo 'sorry... our service is down';
    switch ($e->getMessage()) {
        case 'SOAP-ERROR: Encoding: object has no \'DateIssued\' property':
            echo 'Не заполнено дата выдачи';
            break;

        case 'SOAP-ERROR: Encoding: object has no \'GUIDSpecialityBeforeEntering\' property':
            echo 'Не заполнено специальность переде поступлением';
            break;


    }
}

?>