<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");

$arSort = Array("ID" => "ASC");

$arFilter = Array(
    "IBLOCK_ID" => MAIN_CATALOG_ID,
    "ACTIVE" => "Y"
);

$goFilter = true;

if(intval($_POST["paramId"]) > 0){
    $arFilter[$_POST["code"]] = intval($_POST["paramId"]);
}

if(count($_POST["prevParams"]) > 0){
    foreach($_POST["prevParams"] as $keyPrevParam => $idPrevParam){
        $arFilter[$keyPrevParam] = intval($idPrevParam);
    }
}

$nextCode = $_POST["nextCode"];

$arSelect = Array("ID", "NAME", $nextCode, $nextCode.".NAME", "PROPERTY_PRICE");

if($goFilter == true){

    $obj = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

    while($array = $obj->Fetch()){
        $arCode[$array[$nextCode."_VALUE"]]["NAME"] = $array[$nextCode."_NAME"];
        $arCode[$array[$nextCode."_VALUE"]]["VALUE"] = $array[$nextCode."_VALUE"];
        $arPrograms[$array["ID"]] = $array;
    }

    $count = $obj->SelectedRowsCount();

    sort($arCode);

}

$arResponse = Array(
    "nextAutocompleteArray" => $arCode,
    "count" => $count,
    "filter" => $arFilter,
    "post" => $_POST,
    "programs" => $arPrograms,
    "nextCode" => $_POST["nextCode"]
);

echo json_encode($arResponse);
?>