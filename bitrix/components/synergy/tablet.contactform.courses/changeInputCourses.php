<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("crm");

$deal = new CCrmDeal;
$contact = new CCrmContact;

if(!$_REQUEST["valueId"]){

    $arParams = Array(
        $_REQUEST["codeField"] => $_REQUEST["value"]
    );

}else{

    $arParams = Array(
        "FM" => Array(
            $_REQUEST["codeField"] => Array(
                $_REQUEST["valueId"] => Array(
                    "VALUE" => $_REQUEST["value"],
                    "VALUE_TYPE" => $_REQUEST["valueType"]
                )
            )
        )
    );

}





if($_REQUEST["typeField"] == "DEAL"){
    $deal->Update($_REQUEST["dealId"], $arParams, true, true);
    $error = $deal->LAST_ERROR;
}elseif($_REQUEST["typeField"] == "CONTACT"){
    $contact->Update($_REQUEST["contactId"], $arParams, true, true);
    $error = $contact->LAST_ERROR;
}

if(strlen($error) == 0){
    $response = true;
}else{
    $response = false;
}

$arResponse = Array(
    "response" => $response,
    "error" => $error
);

echo json_encode($arResponse);
?>