<?
$MESS["CRM_COLUMN_NAME"] = "Vardas";
$MESS["CRM_COLUMN_LAST_NAME"] = "Pavardė";
$MESS["CRM_COLUMN_SECOND_NAME"] = "Tėvo vardas";
$MESS["CRM_COLUMN_PHONE"] = "Telefonas";
$MESS["CRM_COLUMN_EMAIL"] = "El.paštas";
$MESS["CRM_COLUMN_STATUS"] = "Statusas";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Pakeista";
$MESS["CRM_COLUMN_PRODUCTS"] = "Produktai";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Kontakto info";
$MESS["CRM_OPER_SHOW"] = "Peržiūrėti";
$MESS["CRM_OPER_EDIT"] = "Redaguoti";
?>