<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?

if(count($arResult["ENTRIES"])>0)
{
    $usersID=array();
    foreach($arResult["ENTRIES"] as $id=>$arItem)
    {
        if(is_array( $arItem["EMPLOYEES"] ) && count ( $arItem["EMPLOYEES"] )>0)
        {
            foreach($arItem["EMPLOYEES"] as $j=>$arUser)
            {
                $name = strip_tags(CUser::FormatName($arParams['NAME_TEMPLATE'], $arUser));
                $arResult["ENTRIES"][$id]["EMPLOYEES"][$j]["NAME"]=$name;
                $usersID[]=$arUser["ID"];
            }
        }
    }
    if(is_array($usersID)&&count($usersID)>0)
    {
        $arUsers=array();
        $dbUsers=CUser::GetList(($by="id"), ($order="asc"), array("ID"=>implode('|', $usersID)),array("FIELDS"=>array("ID","EMAIL","WORK_PHONE")));
        while($arUser=$dbUsers->GetNext())
        {
            $arUsers[$arUser["ID"]]["EMAIL"]=$arUser["EMAIL"];
            $arUsers[$arUser["ID"]]["WORK_PHONE"]=$arUser["WORK_PHONE"];
        }
    }
    foreach($arResult["ENTRIES"] as $id=>$arItem)
    {
        if(is_array( $arItem["EMPLOYEES"] ) && count ( $arItem["EMPLOYEES"] )>0)
        {
            foreach($arItem["EMPLOYEES"] as $i=>$arUser)
            {
                $arResult["ENTRIES"][$id]["EMPLOYEES"][$i]["EMAIL"]=$arUsers[$arUser["ID"]]["EMAIL"];
                $arResult["ENTRIES"][$id]["EMPLOYEES"][$i]["WORK_PHONE"]=$arUsers[$arUser["ID"]]["WORK_PHONE"];
                $arResult["ENTRIES"][$id]["EMPLOYEES"][$i]["ONLINE"]=CUser::IsOnLine( $arUser["ID"] );
            }
        }
    }
}

?>