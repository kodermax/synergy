<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule('crm'))
{
	ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
	return;
}

if (IsModuleInstalled('bizproc'))
{
	if (!CModule::IncludeModule('bizproc'))
	{
		ShowError(GetMessage('BIZPROC_MODULE_NOT_INSTALLED'));
		return;
	}
}

$CCrmDeal = new CCrmDeal();
if ($CCrmDeal->cPerms->HavePerm('DEAL', BX_CRM_PERM_NONE, 'READ'))
{
	ShowError(GetMessage('CRM_PERMISSION_DENIED'));
	return;
}

$arParams['PATH_TO_DEAL_LIST'] = CrmCheckPath('PATH_TO_DEAL_LIST', $arParams['PATH_TO_DEAL_LIST'], $APPLICATION->GetCurPage());
$arParams['PATH_TO_DEAL_SHOW'] = CrmCheckPath('PATH_TO_DEAL_SHOW', $arParams['PATH_TO_DEAL_SHOW'], $APPLICATION->GetCurPage().'?deal_id=#deal_id#&show');
$arParams['PATH_TO_DEAL_EDIT'] = CrmCheckPath('PATH_TO_DEAL_EDIT', $arParams['PATH_TO_DEAL_EDIT'], $APPLICATION->GetCurPage().'?deal_id=#deal_id#&edit');
$arParams['PATH_TO_CONTACT_SHOW'] = CrmCheckPath('PATH_TO_CONTACT_SHOW', $arParams['PATH_TO_CONTACT_SHOW'], $APPLICATION->GetCurPage().'?contact_id=#contact_id#&show');
$arParams['PATH_TO_CONTACT_EDIT'] = CrmCheckPath('PATH_TO_CONTACT_EDIT', $arParams['PATH_TO_CONTACT_EDIT'], $APPLICATION->GetCurPage().'?contact_id=#contact_id#&edit');
$arParams['PATH_TO_COMPANY_SHOW'] = CrmCheckPath('PATH_TO_COMPANY_SHOW', $arParams['PATH_TO_COMPANY_SHOW'], $APPLICATION->GetCurPage().'?company_id=#company_id#&show');
$arParams['PATH_TO_COMPANY_EDIT'] = CrmCheckPath('PATH_TO_COMPANY_EDIT', $arParams['PATH_TO_COMPANY_EDIT'], $APPLICATION->GetCurPage().'?company_id=#company_id#&edit');
$arParams['PATH_TO_LEAD_SHOW'] = CrmCheckPath('PATH_TO_LEAD_SHOW', $arParams['PATH_TO_LEAD_SHOW'], $APPLICATION->GetCurPage().'?lead_id=#lead_id#&show');
$arParams['PATH_TO_LEAD_EDIT'] = CrmCheckPath('PATH_TO_LEAD_EDIT', $arParams['PATH_TO_LEAD_EDIT'], $APPLICATION->GetCurPage().'?lead_id=#lead_id#&edit');
$arParams['PATH_TO_LEAD_CONVERT'] = CrmCheckPath('PATH_TO_LEAD_CONVERT', $arParams['PATH_TO_LEAD_CONVERT'], $APPLICATION->GetCurPage().'?lead_id=#lead_id#&convert');
$arParams['PATH_TO_USER_PROFILE'] = CrmCheckPath('PATH_TO_USER_PROFILE', $arParams['PATH_TO_USER_PROFILE'], '/company/personal/user/#user_id#/');

CUtil::InitJSCore(array('ajax', 'tooltip'));

global $USER_FIELD_MANAGER;

$CCrmUserType = new CCrmUserType($USER_FIELD_MANAGER, CCrmDeal::$sUFEntityID);

$arParams['ELEMENT_ID'] = (int) $arParams['ELEMENT_ID'];

$arFilter = array(
	'ID' => $arParams['ELEMENT_ID']
);
$obFields = CCrmDeal::GetList(array(), $arFilter, array());
$arFields = $obFields->GetNext();

$arResult['ELEMENT'] = $arFields;
unset($arFields);

if (empty($arResult['ELEMENT']['ID']))
	LocalRedirect(CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_DEAL_LIST'], array()));

$arResult['FORM_ID'] = 'CRM_DEAL_SHOW';
$arResult['GRID_ID'] = 'CRM_DEAL_LIST';
$arResult['BACK_URL'] = $arParams['PATH_TO_DEAL_LIST'];
$arResult['STAGE_LIST'] = CCrmStatus::GetStatusListEx('DEAL_STAGE');
$arResult['STATE_LIST'] = CCrmStatus::GetStatusListEx('DEAL_STATE');
$arResult['TYPE_LIST'] = CCrmStatus::GetStatusListEx('DEAL_TYPE');
$arResult['PRODUCT_LIST'] = CCrmStatus::GetStatusListEx('PRODUCT');
$arResult['CURRENCY_LIST'] = CCrmStatus::GetStatusListEx('CURRENCY');
$arResult['EVENT_LIST'] = CCrmStatus::GetStatusListEx('EVENT_TYPE');

$arResult['PATH_TO_COMPANY_SHOW'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_COMPANY_SHOW'],
	array(
		'company_id' => $arResult['ELEMENT']['COMPANY_ID']
	)
);
$arResult['PATH_TO_CONTACT_SHOW'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_CONTACT_SHOW'],
	array(
		'contact_id' => $arResult['ELEMENT']['CONTACT_ID']
	)
);

$arResult['FIELDS'] = array();
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'section_contact_info',
	'name' => GetMessage('CRM_SECTION_CONTACT_INFO'),
	'type' => 'section'
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'ID',
	'name' => 'ID',
	'params' => array('size' => 50),
	'value' => $arResult['ELEMENT']['ID'],
	'type' => 'label'
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'TITLE',
	'name' => GetMessage('CRM_FIELD_TITLE_DEAL'),
	'params' => array('size' => 50),
	'value' => isset($arResult['ELEMENT']['TITLE']) ? $arResult['ELEMENT']['TITLE'] : '',
	'type' => 'label'
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'PRODUCT_ID',
	'name' => GetMessage('CRM_FIELD_PRODUCT_ID'),
	'type' => 'label',
	'items' => $arResult['PRODUCT_LIST'],
	'value' => $arResult['PRODUCT_LIST'][$arResult['ELEMENT']['PRODUCT_ID']]
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'OPPORTUNITY',
	'name' => GetMessage('CRM_FIELD_OPPORTUNITY'),
	'params' => array('size' => 50),
	'type' => 'label',
	'value' => isset($arResult['ELEMENT']['OPPORTUNITY']) ? ('<nobr>'.number_format((string)(double)$arResult['ELEMENT']['OPPORTUNITY'], 2, ',', ' ').'</nobr>').' ('.($arResult['CURRENCY_LIST'][$arResult['ELEMENT']['CURRENCY_ID']]).')' : ''
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'PROBABILITY',
	'name' => GetMessage('CRM_FIELD_PROBABILITY'),
	'params' => array('size' => 3),
	'type' => 'label',
	'value' => isset($arResult['ELEMENT']['PROBABILITY']) ? (string)(double)$arResult['ELEMENT']['PROBABILITY'].'%' : ''
);
if (!$CCrmDeal->cPerms->HavePerm('CONTACT', BX_CRM_PERM_NONE, 'READ'))
{
	$arResult['FIELDS']['tab_1'][] = array(
		'id' => 'CONTACT_TITLE',
		'name' => GetMessage('CRM_FIELD_CONTACT_TITLE'),
		'params' => array('size' => 50),
		'type' => 'label',
		'value' => isset($arResult['ELEMENT']['CONTACT_FULL_NAME']) ?
			'<a href="'.$arResult['PATH_TO_CONTACT_SHOW'].'" id="balloon_'.$arResult['GRID_ID'].'_C_'.$arResult['ELEMENT']['ID'].'">'.$arResult['ELEMENT']['CONTACT_FULL_NAME'].'</a>'.
			'<script type="text/javascript">BX.tooltip("CONTACT_'.$arResult['ELEMENT']['~CONTACT_ID'].'", "balloon_'.$arResult['GRID_ID'].'_C_'.$arResult['ELEMENT']['ID'].'", "/bitrix/components/bitrix/crm.contact.show/card.ajax.php", "crm_balloon_contact");</script>'
			: ''
	);
}
if (!$CCrmDeal->cPerms->HavePerm('COMPANY', BX_CRM_PERM_NONE, 'READ'))
{
	$arResult['FIELDS']['tab_1'][] = array(
		'id' => 'COMPANY_TITLE',
		'name' => GetMessage('CRM_FIELD_COMPANY_TITLE'),
		'params' => array('size' => 50),
		'type' => 'label',
		'value' => isset($arResult['ELEMENT']['COMPANY_TITLE']) ?
			'<a href="'.$arResult['PATH_TO_COMPANY_SHOW'].'" id="balloon_'.$arResult['GRID_ID'].'_'.$arResult['ELEMENT']['ID'].'">'.$arResult['ELEMENT']['COMPANY_TITLE'].'</a>'.
			'<script type="text/javascript">BX.tooltip("COMPANY_'.$arResult['ELEMENT']['~COMPANY_ID'].'", "balloon_'.$arResult['GRID_ID'].'_'.$arResult['ELEMENT']['ID'].'", "/bitrix/components/bitrix/crm.company.show/card.ajax.php", "crm_balloon_company");</script>'
			: ''
	);
}
ob_start();
$APPLICATION->IncludeComponent('bitrix:main.user.link',
	'',
	array(
		'ID' => $arResult['ELEMENT']['ASSIGNED_BY'],
		'HTML_ID' => 'crm_assigned_by',
		'USE_THUMBNAIL_LIST' => 'Y',
		'SHOW_YEAR' => 'M',
		'CACHE_TYPE' => 'A',
		'CACHE_TIME' => '3600',
		'NAME_TEMPLATE' => '',//$arParams['NAME_TEMPLATE'],
		'SHOW_LOGIN' => 'Y',
	),
	false,
	array('HIDE_ICONS' => 'Y', 'ACTIVE_COMPONENT'=>'Y')
);
$sVal = ob_get_contents();
ob_end_clean();
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'ASSIGNED_BY_ID',
	'name' => GetMessage('CRM_FIELD_ASSIGNED_BY_ID'),
	'type' => 'custom',
	'value' => $sVal
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'OPENED',
	'name' => GetMessage('CRM_FIELD_OPENED'),
	'type' => 'label',
	'params' => array(),
	'value' => $arResult['ELEMENT']['OPENED'] == 'Y' ? GetMessage('MAIN_YES') : GetMessage('MAIN_NO')
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'STAGE_ID',
	'name' => GetMessage('CRM_FIELD_STAGE_ID'),
	'type' => 'label',
	'items' => $arResult['STAGE_LIST'],
	'value' => $arResult['STAGE_LIST'][$arResult['ELEMENT']['STAGE_ID']]
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'CLOSED',
	'name' => GetMessage('CRM_FIELD_CLOSED'),
	'type' => 'label',
	'value' => (isset($arResult['ELEMENT']['CLOSED']) ? ($arResult['ELEMENT']['CLOSED'] == 'Y' ? GetMessage('MAIN_YES') : GetMessage('MAIN_NO')) : GetMessage('MAIN_NO'))
);
/*
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'STATE_ID',
	'name' => GetMessage('CRM_FIELD_STATE_ID'),
	'type' => 'label',
	'items' => $arResult['STATE_LIST'],
	'value' => $arResult['STATE_LIST'][$arResult['ELEMENT']['STATE_ID']]
);*/
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'TYPE_ID',
	'name' => GetMessage('CRM_FIELD_TYPE_ID'),
	'type' => 'label',
	'items' => $arResult['TYPE_LIST'],
	'value' => $arResult['TYPE_LIST'][$arResult['ELEMENT']['TYPE_ID']]
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'COMMENTS',
	'name' => GetMessage('CRM_FIELD_COMMENTS'),
	'type' => 'label',
	'params' => array(),
	'value' => isset($arResult['ELEMENT']['COMMENTS']) ? htmlspecialcharsback($arResult['ELEMENT']['COMMENTS']) : ''
);


$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'section_date',
	'name' => GetMessage('CRM_SECTION_DATE'),
	'type' => 'section'
);

$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'BEGINDATE',
	'name' => GetMessage('CRM_FIELD_BEGINDATE'),
	'params' => array('size' => 10),
	'type' => 'label',
	'value' => !empty($arResult['ELEMENT']['BEGINDATE']) ? ConvertTimeStamp(strtotime($arResult['ELEMENT']['BEGINDATE'])) : ''
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'CLOSEDATE',
	'name' => GetMessage('CRM_FIELD_CLOSEDATE'),
	'params' => array('size' => 10),
	'type' => 'label',
	'value' => !empty($arResult['ELEMENT']['CLOSEDATE']) ? ConvertTimeStamp(strtotime($arResult['ELEMENT']['CLOSEDATE'])) : ''
);

$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'section_planned_event',
	'name' => GetMessage('CRM_SECTION_PLANNED_EVENT'),
	'type' => 'section'
);

$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'EVENT_DATE',
	'name' => GetMessage('CRM_FIELD_EVENT_DATE'),
	'params' => array('size' => 10),
	'type' => 'label',
	'value' => !empty($arResult['ELEMENT']['EVENT_DATE']) ? ConvertTimeStamp(strtotime($arResult['ELEMENT']['EVENT_DATE'])) : ''
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'EVENT_ID',
	'name' => GetMessage('CRM_FIELD_EVENT_ID'),
	'type' => 'label',
	'items' => $arResult['EVENT_LIST'],
	'value' => $arResult['EVENT_LIST'][$arResult['ELEMENT']['EVENT_ID']]
);
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'EVENT_DESCRIPTION',
	'name' => GetMessage('CRM_FIELD_EVENT_DESCRIPTION'),
	'type' => 'label',
	'params' => array(),
	'value' => isset($arResult['ELEMENT']['EVENT_DESCRIPTION']) ? nl2br($arResult['ELEMENT']['EVENT_DESCRIPTION']) : ''
);

if (IsModuleInstalled('bizproc'))
{
	$arDocumentStates = CBPDocument::GetDocumentStates(
		array('crm', 'CCrmDocumentDeal', 'DEAL'),
		array('crm', 'CCrmDocumentDeal', 'DEAL_'.$arResult['ELEMENT']['ID'])
	);

	if (!empty($arDocumentStates))
	{
		$arResult['FIELDS']['tab_1'][] = array(
			'id' => 'section_bizproc',
			'name' => GetMessage('CRM_SECTION_BIZPROC'),
			'type' => 'section'
		);
		foreach ($arDocumentStates as $arDocumentState)
		{
			$arResult['FIELDS']['tab_1'][] = array(
				'id' => 'BIZPROC_'.$arDocumentState['TEMPLATE_ID'],
				'name' => $arDocumentState['TEMPLATE_NAME'],
				'params' => array('size' => 50),
				'type' => 'label',
				'value' => '<a href="javascript:bxForm_'.$arResult['FORM_ID'].'.SelectTab(\'tab_bizproc\');">'.$arDocumentState['STATE_TITLE'].'</a>'
			);
		}
	}
}
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'section_additional',
	'name' => GetMessage('CRM_SECTION_ADDITIONAL'),
	'type' => 'section'
);

ob_start();
$APPLICATION->IncludeComponent('bitrix:main.user.link',
	'',
	array(
		'ID' => $arResult['ELEMENT']['CREATED_BY'],
		'HTML_ID' => 'crm_created_by',
		'USE_THUMBNAIL_LIST' => 'Y',
		'SHOW_YEAR' => 'M',
		'CACHE_TYPE' => 'A',
		'CACHE_TIME' => '3600',
		'NAME_TEMPLATE' => '',//$arParams['NAME_TEMPLATE'],
		'SHOW_LOGIN' => 'Y',
	),
	false,
	array('HIDE_ICONS' => 'Y', 'ACTIVE_COMPONENT'=>'Y')
);
$sVal = ob_get_contents();
ob_end_clean();
$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'CREATED_BY_ID',
	'name' => GetMessage('CRM_FIELD_CREATED_BY_ID'),
	'type' => 'custom',
	'value' => $sVal
);

$arResult['FIELDS']['tab_1'][] = array(
	'id' => 'DATE_CREATE',
	'name' => GetMessage('CRM_FIELD_DATE_CREATE'),
	'params' => array('size' => 50),
	'type' => 'label',
	'value' => isset($arResult['ELEMENT']['DATE_CREATE']) ? FormatDate('x', MakeTimeStamp($arResult['ELEMENT']['DATE_CREATE'])) : ''
);

if ($arResult['ELEMENT']['DATE_CREATE'] != $arResult['ELEMENT']['DATE_MODIFY'])
{
	ob_start();
	$APPLICATION->IncludeComponent('bitrix:main.user.link',
		'',
		array(
			'ID' => $arResult['ELEMENT']['MODIFY_BY'],
			'HTML_ID' => 'crm_modify_by',
			'USE_THUMBNAIL_LIST' => 'Y',
			'SHOW_YEAR' => 'M',
			'CACHE_TYPE' => 'A',
			'CACHE_TIME' => '3600',
			'NAME_TEMPLATE' => '',//$arParams['NAME_TEMPLATE'],
			'SHOW_LOGIN' => 'Y',
		),
		false,
		array('HIDE_ICONS' => 'Y', 'ACTIVE_COMPONENT'=>'Y')
	);
	$sVal = ob_get_contents();
	ob_end_clean();
	$arResult['FIELDS']['tab_1'][] = array(
		'id' => 'MODIFY_BY_ID',
		'name' => GetMessage('CRM_FIELD_MODIFY_BY_ID'),
		'type' => 'custom',
		'value' => $sVal
	);
	$arResult['FIELDS']['tab_1'][] = array(
		'id' => 'DATE_MODIFY',
		'name' => GetMessage('CRM_FIELD_DATE_MODIFY'),
		'params' => array('size' => 50),
		'type' => 'label',
		'value' => isset($arResult['ELEMENT']['DATE_MODIFY']) ? FormatDate('x', MakeTimeStamp($arResult['ELEMENT']['DATE_MODIFY'])) : ''
	);
}

$CCrmUserType->AddFields($arResult['FIELDS']['tab_1'], $arResult['ELEMENT']['ID'], $arResult['FORM_ID'], false, true);

if (IsModuleInstalled('bizproc'))
{
	$arResult['BIZPROC'] = 'Y';
	ob_start();

	if ((isset($_REQUEST['bizproc_task']) && strlen($_REQUEST['bizproc_task']) > 0))
	{
		$APPLICATION->IncludeComponent(
			'bitrix:bizproc.task',
			'htmls.bizproc.task',
			Array(
				'TASK_ID' => (int)$_REQUEST['bizproc_task'],
				'USER_ID' => 0,
				'WORKFLOW_ID' => '',
				'DOCUMENT_URL' =>  CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_DEAL_SHOW'],
					array(
						'deal_id' => $arResult['ELEMENT']['ID']
					)
				),
				'SET_TITLE' => 'Y',
				'SET_NAV_CHAIN' => 'Y'
	        ),
			'',
			array('HIDE_ICONS' => 'Y')
		);
	}
	elseif (isset($_REQUEST['bizproc_log']) && strlen($_REQUEST['bizproc_log']) > 0)
	{
		$APPLICATION->IncludeComponent('bitrix:bizproc.log',
			'',
			Array(
				'MODULE_ID' => 'crm',
				'ENTITY' => 'CCrmDocumentDeal',
				'DOCUMENT_TYPE' => 'DEAL',
			    'COMPONENT_VERSION' => 2,
				'DOCUMENT_ID' => 'DEAL_'.$arResult['ELEMENT']['ID'],
				'ID' => $_REQUEST['bizproc_log'],
				'SET_TITLE'	=>	'Y',
				'INLINE_MODE' => 'Y',
				'AJAX_MODE' => 'N'
			),
			'',
			array("HIDE_ICONS" => "Y")
		);
	}
	else if (isset($_REQUEST['bizproc_start']) && strlen($_REQUEST['bizproc_start']) > 0)
	{
		$APPLICATION->IncludeComponent('bitrix:bizproc.workflow.start',
			'htmls.bizproc.workflow.start',
			Array(
				'MODULE_ID' => 'crm',
				'ENTITY' => 'CCrmDocumentDeal',
				'DOCUMENT_TYPE' => 'DEAL',
				'DOCUMENT_ID' => 'DEAL_'.$arResult['ELEMENT']['ID'],
				'TEMPLATE_ID' => $_REQUEST['workflow_template_id'],
				'SET_TITLE'	=>	'Y'
			),
			'',
			array('HIDE_ICONS' => 'Y')
		);
	}
	else
	{
		$APPLICATION->IncludeComponent('bitrix:bizproc.document',
			'htmls.bizproc.document',
			Array(
				'MODULE_ID' => 'crm',
				'ENTITY' => 'CCrmDocumentDeal',
				'DOCUMENT_TYPE' => 'DEAL',
				'DOCUMENT_ID' => 'DEAL_'.$arResult['ELEMENT']['ID'],
				'TASK_EDIT_URL' => CHTTP::urlAddParams(CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_DEAL_SHOW'],
					array(
						'deal_id' => $arResult['ELEMENT']['ID']
					)),
					array('bizproc_task' => '#ID#', 'CRM_DEAL_SHOW_active_tab' => 'tab_bizproc')
				),
				'WORKFLOW_LOG_URL' => CHTTP::urlAddParams(CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_DEAL_SHOW'],
					array(
						'deal_id' => $arResult['ELEMENT']['ID']
					)),
					array('bizproc_log' => '#ID#', 'CRM_DEAL_SHOW_active_tab' => 'tab_bizproc')
				),
				'WORKFLOW_START_URL' => CHTTP::urlAddParams(CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_DEAL_SHOW'],
					array(
						'deal_id' => $arResult['ELEMENT']['ID']
					)),
					array('bizproc_start' => 1, 'CRM_DEAL_SHOW_active_tab' => 'tab_bizproc')
				),
				'back_url' => CHTTP::urlAddParams(CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_DEAL_SHOW'],
					array(
						'deal_id' => $arResult['ELEMENT']['ID']
					)),
					array('CRM_DEAL_SHOW_active_tab' => 'tab_bizproc')
				),
				'SET_TITLE'	=>	'Y'
			),
			'',
			array('HIDE_ICONS' => 'Y')
		);
	}
	$sVal = ob_get_contents();
	ob_end_clean();
	$arResult['FIELDS']['tab_bizproc'][] = array(
		'id' => 'DEAL_BIZPROC',
		'name' => GetMessage('CRM_FIELD_DEAL_BIZPROC'),
		'colspan' => true,
		'type' => 'custom',
		'value' => $sVal
	);
}

if (!$CCrmDeal->cPerms->HavePerm('CONTACT', BX_CRM_PERM_NONE, 'READ'))
{
	ob_start();
	$arResult['CONTACT_COUNT'] = $APPLICATION->IncludeComponent(
		'bitrix:crm.contact.list',
		'',
		array(
			'CONTACT_COUNT' => '20',
			'PATH_TO_CONTACT_SHOW' => $arParams['PATH_TO_CONTACT_SHOW'],
			'PATH_TO_CONTACT_EDIT' => $arParams['PATH_TO_CONTACT_EDIT'],
			'PATH_TO_DEAL_EDIT' => $arParams['PATH_TO_DEAL_EDIT'],
			'INTERNAL_FILTER' => array('ID' => $arResult['ELEMENT']['CONTACT_ID']),
			'FORM_ID' => $arResult['FORM_ID'],
			'TAB_ID' => 'tab_contact'
		),
		false
	);
	$sVal = ob_get_contents();
	ob_end_clean();
	$arResult['FIELDS']['tab_contact'][] = array(
		'id' => 'DEAL_CONTACTS',
		'name' => GetMessage('CRM_FIELD_DEAL_CONTACTS'),
		'colspan' => true,
		'type' => 'custom',
		'value' => $sVal
	);
}

if (!$CCrmDeal->cPerms->HavePerm('COMPANY', BX_CRM_PERM_NONE, 'READ'))
{
	ob_start();
	$arResult['COMPANY_COUNT'] = $APPLICATION->IncludeComponent(
		'bitrix:crm.company.list',
		'',
		array(
			'CONTACT_COUNT' => '20',
			'PATH_TO_COMPANY_SHOW' => $arParams['PATH_TO_COMPANY_SHOW'],
			'PATH_TO_COMPANY_EDIT' => $arParams['PATH_TO_COMPANY_EDIT'],
			'PATH_TO_CONTACT_EDIT' => $arParams['PATH_TO_CONTACT_EDIT'],
			'PATH_TO_DEAL_EDIT' => $arParams['PATH_TO_DEAL_EDIT'],
			'INTERNAL_FILTER' => array('ID' => $arResult['ELEMENT']['COMPANY_ID']),
			'FORM_ID' => $arResult['FORM_ID'],
			'TAB_ID' => 'tab_company'
		),
		false
	);
	$sVal = ob_get_contents();
	ob_end_clean();

	$arResult['FIELDS']['tab_company'][] = array(
		'id' => 'DEAL_COMPANY',
		'name' => GetMessage('CRM_FIELD_DEAL_COMPANY'),
		'colspan' => true,
		'type' => 'custom',
		'value' => $sVal
	);
}
if (intval($arResult['ELEMENT']['LEAD_ID']) > 0 && !$CCrmDeal->cPerms->HavePerm('LEAD', BX_CRM_PERM_NONE, 'READ'))
{
	ob_start();
	$arResult['LEAD_COUNT'] = $APPLICATION->IncludeComponent(
		'bitrix:crm.lead.list',
		'',
		array(
			'LEAD_COUNT' => '20',
			'PATH_TO_LEAD_SHOW' => $arParams['PATH_TO_LEAD_SHOW'],
			'PATH_TO_LEAD_EDIT' => $arParams['PATH_TO_LEAD_EDIT'],
			'PATH_TO_LEAD_CONVERT' => $arParams['PATH_TO_LEAD_CONVERT'],
			'INTERNAL_FILTER' => array('ID' => $arResult['ELEMENT']['LEAD_ID']),
			'FORM_ID' => $arResult['FORM_ID'],
			'TAB_ID' => 'tab_lead'
		),
		false
	);
	$sVal = ob_get_contents();
	ob_end_clean();
	$arResult['FIELDS']['tab_lead'][] = array(
		'id' => 'DEAL_LEAD',
		'name' => GetMessage('CRM_FIELD_DEAL_LEAD'),
		'colspan' => true,
		'type' => 'custom',
		'value' => $sVal
	);
}

ob_start();
$arResult['EVENT_COUNT'] = $APPLICATION->IncludeComponent(
	'bitrix:crm.event.view',
	'',
	array(
		'ENTITY_TYPE' => 'DEAL',
		'ENTITY_ID' => $arResult['ELEMENT']['ID'],
		'PATH_TO_USER_PROFILE' => $arParams['PATH_TO_USER_PROFILE'],
		'FORM_ID' => $arResult['FORM_ID'],
		'TAB_ID' => 'tab_event',
		'INTERNAL' => 'Y'
	),
	false
);
$sVal = ob_get_contents();
ob_end_clean();
$arResult['FIELDS']['tab_event'][] = array(
	'id' => 'DEAL_EVENT',
	'name' => GetMessage('CRM_FIELD_DEAL_EVENT'),
	'colspan' => true,
	'type' => 'custom',
	'value' => $sVal
);

if (IsModuleInstalled(CRM_MODULE_CALENDAR_ID))
{
	$arResult['FIELDS']['tab_activity'][] = array(
		'id' => 'section_activity_calendar',
		'name' => GetMessage('CRM_SECTION_ACTIVITY_CALENDAR'),
		'type' => 'section'
	);
	$strVal = '';
	ob_start();
	$arResult['CALENDAR_COUNT'] = $APPLICATION->IncludeComponent(
		'bitrix:crm.activity.calendar.list',
		'',
		array(
			'ACTIVITY_CALENDAR_COUNT' => '20',
			'INTERNAL_FILTER' => array('ENTITY_TYPE' => 'DEAL', 'ENTITY_ID' => $arResult['ELEMENT']['ID']),
			'FORM_ID' => $arResult['FORM_ID'],
			'TAB_ID' => 'tab_activity'
		),
		false
	);
	$strVal = ob_get_contents();
	ob_end_clean();
	$arResult['FIELDS']['tab_activity'][] = array(
		'id' => 'DEAL_CALENDAR',
		'name' => GetMessage('CRM_FIELD_DEAL_CALENDAR'),
		'colspan' => true,
		'type' => 'custom',
		'value' => $strVal,
	);
}

if (IsModuleInstalled('tasks'))
{
	$arResult['FIELDS']['tab_activity'][] = array(
		'id' => 'section_activity_task',
		'name' => GetMessage('CRM_SECTION_ACTIVITY_TASK'),
		'type' => 'section'
	);
	ob_start();
	$arResult['ACTIVITY_COUNT'] = $APPLICATION->IncludeComponent(
		'bitrix:crm.activity.task.list',
		'htmls.crm.activity.task.list',
		array(
			'ACTIVITY_TASK_COUNT' => '20',
			'INTERNAL_FILTER' => array('ENTITY_TYPE' => 'DEAL', 'ENTITY_ID' => $arResult['ELEMENT']['ID']),
			'FORM_ID' => $arResult['FORM_ID'],
			'TAB_ID' => 'tab_activity'
		),
		false
	);
	$sVal = ob_get_contents();
	ob_end_clean();

	$arResult['FIELDS']['tab_activity'][] = array(
		'id' => 'COMPANY_ACTIVITY',
		'name' => GetMessage('CRM_FIELD_DEAL_ACTIVITY'),
		'colspan' => true,
		'type' => 'custom',
		'value' => $sVal
	);
}

$this->IncludeComponentTemplate();

include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/bitrix/crm.deal/include/nav.php');

?>