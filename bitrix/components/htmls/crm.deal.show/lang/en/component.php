<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "The CRM module is not installed.";
$MESS["CRM_PERMISSION_DENIED"] = "Access Denied";
$MESS["CRM_FIELD_TITLE_DEAL"] = "Name";
$MESS["CRM_FIELD_COMMENTS"] = "Comment";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Income";
$MESS["CRM_FIELD_COMPANY_ID"] = "Company";
$MESS["CRM_FIELD_CONTACT_ID"] = "Contact";
$MESS["CRM_FIELD_STAGE_ID"] = "Stage";
$MESS["CRM_FIELD_PRODUCT_ID"] = "Product";
$MESS["CRM_FIELD_STATE_ID"] = "State";
$MESS["CRM_FIELD_CLOSED"] = "Closed";
$MESS["CRM_FIELD_CURRENCY_ID"] = "Currency";
$MESS["CRM_FIELD_PROBABILITY"] = "Probability, %";
$MESS["CRM_FIELD_TYPE_ID"] = "Type";
$MESS["CRM_FIELD_CLOSEDATE"] = "Close Date";
$MESS["CRM_FIELD_BEGINDATE"] = "Commencement Date";
$MESS["CRM_FIELD_ASSIGNED_BY_ID"] = "Responsible";
$MESS["CRM_FIELD_CREATED_BY_ID"] = "Created By";
$MESS["CRM_FIELD_MODIFY_BY_ID"] = "Modified By";
$MESS["CRM_FIELD_DATE_CREATE"] = "Created";
$MESS["CRM_FIELD_DATE_MODIFY"] = "Modified";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Contact Information";
$MESS["CRM_SECTION_ADDITIONAL"] = "More";
$MESS["CRM_SECTION_PROFIT"] = "Income";
$MESS["CRM_SECTION_DATE"] = "Period";
$MESS["CRM_SECTION_PLANNED_EVENT"] = "Planned Event";
$MESS["CRM_FIELD_EVENT_DATE"] = "Event Date";
$MESS["CRM_FIELD_EVENT_ID"] = "Event Type";
$MESS["CRM_FIELD_EVENT_DESCRIPTION"] = "Event Description";
$MESS["CRM_FIELD_DEAL_CONTACTS"] = "Deal Contacts";
$MESS["CRM_FIELD_DEAL_COMPANY"] = "Deal Companies";
$MESS["CRM_FIELD_DEAL_LEAD"] = "Deal Leads";
$MESS["CRM_FIELD_DEAL_EVENT"] = "Deal Events";
$MESS["CRM_DEAL_NAV_TITLE_EDIT"] = "Deal: #NAME#";
$MESS["CRM_DEAL_NAV_TITLE_ADD"] = "Add Deal";
$MESS["CRM_DEAL_NAV_TITLE_LIST"] = "Deals";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "Company";
$MESS["CRM_FIELD_CONTACT_TITLE"] = "Contact";
$MESS["CRM_SECTION_ACTIVITY_TASK"] = "Tasks";
$MESS["CRM_FIELD_DEAL_ACTIVITY"] = "Tasks";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "The Business Processes module is not installed.";
$MESS["CRM_SECTION_BIZPROC"] = "Business process";
$MESS["CRM_FIELD_DEAL_BIZPROC"] = "Business processes";
$MESS["CRM_FIELD_OPENED"] = "Available to everyone";
$MESS["CRM_SECTION_ACTIVITY_CALENDAR"] = "Calls and events";
$MESS["CRM_FIELD_DEAL_CALENDAR"] = "Calendar records";
?>