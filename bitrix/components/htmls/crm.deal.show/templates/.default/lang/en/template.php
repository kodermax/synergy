<?
$MESS["CRM_TAB_1"] = "Deal";
$MESS["CRM_TAB_1_TITLE"] = "Deal Properties";
$MESS["CRM_TAB_2"] = "Contacts";
$MESS["CRM_TAB_2_TITLE"] = "Deal Contacts";
$MESS["CRM_TAB_3"] = "Companies";
$MESS["CRM_TAB_3_TITLE"] = "Deal Companies";
$MESS["CRM_TAB_4"] = "Leads";
$MESS["CRM_TAB_4_TITLE"] = "Deal Leads";
$MESS["CRM_TAB_5"] = "Events";
$MESS["CRM_TAB_5_TITLE"] = "Deal Events";
$MESS["CRM_TAB_6"] = "Actions";
$MESS["CRM_TAB_6_TITLE"] = "Deal actions";
$MESS["CRM_TAB_7"] = "Business processes";
$MESS["CRM_TAB_7_TITLE"] = "Deal business processes";
?>