<?
$MESS["CRM_TAB_1"] = "Сделка";
$MESS["CRM_TAB_1_TITLE"] = "Свойства сделки";
$MESS["CRM_TAB_2"] = "Контакт";
$MESS["CRM_TAB_2_TITLE"] = "Контакт сделки";
$MESS["CRM_TAB_3"] = "Компания";
$MESS["CRM_TAB_3_TITLE"] = "Компания сделки";
$MESS["CRM_TAB_4"] = "Лид";
$MESS["CRM_TAB_4_TITLE"] = "Лид сделки";
$MESS["CRM_TAB_5"] = "События";
$MESS["CRM_TAB_5_TITLE"] = "События сделки";
$MESS["CRM_TAB_6"] = "Действия";
$MESS["CRM_TAB_6_TITLE"] = "Действия сделки";
$MESS["CRM_TAB_7"] = "Бизнес-процессы";
$MESS["CRM_TAB_7_TITLE"] = "Бизнес-процессы сделки";
?>