<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript">
function ForAllClick(el){
	if(el.checked){
		if(confirm('<?=GetMessage("HTMLS_DOCDESIGNER_SELECT_ALL")?>')){
			//go through rows
			var ids = el.form['ID[]'];
			if(ids){
				if(!ids.length)
					ids = new Array(ids);

				for(var i=0; i<ids.length; i++){
					ids[i].disabled = el.checked;
					ids[i].checked = el.checked;
				}
			}
			if(el.checked)
				document.getElementById('submit').disabled = false;
			else
				document.getElementById('submit').disabled = true;
		}
		else
			el.checked = false;
	}
	else{
		//go through rows
		var ids = el.form['ID[]'];
		if(ids){
			if(!ids.length)
				ids = new Array(ids);

			for(var i=0; i<ids.length; i++){
				ids[i].disabled = el.checked;
				ids[i].checked = false;//el.checked;
			}
		}
		if(el.checked)
			document.getElementById('submit').disabled = false;
		else
			document.getElementById('submit').disabled = true;
	}
}

function ForClick(el){
	if(el.checked)
		document.getElementById('submit').disabled = false;
	else
		ForAllCheck(el);
}

function ForAllCheck(el){
	//go through rows
	var ids = el.form['ID[]'];

	var disabled = true;

	if(ids)
	{
		if(!ids.length)
			ids = new Array(ids);

		for(var i=0; i<ids.length; i++){
			if(ids[i].checked)
				disabled = false;
		}
	}
	document.getElementById('submit').disabled = disabled;
}
</script>
<?
global $MESS, $APPLICATION;
$DemoDD = true;
if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
	$CContract = new CDocDesignerContracts();
	$DemoDD = false;
}
$disable = ($USER->IsAdmin())? '' : ' disabled';

?>
<pre><?/*print_r($arResult);*/?></pre>
<br/>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&lang=<?=LANGUAGE_ID?>">
<?echo bitrix_sessid_post();?>
<table class="journal">
<tr>
<th class="top-left-corner">&nbsp;</th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_TEMPLATE_NAME")?></th>
<th class="top-right-corner"><?=GetMessage("HTMLS_DOCDESIGNER_COMMON")?></th></tr>

<?foreach($arResult as $TYPE => $arItems):?>
<tr>
	<th colspan="3" class="colspan3">
		<?=GetMessage('HTMLS_DOCDESIGNER_'.$TYPE)?>
	</th>
</tr>
<?foreach($arItems as $id => $arVal):?>
		<?foreach($arVal as $tid => $arTmp):?>
		<tr>
			<?if($tid == 'IBLOCK_NAME'):?>
				<tr>
					<td class="left-col"></td>
					<th colspan="2" style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle;text-align:left;"><?=$arTmp?>&nbsp;[<?=$id?>]</th>
				</tr>
			<?else:?>
				<td class="left-col"><input type="checkbox" name="ID[]" id="ID_<?=$tid?>" value="<?=$tid?>" onclick="ForClick(this);" <?=$disable?>/></td>
				<td class="col">
					<input type="text" size="50" name="tmp_name[<?=$tid?>]" value="<?=$arTmp['tmp_name']?>" <?=$disable?>/><?=$arTmp['bizproc']?>
					<input type="hidden" name="tmp_name_old[<?=$tid?>]" value="<?=$arTmp['tmp_name']?>" />
				</td>
				<td class="right-col">
					<?$checked = (($arTmp['common'] == 'Y')? ' checked' : '');?>
					<input type="checkbox" name="commonID[<?=$tid?>]" id="commonID_<?=$tid?>" value="<?=$tid?>" <?=$checked?> <?=$disable?>/>
					<input type="hidden" name="commonIDold[<?=$tid?>]" value="<?=$arTmp['common']?>" />
				</td>
        	<?endif;?>
		</tr>
		<?endforeach;?>
<?endforeach;?>

<?endforeach;?>
<tr valign="top"><td class="bottom-left-corner">
<input title="<?=GetMessage("HTMLS_DOCDESIGNER_SELECT_ALL")?>" name="htmls_docdesigner_delete_all_pdf_tmp" id="htmls_docdesigner_delete_all_pdf_tmp" value="Y" onclick="ForAllClick(this);" type="checkbox" <?=$disable?>>
</td>
<td class="bottom">
<input id="submit" type="submit" name="apply" class="adm-btn" value="<?=GetMessage("HTMLS_DOCDESIGNER_BUTTON")?>" disabled>
<input id="save" type="submit" name="save" class="adm-btn" value="<?=GetMessage("HTMLS_DOCDESIGNER_SAVE")?>">
<input id="reset" type="reset" name="reset" class="adm-btn" value="<?=GetMessage("HTMLS_DOCDESIGNER_RESET")?>">
</td>
<td class="bottom-right-corner"></td></tr>
</table>
</form>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
