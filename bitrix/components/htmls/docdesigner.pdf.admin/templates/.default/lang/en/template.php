<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";

$MESS["HTMLS_DOCDESIGNER_TEMPLATE_NAME"] = "Template name";
$MESS["HTMLS_DOCDESIGNER_COMMON"] = "Common";
$MESS["HTMLS_DOCDESIGNER_IBLOCK"] = "Business proccess";
//$MESS["HTMLS_DOCDESIGNER_DEAL"] = "Дата";
$MESS["HTMLS_DOCDESIGNER_LEAD"] = "Lead";
$MESS["HTMLS_DOCDESIGNER_COMPANY"] = "Company";
$MESS["HTMLS_DOCDESIGNER_DEAL"] = "Deal";
$MESS["HTMLS_DOCDESIGNER_CONTACT"] = "Contact";
$MESS["HTMLS_DOCDESIGNER_SELECT_ALL"] = "Allow action for all records?";
$MESS["HTMLS_DOCDESIGNER_BUTTON"] = "Delete";
$MESS["HTMLS_DOCDESIGNER_SAVE"] = "Save";
$MESS["HTMLS_DOCDESIGNER_RESET"] = "Cancel";
?>