<?
$MESS["CRM_TAB_1"] = "Lead";
$MESS["CRM_TAB_1_TITLE"] = "Lead Properties";
$MESS["CRM_TAB_2"] = "Contacts";
$MESS["CRM_TAB_2_TITLE"] = "Lead's Contacts";
$MESS["CRM_TAB_3"] = "Companies";
$MESS["CRM_TAB_3_TITLE"] = "Lead's Companies";
$MESS["CRM_TAB_4"] = "Deals";
$MESS["CRM_TAB_4_TITLE"] = "Lead's Deals";
$MESS["CRM_TAB_5"] = "Events";
$MESS["CRM_TAB_5_TITLE"] = "Lead's Events";
$MESS["CRM_TAB_6"] = "Actions";
$MESS["CRM_TAB_6_TITLE"] = "Lead actions";
$MESS["CRM_TAB_7"] = "Business processes";
$MESS["CRM_TAB_7_TITLE"] = "Lead business processes";
?>