<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS, $APPLICATION;
$DemoDD = true;
if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
	$CContract = new CDocDesignerContracts();
	$DemoDD = false;
}
$disable = ($USER->IsAdmin())? '' : ' disabled';
?>
<br/>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&lang=<?=LANGUAGE_ID?>">
<?echo bitrix_sessid_post();?>
<table style="width:100%;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;font-size:9pt;color:rgb(96,96,96);font-family:Tahoma" border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203);border-top-left-radius:5px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-left-width:1px;border-left-style:solid;border-left-color:rgb(203,203,203)">&nbsp;</th>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203)"><?=$MESS["HTMLS_DOCDESIGNER_OWN_COMPANY"]?></th>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203)"><?=$MESS["HTMLS_DOCDESIGNER_DOCUMENT"]?></th>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203)"><?=$MESS["HTMLS_DOCDESIGNER_NUMBER"]?></th>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203)"><?=$MESS["HTMLS_DOCDESIGNER_DATE"]?></th>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203)"><?=$MESS["HTMLS_DOCDESIGNER_SUMMA"]?></th>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203)"><?=$MESS["HTMLS_DOCDESIGNER_COMPANY"]?></th>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203)"><?=$MESS["HTMLS_DOCDESIGNER_DEAL"]?></th>
<th style="background-image:initial;background-color:rgb(233,232,228);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;font-size:10pt;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(203,203,203);border-right-width:1px;border-right-style:solid;border-right-color:rgb(203,203,203);border-top-width:1px;border-top-style:solid;border-top-color:rgb(203,203,203);border-top-left-radius:0px;border-top-right-radius:5px;border-bottom-right-radius:0px;border-bottom-left-radius:0px"><?=$MESS["HTMLS_DOCDESIGNER_FILE"]?></th></tr>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	//$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	//$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
<pre><?/*print_r($arItem);*/?></pre>
<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">

	<td style="border-left-width:1px;border-left-style:solid;border-left-color:rgb(195,208,230);border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle;font-size:9pt;text-align:center"><input type="checkbox" name="ID[]" id="ID_<?=$arItem['ID']?>" value="<?=$arItem['ID']?>" onclick="ForClick(this);" <?=$disable?>/></td>
	<td style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"><?=$arItem['PROPERTY_OWN_ENTITIES_NAME']?></td>
	<td style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"><?=$arItem['PROPERTIES']['DOC']['VALUE']?></td>
	<td style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"><?=$arItem['NAME']?></td>
	<td style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"><?=$arItem['DATE_CREATE']?></td>
	<td align="right" style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"><?=number_format($arItem['PROPERTIES']['SUMMA']['VALUE'], 2, ',', ' ')?></td>
	<td style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"><?=$arItem['PROPERTIES']['COMPANY_ID']['VALUE']?></td>
	<td style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"><?=$arItem['PROPERTIES']['DEAL_ID']['VALUE']?></td>
	<td style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"><?=$arItem['PROPERTIES']['FILE']['VALUE']?></td>

</tr>
<?endforeach;?>
<tr valign="top"><td style="border-left-width:1px;border-left-style:solid;border-left-color:rgb(195,208,230);border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:5px;border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle;text-align:center;">
<input title="<?=$MESS["HTMLS_DOCDESIGNER_SELECT_ALL"]?>" name="htmls_docdesigner_delete_all_docs" id="htmls_docdesigner_delete_all_docs" value="Y" onclick="ForAllClick(this);" type="checkbox" <?=$disable?>>
</td>
<td colspan="7" style="border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle;font-size:9pt;text-align:left;">
<input id="submit" type="submit" name="apply" value="<?=$MESS["HTMLS_DOCDESIGNER_BUTTON"]?>" disabled>
</td>
<td style="border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:5px;border-bottom-left-radius:0px;text-align:right;border-right-width:1px;border-right-style:solid;border-right-color:rgb(195,208,230);border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(195,208,230);padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;vertical-align:middle"></td></tr>
</table>
</form>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
