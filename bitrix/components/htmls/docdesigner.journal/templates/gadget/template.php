<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS, $APPLICATION;
?>
<div class="news-list">
<table class="journal">
<tr>
<th class="top-left-corner"><?=GetMessage("HTMLS_DOCDESIGNER_DOCUMENT")?><br/><?=GetMessage("HTMLS_DOCDESIGNER_NUMBER")?></th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_SUMMA")?></th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_COMPANY")?><br/><?=GetMessage("HTMLS_DOCDESIGNER_DEAL")?></th>
<th class="top-right-corner"><?=GetMessage("HTMLS_DOCDESIGNER_FILE")?></th></tr>
<?
$rows = count($arResult["ITEMS"]);
$numRow = 0;
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$numRow++;
	?>
	<tr>
	    <?if($numRow == $rows):?>
	    	<td class="bottom-left-corner">
	    <?else:?>
			<td class="left-col">
		<?endif;?>
		<?=$arItem['PROPERTIES']['DOC']['VALUE']?><br/><?=$arItem['NAME']?></td>
		<td class="col-number"><?=number_format($arItem['PROPERTIES']['SUMMA']['VALUE'], 2, ',', ' ')?></td>
		<td class="col"><?=$arItem['PROPERTIES']['COMPANY_ID']['VALUE']?><br/><?=$arItem['PROPERTIES']['DEAL_ID']['VALUE']?></td>
		<?if($numRow == $rows):?>
			<td class="bottom-right-corner">
		<?else:?>
			<td class="right-col">
		<?endif;?>
	    <?=$arItem['PROPERTIES']['FILE']['VALUE']?></td>
	</tr>
<?endforeach;?>
</table>
</div>
