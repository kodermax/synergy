<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
global $MESS, $APPLICATION;
$DemoDD = true;
if(CModule::IncludeModuleEx("htmls.docdesigner") < 3){
	$CContract = new CDocDesignerContracts();
	$DemoDD = false;
}
$disable = ($USER->IsAdmin())? '' : ' disabled';
?>
<br/>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<form method="GET" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&lang=<?=LANGUAGE_ID?>">
<?
if($_REQUEST['DATE_CREATE_FROM']) $DATE_CREATE_FROM = $_REQUEST['DATE_CREATE_FROM'];
if($_REQUEST['DATE_CREATE_TO']) $DATE_CREATE_TO = $_REQUEST['DATE_CREATE_TO'];
if($_REQUEST['COMPANY_ID']) $COMPANY_ID = $_REQUEST['COMPANY_ID'];
?>
<div style="top: 0px;" class="adm-detail-toolbar"><span style="position:absolute;"></span>
<?$APPLICATION->IncludeComponent("bitrix:main.calendar", ".default", array(
	"SHOW_INPUT" => "Y",
	"FORM_NAME" => "",
	"INPUT_NAME" => "DATE_CREATE_FROM",
	"INPUT_NAME_FINISH" => "DATE_CREATE_TO",
	"INPUT_VALUE" => $DATE_CREATE_FROM,
	"INPUT_VALUE_FINISH" => $DATE_CREATE_TO,
	"SHOW_TIME" => "N",
	"HIDE_TIMEBAR" => "Y"
	),
	false
);?>
<? $APPLICATION->IncludeComponent(
	'htmls:company.search',
	'',
	array()
); ?>
<div style="top: 0px;" class="adm-detail-toolbar-right" id="context_right_ZqFP3GGU">
<input class="adm-btn" name="set_filter" value="<?=GetMessage("HTMLS_DOCDESIGNER_FILTER")?>" type="submit">
<input name="set_filter" value="Y" type="hidden">
<input class="adm-btn" name="del_filter" value="<?=GetMessage("HTMLS_DOCDESIGNER_CANCEL")?>" type="submit">
</div>
</div>
<script>
	$(document).ready(function(){
		$('.journal tbody tr:even').css('background-color', '#f5f9f9');
	});
	$(document).ready(function(){
		$('.journal tbody tr').mouseover(function(){
			$(this).addClass('hoover');}
			);
	});
	$(document).ready(function(){
		$('.journal tbody tr').mouseout(function(){
			$(this).removeClass('hoover');}
			);
	});
</script>
<?echo bitrix_sessid_post();?>
<table class="journal">
<tr>
<th class="top-left-corner">&nbsp;</th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_OWN_COMPANY")?></th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_DOCUMENT")?></th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_NUMBER")?></th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_DATE")?></th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_SUMMA")?></th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_COMPANY")?></th>
<th class="top-header"><?=GetMessage("HTMLS_DOCDESIGNER_DEAL")?></th>
<th class="top-right-corner"><?=GetMessage("HTMLS_DOCDESIGNER_FILE")?></th>
</tr>
<tbody>
<?foreach($arResult["ITEMS"] as $arItem):?>
<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<td class="left-col"><input type="checkbox" name="ID[]" id="ID_<?=$arItem['ID']?>" value="<?=$arItem['ID']?>" onclick="ForClick(this);" <?=$disable?>/></td>
	<td class="col"><?=$arItem['PROPERTY_OWN_ENTITIES_NAME']?></td>
	<td class="col"><?=$arItem['PROPERTIES']['DOC']['VALUE']?></td>
	<td class="col"><?=$arItem['NAME']?></td>
	<td class="col"><?=$arItem['DATE_CREATE']?></td>
	<td class="col-number"><?=number_format($arItem['PROPERTIES']['SUMMA']['VALUE'], 2, ',', ' ')?></td>
	<td class="col"><?=$arItem['PROPERTIES']['COMPANY_ID']['VALUE']?></td>
	<td class="col"><?=$arItem['PROPERTIES']['DEAL_ID']['VALUE']?></td>
	<td class="right-col"><?=$arItem['PROPERTIES']['FILE']['VALUE']?></td>
</tr>
<?endforeach;?>
</tbody>
<tfoot>
<tr valign="top"><td class="bottom-left-corner">
<input title="<?=GetMessage("HTMLS_DOCDESIGNER_SELECT_ALL")?>" name="htmls_docdesigner_delete_all_docs" id="htmls_docdesigner_delete_all_docs" value="Y" onclick="ForAllClick(this);" type="checkbox" <?=$disable?>>
</td>
<td colspan="7" class="bottom">
<input class="adm-btn" id="submit" type="submit" name="apply" value="<?=GetMessage("HTMLS_DOCDESIGNER_BUTTON")?>" disabled>
</td>
<td class="bottom-right-corner"></td></tr>
</tfoot>
</table>
</form>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
