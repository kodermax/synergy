<?

$MESS ['SCRUMBAN_TRIAL_END1'] = "К сожалению,<br />ваша пробная версия истекла.";

$MESS ['SCRUMBAN_TRIAL_END2'] = "Установите <a href='http://marketplace.1c-bitrix.ru/tobasket.php?ID=sibirix.scrumban' target='_blank'>полную версию</a> Доски задач, чтобы открыть еще больше возможностей вашего бизнеса.";

$MESS ['SCRUMBAN_TRIAL_END3'] = "Если у вас возникнут вопросы &mdash; мы обязательно поможем.";

$MESS ['SCRUMBAN_TRIAL_END4'] = "Обращайтесь в техническую поддержку: <a href=\"http://scrumban.reformal.ru/\">http://scrumban.reformal.ru/</a>";

$MESS ['SCUMBAN_MODULE_NOT_INSTALLED'] = 'Модуль "Доска задач" не установлен';

$MESS ['SCUMBAN_COMPONENT_WRONG_PATH'] = 'Ooooops! Компонент Доска Задач работает из папки /scrumban/, и не предназначен для размещения пользователями в другую папку.';



$MESS["SCRUMBAN_MENU_KANBAN"] = "Доска задач";

$MESS["SCRUMBAN_MENU_PLANNING"] = "Доска планирования";