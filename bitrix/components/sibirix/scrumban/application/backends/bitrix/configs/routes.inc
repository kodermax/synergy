<?php

return array(
    'routes' => array(
        'task-view-kp' => array(
            'route' => '/company/personal/user/:userId/tasks/task/view/:taskId/',
        ),
        'system-workgroup-settings' => array(
            'route' => '/workgroups/group/:projectId/',
        ),
        'projects-projectInvite' => array(
            'route' => '/workgroups/group/:projectId/edit/?tab=invite&IFRAME=Y&SONET=Y'
        ),
    ),
);
