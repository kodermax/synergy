<?php

return array(
    'routes' => array(
        'index-index'               => array(
            'route'    => '/index/*/',
            'defaults' => array(
                'controller' => 'index',
                'action'     => 'index'
            )
        ),
        'index-all'               => array(
            'route'    => '/all/*/',
            'defaults' => array(
                'controller' => 'index',
                'action'     => 'all'
            )
        ),
        'redirect-301-index-all'               => array(
            'route'    => '/index/all/*/',
            'defaults' => array(
                'controller' => 'index',
                'action'     => 'all',
                '?redirect301?' => 'index-all'
            )
        ),
        'index-loadBoard'         => array(
            'route'    => '/index/load-board/*/',
            'defaults' => array(
                'controller' => 'index',
                'action'     => 'load-board'
            )
        ),
        'task-findOne'              => array(
            'route'    => '/task/find/kanbanBoardTaskId/:kanbanBoardTaskId/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'find',
            ),
        ),
        'task-findAll'              => array(
            'route'    => '/task/find/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'find',
            ),
        ),
        'task-update'               => array(
            'route'    => '/task/save/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'save',
            ),
        ),
        'task-massArchivate'        => array(
            'route'    => '/task/mass-archivate/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'mass-archivate',
            ),
        ),
        'task-create'               => array(
            'route'    => '/task/save/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'save',
            ),
        ),
        'task-destroy'              => array(
            'route'    => '/task/destroy/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'destroy',
            ),
        ),
        'task-allUpdate'            => array(
            'route'    => '/task/all-update/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'all-update',
            ),
        ),
        'task-accompliceAdd'        => array(
            'route'    => '/task/accomplice-add/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'accomplice-add',
            ),
        ),
        'task-accompliceDestroy'    => array(
            'route'    => '/task/accomplice-destroy/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'accomplice-destroy',
            ),
        ),
        'task-watcherAdd'           => array(
            'route'    => '/task/watcher-add/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'watcher-add',
            ),
        ),
        'task-watcherDestroy'       => array(
            'route'    => '/task/watcher-destroy/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'watcher-destroy',
            ),
        ),
        'task-workPlanAdd'          => array(
            'route'    => '/task/add-to-work-plan/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'add-to-work-plan',
            ),
        ),
        'task-workPlanRemove'       => array(
            'route'    => '/task/remove-from-work-plan/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'remove-from-work-plan',
            ),
        ),
        'task-elapsedTimeCreate'    => array(
            'route'    => '/task/elapsed-time-add/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'elapsed-time-add',
            ),
        ),
        'task-elapsedTimeUpdate'    => array(
            'route'    => '/task/elapsed-time-update/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'elapsed-time-update',
            ),
        ),
        'task-elapsedTimeDestroy'   => array(
            'route'    => '/task/elapsed-time-destroy/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'elapsed-time-destroy',
            ),
        ),
        'task-setDodStatus'         => array(
            'route'    => '/task/set-dod-status/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'set-dod-status',
            ),
        ),
        'task-getFile'              => array(
            'route'    => '/task/get-file/:fileId',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'get-file',
            ),
        ),
        'task-moveToBoard'          => array(
            'route'    => '/task/move-to-board/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'move-to-board',
            ),
        ),
        'task-startTimer'          => array(
            'route'    => '/task/timer-start/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'timer-start',
            ),
        ),
        'task-pauseTimer'          => array(
            'route'    => '/task/timer-pause/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'timer-pause',
            ),
        ),
        'comment-getList'           => array(
            'route'    => '/comment/get-list/*/',
            'defaults' => array(
                'controller' => 'comment',
                'action'     => 'get-list',
            ),
        ),
        'comment-save'              => array(
            'route'    => '/comment/save/*/',
            'defaults' => array(
                'controller' => 'comment',
                'action'     => 'save',
            ),
        ),
        'comment-destroy'           => array(
            'route'    => '/comment/destroy/*/',
            'defaults' => array(
                'controller' => 'comment',
                'action'     => 'destroy',
            ),
        ),
        'comment-fileUpload'           => array(
            'route'    => '/comment/upload/*/',
            'defaults' => array(
                'controller' => 'comment',
                'action'     => 'upload',
            ),
        ),
        'task-fileUpload'           => array(
            'route'    => '/task/file-upload/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'file-upload',
            ),
        ),
        'task-filePaste'            => array(
            'route'    => '/task/file-paste/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'file-paste',
            ),
        ),
        'task-fileDelete'           => array(
            'route'    => '/task/file-delete/*/',
            'defaults' => array(
                'controller' => 'task',
                'action'     => 'file-delete',
            ),
        ),
        'column-findOne'            => array(
            'route'    => '/column/find/kanbanBoardColumnId/:kanbanBoardColumnId/*/',
            'defaults' => array(
                'controller' => 'column',
                'action'     => 'find',
            ),
        ),
        'column-findAll'            => array(
            'route'    => '/column/find/*/',
            'defaults' => array(
                'controller' => 'column',
                'action'     => 'find',
            ),
        ),
        'column-update'             => array(
            'route'    => '/column/save/*/',
            'defaults' => array(
                'controller' => 'column',
                'action'     => 'save',
            ),
        ),
        'column-create'             => array(
            'route'    => '/column/save/*/',
            'defaults' => array(
                'controller' => 'column',
                'action'     => 'save',
            ),
        ),
        'column-destroy'            => array(
            'route'    => '/column/destroy/*/',
            'defaults' => array(
                'controller' => 'column',
                'action'     => 'destroy',
            ),
        ),
        'column-saveTaskSorts'      => array(
            'route'    => '/column/save-task-sorts/*/',
            'defaults' => array(
                'controller' => 'column',
                'action'     => 'save-task-sorts',
            ),
        ),
        'board-findOne'             => array(
            'route'    => '/board/find/kanbanBoardId/:kanbanBoardId/*/',
            'defaults' => array(
                'controller' => 'board',
                'action'     => 'find',
            ),
        ),
        'board-findAll'             => array(
            'route'    => '/board/find/*/',
            'defaults' => array(
                'controller' => 'board',
                'action'     => 'find',
            ),
        ),
        'board-update'              => array(
            'route'    => '/board/save/*/',
            'defaults' => array(
                'controller' => 'board',
                'action'     => 'save',
            ),
        ),
        'board-create'              => array(
            'route'    => '/board/save/*/',
            'defaults' => array(
                'controller' => 'board',
                'action'     => 'save',
            ),
        ),
        'board-destroy'             => array(
            'route'    => '/board/destroy/*/',
            'defaults' => array(
                'controller' => 'board',
                'action'     => 'destroy',
            ),
        ),
        'board-saveSettings'        => array(
            'route'    => '/board/save-settings/kanbanBoardId/:kanbanBoardId/*/',
            'defaults' => array(
                'controller' => 'board',
                'action'     => 'save-settings',
            ),
        ),
        'board-archiveList'         => array(
            'route'    => '/board/archive-list/kanbanBoardId/:kanbanBoardId/*/',
            'defaults' => array(
                'controller' => 'board',
                'action'     => 'archive-list',
            ),
        ),
        'board-getArchivedTask'     => array(
            'route'    => '/board/get-archived-task/kanbanBoardId/:kanbanBoardId/backendTaskId/:backendTaskId/',
            'defaults' => array(
                'controller' => 'board',
                'action'     => 'get-archived-task',
            ),
        ),
        'planning-index'            => array(
            'route'    => '/planning/project/:projectId/*/',
            'defaults' => array(
                'controller' => 'planning',
                'action'     => 'index',
            ),
        ),
        'redirect-301-planning-index'            => array(
            'route'    => '/planning/index/project/:projectId/*/',
            'defaults' => array(
                'controller' => 'planning',
                'action'     => 'index',
                '?redirect301?' => 'planning-index'
            ),
        ),
        'planning-sprint'            => array(
            'route'    => '/planning/project/:projectId/sprint/:sprintId/*/',
            'defaults' => array(
                'controller' => 'planning',
                'action'     => 'index',
            ),
        ),
        'redirect-301-planning-sprint'            => array(
            'route'    => '/planning/index/project/:projectId/sprint/:sprintId/*/',
            'defaults' => array(
                'controller' => 'planning',
                'action'     => 'index',
                '?redirect301?' => 'planning-sprint'
            ),
        ),
        'planning-taskList'         => array(
            'route'    => '/planning/task-list/',
            'defaults' => array(
                'controller' => 'planning',
                'action'     => 'task-list'
            ),
        ),
        'planning-updateSorts'      => array(
            'route'    => '/planning/save-task-sorts/*/',
            'defaults' => array(
                'controller' => 'planning',
                'action'     => 'update-sorts',
            ),
        ),
        'planning-moveToSprint'     => array(
            'route'    => '/planning/move-to-sprint/*/',
            'defaults' => array(
                'controller' => 'planning',
                'action'     => 'move-to-sprint',
            ),
        ),
        'project-index'             => array(
            'route'    => '/index/project/:projectId/*/',
            'defaults' => array(
                'controller' => 'index',
                'action'     => 'index'
            ),
        ),
        'redirect-301-project-index'             => array(
            'route'    => '/index/index/project/:projectId/*/',
            'defaults' => array(
                'controller' => 'index',
                'action'     => 'index',
                '?redirect301?'    => 'project-index'
            ),
        ),
        'sprint-index'              => array(
            'route'    => '/index/sprint/:sprintId/*/',
            'defaults' => array(
                'controller' => 'index',
                'action'     => 'index',
            ),
        ),
        'redirect-301-sprint-index'              => array(
            'route'    => '/index/index/sprint/:sprintId/*/',
            'defaults' => array(
                'controller' => 'index',
                'action'     => 'index',
                '?redirect301?'    => 'sprint-index'
            ),
        ),
        'sprint-findOne'            => array(
            'route'    => '/sprint/find/sprintId/:sprintId/*/',
            'defaults' => array(
                'controller' => 'sprint',
                'action'     => 'find',
            ),
        ),
        'sprint-findAll'            => array(
            'route'    => '/sprint/find/*/',
            'defaults' => array(
                'controller' => 'sprint',
                'action'     => 'find',
            ),
        ),
        'sprint-update'             => array(
            'route'    => '/sprint/save/*/',
            'defaults' => array(
                'controller' => 'sprint',
                'action'     => 'save',
            ),
        ),
        'sprint-create'             => array(
            'route'    => '/sprint/save/*/',
            'defaults' => array(
                'controller' => 'sprint',
                'action'     => 'save',
            ),
        ),
        'sprint-destroy'            => array(
            'route'    => '/sprint/destroy/*/',
            'defaults' => array(
                'controller' => 'sprint',
                'action'     => 'destroy',
            ),
        ),
        'sprint-getChartData'            => array(
            'route'    => '/sprint/get-chart-data/*/',
            'defaults' => array(
                'controller' => 'sprint',
                'action'     => 'get-chart-data',
            ),
        ),
        'sprint-moveTasks'          => array(
            'route'    => '/sprint/move-tasks/*/',
            'defaults' => array(
                'controller' => 'sprint',
                'action'     => 'move-tasks',
            ),
        ),
        'checklist-update'          => array(
            'route'    => '/checklist/save/checklistItemId/:checklistItemId/*/',
            'defaults' => array(
                'controller' => 'checklist',
                'action'     => 'save',
            ),
        ),
        'checklist-create'          => array(
            'route'    => '/checklist/save/*/',
            'defaults' => array(
                'controller' => 'checklist',
                'action'     => 'save',
            ),
        ),
        'checklist-destroy'         => array(
            'route'    => '/checklist/destroy/*/',
            'defaults' => array(
                'controller' => 'checklist',
                'action'     => 'destroy',
            ),
        ),
        'checklist-convert'         => array(
            'route'    => '/checklist/convert/*/',
            'defaults' => array(
                'controller' => 'checklist',
                'action'     => 'convert',
            ),
        ),
        'checklist-saveNew'         => array(
            'route'    => '/checklist/save-new/*/',
            'defaults' => array(
                'controller' => 'checklist',
                'action'     => 'save-new',
            ),
        ),
        'label-create'              => array(
            'route'    => '/label/save/*/',
            'defaults' => array(
                'controller' => 'label',
                'action'     => 'save',
            ),
        ),
        'label-destroy'             => array(
            'route'    => '/label/destroy/*/',
            'defaults' => array(
                'controller' => 'label',
                'action'     => 'destroy',
            ),
        ),
        'label2task-create'         => array(
            'route'    => '/label2task/save/*/',
            'defaults' => array(
                'controller' => 'label2task',
                'action'     => 'save',
            ),
        ),
        'label2task-destroy'        => array(
            'route'    => '/label2task/destroy/*/',
            'defaults' => array(
                'controller' => 'label2task',
                'action'     => 'destroy',
            ),
        ),
        'dodCheckList-create'       => array(
            'route'    => '/dod/add-check/*/',
            'defaults' => array(
                'controller' => 'dod',
                'action'     => 'add-check',
            ),
        ),
        'dodCheckList-destroy'      => array(
            'route'    => '/dod/destroy-check/*/',
            'defaults' => array(
                'controller' => 'dod',
                'action'     => 'destroy-check',
            ),
        ),
        'dodList-create'            => array(
            'route'    => '/dod/save/*/',
            'defaults' => array(
                'controller' => 'dod',
                'action'     => 'save',
            ),
        ),
        'dodList-update'            => array(
            'route'    => '/dod/save/dodId/:dodId/*/',
            'defaults' => array(
                'controller' => 'dod',
                'action'     => 'save',
            ),
        ),
        'dodList-destroy'           => array(
            'route'    => '/dod/destroy/*/',
            'defaults' => array(
                'controller' => 'dod',
                'action'     => 'destroy',
            ),
        ),
        /* Projects board routes */
        'projects-index'            => array(
            'route'    => '/projects/*/',
            'defaults' => array(
                'controller' => 'projects',
                'action'     => 'index',
            ),
        ),
        'projects-syncAll'            => array(
            'route'    => '/projects-sync-all/',
            'defaults' => array(
                'controller' => 'projects',
                'action'     => 'sync-all',
            ),
        ),
        'redirect-301-projects-index'            => array(
            'route'    => '/projects/index/*/',
            'defaults' => array(
                'controller' => 'projects',
                'action'     => 'index',
                '?redirect301?' => 'projects-index'
            ),
        ),
        'projects-update'           => array(
            'route'    => '/projects/save/*/',
            'defaults' => array(
                'controller' => 'projects',
                'action'     => 'save',
            ),
        ),
        'projects-logoUpload'       => array(
            'route'    => '/projects/logo-upload/*/',
            'defaults' => array(
                'controller' => 'projects',
                'action'     => 'logo-upload'
            ),
        ),
        'projects-userExclude'      => array(
            'route'    => '/projects/user-exclude/:projectId/:userId/',
            'defaults' => array(
                'controller' => 'projects',
                'action'     => 'user-exclude'
            ),
        ),
        'projects-userInvite'      => array(
            'route'    => '/projects/user-invite/:projectId/:userId/',
            'defaults' => array(
                'controller' => 'projects',
                'action'     => 'user-invite'
            ),
        ),
        /* end of projects board */

        /* Lead board routes */
        'leads-index'            => array(
            'route'    => '/leads/*/',
            'defaults' => array(
                'controller' => 'lead',
                'action'     => 'index',
            ),
        ),
        'leads-save'            => array(
            'route'    => '/leads/save/*/',
            'defaults' => array(
                'controller' => 'lead',
                'action'     => 'save',
            ),
        ),
        'leads-convert'         => array(
            'route'    => '/leads/convert/*/',
            'defaults' => array(
                'controller' => 'lead',
                'action'     => 'convert',
            ),
        ),
        /* end of lead board routes */

        'debug-exception'           => array(
            'route' => '/error/exception',
        ),
        'user-save' => array(
            'route' => '/user/save/',
            'defaults' => array(
                'module'     => 'local',
                'controller' => 'user',
                'action'     => 'save',
            )
        ),
    ),
);
