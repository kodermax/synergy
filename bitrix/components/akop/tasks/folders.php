<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arFoldersActionTill = array(
	"TODAY" => array("name" => GetMessage("AKOP_TASKS_TODAY"), "kind" => "till"),
	"TOMORROW" => array("name" => GetMessage("AKOP_TASKS_TOMORROW"), "kind" => "till"),
	"WEEK" => array("name" => GetMessage("AKOP_TASKS_WEEK"), "kind" => "till"),
);

$arFoldersAction = array(
	"FOCUS" => array("name" => GetMessage("AKOP_TASKS_FOCUS"), "kind" => "folder"),
	"NORMAL" => array("name" => GetMessage("AKOP_TASKS_NORMAL"), "kind" => "folder"),
	"MAYBE" => array("name" => GetMessage("AKOP_TASKS_MAYBE"), "kind" => "folder"),
	"WAITINGFOR" => array("name" => GetMessage("AKOP_TASKS_WAITINGFOR"), "kind" => "folder"),
	"FAST" => array("name" => GetMessage("AKOP_TASKS_FAST"), "kind" => "folder"),
	"STUPID" => array("name" => GetMessage("AKOP_TASKS_STUPID"), "kind" => "folder"),
);
?>