<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

require_once("CAkopTasks.php");


function cmp($a, $b) {
	return strcmp($a["NAME"], $b["NAME"]);
}

$tasks = new CAkopTasks();
$arResult["ITEMS"] = $tasks->getList(false, array("!STATUS" => 5));
$arResult["TAGS"] = $tasks->getListTags();

$arResult["RESPONSIBLES"] = array();
foreach ($arResult["ITEMS"] as $key => $value) {
	if (!isset($arResult["RESPONSIBLES"][$value["RESPONSIBLE_ID"]])) {
		$arResult["RESPONSIBLES"][$value["RESPONSIBLE_ID"]] = array(
			"NAME" => $value["RESPONSIBLE_LAST_NAME"] . " " . $value["RESPONSIBLE_NAME"],
			"COUNT" => 1
		);
	}
	$arResult["RESPONSIBLES"][$value["RESPONSIBLE_ID"]]["COUNT"]++;
}

uasort($arResult["RESPONSIBLES"], "cmp");


$arMenu = array(
	"ALL" => array("name" => GetMessage("AKOP_TASKS_ALL"), "kind" => "system"),
	"INBOX" => array("name" => GetMessage("AKOP_TASKS_INBOX"), "kind" => "system"),
);

include("folders.php");

$arMenu = array_merge($arMenu, $arFoldersActionTill, $arFoldersAction);
foreach ($arMenu as $key => $value) {

	if ($arParams["FOLDER_" . $key . "_SHOW"]  == "N")	{
		if ($arMenu[$key]["kind"] == "folder") {
			unset($arFoldersAction[$key]);
		} else {
			unset($arFoldersActionTill[$key]);
		}
		unset($arMenu[$key]);

	} else {
		if (isset($arParams["FOLDER_" . $key])) {
			$arMenu[$key]["name"] = $arParams["FOLDER_" . $key];
		}
	}
}
$arResult["MENU_ITEMS"] = $arMenu;
$arResult["FOLDERS_ACTION"] = $arFoldersAction;
$arResult["FOLDERS_ACTION_TILL"] = $arFoldersActionTill;

$this->IncludeComponentTemplate();
?>