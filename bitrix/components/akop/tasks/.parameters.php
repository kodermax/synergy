<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

include("folders.php");

$arFolders = array_merge($arFoldersActionTill, $arFoldersAction);

$arComponentParameters = array(
	"PARAMETERS" => array(
		"RESPONSIBLE_INC" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("AKOP_TASKS_RESPONSIBLE_INC"),
			"DEFAULT" => "5",
	    "TYPE" => "STRING",
		),
		"TAG_INC" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("AKOP_TASKS_TAG_INC"),
			"DEFAULT" => "20",
	    "TYPE" => "STRING",
		)
	)
);

foreach ($arFolders as $key => $value) {
	$arComponentParameters["GROUPS"][$key]	= array("NAME" => $value["name"]);
	$arComponentParameters["PARAMETERS"]["FOLDER_" . $key]	= array(
		"PARENT" => $key,
		"NAME" => GetMessage("AKOP_TASKS_FOLDER_NAME") . " " . $value["name"],
		"DEFAULT" => $value["name"],
    "TYPE" => "STRING",
	);
	$arComponentParameters["PARAMETERS"]["FOLDER_" . $key . "_SHOW"]	= array(
		"PARENT" => $key,
		"NAME" => GetMessage("AKOP_TASKS_SHOW"),
		"DEFAULT" => "Y",
    "TYPE" => "CHECKBOX",
	);
}

?>