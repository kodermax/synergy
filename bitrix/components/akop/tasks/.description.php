<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("AKOP_TASKS_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("AKOP_TASKS_COMPONENT_DESCR"),
	"COMPLEX" => "N",
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "content",
		"NAME" => GetMessage("AKOP_TASKS")
	)
	// 	"CHILD" => array(
	// 		"ID" => "akop",
	// 		"NAME" => GetMessage("AKOP_TASKS")
	// ),
);
?>