<?
$MESS["AKOP_TASKS_ALL"] = "Все";
$MESS["AKOP_TASKS_INBOX"] = "Входящие";
$MESS["AKOP_TASKS_TODAY"] = "Сегодня";
$MESS["AKOP_TASKS_TOMORROW"] = "Завтра";
$MESS["AKOP_TASKS_WEEK"] = "Неделя";
$MESS["AKOP_TASKS_FOCUS"] = "Важно";
$MESS["AKOP_TASKS_NORMAL"] = "Стандарт";
$MESS["AKOP_TASKS_MAYBE"] = "Может быть";
$MESS["AKOP_TASKS_WAITINGFOR"] = "Ожидание";
$MESS["AKOP_TASKS_FAST"] = "Быстрые дела";
$MESS["AKOP_TASKS_STUPID"] = "Рутина";
$MESS["AKOP_TASKS_SHOW"] = "Показывать?";
$MESS["AKOP_TASKS_FOLDER_NAME"] = "Название папки";
?>