<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

CJSCore::Init(array("jquery"));
$GLOBALS["APPLICATION"]->AddHeadScript("/bitrix/js/akop.tasks/tools.js");
$GLOBALS['APPLICATION']->AddHeadScript("/bitrix/components/akop/tasks/templates/.default/script.js");
$GLOBALS['APPLICATION']->AddHeadScript("/bitrix/components/bitrix/tasks.list/templates/.default/table-view.js");


// $GLOBALS["APPLICATION"]->AddHeadScript("//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js", true);
$GLOBALS["APPLICATION"]->AddHeadString("<script type=\"text/javascript\" src=\"//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js\"></script>", true);
$GLOBALS["APPLICATION"]->AddHeadString("<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css\">", true);

// CAkop::pr_var($arResult["MENU_ITEMS"], "MENU_ITEMS");
// CAkop::pr_var($arResult["FOLDERS_ACTION"], "FOLDERS_ACTION");

?>

<form class="navbar-form" id="task-filter">
  <div class="form-group">
		<select class="filter-group form-control">
		</select>
		<select class="filter-whoami form-control" id="filter-whoami">
			<option value="ALL" selected="selected"><?=GetMessage("AKOP_TASKS_ALL")?></option>
			<option value="RESPONSIBLE"><?=GetMessage("AKOP_TASKS_RESPONSIBLE")?></option>
			<option value="CREATOR"><?=GetMessage("AKOP_TASKS_CREATOR")?></option>
			<option value="CONTROL"><?=GetMessage("AKOP_TASKS_CONTROL")?></option>
		</select>
  </div>
	<div class="pull-right">
		<a id="tasks-print" class="btn btn-lg btn-default" href="#print" target="_blank"><span class="glyphicon glyphicon-print"></span></a>
		<a class="btn btn-lg btn-info" href="https://docs.google.com/document/d/1-BVAap47ipUYhMj113UyhHvMk8dumiHED4hHICmJeXA/edit?usp=sharing" target="_blank"><span class="glyphicon glyphicon-question-sign"></span></a>
	</div>
</form>
<div class="clearfix"></div>

<div>
	<ul id="nav-tasks" class="nav nav-pills">
		<?foreach ($arResult["MENU_ITEMS"] as $key => $value) : ?>
		  <li id="tab_<?=$key?>" data-folder-name="<?=$key?>" data-folder-kind="<?=$value["kind"]?>">
		    <a href="#">
		    	<?=$value["name"]?>&nbsp;<span class="label label-default"></span>
		    </a>
		  </li>
		<?endforeach;?>
	</ul>
</div>

<div class="tasks container-list container-fluid">
</div>
<div class="helper data-init" id="list-tasks" data-user-id="<?=CUser::GetId()?>" style="display:none;"><?=json_encode($arResult["ITEMS"], JSON_HEX_AMP)?></div>
<div class="helper data-init" id="list-folders" style="display:none;"><?=json_encode($arResult["MENU_ITEMS"], JSON_HEX_AMP)?></div>