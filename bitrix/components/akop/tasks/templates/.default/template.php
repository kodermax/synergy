<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

CJSCore::Init(array("jquery"));
$GLOBALS["APPLICATION"]->AddHeadScript("/bitrix/js/akop.tasks/tools.js");
$GLOBALS['APPLICATION']->AddHeadScript("/bitrix/components/bitrix/tasks.list/templates/.default/table-view.js");


// $GLOBALS["APPLICATION"]->AddHeadScript("//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js", true);
$GLOBALS["APPLICATION"]->AddHeadString("<script type=\"text/javascript\" src=\"//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js\"></script>", true);
$GLOBALS["APPLICATION"]->AddHeadString("<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css\">", true);

$GLOBALS["APPLICATION"]->SetTitle($GLOBALS["APPLICATION"]->GetTitle() . " - " . $arResult["USER_NAME"]);

// CAkop::pr_var($arResult["MENU_ITEMS"], "MENU_ITEMS");
// CAkop::pr_var($arResult["FOLDERS_ACTION"], "FOLDERS_ACTION");

?>
<div class="gtd-container">
	<div class="container-filter col-lg-3 pull-right">
		<div class="panel panel-info">
		  <div class="panel-heading">
		    <h3 class="panel-title"><?=GetMessage("AKOP_TASKS_FILTER_HEADING")?></h3>
		  </div>
	  	<div class="panel-body">
	  		<div>
				  <h4>
				    <?=GetMessage("AKOP_TASKS_FILTER_GROUP")?>
				  </h4>
					<select class="filter-group form-control">
					</select>
	  		</div>
	  		<div>
				  <h4>
				    <?=GetMessage("AKOP_TASKS_FILTER_WHOAMI")?>
				  </h4>
					<select class="filter-whoami form-control" id="filter-whoami" size="4">
						<option value="ALL" selected="selected"><?=GetMessage("AKOP_TASKS_ALL")?></option>
						<option value="RESPONSIBLE"><?=GetMessage("AKOP_TASKS_RESPONSIBLE")?></option>
						<option value="CREATOR"><?=GetMessage("AKOP_TASKS_CREATOR")?></option>
						<option value="CONTROL"><?=GetMessage("AKOP_TASKS_CONTROL")?></option>
					</select>
	  		</div>
	  		<div>
				  <h4>
				    <?=GetMessage("AKOP_TASKS_FILTER_RESPONSIBLE")?>
				  </h4>
		  		<div class="filter-responsibles">
		  			<?foreach ($arResult["RESPONSIBLES"] as $key => $value) : ?>
							<a href="#<?=$key?>" data-responsible-id="<?=$key?>" style="font-size:<?=(100 + $arParams["RESPONSIBLE_INC"] * $value["COUNT"])?>%;"><?=$value["NAME"]?></a>
						<?endforeach;?>
						<br/><a href="#all-responsibles" class="active btn btn-block filter-clear" data-tasks="false"><?=GetMessage("AKOP_TASKS_NO_FILTER_RESPONSIBLE")?></a>
		  		</div>
	  		</div>
<!-- 
	  		<div>
				  <h4>
				    <?=GetMessage("AKOP_TASKS_FILTER_TILL")?>
				  </h4>
		  		<div class="filter-till">
		  		</div>
	  		</div>
 -->

	  		<div>
				  <h4>
				    <?=GetMessage("AKOP_TASKS_FILTER_TAG")?>
				  </h4>
		  		<div class="filter-tags">
		  			<?foreach ($arResult["TAGS"] as $key => $value) : ?>
							<a href="#<?=$key?>" data-tasks="<?=implode(",", $value)?>" style="font-size:<?=(100 + $arParams["TAG_INC"] * count($value))?>%;"><?=$key?></a>
						<?endforeach;?>
						<br/><a href="#all-tags" class="active btn btn-block filter-clear" data-tasks="false"><?=GetMessage("AKOP_TASKS_NO_FILTER_TAG")?></a>
		  		</div>
	  		</div>
			</div>
			<?if ((count($arResult["SUBORDINATE_EMPLOYEES"]) > 0) || ($arResult["MANAGER_ID"] <> $arResult["USER_ID"])):?>
	  		<div>
					  <h4>
					    <?=GetMessage("AKOP_TASKS_EMPLOYEE")?>
					  </h4>
			  		<div class="subordinate-employees">
			  			<?foreach ($arResult["SUBORDINATE_EMPLOYEES"] as $key => $value) : ?>
								<!-- <a href="<?= __FILE__?>?user_id=<?=$key?>" data-employee-id="<?=$key?>"><?=$value?></a><br/> -->
								<a href="<?=$_SERVER['PHP_SELF']?>?user_id=<?=$key?>" data-employee-id="<?=$key?>"><?=$value?></a><br/>
							<?endforeach;?>
							<br/><a href="<?=$_SERVER['PHP_SELF']?>" class="active btn btn-block filter-clear"  data-employee-id="false"><?=GetMessage("AKOP_TASKS_ME")?></a>
			  		</div>
		  		</div>
				</div>
			<?endif;?>
		</div>
	</div>

	<div class="col-lg-9 pull-right">
		<div>
			<ul id="nav-tasks" class="nav nav-pills">
				<?foreach ($arResult["MENU_ITEMS"] as $key => $value) : ?>
				  <li id="tab_<?=$key?>" data-folder-name="<?=$key?>" data-folder-kind="<?=$value["kind"]?>">
				    <a href="#">
				    	<?=$value["name"]?>&nbsp;<span class="label label-default"></span>
				    </a>
				  </li>
				<?endforeach;?>
				<div class="pull-right">
					<a id="tasks-print" class="btn btn-lg btn-default disabled" href="#print" target="_blank"><span class="glyphicon glyphicon-print"></span></a>
					<a class="btn btn-lg btn-info" href="https://docs.google.com/document/d/1-BVAap47ipUYhMj113UyhHvMk8dumiHED4hHICmJeXA/edit?usp=sharing" target="_blank"><span class="glyphicon glyphicon-question-sign"></span></a>
				</div>
			</ul>
		</div>
		<div id="dismissable" class="container dismissable"> </div>

		<div class="tasks container-list container-fluid" id="tasks-container">
		</div>
	</div>


</div>
<?//echo json_encode($arResult["TAGS"], JSON_HEX_AMP)?>
<div class="helper data-init" id="list-tasks" data-user-id="<?=CUser::GetId()?>" style="display:none;"><?=json_encode($arResult["ITEMS"], JSON_HEX_AMP)?></div>
<div class="helper data-init" id="list-folders" style="display:none;"><?=json_encode($arResult["MENU_ITEMS"], JSON_HEX_AMP)?></div>
