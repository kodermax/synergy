<?
$MESS["AKOP_TASKS_ALL"] = "Все";
$MESS["AKOP_TASKS_INBOX"] = "Входящие";
$MESS["AKOP_TASKS_TODAY"] = "Сегодня";
$MESS["AKOP_TASKS_TOMORROW"] = "Завтра";
$MESS["AKOP_TASKS_WEEK"] = "Неделя";
$MESS["AKOP_TASKS_FOCUS"] = "Важно";
$MESS["AKOP_TASKS_NORMAL"] = "Стандарт";
$MESS["AKOP_TASKS_MAYBE"] = "Может быть";
$MESS["AKOP_TASKS_WAITINGFOR"] = "Ожидание";
$MESS["AKOP_TASKS_FAST"] = "Быстрые дела";
$MESS["AKOP_TASKS_STUPID"] = "Рутина";
$MESS["AKOP_TASKS_ABOUT"] = "Описание";
$MESS["AKOP_TASKS_RESPONSIBLE"] = "Я - ответственный";
$MESS["AKOP_TASKS_CREATOR"] = "Я - постановщик";
$MESS["AKOP_TASKS_CONTROL"] = "Требуется мой контроль";
$MESS["AKOP_TASKS_FILTER_HEADING"] = "Фильтр задач";
$MESS["AKOP_TASKS_FILTER_GROUP"] = "Группа/проект";
$MESS["AKOP_TASKS_FILTER_WHOAMI"] = "Я как участник задачи";
$MESS["AKOP_TASKS_FILTER_TAG"] = "Теги задач";
$MESS["AKOP_TASKS_FILTER_RESPONSIBLE"] = "Ответственный";
$MESS["AKOP_TASKS_FILTER_TILL"] = "Крайний срок";
$MESS["AKOP_TASKS_NO_FILTER_RESPONSIBLE"] = "Не фильтровать по ответственному";
$MESS["AKOP_TASKS_NO_FILTER_TAG"] = "Не фильтровать по тегам";
?>