var pathName = "/bitrix/tools/akop.tasks/";

var groups = {
	currentId: -1,
	list: [],

	show: function () {
		$select = $(".filter-group");
		$select.find("option").remove();
		
		var strGroups = "<option value=\"-1\" selected=\"selected\">ВСЕ группы/проекты</option>"
			+ "<option value=\"0\">Задачи вне групп/проектов</option>";

		for (var key in groups.list) {
			counter = 0;
			for (var i = 0; i < tasks.list.length; i++) {
				if (tasks.list[i].GROUP_ID == key) counter++;
			}
			if (counter) {
				strGroups += "<option value=\"" + key + "\">" + groups.list[key] + "</option>";
			}
		}
		$select.append(strGroups);
		$select.on("click", function(e) {

			groups.currentId = e.target.value;
			tasks.show();

		});

	},

	getList: function () {
		$.getJSON(pathName + "getGroupList.php")
			.done(function(response) {
				if (response["OK"]) {
					
					groups.list = response.ITEMS;

					groups.show();
					tasks.show();
				} else {
					akop.showMessage("Неизвестная ошибка", 5000, "danger");
				}
			})
			.fail(function(){
				akop.showMessage("Ошибка соединения с сервером. Попробуйте позже.", 5000, "danger");
			});

	}
}


var tasks = {
	userId: false,
	filterWhoAmI: "all",
	list: [],
	listTags: false,
	listByTag: false,
	listByEmployee: false,
	responsibleId: false,
	listStages: [],
	folders: [],
	foldersTill: [],

	/**
	 * show task list
	 */
	show: function () {

		/** Remove all tasks from container */
		
		$(".container-list .task").remove();
		
		var k = $(".nav-tasks .active");
		var folderName = $("#nav-tasks .active").attr("data-folder-name");

		var counters = [];
		counters["ALL"] = 0;
		counters["INBOX"] = 0;
		for (var key in tasks.folders) {
			counters[key] = 0;
		}

		for (var i = 0; i < tasks.list.length; i++) {
			var d = tasks.list[i];

			switch (tasks.filterWhoAmI) {
		    case "ALL":
					filterByMember = true;
		      break;
		    case "RESPONSIBLE":
					filterByMember = (d.RESPONSIBLE_ID == tasks.userId);
		      break;
		    case "CREATOR":
					filterByMember = (d.CREATED_BY == tasks.userId);
		      break;
		    case "CONTROL":
					filterByMember = ((d.STATUS == 4) && (d.CREATED_BY == tasks.userId));
		      break;
	      default:
					filterByMember = true;
			}

			var filterByGroup = ((d.GROUP_ID == groups.currentId) || (groups.currentId == -1));
			var filterByResponsible = (tasks.responsibleId) ? (tasks.responsibleId == d.RESPONSIBLE_ID) : true;
			var filterByTag = ((typeof tasks.listByTag == "boolean") ? true : (tasks.listByTag.indexOf(d.ID) > -1));

			if (filterByMember && filterByGroup && filterByResponsible && filterByTag) {
				counters["ALL"]++;
				counters[d.FOLDER]++;
				counters[d.FOLDER_TILL]++;
			}

			if (
				((d.FOLDER == folderName) || (d.FOLDER_TILL == folderName) || (folderName == "ALL")) // filter by folder
				&& filterByGroup // filter by group
				&& filterByMember // filter by whoami
				&& filterByResponsible 
				&& filterByTag
				) {

				/* wrap div */
				var strTask = 
					"<div id=\"task"	+ d.ID + "\" data-id=\"" + d.ID + "\"" 
					+ "class=\"task well row priority" + d.PRIORITY 
					+ ((d.STATUS == 5) ? " closed" : "") 
					+ ((d.STATUS == 4) ? " almost-closed" : "") 
					+ "\"" 
					+ " data-number=\"" + i + "\""
					+ ">";

				/* Task title */
				strTask += "<div class=\"task-main col-md-5\">"
				 	+ "<span class=\"label label-success\">" + d.ID + "</span>"
					+ "<div class=\"task-title\">"
					+ "<a href=\"/company/personal/user/" + tasks.userId + "/tasks/task/view/" + d.ID + "/\">" + d.TITLE + "</a>"
					+ "</div>";

				if (groups.currentId == -1) {
					strTask += ((d.GROUP_ID > 0) ? "<div class=\"task-group\">"
						+ "<a href=\"/workgroups/group/" + d.GROUP_ID + "/\">" + groups.list[d.GROUP_ID] + "</a>"
						+ "</div>" : "")
				}


				if ("ALL_TODAY_TOMORROW_WEEK".indexOf(folderName) > -1) {
					strTask += "<span class=\"glyphicon glyphicon-folder-close\"></span>&nbsp;" + tasks.folders[d.FOLDER]["name"];
				}

				strTask += "</div>";


				/* Actions */
				strTask += "<div class=\"task-tools col-md-3\">";

				if (!d.ACTION_APPROVE && !d.ACTION_DISAPPROVE && (d.RESPONSIBLE_ID == tasks.userId)) 
					strTask += "<a href=\"#\" class=\"\" data-function=\"tasks.close\"><span class=\"glyphicon glyphicon-flag\"></span>&nbsp;Завершить задачу</a><br/>";

				if (d.ACTION_APPROVE) 
					strTask += "<a href=\"#\" class=\"\" data-function=\"tasks.approve\"><span class=\"glyphicon glyphicon-thumbs-up\"></span>&nbsp;Принять работу</a><br/>";
            
				if (d.ACTION_DISAPPROVE) 
					strTask += "<a href=\"#\" class=\"\" data-function=\"tasks.disapprove\"><span class=\"glyphicon glyphicon-thumbs-down\"></span>&nbsp;Доделать</a><br/>";

				if (d.ACTION_CHANGE_DEADLINE) {
	        strTask += 
	          "<div><a id=\"dropdownMenuDefer\" href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-time\"></span>&nbsp;Отложить <b class=\"caret\"></b></a>"
						+ "<ul class=\"dropdown-menu\" id=\"#defer\" role=\"menu\">"
						+ "<li><a href=\"#\" data-function=\"tasks.defer\" data-days=\"1\">На завтра</a></li>"
						+ "<li><a href=\"#\" data-function=\"tasks.defer\" data-days=\"7\">На неделю</a></li>"
						+ "</ul></div>";
				}

        strTask += 
					"<div><a id=\"dropdownMenuFolder\" href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-folder-open\"></span>&nbsp;Переместить в папку <b class=\"caret\"></b></a>"
					+ "<ul class=\"dropdown-menu\" role=\"menu\">" ;

				for (var key in tasks.folders) {
					if (tasks.folders[key].kind == "folder") {
						strTask += "<li><a href=\"#\" data-function=\"tasks.moveToFolder\" data-folder=\"" + key + "\">" + tasks.folders[key]["name"] + "</a></li>";
					}
				}
				strTask += "</ul></div></div>";

				/* Deadline */
 				if (d.DEADLINE) deadline = d.DEADLINE.split(' ');
				strTask += "<div class=\"task-deadline-col col-md-2\">"
					+ ((d.DEADLINE) 
						? "<div class=\"task-deadline" + ((d.OVERDUED) ? " task-status-overdue" : "") + "\">"
							+ "<span class=\"glyphicon glyphicon-time\"></span>&nbsp;" 
							+ "<span>" 
							+ "<span class=\"task-deadline-date webform-field-action-link\" "
							+ "onclick=\"tasks.onDeadlineChangeClick(" + d.ID + ", this, '" + d.DEADLINE + "');\">" + deadline[0]
							// + "onclick=\"tasksListNS.onDeadlineChangeClick(" + d.ID + ", this, '" + d.DEADLINE + "');\">" + deadline[0]
							+ "</span>&nbsp;"
							+ "<span class=\"task-deadline-time webform-field-action-link\" "
							+ "onclick=\"tasks.onDeadlineChangeClick(" + d.ID + ", this, '" + d.DEADLINE + "');\">" + deadline[1]
							// + "onclick=\"tasksListNS.onDeadlineChangeClick(" + d.ID + ", this, '" + d.DEADLINE + "');\">" + deadline[1]
							+ "</span>"
							+ "</span>"
							+ "</div>" 
						: "")
					+ ((d.CLOSED_DATE && (d.STATUS == 4)) ? "<div><span class=\"glyphicon glyphicon-flag\"></span>&nbsp;" + d.CLOSED_DATE + "</div>" : "")
					+ "</div>";

				/* Task team */
				strTask += "<div class=\"task-team col-md-2\">" + tasks.getName(d.CREATED_BY_LAST_NAME, d.CREATED_BY_NAME, d.CREATED_BY_LOGIN) + "<br/>"
					+ "<span class=\"glyphicon glyphicon-hand-down\"></span>&nbsp;" + "<br/>"
					+ tasks.getName(d.RESPONSIBLE_LAST_NAME, d.RESPONSIBLE_NAME, d.RESPONSIBLE_LOGIN) + "</div>";

				strTask += "</div>";

				$(".container-list")
					.append(strTask);
			}

		};

		if (strTask == undefined) {
			$(".container-list")
				.append("<div class=\"task\">Нет элементов для просмотра в данном представлении</div>");
		}

		for (var key in counters) {
			$("#tab_" + key + " span").text((counters[key] > 0) ? counters[key] : "");
		}



		akop.closeMessage();

		$(".task").on("mouseenter", function(e) {
			console.log("onmouseover", e.target.id, e);
			// rect = akop.getOffsetRectById(e.target.id);
			console.log(rect);

	    var str = 
				"<div class=\"move2folder task-tools\">Переместить в папку"
				+ "<ul>" ;

			for (var key in tasks.folders) {
				if (tasks.folders[key].kind == "folder") {
					str += "<li><a href=\"#\" data-function=\"tasks.moveToFolder\" data-folder=\"" + key + "\">" + tasks.folders[key]["name"] + "</a></li>";
				}
			}
			str += "</ul></div></div>";

			$(".move2folder").remove();
			$("#" + e.target.id).append(str);
			// $("#" + e.target.id).append($(".move2folder").html());
			// $(".move2folder")
			tasks.setListener4TaskTool();
		});

		tasks.setListener4TaskTool();

	},


	setListener4TaskTool: function () {
		$(".task-tools a")
			.off()
			.on("click", function(e) {
				console.log("onclick", e, this.dataset);
				e.preventDefault();
				if ((this.dataset) && (this.dataset.function)) {
					var myFunction = eval('(' + this.dataset.function + ')');
					myFunction.call(this);
				}
			});
	},

	onDeadlineChange: function () {
		akop.debug.log("onDeadlineChange", tasks.deadline);

	},

	onDeadlineChangeClick: function (taskId, node, curDeadline) {
		BX.calendar({
			node: node, 
			value : curDeadline,
			form: '', 
			bTime: true, 
			currentTime: Math.round((new Date()) / 1000) - (new Date()).getTimezoneOffset()*60, 
			bHideTimebar: false,
			callback_after: (function(node, taskId) {
				return function(value, bTimeIn){
					$.getJSON(pathName + "setDeadline.php", {task_id: taskId, deadline: value})
						.done(function(response) {
							if (response["OK"]) {
								var taskNumber = tasks.getNumberById(response["ID"]);
								tasks.list[taskNumber].DEADLINE = response["DEADLINE"];
								tasks.list[taskNumber].FOLDER_TILL = response["FOLDER_TILL"];
								tasks.list[taskNumber].OVERDUED = response["OVERDUED"];
								tasks.show();
							} else {
								akop.showMessage("Неизвестная ошибка", 5000, "danger");
							}
						})
						.fail(function(){
							akop.showMessage("Ошибка соединения с сервером. Попробуйте позже.", 5000, "danger");
						});				
					};
				})
			(node, taskId)
		});
	},

	print: function () {

		// return 
	},

	getTask: function (obj) {
		return $(obj).closest(".task");
	},

	getId: function ($taskDiv) {
		return $($taskDiv).attr("data-id");
	},

	getNumber: function ($taskDiv) {
		return $($taskDiv).attr("data-number");
	},

	getNumberById: function (taskId) {
		return $('[data-id="' + taskId + '"]').attr("data-number");
	},

	approve: function () {
		tasks.action.call(this, "approve");
	},

	disapprove: function () {
		tasks.action.call(this, "disapprove");
	},

	defer: function () {
		tasks.action.call(this, "defer");
	},

	close: function () {
		tasks.action.call(this, "close");
	},

	moveToFolder: function () {
		tasks.action.call(this, "moveToFolder");
	},

	action: function (filename) {
		// akop.debug.log("moveToFolder", this, this.dataset.folder);
		var $taskDiv = tasks.getTask(this);
		var taskId = tasks.getId($taskDiv);
		// var taskNumber = tasks.getNumber($taskDiv);
		params = {task_id: taskId};
		switch (filename) {
	    case "moveToFolder":
	      params["folder"] = this.dataset.folder;
	      break;
	    case "defer":
	      params["days"] = this.dataset.days;
	      break;
		}

		$.getJSON(pathName + filename + ".php", params)
			.done(function(response) {
				if (response["OK"]) {
					var taskNumber = tasks.getNumberById(response["ID"]);
					switch (filename) {
				    case "moveToFolder":
							tasks.list[taskNumber].FOLDER = response["FOLDER"];
				      break;
				    case "defer":
							tasks.list[taskNumber].DEADLINE = response["DEADLINE"];
							tasks.list[taskNumber].OVERDUED = false;
							tasks.list[taskNumber].FOLDER_TILL = response["FOLDER_TILL"];
				      break;
				    default:
							tasks.list.splice(taskNumber, 1);
				      break;
					}
					
		// akop.debug.log("action", this, filename);
					tasks.show();
				} else {
					akop.showMessage("Неизвестная ошибка", 5000, "danger");
					// tasks.show();
				}
			})
			.fail(function(){
				akop.showMessage("Ошибка соединения с сервером. Попробуйте позже.", 5000, "danger");
				// tasks.show();
			});
	},

	getName: function (lastname, name, login) {
		result = lastname + " " + name;
		if (result == " ") result = "(" + login + ")";
		return result;
	},


	onload: function () {
		/* Показ сообщения о загрузке */
		akop.showMessage("Загрузка данных...");
		$tasks = $(".tasks");
		rect = akop.getOffsetRectById("tasks-container");
		$("#akop-alerter")
			.css("left", rect.left)
			.css("top", rect.top)
			.css("bottom", "auto");

		$listTasks = $("#list-tasks");
		tasks.userId = $listTasks.attr("data-user-id");

		tasks.folders = JSON.parse($("#list-folders").text());
		
		/* tags */
		$resp = $(".filter-responsibles");
		if ($resp.length > 0) {
			$resp.find("a")
				.on("click", function(e) {
					e.preventDefault();
					// akop.debug.log("resp click", e.target.dataset);
					$(".filter-responsibles a").removeClass("active")
					$(this).addClass("active");
					tasks.responsibleId = (e.target.dataset.responsibleId == "false") ? false : e.target.dataset.responsibleId;
					// tasks.listByTag = (e.target.dataset.tasks == "false") ? false : e.target.dataset.tasks.split(",");
					tasks.show();
				});
		}
		
		/* tags */
		$tags = $(".filter-tags");
		if ($tags.length > 0) {
			$tags.find("a")
				.on("click", function(e) {
					e.preventDefault();
					// akop.debug.log("tag click", e.target.dataset.tasks);
					$(".filter-tags a").removeClass("active")
					$(this).addClass("active");
					tasks.listByTag = (e.target.dataset.tasks == "false") ? false : e.target.dataset.tasks.split(",");
					tasks.show();
				});
		}

		tasks.list = JSON.parse($listTasks.text(), function(key, value) {
		  if (key == "FOLDER") {
		  	return (tasks.folders[value] == undefined) ? "INBOX" : value;
	  	}
		  return value;
		});


		$("#nav-tasks li")
			.removeClass("active")
			.on("click", function(e, i) {
				e.preventDefault();
				$("#nav-tasks li")
					.removeClass("active");

				e.target.parentNode.className = "active";
				tasks.show();
			});
		$("#tab_INBOX").addClass("active");
		$("body").css("background-color", $("#footer").css("background-color"));

		$(".filter-whoami").on("click", function(e) {
			tasks.filterWhoAmI = e.target.value;
			tasks.show();

		});

		$("#tasks-print").on("click", function(e) {
			e.preventDefault();
			tasks.print();

		});

		$(".data-init").remove();

		$("#tab_TODAY span").removeClass("label-default").addClass("label-danger");
		$("#tab_TOMORROW span").removeClass("label-default").addClass("label-warning");
		$("#tab_FOCUS span").removeClass("label-default").addClass("label-danger");
		$("#tab_INBOX span").removeClass("label-default").addClass("label-info");
/*
    var str = 
			"<div class=\"helper1\"><div class=\"move2folder\">Переместить в папку"
			+ "<ul>" ;

		for (var key in tasks.folders) {
			if (tasks.folders[key].kind == "folder") {
				str += "<li><a href=\"#\" data-function=\"tasks.moveToFolder\" data-folder=\"" + key + "\">" + tasks.folders[key]["name"] + "</a></li>";
			}
		}
		str += "</ul></div></div></div>";

		$(".gtd-container").append(str);
*/

		// tasks.show();

	},
}

$(document).ready(function() {
	tasks.onload();
	groups.getList();
});

