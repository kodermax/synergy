<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

$array = explode('/', __FILE__);
array_shift($array);

$array = array_slice($array, 0, -1);
$array[] = 'cities';
$array[] = 'cities.php';

$path = '/'.implode('/', $array);

include($path);

$arCityValues = Array();
$changeLang = false;

foreach($xml_city->city as $city)
{
  
	$id = $city->attributes()->id;

	$id2 = (int) $id[0];

	if (mb_detect_encoding($arCityValues[$id2]) !== LANG_CHARSET)
		$changeLang = true;
		
	$arCityValues[$id2] = ($changeLang) ? iconv(mb_detect_encoding($city[0]), LANG_CHARSET, $city[0]) : $city[0];
  
}

$arComponentParameters = array(
    'PARAMETERS' => array(
        'CACHE_TIME'  =>  array('DEFAULT'=>3600),
		'CITY_NAME' => array(
			'NAME'	=>	GetMessage('CITY_NAME'),
            'TYPE'  => "LIST",
            "VALUES" => $arCityValues,
			'DEFAULT'	=>	COption::GetOptionString('ithive.informer', 'city_name')
		)
	)
);
?>