<?

class CIthiveInformer {

	static function convert_encoding($toCharset, $string) {
		if (LANG_CHARSET != 'UTF-8') 
			return iconv(mb_detect_encoding($string), $toCharset, $string);
		else
			return $string;
	}
	
}

?>