<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage('NAME'),
    "DESCRIPTION" => GetMessage('DESCRIPTION'),
    "ICON" => "#",
    "SORT" => 20,
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "ID" => "ithive.informer",
            "NAME" => GetMessage('PATH_NAME'),
            "SORT" => 10,
            "CHILD" => array(
                "ID" => "news_cmpx",
            ),
        ),
    ),
);

?>