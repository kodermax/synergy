<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

require_once('classes/general/CIthiveInformer.php'); 

$obCache = new CPHPCache;

$life_time = 60 * 60 * 1;

$cache_id = "ithive.informer";

if ($obCache->StartDataCache($life_time, $cache_id, '/upload/')) {
	
	if ($arParams['CITY_NAME']) COption::SetOptionString($cache_id, 'city_name', $arParams['CITY_NAME']);
	$city_name = COption::GetOptionString($cache_id, 'city_name');
		
	$data_file = "http://export.yandex.ru/weather-ng/forecasts/".$city_name.".xml";	
	$xml = simplexml_load_file($data_file);
	$city_id = $xml->attributes()->region;
	
	$data_file = "http://export.yandex.ru/bar/reginfo.xml?region=$city_id"; // адрес xml файла 
	
	$xml = simplexml_load_file($data_file); 
	$arResult['traffic'] = (array) $xml->traffic;
	$arResult['weather'] = json_decode(json_encode((array) $xml->weather->day), TRUE);	
	
	/** Выбор двух дат по дням недели. Т.к. cbr не отдаёт данные о курсе валют за вск, понедельник. **/
	switch(date('D')) {
		case 'Sun':
			$arResult['date'][1] = $date1 = date('d/m/Y', time() - 60 * 60 * 48);
			$arResult['date'][2] = $date2 = date('d/m/Y', time() - 60 * 60 * 24);
			break;
		case 'Mon':
			$arResult['date'][1] = $date1 = date('d/m/Y', time() - 60 * 60 * 48);
			$arResult['date'][2] = $date2 = date('d/m/Y', time() + 60 * 60 * 24);
			break;
		case 'Tue':
			$arResult['date'][1] = $date1 = date('d/m/Y', time() - 60 * 60 * 72);
			$arResult['date'][2] = $date2 = date('d/m/Y');
			break;
		default:				
			$arResult['date'][1] = $date1 = date('d/m/Y', time() - 60 * 60 * 24);
			$arResult['date'][2] = $date2 = date('d/m/Y');
			break;
	}
	
	/** Парсинг курса валют по xml cbr.ru **/
	$cbr = array('USD'=>'R01235', 'EUR'=>'R01239');
	$data_temp = 'http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1='.$date1.'&date_req2='.$date2.'&VAL_NM_RQ=';
	
	foreach($cbr as $k=>$c) {
		$xml = simplexml_load_file($data_temp.$c);
		foreach($xml->Record as $Record) 
			$arResult['cbr'][$k][] = (string) $Record->Value;
	}
	
	/** Если есть блоки, которые не нужно показывать, то убираем их с помощью css. Кривой способ. **/
	$iblocks = COption::GetOptionString("ithive.informer", "informer_blocks");
	$arResult['BLOCKS_TO_SHOW'] = explode('|', $iblocks);
	
	echo "<style type='text/css'>";
	foreach($arResult['BLOCKS_TO_SHOW'] as $k=>$blocks) {
		$k2 = $blocks*2;
		$k3 = $k2+1;
		echo "
			div.city-info.sidebar-widget-item:nth-child($k2) {display: none;}
			div.weather-info.sidebar-widget-item:nth-child($k3) {display: none;}
			";
	}
	echo "</style>";
	
	$obCache->EndDataCache($arResult);
} else {
	$arResult = $obCache->GetVars();
}

$this->IncludeComponentTemplate();
?>