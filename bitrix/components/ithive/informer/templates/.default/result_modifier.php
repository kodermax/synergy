<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?	
	$APPLICATION->AddHeadString('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>');
	$APPLICATION->AddHeadString('<script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>');
	$APPLICATION->AddHeadString('<link href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css" rel="prefetch stylesheet"></link>');
	
	foreach($arResult['weather']['day_part'] as $k=>$day_part) {
		$celc = ($day_part['temperature']) ? intval($day_part['temperature']) : intval(($day_part['temperature_from'] + $day_part['temperature_to'])/2);
		$arResult['weather']['day_part'][$k]['temperature_2'] = ($celc > 0) ? "+ $celc" : "$celc";
	}
	$this->SetViewTarget("sidebar", COption::GetOptionString('ithive.informer', 'informer_sort'));
?>