<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if(!IsModuleInstalled('ithive.informer')) return;?>
<div class='sidebar-widget widget-informer'>
	<div class='sidebar-widget-top'>
		<div class='sidebar-widget-top-title'><?=GetMessage('TRAFFIC_WEATHER_TITLE')?></div>
	</div>
	
	<div class="city-info sidebar-widget-item">
		<?=GetMessage('TRAFFIC_TITLE')?> <?=CIthiveInformer::convert_encoding(SITE_CHARSET, $arResult['traffic']['title'])?>
	</div>
	<div class='weather-info sidebar-widget-item'>
		<span class='weather-traffic' style='background: <?=$arResult['traffic']['icon']?>;'>
			<span class='weather-level'><?=$arResult['traffic']['level']?></span>
		</span>
		<div class='traffic-info'>	
			<span class='weather-hint'><?=CIthiveInformer::convert_encoding(SITE_CHARSET, $arResult['traffic']['hint'][0])?></span>
		</div>
	</div>
		
	<div class="city-info sidebar-widget-item">
		<?=GetMessage('WEATHER_TITLE')?> <?=CIthiveInformer::convert_encoding(SITE_CHARSET, $arResult['traffic']['title'])?>
	</div>
	<div class='weather-info sidebar-widget-item'>
		<?$count = 0;?>
		<?foreach($arResult['weather']['day_part'] as $k=>$day_part):?>
			<?if ($count == 3): ?>
				<small><?=GetMessage('TOMORROW')?></small>
			<?endif;?>
			<div class='weather-daypart <?=($count==0)?"big_picture":(($count>2)?"no_picture":"small_picture")?>'>
				<span class='weather-timeofday'><?=CIthiveInformer::convert_encoding(SITE_CHARSET, $day_part['@attributes']['type'])?></span>
				
				<img src="<?=($count==0)?$day_part['image-v3']:$day_part['image-v2']?>" class='weather-image' />
				
				<?if ($day_part['temperature_2']) :?>
					<span class='weather-temp'>
						<span class='weather-temp-name'><?=$day_part['temperature_2']?></span>
					</span>
				<?else:?>
					<span class='weather-temp'><?=intval(($day_part['temperature_from'] + $day_part['temperature_to'])/2)?></span>
				<?endif;?>
			</div>
		<?$count++;?>
		<?endforeach;?>
	</div>
	
	
	<div class="city-info sidebar-widget-item">
		<?=GetMessage('CURRENCY_TITLE')?>
	</div>
	<div class='weather-info sidebar-widget-item'>
		<div class='currency-info'>
			<span class='currency-name'>&nbsp;</span>
			<span class='currency-market'>
				<span><?=$arResult['date'][1]?></span>
				<span><?=$arResult['date'][2]?></span>
			</span>
		</div>
		<?foreach($arResult['cbr'] as $k=>$v):?>
			<div class='currency-info'>
				<span class='currency-name'><?=$k?></span>
				<span class='currency-market'>
					<?foreach($v as $val):?>
						<span><?=$val?></span>
					<?endforeach;?>
				</span>
			</div>
		<?endforeach;?>
	</div>
</div>